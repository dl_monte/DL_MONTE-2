! *******************************************************************************
! *   DL_MONTE-2 software (STFC/DL)                                             *
! *                                                                             *
! *   A.V.Brukhno (C) 2016                                                      *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *                                                                             *
! *   - USER defined analytical forms for force-fields (vdw_*.f90 & user_*.inc) *
! *                                                                             *
! *******************************************************************************

    ! USER convention for parameterisation:

    ! a = prmvdw(1,ivdw) - ref distance (sigma, r_0)
    ! b = prmvdw(2,ivdw) - custom  (ref energy, e_0)
    ! c = prmvdw(3,ivdw) - custom
    ! d = prmvdw(4,ivdw) - custom
    ! e = prmvdw(5,ivdw) - custom

! << - user-defined : key > 100 ; keywords: 'usr1'/'usr2'/'usr3'/...

    ! Example user potential (12-6)
    !vu0(ru,rui,a,b,c,d,e) = (a*rui**6-b)*rui**6   ! test...
    !gu0(ru,rui,a,b,c,d,e) = (12.0_wp*a*rui**6 - 6.0_wp*b)*rui**6

    vu0(rui,a,b) = (a*rui-b)*rui                   ! = (a/r**6-b)/r**6 ! test...
    gu0(rui,a,b) = (12.0_wp*a*rui - 6.0_wp*b)*rui  ! = (12.0_wp*a/r**6-6.0_wp*b)/r**6

    ! User potential 1 (10-4)
    vu1(ru,rui,a,b) = (a*ru-b)*rui                 ! = (a/r**6-b)/r**4 ! test...
    gu1(ru,rui,a,b) = (10.0_wp*a*ru - 4.0_wp*b)*rui

    ! User potential 2 (9-3)
    vu2(ru,rui,a,b) = (a*ru-b)*rui                 ! = (a/r**6-b)/r**3 ! test...
    gu2(ru,rui,a,b) = (9.0_wp*a*ru - 3.0_wp*b)*rui

    ! User potential 3 (Morse + LJ) - just an example...
    vu3(ru,rui,a,b,c,d,e) = a*((1.0_wp - exp(c*(ru+b)))**2 - 1.0_wp) & ! test...
                          + rui*(d*rui + e)

    gu3(ru,rui,a,b,c,d,e) = 2.0_wp*ru*a*c*( 1.0_wp - exp(c*(ru+b)) ) & ! test...
                          * exp(c*(ru+b)) + rui*(12.0_wp*d*rui + 6.0_wp*e)

    ! User potential 4 (undefined)
    vu4(ru,rui,a,b,c,d,e) = 0.0_wp !*(ru+rui+a+b+c+d+e)
    gu4(ru,rui,a,b,c,d,e) = 0.0_wp !*(ru+rui+a+b+c+d+e)

    ! User potential 5 (undefined)
    vu5(ru,rui,a,b,c,d,e) = 0.0_wp !*(ru+rui+a+b+c+d+e)
    gu5(ru,rui,a,b,c,d,e) = 0.0_wp !*(ru+rui+a+b+c+d+e)

! user-defined - >>


