! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief 
!> - Data type defining an angle list within a molecule
!> @usage 
!> - call as `<angle_list_container>%<attribute>`
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @typefor an angle list
module anglist_type

    use kinds_f90

    implicit none

type anglist

        !> the number of angles within a molecule
    integer :: nang = 0

        !> apair(nang,4) stores triplets of connected atoms (indices/pointers?) and potential number (type?)
    integer, dimension(:,:), allocatable :: apair

end type

end module
