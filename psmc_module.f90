!***************************************************************************
!   Copyright (C) 2015 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - I/O, utility procedures and core non-control variables pertaining to phase-switch Monte Carlo.
!> - Phase-switch Monte Carlo moves can be found in `psmc_moves`
!> - Terminology: Particle positions within the unit cell can be either fractional or Cartesian;
!>   I often use the term 'real' to refer to Cartesian.
!> - Terminology: The 'conjugate' configuration of a configuration 'cfg' is the configuration which would
!>   result if a phase switch move were to be performed from 'cfg'. Similarly for the 'conjugate displacements',
!>   'conjugate volume', etc.
!> - Application of periodic boundary conditions: 1) When site positions are set they are always set to be within 
!>   the primary image; 2) When displacements are set they are always set to be within the primary image;
!>   3) When atomic or molecular positions are set, they are set so that the particle is moved to the position
!>   of the closest image of the vector sum of its site plus its displacement - which may not be the primary image.
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `psmc_control_type`
!> - `config_type`

!> @modulefor phase-switch Monte Carlo (except moves)
module psmc_module

    use kinds_f90
    use psmc_control_type
    use config_type    

    implicit none


        !> Global control parameters for PSMC
    type(psmc_control) :: psmc

        !> Active phase / box number (box 1 corresponds to phase 1, box 2 to phase 2)
    integer :: ib_act

        !> Passive phase / box number (box 1 corresponds to phase 1, box 2 to phase 2)
    integer :: ib_pas


        !> 2 'ideal' configurations which define phases 1 and 2, and define the phase switch.
        !> Note that not everything will be initialised in these configurations: they exist only
        !> as a convenient way to define the phases and lattice switch, and are not 'real'
        !> configurations
    type(config), dimension(2) :: psmc_cfgs

        !> Matrix defining the unit cell change upon switching from phase 1 to phase 2.
        !> Lattice vector l2_i (i=1,2,3) for phase 2 is obtained from that of phase 1 via:
        !> l2_i = cell_switch_12(i,1)*l1_1 + cell_switch_12(i,2)*l1_2 + cell_switch_12(i,3)*l1_3
    real(kind=wp), dimension(3,3) :: cell_switch_12

        !> Matrix defining the unit cell change upon switching from phase 2 to phase 1.
        !> Lattice vector l1_i (i=1,2,3) for phase 1 are obtained from that of phase 2 via:
        !> l1_i = cell_switch_21(i,1)*l2_1 + cell_switch_21(i,2)*l2_2 + cell_switch_21(i,3)*l2_3
    real(kind=wp), dimension(3,3) :: cell_switch_21


    public :: read_psmc_extras, &
              initialise_psmc, &
              configs_correspond, &
              is_ortho, &
              atom_can_move, &
              mol_can_move, &
              mol_can_rot, &
              write_config_dlpoly_2, &
              write_config_psmc_geom, &
              psmc_data_output, &
              psmc_melt_check, &
              real2frac, &
              frac2real, &
              frac2real_cfg, &
              set_atom_site, &
              set_atoms_site, &
              set_mol_site, &
              set_cfg_sites, &
              set_atom_u_from_pos, &                    
              set_atoms_u_from_pos, &
              set_mol_u_from_pos, & 
              set_cfg_u_from_pos, &
              set_atom_pos_from_u, &
              set_atoms_pos_from_u, &
              set_mol_pos_from_u, &
              set_cfg_pos_from_u, &
              translate_atom_u, &
              translate_atoms_u, &
              translate_mol_u, &
              translate_cfg_u, &
              set_passive_cfg


contains




! I/O and initialisation procedures




!> @brief
!> - Reads PSMC subsection in 'use fed' block.
!> - Terminates when encountering a line with 'ps done' as the first word - so long as all the 
!>   compulsory variables have been read, otherwise an error is returned.
!> @using
!> - `kinds_f90`
!> - `constants_module`
!> - `parse_module`
subroutine read_psmc_extras(unit)

    use kinds_f90
    use constants_module
    use parse_module

    implicit none

        !> Unit to read from
    integer, intent(in) :: unit

    ! Specifies if all the required variables have been read from the subsection
    logical :: is_done

    ! Array telling us whether the compulsory variables, of which there are n_comp, have 
    ! been read: comp_read(n)=0 if the nth compulsory variable has not been read, and 1 if 
    ! it has
    integer, parameter :: n_comp = 2
    integer, dimension(n_comp), save :: comp_read = 0
    
    ! Array telling us whether the optional variables, of which there are n_opt, have
    ! been read: opt_read(n)=0 if the the nth optional variable has not been read and 1 if
    ! it has
    integer, parameter :: n_opt = 8
    integer, dimension(n_opt), save :: opt_read = 0


    ! Temporary keyword & value storage
    character :: line*80, word*40, word1*40, fname*40
    
    logical :: more, safe

    is_done = .false.
    more = .true.
    safe = .true.

    get: do while( more )

        call get_line(safe,unit,line)
        if (.not.safe) call error(51)
        call lower_case(line)

        call get_word(line, word)

        if( word == "" .or. word(1:1) == "#" ) cycle get

        if( word(1:2) == "ps" ) then

            call get_word(line, word)     

            if( word(1:4) == "done" ) then

                ! 'ps done' has been detected...

                ! Check that all compulsory variables have been read. If so we really are done;
                ! if not then something has gone wrong

                if( sum(comp_read) == n_comp ) then

                    more = .false.
                    is_done = .true.

                else
                
                    call cry(uout,'', &
                    "ERROR: Encountered 'ps done' in PSMC block in CONTROL before all compulsory "// &
                    "PSMC variables have been read.", 51)

                end if

            else

                ! 'ps' followed by something other than 'done' has been detected...

                call cry(uout, '', &
                "ERROR: Unrecognised keyword '"//trim(word)//"' after 'ps' in FED PSMC block in "// &
                "CONTROL. Expecting either 'ps done' or more compulsory PSMC variables.", 51)

            end if


        else if( word(1:10) == "switchfreq" ) then

            ! Compulsory variable 1: switch_freq

            ! Check for double reading of this variable
            if( comp_read(1) /= 0 ) then
               
                call cry(uout,'', &
                    "ERROR: Keyword '"//word(1:10)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else
                
                call get_word(line, word)
                psmc%switch_freq = nint(word_2_real(word))
                comp_read(1) = 1

                ! Check the variable is sensible
                if( psmc%switch_freq < 0) then

                   call cry(uout,'', &
                        "ERROR: Phase switch move frequency must be >= 0.", 51)
                   stop

                end if
                
            end if


        else if( word(1:4) == "init" ) then

            ! Compulsory variable 2: init_act

            ! Check for double reading of this variable
            if( comp_read(2) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:4)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%init_act = nint(word_2_real(word))
                comp_read(2) = 1

                ! Check the variable is sensible
                if( psmc%init_act < 1 .or. psmc%init_act > 2) then

                    call cry(uout, '', &
                         "ERROR: Initial phase number must be 1 or 2.", 51)                    

                end if

            end if


        else if( word(1:8) == "datafreq" ) then

            ! Optional variable 1: data_freq

            ! Check for double reading of this variable
            if( opt_read(1) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:8)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%data_freq = nint(word_2_real(word))
                opt_read(1) = 1

                ! Check the variable is sensible
                if( psmc%data_freq < 1) then

                    call cry(uout, '', &
                         "ERROR: PSMC data frequency must be > 0.", 51)
                    
                end if

            end if


        else if( word(1:9) == "meltcheck" ) then

            ! Optional variable 2: melt_check (set to .true. if the keyword is detected; default is .false.)

            ! Check for double reading of this variable
            if( opt_read(2) /= 0 ) call cry(uout, '', &
                   "ERROR: Keyword '"//word(1:9)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            psmc%melt_check = .true.

            opt_read(2) = 1


        else if( word(1:10) == "meltthresh" ) then

            ! Optional variable 3: melt_thresh

            ! Check for double reading of this variable
            if( opt_read(3) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:10)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%melt_thresh = word_2_real(word)
                opt_read(3) = 1

                ! Check the variable is sensible
                if( psmc%melt_thresh <= 0.0_wp) then

                    call cry(uout, '', &
                         "ERROR: PSMC melt check threshold must be > 0", 51)
                    
                end if

            end if

        else if( word(1:8) == "meltfreq" ) then

            ! Optional variable 4: melt_freq

            ! Check for double reading of this variable
            if( opt_read(4) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:8)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%melt_freq = nint(word_2_real(word))
                opt_read(4) = 1

                ! Check the variable is sensible
                if( psmc%melt_freq < 1) then

                    call cry(uout, '', &
                         "ERROR: PSMC melt check frequency must be > 0.", 51)
                    
                end if

            end if


        else if( word(1:11) == "hardcorerad" ) then

            ! Optional variable 5: hc_vdw

            ! Check for double reading of this variable
            if( opt_read(5) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:11)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%hc_vdw = word_2_real(word)
                opt_read(5) = 1

                ! Check the variable is sensible
                if( psmc%hc_vdw <= 0.0) then

                    call cry(uout, '', &
                         "ERROR: PSMC hard-core radius must be > 0.", 51)
                    
                end if

            end if


        else if( word(1:10) == "switchbias" ) then

            ! Optional variable 6: switchbias

            ! Check for double reading of this variable
            if( opt_read(6) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:10)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else

                call get_word(line, word)
                psmc%switchbias = word_2_real(word)
                opt_read(6) = 1

            end if

        else if( word(1:14) == "cartdispswitch" ) then

            ! Optional variable 7: cartdispswitch

            ! Check for double reading of this variable
            if( opt_read(7) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:14)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else
        
                psmc%cart_disp_switch = .true.
                opt_read(7) = 1

            end if

        else if( word(1:16) == "ignorevolscaling" ) then

            ! Optional variable 8: ignorevolscaling

            ! Check for double reading of this variable
            if( opt_read(8) /= 0 ) then

                call cry(uout, '', &
                     "ERROR: Keyword '"//word(1:14)//"' has already been specified in FED PSMC block in CONTROL.", 51)

            else
        
                psmc%ignore_vol_scaling = .true.
                opt_read(8) = 1

            end if


        else

            ! Unrecognised keyword

            call cry(uout, '', &
                 "ERROR: Unrecognised keyword '"//trim(word)//"' in FED PSMC block in CONTROL. Expecting either"// &
                 " 'ps done' or more compulsory PSMC variables.", 51)

        end if


    end do get

end subroutine read_psmc_extras




!> @brief
!> - Read psmc_cfgs from the files CONFIG.1 and CONFIG.2, and initialise PSMC variables.
subroutine initialise_psmc(job)

    use kinds_f90
    use control_type
    use config_type
    use config_module, only : inputconfig
    use constants_module
    use cell_module, only : cfgs, set_mol_coms
    use latticevectors_module, only : invertlatvec
    use comms_mpi_module, only : master
    use field_module, only : vdwcap
    use molecule_module, only : full_mol_com
    use vdw_module, only : prmvdw, ntpvdw, ltpvdw

    implicit none

        !> Simulation control variables
    type (control), intent(inout) :: job

    ! Dummy integer for 'inputconfig': not used for anything
    integer :: ib
    ! Dummy integer for 'inputconfig': not used for anything
    integer :: cfgfmt
    ! Configuration / active replica number
    integer :: icfg

    ! Flag which is .true. if an atom is taken outside the slit - if the
    ! slit is in use
    logical :: atom_out

    real(kind=wp) :: dot
    integer :: fail, i, j
    logical file_exists




    ! Read ideal configurations



    if( master ) then
        
        write(uout,*)    
        write(uout,*)
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*)
        write(uout,*) "Reading PSMC phase 1 ideal configuration from 'CONFIG.1'..."
        write(uout,*)
        
    end if

    inquire(file='CONFIG.1', exist=file_exists, iostat=fail) 
    
    if(.not. file_exists .or. fail>0) then
    
        call cry(uout, '', &
             "ERROR: Cannot find file 'CONFIG.1'", 51)
       
    end if
    
    open(ucfg, file='CONFIG.1', status = 'old', iostat=fail)

    if(fail>0) then

        call cry(uout, '', "ERROR: Cannot open file 'CONFIG.1'", 51)

    end if

    ! 'ib' is only used by 'inputconfig' in that it if any atoms are outside the box a warning is
    ! output referring to the box as "box 'ib'". Use ib=-1 why not.
    ib=-1
    call inputconfig(job, ib, psmc_cfgs(1), cfgfmt)

    ! If the 2nd line of CONFIG.1 specifies fractional coordinates then we must convert
    ! the current coordinates, which are fractional, into 'real' coordinates for internal
    ! use by the program
    if (cfgfmt == 0) call frac2real_cfg(psmc_cfgs(1))
   
    close(ucfg)

    ! Note: No need to put atoms in psmc_cfgs(1) and psmc_cfgs(2) back in primary image here (or ever),
    ! since when psmc_cfgs(1) or psmc_cfgs(2) is used to determine the position of a site in the active
    ! or passive box the site position is wrapped to the primary image - regardless of whether or not the
    ! position in psmc_cfgs(1) or psmc_cfgs(2) is in the primary image.

    if( master ) then

        write(uout,*)    
        write(uout,*)    
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*) "                PSMC phase 1 ideal configuration"
        write(uout,*)
    
        ! To be replaced with 'call printbasis_phase(1)' when written
        call write_config_dlpoly_2(uout, psmc_cfgs(1))


        write(uout,*)
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*)
        write(uout,*) "Reading PSMC phase 2 ideal configuration from 'CONFIG.2'..."
        write(uout,*)
        
    end if

    inquire(file='CONFIG.2', exist=file_exists, iostat=fail) 
    
    if(.not. file_exists .or. fail>0) then
           
        call cry(uout, '', "ERROR: Cannot find file 'CONFIG.2'", 51)
        
    end if

    open(ucfg, file='CONFIG.2', status = 'old', iostat=fail)

    if(fail>0) then
            
        call cry(uout, '', "ERROR: Cannot open file 'CONFIG.2'", 51) 

    end if

    ! 'ib' is only used by 'inputconfig' in that it if any atoms are outside the box a warning is
    ! output referring to the box as "box 'ib'". Use ib=-2 why not.
    ib=-2
    call inputconfig(job, ib, psmc_cfgs(2), cfgfmt)

    ! If the 2nd line of CONFIG.2 specifies fractional coordinates then we must convert
    ! the current coordinates, which are fractional, into 'real' coordinates for internal
    ! use by the program
    if (cfgfmt == 0) call frac2real_cfg(psmc_cfgs(2))


    close(ucfg)

    if( master ) then
           
        write(uout,*)    
        write(uout,*)    
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*) "                PSMC phase 2 ideal configuration"
        write(uout,*) 

        ! To be replaced with 'call printbasis_phase(2)' when written
        call write_config_dlpoly_2(uout, psmc_cfgs(2))  
        

    end if

    ! Initialise only required variables (not all) in ideal configurations

    ! Set inverse lattice vectors
    call invertlatvec(psmc_cfgs(1)%vec)
    call invertlatvec(psmc_cfgs(2)%vec)

    ! Set the molecule COMs for the reference configurations. This is required before the molecular sites and 
    ! displacements can be set for the passive and active configurations.
    do i=1, psmc_cfgs(1)%num_mols

        call full_mol_com(psmc_cfgs(1)%mols(i),  psmc_cfgs(1)%vec%invlat, psmc_cfgs(1)%vec%latvector)

    end do

    do i=1, psmc_cfgs(2)%num_mols

        call full_mol_com(psmc_cfgs(2)%mols(i),  psmc_cfgs(2)%vec%invlat, psmc_cfgs(2)%vec%latvector)

    end do
    



    ! Initialisation and checks...


    ! DCD output is not supported with PSMC
    if( job%archive_format == DLP2DCD .or. job%archive_format > DLP4 ) then

        call cry(uout, '', &
            "ERROR: DCD trajectory format is not supported with PSMC. Amend 'archiveformat' in CONTROL.", 51)

    end if


    ! Check that only the allowed move types have been specified
    
    ! Only allow non-sequential atom, molecule translation, molecule rotation, and volume moves for now; insist on them
    
    if( job%useseqmove .or.  job%gcmcatom .or. job%swapatoms .or. &
       job%semiwidomatms .or. job%gibbsatomtran .or. job%gibbsatomexch .or. &
       job%gcmcmol .or. job%semigrandatms .or. job%semigrandmols .or. &
       job%swapmols .or. job%gibbsmoltran ) then
       
       call cry(uout, '', &
            "ERROR: Only non-sequential atom and molecule rotation/translation moves currently supported with PSMC.", 51)

    end if

    if(.not.( job%moveatm .or. job%movemol .or. job%rotatemol .or. &
              job%type_vol_move /= NPT_OFF .or. psmc%switch_freq > 0 )) then
       
       call cry(uout, '', &
            "ERROR: PSMC requires at least one of atom, molecule, volume or switch moves to be enabled", 51)

    end if

!!$    ! Preserving Cartesian (as opposed to fractional) displacements in a switch is only supported for constant-volume
!!$    ! ensemble - because the acceptance rule code hasn't been generalised for this situation yet!
!!$    if( psmc%cart_disp_switch .and. job%type_vol_move /= NPT_OFF ) then
!!$       
!!$       call cry(uout, '', &
!!$            "ERROR: PSMC preserving Cartesian displacement not supported for volume moves", 51)
!!$
!!$    end if
    

    ! Insist that quaternions are used
    if( job%rotatemol ) then
        
        if( .not. job%usequaternion ) then

            call cry(uout, '', &
                "ERROR: Only quaternion rotations are currently supported with PSMC.", 51)            

        end if

    end if

    if( job%movemol .or. job%rotatemol ) then
        
       call cry(uout, '', &
            "WARNING: PSMC for molecule moves is still under development. Expect the unexpected!", 0)

    end if


    ! Check that there are 2 configurations read from CONFIG - which will be the starting
    ! configurations for phase 1 and 2
    if( job%nconfigs /= 2 ) then

       call cry(uout, '', "ERROR: 2 configurations must be present in CONFIG for PSMC.", 51)
    
    end if

    ! Perform checks on ideal configurations and those in CONFIG
    
    ! Check the all configurations have 'corresponding structures'
    call configs_correspond(job)

    ! Check that the ideal configurations and the configurations in CONFIG are all orthorhombic
    if( is_ortho() .neqv. .true. ) then

        call cry(uout, '', &
             "ERROR: All input configurations (CONFIG, CONFIG.1, CONFIG,2) must be orthorhombic for PSMC.", 51)
       
    end if
    
    ! Determine cell_switch_12 and cell_switch_21 (assumes orthorhombic!)
    do i = 1, 3
        do j = 1, 3
            dot = dot_product(psmc_cfgs(2)%vec%latvector(i,:), psmc_cfgs(1)%vec%latvector(j,:))
            cell_switch_12(i,j) = dot / dot_product(psmc_cfgs(1)%vec%latvector(j,:), psmc_cfgs(1)%vec%latvector(j,:))
            cell_switch_21(i,j) = dot / dot_product(psmc_cfgs(2)%vec%latvector(i,:), psmc_cfgs(2)%vec%latvector(i,:))
        end do
    end do

    if( master ) then

        write(uout,*)
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*) "       PSMC unit cell transformation matrices for switch"
        write(uout,*) 
        write(uout,*) "phase 1 -> 2:"
        write(uout,*)
        write(uout,*) cell_switch_12(1,:)
        write(uout,*) cell_switch_12(2,:)
        write(uout,*) cell_switch_12(3,:)
        write(uout,*)
        write(uout,*) "phase 2-> 1:"
        write(uout,*)
        write(uout,*) cell_switch_21(1,:)
        write(uout,*) cell_switch_21(2,:)
        write(uout,*) cell_switch_21(3,:)
        write(uout,*)
        
    end if

    ! Set the initial active and passive box numbers / phases
    select case(psmc%init_act)
    case(1)

       ib_act = 1
       ib_pas = 2

    case(2)

       ib_act = 2
       ib_pas = 1

    case default

       call cry(uout, '', "ERROR: initial PSMC active phase is not 1 or 2", 51)
      
    end select


    ! Change 'vdwcap'...
    ! The variable 'vdwcap' in 'field.f90' is an important threshold for PSMC. If, in calculating
    ! the VdW energy associated with an atom, the energy at any point is found to be greater than 
    ! 'vdwcap', then the calculation is stopped, and the 'current' energy is returned. For example
    ! if an atom has 3 hard-sphere overlaps with surrounding atoms, then, since HS_ENERGY (see 
    ! 'vdw_module.f90') >> vdwcap by default, instead of an energy of 3*HS_ENERGY being returned, 
    ! HS_ENERGY is returned - the calculation never proceeds to consider the other two overlaps. 
    ! This is problematic for PSMC since overlaps are expected in the passive box, and it is
    ! essential that the energy of the passive box reflects the number of overlaps in the box.
    ! Another problem with 'vdwcap' is that moves which yield energies greater than 'vdwcap' are
    ! automatically rejected. This could be disabled for passive boxes, allowing passive boxes to
    ! take any energy. However, it is wise to have an upper limit on the allowed energy for any
    ! box to avoid floating box issues. 
    ! The default value of 'vdwcap', namely 1.0E5, is too small for both problems mentioned above.
    ! Hence we set 'vdwcap' to a value more suitable for PSMC - unless the default has already
    ! been overidden by the user (i.e., it is not 1.0E5).
    if( vdwcap == 1.0E5 ) then

        if( master ) then

            write(uout,*)
            write(uout,*) "----------------------------------------------------------------------"
            write(uout,*)
            write(uout, "(/,1x,'WARNING: Default vdwcap of 1.0E5 detected. Overriding this to 1.0E106 for PSMC.')" )
            write(uout, "(1x,'         (This is normal behaviour for PSMC)')")
            write(uout,*)

        end if

        vdwcap = 1.0E106_wp
       
    else

        if( master ) then

            write(uout,*)
            write(uout,*) "----------------------------------------------------------------------"
            write(uout,*)
            write(uout, "(/,1x,'WARNING: User-specified VdW cap detected. Leaving VdW cap at its current value of ',E12.4)" ) &
                 vdwcap
            write(uout, "(10x,'This may result in moves being spuriously rejected due to VdW cap being exceeded in passive box.')")
            write(uout,*)

        end if

    end if

    ! Change the hard-core radius for ALL VdW potentials which are ENTIRELY SOFT (e.g. the behvaiour of the hard-sphere, A-O 
    ! and square-well potentials are not altered below). This radius is stored in 'prmvdw(0,:)' in 'vdw_module.f90'.
    ! If pairs of atoms are less than this radius apart then the VdW interaction energy associated with the pair is set to 
    ! 'a very high value'.
    ! Note that, unlike the case for non-PSMC simulations, in PSMC the hard-core radius for entirely soft potentials is set
    ! to the same value for all types of potential. By contrast in non-PSMC simulations the hard-core radius is smaller for
    ! potentials with a shorter range (e.g., in the Lennard-Jones potential the smaller sigma, the smaller the hard-core 
    ! radius).
    do i = 1, ntpvdw
    
        ! Cycle through all VdW potentials...
        
        ! Alter the cap for all potentials except those containing a 'hard' component, 
        ! i.e., hard-sphere / square-well (VdW type label = -1), A-O (label = 14), and Yukawa (label = 15)

        select case(ltpvdw(i))
        case(-1, 14, 15)

            ! Do nothing for potentials with an explicit hard component...
            continue

        case default

            ! For all other potentials alter the hard-core radius
            prmvdw(0,i) = psmc%hc_vdw

            if( master ) then

                write(uout,*)
                write(uout,*) "----------------------------------------------------------------------"
                write(uout,*)
                write(uout, "(/,1x,'WARNING: Altering D_core for VdW potential no. ',I5,' to ',E12.4,' angstrom."// &
                            "This is normal PSMC behaviour')" )i, psmc%hc_vdw
                write(uout, "(10x,'D_core should be less than ''typical'' atomic separations in both boxes "// &
                            "during the PSMC simulation,')")
                write(uout, "(10x,'otherwise huge order parameter values could occur. "// &
                            "See the manual for how to change this value')")
                write(uout, "(10x,'if required.')")
                write(uout,*)
        
            end if

        end select

    end do


    ! Set the 'psmcparticle' flags for all molecules
    call set_psmcparticle(job)

    ! Initialise the atom initial sites and displacements

    ! Set the active box sites and displacements
    call set_cfg_sites(ib_act, ib_act)
    call set_cfg_u_from_pos(ib_act)

    ! Set the passive box to be the conjugate of the active box (catch 'atom_out' error below)
    call set_passive_cfg(ib_act, ib_pas, ib_act, atom_out)

    if( master ) then

        write(uout,*)
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*)
        write(uout, "(/,1x,'WARNING: Initial configuration of box ',I1,' has been set to the conjugate of ',I1)" &
             ) ib_pas, ib_act
        write(uout, "(1x,'         (This is normal behaviour for PSMC)')")
        
        write(uout,*)    
        write(uout,*)    
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*) "         PSMC phase 1 initial site positions and displacements"
        write(uout,*) 
        call write_config_psmc_geom(job, uout, cfgs(1))

        write(uout,*)    
        write(uout,*)    
        write(uout,*) "----------------------------------------------------------------------"
        write(uout,*) "         PSMC phase 2 initial site positions and displacements"
        write(uout,*) 
        call write_config_psmc_geom(job, uout, cfgs(2))

    end if

    ! Catch error if conjugate configuration has atom outside slit - if slit is in use
    if( atom_out ) call cry(uout, '', &
                            "ERROR: Initial PSMC conjugate configuration has atom outside slit",999)

    if( master ) then

        write(uout,*)    
        write(uout,*)    
        write(uout,*) "----------------------------------------------------------------------"       
        write(uout,*) "                   Miscellaneous PSMC information"
        write(uout,*) 
        write(uout, "(' PSMC initial active phase                         = ',I17,/)" ) ib_act
        write(uout, "(' PSMC initial passive phase                        = ',I17,/)" ) ib_pas
        write(uout, "(' Frequency of switch moves (MC cycles)             = ',I17,/)" ) psmc%switch_freq
        write(uout, "(' Frequency of output to PSDATA* (cycles)           = ',I17,/)" ) psmc%data_freq
        write(uout, "(' Use melt checks                                   = ',L17,/)" ) psmc%melt_check
        write(uout, "(' Melt threshold (Angstroms)                        = ',F17.8,/)" ) psmc%melt_thresh
        write(uout, "(' Melt check frequency (cycles) (output to OUTPUT*) = ',I17,/)" ) psmc%melt_freq
        write(uout, "(' D_core for soft VdW potentials (Angstroms)        = ',F17.8,/)" ) psmc%hc_vdw
        write(uout,*)
        write(uout,*) "----------------------------------------------------------------------" 
        write(uout,*)
        
    end if

end subroutine initialise_psmc




!> @brief
!> - Checks that the lattice vectors for all configurations used in PSMC are orthorhombic (with 
!>   each vector along a different Cartesian direction)
!> @using
!> - `kinds_f90`
!> - `cell_module, only : cfgs`
logical function is_ortho()

    use kinds_f90
    use cell_module, only : cfgs

    implicit none

    integer :: i, j, counts

    is_ortho = .true.

    ! For each row and column of the lattice vector matrix latvector for the
    ! PSMC configurations, count the number of 0 elements in that row and column.
    ! It should always be 2
    do i = 1, 3

        ! Ideal configuration for phase 1, row i
        counts = count( psmc_cfgs(1)%vec%latvector(i,:) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 1, column i
        counts = count( psmc_cfgs(1)%vec%latvector(:,i) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 2, row i
        counts = count( psmc_cfgs(2)%vec%latvector(i,:) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 2, column i
        counts = count( psmc_cfgs(2)%vec%latvector(:,i) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Configuration for phase 1, row i
        counts = count( cfgs(1)%vec%latvector(i,:) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 1, column i
        counts = count( cfgs(1)%vec%latvector(:,i) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 2, row i
        counts = count( cfgs(2)%vec%latvector(i,:) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

        ! Ideal configuration for phase 2, column i
        counts = count( cfgs(2)%vec%latvector(:,i) == 0.0_wp )
        if( counts /= 2) is_ortho = .false.

    end do

end function is_ortho




!> @brief
!> - Checks that all configurations used by PSMC have 'corresponding structures': that they all have the same 
!>   number of molecules; that molecule 'j', if it moves (rotational or translational), then it moves for all
!>   configurations; that all moving molecules are rigid bodies; that a non-moving molecule 'j' which contains moving
!>   atoms has the same number of moving atoms over all configurations; and that if an atom 'i' is moving in molecule
!>   'j' then this is the case for all configurations.
subroutine configs_correspond(job)

    use control_type
    use constants_module, only : uout
    use cell_module, only : cfgs
    use species_module, only : number_of_molecules, uniq_mol

    implicit none

        !> Simulation control variables
    type(control), intent(in) :: job

    integer :: i, j, num_mols, natom
    integer, dimension(4) :: moveableatms

    logical :: correspond, moveable, molcanmove, molcanrot

    correspond = .true.

    ! Check number of molecules is the same for all configurations (psmc_cfgs(1), psmc_cfgs(2), 
    ! cfgs(1), cfgs(2))
    num_mols = psmc_cfgs(1)%num_mols
    if( psmc_cfgs(2)%num_mols /= num_mols ) correspond = .false. 
    if( cfgs(1)%num_mols /= num_mols ) correspond = .false.
    if( cfgs(2)%num_mols /= num_mols ) correspond = .false.

    if( .not. correspond ) then

        call cry(uout, '', &
             "ERROR: Input configurations (CONFIG, CONFIG.1, CONFIG.2) do not have same number of molecules.",51)

    end if

    ! Loop over molecules
    do i = 1, num_mols

        ! Molecular checks...

        molcanmove = .false.
        molcanrot = .false.

        if( job%movemol ) then
        
            ! Check that molecule i uses translation moves, or not, in all configurations.
            molcanmove = mol_can_move( job, psmc_cfgs(1)%mols(i) )
            if( mol_can_move( job, psmc_cfgs(2)%mols(i) ) .neqv. molcanmove )  correspond = .false.
            if( mol_can_move( job, cfgs(1)%mols(i)      ) .neqv. molcanmove )  correspond = .false.
            if( mol_can_move( job, cfgs(2)%mols(i)      ) .neqv. molcanmove )  correspond = .false.

            if( .not. correspond ) then

                call cry(uout, '', &
                     "ERROR: Mol. translation moves must be enabled for corresponding molecules in " // &
                     "CONFIG, CONFIG.1 & CONFIG.2.", 51)

            end if

        end if

        if( job%rotatemol ) then
        
            ! Check that molecule i uses rotation moves, in all configurations.
            molcanrot = mol_can_rot( job, psmc_cfgs(1)%mols(i) )
            if( mol_can_rot( job, psmc_cfgs(2)%mols(i) ) .neqv. molcanrot )  correspond = .false.
            if( mol_can_rot( job, cfgs(1)%mols(i)      ) .neqv. molcanrot )  correspond = .false.
            if( mol_can_rot( job, cfgs(2)%mols(i)      ) .neqv. molcanrot )  correspond = .false.

            if( .not. correspond ) then

                call cry(uout, '', &
                     "ERROR: Mol. rotation moves must be enabled for corresponding molecules in " // &
                     "CONFIG, CONFIG.1 & CONFIG.2.", 51)

            end if

        end if

     
        ! Atom checks...
       

        ! Count how many moving atoms are in this molecule
        moveableatms(:) = 0

        do j = 1, psmc_cfgs(1)%mols(i)%natom
            if( atom_can_move( job, psmc_cfgs(1)%mols(i)%atms(j) ) ) &
                 moveableatms(1) = moveableatms(1) + 1
        end do
        do j = 1, psmc_cfgs(2)%mols(i)%natom
            if( atom_can_move( job, psmc_cfgs(2)%mols(i)%atms(j) ) ) &
                 moveableatms(2) = moveableatms(2) + 1
        end do
        do j = 1, cfgs(1)%mols(i)%natom
            if( atom_can_move( job, cfgs(1)%mols(i)%atms(j) ) ) &
                 moveableatms(3) = moveableatms(3) + 1
        end do
        do j = 1, cfgs(2)%mols(i)%natom
            if( atom_can_move( job, cfgs(2)%mols(i)%atms(j) ) ) &
                 moveableatms(4) = moveableatms(4) + 1
        end do

        ! The number of moving atoms should be the same for this molecule in all configurations. Check this
        if( moveableatms(2) /= moveableatms(1) .or. &
            moveableatms(3) /= moveableatms(1) .or. &
            moveableatms(4) /= moveableatms(1) ) then
             call cry(uout, '', &
                 "ERROR: Corresponding non-moving molecules in CONFIG, CONFIG.1 & CONFIG.2 must have "// &
                 "the same number of moving atoms.", 51)
            
        end if

        ! If the molecule is moving then we insist that it have no moving atoms, and that it is a rigid body
        if( ( molcanmove .or. molcanrot ) .and. moveableatms(1) > 0 ) then

            call cry(uout, '', &
                 "ERROR: Flexible molecules are forbidden in PSMC - moving molecules cannot contain "// &
                 "moving atoms.", 51)

        end if

        if(  ( molcanmove .or. molcanrot ) .and. &
             ( (.not. cfgs(1)%mols(i)%rigid_body) .or. (.not. cfgs(2)%mols(i)%rigid_body) )  ) then

            call cry(uout, '', &
                 "ERROR: Moving molecules in PSMC must be rigid bodies. Try adding 'rigid' to molecule "// &
                 "specifications in 'FIELD'.", 51)

        end if
                

        if( moveableatms(1) > 0 ) then

            ! If there are any moving atoms in the molecule...
               
               
            ! Check the number of atoms in molecule i is the same for all configurations
            natom = psmc_cfgs(1)%mols(i)%natom
            if( psmc_cfgs(2)%mols(i)%natom /= natom ) correspond = .false. 
            if( cfgs(1)%mols(i)%natom /= natom ) correspond = .false.
            if( cfgs(2)%mols(i)%natom /= natom ) correspond = .false.

            if( .not. correspond ) then

                 call cry(uout, '', &
                     "ERROR: Corresponding non-moving molecules in CONFIG, CONFIG.1 & CONFIG.2 must have "// &
                     "the same number of atoms.", 51)
                  
            end if

       
            ! Check that if atom j moves then this is the case over all configurations
            do j = 1, natom
                   
                moveable = atom_can_move( job, psmc_cfgs(1)%mols(i)%atms(j) )
                if( atom_can_move( job, psmc_cfgs(2)%mols(i)%atms(j) ) .neqv. moveable ) correspond = .false. 
                if( atom_can_move( job, cfgs(1)%mols(i)%atms(j) ) .neqv. moveable ) correspond = .false. 
                if( atom_can_move( job, cfgs(2)%mols(i)%atms(j) ) .neqv. moveable ) correspond = .false.             
                   
                if( .not. correspond ) then

                    call cry(uout, '', &
                         "ERROR: Corresponding non-moving molecules in CONFIG, CONFIG.1 & CONFIG.2 must have "// &
                         "corresponding moving atoms.", 51)
                       
                end if

            end do

        end if

    end do

end subroutine configs_correspond




!> @brief
!> - For both boxes, determines whether or not each molecule is to be considered as a 'particle' for the 
!>   purposes of PSMC, or whether its constituent atoms are to be considered as particles, and assigns the
!>   'psmcparticle' flags accordingly. 
!> - Molecules which move via translational or rotational moves are to be considered as PSMC particles (elsewhere
!>   such particles are forbidden to have any of their constituent atoms earmarked to move during the simulation).
!> @using
!> - `control_type`
!> - `cell_module, only : cfgs`
subroutine set_psmcparticle(job)

    use control_type
    use cell_module, only : cfgs

    implicit none

        !> Simulation control variables
    type(control), intent(in) :: job

    integer :: i

    ! Assume that boxes 1 and 2 have 'corresponding structures' (which is guaranteed if DL_MONTE is still
    ! running after 'configs_correspond' is called)
    do i = 1, cfgs(1)%num_mols

        if( mol_can_move( job, cfgs(1)%mols(i) ) .or. mol_can_rot( job, cfgs(1)%mols(i) ) ) then

            cfgs(1)%mols(i)%psmcparticle = .true.
            cfgs(2)%mols(i)%psmcparticle = .true.

        else

            cfgs(1)%mols(i)%psmcparticle = .false.
            cfgs(2)%mols(i)%psmcparticle = .false.

        end if

    end do

end subroutine set_psmcparticle




!> @brief
!> - Returns a logical specifying whether or not the specified atom belongs to a species which is earmarked to be moved or
!>   not during the simulation.
!> @using
!> - `control_type`
!> - `atom_type`
!> - `species_module, only : eletype, element`
logical function atom_can_move(job, atm)

    use control_type
    use atom_type
    use species_module, only : eletype, element

    implicit none

        !> Simulation control variables
    type(control), intent(in) :: job

        !> Atom to check
    type(atom), intent(in) :: atm

    ! atm%atlabel is an integer label for the atom's species; element(atm%atlabel) is 
    ! the character label for the atom's species. If the latter matches any 'character' in job%atmmover then the 
    ! atom's species is to be moved.
    ! eletype(atm%atype) is the integer label for the type (e.g. "core", "metal") for the atom's species. 
    ! If this matches any integer in job%atmmover_type then the atom's type is to be moved.
    ! This is only applicable though if job%moveatm=.true., otherwise no atoms are moved during the simulation.

    atom_can_move = .false.

    if(job%moveatm) then

        !TU: BUG - This does not do what I thought it did; it checks for matches of element and type separately
        !TU: while element(i) and eletyp(i) should be checked together
        if( any( job%atmmover == element(atm%atlabel) ) .and. any( job%atmmover_type == eletype(atm%atype) ) ) &
             atom_can_move = .true.

    end if

end function atom_can_move




!> @brief
!> - Returns a logical specifying whether or not the specified molecule belongs to a species which is earmarked to be moved
!>   or not during the simulation via translation.
!> @using
!> - `control_type`
!> - `molecule_type`
!> - `species_module, only : uniq_mol`
logical function mol_can_move(job, mol)

    use control_type
    use molecule_type
    use species_module, only : uniq_mol

    implicit none

        !> Simulation control variables
    type(control), intent(in) :: job

        !> Molecule to check
    type(molecule), intent(in) :: mol

    ! mol%mol_label is an integer label for the molecule's species; uniq_mol(mol%mol_label) is 
    ! the character label for the molecule's species. If the latter matches any 'character' in 
    ! job%molmover then the molecule's species is to be moved.

    mol_can_move = .false.

    if(job%movemol) then

        if(  any( job%molmover == uniq_mol(mol%mol_label)%molname )  )  mol_can_move = .true.

    end if

end function mol_can_move




!> @brief
!> - Returns whether or not the specified molecule belongs to a species which is earmarked to be moved via rotation
!>   or not during the simulation.
!> @using
!> - `control_type`
!> - `molecule_type`
!> - `species_module, only : uniq_mol`
logical function mol_can_rot(job, mol)

    use control_type
    use molecule_type
    use species_module, only : uniq_mol

    implicit none

    type(control), intent(in) :: job
    type(molecule), intent(in) :: mol

    ! mol%mol_label is an integer label for the molecule's species; uniq_mol(mol%mol_label) is 
    ! the character label for the molecule's species. If the latter matches any 'character' in 
    ! job%molmover then the molecule's species is to be moved.

    mol_can_rot = .false.

    if(job%rotatemol) then

        if(  any( job%molroter == uniq_mol(mol%mol_label)%molname )  )  mol_can_rot = .true.

    end if

end function mol_can_rot




!> @brief
!> - Writes the specified configuration to the specified unit (in DL_POLY format)
!> - Code almost identical to write_config_dlmonte in cell_module.f90
!> @using
!> - `kinds_f90`
!> - `config_type`
!> - `species_module, only : set_species_type, element`
!> - `atom_module, only : write_atom_pos`
subroutine write_config_dlpoly_2(unit, cfg)

    use kinds_f90
    use config_type
    use species_module, only : set_species_type, element
    use atom_module, only : write_atom_pos

    implicit none

        !> unit to output to
    integer, intent(in) :: unit

        !> configuration to output details of
    type(config), intent(inout) :: cfg

    integer :: lb, i, j
    character :: tag*1

    ! lattice parameters
    write(unit,'(a80)') cfg%cfg_title

    write(unit,"('0',4x,'1')")
    
    write(unit,'(3f15.7)')(cfg%vec%latvector(1,i), i = 1,3)
    write(unit,'(3f15.7)')(cfg%vec%latvector(2,i), i = 1,3)
    write(unit,'(3f15.7)')(cfg%vec%latvector(3,i), i = 1,3)

    do i = 1, cfg%num_mols

        do j = 1, cfg%mols(i)%natom

            lb = cfg%mols(i)%atms(j)%atlabel
            tag = set_species_type(cfg%mols(i)%atms(j)%atype)

            write(unit, "(1x,a8,1x,a1)") element(lb), tag
            call write_atom_pos(cfg%mols(i)%atms(j), unit)
    
        enddo

    enddo

end subroutine write_config_dlpoly_2




!> @brief
!> - Prints PSMC-related information for the specified configuration, including the PSMC atomic or molecular (depending on 
!>   whether the atom or molecule is 'moving' or not) site positions and displacements.
!> @using
!> - `control_type`
!> - `config_type`
!> - `species_module, only : set_species_type, element`
subroutine write_config_psmc_geom(job, unit, cfg)

    use control_type
    use config_type
    use species_module, only : set_species_type, element

    implicit none

        !> Simulation control variables
    type (control), intent(in) :: job

        !> Unit to output to
    integer, intent(in) :: unit

        !> Configuration to output details of
    type(config), intent(in) :: cfg

    integer :: lb, i, j
    character :: tag*1
    logical :: canmove, canrot

    write(unit,*)
    write(unit,*) "( sitex, sitey, sitez, ux, uy, uz ) for atoms and molecules moving during simulation:"
    write(unit,*)
    write(unit,*) 


    do i = 1, cfg%num_mols

       canmove = mol_can_move(job, cfg%mols(i))
       canrot = mol_can_rot(job, cfg%mols(i))

       if( canmove .or. canrot ) then

           write(unit,*) "molecule '", trim(cfg%mols(i)%molname), "'"
           write(unit, '(3f15.7,4x,3f15.7)') cfg%mols(i)%psmcsite(1:3), cfg%mols(i)%psmcu(1:3)
           write(unit,*)

       else

           do j = 1, cfg%mols(i)%natom
 
               lb = cfg%mols(i)%atms(j)%atlabel
               tag = set_species_type(cfg%mols(i)%atms(j)%atype)
               canmove = atom_can_move(job, cfg%mols(i)%atms(j))
               
               if(canmove) then
                   
                   write(unit,*) "atom '", trim(element(lb)), " ",tag, "' in molecule '", trim(cfg%mols(i)%molname), "'"
                   write(unit, '(10x,3f15.7,4x,3f15.7)') cfg%mols(i)%atms(j)%psmcsite(1:3), cfg%mols(i)%atms(j)%psmcu(1:3)
                   write(unit,*)
                
                end if
          
           end do

        end if

    end do

end subroutine write_config_psmc_geom




!> @brief
!> - Output PSMC-related simulation data to unit 'upsmc'
!> - The output is, on a single line: the iteration number, the current phase (box), 
!>   the current FED state, the current order parameter, and the current FED bias 
!>   for the current state.
!> @using
!> - `kinds_f90`
!> - `constants_module, only : upsmc`
!> - `use comms_mpi_module, only : master`
subroutine psmc_data_output(iter, phase, state, param, bias)

    use kinds_f90
    use constants_module, only : upsmc
    use comms_mpi_module, only : master

    implicit none

        !> iteration number
    integer, intent(in) :: iter
    
        !> current PSMC phase / active simulation box
    integer, intent(in) :: phase

        !> current FED state
    integer, intent(in) :: state

        !> current FED order parameter
    real(kind=wp), intent(in) :: param

        !> current FED bias
    real(kind=wp), intent(in) :: bias

    ! Only masters open and write to files
    if( master ) then

        !TU: Add the the switch bias for phase 2 (which is the 'true' bias for phase
        !TU: 2 configurations here) if it is in play. If it is not in play then it
        !TU: takes its default value of 0, and hence there is no effect.
        if( phase == 2 ) then

            write(upsmc, '(3i10, 2e19.10)' ) iter, phase, state, param, bias + psmc%switchbias

        else

            write(upsmc, '(3i10, 2e19.10)' ) iter, phase, state, param, bias

        end if

    end if

end subroutine psmc_data_output




!> @brief
!> - Check that the PSMC displacements (minus the CoM drift of the system) 
!>   have not exceeded the threshold for phase 1 or
!>   phase 2, and output some statistics regarding the displacements to uout.
!> @using
!> - `kinds_f90`
!> - `control_type`
!> - `constants_module, only : uout`
!> - `cell_module, only : cfgs`
!> - `use comms_mpi_module, only : master`
subroutine psmc_melt_check(job, iter)

    use kinds_f90
    use control_type
    use constants_module, only : uout
    use cell_module, only : cfgs
    use comms_mpi_module, only : master

    implicit none

        !> Simulation control variables
    type(control), intent(in) :: job

        !> The current move iteration number
    integer, intent(in) :: iter

    integer :: i, j

    ! Number of active atoms in phase 1 (and 2)
    integer :: active

    ! Mean displacement vector over all active atoms in phase 1 and 2
    real(kind=wp), dimension(3) :: meanu1, meanu2

    ! Maximum displacement in any Cartesian direction for phase 1 and 2
    real(kind=wp) :: maxu1, maxu2

    active = 0
    maxu1 = 0.0_wp
    maxu2 = 0.0_wp
    meanu1 = 0.0_wp
    meanu2 = 0.0_wp


    ! Calculate the CoM shift in the system relative to its starting position
    ! This will be meanu1 (for phase 1) and meanu2 (for phase 2)

    
    ! Loop over molecules - the number should be the same for both boxes 1 and 2
    ! (see function 'configs_correspond')
    do i = 1, cfgs(1)%num_mols

        ! If the molecule can move (i.e., translational moves - I do not consider rotational moves here)
        ! then consider its displacement. If it cannot move only then consider atomic displacements
        ! within this molecule. (Moving atoms within moving molecules are forbidden in PSMC - it only
        ! currently treats rigid molecules).
        ! Note that if something moves in cfgs(1) then it must also do so in cfgs(2).
        if( mol_can_move( job, cfgs(1)%mols(i) ) ) then

            active = active + 1
                    
            meanu1 = meanu1 + cfgs(1)%mols(i)%psmcu(:)
            meanu2 = meanu2 + cfgs(2)%mols(i)%psmcu(:)

        else

            ! Loop over atoms - the number should be the same in corresponding molecules
            ! for both boxes 1 and 2 (see function 'configs_correspond')
            do j = 1, cfgs(1)%mols(i)%natom

                ! Consider the displacements only for moveable atoms - moveable atoms
                ! should correspond between boxes 1 and 2 - see function 'configs_correspond'
                if( atom_can_move(job, cfgs(1)%mols(i)%atms(j)) ) then
    
                    active = active + 1
                    
                    meanu1 = meanu1 + cfgs(1)%mols(i)%atms(j)%psmcu(:)
                    meanu2 = meanu2 + cfgs(2)%mols(i)%atms(j)%psmcu(:)
    
                end if
    
            end do

        end if

    end do

    meanu1 = meanu1 / active
    meanu2 = meanu2 / active



    ! Calculate the maximum displacement of a particle, over all particles in the system, 
    ! subtracting the CoM shift in the system


    ! Loop over molecules - the number should be the same for both boxes 1 and 2
    ! (see function 'configs_correspond')
    do i = 1, cfgs(1)%num_mols

        ! If the molecule can move (i.e., translational moves - I do not consider rotational moves here)
        ! then consider its displacement. If it cannot move only then consider atomic displacements
        ! within this molecule. (Moving atoms within moving molecules are forbidden in PSMC - it only
        ! currently treats rigid molecules).
        ! Note that if something moves in cfgs(1) then it must also do so in cfgs(2).
        if( mol_can_move( job, cfgs(1)%mols(i) ) ) then
    
            maxu1 = max( maxu1, abs( cfgs(1)%mols(i)%psmcu(1) - meanu1(1) ), &
                                abs( cfgs(1)%mols(i)%psmcu(2) - meanu1(2) ), & 
                                abs( cfgs(1)%mols(i)%psmcu(3) - meanu1(3) ))
            maxu2 = max( maxu2, abs( cfgs(2)%mols(i)%psmcu(1) - meanu2(1) ), &
                                abs( cfgs(2)%mols(i)%psmcu(2) - meanu2(2) ), & 
                                abs( cfgs(2)%mols(i)%psmcu(3) - meanu2(3) ))

        else

            ! Loop over atoms - the number should be the same in corresponding molecules
            ! for both boxes 1 and 2 (see function 'configs_correspond')
            do j = 1, cfgs(1)%mols(i)%natom

                ! Consider the displacements only for moveable atoms - moveable atoms
                ! should correspond between boxes 1 and 2 - see function 'configs_correspond'
                if( atom_can_move(job, cfgs(1)%mols(i)%atms(j)) ) then

                    maxu1 = max( maxu1, abs( cfgs(1)%mols(i)%atms(j)%psmcu(1) - meanu1(1) ), &
                                        abs( cfgs(1)%mols(i)%atms(j)%psmcu(2) - meanu1(2) ), & 
                                        abs( cfgs(1)%mols(i)%atms(j)%psmcu(3) - meanu1(3) ))
                    maxu2 = max( maxu2, abs( cfgs(2)%mols(i)%atms(j)%psmcu(1) - meanu2(1) ), &
                                        abs( cfgs(2)%mols(i)%atms(j)%psmcu(2) - meanu2(2) ), & 
                                        abs( cfgs(2)%mols(i)%atms(j)%psmcu(3) - meanu2(3) ))
    
                end if
    
            end do

        end if

    end do

    if( master ) then

        write(uout,*)
        write(uout,*)
        write(uout,*) "--------------------------------------------------------------------------------------"
        write(uout, "('                   PSMC melt check at iteration ',I10)" ) iter
        write(uout,*)
        write(uout, "(' Mean displacement phase 1  =',3(1X,F15.8))" ) meanu1
        write(uout, "(' Mean displacement phase 2  =',3(1X,F15.8))" ) meanu2
        write(uout,*)
        write(uout, "(' Max. Cartesian displacement (minus mean) phase 1 = ',F15.8)" ) maxu1
        write(uout, "(' Max. Cartesian displacement (minus mean) phase 2 = ',F15.8)" ) maxu2
        write(uout,*)
        write(uout,*) "--------------------------------------------------------------------------------------"
        write(uout,*)
        write(uout,*)

    end if

    ! Return an error if the threshold is exceeded
    if( maxu1 > psmc%melt_thresh .or. maxu2 > psmc%melt_thresh ) then
        
        call cry(uout,'', &
            "ERROR: A PSMC displacement has exceeded the threshold value: the system has 'melted'.", 360)

    end if

end subroutine psmc_melt_check




! Utility procedures




! Note: This procedure probably has redundancy with procedures for doing similar in 'cell_module'.
!       Moreover ideally this would be  rewritten to allow vectorisation, and given a more sensible name.
!
!> @brief
!> - Converts real coordinates within the speficied 'lattice vectors' to fractional coordinates
!> @using
!> - `kinds_f90` 
!> - `latticevectors_type`
subroutine real2frac(vec, rx, ry, rz)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> Lattice vectors
    type(latticevectors), intent(in) :: vec

        !> x-position of lattice site (real coordinates in, fractional coordinates out)
    real(kind=wp), intent(inout) :: rx

        !> y-position of lattice site (real coordinates in, fractional coordinates out)
    real(kind=wp), intent(inout) :: ry

        !> z-position of lattice site (real coordinates in, fractional coordinates out)
    real(kind=wp), intent(inout) :: rz

    ! Coordinates of position in real coordinates
    real(kind=wp) :: rxreal, ryreal, rzreal

    rxreal = rx
    ryreal = ry
    rzreal = rz

    rx = vec%invlat(1,1) * rxreal + vec%invlat(2,1) * ryreal + vec%invlat(3,1) * rzreal
    ry = vec%invlat(1,2) * rxreal + vec%invlat(2,2) * ryreal + vec%invlat(3,2) * rzreal
    rz = vec%invlat(1,3) * rxreal + vec%invlat(2,3) * ryreal + vec%invlat(3,3) * rzreal
    
end subroutine real2frac




! Note: This procedure probably has redundancy with procedures for doing similar in 'cell_module'.
!       Moreover ideally this would be  rewritten to allow vectorisation, and given a more sensible name.
!
!> @brief
!> - Converts fractional coordinates within the speficied 'lattice vectors' to real coordinates
!> @using
!> - `kinds_f90` 
!> - `latticevectors_type`
subroutine frac2real(vec, rx, ry, rz)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> Lattice vectors
    type(latticevectors), intent(in) :: vec

        !> x-position of lattice site (fractional coordinates in, real coordinates out)
    real(kind=wp), intent(inout) :: rx

        !> y-position of lattice site (fractional coordinates in, real coordinates out)
    real(kind=wp), intent(inout) :: ry

        !> z-position of lattice site (fractional coordinates in, real coordinates out)
    real(kind=wp), intent(inout) :: rz

    ! Coordinates of position in fractional coordinates
    real(kind=wp) :: rxfrac, ryfrac, rzfrac

    rxfrac = rx
    ryfrac = ry
    rzfrac = rz

    rx = vec%latvector(1,1) * rxfrac + vec%latvector(2,1) * ryfrac + vec%latvector(3,1) * rzfrac
    ry = vec%latvector(1,2) * rxfrac + vec%latvector(2,2) * ryfrac + vec%latvector(3,2) * rzfrac
    rz = vec%latvector(1,3) * rxfrac + vec%latvector(2,3) * ryfrac + vec%latvector(3,3) * rzfrac
    
end subroutine frac2real




!> @brief
!> - Converts the positions of all atoms in the specified configuration to real coordinates,
!>   assuming that they were initially fractional coordinates
!> @using
!> - `kinds_f90`
!> - `config_type`
subroutine frac2real_cfg(cfg)
  
    use kinds_f90
    use config_type

    implicit none

        !> Configuration
    type(config), intent(inout) :: cfg

    integer :: j, i
    
    do j = 1, cfg%num_mols

        do i = 1, cfg%mols(j)%natom

            call frac2real(cfg%vec, cfg%mols(j)%atms(i)%rpos(1), &
                                    cfg%mols(j)%atms(i)%rpos(2), &
                                    cfg%mols(j)%atms(i)%rpos(3) )

        end do

     end do

end subroutine frac2real_cfg




! Written for performing simulations in centre of mass (for the system) reference frame, but never used.
! I leave it here because it centre-of-mass simulations may be supported in the future.
!
! !> @brief
! !> - Performs a global shift on all atoms' positions in the specified box by the
! !>   specified vector
! !> @using
! !> - `kinds_f90`
! !> - `cell_module, only cfgs, move_atom_by`
! subroutine move_cfg_by(ib, shift)
!
!     use kinds_f90
!     use cell_module, only cfgs, move_atom_by
!
!     implicit none
!
!         !> Box number
!     integer :: ib
!
!         !> Shift in positions to perform
!     real(kind=wp), intent(in) :: shift(3)
!
!     integer :: j, i
!
!     logical :: atom_out
!
!     do j = 1, cfgs(ib)%num_mols
!
!         do i = 1, cfgs(ib)%mols(i)%natom
!
!            call move_atom_by(ib, j, i, shift, atom_out)
!           
!         end do
!
!     end do
!
! end subroutine move_cfg_by




! Actual PSMC procedures




!> @brief
!> - Sets the site position for the specified atom in the specified configuration relative to the 
!>   specified phase
!> - The site position reflects the fractional coordinates of the analogous atom in the 'ideal'
!>   configuration (read from CONFIG.1 or CONFIG.2) for the specified phase. 
!>   Specifically, the fractional position of the site will be the same as the fractional 
!>   position of the atom in the ideal configuration AFTER IT IS WRAPPED INTO THE PRIMARY
!>   IMAGE. To be clear, the atom in the ideal configuration is allowed to be outside the 
!>   primary image, but the site in the specified configuration will be within the primary
!>   image.
!> @using
!> - `kinds_f90`
!> - `use cell_module, only : cfgs, pbc_atom_pos_calc`
subroutine set_atom_site(ib, j, i, phase)

    use kinds_f90
    use cell_module, only : cfgs, pbc_atom_pos_calc

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Atom number in the molecule
    integer, intent(in) :: i

        !> Phase to measure displacements with respect to
    integer, intent(in) :: phase

        ! Position of site
    real(kind = wp) :: rx, ry, rz

    ! Get the position of the atom in the ideal configuration
    rx = psmc_cfgs(phase)%mols(j)%atms(i)%rpos(1)
    ry = psmc_cfgs(phase)%mols(j)%atms(i)%rpos(2)
    rz = psmc_cfgs(phase)%mols(j)%atms(i)%rpos(3)

    ! Convert it into fractional coordinates
    call real2frac(psmc_cfgs(phase)%vec, rx, ry, rz)

    ! Apply periodic boundary conditions to the fractional position
    call pbc_atom_pos_calc(rx, ry, rz)

    ! Convert this fractional position to Cartesian coordinates in the configuration we are considering
    call frac2real(cfgs(ib)%vec, rx, ry, rz)

    ! Set the site position for the atom
    cfgs(ib)%mols(j)%atms(i)%psmcsite(1) = rx
    cfgs(ib)%mols(j)%atms(i)%psmcsite(2) = ry
    cfgs(ib)%mols(j)%atms(i)%psmcsite(3) = rz
  
end subroutine set_atom_site




!> @brief
!> - Sets the site position for the specified molecule in the the specified configuration relative 
!>   to the specified phase
!> - The site position reflects the fractional coordinates of the (COM of the) analogous molecule 
!>   in the 'ideal' configuration (read from CONFIG.1 or CONFIG.2) for the specified phase.
!>   Specifically, the fractional position of the site will be the same as the fractional 
!>   position of the molecule in the ideal configuration AFTER IT IS WRAPPED INTO THE PRIMARY
!>   IMAGE. To be clear, the molecule in the ideal configuration is allowed to be outside the 
!>   primary image, but the site in the specified configuration will be within the primary
!>   image.
!> @using
!> - `kinds_f90`
!> - `use cell_module, only : cfgs, pbc_atom_pos_calc`
subroutine set_mol_site(ib, j, phase)

    use kinds_f90
    use cell_module, only : cfgs, pbc_atom_pos_calc

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Phase to measure displacements with respect to
    integer, intent(in) :: phase

        ! Position of site
    real(kind = wp) :: rx, ry, rz

    ! Get the position of the molecule in the ideal configuration
    rx = psmc_cfgs(phase)%mols(j)%rcom(1)
    ry = psmc_cfgs(phase)%mols(j)%rcom(2)
    rz = psmc_cfgs(phase)%mols(j)%rcom(3)

    ! Convert it into fractional coordinates
    call real2frac(psmc_cfgs(phase)%vec, rx, ry, rz)

    ! Apply periodic boundary conditions to the fractional position
    call pbc_atom_pos_calc(rx, ry, rz)

    ! Convert this fractional position to Cartesian coordinates in the configuration we are considering
    call frac2real(cfgs(ib)%vec, rx, ry, rz)

    ! Set the site position for the molecule
    cfgs(ib)%mols(j)%psmcsite(1) = rx
    cfgs(ib)%mols(j)%psmcsite(2) = ry
    cfgs(ib)%mols(j)%psmcsite(3) = rz

end subroutine set_mol_site





!> @brief
!> - Sets the site positions for atoms within the specified molecule within the specified configuration relative 
!>   to the specified phase
!> - Calls `set_atom_site` for all atoms in the specified molecule - see that procedure for further details.
!> @using
!> - `config_type`
!> - `use cell_module, only : cfgs`
subroutine set_atoms_site(ib, j, phase)

    use config_type
    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Phase to measure displacements with respect to
    integer, intent(in) :: phase

    integer :: i

    do i = 1, cfgs(ib)%mols(j)%natom

       call set_atom_site(ib, j, i, phase)

    end do

end subroutine set_atoms_site




!> @brief
!> - Sets the displacement of the specified atom in the specified configuration according to its current position 
!>   and lattice site position
!> - Applies periodic boundary conditions to the displacement: the displacement is the separation between the primary
!>   images of the site and the atom's position via the minimum image convention (except in the z-direction if slit
!>   geometry is in use)
!> @using
!> - `config_type`
!> - `use cell_module, only : cfgs, pbc_sepvec`
subroutine set_atom_u_from_pos(ib, j, i)

    use kinds_f90
    use cell_module, only : cfgs, pbc_sepvec
  
    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Atom number in the molecule
    integer, intent(in) :: i

    cfgs(ib)%mols(j)%atms(i)%psmcu = pbc_sepvec(ib, &
            cfgs(ib)%mols(j)%atms(i)%psmcsite, cfgs(ib)%mols(j)%atms(i)%rpos)

end subroutine set_atom_u_from_pos




!> @brief
!> - Sets the displacement of the specified molecule in the specified configuration according to its current position 
!>   and lattice site position
!> - Applies periodic boundary conditions to the displacement: the displacement is the separation between the primary
!>   images of the site and the molicule's position (COM) via the minimum image convention (except in the z-direction if slit
!>   geometry is in use)
!> @using
!> - `kinds_f90`
!> - `use cell_module, only : cfgs, pbc_sepvec`
subroutine set_mol_u_from_pos(ib, j)

    use kinds_f90
    use cell_module, only : cfgs, pbc_sepvec
  
    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

    cfgs(ib)%mols(j)%psmcu = pbc_sepvec(ib, &
            cfgs(ib)%mols(j)%psmcsite, cfgs(ib)%mols(j)%rcom)

end subroutine set_mol_u_from_pos




!> @brief
!> - Sets the displacements of the atoms in the specified molecule in the specified configuration according to their
!>  current position and lattice site positions
!> - Calls `set_atom_u_from_pos` for all atoms in the specified molecule - see that procedure for further details.
!> @using
!> - `use cell_module, only : cfgs`
subroutine set_atoms_u_from_pos(ib, j)

    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j


    integer :: i

    do i = 1, cfgs(ib)%mols(j)%natom

       call set_atom_u_from_pos(ib, j, i)

    end do

end subroutine set_atoms_u_from_pos




!> @brief
!> - Sets the position of the specified atom in the specified configuration according to its current displacement 
!>   vector and lattice site position
!> - This procedure moves the atom so that its position corresponds to the closest image (relative to its previous
!>   position) of the vector sum of its site plus its displacement. THIS POSITION MAY NOT BE THE PRIMARY IMAGE.
!> @using
!> - `kinds_f90`
!> - `use cell_module, only : cfgs, move_atom_by, pbc_atom_pos_calc`
!> - `use constants_module, only : uout`
subroutine set_atom_pos_from_u(ib, j, i, atom_out)

    use kinds_f90
    use cell_module, only : cfgs, move_atom_by, pbc_atom_pos_calc
    use constants_module, only : uout
  
    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Atom number in the molecule
    integer, intent(in) :: i

        !> Flag which is .true. if the atom's new position is outwith the slit - if slit geometry is in use
    logical, intent(out) :: atom_out

    real(kind=wp) :: rshift(3)

    ! Calculate the vector required to add to the current position to yield the desired
    ! position, i.e., the vector sum of the site plus the displacement. Note that
    ! 'psmcsite' and 'psmcu' should be in the primary image (the procedures which set them guarantee this), 
    ! but 'rpos' may not be, in which case 'rshift' evaluated by the following line could be such that
    ! 'rshift' is large
    rshift = cfgs(ib)%mols(j)%atms(i)%psmcsite + cfgs(ib)%mols(j)%atms(i)%psmcu &
           - cfgs(ib)%mols(j)%atms(i)%rpos

    ! Convert 'rshift' into fractional coordinates
    call real2frac(cfgs(ib)%vec, rshift(1), rshift(2), rshift(3))

    ! Apply periodic boundary conditions to the fractional vector - to get the shortest vector taking
    ! the position of the atom to ANY image of the desired position
    call pbc_atom_pos_calc(rshift(1), rshift(2), rshift(3))

    ! Convert this fractional position to Cartesian coordinates in the configuration we are considering
    call frac2real(cfgs(ib)%vec, rshift(1), rshift(2), rshift(3))

    ! Move the atom by 'rshift'
    call move_atom_by(ib, j, i, rshift, atom_out)

end subroutine set_atom_pos_from_u




!> @brief
!> - Sets the position of the specified molecule in the specified configuration according to its current displacement 
!>   vector and lattice site position
!> - This procedure moves the molecule so that its (COM) position corresponds to the closest image (relative to its previous
!>   position) of the vector sum of its site plus its displacement. THIS POSITION MAY NOT BE THE PRIMARY IMAGE.
!> @using
!> - `kinds_f90`
!> - `use cell_module, only : cfgs, move_atom_by, pbc_atom_pos_calc`
!> - `use constants_module, only : uout`
subroutine set_mol_pos_from_u(ib, j, atom_out)

    use kinds_f90
    use cell_module, only : cfgs, move_molecule_by, pbc_atom_pos_calc
    use constants_module, only : uout
  
    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Flag which is .true. if an atom's new position is outwith the slit - if slit geometry is in use
    logical, intent(out) :: atom_out


    real(kind=wp) :: rshift(3)

    ! Calculate the vector required to add to the current position to yield the desired
    ! position, i.e., the vector sum of the site plus the displacement. Note that
    ! 'psmcsite' and 'psmcu' should be in the primary image (the procedures which set them guarantee this), 
    ! but 'rcom' may not be, in which case 'rshift' evaluated by the following line could be such that
    ! 'rshift' is large
    rshift = cfgs(ib)%mols(j)%psmcsite + cfgs(ib)%mols(j)%psmcu &
           - cfgs(ib)%mols(j)%rcom
    
    ! Convert 'rshift' into fractional coordinates
    call real2frac(cfgs(ib)%vec, rshift(1), rshift(2), rshift(3))

    ! Apply periodic boundary conditions to the fractional vector - to get the shortest vector taking
    ! the position of the molecule to ANY image of the desired position
    call pbc_atom_pos_calc(rshift(1), rshift(2), rshift(3))

    ! Convert this fractional position to Cartesian coordinates in the configuration we are considering
    call frac2real(cfgs(ib)%vec, rshift(1), rshift(2), rshift(3))

    ! Move the molecule by 'rshift'
    call move_molecule_by(ib, j, rshift, atom_out)


end subroutine set_mol_pos_from_u




!> @brief
!> - Sets the positions of the atoms in the specified molecule in the specified configuration according to their
!>  current displacement vectors and lattice site positions
!> @using
!> - `use cell_module, only : cfgs`
subroutine set_atoms_pos_from_u(ib, j, atom_out)

    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Molecule number in the configuration
    integer, intent(in) :: j

        !> Flag which is .true. if an atom's new position is outwith the slit - if slit geometry is in use
    logical, intent(out) :: atom_out

    logical :: ao

    integer :: i

    atom_out = .false.

    do i = 1, cfgs(ib)%mols(j)%natom

       call set_atom_pos_from_u(ib, j, i, ao)

       atom_out = ( atom_out .or. ao )

    end do

end subroutine set_atoms_pos_from_u




!> @brief
!> - Transfers the displacement of the specified atom in configuration 'ib' to the analogous atom in the 
!>   conjugate configuration 'ib_conj'
!> - The displacement in 'ib_conj' reflects the fractional displacement of the atom in 'ib', i.e., the fractional
!>   displacements of the atoms in both configurations are identical, unless the `cart_disp_switch` flag is
!>   active in which case the Cartesian displacement of the atom in 'in_conj' will reflect that of 'ib'
!> - Periodic boundary conditions are not applied in this procedure: it should already be the case that the fractional
!>   displacement is within the primary image.
!> @using
!> - `use cell_module, only : cfgs`
subroutine translate_atom_u(ib, ib_conj, j, i)

    use cell_module, only : cfgs

    implicit none

        !> Configuration which will be used as the basis of the transfer
    integer, intent(in) :: ib

        !> Configuration which will have a new displacement for the specified atom after this procedure
    integer, intent(in) :: ib_conj

        !> Molecule number
    integer, intent(in) :: j

        !> Atom number in the molecule
    integer, intent(in) :: i


        ! Atom displacement
    real(kind=wp) :: ux, uy, uz


    ! Calculate the atom's displacement in cfg in fractional coordinates
    ux = cfgs(ib)%mols(j)%atms(i)%psmcu(1)
    uy = cfgs(ib)%mols(j)%atms(i)%psmcu(2)
    uz = cfgs(ib)%mols(j)%atms(i)%psmcu(3)

    if( .not. psmc%cart_disp_switch ) then

        call real2frac(cfgs(ib)%vec, ux, uy, uz)

        ! Convert the atom's displacement into real coordinates in cfg_conj
        call frac2real(cfgs(ib_conj)%vec, ux, uy, uz)

    end if

    ! Assign the displacement to the atom in cfg_conj
    cfgs(ib_conj)%mols(j)%atms(i)%psmcu(1) = ux
    cfgs(ib_conj)%mols(j)%atms(i)%psmcu(2) = uy
    cfgs(ib_conj)%mols(j)%atms(i)%psmcu(3) = uz

end subroutine translate_atom_u




!> @brief
!> - Transfers the displacement of the specified molecule in configuration 'ib' to the analogous molecule 
!>   in the conjugate configuration 'ib_conj'
!> - The displacement in 'ib_conj' reflects the fractional displacement of the molecule in 'ib', i.e., the fractional
!>   displacements of the molecules in both configurations are identical, unless the `cart_disp_switch` flag is
!>   active in which case the Cartesian displacement of the molecule in 'in_conj' will reflect that of 'ib'
!> - Periodic boundary conditions are not applied in this procedure: it should already be the case that the fractional
!>   displacement is within the primary image.
!> @using
!> - `use cell_module, only : cfgs`
subroutine translate_mol_u(ib, ib_conj, j)

    use cell_module, only : cfgs

    implicit none

        !> Configuration which will be used as the basis of the transfer
    integer, intent(in) :: ib

        !> Configuration which will have a new displacement for the specified atom after this procedure
    integer, intent(in) :: ib_conj

        !> Molecule number
    integer, intent(in) :: j


        ! Molecule displacement
    real(kind=wp) :: ux, uy, uz


    ! Calculate the mol's displacement in cfg in fractional coordinates
    ux = cfgs(ib)%mols(j)%psmcu(1)
    uy = cfgs(ib)%mols(j)%psmcu(2)
    uz = cfgs(ib)%mols(j)%psmcu(3)

    if( .not. psmc%cart_disp_switch ) then

        call real2frac(cfgs(ib)%vec, ux, uy, uz)

        ! Convert the mol's displacement into real coordinates in cfg_conj
        call frac2real(cfgs(ib_conj)%vec, ux, uy, uz)

    end if

    ! Assign the displacement to the mol in cfg_conj
    cfgs(ib_conj)%mols(j)%psmcu(1) = ux
    cfgs(ib_conj)%mols(j)%psmcu(2) = uy
    cfgs(ib_conj)%mols(j)%psmcu(3) = uz

end subroutine translate_mol_u




!> @brief
!> - Transfers the displacements of the atoms in the specified molecule in configuration 'ib' to the analogous atoms in the 
!>   conjugate configuration 'ib_conj'
!> - Calls `translate_atom_u` for all atoms in the specified molecule - see that procedure for more details.
!> @using
!> - `use cell_module, only : cfgs`
subroutine translate_atoms_u(ib, ib_conj, j)

    use cell_module, only : cfgs

    implicit none

        !> Configuration which will be used as the basis of the transfer
    integer, intent(in) :: ib

        !> Configuration which will have new atom displacements for the specified molecule after this procedure
    integer, intent(in) :: ib_conj

        !> Molecule number
    integer, intent(in) :: j


    integer :: i

    do i = 1, cfgs(ib)%mols(j)%natom

       call translate_atom_u(ib, ib_conj, j, i)

    end do

end subroutine translate_atoms_u




!> @brief
!> - Transfers the displacements of all particles (atoms/molecules) in configuration 'ib' to the analogous ones
!>   in the conjugate configuration 'ib_conj'
!> - Each molecule is treated either as a 'PSMC particle' itself, or as a container for atoms which are themselves
!>   PSMC particles, depending on the molecule's 'psmcparticle' flag. 
!> @using
!> - `use cell_module, only : cfgs`
subroutine translate_cfg_u(ib, ib_conj)

    use cell_module, only : cfgs

    implicit none

        !> Configuration which will be used as the basis of the transfer
    integer, intent(in) :: ib

        !> Configuration which will have a new displacement for the specified atom after this procedure
    integer, intent(in) :: ib_conj


    integer :: j

    do j = 1, cfgs(ib)%num_mols

        if(cfgs(ib)%mols(j)%psmcparticle) then

            call translate_mol_u(ib, ib_conj, j)

        else

            call translate_atoms_u(ib, ib_conj, j)

        end if

    end do

end subroutine translate_cfg_u




!> @brief
!> - Sets the positions for all particles (atoms/molecules) within the specified configuration according to their
!>   current displacements and sites
!> - Each molecule is treated either as a 'PSMC particle' itself, or as a container for atoms which are themselves
!>   PSMC particles, depending on the molecule's 'psmcparticle' flag.
!> - See `set_mol_pos_from_u` and `set_atoms_pos_from_u` for further details
!> @using
!> - `use cell_module, only : cfgs`
subroutine set_cfg_pos_from_u(ib, atom_out)

    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Flag which is .true. if an atom's new position is outwith the slit - if slit geometry is in use
    logical, intent(out) :: atom_out

    integer :: j

    logical :: ao

    atom_out = .false.

    do j = 1, cfgs(ib)%num_mols

        if(cfgs(ib)%mols(j)%psmcparticle) then

            call set_mol_pos_from_u(ib, j, ao)

            atom_out = ( atom_out .or. ao )

        else

            call set_atoms_pos_from_u(ib, j, ao)

            atom_out = ( atom_out .or. ao )

        end if

    end do

end subroutine set_cfg_pos_from_u




!> @brief
!> - Sets the site variables for all particles (atoms/molecules) within the specified configuration relative to
!>   the specified phase
!> - Each molecule is treated either as a 'PSMC particle' itself, or as a container for atoms which are themselves
!>   PSMC particles, depending on the molecule's 'psmcparticle' flag.
!> - See `set_mol_site` and `set_atoms_site` for further details
!> @using
!> - `use cell_module, only : cfgs`
subroutine set_cfg_sites(ib, phase)

    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

        !> Phase to measure displacements with respect to
    integer, intent(in) :: phase

    integer :: j

    do j = 1, cfgs(ib)%num_mols

        if(cfgs(ib)%mols(j)%psmcparticle) then

            call set_mol_site(ib, j, phase)

        else

            call set_atoms_site(ib, j, phase)

        end if

    end do

end subroutine set_cfg_sites




!> @brief
!> _ Sets the sites, displacements and cell dimensions of configuration 'ib_conj'
!>   to be the 'conjugate' of configuration 'ib', where by 'conjugate' we mean that the 
!>   the geometry of 'ib_conj' which results if one peforms a phase switch from 'ib'. 
!> - Note that the molecular orientations are NOT altered by this procedure.
subroutine set_passive_cfg(ib, ib_conj, ib_phase, atom_out)

    use kinds_f90
    use constants_module, only : uout
    use latticevectors_module, only : setorthovec, invertlatvec, cellsize, setrcpvec
    use cell_module, only : cfgs, pbc_simulation_cell, set_rigid_mol_coms

    implicit none

        !> Main configuration
    integer, intent(in) :: ib
    
        !> The configuration whose atom displacements and cell volume will be set to be the
        !> conjugate of cfg
    integer, intent(in) :: ib_conj

        !> The phase which 'ib' corresponds to (1 or 2); 'ib_conj' is assumed to be the 'other' phase
    integer, intent(in) :: ib_phase

        !> Flag which is .true. if an atom's new position is outwith the slit - if slit geometry is in use
    logical, intent(out) :: atom_out
    
    
        ! The phase for cfg_conj
    integer :: ib_conj_phase

        ! Dummy argument for 'cellsize' procedure - not used for anything
    real(kind=wp) :: vol

    integer :: i, j

    ! Set ib_conj_phase
    select case(ib_phase)
    case(1)

       ib_conj_phase = 2

    case(2)

       ib_conj_phase = 1

    case default

       call cry(uout, '', "ERROR: 'conj_phase' is not 1 or 2 in 'set_passive_cfg'", 999)
       stop

    end select

    ! Unwrap all atomic positions in 'ib_conj' before amending the molecular and
    ! atomic positions. 'set_rigid_mol_coms' does this, as well as recalculate
    ! the COMs of all 'rigid_body' molecules. If atomic positions are 'wrapped'
    ! within rigid molecules which are to be treated using PSMC, then a molecule 
    ! with a bond which straddles a periodic boundary will have that bond
    ! stretched/compressed if the cell expands/contracts - which is not
    ! desirable.
    call set_rigid_mol_coms(ib_conj) 

    ! Amend the lattice vectors of cfg_conj
    do i = 1, 3

       select case(ib_phase)
       case(1)

          cfgs(ib_conj)%vec%latvector(i,:) = cell_switch_12(i,1) * cfgs(ib)%vec%latvector(1,:) &
                                      + cell_switch_12(i,2) * cfgs(ib)%vec%latvector(2,:) &
                                      + cell_switch_12(i,3) * cfgs(ib)%vec%latvector(3,:)
       case(2)

          cfgs(ib_conj)%vec%latvector(i,:) = cell_switch_21(i,1) * cfgs(ib)%vec%latvector(1,:) &
                                      + cell_switch_21(i,2) * cfgs(ib)%vec%latvector(2,:) &
                                      + cell_switch_21(i,3) * cfgs(ib)%vec%latvector(3,:)
       end select

    end do

    ! Update geometric variables which depend on the lattice vectors

    ! Set the inverse lattice vectors in ib_conj
    call invertlatvec(cfgs(ib_conj)%vec)
    ! Set the cell volume
    call cellsize(cfgs(ib_conj)%vec, vol)
    ! Set the reciprocal lattice vectors
    call setrcpvec(cfgs(ib_conj)%vec)

    if(cfgs(ib_conj)%vec%is_orthogonal) then

        call setorthovec(cfgs(ib_conj)%vec)

    end if
    ! Update the sites in ib_conj
    call set_cfg_sites(ib_conj, ib_conj_phase)

    ! Translate the displacements from ib to ib_conj
    call translate_cfg_u(ib, ib_conj)

    ! Update the positions in ib_conj
    call set_cfg_pos_from_u(ib_conj, atom_out)


    !TU: James wants all atoms to take their primary image positions if the 'orthogonal' flag is used...

    !JG:
    ! if is_orthogonal, which it has to be then need to correct for PIC
    ! first need to set half lattice vectors
    ! then check PIC on all atoms - simulation

    if(cfgs(ib_conj)%vec%is_orthogonal) then

        call pbc_simulation_cell(ib_conj_phase)

    end if

end subroutine set_passive_cfg




!> @brief
!> - Sets the displacements for all particles (atoms/molecules) within the specified configuration according to their
!>  current positions and sites
!> @using
!> - `use cell_module, only : cfgs`
subroutine set_cfg_u_from_pos(ib)

    use cell_module, only : cfgs

    implicit none

        !> Configuration number
    integer, intent(in) :: ib

    integer :: j

    do j = 1, cfgs(ib)%num_mols

        if(cfgs(ib)%mols(j)%psmcparticle) then

            call set_mol_u_from_pos(ib, j)

        else

            call set_atoms_u_from_pos(ib, j)

        end if

    end do

end subroutine set_cfg_u_from_pos


end module psmc_module
