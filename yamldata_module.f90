!***************************************************************************
!   Copyright (C) 2016 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> Module containing procedures for outputting simulation data in YAML format
!> to a files with names such as YAMLDATA.???, YAMLDATA-1.??? and YAMLDATA-2.???.
!> Note that this is a high-level module, using variables in many other modules
!> of DL_MONTE. The output corresponds to a list (the data structure) of
!> 'frames', where each frame contains data pertaining to a specific timestep
!> during the simulation. The frame itself is a list containing two elements:
!> the timestep number; and an associative array (dictionary) containing the
!> data describing the system at that timestep, e.g., energies, the system's volume.
!> Usually the 'system' is one simulation box. In this case all output data
!> pertains to that box, and the output file is of the form YAMLDATA.???.
!> For Gibbs and phase-switch Monte Carlo simulations two boxes are used. For
!> Gibbs simulations the data pertaining to box 1 is output to YAMLDATA-1.??? while
!> the data pertaining to box 2 is output to YAMLDATA-2.???. For PSMC the box
!> whose data is output is taken to be the currently 'active' box, and the data
!> is output to YAMLDATA.???. Thus for PSMC data pertaining to the 'passive' box
!> is not output. (This is done for the sake of simplicity, bearing in mind
!> that the PSMC passive box does not contain thermodynamically useful data - it
!> is simply a 'workspace' for calculating the PSMC order parameter). Note that
!> all of the above is actually taken care of in 'montecarlo_module.f90', not here.
!>
!> The file YAMLDATA.??? consists of 2 YAML documents (separated by '---'). The
!> first document is metadata which defines the ensemble. The temperature is
!> always specified (keyword 'temperature' followed by its value). All other 
!> keywords are optional, and imply that the ensemble is NOT NVT in some regard:
!> - The presence of the keyword 'pressure', followed by its value, indicates that 
!>   the simulation is performed at constant pressure (i.e., volume moves are used).
!>   In this case expect 'volume' every frame in the data (see below).
!> - The presence of the keyword 'ensembleatoms' indicates that one or more atomic
!>   species is subject to insertion/rejection moves (grand-canonical Monte Carlo only -
!>   the semi-grand ensemble is not supported yet). A list branches from this keyword
!>   specifying whether each atomic species has a fixed number of particles (i.e., is not
!>   subject to insertions/deletions), or if it is subject to insertions/deletions. The
!>   nth element in the list pertains to the nth atomic species. If the species has a fixed
!>   number of particles then the corresponding element in the list is 'fixedn'; if it
!>   is subject to insertions/deletions then the element in the list is 'activity'
!>   followed by the value of the thermodynamic activity for the species - where the
!>   thermodynamic activity is exp(mu/(k_B*T)) (and is a dimensionless quantity).
!>   If 'ensembleatoms' is specified expect 'natom' every frame in the data.
!> - The presence of the keyword 'ensemblemols' indicates that one or more molecular
!>   species is subject to insertion/rejection moves (grand-canonical Monte Carlo only -
!>   the semi-grand ensemble is not supported yet). A list branches from this keyword
!>   with similar significance as described above for 'ensembleatoms'.
!>   If 'ensemblemols' is specified expect 'nmols' every frame in the data.
!> - The presence of the keyword 'usingbias' implies that the simulation was performed
!>   using a multicanonical ensemble. In this case expect 'bias' every frame in the
!>   data.
!> - The presence of the keyword 'usingbias' implies that the simulation was performed
!>   using a multicanonical ensemble. In this case expect 'bias' every frame in the
!>   data.
!> - The presence of the keyword 'usingpsmc' implies that the simulation was performed
!>   using phase-switch Monte Carlo. In this case expect 'psmcactive' every frame in the
!>   data.
!> - The presence of the keyword 'gibbsatoms' implies that this YAMLDATA file pertains
!>   to a Gibbs ensemble simulation where atom transfers occur between boxes
!> - The presence of the keyword 'gibbsmols' implies that this YAMLDATA file pertains
!>   to a Gibbs ensemble simulation where molecule transfers occur between boxes
!>
!> The second document in YAMLDATA.??? is the actual data. It is a list where each
!> element is a 'frame' of data corresponding to a specific timestep of the simulation.
!> Within each frame the total energy is always specified (keyword 'energy' followed by
!> its value), as is the timestep (keyword 'timestamp' followed by its value). All other
!> keywords are optional, and are output only if pertinent or if they are requested.
!> These keywords are as follows:
!> - 'bias' is the bias corresponding to this state (used in multicanonical simulations).
!>   Denoting the quantity specified as x, exp(x) gives the weight one should accord this
!>   frame in calculating thermodynamic quantities (for non-multicanonical simulations
!>   equal weight is given to all frames).  
!> - 'orderparam' is the order parameter
!> - 'psmcactive' is the current phase of the system (1 or 2) (for phase-switch Monte 
!>    Carlo)
!> - 'enthalpy' is the enthalpy (for constant pressure simulations)
!> - 'volume' is the volume (for constant pressure simulations)
!> - 'natom' is a list containing the number of atoms belonging to each atomic species:
!>   element n in the list corresponds to the nth species
!> - 'nmol' is a list containing the number of atoms belonging to each molecular 
!>   species: element n in the list corresponds to the nth species
!> - 'energyrcp' is the reciprocal component of the Coulomb energy
!> - 'energyreal' is the real component of the Coulomb energy
!> - 'energyvdw' is the Van der Waals energy
!> - 'energythree' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energypair' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energyang' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energyfour' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energymany' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energyext' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'energymfa' is [TO BE DEDUCED - MIMICS ANALOGOUS QUANTITY IN 'PTFILE']
!> - 'scalarop2' is the scalar order parameter, 2nd Legendre polynomial (for
!>   atomic orientations)
!> - 'scalarop4' is the scalar order parameter, 4th Legendre polynomial (for
!>   atomic orientations)
!> - 'orthodims' are the cell dimensions (x, y and z) of the orthorhombic
!>   system (for anisotropic NPT simulations in orthorhombic cells)
!>
!> Details of how to use Python to import and manipulate the metadata and data in 
!> YAMLDATA.??? will be provided elsewhere.
module yamldata_module
  
    use kinds_f90
    
    implicit none


    ! Parameters determining the nature of the YAML output


        !> Format string corresponding to an indent width in the YAML output
    character(*), parameter :: indentfmt = "(2X)"

        !> Format string for YAML 'keywords'. This should be large enough to support
        !> the largest keyword: enough to support the string "- X:", where 'X' is the
        !> largest keyword
    character(*), parameter :: kfmt = "(a)"
  
        !> Format string for reals in the YAML output
    character(*), parameter :: rfmt = "(es21.14)"

        !> Format string for integers in the YAML output
    character(*), parameter :: ifmt = "(i21)"

    ! Flags to control what is output each frame
        
    logical :: output_boxindex = .false.      ! Labeled 'box:' in the YAML output file (not 'boxindex:')
    
    logical :: output_energytot   = .false.   ! Labeled 'energy:' in the YAML output file (not 'energytot:').
    logical :: output_enthalpytot = .false.   ! Labeled 'enthalpy:' in the YAML output file (not 'enthalpyot:').
    logical :: output_energyrcp  = .false.
    logical :: output_energyreal  = .false.
    logical :: output_energyvdw   = .false.
    logical :: output_energythree = .false.
    logical :: output_energypair  = .false.
    logical :: output_energyang   = .false.
    logical :: output_energyfour  = .false.
    logical :: output_energymany  = .false.
    logical :: output_energyext   = .false.
    logical :: output_energymfa   = .false.
    logical :: output_virialtot   = .false.     ! Never currently set to .true. anywhere
    logical :: output_volume      = .false.
    
    logical :: output_natom       = .false.     ! Number of atoms belonging to each element
    logical :: output_nmol        = .false.     ! Number of molecules belonging to each species
    
    logical :: output_bias        = .false.     ! FED bias for current microstate
    logical :: output_orderparam  = .false.     ! FED order parameter for current microstate
    logical :: output_psmcactive  = .false.     ! Active box/phase number for PSMC
    
    ! Nematic scalar order parameter, 2nd Legendre polynomial (for atomic orientations)
    logical :: output_scalarop2   = .false.
    ! Nematic scalar order parameter, 4th Legendre polynomial (for atomic orientations)
    logical :: output_scalarop4   = .false.

    ! Cell x, y and z dimensions - for anisotropic orthorhombic NPT
    logical :: output_orthodims   = .false.

    ! Hamiltonian coupling paramter lambda - for lambda moves
    logical :: output_lambda = .false.    
    
contains


    !> Set output flags depending on the nature of the simulation
    subroutine yaml_set_output_flags(job)

        use control_type
        use constants_module, only : uout, FED_PS, NPT_GIBBS, NPT_ORTHOANI
        use vdw_module, only : ntpvdw
        use thbpotential_module, only : numthb
        use bond_module, only : ntpbond
        use angle_module, only : numang
        use inversion_module, only : numinv
        use dihedral_module, only : numdih
        use tersoff_module, only : ntersoff
        use metpot_module, only : nmetpot
        use external_potential_module, only : nextpot
        use fed_calculus_module, only : fed
        use slit_module, only : mfa_type

            !> Simulation control variables
        type(control), intent(in) :: job


        ! Select energy types to output

        output_energytot = .true.

        if( any(job%coultype > 0) ) then

           output_energyrcp = .true.
           output_energyreal = .true.

        end if

        if( ntpvdw > 0 ) output_energyvdw = .true.

        if( numthb > 0 ) output_energythree = .true.

        if( ntpbond > 0 ) output_energypair = .true.

        if( numang > 0 ) output_energyang = .true.
        
        if( numdih > 0 .or. numinv > 0 ) output_energyfour = .true.
        
        if( ntersoff > 0 .or. nmetpot > 0 ) output_energymany = .true.

        if( nextpot > 0 ) output_energyext = .true. 

        if( mfa_type /= 0 ) output_energymfa = .true. 

        
        ! For NPT ensemble or Gibbs ensemble with volume exchanges

        if( job%type_vol_move == NPT_GIBBS) then

            output_volume = .true.

        else if( job%type_vol_move /= 0 ) then

           output_enthalpytot = .true.
           output_volume = .true.

        end if

        if( job%type_vol_move == NPT_ORTHOANI) then

            output_orthodims = .true.
            
        end if
            
        ! For atomic semi-grand, Gibbs and grand-canonical ensembles output the number of atoms for each element

        if( job%semigrandatms .or. job%gcmcatom .or. job%gibbsatomtran ) output_natom = .true.

        ! ... but semi-grand is not supported yet
        if( job%semigrandatms ) call cry(uout,'', &
                    "WARNING: Semi-grand ensemble is not yet fully supported in YAML output.", 0)


        ! For molecular semi-grand, Gibbs and grand-canonical ensembles output the number of molecules for each species

        if( job%semigrandmols .or. job%gcmcmol .or. job%gibbsmoltran .or. job%lambdainsertmol ) output_nmol = .true.

        ! ... but semi-grand is not supported yet
        if( job%semigrandmols ) call cry(uout,'', &
                    "WARNING: Semi-grand ensemble is not yet fully supported in YAML output.", 0)


        ! For lambda moves
        
        if( job%lambdamoves .or. job%lambdainsertmol ) output_lambda = .true.


        ! For FED calculations, including PSMC

        if( job%fedcalc ) then

            output_bias = .true.
            output_orderparam = .true.

            if( fed%flavor == FED_PS ) output_psmcactive = .true.

        end if


        ! Flags reflecting those in 'job'

        output_scalarop2 = job % yamldata_output_scalarop2
        output_scalarop4 = job % yamldata_output_scalarop4
        
    end subroutine yaml_set_output_flags




    !> Output simulation metadata to the specified unit.
    subroutine yaml_write_metadata( unit, job )

        use control_type
        use fed_calculus_module, only : fed
        use constants_module, only : FED_PS, NPT_GIBBS
        use species_module, only : number_of_elements, number_of_molecules

            !> Unit to output frame to
        integer, intent(in) :: unit
        
            !> Simulation control variables
        type(control), intent(in) :: job

        logical :: isgcmc
        
        real(wp) :: activity

        integer :: typ


        write(unit,"("//kfmt//","//rfmt//")") "temperature:      ", job%systemp

        if( job%type_vol_move /= 0 .and. job%type_vol_move /= NPT_GIBBS ) then

            write(unit,"("//kfmt//","//rfmt//")") "pressure:         ", job%syspres

        end if

        if( job%gibbsatomtran ) then

           write(unit,"("//kfmt//","//rfmt//")")  "gibbsatoms:       "

           ! TO DO: LIST SPECIES IN SWAPS? IS IT NECESSARY?

        end if

        if( job%gibbsmoltran ) then

           write(unit,"("//kfmt//","//rfmt//")")  "gibbsmols:       "


           ! TO DO: LIST SPECIES IN SWAPS? IS IT NECESSARY?

        end if

        if(job%semigrandatms .or. job%gcmcatom) then

            write(unit, '(a)') "ensembleatoms:    "

            ! Cycle over atomic species to determine whether they are inserted/deleted, or 'fixed', and output accordingly
            do typ = 1, number_of_elements

                call atom_spec_gcmc_info(job, typ, isgcmc, activity)
                
                write(unit,"("//indentfmt//"a)") "-"

                if(isgcmc) then

                    write(unit,"(2"//indentfmt//","//kfmt//","//rfmt//")") "activity:         ", activity

                else
                    
                    write(unit,"(2"//indentfmt//","//kfmt//")") "fixedn:           "

                end if
                
            end do

        end if

        if(job%semigrandmols .or. job%gcmcmol) then

            write(unit, '(a)') "ensemblemols:    "

            ! Cycle over molecular species to determine whether they are inserted/deleted, or 'fixed', and output accordingly
            do typ = 1, number_of_molecules
                
                call mol_spec_gcmc_info(job, typ, isgcmc, activity)


                if(isgcmc) then
                    
                    write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") "- activity:         ", activity

                else
                    
                    write(unit,"("//indentfmt//","//kfmt//")") "- fixedn:           "

                end if
                
            end do

        end if
        
        if( job%fedcalc ) then

            write(unit,"("//kfmt//")") "usingbias:       "

            if( fed%flavor == FED_PS ) write(unit,"("//kfmt//")") "usingpsmc:       "

        end if

        ! Write the YAML 'end of document' symbol
        write(unit,'(a)') "---"

        flush(unit)

    end subroutine yaml_write_metadata




    !> Returns information describing whether atoms belonging to the specified species are inserted/deleted via
    !> GCMC, and if so, what the species' thermodynamic activity is (the 'chemical potential' within in DL_MONTE is
    !> in fact a thermodynamic activity)
    subroutine atom_spec_gcmc_info(job, spec, isgcmc, activity)
        
        use control_type
        use gcmc_moves, only : gcmclookup, gcmc_pot
        
        implicit none
        
            !> Simulation control variables
        type(control), intent(in) :: job

            !> Atomic species to query
        integer, intent(in) :: spec
        
            !> .true. if this species is inserted/deleted, .false. if not
        logical, intent(out) :: isgcmc

            !> The thermodynamic activity of the species - only meaningful if 'gcmc' is .true. (0 by default)
        real(wp), intent(out) :: activity

        integer :: i

        isgcmc = .false.
        activity = 0.0_wp
        
        do i = 1, job%numgcmcatom

           if( gcmclookup(i) == spec ) then

               isgcmc = .true.
               activity = gcmc_pot(i)

               return

           end if

        end do
        
    end subroutine atom_spec_gcmc_info




    !> Returns information describing whether molecules belonging to the specified species are inserted/deleted via
    !> GCMC, and if so, what the species' thermodynamic activity is (the 'chemical potential' within in DL_MONTE is
    !> in fact a thermodynamic activity)
    subroutine mol_spec_gcmc_info(job, spec, isgcmc, activity)
        
        use control_type
        use gcmc_moves, only : gcmclookup, gcmc_pot, gcinsert_molecules
        
        implicit none
        
            !> Simulation control variables
        type(control), intent(in) :: job

            !> Atomic species to query
        integer, intent(in) :: spec

            !> .true. if this species is inserted/deleted, .false. if not
        logical, intent(out) :: isgcmc

            !> The thermodynamic activity of the species - only meaningful if 'gcmc' is .true. (0 by default)
        real(wp), intent(out) :: activity

        integer :: i

        isgcmc = .false.
        activity = 0.0_wp
        
        do i = 1, job%numgcmcmol

            if( gcinsert_molecules(i) == spec ) then

               isgcmc = .true.
               activity = gcmc_pot(i)

               return

           end if

        end do
        
    end subroutine mol_spec_gcmc_info




    !> Output frame of simulation data for the current timestep for the specified simulation
    !> box to the specified unit. 
    ! NOTE FOR DEVELOPERS:
    ! If amending this procedure to output something new, import the new variable from the
    ! relevant module using Fortran's 'use' statement if possible, as opposed to passing them
    ! as arguments to this procedure. Variables stored in 'montecarlo_module.f90' are the
    ! exception, since 'montecarlo_module.f90' uses this module, and hence those variables 
    ! must be 'passed down' to this module as arguments. Box energies and the current step
    ! number are examples of such variables.
    subroutine yaml_write_frame( ib, unit, job, iter, &
         energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
         energypair, energyang, energyfour, energymany, energyext, energymfa, virialtot, &
         volume )

        use control_type
        use cell_module, only : cfgs
        use species_module, only : number_of_elements, number_of_molecules
        use fed_calculus_module, only :  id_cur, param_cur, bias_vec, fed
        use constants_module, only : FED_PS
        use psmc_module, only : ib_act, psmc
        use lc_order_module, only : scalar_nematic_op
        use lambda_module, only : lambda
        
        implicit none
        
            !> Simulation box number
        integer, intent(in) :: ib

            !> Unit to output frame to
        integer, intent(in) :: unit
        
            !> Simulation control variables
        type(control), intent(in) :: job

            !> MC iteration number
        integer, intent(in) :: iter

            !> Energies of various types for all boxes
        real(wp), intent(in), dimension(:) ::  &
             energytot, enthalpytot, energyrcp, energyreal, energyvdw, energythree, &
             energypair, energyang, energyfour, energymany, energyext, energymfa, virialtot, &
             volume

            ! Number of atoms of each species
        integer, dimension(:), allocatable, save :: atomspec_counts
        
            ! Number of molecules each species
        integer, dimension(:), allocatable, save :: molspec_counts

        integer :: i, j, typ
        


        ! Output header of frame

        write(unit,'(a)') "-"
        write(unit,"("//indentfmt//","//kfmt//","//ifmt//")") &
            "timestamp:    ",iter


        ! Output box index
        if(output_boxindex) write(unit,"("//indentfmt//","//kfmt//","//ifmt//")") &
            "box:         ", ib

        ! Output FED (including PSMC) stuff

        !TU: If the switch bias is in play, add the switch bias for phase 2 (which is the 'true' bias for phase
        !TU: 2 configurations here): bias_vec(id_cur) + psmc%switchbias for phase 2. (Note that if it is not
        !TU: in play then its value is the default value of 0.0
        if(output_bias) then

            if( ib_act == 2 ) then

                write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
                        "bias:         ", bias_vec(id_cur) + psmc%switchbias

            else

                write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
                        "bias:         ", bias_vec(id_cur)

            end if

        end if
 

        if(output_orderparam) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "orderparam:   ", param_cur

        if(output_psmcactive) write(unit,"("//indentfmt//","//kfmt//","//ifmt//")") &
            "psmcactive:   ", ib_act


        ! Output energies for box

        if(output_energytot) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energy:       ", energytot(ib) / job%energyunit
        
        if(output_enthalpytot) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "enthalpy:     ", enthalpytot(ib) / job%energyunit
        
        if(output_energyrcp) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyrcp:    ", energyrcp(ib) / job%energyunit
        
        if(output_energyreal) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyreal:   ", energyreal(ib) / job%energyunit
        
        if(output_energyvdw) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyvdw:    ", energyvdw(ib) / job%energyunit
        
        if(output_energythree) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energythree:  ", energythree(ib) / job%energyunit
        
        if(output_energypair) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energypair:   ", energypair(ib) / job%energyunit
        
        if(output_energyang) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyang:    ", energyang(ib) / job%energyunit
        
        if(output_energyfour) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyfour:   ", energyfour(ib) / job%energyunit
        
        if(output_energymany) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energymany:   ", energymany(ib) / job%energyunit
        
        if(output_energyext) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energyext:    ", energyext(ib) / job%energyunit
        
        if(output_energymfa) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "energymfa:    ", energymfa(ib) / job%energyunit

        if(output_virialtot) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "virialtot:    ", virialtot(ib) / job%energyunit
        
        if(output_volume) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "volume:       ", volume(ib)
        
        
        ! Output atom species data

        if(output_natom) then
            
            ! Calculate number of atoms for each element (element = atomic species)
            
            if(.not. allocated(atomspec_counts)) allocate(atomspec_counts(number_of_elements))

            atomspec_counts = 0
            
            do i = 1, cfgs(ib)%num_mols
                
                do j = 1, cfgs(ib)%mols(i)%natom
                    
                    typ = cfgs(ib)%mols(i)%atms(j)%atlabel
                    atomspec_counts(typ) = atomspec_counts(typ) + 1
                    
                enddo
                
            enddo


            ! Output the number of atoms for each element
            
            write(unit,"("//indentfmt//","//kfmt//")", advance="no") "natom:         [ "
            
            do typ = 1, number_of_elements - 1
                
                write(unit,"(i6,a)", advance="no") atomspec_counts(typ)," , "
                
            end do
            
            write(unit,"(i6,a)") atomspec_counts(number_of_elements)," ]"
            
        end if

        
        ! Output molecule species data
        
        if(output_nmol) then
            
            ! Output the number of molecules for each species
            
            write(unit,"("//indentfmt//","//kfmt//")", advance="no") "nmol:          [ "
            
            do typ = 1, number_of_molecules - 1
                
                write(unit,"(i6,a)", advance="no") cfgs(ib)%mtypes(typ)%num_mols," , "
                
            end do
            
            write(unit,"(i6,a)") cfgs(ib)%mtypes(number_of_molecules)%num_mols," ]"

        end if


        ! Output scalar nematic order parameters
        
        if(output_scalarop2) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "scalarop2:    ", scalar_nematic_op(ib,2)

        if(output_scalarop4) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "scalarop4:    ", scalar_nematic_op(ib,4)

        ! Output extra information about the cell dimensions
        if(output_orthodims) then

            write(unit,"("//indentfmt//","//kfmt//",a,"//rfmt//","//rfmt//","//rfmt//",a)") &
                "orthodims:    "," [ ", &
                cfgs(ib)%vec%latvector(1,1), cfgs(ib)%vec%latvector(2,2), cfgs(ib)%vec%latvector(3,3), &
                " ]"

        end if

        ! Output Hamiltonian coupling parameter lambda

        if(output_lambda) write(unit,"("//indentfmt//","//kfmt//","//rfmt//")") &
            "lambda:       ", lambda
        
        flush(unit)


    end subroutine yaml_write_frame
        


end module yamldata_module
