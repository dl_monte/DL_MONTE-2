! *******************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                                   *
! *   john.purton[@]stfc.ac.uk                                                  *
! *                                                                             *
! *   Contributors:                                                             *
! *   -------------                                                             *
! *   A.V.Brukhno (C) 2015-2016                                                 *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *   - overall optimizations (general MC scheme, energy calculus and MC steps) *
! *   - Free Energy Difference (FED) & order parameters (fed_*_module.f90)      *
! *   - Replica Exchange (RE) algorithm (rep_exchange_module.f90)               *
! *   - VdW and general potential forms (vdw_*_module.f90 etc)                  *
! *   - planar pore constraint (slit_module.f90)                                *
! *   - USER defined analytical forms for force-fields (vdw_*.f90 & user_*.inc) *
! *                                                                             *
! *   T.L.Underwood (C) 2015-2016                                               *
! *   t.l.Underwood[@]bath.ac.uk                                                *
! *   - Lattice/Phase-Switch MC methodology                                     *
! *   - algorithm optimizations (gcmc_*_module.90, MC steps, random generator)  *
! *                                                                             *
! *   R.J.Grant (C) 2016                                                        *
! *   r.j.grant[@]bath.ac.uk                                                    *
! *   - extra potential forms (VDW, vdw_direct_module.f90)                      *
! *                                                                             *
! *******************************************************************************

! ==============================================
! Parameters for VdW potential forms from FIELD
!       - to be included where necessary -
!              (singled out by AB)
! ==============================================

! read in vdw potentials and convert to internal units
! also generates the look-up tables
subroutine read_vdw(eng_unit, vdw_rcut, vdw_ecap, vdw_shift)

    use kinds_f90
    use parse_module
    use species_module
    use constants_module, only : uout, ufld, TWOPI, PI
    use comms_mpi_module, only : master, idnode
    use gb_potential_module, only : calc_chi_prime, calc_chi
    use lambda_module, only : using_lambda_ff
    
    implicit none

    !> energy unit flag
    real(kind = wp), intent(in) :: eng_unit

    !> short-range cut-off radius for all non-bonded VdW interactions
    real(kind = wp), intent(in) :: vdw_rcut

    !> repulsion energy capping to avoid high energy values overflows
    real(kind = wp), intent(in) :: vdw_ecap

    !> shift flag
    integer, intent(in) :: vdw_shift

    integer :: katom1, katom2, jtpatm, itpvdw, maxpar, i, j, atm1typ, atm2typ
    integer :: keypot, nread, ntpatm

    real(kind = wp) :: parpot(-1:mxpvdw)!, dc126, a, b, c

    character   :: line*100, word*40
    character*12 :: atom1, atom2, keyword

    logical :: safe

    logical :: uselrc
    real(kind=wp) :: lrccoeff

    logical :: foundmax
    real(kind=wp) :: rloc
 
    !AB: short-range core separations at the repuslion cap:

    ! 12-6 potential (LJ is also set via this function)

    !dc126(a,b,c)=( (-b+sqrt(b*b+4.0_wp*a*c)) / (2.0_wp*c) )**(1.0_wp/6.0_wp)

    !cr126(a,b,c)= -a/(9.0_wp*c**9) + b/(3.0_wp*c**3) ! TWOPI * \int{ U(r)*g(r)*r^2 dr } [*Ni*Nj/V]; assuming g(r)=1

    ! Other potentials ... to be added here
    !dcLJ(a,b,c)=(  )

    !AB: << potential form definitions

    include "potential_forms.inc"

    !AB: >> potential form definitions

    nread = ufld
    ntpatm = number_of_elements

    if( master ) then 

        write(uout,"(/,/,1x,100('-'))")
        write(uout,"(' nonbonded (vdw) potentials :')")
        write(uout,"(1x,100('-'))")

    end if

    !TU: Note that the 'lrc/disp/corr' flag below does not correspond to my implementation of long-range
    !TU: corrections, and is AB's implementation which is now dead code (some of which I in fact built the
    !TU: new implementation on).

    !AB: <<-- DL_MONTE-2 new style 'VDW' directive
    !AB:
    !AB: "VDW [types] <int> [ [cutoff/rcut <real>] [ecap/repcap] <real> [lrc/disp/corr] <int> ]"
    !AB:
    !AB: Since version 2.02 all potentials must have a general form (where practically reasonable):
    !AB:
    !AB: U(r; r_surf, r_cut, e_cap) = U(r-r_surf) - U_c( r = r_cut ) [<= e_cap]
    !AB:
    !AB: where r_surf <= d_hc <= r < r_cut; otherwise U(r > r_cut) = 0
    !AB: and energy capping must be applied for U(r) > e_cap
    !AB: 
    !AB: the three extra parameters, r_cut, r_surf and e_cap to be (optionally) specified in VDW directive(s)
    !AB: the last (4-th) optional parameter should be a flag for dispersion correction when applicable (0/1)
    !AB: any of these should be ignored where not applicable (or not implemented yet)
    !AB: 
    !AB: this way we allow for the most versatile, yet safe, force-field scenaria
    !AB: whilst backwards compatibility is conserved (legacy considerations)
    !AB:
    !AB: -->> DL_MONTE-2 new style 'VDW' directive

    !TU: This pertains to AB's old implementation of long-range VdW corrections, which is no longer in use
    !is_lrcorr = ( vdw_lrc > 0 )

    is_shift  = ( vdw_shift > 0 )

    do itpvdw = 1, ntpvdw

        uselrc = .false.

        ! zero the parameters for the pair potential
        parpot = 0.0_wp

        !AB: number of the compulsory parameters (max)
        maxpar = 5

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000
        
        ! first atom name
        call get_word(line,word)
        atom1 = word

        ! first atom type
        call get_word(line,word)
        atm1typ = get_species_type(word)

        ! second atom name
        call get_word(line,word)
        atom2=word

        ! second atom type
        call get_word(line,word)
        atm2typ = get_species_type(word)

        ! pair potential type
        call get_word(line,word)
        call lower_case(word)

        keyword = word

        !AB: using negative 'keypot' for non-analytical potential forms

        if( keyword == 'hs' .or. keyword == 'sw' .or. keyword == 'hsqw' ) then
        !TU: hard-sphere (hard-core) plus square-well potential

            keypot = -1
            maxpar = 3

        !TU: '+lj' corresponds to the Lennard-Jones with the long-range correction included
        else if( keyword == 'lj' .or. keyword == 'slj' .or. keyword == 'surflj' .or. &
                 keyword == '+lj' ) then
        !JP: Lennard-Jones potential (2)

            keypot = 1 ! <- 2
            maxpar = 2

            if( keyword(1:1) == '+') then

                uselrc = .true.

            end if

        !TU: '+12-6' corresponds to the 12-6 potential with the long-range correction included
        else if( keyword == '12-6' .or. keyword == 's12-6' .or. keyword == 'surf12-6' .or. &
                 keyword == '+12-6' ) then
        !JP: 12-6 potential (1)

            keypot = 2 ! <- 1
            maxpar = 2

            if( keyword(1:1) == '+') then

                uselrc = .true.

            end if


        else if( keyword == '9-3' .or. keyword == 's9-3' .or. keyword == 'surf9-3' ) then

            keypot = 3 ! <- 12
            maxpar = 2

        else if( keyword == '10-4' .or. keyword == 's10-4' .or. keyword == 'surf10-4' ) then

            keypot = 4 ! <- 13
            maxpar = 2

        else if( keyword == 'wca' .or. keyword == 'wcalj' ) then
        !AB: Weeks-chandler-Andersen (WCA) - center-shifted & truncated LJ repulsion (only!) (9)
        !AB: center-shifted, truncated-&-shifted LJ all go as 'lj' now

        !JG: 'wca' not currently referenced in the manual

            keypot = 5 ! <- 9
            maxpar = 2

        else if( keyword == 'hbnd' ) then
        !JP: Hydrogen-bond 12-10 potential (6)

            keypot = 6
            maxpar = 2

        else if( keyword == 'ew' ) then
        !JP: Espanol - Warren (10)
        !JG: 'ew' not currently referenced in the manual

            keypot = 7 ! <- 10
            maxpar = 2

        else if( keyword == 'buck' ) then
        !JP: Buckingham exp-6 potential (4)

            keypot = 8 ! <- 4
            maxpar = 3

        else if( keyword == 'mors' .or. keyword == 'morl' ) then
        !JP: Morse potential (8)
        !JG: 'mors' not currently referenced in the manual

            keypot = 9 ! <- 8
            maxpar = 4

        else if( keyword == 'pnm' .or. keyword == 'pwnm' .or. keyword == 'ljnm' ) then
        !AB: simple two powers potential: u(r)=a*(d/r)^n-b*(d/r)^m (11)

            keypot = 10 ! <- 11
            maxpar = 4

        else if( keyword == 'nm' ) then
        !JP: n-m potential (3)

            keypot = 11 ! <- 3
            maxpar = 4

        else if( keyword == 'snm' .or. keyword == 'hcnm' ) then
        !JP: shifted and force corrected n-m potential by W.Smith (7)

            keypot = 12 ! <- 7
            maxpar = 4

        else if( keyword == 'bhm' ) then
        !JP: Born-Huggins-Meyer exp-6-8 potential (5)

            keypot = 13 ! <- 5
            maxpar = 5

!AB: << extra potential forms introduced by JG 
!
!AB: no need for extra keywords with the only difference being extra parameters 
!AB: the latter can merely be set zero (or optional) when unnecessary

        else if( keyword == 'a-o' ) then

            keypot = 14 ! <- 12
            maxpar = 3

        else if( keyword == 'yuka' .or. keyword == 'yukw' .or. keyword == 'yukawa' ) then

            keypot = 15 ! <- 13
            maxpar = 3

!AB: >> extra potential forms introduced by JG

        else if( keyword == 'lambdahs' ) then

            !TU: LAMBDA-DEPENDENT hard-sphere potential

            !TU: Form: -epsilon_0*ln(1-lambda) if r<sigma; 0 if r>sigma
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon_0 (energy)
            !    2) sigma     (length)
            !
            ! Note that there is no hard-core repulsion for very small r for
            ! lambda-dependent potentials

            keypot = 16
            maxpar = 2


        else if( keyword == 'lambdalj' .or. keyword == '+lambdalj' ) then

            !TU: LAMBDA-DEPENDENT Lennard-Jones potential

            !TU: Form: lambda^a*4*epsilon*( (alpha*(1-lambda)^b+(r/sigma))^(-12/c) - (alpha*(1-lambda)^b+(r/sigma))^(-6/c) )
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon (energy)
            !    2) sigma   (length)
            !    3) alpha   (dimensionless)
            !    4) a       (dimensionless)
            !    5) b       (dimensionless)
            !    6) c       (dimensionless)
            !
            ! Note that there is no hard-core repulsion for very small r for
            ! lambda-dependent potentials

            keypot = 17
            maxpar = 6

            if( keyword(1:1) == "+") then

                uselrc = .true.

            end if

        else if( keyword == 'gauss' ) then

            !TU: Gaussian potential

            !TU: Form: epsilon*exp(-0.5*(r/sigma)^2)
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon (energy)
            !    2) sigma   (length)
            !
            ! Note that there is no hard-core repulsion for very small r for this potential

            keypot = 18
            maxpar = 2

        else if( keyword == 'stillweb' ) then

            !TU: Pair potential component of Stillinger-Weber potential
           
            !TU: Parameters read from FIELD are (same meaning as LAMMPS form):
            !    1) epsilon     (energy)
            !    2) sigma       (length)
            !    3) a           (dimensionless)
            !    4) A           (dimensionless)
            !    5) B           (dimensionless)
            !    6) p           (dimensionless)
            !    7) q           (dimensionless)
            keypot = 19
            maxpar = 7

            
        else if( keyword == 'gb' ) then

            !TU: Gay-Berne potential

            !TU: Parameters read from FIELD are:
            !    1) epsilon_0   (energy)
            !    2) kappa_prime (dimensionless)
            !    3) sigma_s     (length)
            !    4) kappa       (dimensionless)
            !    5) mu          (dimensionless)
            !    6) nu          (dimensionless)
            
            keypot = 50
            maxpar = 6
            
        else if( keyword(1:3) == 'tab' ) then

            keypot = 99
            maxpar = 2

            if( is_tables ) then

                !AB: this is yet to implement... (maybe not even here?)
                !call vdw_read_table()

                call cry(uout,'', &
                     "ERROR: VDW specification with potential type '"//trim(keyword)//&
                     &"' - not implemented yet!!!",124)

            else

                call cry(uout,'', &
                     "ERROR: VDW specification with potential type '"//trim(keyword)//&
                     &"' - switch to the 'tables' compilation branch!!!",124)

            end if

        else if( keyword(1:3) == 'usr' ) then

            if( is_tables ) & 
                call cry(uout,'', &
                     "ERROR: VDW specification with potential type '"//trim(keyword)//&
                     &"' - switch to the 'direct' compilation branch!!!",124)

          if( keyword == 'usr0' ) then

            keypot = 100
            maxpar = 2

          else if( keyword == 'usr1' ) then

            keypot = 101
            !maxpar = 5

          else if( keyword == 'usr2' ) then

            keypot = 102
            !maxpar = 5

          else if( keyword == 'usr3' ) then

            keypot = 103
            !maxpar = 5

          else if( keyword == 'usr4' ) then

            keypot = 104
            !maxpar = 5

          else if( keyword == 'usr5' ) then

            keypot = 105
            !maxpar = 5

!          else if( keyword == 'usr6' ) then
!
!            keypot = 106
!            !maxpar = 5

          endif

        !>>>>>>>>>>>>>> TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL >>>>>>>>>>>>>>

        ! Change the string 'testpairpot' to a suitable string for your potential.
        ! This is the string which will be used to signify the potential in the 'VDW'
        ! section of the FIELD file. Note that there is a maximum length for this
        ! string, which is given next to the declaration of the 'keyword' variable
        ! at the top of this function. At the time of writing this maximum was 12
        ! characters.
          
        else if( keyword == "testpairpot" ) then

            ! Change the number '9999' here to the 'code' for your potential which you
            ! ultimately choose. Note that this code must not be the same as any code used
            ! above for an existing potential in DL_MONTE. Note that this code must match
            ! the code corresponding to this potential in 'vdw_direct_module.f90' (which
            ! is '9999' by default) and elsewhere in this file.      
            keypot = 9999

            ! Set 'maxpar' to the number of parameters which your pair potential will have.
            ! For example the Lennard-Jones potential has two parameters, epsilon and sigma,
            ! and so for this potential 'maxpar' would be 2. With this, DL_MONTE will read
            ! the first 'maxpar' numbers on a line in the 'VDW' block in the 'FIELD' file
            ! containing your potential. E.g. for 'maxpar=3' and
            ! 'A core B core testpairpot 1.0 2.0 3.0 4.0' in FIELD DL_MONTE would read the
            ! numbers '1.0', '2.0' and '3.0' as the 3 parameters for your potential; note
            ! that since 'maxpar=3' the 4th parameter '4.0' is ignored! To elaborate,
            ! these numbers are stored in an array 'parpot', which we must do more with
            ! later; see below...
            maxpar = 3         

        !<<<<<<<<<<<<<< TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL <<<<<<<<<<<<<<
          
        else

            call cry(uout,'', &
                 "ERROR: VDW specification with unknown potential type '"//trim(keyword)//"'!!!",124)

            !call error(124)

        endif

        !AB: default short-range core separation at the repuslion cap:
        !TU: Note that in PSMC the behaviour of this hard-core radius is altered - except
        !TU: for potentials with an explicit 'hard' component, e.g., the hard-sphere potential, 
        !TU: or the A-O potential (see below)
        parpot(0) = HC_VDW

        !AB: read-in all the compulsory parameters
        do i = 1,maxpar

           call get_word(line,word)
           parpot(i) = word_2_real(word)
           
        end do
       

        !AB: index of the first optional parameter (if any)
        maxpar = maxpar+1

        if( master ) write(uout,"(1x,2a8,3x,a10,3x,10f15.6)") &
                           atom1,atom2,keyword,(parpot(j),j=0,mxpvdw)

        katom1=0
        katom2=0

        do jtpatm=1,ntpatm

           if (atom1 == element(jtpatm) .and. atm1typ == eletype(jtpatm)) katom1=jtpatm
           if (atom2 == element(jtpatm) .and. atm2typ == eletype(jtpatm)) katom2=jtpatm

        end do

        if (katom1 == 0 .or. katom2 == 0) then

           call error(125)

        endif

        lstvdw(katom1,katom2) = itpvdw
        lstvdw(katom2,katom1) = itpvdw

        ! convert energies to internal unit (except when not needed)
        if( keypot > 0 .and. keypot < 14 ) parpot(1) = parpot(1)*eng_unit

        if (keypot == -1) then
            !TU: hard-sphere (hard-core) + square well potential  ('hs'/'hsqw' -> -1)
            !JG: Exceptions for square well potential

            if( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: Hard-sphere interaction radius is negative!"

                call error(999)

            end if

            if( parpot(1) > vdw_rcut ) then

                if( master ) write(uout,*) "ERROR: Hard-sphere interaction radius > cut-off!"

                call error(999)

            end if

            parpot(0) = parpot(1)

            if( abs(parpot(2)) > 1.0e-10_wp ) then

                if( parpot(2) > vdw_rcut ) then

                    if( master ) write(uout,*) "ERROR: Square-well interaction radius > cut-off!"

                    call error(999)

                end if

                if( parpot(2) < parpot(1) ) then

                    if( master ) write(uout,*) "ERROR: Square-well interaction radius < hard sphere radius!"

                    call error(999)

                end if

            end if

            !AB: allow either attractive well or repulsive shoulder (but not both!)
            if( abs(parpot(3)) > 1.0e-10_wp ) parpot(3) = parpot(3) * eng_unit

        else if( keypot == 1 ) then !if (keypot == 2) then

            !JP: Lennard-Jones potential
            !AB: also shifted Lennard-Jones potential 
            !AB: also surface centered Lennard-Jones potential

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            !AB: parpot(3) sets the distance from the particle centre to the 12-6 origin (i.e. surface diameter)
            !AB: so the 12-6 interaction is shifted to the particle surface(s)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the LJ (VdW) surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'LJ (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(3)

                if( uselrc ) then

                    call cry(uout,'',"ERROR: Long-range corrections are not supported with LJ potential" &
                             //" with shifted origin",999)

                end if

            end if

            !AB: short-range core separation at the repuslion cap:
            !TU: Note that this is altered in 'psmc_module.f90' if we are using PSMC
            parpot(0) = dc126( 4.0_wp*parpot(1)*parpot(2)**12, 4.0_wp*parpot(1)*parpot(2)**6, vdw_ecap )

            if( is_shift ) then 

                ! Lennard-Jones potential
                !evdw = vlj( (parpot(2) / (vdw_rcut-parpot(3)))**6 , parpot(1) )  ! vlj(sri6,a)

                parpot(-1) = -vlj( (parpot(2) / (vdw_rcut-parpot(3)))**6 , parpot(1) )  ! vlj(sri6,a)

                if( uselrc ) then

                    call cry(uout,'',"ERROR: Long-range corrections are not supported with shifted LJ " &
                             //" potential",999)

                end if

            !TU: This pertains to AB's old implementation of long-range VdW corrections, which is no longer in use
            !else if( is_lrcorr ) then 
            !
            !    !parpot(-1) = 4.0_wp*TWOPI*cr126( parpot(1)*parpot(2)**12, parpot(1)*parpot(2)**6, vdw_rcut )

            end if

            !TU: Calculate the coefficient for the long-range correction and store it in the matrix
            if( uselrc ) then

                !TU: Use of shifts and LRC together is forbidden and caught above - see 'cry' above.
               
                !TU: Calculate the long-range correction coefficient
                lrccoeff = vljlrc( vdw_rcut, parpot(2) ) * parpot(1)

                lrcvdw(katom1,katom2) = lrccoeff
                lrcvdw(katom2,katom1) = lrccoeff

                if( master ) write(uout,'(/,1x,a,i3,a)') &
                    'LJ (VdW) pair potential (',itpvdw,') will have long-range corrections applied '

            end if

        else if( keypot == 2 ) then !if (keypot == 1) then

            !JP: 12-6 potential ('12-6' -> 1)
            !AB: also shifted 12-6 potential
            !AB: also surface centered 12-6 potential

            parpot(2)=parpot(2)*eng_unit

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            !AB: parpot(3) sets the distance from the particle centre to the 12-6 origin (i.e. surface diameter)
            !AB: so the 12-6 interaction is shifted to the particle surface(s)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the 12-6 (VdW) surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    '12-6 (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(3)

                if( uselrc ) then

                    call cry(uout,'',"ERROR: Long-range corrections are not supported with 12-6 potential" &
                             //" with shifted origin",999)

                end if


            end if

            !AB: short-range core separation at the repuslion cap:
            !TU: Note that this is altered in 'psmc_module.f90' if we are using PSMC
            parpot(0) = dc126( parpot(1), parpot(2), vdw_ecap )

            if( is_shift ) then 

                ! 12-6 potential
                !evdw = v126( 1.0_wp/(vdw_rcut-parpot(3))**6, parpot(1) , parpot(2) ) ! v126(ri6,a,b)

                parpot(-1) = -v126( 1.0_wp/(vdw_rcut-parpot(3))**6, parpot(1) , parpot(2) ) ! v126(ri6,a,b)

                if( uselrc ) then

                    call cry(uout,'',"ERROR: Long-range corrections are not supported with shifted 12-6 " &
                             //" potential",999)

                end if

            !TU: This pertains to AB's old implementation of long-range VdW corrections, which is no longer in use
            !else if( is_lrcorr ) then 
            !
            !    !parpot(-1) = TWOPI*cr126( parpot(1), parpot(2), vdw_rcut )

            end if


            !TU: Calculate the coefficient for the long-range correction and store it in the matrix
            if( uselrc ) then

                !TU: Use of shifts and LRC together is forbidden and caught above - see 'cry' above.
               
                !TU: Calculate the long-range correction coefficient
                lrccoeff = v126lrc( vdw_rcut, parpot(1), parpot(2) )

                lrcvdw(katom1,katom2) = lrccoeff
                lrcvdw(katom2,katom1) = lrccoeff

                if( master ) write(uout,'(/,1x,a,i3,a)') &
                    '12-6 (VdW) pair potential (',itpvdw,') will have long-range corrections applied '

            end if


        else if( keypot == 3 .or. keypot == 4 ) then !if( keypot == 12 .or. keypot == 13 ) then

            !JG: a/r^n-b/r^m potential, n = [9,10] and m = [3,4]

            if ( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: power (9-3 or 10-4) repulsion parameter (a) must be positive!!!"

                call error(999)

            else if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: power (9-3 or 10-4) attraction parameter (b) must be positive!!!"

                call error(999)

            end if

            parpot(2) = parpot(2)*eng_unit

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Power n-m (LJ-type VdW) core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Power n-m (LJ-type VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if (keypot == 5) then !if (keypot == 9) then

            !JP: Weeks-chandler-Anderson - shifted & truncated Lenard-Jones ('wca' -> 9)

            parpot(2) = Abs(parpot(2))

            !AB: reset the shift of origin if unsuitable value is given in the input

            if( parpot(3) > parpot(2)/2.0_wp ) &
                parpot(3) = Sign(1.0_wp,parpot(3))*parpot(2)/2.0_wp

            parpot(4) = 2.0_wp**(1.0_wp/6.0_wp)*parpot(2)+parpot(3)

            !AB: judging by the original code in vdw_interpolate_module.f90::vdw_generate(..)
            !AB: parpot(4) serves as an extra cut-off specific to this interaction
            !AB: but it makes more sense to do the check and resetting here rather than there

            !AB: make sure it's <= cut-off
            !if( parpot(4) == 0.0_wp) parpot(4) = vdw_rcut
            if( parpot(4) == 0.0_wp .or. parpot(4) > vdw_rcut ) parpot(4) = vdw_rcut

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the WCA-LJ core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'WCA-LJ (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if (keypot == 6) then

            !JP: Hydrogen-bond 12-10 potential ('hbnd' -> 6)

            parpot(2)=parpot(2)*eng_unit

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Hydrogen-bond core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Hydrogen-bond (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if (keypot == 7) then !if (keypot == 10) then

            !JP: espanol - warren ('ew' -> 10)

            !AB: originally there was nothing here in terms of the input for this case

            !AB: judging by the original code in vdw_interpolate_module.f90::vdw_generate(..)
            !AB: parpot(2) serves as an extra cut-off specific to this interaction
            !AB: but it makes more sense to do the check and resetting here rather than there

            !AB: make sure it's <= cut-off
            !if( parpot(2) == 0.0_wp) parpot(2) = vdw_rcut
            if( parpot(2) == 0.0_wp .or. parpot(2) > vdw_rcut ) parpot(2) = vdw_rcut

            !maxpar = 3

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Espanol-Warren core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Espanol-Warren (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if (keypot == 8) then !if (keypot == 4) then

            !JP: Buckingham exp-6 potential ('buck' -> 4)

            parpot(3)=parpot(3)*eng_unit

            !maxpar = 4

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the Buckingham core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Buckingham (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

            !TU: Set hard core repulsion to the location of the maximum in the Buckingham potential (to prevent
            !TU: particles getting 'stuck together' due to the Buckingham potential's divergence to -Infinity at r=0).
            !TU: Determine the location of the maximum to a precision of 0.0001 Angstroms; consider separations
            !TU: between r=0.0001 and r=vdw_rcut in the search for the maximum
            call buckingham_max_location(parpot(1), parpot(2), parpot(3), 0.0001_wp, vdw_rcut, foundmax, rloc)
            
            if(foundmax) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Buckingham (VdW) pair potential (',itpvdw,') maximum location determined to be = ',rloc

                parpot(0) = rloc
            
            else

                call cry(uout, '', "WARNING: Could not find maximum location in Buckingham potential to set as D_core. " &
                         //"Setting D_core (ignoring any shift) to 0.",0)
                parpot(0) = 0.0_wp

            end if 

            !TU: Account for a shift if need be
            parpot(0) = parpot(0) + parpot(maxpar)


            !TU: The 'buckingham_max_location' function is not foolproof; the user could put in Buckingham parameters
            !TU: which result in an unreasonably small value being chosen for the hard-core repulsion radius parpot(0)
            !TU: (or perhaps even an unreasonably large one, e.g. the cut-off radius for the potential). A real-world 
            !TU: situation I've come across was when parpot(3)=0, leaving just the exponential term, in which case 
            !TU: there is no maximum in the potential and the max. location is found to be 0. We force DL_MONTE to 
            !TU: not allow the hard-core radius to be less than HC_VDW for the Buckingham potential
            if( parpot(0) < HC_VDW ) then

               if(foundmax) then

                   call cry(uout, '', "WARNING: Max. location for Buckingham potential is small (<0.5); deemed too"// &
                                      " small to use as D_core. Check potential parameters? Setting D_core to 0.5.", 0)

               else

                   call cry(uout, '', "WARNING: Assigned D_core for Buckingham potential is small (<0.5); deemed too"// &
                                      " small to use as D_core. Check potential parameters? Setting D_core to 0.5.", 0)

               end if

               parpot(0) = HC_VDW

            end if

            if( master ) write(uout,'(/,1x,a,i3,a,f12.6,a,e12.6)') &
                'Buckingham (VdW) pair potential (',itpvdw,') capped at D_core = ',parpot(0),' by E_cap = ',vdw_ecap


        else if (keypot == 9) then !if (keypot == 8) then

            ! Morse potential ('mors' -> 8)
            ! also 'morl' potential - 'mors' plus A*r^{-12} repulsion ('morl' -> 13)

            !AB: this potential was not implemented in the original vdw_interpolate_module.f90 - ???

            !JG: Morse potential exceptions

            if( parpot(1) < 0.0_wp ) then

                !TU: This used to be an error (code 999), but I've downgraded it to a warning at the
                !TU: request of Steve Parker
                call cry(uout, '', "WARNING: Negative Morse E0 parameter detected!!!", 0)

            end if

            if( parpot(2) < 0.0_wp ) then 

                if( master ) write(uout,*)'ERROR: Negative Morse k parameter detected!!!',itpvdw

                call error(999)
            end if

            if( parpot(3) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: Negative Morse r0 distance detected!!! ',itpvdw

                call error(999)
            end if

            if( parpot(4) < 0.0_wp ) then

                !TU: This used to be an error (code 999), but I've downgraded it to a warning at the
                !TU: request of Steve Parker
                call cry(uout,'', "WARNING: Negative Morl LJ repulsion detected!!!", 0)

            end if


            !JG: Remember to correct for units of repulsion term
            parpot(4) = parpot(4) * eng_unit
            
            !maxpar = 5

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Morse core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Morse (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if( keypot == 10 ) then !if (keypot == 11) then

            !JG: a/r^n-b/r^m potential

            if ( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm repulsion parameter (a) must be positive!!!"

                call error(999)

            else if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm attraction parameter (b) must be positive!!!"

                call error(999)

            else if ( parpot(3) > 0.0_wp .and. parpot(4) > 0.0_wp .and. parpot(3) < parpot(4) ) then

                if( master ) write(uout,*) "ERROR: pwnm/ljnm repulsion power (n) smaller than attraction power (m)!!!"

                call error(999)

            end if

            parpot(2) = parpot(2)*eng_unit

            !maxpar = 5

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*) &
                             'ERROR: negative surface diameter for the Power n-m (LJ-type VdW) core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'Power n-m (LJ-type VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

        else if (keypot == 11) then !if (keypot == 3) then

            !JP: n-m potential ('nm' -> 3)

            !AB: originally there was nothing here in terms of the input for this case

            call get_word(line,word)
            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

            !AB: parpot(maxpar) sets the distance from the particle centre to the n-m origin (i.e. surface core distance)
            !AB: so the n-m interaction is shifted to the particle surface(s)

            !maxpar = 5

            if( parpot(maxpar) < 0.0_wp ) then

                if( master ) write(uout,*)'ERROR: negative surface diameter for the n-m core surface!!! ',itpvdw

                call error(999)

            else if( parpot(maxpar) > 0.0_wp ) then

                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
                    'n-m (VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)

            end if

            if ( parpot(3) > 0.0_wp .and. parpot(4) > 0.0_wp .and. parpot(3) < parpot(4) ) then

                if( master ) write(uout,*) "ERROR: n-m repulsion power (n) smaller than attraction power (m)!!!"

                call error(999)

            end if

        else if (keypot == 12) then !if (keypot == 7) then

            !JP: shifted and force corrected n-m potential by W.Smith ('snm' -> 7)

            !AB: originally there was nothing here in terms of the input for this case
 
            if (parpot(3) > parpot(2)) call error(118)

            if (vdw_rcut/parpot(4) < 1.0_wp) call error(119)

            !maxpar = 5

!AB: does not make sense to shift the origin to the surface (???)
!            call get_word(line,word)
!            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)
!
!            if( parpot(maxpar) < 0.0_wp ) then
!
!                if( master ) write(uout,*) &
!                             'ERROR: negative surface diameter for the Shifted n-m (W.Smith) core surface!!! ',itpvdw
!
!                call error(999)
!
!            else if( parpot(maxpar) > 0.0_wp ) then
!
!                if( master ) write(uout,'(/,1x,a,i3,a,f12.6)') &
!                    'Shifted n-m (W.Smith VdW) pair potential (',itpvdw,') origin is shifted to D_surf = ',parpot(maxpar)
!
!            end if

        else if (keypot == 13) then !if (keypot == 5) then

            !JP: Born-Huggins-Meyer exp-6-8 potential ('bhm' -> 5)

            parpot(4)=parpot(4)*eng_unit
            parpot(5)=parpot(5)*eng_unit

!AB: does not make sense to shift the origin to the surface (???)
!            call get_word(line,word)
!            if( trim(word) /= "" .and. word(1:1) /= "#" ) parpot(maxpar) = word_2_real(word)

        else if (keypot == 14) then

            !JG: a-o potential inherited from TU:s code for hard-sphere potential...

            if( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: A-O Hard-sphere interaction radius is negative!"

                call error(999)

            end if

            if( parpot(1) > vdw_rcut ) then

                if( master ) write(uout,*) "ERROR: A-O Hard-sphere interaction radius > cut-off radius!"

                call error(999)

            else if ( parpot(1)*(1.0_wp + parpot(2)) > vdw_rcut ) then

                if( master ) write(uout,*) "ERROR: A-O Hard-sphere + polymer diameter > cut-off radius!"

                call error(999)

            else if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: A-O Negative size ratio!"

                call error(999)

            else if ( parpot(2) > 1.0_wp ) then

                if( master ) write(uout,*) "ERROR: A-O Size ratio > 1!"

                call error(999)

            else if ( parpot(3) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: A-O Negative polymer volume fraction!"

                call error(999)

            end if

!AB: does not make sense to shift the origin to the surface (???)
!            call get_word(line,word)
!            parpot(maxpar) = word_2_real(word)

            !JG: a-o potential parameters pre-calculated

            ! patpot(0) serves as \sigma = hard-core radius

            parpot(0) = parpot(1)

            ! parpot(2) is set to \sigma_big + \sigma_small through \sigma * (1 + q)
            parpot(2) = ( 1.0_wp + parpot(2) ) * parpot(1)

            !AB: make sure it's <= cut-off
            if( parpot(2) == 0.0_wp .or. parpot(2) > vdw_rcut ) parpot(2) = vdw_rcut

            ! parpot(3) the prefactor - \nu * ( 1 + q )^3/ q^3
            parpot(3) = -parpot(3) * ( 1.0_wp + parpot(2) )**3 / parpot(2)**3

            ! parpot(4) the r^2 coefficient -3 / (2 * \sigma * (1 + q))
            parpot(4) = -3.0_wp / ( 2.0_wp * parpot(1) * ( 1.0_wp + parpot(2) ) )

            ! parpot(5) stores the r^3 coefficient 1 / (2 * \sigma^3 * (1 + q)^3)
            parpot(5) = 1.0_wp / ( 2.0_wp * parpot(1)**3 * ( 1.0_wp + parpot(2) )**3 )

        else if (keypot == 15) then

            !JG: Yukawa potential inherited from TU: Code for hard-sphere potential ('hs')...

            if( parpot(1) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: Yukawa Hard-Sphere interaction radius is negative!!!"

                call error(999)

            end if

            if( parpot(1) > vdw_rcut ) then

                if( master ) write(uout,*) "ERROR: Yukawa Hard-Sphere interaction radius > VdW cut-off!!!"

                call error(999)

            else if ( parpot(2) < 0.0_wp ) then

                if( master ) write(uout,*) "ERROR: Yukawa Negative inverse screening length!!!"
            
                call error(999)

            else if ( parpot(3) < 0.0_wp ) then

                call cry(uout,'', "WARNING: Yukawa Negative interaction strength!!!",0)

            end if

!AB: does not make sense to shift the origin to the surface (???)
!            call get_word(line,word)
!            parpot(maxpar) = word_2_real(word)

            ! patpot(0) serves as \sigma = hard-core radius

            parpot(0) = parpot(1)
            
            !TU: Convert free parameter with dimension energy to internal units
            parpot(3) = parpot(3)*eng_unit

        else if(keypot == 16) then

            !TU: LAMBDA-DEPENDENT hard-sphere potential

            !TU: Form: -epsilon_0*ln(1-lambda) if r<sigma; 0 if r>sigma
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon_0 (energy)
            !    2) sigma
            !
            ! Note that there is no hard-core repulsion for very small r for
            ! lambda-dependent potentials

            using_lambda_ff = .true.

            !TU: Convert epsilon_0 (which has dimension energy) to internal units
            parpot(1) = parpot(1) * eng_unit


        else if(keypot == 17) then

            !TU: LAMBDA-DEPENDENT Lennard-Jones potential

            !TU: Form: lambda^a*4*epsilon*( (alpha*(1-lambda)^b+(r/sigma))^(-12/c) - (alpha*(1-lambda)^b+(r/sigma))^(-6/c) )
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon (energy)
            !    2) sigma   (length)
            !    3) alpha   (dimensionless)
            !    4) a       (dimensionless)
            !    5) b       (dimensionless)
            !    6) c       (dimensionless)
            !
            ! Note that there is no hard-core repulsion for very small r for
            ! lambda-dependent potentials

            using_lambda_ff = .true.

            !TU: Convert epsilon (which has dimension energy) to internal units
            parpot(1) = parpot(1) * eng_unit

            !TU: Calculate the coefficient for the long-range correction and store it in the matrix
            if( uselrc ) then

                !TU: Use the expression corresponding to the lambda=1 version of the potential (i.e. the FULL Lennard-Jones
                !TU: potential) for lrccoeff; this is in effect scaled by a factor lambda_vdw (see lambda_module.f90) in 
                !TU: the vdw_lrc_energy procedure when the long-range correction needs to be calculated.
               
                !TU: Calculate the long-range correction coefficient
                lrccoeff = vljlrc( vdw_rcut, parpot(2) ) * parpot(1)

                lrcvdw(katom1,katom2) = lrccoeff
                lrcvdw(katom2,katom1) = lrccoeff

                if( master ) write(uout,'(/,1x,a,i3,a)') &
                    'lambdaLJ (VdW) pair potential (',itpvdw,') will have lambda-dependent long-range corrections applied'

            end if

        else if(keypot == 18) then

            !TU: Gaussian potential

            !TU: Form: epsilon*exp(-0.5*(r/sigma)^2)
            !   
            !    Parameters read from FIELD are:
            !    1) epsilon (energy)
            !    2) sigma   (length)
            ! Note that there is no hard-core repulsion for very small r for this potential

            !TU: Convert epsilon (which has dimension energy) to internal units
            parpot(1) = parpot(1) * eng_unit

            !TU: Have no hard-core radius for this potential
            parpot(0) = 0.0_wp                        


        else if (keypot == 19) then

            !TU: Pair potential component of Stillinger-Weber potential (TU)

            !TU: Convert epsilon with dimension energy to internal units
            parpot(1) = parpot(1)*eng_unit


        else if(keypot == 50) then

            !TU: Gay-berne potential
            
            !TU: Parameters read from FIELD (currently stored in 'parpot') are:
            !    1) epsilon_0   (energy)
            !    2) kappa_prime (dimensionless)
            !    3) sigma_s     (length)
            !    4) kappa       (dimensionless)
            !    5) mu          (dimensionless)
            !    6) nu          (dimensionless)

            !TU: Instead of kappa_prime and kappa, it is more efficient to work with
            !    the dimensionless parameters chi_prime and chi: chi_prime is a
            !    function of kappa_prime and mu; and chi is a function of kappa. Hence
            !    we change parpot(2) and parpot(4) to chi_prime and chi, respectively
            parpot(2) = calc_chi_prime(parpot(2), parpot(5))
            parpot(4) = calc_chi(parpot(4))
            
            !TU: Convert epsilon_0 (which has dimension energy) to internal units
            parpot(1) = parpot(1) * eng_unit
            
            call cry(uout, '(/,1x,a,/)', &
                "WARNING: Gay-Berne potential is a new feature under development", 0)


        !>>>>>>>>>>>>>> TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL >>>>>>>>>>>>>>

        ! Change the number '9999' here to the 'code' for your potential which you
        ! ultimately choose. Note that this code must not be the same as any code used
        ! above for an existing potential in DL_MONTE. Note that this code must match
        ! the code corresponding to this potential in 'vdw_direct_module.f90' (which
        ! is '9999' by default) and elsewhere in this file.

        else if( keypot == 9999 ) then
            
            ! At this point in the code the parameters for your potential, of which
            ! recall there are 'maxpar', are stored in an array 'parpot': 'parpot(1)',
            ! 'parpot(2)', 'parpot(3)', ..., 'parpot(maxpar)' are the 1st, 2nd, 3rd,
            ! ..., 'maxpar'th parameters. However these values may be in a system of
            ! units specified in the FIELD file, and should be converted into the
            ! system of units used internally in DL_MONTE: distances in Angstroms and
            ! energies of 10 J/mol. Any unit conversions should be done here.
            !
            ! Any potential parameters with dimension of energy should in the FIELD file
            ! be specified in energy units given at the top of the FIELD file. To
            ! convert such parameters into energies in internal units multiply by
            ! the variable 'eng_unit'. Below this is done for the 1st parameter of the
            ! potential.
            parpot(1) = parpot(1) * eng_unit

            ! OPTIONAL: Each potential type has an associated 'hard-core radius'. Below
            ! the hard-core radius the potential is assumed to be infinity. DL_MONTE
            ! assigns a default value to this for all pair potentials. However you can
            ! overide it by specifying it explicitly; here the 'parpot(0)' variable
            ! is the hard-core radius for this potential. The commented-out code
            ! below sets the hard-core radius to 0, effectively switching off the
            ! hard-core-radius behaviour.
            !parpot(0) = 0.0_wp

            ! IMPORTANT: In Fortran double-precision must be specified explicitly for
            ! floating-point constants; they are assumed to be single-precision by
            ! default. In DL_MONTE's source code double-precision for a constant can 
            ! by adding a '_wp' to the constant. E.g. '0.1' is the floating point number
            ! 0.1 in single precision, while '0.1_wp' is the number in double-precision.
            ! If 'x' is a double-precision variable, then the statement 'x=0.1' will
            ! result in a casting of the 0.1 in single-precision to double precision,
            ! resulting in a loss of precision. However the statement 'x=0.1_wp' will
            ! not have this issue. Hence always use '_wp' with constants in DL_MONTE's
            ! source code to prevent loss of precision.
            
            ! ADVANCED: It may be more computationally efficient for the parameters
            ! specified by the user in FIELD to be transformed to a different set of
            ! parameters which define the potential - for internal calculations in
            ! DL_MONTE. See the Gay-Berne potential above for an example of this.
            ! Complicated transformations may warrant functions to be used, as was
            ! done above for the Gay-Berne potential. Such functions could be added
            ! at the bottom of this file.
           
        !<<<<<<<<<<<<<< TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL <<<<<<<<<<<<<<
           
        end if


        if( master ) then

            if( keypot == 16 .or. keypot == 17 ) then

                !TU: Output only for lambda-dependent potentials

                !TU: Note that there is no capping for lambda-dependent potentials! parpot(0) is not
                !TU: 0, but is not used for these potentials
                write(uout,'(/,1x,a,i3,a,/)') &
                    'VdW potential ',itpvdw,' is lambda-dependent with no hard-core cap at small seperations.'

            else if( keypot == 18 ) then

                !TU: Output only for soft potentials: Gaussian potential

                write(uout,'(/,1x,a,i3,a,/)') &
                    'VdW potential ',itpvdw,' has no hard-core cap at small seperations.'
                
            else

                write(uout,'(/,1x,a,i3,a,f12.6,a,e12.6,/)') &
                'VdW repulsion capped at D_core(',itpvdw,') = ',parpot(0),' by E_cap = ',vdw_ecap

                if( is_shift ) then

                    write(uout,'(1x,a,i3,a,e12.6,a,e12.6,/)') &
                    'VdW truncating & shifting term(',itpvdw,') = ',parpot(-1),' at R_cut = ',vdw_rcut

                !TU: This pertains to AB's old implementation of long-range VdW corrections, which is no longer in use
                !else if( is_lrcorr ) then
                !
                !    write(uout,'(1x,a,i3,a,e12.6,a,e12.6,/)') &
                !    'VdW long-range correction term(',itpvdw,') = ',parpot(-1),' at R_cut = ',vdw_rcut

                end if

            end if

        end if

        !set the type of potential
        ltpvdw(itpvdw) = keypot

        !copy parameters across
        prmvdw(:,itpvdw) = parpot(:)

    end do   ! end loop over potentials

    HC_MIN  = MinVal(prmvdw(0,:))
    HC_MIN2 = HC_MIN*HC_MIN

    if( (vdw_ecap - HS_ENERGY) > 1.e-10 ) HS_ENERGY = 2.0_wp*vdw_ecap 

!AB: this only to be included in the "tabulated & interpolated" case
!JP: generate nonbonded force arrays
    if( is_tables .and. ntpvdw > 0 ) call vdw_generate(vdw_rcut)

    return

1000 call error(123)

    stop

end subroutine read_vdw




!TU: Added by me...
!> Determines the location of the maximum in the Buckingham potential for the
!> specified parametrisation. The Buckingham potential is U(r)=A*exp(-r/rho)-C/r^6,
!> which diverges to -Infintity at r=0 approaching from r>0; the potential is 
!> unphysical for r<r_max, where r_max is the location of the maximum in U(r).
!> (Arguably the potential becomes unphysical at some r>r_max, e.g., at the
!> point of inflection in U(r) where the second derivative is 0). In DL_MONTE we
!> treat the Buckingham potential as a hard-sphere potential for r<r_max, thus
!> eliminating the prospect of particles interacting via this potential 'sticking
!> together' if they somehow become separated by a distance r<r_max.
!> The algorithm determines the maximum by starting at a very small separation, then
!> increasing the separation until the potential turns over. Note that this function
!> will always return a separation slightly larger than the true location of the
!> maximum - which is preferable to returning a separation slightly smaller than the
!> true location of the true maximum (one or another is inevitable because of the
!> finite precision).
subroutine buckingham_max_location(A, rho, C, prec, rrangemax, foundmax, rloc)

    use constants_module, only : uout

        !> Parameter 'A' for the Buckingham potential
    real(wp), intent(in) :: A

        !> Parameter 'rho' for the Buckingham potential
    real(wp), intent(in) :: rho

        !> Parameter 'C' for the Buckingham potential
    real(wp), intent(in) :: C

        !> Precision to which the maximum is to be determined
    real(wp), intent(in) :: prec

        !> Upper bound for the separation in the search
    real(wp), intent(in) :: rrangemax
    
        !> Logical signifying if the max. location was found or not
    logical, intent(out) :: foundmax

        !> Location of the maximum
    real(wp), intent(out) :: rloc

        ! Starting separation
    real(wp) :: rstart
        ! Step size
    real(wp) :: rstep
        ! Maximum separation to consider (hard-coded below to be 1000*rho)
    real(wp) :: rend
        ! Separation under consideration
    real(wp) :: r

        ! Values of the Buckingham potential at various r
    real(wp) :: u_old, u_new
    
    foundmax = .false.
    rloc = 0.0_wp

    rstart = prec
    rstep = prec
    rend = rrangemax

    r=prec
    
    u_old = buckingham_potential(A,rho,C,r)

    do while(r < rend)

        r = r + rstep
        u_new = buckingham_potential(A,rho,C,r)

        if(u_new <= u_old) then

            foundmax = .true.
            rloc = r
            exit

        end if

        u_old = u_new

    end do

end subroutine buckingham_max_location




!> A simple function to calculate the Buckingham potential for a given inter-particle separation
real(wp) function buckingham_potential(A, rho, C, r)

        !> Parameter 'A' for the Buckingham potential
    real(wp), intent(in) :: A

        !> Parameter 'rho' for the Buckingham potential
    real(wp), intent(in) :: rho

        !> Parameter 'C' for the Buckingham potential
    real(wp), intent(in) :: C

        !> Inter-particle separation
    real(wp), intent(in) :: r

    buckingham_potential = A*exp(-r/rho) - C/r**6

end function buckingham_potential
