! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   J.Grant - Introduction of orthogonal lattice vectors                 *
! *   r.j.grant[@]bath.ac.uk                                                *
! *                                                                         *
! ***************************************************************************

module latticevectors_type

use kinds_f90

implicit none

type latticevectors

    !> lattice vectors
    real(kind = wp) :: latvector(3,3)

    !> full lattice vectors for orthogonal calculations
    real(kind = wp) :: fullvector(3)

    !> half lattice vectors for orthogonal calculations
    real(kind = wp) :: halfvector(3)

    !> reciprocal lattice vectors
    real(kind = wp) :: rcpvector(3,3)

    !> inverted lattice vectors
    real(kind = wp) :: invlat(3,3)

    !> temp storage for lattice vectors
    real(kind = wp) :: latstore(3,3)

    !> temp storage for inverted vectors
    real(kind = wp) :: invstore(3,3)

    !> temp storage for reciprocal lattice vectors
    real(kind = wp) :: rcpstore(3,3)

    !> stress
    real(kind = wp) :: stress(3,3)

    !> cell volume
    real(kind = wp) :: volume

    !> temp copy of volume
    real(kind = wp) :: volstore

    !> simple orthogonal lattice vectors flag ie leading diagonal only -> to simplify some calculation
    logical :: is_simple_orthogonal

    !> orthogonal lattice vectors flag less stringent flag than is_simple_orthogonal but more inclusive
    logical :: is_orthogonal

end type

end module
