!***************************************************************************
!   Copyright (C) 2018 by Nishant Birdi & T L Underwood                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Module containing procedures and variables pertaining to the Gay-Berne
!>   potential
!> @using
!> - `kinds_f90`

module gb_potential_module

    use kinds_f90

    implicit none


    
contains

    

!> Returns the constant chi_prime used in the Gay-Berne potential
!> implementation, given `kappa_prime` and `mu`
real(kind=wp) function calc_chi_prime(kappa_prime, mu)

    use kinds_f90

    implicit none

    real(kind=wp), intent(in) :: kappa_prime
    real(kind=wp), intent(in) :: mu

    real(kind=wp) :: mu_inv
    
    mu_inv = 1.0 / mu
    
    calc_chi_prime = ( kappa_prime**(mu_inv) - 1.0_wp ) / ( kappa_prime**(mu_inv) + 1.0_wp )
    
end function calc_chi_prime



!> Returns the constant chi used in the Gay-Berne potential
!> implementation, given `kappa`
real(kind=wp) function calc_chi(kappa)

    use kinds_f90

    implicit none

    real(kind=wp), intent(in) :: kappa

    real(kind=wp) :: kappa_2
    
    kappa_2 = kappa * kappa
    
    calc_chi = (kappa_2 - 1.0_wp) / (kappa_2 + 1.0_wp)
    
end function calc_chi



!> Returns the Gay-Berne potential given the particles' orientations and
!> vector separation, and the parameters chi_prime, chi and nu
real(kind=wp) function gb_potential(ui, uj, r, epsilon_0, chi_prime, sigma_s, chi, mu, nu)

    use kinds_f90
    
    implicit none

        !> Orientation of 1st atom
    real(kind=wp), dimension(3), intent(in) :: ui

        !> Orientation of 2nd atom
    real(kind=wp), dimension(3), intent(in) :: uj

        !> Separation vector between atoms
    real(kind=wp), dimension(3), intent(in) :: r

        !> Gay-Berne potential parameter epsilon_0
    real(kind=wp), intent(in) :: epsilon_0
    
        !> Gay-Berne potential parameter chi_prime
    real(kind=wp), intent(in) :: chi_prime

        !> Gay-Berne potential parameter sigma_s
    real(kind=wp), intent(in) :: sigma_s
    
        !> Gay-Berne potential parameter chi
    real(kind=wp), intent(in) :: chi
    
        !> Gay-Berne potential parameter mu
    real(kind=wp), intent(in) :: mu
    
        !> Gay-Berne potential parameter nu
    real(kind=wp), intent(in) :: nu
    
    integer :: i
    
    real(kind=wp) :: mod_r, ui_r, uj_r, ui_uj, ui_uj_2   
    
    real(kind=wp) :: chi_ui_uj, chi_prime_ui_uj
    
    real(kind=wp) :: epsilon_1, epsilon_2, epsil, sigma
    
    real(kind=wp) :: a1, a2, b1, b2
    
    real(kind=wp) :: t_1, t_3, t_6, t_12
    
    real(kind=wp) :: energy_ij
    
    real(kind=wp), dimension (3) :: r_norm

    !TU: This is wasteful. The magnitude of r could be passed from vdw_energy
    mod_r=sqrt(dot_product(r,r))
    

    r_norm = r / mod_r
    
    ui_uj=dot_product(ui,uj)
    ui_uj_2= ui_uj*ui_uj
    
    ui_r=dot_product(ui,r_norm)
    uj_r=dot_product(uj,r_norm)
    
    chi_ui_uj = chi * ui_uj
    
    chi_prime_ui_uj = chi_prime * ui_uj
    
    epsilon_1 = 1.0_wp / sqrt(1.0_wp - chi_ui_uj * chi_ui_uj)
        
    a1 = ui_r + uj_r 
        
    a2 = ui_r - uj_r
        
    b1 = a1 * a1
        
    b2 = a2 * a2
        
    epsilon_2 = 1.0_wp - 0.5_wp * chi_prime * ( b1 / (1.0_wp + chi_prime_ui_uj) + b2 / (1.0_wp - chi_prime_ui_uj) )
        
    epsil = epsilon_0 * (epsilon_1 ** nu) * (epsilon_2 ** mu) 
    
    sigma = sigma_s / sqrt(  1.0_wp - 0.5_wp * chi * ( b1 / (1.0_wp + chi_ui_uj) + b2 / (1.0_wp - chi_ui_uj) )  )
        
    t_1 = sigma_s / (mod_r - sigma + sigma_s)
        
    t_3 = t_1 * t_1 * t_1
        
    t_6 = t_3 * t_3
        
    t_12 = t_6 * t_6
        
    energy_ij = 4.0_wp * epsil * (t_12 - t_6)
    
    gb_potential = energy_ij
        
end function gb_potential

    

    
end module gb_potential_module
