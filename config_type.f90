! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module config_type

use kinds_f90
use molecule_type
use thblist_type
use latticevectors_type
use coul_type
use cell_list_type

implicit none

type config

    character*4  coord_type

        !> title of configuration
    character*80         :: cfg_title

        !> number of distinct molecules in basis
    integer              :: num_mols = 0

        !> the total number of atoms
    integer              :: number_of_atoms = 0

        !> the maximum total number of atoms
    integer              :: maxno_of_atoms = 0

        !> the total number of molecules possible in the simulation
    integer              :: mxmol = 0

        !> the number of moving sites for volume displacement
    integer              :: num_vol_sites = 0

    logical   :: newjob

        !> lattice vectors of the config
    type(latticevectors) :: vec

        !> stores recip vector and phase factors for configuration
    type(coul) :: cl

        !> the molecule data
    type(molecule), dimension(:), allocatable :: mols
    type(species),  dimension(:), allocatable :: mtypes

        !> the number of molecules of each type allowed
    integer, dimension(:), allocatable :: mxmol_type

        !> id's of translated molecules 
    !integer, dimension(:), allocatable :: tmol_ids

        !> id's of rotated molecules 
    !integer, dimension(:), allocatable :: rmol_ids

        !> id's of molecules with movable atoms
    !integer, dimension(:), allocatable :: amol_ids

        !> number of each element
    integer, dimension(:), allocatable :: nums_elemts

        !> number of each molecule
    !integer, dimension(:), allocatable :: nums_molecs

        !> list of free points - 0 is free 1 is occupied
    integer, dimension(:), allocatable :: nrcav, cavity, cavids

    real(kind = wp), dimension(:), allocatable :: drcav

        !> cavity counts vs total number of particles
    real(kind = wp), dimension(:,:), allocatable :: pcavity_tot

        !> cavity counts vs number of particles (per element)
    real(kind = wp), dimension(:,:), allocatable :: pcavity_typ

        !> Cavity bias grid (gx,gy,gz)
    real(kind = wp), dimension(:,:), allocatable :: cavgrid


        !> Cell list for configuration
    type(cell_list) :: celllist

end type config

end module
