! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! module for tersoff interactions - based on dl_poly routines 
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module tersoff_module

    use kinds_f90

    implicit None

    integer,                        save :: ntersoff
    Data                                    ntersoff / 0 /

    !> the number of paraterers for tersoff
    integer, parameter :: mxpter = 12

    integer :: mxter, mxcrs

    integer,           allocatable, save :: lstter(:),ltpter(:)

    real(kind = wp) :: dlrter

    real( Kind = wp ), allocatable, save :: prmter(:,:), prmter2(:,:)
    real( Kind = wp ), allocatable, save :: vmbp(:,:,:), gmbp(:,:,:)

contains

subroutine allocate_tersoff_arrays(nter)

    use kinds_f90
    use constants_module, only : MAXMESH, uout

    use species_module, only : number_of_elements

    implicit none

    integer, intent(in) :: nter
    integer, dimension( 1:6 ) :: fail

    fail = 0

    ntersoff = nter

    mxter = number_of_elements
    mxcrs = (mxter * (mxter + 1)) / 2

    allocate (lstter(1:mxter),            stat = fail(1))
    allocate (ltpter(1:mxter),            stat = fail(2))
    allocate (prmter(1:mxter,1:mxpter),   stat = fail(3))
    allocate (prmter2(1:mxcrs,1:mxpter),   stat = fail(4))
    allocate (vmbp(1:MAXMESH,1:mxcrs,1:3), stat = fail(5))
    allocate (gmbp(1:MAXMESH,1:mxcrs,1:3), stat = fail(6))

    if (any(fail > 0)) then

        call error(154)
        
    endif

    lstter = 0
    ltpter = 0

    prmter = 0.0_wp
    prmter2 = 0.0_wp
    gmbp   = 0.0_wp
    vmbp   = 0.0_wp

end subroutine 

subroutine read_tersoff(cutoff, engunit)

    use kinds_f90
    use constants_module, only : uout, ufld
    use parse_module
    use species_module, only : number_of_elements, element, eletype, get_species_type
    use comms_mpi_module, only : idnode

    implicit none

    real(kind = wp), intent(in) :: cutoff, engunit

    integer :: itpter, katom1, katom2, i, j, keypot, jtpatm, nread
    integer :: icross, npairs, typi, typj, keyter, typ1, typ2

    real(kind = wp) :: parpot(mxpter)

    character*8 :: atom1, atom2
    character :: line*100, word*40, keyword*4

    logical :: lter_safe, safe

    safe = .true.
    lter_safe = .true.
    nread = ufld

    if (idnode == 0) write(uout,"(/,/,' nonbonded (tersoff) potentials ',/)")

    do itpter = 1, ntersoff

        parpot = 0.0_wp

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line,word)
        atom1=word
        call get_word(line,word)
        typ1 = get_species_type(word)

        call get_word(line,word)
        call lower_case(word)

        keyword=word

        if (keyword(1:4) == 'ters') then
            
            keypot = 1

        elseif (keyword(1:4) == 'atrs') then

            keypot = 2

        else

            call error(155)

        endif

        ! first line of potential paraterers
        call get_word(line, word)
        parpot(1) = word_2_real(word)  !A_i
        call get_word(line, word)
        parpot(2) = word_2_real(word)  !a_i
        call get_word(line, word)
        parpot(3) = word_2_real(word)  !B_i
        call get_word(line, word)
        parpot(4) = word_2_real(word)  !b_i
        call get_word(line, word)
        parpot(5) = word_2_real(word)  !R_i

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        !second line
        call get_word(line, word)
        parpot(6) = word_2_real(word)  !S_i
        call get_word(line, word)
        parpot(7) = word_2_real(word)  !beta_i
        call get_word(line, word)
        parpot(8) = word_2_real(word)  !eta_i
        call get_word(line, word)
        parpot(9) = word_2_real(word)  !c_i
        call get_word(line, word)
        parpot(10) = word_2_real(word) !d_i
        call get_word(line, word)
        parpot(11) = word_2_real(word) !h_i
        call get_word(line, word)
        parpot(12) = word_2_real(word) !lambda

        if (idnode == 0) write(uout,"(1x,i2,3x,a8,3x,a4,1x,5f15.6)") &
            itpter,atom1,keyword,(parpot(j),j=1,5)
        if (idnode == 0) write(uout,"(20x,6f15.6)") (parpot(j),j=6,mxpter)

        !check atom types exist
        katom1 = 0

        do jtpatm = 1, number_of_elements
           if (atom1 == element(jtpatm) .and. typ1 == eletype(jtpatm)) katom1 = jtpatm
        end do

        if (katom1 == 0) then

           call error(156)
           
        endif

        if (lstter(katom1) /= 0) then

            call error(157)
            
        endif

        lstter(katom1) = itpter
        ltpter(itpter) = keypot

        ! convert energies to internal unit

        parpot(1) = parpot(1) * engunit
        parpot(3) = parpot(3) * engunit

        do i = 1, mxpter
            prmter(itpter,i) = parpot(i)
        enddo

    enddo 

    npairs = (ntersoff * (ntersoff + 1)) / 2

    if (idnode == 0) write(uout,"(/,/,1x,'number of tersoff cross terms',i10)") npairs
    if (idnode == 0) write(uout,"(/,/,16x,'atom    ','atom    ',10x,'paraterers'/,/)")

    do icross = 1, npairs

        parpot = 0.0

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line,word)
        atom1 = word
        call get_word(line,word)
        typ1 = get_species_type(word)

        call get_word(line,word)
        atom2 = word
        call get_word(line,word)
        typ2 = get_species_type(word)

        call get_word(line, word)
        parpot(1) = word_2_real(word)  !chi_ij
        call get_word(line, word)
        parpot(2) = word_2_real(word)  !omega_ij

        !check atom types exist
        katom1 = 0
        katom2 = 0

        do jtpatm = 1, number_of_elements

           if (atom1 == element(jtpatm) .and. typ1 == eletype(jtpatm)) katom1 = jtpatm
           if (atom2 == element(jtpatm) .and. typ1 == eletype(jtpatm)) katom2 = jtpatm

        end do

        if (katom1 == 0 .or. katom2 == 0) then

           call error(158)
           
        endif

        typi = max(katom1,katom2)
        typj = min(katom1,katom2)

        keyter = (typi * (typi - 1)) / 2 + typj

        if (keyter > mxcrs) then

            call error(159)
            
        endif

        prmter2(keyter,1) = parpot(1)
        prmter2(keyter,2) = parpot(2)

        if (idnode == 0) write(uout,"(1x,i10,4x,2a8,2x,2f15.6)") icross, atom1, atom2, (parpot(j),j=1,2)

    enddo

    if (ntersoff /= 0) call tersoff_generate(cutoff)

    return

1000 continue
    call error(160)

end subroutine

subroutine tersoff_generate(cutoff)

    use kinds_f90
    use species_module, only : number_of_elements
    use constants_module, only : uout, MAXMESH, PI

    implicit none

    real(kind = wp), intent(in) :: cutoff

    integer :: i, katom1, katom2, ipt, jpt, kpt
    real( Kind = wp ) :: dlrpot, baij, saij, bbij, sbij, rij, sij, rrr, arg, rep, att

    ! define grid resolution for potential arrays

    dlrter = cutoff / real(MAXMESH-4, wp)

    ! construct arrays for all types of tersoff potential

    do katom1 = 1, ntersoff

        do katom2 = 1, katom1

            if (ltpter(katom1) == 1 .and. ltpter(katom2) == 1) then

                ipt=lstter(katom1)
                jpt=lstter(katom2)

                kpt=(max(ipt,jpt) * (max(ipt,jpt) - 1)) / 2 + min(ipt,jpt)

                ! define tersoff parameters

                baij =    sqrt(prmter(ipt,1) * prmter(jpt,1))
                saij = 0.5_wp*(prmter(ipt,2) + prmter(jpt,2))
                bbij =    sqrt(prmter(ipt,3) * prmter(jpt,3))
                sbij = 0.5_wp*(prmter(ipt,4) + prmter(jpt,4))
                rij  =    sqrt(prmter(ipt,5) * prmter(jpt,5))
                sij  =    sqrt(prmter(ipt,6) * prmter(jpt,6))

                ! store potential cutoff

                vmbp(1,kpt,1) = sij


                do i = 2, MAXMESH
                    rrr = real(i,wp) * dlrter

                    if (rrr <= rij) then

                        vmbp(i,kpt,1) = 1.0_wp
                        gmbp(i,kpt,1) = 0.0_wp

                    else if (rrr <= sij) then

                        arg = PI * (rrr - rij) / ( sij - rij)

                        vmbp(i,kpt,1) = 0.5_wp * (1.0_wp + cos(arg))
                        gmbp(i,kpt,1) = 0.5_wp * PI * rrr * sin(arg) / (sij - rij)

                    endif

                enddo

                ! calculate screening repulsion & attraction functions
                do i = 2, MAXMESH
                    rrr = real(i,wp) * dlrter

                    !first the repulsive part
                    rep = baij * exp(-saij * rrr)

                    vmbp(i,kpt,2) = rep * vmbp(i,kpt,1)
                    gmbp(i,kpt,2) = rep * (gmbp(i,kpt,1) + saij * rrr * vmbp(i,kpt,1))

                enddo

                do i = 2, MAXMESH

                    rrr = real(i,wp) * dlrter
                    !attractive part
                    att = bbij * exp(-sbij * rrr)

                    vmbp(i,kpt,3) = att * vmbp(i,kpt,1)
                    gmbp(i,kpt,3) = att * (gmbp(i,kpt,1) + sbij * rrr * vmbp(i,kpt,1))

                enddo

            endif

        enddo

    enddo

end subroutine

subroutine tersoff_two(ai, aj, r, eat, erp, esc, gat, grp, gsc)

    use kinds_f90

    implicit none

    integer, intent(in) :: ai, aj
    real(kind = wp), intent(in) :: r
    real(kind = wp), intent(out) :: eat, erp, esc, gat, grp, gsc

    integer :: k0, l, iter, jter
    real(kind = wp) :: ab, vk0, vk1, vk2, t1, t2, ppp, rdr
    real(kind = wp) :: gk0, gk1, gk2

    eat = 0.0_wp
    erp = 0.0_wp
    esc = 0.0_wp
    gat = 0.0_wp
    grp = 0.0_wp
    gsc = 0.0_wp

!   if(ai > aj) then
!       ab = ai * (ai - 1.0) * 0.5 + aj + 0.5
!   else
!       ab = aj * (aj - 1.0) * 0.5 + ai + 0.5
!   endif

    !ab=(max(aj,ai) * (max(aj,ai) - 1)) / 2 + min(aj,ai)
    ab=(max(ai,aj) * (max(ai,aj) - 1)) / 2 + min(ai,aj)

    iter = lstter(ai)
    jter = lstter(aj)
    k0=(max(iter,jter) * (max(iter,jter) - 1)) / 2 + min(iter,jter)

    if(k0 >= 1) then

        if(r <= vmbp(1,k0,1)) then
!       if((ltpter(k0) >= 1) .and. r <= vmbp(1,k0,1)) then

            ! interpolation paraterers

            rdr = 1.0_wp / dlrter
            l = int(r * rdr)
            ppp = r * rdr - real(l,wp)

            ! interpolate screening function

            vk0 = vmbp(l,  k0,1)
            vk1 = vmbp(l+1,k0,1)
            vk2 = vmbp(l+2,k0,1)

            t1 = vk0 + (vk1 - vk0) * ppp
            t2 = vk1 + (vk2 - vk1) * (ppp - 1.0_wp)

            esc = t1 + (t2 - t1) * ppp * 0.5_wp

            ! interpolate derivative of screening function
            gk0 = gmbp(l,  k0,1)
            gk1 = gmbp(l+1,k0,1)
            gk2 = gmbp(l+2,k0,1)

            t1 = gk0 + (gk1 - gk0) * ppp
            t2 = gk1 + (gk2 - gk1) * (ppp - 1.0_wp)

            gsc = -(t1 + (t2 - t1) * ppp * 0.5_wp) / r

            ! interpolate repulsiv function

            vk0 = vmbp(l,  k0,2)
            vk1 = vmbp(l+1,k0,2)
            vk2 = vmbp(l+2,k0,2)

            t1 = vk0 + (vk1 - vk0) * ppp
            t2 = vk1 + (vk2 - vk1) * (ppp - 1.0_wp)

            erp = t1 + (t2 - t1) * ppp * 0.5_wp

            ! interpolate derivative of screening function
            gk0 = gmbp(l,  k0,2)
            gk1 = gmbp(l+1,k0,2)
            gk2 = gmbp(l+2,k0,2)

            t1 = gk0 + (gk1 - gk0) * ppp
            t2 = gk1 + (gk2 - gk1) * (ppp - 1.0_wp)

            grp = -(t1 + (t2 - t1) * ppp * 0.5_wp) / r

            ! interpolate attractive function

            vk0 = vmbp(l,  k0,3)
            vk1 = vmbp(l+1,k0,3)
            vk2 = vmbp(l+2,k0,3)

            t1 = vk0 + (vk1 - vk0) * ppp
            t2 = vk1 + (vk2 - vk1) * (ppp - 1.0_wp)

            eat = t1 + (t2 - t1) * ppp * 0.5_wp

            ! interpolate attractive of screening function
            gk0 = gmbp(l,  k0,3)
            gk1 = gmbp(l+1,k0,3)
            gk2 = gmbp(l+2,k0,3)

            t1 = gk0 + (gk1 - gk0) * ppp
            t2 = gk1 + (gk2 - gk1) * (ppp - 1.0_wp)

            gat = -(t1 + (t2 - t1) * ppp * 0.5_wp) / r
        endif

    endif

end subroutine

integer function get_potential(typ) result(iter)

    implicit none

    integer, intent(in) :: typ

    iter = lstter(typ)

end function

real(kind = wp) function evaluate_gtheta(costh, iter) result (gtheta)

    use kinds_f90
    implicit none

    integer, intent(in) :: iter
    real(kind = wp), intent(in) :: costh

    real(kind = wp) :: tmp, ci, di, hi

    gtheta = 0.0_wp

    ci = prmter(iter,9)
    di = prmter(iter,10)
    hi = prmter(iter,11)

    gtheta = 1.0_wp + (ci/di)**2 - ci**2 / (di**2 + (hi - costh)**2)

end function

real(kind = wp) function evaluate_omega(rij, rik, iter) result (omega)

    use kinds_f90
    implicit none

    integer, intent(in) :: iter
    real(kind = wp), intent(in) :: rij, rik

    integer :: key
    real(kind = wp) :: lam, arg

    key = ltpter(iter)
    omega = 0.0_wp


    select case (key)

        case (1)

            omega =  prmter2(iter,2)

        case (2)

            lam = prmter2(iter,2)**3
            arg = (rij - rik)**3
   
            omega = exp(arg * lam)

        case default
            
            call error(0)

    end select

end function

real(kind = wp) function evaluate_b(zeta_ij, chi_ij, iter) result(b_ij)

    use kinds_f90
    implicit none

    integer, intent(in) :: iter
    real(kind = wp), intent(in) :: zeta_ij, chi_ij

    integer :: key
    real(kind = wp) :: arg, arg1

    key = ltpter(iter)
    b_ij = 0.0_wp
    
!   arg = -1.0 / (2.0 * m_param8);
!   arg1 = m_param7 * pow(zeta_ij, m_param8);
!   b_ij = pow((1.0 + arg1), arg);

!   !the pure tersoff is multiplied by chi "mixing term"
!   select case (key)

!       case (1)

!           b_ij = m_chi_ij * b_ij

!   end select
    
end function

end module
