! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module inversion_module

    use kinds_f90

    implicit none

    integer ::    numinv

        !> the inversion potetial parameters
    real(kind = wp), allocatable, dimension(:,:) :: prminv
    integer, allocatable, dimension(:) :: ltpinv

contains

subroutine alloc_invpar_arrays(npar)

    use kinds_f90
    use constants_module, only : uout

    implicit none

    integer, intent(in) :: npar

    integer :: fail(2)

    fail = 0
    numinv = npar

    allocate(prminv(npar, 3), stat = fail(1))
    allocate(ltpinv(npar), stat = fail(2))

    if(any(fail > 0)) then

        call error(128)

    endif

end subroutine alloc_invpar_arrays


!reads in, prints out, and convets to internal units inversion potentisla

subroutine read_inversion_par(npar, unit)

    use kinds_f90
    use constants_module, only : uout, ufld, TORADIANS
    use parse_module
    use comms_mpi_module, only : master,idnode

    implicit none

    integer, intent(in) :: npar

    integer :: nread
    real(kind = wp), intent(in) :: unit

    integer :: i

    character :: line*100, word*40

    logical safe

    safe = .true.

    nread = ufld

    !if( master ) write(uout,"(/,/,' inversion potentials ',/)")

    if( master ) write(uout,"(/,/,24('-'),/,' inversion potentials: ',/,24('-'))")

    do i = 1, npar

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line, word)
        if( word == 'harm' .or. word == '-harm' ) then !harmonic inversion potential

            ltpinv(i) = 1
            if( master ) write(uout,"(1x,'harmonic inversion potential')")

        else if( word == 'hcos' .or. word == '-hcos' ) then !harmonic cosine inversion potential

            ltpinv(i) = 2
            if( master ) write(uout,"(1x,'harmonic cosine inversion potential')")

        else if( word == 'plan' .or. word == '-plan' ) then !planar inversion potentials

            ltpinv(i) = 3
            if( master ) write(uout,"(1x,'planar inversion potential')")

        else if( word == 'xpln' .or. word == '-xpln' ) then !extended planar inversion potentials

            ltpinv(i) = 4
            if( master ) write(uout,"(1x,'extended planar inversion potential')")

        else 

            call error(172)

        endif

        if( word(1:1) == '-' ) ltpinv(i) = -ltpinv(i)

        call get_word(line, word)
        prminv(i,1) = word_2_real(word)
        call get_word(line, word)
        prminv(i,2) = word_2_real(word)

        if( master ) write(uout,"(1x,'force constant ', f6.3, ' zero ', f10.4)")prminv(i,:)

        prminv(i,1) =  prminv(i,1) * unit

        if (ltpinv(i) == 2) prminv(i,2) = cos(prminv(i,2) * TORADIANS)

    enddo

    return

1000 call error(130)

end subroutine

!> calculates the energy for an inversion
subroutine calc_inversion_energy(pot, cosb, cosc, cosd, eng)

    use kinds_f90

    integer, intent(in) :: pot
    real(kind = wp), intent(in) :: cosb, cosc, cosd
    real(kind = wp), intent(out) :: eng

    real(kind = wp) :: fc, cz, thb, thc, thd, m

    integer :: typ
    
    typ = abs(ltpinv(pot))

    eng = 0.0_wp

    if(typ == 1) then

        fc = prminv(pot, 1)
        cz = prminv(pot, 2)
        thb = acos(cosb)
        thc = acos(cosc)
        thd = acos(cosd)

        eng = 0.5_wp * fc * ((thb - cz)**2 + (thc - cz)**2 + (thd - cz)**2) / 3.0_wp

    else if (typ == 2) then

        fc = prminv(pot, 1)
        cz = prminv(pot, 2)

        eng = 0.5_wp * fc * ((cosb - cz)**2 + (cosc - cz)**2 + (cosb - cz)**2) / 3.0_wp

    else if (typ == 3) then

        fc = prminv(pot, 1)

        eng = fc * ((1.0_wp - cosb) + (1.0_wp - cosc) + (1.0_wp - cosd)) / 3.0_wp 

    else if (typ == 4) then

        fc = prminv(pot, 1)
        cz = prminv(pot, 2)
        m = prminv(pot, 3)

        eng = 3.0_wp * fc * (1.0_wp - (cos(m * thb - cz) + cos(m * thc - cz) + cos(m * thd - cz)) / 3.0_wp)


    else

        call error(173)

    endif


end subroutine

end module
