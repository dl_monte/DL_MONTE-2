! ***************************************************************************
! *   Copyright (C) 2015 by A.V.Brukhno                                     *
! *   andrey.brukhno[@]stfc.ac.uk                                           *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Everything related to order parameter calculations: groups, COM:s, structure, aggregates etc
!> @usage 
!> - most of the control is done via `<fed_interface_type>` instance `'fed'` normally
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `fed_calculus_module`
!> - `fed_interface_type` & `arrays_module` (via `fed_calculus_module`)

!> @modulefor order parameter calculus: groups, COM:s, structure, aggregates etc

module fed_order_module

    use kinds_f90
    use constants_module
    use fed_calculus_module

    !AB: the following comes with the use of fed_order_module
    !use fed_interface_type
    !use arrays_module 
    !use comms_mpi_module, only : master
    !use parallel_loop_module, only : idgrp

    implicit none


    !AB: *** FED CALCULUS SCOPE *** - all the commented-out variables
    ! below are found in fed_calculus_module.f90


        ! FED control structure
    !type(fed_interface),save :: fed

        ! FED names container
    !type(fed_interface_name),save :: fed_name

        ! generic FED order parameter vector
    !real(kind=wp), allocatable, save :: parm_vec(:)

        ! generic FED histogram vector
    !real(kind=wp), allocatable, save :: hist_vec(:)

        ! generic FED bias vector
    !real(kind=wp), allocatable, save :: bias_vec(:)

        ! numbers of attempted & accepted FED moves (for each box
    ! /config)
    !integer, dimension(:), allocatable :: num_attempted_fedmoves,
    ! num_accepted_fedmoves

        ! previous & current order parameter values
    !real(kind=wp), save :: param_old, param_cur

        ! previous and current ID/index of (sub)state in FED sampling
    !integer, save :: id_old, id_cur

        ! running numbers of attempted & accepted FED moves
    !integer, save :: num_total_fedmoves, num_successful_fedmoves


    !AB: *** LOCAL SCOPE *** - all local variables go below


    real(kind=wp), save :: coms_xyz(3)

        !> molecule indices for complex order parameters
    integer, allocatable, save :: grp_mol_ids(:,:)

        !> atom indices (within molecule) for complex order parameters
    integer, allocatable, save :: grp_atm_ids(:,:)

        !> matrix to store molecule(s)/cluster(s) COM:s for complex
    ! order parameters
    real(kind=wp), allocatable, save :: coms_pos(:,:,:)

    !real(kind=wp), allocatable, save :: coms_xyz(:)

    !AB: the allocation of the above matrix must result in this sort
    ! of array:
    !real(kind=wp), dimension(0:3, 0:ngrp_ids, 1:nconfig)  :: coms_pos

        !> numbers of entries read from FED input for molecules &
    ! atoms in groups/COM:s specs
    integer, save :: grp_nmols, grp_natms, ngrp_ids

        !> flags to indicate if molecule/atom indices were found in
    ! the input (CONTROL)
    logical, save :: is_mol_com, is_atm_com


contains


!> checks for consistency of FED input w.r.t. order parameter initial
  ! value(s) within the specified range
!> Not suitable for PSMC order parameters, see 'fed_param_check_psmc'.
subroutine fed_param_check(job, nconfs)

    use constants_module
    use control_type
    use config_type
    use comms_mpi_module, only : master, is_parallel, mxnode, gsum_world
    use parallel_loop_module, only : wkgrp_size !, idgrp
    use psmc_module, only : initialise_psmc

        !> control structure for the job (various flags and switches,
    ! see `control_type.f90`)
    type(control), intent(inout) :: job

        !> the id/index of the configuration box
    integer, intent(in) :: nconfs

    integer :: fed_pkind

    fed_pkind = fed%par_kind
    if( is_parallel ) then

        call gsum_world(fed_pkind)
        fed_pkind = fed_pkind/mxnode

    end if

    if( job % fedcalc ) then

        if( fed_pkind /= fed%par_kind ) &
            call cry(uout,'', &
                 "ERROR: type of FED parameter 'fed%par_kind' is inconsistent between worker processes!!!",999)

        if( fed%flavor == FED_PS ) then

            call initialise_psmc(job)

            return
        endif

    else if( fed_pkind /= FED_PAR_NONE ) then

        call cry(uout,'', &
                 "WARNING: 'use fed ...' directive is absent in CONTROL but fed%par_kind =/= 0 - resetting!!!",0)

        fed%par_kind = FED_PAR_NONE

        return
    else

        if( master ) then
            write(uout,"(/,/,1x,50('-'))")
            write(uout,"(1x,a,i2,a)")&
                 "NOTE: FED calculation is OFF; order parameter = ",fed%par_kind," (should be zero)"
        endif

        return
    endif 

    !write(uout,*)

    ! check the parameter specs according to its kind
    select case ( fed % par_kind )

        case(FED_PAR_TEMP)
            ! T-variation (multicanonical) - can be set or picked up
           !  from a range

            call fed_check_temp(job)

        case(FED_PAR_BETA)
            ! T-variation (multicanonical) - can be set or picked up
           !  from a range

            call fed_check_temp(job)

        case(FED_PAR_LAMB)
            ! coupling strength variation - can be set or picked up
           !  from a range

            !if( master) write(uout,*)"The case of FED parameter
           ! Kirkwood 'Lambda-coupling' has to be added yet..."

            call fed_check_grp_ids(job, nconfs)

            call fed_check_range(job, nconfs)

            call cry(uout,'', &
                     "ERROR: FED order parameter Kirkwood 'Lambda-coupling' is unsupported yet!!!",51)

            !call cry(uout,'', &
            !         "*WARNING*: FED order parameter Kirkwood 'Lambda-coupling' is still experimental...",0)

        case(FED_PAR_VOLM)
            ! V-variation - must be known already

            !if( master) write(uout,*)"The case of FED parameter 'Volume' has to be added yet..."

            call fed_check_range(job, nconfs)

            !call cry(uout,'', &
            !         "ERROR: FED order parameter 'Volume' is unsupported yet!!!",51)

        case(FED_PAR_DENS)
            ! rho-variation - must be known already

            !if( master) write(uout,*)"The case of FED parameter 'Density' has to be added yet..."

            call fed_check_range(job, nconfs)

            call cry(uout,'', &
                     "ERROR: FED order parameter 'Density' is unsupported yet!!!",51)

        case(FED_PAR_DIST1)
            ! COM distance variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'COM distance 1' has to be added yet..."

            call cry(uout,'', &
                     "ERROR: FED order parameter 'Com1 - one COM distance' is unsupported yet!!!",51)

        case(FED_PAR_DIST2)
            ! R(com)-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'two COM:s distance' is still experimental..."

            call fed_check_grp_ids(job, nconfs)

            call cry(uout,'', &
                     "*WARNING*: FED order parameter 'Com2 - two COM:s distance' is still experimental...",0)

        case(FED_PAR_ORDR1)
            ! Structure-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'Structure/Order 1' has to be added yet..."

            call cry(uout,'', &
                     "ERROR: FED order parameter 'Structure 1' is unsupported yet!!!",51)

        case(FED_PAR_ORDR2)
            ! R(com)-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'Structure/Order 2' has to be added yet..."

            call cry(uout,'', &
                     "ERROR: FED order parameter 'Structure 2' is unsupported yet!!!",51)

        case(FED_PAR_NMOLS)
            ! Total number of molecules in the simulation box - must be known already

            !call cry(uout,'', &
            !         "ERROR: FED order parameter 'Total number of molecules' is unsupported yet!!!",51)
            
            call fed_check_range(job, nconfs)            
            
        case(FED_PAR_NMOLS_SPEC)
            ! Number of molecules of a given species
            
            call fed_check_range(job, nconfs)            

        case(FED_PAR_NATOMS)
            ! Total number of atoms in the simulation box - must be known already
            
            call fed_check_range(job, nconfs)            

        case(FED_PAR_NATOMS_SPEC)
            ! Number of atoms of a given species

            !call cry(uout,'', &
            !         "ERROR: FED order parameter 'Total number of molecules' is unsupported yet!!!",51)
            
            call fed_check_range(job, nconfs)

        case(FED_PAR_LCS2, FED_PAR_LCS4)

            ! Liquid crystal scalar order parameters - 2nd and 4th order Legendre polynomial

            call fed_check_range(job, nconfs)

        case(FED_PAR_LAMBDA)

            ! Hamiltonian coupling parameter lambda
            call fed_check_range(job, nconfs)


        case default
            !AB: do nothing as defualt

            call cry(uout,'', &
                 "ERROR: unknown order parameter type for FED 'fed%par_kind'!!!",999)

            continue

    end select

end subroutine fed_param_check


!> Checks that a pair of boxes with the specified energies is within the allowed order parameter range
!> - for PSMC order parameters. Flags an error if not. This function also (via 'fed_check_value') 
!> sets the current state number (id_cur) and order parameter value (param_cur) to that 
!> corresponding to the arguments.
subroutine fed_param_check_psmc(energy1, energy2)

        !> The energy of phase 1
    real(kind=wp), intent(in) :: energy1
    
        !> The energy of phase 2
    real(kind=wp), intent(in) :: energy2

    real(kind=wp) :: param

    param = fed_param_psmc(energy1, energy2)

    !TU: Check whether the value is witin the range; flag error if not;
    !TU: set current state index and order parameter value if so.
    call fed_check_value(param)

end subroutine fed_param_check_psmc


!> returns the value (and possibly id/index) of the FED order parameter.
!> Not suitable for PSMC order parameters, see 'fed_param_dbias_psmc'.
real(kind=wp) function fed_param_value(id, ib, cfg, is_new)

    use constants_module
    use config_type
    use latticevectors_module, only : cellsize
    use comms_mpi_module, only : master
    use lc_order_module, only : scalar_nematic_op
    use lambda_module, only : lambda

    !use parallel_loop_module, only : idgrp

        !> the id/index of the (sub)state in FED calculation \n
        !> can be either `in` or `out` in meaning and usage
    integer, intent(inout) :: id

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the configurations/boxes from input
    type(config), intent(inout) :: cfg

    logical, intent(in) :: is_new

    integer :: k
    
    ! the parameter value is fetched according to its kind
    select case ( fed % par_kind )

        case(FED_PAR_TEMP)
            ! coupling strength variation - can be set or picked up from a range

            !if( master) write(uout,*)"The case of FED parameter Kirkwood 'Lambda-coupling' has to be added yet..."

            fed_param_value = parm_vec(id)

        case(FED_PAR_LAMB)
            ! coupling strength variation - can be set or picked up from a range

            !if( master) write(uout,*)"The case of FED parameter Kirkwood 'Lambda-coupling' has to be added yet..."

            fed_param_value = parm_vec(id)

        case(FED_PAR_VOLM)
            ! V-variation - must be known already

            !if( master) write(uout,*)"The case of FED parameter 'Volume' has to be added yet..."

            !call cellsize(cfg%vec, fed_param_value)

            fed_param_value = cfg%vec%volume
            id = fed_param_id(fed_param_value)

        case(FED_PAR_DENS)
            ! rho-variation - must be known already
            !AB: to be amended for the case of species density

            !if( master) write(uout,*)"The case of FED parameter 'Density' has to be added yet..."

            fed_param_value = 1.0_wp/cfg%vec%volume
            id = fed_param_id(fed_param_value)

        case(FED_PAR_DIST1)
            ! COM distance variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'COM distance 1' has to be added yet..."

            !fed_param_value = com_dist1
            !id = ...

        case(FED_PAR_DIST2)
            ! R(com)-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'COM distance 2' has to be added yet..."

            fed_param_value = fed_param_com_dist2(id,ib,is_new)

        case(FED_PAR_ORDR1)
            ! Structure-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'Structure/Order 1' has to be added yet..."

            !fed_param_value = order1
            !id = ...

        case(FED_PAR_ORDR2)
            ! R(com)-variation - needs re-calculation

            !if( master) write(uout,*)"The case of FED parameter 'Structure/Order 2' has to be added yet..."

            !fed_param_value = order2
            !id = ...

        case(FED_PAR_NMOLS)
            ! Total number of molecules in the simulation box - must be known already

            fed_param_value = 0.0_wp

            do k = 1, size(cfg%mtypes)

                fed_param_value = fed_param_value + cfg%mtypes(k)%num_mols
                
            end do
            
            id = fed_param_id(fed_param_value)

        case(FED_PAR_NMOLS_SPEC)
            ! Number of molecules of a given species

            fed_param_value = cfg%mtypes( fed%par_nmols_spec_label )%num_mols

            id = fed_param_id(fed_param_value)

        case(FED_PAR_NATOMS)
            ! Total number of atoms

            fed_param_value = 1.0_wp * cfg%number_of_atoms

            id = fed_param_id(fed_param_value)

        case(FED_PAR_NATOMS_SPEC)
            ! Number of atoms of a given species

            fed_param_value = 0.0_wp
            
            do k = 1, size(cfg%mtypes)
            
                fed_param_value = fed_param_value + cfg%mtypes(k)%num_elems( fed%par_natoms_spec_label )
                
            end do
            
            id = fed_param_id(fed_param_value)

        case(FED_PAR_LCS2)

            ! Liquid crystal scalar order parameter - 2nd order Legendre polynomial

            fed_param_value = scalar_nematic_op(ib, 2)
            id = fed_param_id(fed_param_value)           

        case(FED_PAR_LCS4)

            ! Liquid crystal scalar order parameter - 4th order Legendre polynomial

            fed_param_value = scalar_nematic_op(ib, 4)
            id = fed_param_id(fed_param_value)           

        case(FED_PAR_LAMBDA)

            ! Hamiltonian coupling parameter

            fed_param_value = lambda
            id = fed_param_id(fed_param_value)

        case default
            !AB: get the order parameter value from a preset vector

            fed_param_value = parm_vec(id)

    end select

end function fed_param_value



!> returns the id/index for a value within the FED parameter vector (i.e. on 1D grid)
integer function fed_param_id(par_val)

        !> the parameter value to get id/index for
    real(kind=wp), intent(in) :: par_val

    fed_param_id = nint( ((par_val - fed%par_min) / fed%par_inc) - HALF )

    if( fed%inc_kind > 1 ) fed_param_id = nint( fed_param_id**(1.0_wp/fed%inc_kind) )

    fed_param_id = fed_param_id+1

    !TU: If 'soft edges' are active then we allow order parameters outside par_min and par_max. If the order parameter
    !TU: is below par_min then we set fed_param_id to 1 (i.e. we assign the configuration to the lowest order parameter
    !TU: bin); if it is above par_max then we set fed_param_id to fed%numstates (which is the number of bins)
    if( fed % soft_edges ) then

        if( fed_param_id < 1 ) then

            fed_param_id = 1

        else if( fed_param_id > fed%numstates ) then

            fed_param_id = fed%numstates

        end if

    end if

end function fed_param_id


!> checks if a given parameter value (and, optionally, id/index) is within the preset FED parameter range
logical function is_fed_param_ok(par_val, par_id)

        !> the parameter value to check if it is within the preset range
    real(kind=wp), intent(in) :: par_val

        !> the parameter index to check if it is within the preset range
    integer, intent(in), optional :: par_id

    is_fed_param_ok = ( par_val >= fed%par_min .and. par_val <= fed%par_max )

    if( present(par_id) ) is_fed_param_ok = ( is_fed_param_ok .and. is_fed_param_id_ok(par_id) )

end function is_fed_param_ok


!> checks if a given parameter id/index falls within the preset FED parameter range.
!> For 'soft edges' this should in effect is always true, since the id should never be outside
!> the range 1 to fed%numstates, even if the parameter itself is outside the FED parameter range
logical function is_fed_param_id_ok(par_id)

        !> the parameter index to check if it is within the preset range
    integer, intent(in), optional :: par_id

    is_fed_param_id_ok = ( par_id > 0 .and. par_id < fed%numstates+1 )

end function is_fed_param_id_ok


!TU: Added by me...
!> Determines whether the currently proposed move (where 'param_cur' is the trial order parameter and 'param_old'
!> is the 'current' order parameter) should be rejected so that the system either remains within
!> the order parameter window, or does not move further away from the window. The function returns true if the
!> move should be rejected.
logical function fed_window_reject()

    fed_window_reject = .false.

    if( param_old < fed%window_min ) then

        ! We are below the window

        ! Reject if we would move away from the window
        if( param_cur < param_old ) fed_window_reject = .true.

    else if( param_old < fed%window_max ) then

        ! We are within the window

        ! Reject if we would leave the window
        if( param_cur < fed%window_min .or. param_cur > fed%window_max ) &
             fed_window_reject = .true.

    else

        ! We are above the window

        ! Reject if we would move away from the window
        if( param_cur > param_old ) fed_window_reject = .true.
       
    end if

end function fed_window_reject

!> calculates the FED bias difference between new and old (sub)states \n
!> in a general case where paramer values are known (perhaps calculated separately).
!TU:
!> Note that par1 is the order parameter for the old state and par2 is the order
!> parameter for the new state
real(kind=wp) function fed_param_dbias2(par1, par2, is_update)

        !> the parameter value to check if it is within the preset range
    real(kind=wp), intent(in) :: par1, par2

    logical, intent(in) :: is_update

    integer :: id1, id2

    fed_param_dbias2 = 0.0_wp

    id1 = fed_param_id(par1)
    id2 = fed_param_id(par2)

    if( id2 < 1 .or. id2 > fed%numstates ) then
    !AB: infinite walls around the defined range

        fed_param_dbias2 = 1.0e10_wp

    else if( fed % method == FED_US .and. .not.fed%is_bias_input ) then

        fed_param_dbias2 = fed_ums_delta(par1, par2)

    else !if( fed % method /= FED_US .or. fed%is_bias_input ) then

        fed_param_dbias2 = fed_bias_delta(id1, id2)

    end if

    if( fed%par_corr == 1 .and. abs(par1*par2) > 1.0e-10_wp ) then
    !AB: correct for the entropy on the spherical surface area

        fed_param_dbias2 = fed_param_dbias2 + 2.0_wp*log(par2/par1)

    endif

    if( is_update ) then 

        !TU: I added updating of id_old and param_old here, which was omitted and was
        !TU: causing problems in GCMC - where I call this procedure. I assume not updating
        !TU: these variables here was a bug and not deliberate...
        id_old = id_cur
        param_old = param_cur

        id_cur    = id2
        param_cur = par2

    end if

end function fed_param_dbias2


!> calculates the FED bias difference between new and old (sub)states \n
!> in the case of naturally evolving order parameters, i.e. those unknown aforehand.
!> Not suitable for PSMC order parameters, see 'fed_param_dbias_psmc'.
real(kind=wp) function fed_param_dbias(ib, cfg)

    use constants_module, only : FED_PAR_DIST1, FED_PAR_DIST2
    use config_type

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the active configuration box to work with
    type(config), intent(inout) :: cfg
    
    fed_param_dbias = 0.0_wp

    id_old    = id_cur
    param_old = param_cur

    !param_old = fed_param_value(id_old, ib, cfg, .false.)
    param_cur = fed_param_value(id_cur, ib, cfg, .true.)

    if( id_cur < 1 .or. id_cur > fed%numstates ) then ! .or. fed_window_reject() ) then
    !AB: infinite walls around the defined range

        fed_param_dbias = 1.0e10_wp
        return

    else if( fed % method == FED_US .and. .not.fed%is_bias_input ) then

        fed_param_dbias = fed_ums_delta(param_old, param_cur)

    else !if( fed % method /= FED_US .or. fed%is_bias_input ) then

        fed_param_dbias = fed_bias_delta(id_old, id_cur)

    end if

    if( fed%par_corr == 1 ) then
    !AB: correct for the entropy on the spherical surface area

        fed_param_dbias = fed_param_dbias+2.0_wp*log(param_cur/param_old)

    endif

end function fed_param_dbias


!> Calculates the FED bias difference between new and old (sub)states for PSMC order parameters,
!> as well as updating the current and old state number and order parameter values to correspond
!> to the 'new' state.
real(kind=wp) function fed_param_dbias_psmc(energy1, energy2)

        !> The energy of phase 1
    real(kind=wp), intent(in) :: energy1
    
        !> The energy of phase 2
    real(kind=wp), intent(in) :: energy2

    fed_param_dbias_psmc = 0.0_wp

    ! Set the current state number and order parameter value to be the 'old' values
    id_old = id_cur
    param_old = param_cur

    ! Calculate the new state number and order parameter value
    param_cur = fed_param_psmc(energy1, energy2)
    id_cur = fed_param_id(param_cur)

    if( id_cur < 1 .or. id_cur > fed%numstates ) then
        
        ! Ensure rejection if the new state is 'out of bounds' by setting a huge bias.
        fed_param_dbias_psmc = 1.0e10_wp

    else

        fed_param_dbias_psmc = fed_bias_delta(id_old, id_cur)

    end if

end function fed_param_dbias_psmc


!> updates the FED counters & history vector 
subroutine fed_param_hist_inc(ib, is_accepted)

    use constants_module, only : FED_WL, FED_PAR_DIST2

    integer, intent(in) :: ib

    logical, intent(in) :: is_accepted

    real(kind=wp) :: rrr, dvol, bin_scale

    num_attempted_fedmoves(ib) = num_attempted_fedmoves(ib) + 1
    num_total_fedmoves = num_total_fedmoves + 1

    if( is_accepted ) then
    !AB: increment the acceptance counters

        num_accepted_fedmoves(ib) = num_accepted_fedmoves(ib) + 1
        num_successful_fedmoves   = num_successful_fedmoves + 1

    else
    !AB: revert the parameter change

        id_cur    = id_old
        param_cur = param_old

    end if

    call fed_hist_inc(id_cur)

end subroutine fed_param_hist_inc



!TU-: The following procedure is never used, and could do with being removed

!TU: Added by me...
!> updates the FED counters & history vector without altering the old and current
!> values of the order parameter
subroutine fed_param_hist_inc2(ib, is_accepted)

    use constants_module, only : FED_WL, FED_PAR_DIST2

    integer, intent(in) :: ib

    logical, intent(in) :: is_accepted

    real(kind=wp) :: rrr, dvol, bin_scale

    num_attempted_fedmoves(ib) = num_attempted_fedmoves(ib) + 1
    num_total_fedmoves = num_total_fedmoves + 1

    if( is_accepted ) then
    !AB: increment the acceptance counters

        num_accepted_fedmoves(ib) = num_accepted_fedmoves(ib) + 1
        num_successful_fedmoves   = num_successful_fedmoves + 1

    end if

    call fed_hist_inc(id_cur)

end subroutine fed_param_hist_inc2



!> Returns the PSMC order parameter value given the energies of both phases.
!TU: Currently this function is trivial, but in the future it may be turned into
!TU: something more complicated.
!TU: This function implicitly assumes that a PSMC order parameter is in use.
real(kind=wp) function fed_param_psmc(energy1, energy2)

    use vdw_module, only : HS_ENERGY

       !> The energy of phase 1
    real(kind=wp), intent(in) :: energy1
    
        !> The energy of phase 2
    real(kind=wp), intent(in) :: energy2

    fed_param_psmc = energy1 - energy2

    !TU: For the hard-sphere order parameter divide by the energy cost of a hard-sphere
    !TU: overlap to recover the number of overlaps in phase 1 minus the number of
    !TU: overlaps in phase 2
    if( fed % par_kind == FED_PAR_PSMC_HS ) then
       
        fed_param_psmc = fed_param_psmc / HS_ENERGY

    end if

end function fed_param_psmc


!> adds up a displacement `dcom` to the collective COM specified by its id/index `ic`
subroutine fed_upd_com_by(ib, ic, dcom, weight)

    use constants_module, only : uout
    use cell_module, only : pbc_cart_vec_calc

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the id/index of the COM to update
    integer, intent(in) :: ic

        !> the added displacement vector (due to atom or molecule movement)
    real(kind=wp), intent(in) :: dcom(3)

    real(kind=wp), intent(in) :: weight

    !write(uout,*)ib,ic,dcom,weight
    !flush(uout)

    coms_pos(1:3,ic,ib) = coms_pos(1:3,ic,ib) + dcom(:) * weight/coms_pos(0,ic,ib)
    
    !call pbc_cart_vec_calc(ib, coms_pos(1:3,ic,ib))

end subroutine fed_upd_com_by


!> updates the difference vector between two collective COM:s
subroutine fed_upd_com_dist2(ib)

    use constants_module, only : uout
    use cell_module, only : pbc_cart_vec_calc

        !> the id/index of the configuration box
    integer, intent(in) :: ib

    coms_pos(1:3,0,ib) = coms_pos(1:3,1,ib)-coms_pos(1:3,2,ib)
    
    !call pbc_cart_vec_calc(ib, coms_pos(1:3,0,ib))

end subroutine fed_upd_com_dist2


!> returns the distance between collective COM:s for molecules or atom sub-sets thereof
!real(kind=wp) function fed_param_com_dist2(id, ib, cfg, is_new)
real(kind=wp) function fed_param_com_dist2(id, ib, is_new)

    use config_type
    use cell_module, only : cfgs, pbc_cart_vec_calc, pbc_cart_pos_calc !, pbc_atom_pos_calc

        !> the id/index of the (sub)state in FED calculation
    integer, intent(inout) :: id

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the configuration box to work with
    !type(config), intent(inout) :: cfg

    logical, intent(in) :: is_new

    real(kind=wp) :: dcoms(3) !, xx, yy, zz

    if( is_new ) then
    !AB: the case of new (re-)calculation of the COM2 order parameter
    !AB: the two COM:s, but not their difference [coms_pos(1:3,0,ib)],
    !AB: must have been already updated by 'call fed_upd_com_by(ib,icom,rmove)'

    !AB: this is done in the course of an MC attempt, upon moving atom(s)/molecule(s)

        dcoms(1:3) = coms_pos(1:3,1,ib)-coms_pos(1:3,2,ib)

    else
    !AB: the case of COMPLETE update of the COM2 order parameter
    !AB: the two COM:s, and their difference [coms_pos(1:3,0,ib)],
    !AB: must have been already updated by 'call fed_upd_com_dist2(ib)'

    !AB: this is done only upon ACCEPTANCE of an MC attempt

        dcoms(1:3) = coms_pos(1:3,0,ib)

    end if

    !AB: convert coordinates Cartesian -> fractional, apply PBC/MIC, and convert back fractional -> Cartesian

    call pbc_cart_vec_calc(ib, dcoms)
    !call pbc_cart_pos_calc(ib, dcoms(1), dcoms(2), dcoms(3))

    fed_param_com_dist2 = sqrt( coms_xyz(1)*dcoms(1)**2 + coms_xyz(2)*dcoms(2)**2 + coms_xyz(3)*dcoms(3)**2 )

    !AB: get the current bin id/index on the parameter grid

    id = fed_param_id(fed_param_com_dist2)

    !AB: for debugging only
    !if( id < 1 .or. id > fed%numstates ) &
    !    write(*,*) "*WARNING* - COM2 distance outside the range in box No.",ib,id,fed_param_com_dist2

end function fed_param_com_dist2


!> resets the COM difference vector and distance parameter for a given configuration box
subroutine fed_param_reset(ib, cfg, is_swap)

    use config_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : idgrp

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the active configuration box to work with
    type(config), intent(inout) :: cfg

    logical, intent(in) :: is_swap

    character*(96) :: word

    real(kind=wp) :: dcoms(3)

    param_old = fed_param_value(id_old, ib, cfg, .false.)
    param_cur = fed_param_value(id_cur, ib, cfg, .true.)
    
    dcoms(:) = coms_pos(1:3,0,ib)

    if( is_fed_param_ok(param_cur, id_cur) ) then

        !write(word,*)ib," : ",abs(param_old-param_cur)
        write(word,*)ib," : ",param_old," -> ",param_cur

        if( .not.is_swap .and. ( abs(param_old-param_cur) > 1.e-8 .or. id_old /= id_cur ) ) then
            call cry(uout,'', &
                 "ERROR(1.1): failed to maintain order parameter consistently in box No."//trim(word),0)
            write(uout,*)
            write(uout,*)"dCOM0: ",dcoms(:)
            write(uout,*)"COM1 : ",coms_pos(1:3,1,ib)
            write(uout,*)"COM2 : ",coms_pos(1:3,2,ib)
            write(uout,*)"dCOM : ",coms_pos(1:3,1,ib)-coms_pos(1:3,2,ib)
            write(uout,*)"dCOM0: ",coms_pos(1:3,0,ib)
            write(uout,*)
        end if

        id_old    = id_cur
        param_old = param_cur

    else

        write(word,*)ib," : ",param_cur," -> ",param_cur

        call cry(uout,'', &
                 "ERROR(1.2): order parameter out of range (value or index) upon check in box No."//trim(word),0)

    end if

    call fed_set_coms(cfg, coms_pos(:,1:ngrp_ids,ib), .true.)

    !AB: update the COM difference for FED parameter
    if( fed % par_kind == FED_PAR_DIST2 ) call fed_upd_com_dist2(ib)

    param_cur = fed_param_value(id_cur, ib, cfg, .true.)

    if( is_fed_param_ok(param_cur, id_cur) ) then

        !write(word,*)ib," : ",abs(param_old-param_cur)
        write(word,*)ib," : ",param_old," -> ",param_cur

        if( .not.is_swap .and. ( abs(param_old-param_cur) > 1.e-8 .or. id_old /= id_cur ) ) then
            call cry(uout,'', &
                 "ERROR(2.1): failed to reset order parameter consistently in box No."//trim(word),0)
            write(uout,*)
            write(uout,*)"dCOM0: ",dcoms(:)
            write(uout,*)"COM1 : ",coms_pos(1:3,1,ib)
            write(uout,*)"COM2 : ",coms_pos(1:3,2,ib)
            write(uout,*)"dCOM : ",coms_pos(1:3,1,ib)-coms_pos(1:3,2,ib)
            write(uout,*)"dCOM1: ",coms_pos(1:3,0,ib)
            write(uout,*)
        end if

        id_old    = id_cur
        param_old = param_cur

    else

        write(word,*)ib," : ",param_cur," -> ",param_cur

        call cry(uout,'', &
                 "ERROR(2.2): order parameter out of range (value or index) upon reset in box No."//trim(word),0)

    end if

    !if( master ) then
    !AB: for debugging only
    !    if( abs(param_old-param_cur) > 1.e-8 .or. id_old /= id_cur ) &
    !        write(*,*) "*WARNING* - order parameter reset inconsistently in box No.",ib
    !    if( id_cur < 1 .or. id_cur > fed%numstates ) &
    !        write(*,*) "*WARNING* - order parameter reset outside the range in box No.",ib,id_cur
    !end if

end subroutine fed_param_reset


!> sets up collective COM:s for molecules or sub-sets of atoms thereof
subroutine fed_set_coms(cfg, coms_set, is_new)

    use constants_module, only : uout
    use config_type
    use molecule_module, only : full_mol_com, mol_com_stored !, mol_com

        !> the configuration box to work with
    type(config), intent(inout) :: cfg

        !> array to store molecule(s)/cluster(s) COM:s
    real(kind=wp), intent(inout)  :: coms_set(:,:)

    !AB: must comply with the following declaration:
    !real(kind=wp), dimension(0:3,0:ngrp_ids), intent(inout)  :: coms_set

        !> flag for taking actual (`.true.`) or stored (`.false.`) atom coordinates
    logical, intent(in) :: is_new

    real(kind=wp), dimension(0:3,0:grp_natms) :: atms_pos

    real(kind=wp), dimension(0:3,0:grp_nmols) :: mols_pos

    integer :: icom, imol, iatm, im, ia, nmols, natms !, icfg, iglb, ipos

    coms_set = 0.0_wp

    !if( is_mol_com ) then

        if( is_atm_com ) then

        !AB: the case of COM:s for atom subsets within molecule(s)

            coms: do icom = 1,ngrp_ids

               nmols = 0

               mols: do imol = 1,grp_nmols

                  im = grp_mol_ids(imol,icom)

                  if( im < 1 ) exit mols ! must get through at least once!

                  nmols = nmols+1

                  natms = 0

                  !AB: calculate COM for a subset of atoms within each molecule

                  atms: do iatm = 1,grp_natms

                     ia = grp_atm_ids(iatm,icom)

                     if( ia < 1 ) exit atms ! must get through at least once!

                     natms = natms+1

                     atms_pos(0,iatm) = cfg%mols(im)%atms(ia)%mass

                     if( is_new ) then 

                        atms_pos(1:3,iatm) = cfg%mols(im)%atms(ia)%rpos(:)

                     else

                        atms_pos(1:3,iatm) = cfg%mols(im)%atms(ia)%store_rpos(:)

                     end if

                  end do atms

                  if( natms > 1 ) then

                      call set_pos_com(cfg, atms_pos, natms)

                      mols_pos(:,imol) = atms_pos(:,0)

                  else if( natms == 1 ) then

                      !atms_pos(:,0) = atms_pos(:,1)

                      mols_pos(:,imol) = atms_pos(:,1)

                  else ! ERROR

                  end if

               end do mols

               if( nmols > 1 ) then

                   call set_pos_com(cfg, mols_pos, nmols)

                   coms_set(:,icom) = mols_pos(:,0)

               else if( nmols == 1 ) then

                   !mols_pos(:,0) = mols_pos(:,1)

                   coms_set(:,icom) = mols_pos(:,1)

               else ! ERROR

               end if

            end do coms

        else ! same as if( .not. is_atm_com ) then

        !AB: the case of COM:s for whole molecule(s)

            do icom = 1,ngrp_ids

               nmols = 0

               do imol = 1,grp_nmols

                  im = grp_mol_ids(imol,icom)

                  if( im < 1 ) exit ! must get through at least once!

                  nmols = nmols+1

                  !AB: calculate COM for each molecule

                  if( is_new ) then 

                      call full_mol_com( cfg%mols(im), cfg%vec%invlat, cfg%vec%latvector )

                      mols_pos(0,imol) = cfg%mols(im)%mass

                      mols_pos(1:3,imol) = cfg%mols(im)%rcom(:)

                  else

                      call mol_com_stored( cfg%mols(im) )

                      mols_pos(0,imol) = cfg%mols(im)%mass

                      mols_pos(1:3,imol) = cfg%mols(im)%store_rcom(:)

                  end if

               end do !mols

               if( nmols > 1 ) then

                   call set_pos_com(cfg, mols_pos, nmols)

                   coms_set(:,icom) = mols_pos(:,0)

               else if( nmols == 1 ) then

                   !mols_pos(:,0) = mols_pos(:,1)

                   coms_set(:,icom) = mols_pos(:,1)

               else ! ERROR

               end if

            end do !coms

        end if

    !else !if( is_atm_com ) then

        !AB: DO NOT ALLOW atom indices without molecule indices, i.e. global atom indices not supported!
        !AB: it appeared too complicated, costly and unjustified...

    !end if

    !if( master ) then
    !    write(uout,*)
    !    write(uout,*)"Initial COM:s in FED calculation:"
    !    do icom = 1,ngrp_ids
    !       write(uout,*)coms_set(:,icom)
    !    end do !coms
    !end if

end subroutine fed_set_coms


!> calculates centre of mass (COM) for a set of weighted positions (atoms or molecule COM:s) \n
!> BUT beware of atoms more than half any box dimension away from the first one 
subroutine set_pos_com(cfg, pos_set, npos)

    use kinds_f90
    use config_type

    implicit none

        !> active configuration/box
    type(config), intent(inout) :: cfg

    real(kind=wp), dimension(0:3,0:npos), intent(inout) :: pos_set

    integer, intent(in) :: npos

    real(kind = wp) :: orig(3), rx, ry, rz, xx, yy, zz

    real(kind = wp) :: mass, wght, xsum, ysum, zsum

    integer :: i

    !AB: do Cartesian-fractional-Cartesian transformations and PBC unwrapping within one loop

    mass = pos_set(0,1)

    rx = pos_set(1,1)
    ry = pos_set(2,1)
    rz = pos_set(3,1)

    !AB: the first atom sets the origin in the fractional space

    if( cfg%vec%is_orthogonal ) then

    orig(1) = rx
    orig(2) = ry
    orig(3) = rz

    xsum = rx*mass
    ysum = ry*mass
    zsum = rz*mass

      do i = 2, npos

        wght = pos_set(0,i)

        rx = (pos_set(1,i)-orig(1))*cfg%vec%invlat(1,1)
        ry = (pos_set(2,i)-orig(2))*cfg%vec%invlat(2,2)
        rz = (pos_set(3,i)-orig(3))*cfg%vec%invlat(3,3)

        rx = (rx - anint(rx))*cfg%vec%latvector(1,1) + orig(1)
        ry = (ry - anint(ry))*cfg%vec%latvector(2,2) + orig(2)
        rz = (rz - anint(rz))*cfg%vec%latvector(3,3) + orig(3)

        pos_set(1,i) = rx
        pos_set(2,i) = ry
        pos_set(3,i) = rz

        xsum = xsum + wght*rx
        ysum = ysum + wght*ry
        zsum = zsum + wght*rz

        !xsum = xsum + wght*(rx*cfg%vec%latvector(1,1) + orig(1))
        !ysum = ysum + wght*(ry*cfg%vec%latvector(2,2) + orig(2))
        !zsum = zsum + wght*(rz*cfg%vec%latvector(3,3) + orig(3))

        mass = mass + wght

      enddo

    else

    !AB: fractional (transformed) coordinates
    orig(1) = cfg%vec%invlat(1,1) * rx + cfg%vec%invlat(2,1) * ry + cfg%vec%invlat(3,1) * rz
    orig(2) = cfg%vec%invlat(1,2) * rx + cfg%vec%invlat(2,2) * ry + cfg%vec%invlat(3,2) * rz
    orig(3) = cfg%vec%invlat(1,3) * rx + cfg%vec%invlat(2,3) * ry + cfg%vec%invlat(3,3) * rz

    xsum = rx*mass
    ysum = ry*mass
    zsum = rz*mass

      do i = 2, npos

        wght = pos_set(0,i)

        rx = pos_set(1,i)
        ry = pos_set(2,i)
        rz = pos_set(3,i)

        !AB: fractional (transformed) coordinates
        xx = cfg%vec%invlat(1,1) * rx + cfg%vec%invlat(2,1) * ry + cfg%vec%invlat(3,1) * rz
        yy = cfg%vec%invlat(1,2) * rx + cfg%vec%invlat(2,2) * ry + cfg%vec%invlat(3,2) * rz
        zz = cfg%vec%invlat(1,3) * rx + cfg%vec%invlat(2,3) * ry + cfg%vec%invlat(3,3) * rz

        !AB: unwrapped (reformed) coordinates in the fractional space
        rx = xx - anint(xx-orig(1))
        ry = yy - anint(yy-orig(2))
        rz = zz - anint(zz-orig(3))

        !AB: Cartesian (transformed back) coordinates
        xx = cfg%vec%latvector(1,1) * rx + cfg%vec%latvector(2,1) * ry + cfg%vec%latvector(3,1) * rz
        yy = cfg%vec%latvector(1,2) * rx + cfg%vec%latvector(2,2) * ry + cfg%vec%latvector(3,2) * rz
        zz = cfg%vec%latvector(1,3) * rx + cfg%vec%latvector(2,3) * ry + cfg%vec%latvector(3,3) * rz

        xsum = xsum + xx * wght
        ysum = ysum + yy * wght
        zsum = zsum + zz * wght

        mass = mass + wght

      enddo
    
    endif

    pos_set(0,0) = mass
    pos_set(1,0) = xsum / mass
    pos_set(2,0) = ysum / mass
    pos_set(3,0) = zsum / mass

end subroutine set_pos_com


!> check if the molecule & atom indices for groups are consistent with FIELD & CONFIG
!subroutine fed_check_grp_ids(job, cfgs, ncfgs)
subroutine fed_check_grp_ids(job, ncfgs)

    use constants_module
    use control_type
    use config_type
    use cell_module, only : cfgs
    use atom_module, only : set_atom_idcom
    use molecule_module, only : set_molecule_idcom
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : idgrp

        !> control structure for the job (various flags and switches, see `control_type.f90`)
    type(control), intent(in) :: job

        ! array of configurations/boxes from input
    !type(config), allocatable, dimension(:), intent(inout) :: cfgs

        !> number of configurations/boxes from input
    integer, intent(in) :: ncfgs

    integer :: nmols(ngrp_ids), natms(ngrp_ids)

    integer :: nmols_found(ngrp_ids), natms_found(ngrp_ids)

    integer :: icom, imol, iatm, icfg, iglb, ipos

    integer :: icom1, imol1, iatm1, nmol_same, natm_same

    logical :: mols_safe, atms_safe ! is_mol_com, is_atm_com

    mols_safe = .false.
    atms_safe = .false.

    is_mol_com = .false.
    is_atm_com = .false.

    !if( fed%par_kind /= FED_PAR_DIST1 .and. fed%par_kind /= FED_PAR_DIST2 ) return

    if( fed%par_kind /= FED_PAR_DIST2 .and. fed%par_kind /= FED_PAR_LAMB ) return

    if( allocated(grp_mol_ids) ) is_mol_com = any( grp_mol_ids > 0 )
    if( allocated(grp_atm_ids) ) is_atm_com = any( grp_atm_ids > 0 )

    if( .not.is_mol_com .and. .not.is_atm_com ) then

        call cry(uout,'', &
             "WARNING: no check for molecule/atom indices, since the relevant arrays are empty!!!",0)

        return

    end if

    !AB: check input for correctly allocated configuration boxes array 'cfgs'

    if( .not. allocated(cfgs) ) &
        call cry(uout,'', &
             "ERROR: trying to check molecule/atom indices against unallocated `cfgs(nconfigs)` array!!!",999)

    if( ncfgs /= size(cfgs, dim=1) ) &
        call cry(uout,'', &
             "ERROR: trying to check molecule/atom indices against badly-sized `cfgs(nconfigs)` array!!!",999)

    call cry(uout,"(/,/,1x,76('-'),/,1x,a,/,1x,76('-'),/)", &
             "Proceeding to check molecule/atom indices for groups in FED calculus...",0)

    nmols = 0
    natms = 0

    nmol_same = 0
    natm_same = 0

    if( is_mol_com ) then

        if( ngrp_ids*grp_nmols /= size(grp_mol_ids) ) &
            call cry(uout,'', &
                     "ERROR: trying to check molecule indices with ill-sized molecule COM array!!!",999)

        do icom = 1,ngrp_ids

           do imol = 1,grp_nmols

              if( grp_mol_ids(imol,icom) > 0 ) then 

                  nmols(icom) = nmols(icom)+1

                  do icom1 = 1,icom-1

                    do imol1 = 1,grp_nmols

                       if( grp_mol_ids(imol,icom) == grp_mol_ids(imol1,icom1) ) nmol_same = nmol_same+1

                    end do

                  end do ! done - icom1 = 1,icom-1

              end if ! done - if( grp_mol_ids(imol,icom) > 0 )

           end do ! done - imol = 1,grp_nmols

           !if( nmol_same > 0 .and. is_atm_com ) then
           if( is_atm_com ) then

           !AB: the case of COM:s for atom subsets within molecule(s)

           !AB: check for overlaps between index ranges for different groups
           !AB: working within molecules (as opposed to global environment)

               if( ngrp_ids*grp_natms /= size(grp_atm_ids) ) &
                   call cry(uout,'', &
                            "ERROR: trying to check atom indices with ill-sized atom COM array!!!",999)

               do iatm = 1,grp_natms

                  if( grp_atm_ids(iatm,icom) > 0 ) then 

                      natms(icom) = natms(icom)+1

                      do icom1 = 1,icom-1

                         do iatm1 = 1,grp_natms

                            if( grp_atm_ids(iatm,icom) == grp_atm_ids(iatm1,icom1) ) natm_same = natm_same+1

                         end do

                      end do ! done - icom1 = 1,icom-1

                  end if ! done - if( grp_atm_ids(iatm,icom) > 0 )

               end do ! done - iatm = 1,grp_natms

           end if ! done - if( nmol_same > 0 .and. is_atm_com ) 

        end do ! done - icom = 1,ngrp_ids

    else !if( is_atm_com ) then

        !AB: DO NOT ALLOW atom indices without molecule indices, i.e. global atom indices not supported!
        !AB: it appeared too complicated, costly and unjustified...

        !if( ngrp_ids*grp_natms /= size(grp_atm_ids) ) &
            call cry(uout,'', &
                     "ERROR: trying to check atom indices with ill-sized molecule COM array!!!",999)

    end if

    if( master ) then
        write(uout,*)"nmols = ",nmols(:),";   mols to check - ",is_mol_com
        write(uout,*)"natms = ",natms(:),";   atms to check - ",is_atm_com
    end if

    if( any( nmols(:) == 0 ) ) &
        call cry(uout,'', &
             "ERROR: inconsistent input - molecule indices must be present for ALL groups!!!",999)

    if( master ) then
        write(uout,*)
        write(uout,*)"nmol_same = ",nmol_same
        write(uout,*)"natm_same = ",natm_same
    end if

    if( nmol_same > 0 .and. natm_same > 0 ) &
        call cry(uout,'', &
             "WARNING: same molecule & atom indices are shared between different groups - check for consistency!!!",0)

    !AB: go through all configuration boxes and check for the presence of the specified molecules & atoms
    !AB: also set COM identifiers for all molecules and/or atoms participating in COM:s for FED calculation

    !AB: allocate the matrix to store COM:s 
    if( fed%par_kind /= FED_PAR_LAMB ) call reallocate_coms_pos(ngrp_ids,ncfgs)

    do icfg = 1,ncfgs

       nmols_found = 0
       natms_found = 0

       do imol = 1,cfgs(icfg)%num_mols

          do iatm = 1,cfgs(icfg)%mols(imol)%natom
             call set_atom_idcom(cfgs(icfg)%mols(imol)%atms(iatm),0)
          end do

          do icom = 1,ngrp_ids

              !AB: check the molecule indices

              if( is_val_found_in_dim(grp_mol_ids(:,icom),imol,ipos) ) then

                  nmols_found(icom) = nmols_found(icom)+1

                  if( is_atm_com ) then
                  !AB: the case of COM:s for atom subsets within molecule(s)

                      !AB: set the molecule COM ID for FED - negative 
                      !AB: when atom subsets are defined in the input!

                      call set_molecule_idcom(cfgs(icfg)%mols(imol),-icom)

                      do iatm = 1,cfgs(icfg)%mols(imol)%natom

                          if( is_val_found_in_dim(grp_atm_ids(:,icom),iatm,ipos) ) then

                              natms_found(icom) = natms_found(icom)+1

                              call set_atom_idcom(cfgs(icfg)%mols(imol)%atms(iatm),icom)

                          end if

                      end do ! done - iatm = 1,cfgs(icfg)%mols(imol)%natom

                  else 
                  !AB: the case of COM:s for whole molecule(s)

                      !AB: set the molecule COM ID for FED - positive 
                      !AB: when *whole* molecules count (no atom indices in the input)!

                      call set_molecule_idcom(cfgs(icfg)%mols(imol),icom)

                      do iatm = 1,cfgs(icfg)%mols(imol)%natom

                         call set_atom_idcom(cfgs(icfg)%mols(imol)%atms(iatm),icom)

                      end do ! done - iatm = 1,cfgs(icfg)%mols(imol)%natom

                  end if

              end if ! done - if molecule index found in the COM ids row

          end do ! done - icom = 1,ngrp_ids

       end do ! done - imol = 1,cfgs(icfg)%num_mols

       mols_safe = .true.
       atms_safe = .true.

       do icom = 1,ngrp_ids

          mols_safe = mols_safe .and. ( nmols_found(icom) == nmols(icom) )
          atms_safe = atms_safe .and. ( natms_found(icom)/nmols(icom) == natms(icom) )

       end do ! done - icom = 1,ngrp_ids

       !if( any(nmols_found(:) /= nmols(:)) .or. any(natms_found(:) /= natms(:)) ) then
       if( .not.mols_safe .or. .not.atms_safe ) then

           if( master ) then

               write(uout,'(/,1x,a,/)')&
                     "Check molecule/atom indices for groups in `fed order com2` directive for consistency ..."

               write(uout,*)"mols found = ",nmols_found(:)," =/= nmols;   mols checked - ",is_mol_com
               write(uout,*)"atms found = ",natms_found(:)/nmols_found(:),&
                            " =/= natms;   atms checked - ",is_atm_com

           end if

           call deallocate_coms_pos()

           call cry(uout,'', &
                "ERROR: Failed the check for molecule/atom indices in `grp_atm_ids(grp_natms,ngrp_ids)` array!!!",999)

       end if

       !AB: set up all the joint COM:s for molecules or sub-sets of atoms thereof
       if( fed%par_kind /= FED_PAR_LAMB ) then

           call fed_set_coms(cfgs(icfg), coms_pos(:,1:ngrp_ids,icfg), .true.)

           if( master ) then

               write(uout,*)
               write(uout,*)"------------------------------------------------"
               write(uout,*)"Initial COM:s for FED calculation in box No.",icfg
               write(uout,*)"------------------------------------------------"

               do icom = 1,ngrp_ids

                  write(uout,*)coms_pos(:,icom,icfg)

               end do !coms

           end if

           if( ngrp_ids > 1 ) then 

               !AB: set the distance between the first two COM:s
               coms_pos(:,0,icfg) = coms_pos(:,1,icfg)-coms_pos(:,2,icfg)

               param_cur = fed_param_com_dist2(id_cur, icfg, .true.)

               if( master ) then
                   write(uout,"(1x,100('-'))")
                   write(uout,*)coms_pos(:,0,icfg)
                   write(uout,"(1x,100('-'))")
                   write(uout,*)" -> current COM2 & bin : ", param_cur, id_cur
                   write(uout,"(1x,100('-'))")

                   !write(uout,*)coms_pos(:,0,icfg)," -> ", fed_param_com_dist2(id_cur, icfg, .true.), id_cur
               end if

           end if

       end if

    end do ! done - icfg = 1,ncfgs

end subroutine fed_check_grp_ids


logical function is_mol_atom_in_com(ib, im, ia, ic)

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the id/index of the molecule
    integer, intent(in) :: im

        !> the id/index of the atom
    integer, intent(in) :: ia

        !> the id/index of the COM
    integer, intent(inout) :: ic

    integer :: icom, ipos

    is_mol_atom_in_com = .false.
    ic = 0

    !AB: check the molecule/atom indices (need to have a flag within molecule_type and/or atom_type ???)
    do icom = 1, ngrp_ids

       if( is_val_found_in_dim(grp_mol_ids(:,icom),im,ipos) .and. & 
           is_val_found_in_dim(grp_atm_ids(:,icom),ia,ipos) ) then

           ic = icom

           is_mol_atom_in_com = .false.

           !return
           exit

       end if

    end do ! done - icom = 1,ngrp_ids

end function is_mol_atom_in_com


logical function is_mol_in_com(ib, im, ic)

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the id/index of the molecule
    integer, intent(in) :: im

        !> the id/index of the COM
    integer, intent(inout) :: ic

    integer :: icom, ipos

    is_mol_in_com = .false.
    ic = 0

    !AB: check the molecule indices (need to have a flag within molecule_type and/or atom_type ???)
    do icom = 1, ngrp_ids

       if( is_val_found_in_dim(grp_mol_ids(:,icom),im,ipos) ) then 

           ic = icom

           is_mol_in_com = .false.

           !return
           exit

       end if

    end do ! done - icom = 1,ngrp_ids

end function is_mol_in_com


logical function is_atm_in_com(ib, ia, ic)

        !> the id/index of the configuration box
    integer, intent(in) :: ib

        !> the id/index of the atom
    integer, intent(in) :: ia

        !> the id/index of the COM
    integer, intent(inout) :: ic

    integer :: icom, ipos

    is_atm_in_com = .false.
    ic = 0

    !AB: check the atom indices (need to have a flag within molecule_type and/or atom_type ???)
    do icom = 1, ngrp_ids

       if( is_val_found_in_dim(grp_atm_ids(:,icom),ia,ipos) ) then 

           ic = icom

           is_atm_in_com = .false.

           !return
           exit

       end if

    end do ! done - icom = 1,ngrp_ids

end function is_atm_in_com


!> check if temperature value is within the T-parameter range
!subroutine fed_check_temp(temp)
subroutine fed_check_temp(job)

    !use constants_module  ! due to module dependence
    !use comms_mpi_module, only : master ! due to module dependence
    use control_type

    type(control), intent(in) :: job

        ! tested `T`-value
    !real(kind=wp), intent(in) :: temp

        ! temporary `T`-value
    real(kind=wp) :: temp, temp_par

        ! indices & counters
    integer :: k, kin

        ! flag for the test result
    logical :: is_temp_ok

    is_temp_ok = .false.

    if( fed % par_kind /= FED_PAR_TEMP .and. fed % par_kind /= FED_PAR_BETA ) return

    temp = job%systemp

    do k=1,fed%numstates
        !AB: for T-variation parameter is always 1/(Rg*T), see initialisation above
        !AB: however the variation range setting can be either in T or 1/(RgT)

        temp_par = 1.0_wp/(parm_vec(k)*BOLTZMAN)

        if( abs(temp - temp_par) < 1.0e-8_wp ) then 

            if( is_temp_ok ) then 

                call cry(uout,'', &
                         "ERROR: FED specification with repetitive values of T"// &
                         "' - either amend or revoke FED specs!!!",51)

            else

                 is_temp_ok = .true.

            end if

            temp_par = temp

            if( fed % par_kind == FED_PAR_BETA ) temp_par = 1.0_wp/(temp*BOLTZMAN)

            id_cur = fed_param_id(temp_par)

            if( id_cur /= k ) then

                if( master ) then

                    write(uout,*)
                    write(uout,*)
                    write(uout,*)"ERROR: FED specification initial state id = ",id_cur, &
                                 " does not correspond to actual T = ",temp," (id must be ",k,")!!!"

                end if

                call error(4)

            end if

        end if

    end do

    param_old = temp
    param_cur = temp

    id_old = id_cur

    if( is_temp_ok ) then

        if( master ) then

            write(uout,*)
            write(uout,*)"----------------------------"
            write(uout,*)" FED calculation starts with T = ",temp," in ",id_cur," state"
            write(uout,*)"----------------------------"

        end if

    else

        if( master ) then

            write(uout,*)
            write(uout,*)
            write(uout,*)"ERROR: FED specification with actual T = ",temp, &
                         " not found in the FED parameter range!!!"

        end if

        call error(4)

    end if

end subroutine fed_check_temp


!> check if given order parameter value is within the input range
! only relevant for order parameters dependent on configuration or box (or boxes, as in PSMC)
subroutine fed_check_value(par_val)

        !> order parameter value to check for being within the range
    real(kind=wp), intent(in) :: par_val

    character*(40) :: word

        ! indices & counters
    integer :: k, kin

    if( is_fed_param_ok(par_val) ) then

        id_cur = fed_param_id(par_val)

        !if( is_fed_param_id_ok(id_cur) ) then
        !else
        if( .not. is_fed_param_id_ok(id_cur) ) then

            write(word,"(i4,' (value = ',e14.7,')')")id_cur, par_val

            call cry(uout,'', &
                     "ERROR: FED order parameter index out of bounds: "//trim(word)// &
                     " - either amend or revoke FED input!!!",51)

        end if

    else

        write(word,'(e14.7)')par_val

        call cry(uout,'', &
                 "ERROR: FED order parameter value out of range: "//trim(word)// &
                 " - either amend or revoke FED input!!!",51)

    end if

    param_old = par_val
    param_cur = par_val

    id_old = id_cur

end subroutine fed_check_value


!> check if current order parameter values are within the input range for all configuration boxes
! only relevant for order parameters dependent on configuration or box
subroutine fed_check_range(job, ncfgs)

    !use config_type
    !use lattice_module
    use control_type
    use cell_module, only : cfgs

        ! the configurations/boxes from input
    !type(config), intent(inout) :: cfgs

        !> control structure for the job (various flags and switches, see `control_type.f90`)
    type(control), intent(in) :: job

        !> the id/index of the configuration box
    integer, intent(in) :: ncfgs

    character*(40) :: word

        !> value to check
    real(kind=wp) :: par_val

        ! indices & counters
    integer :: ib, id

    id = 1

    do  ib = 1,ncfgs

        id = id_cur

        !write(*,*)'fed_check_range(): checking parameter for box No.',ib,cfgs(ib)%vec%volume
        !par_val = cfgs(ib)%vec%volume

        par_val = fed_param_value(id,ib,cfgs(ib),.true.)

        !call fed_check_value(par_val)

        if( is_fed_param_ok(par_val) ) then

            id_cur = fed_param_id(par_val)

            if( id_cur /= id ) then

                write(word,"(i3,' =/= ',i3)")id_cur, id

                call cry(uout,'', &
                     "ERROR: inconsistent FED order parameter index: "//trim(word)// &
                     " - either amend or revoke FED input!!!",51)

            end if

            if( is_fed_param_id_ok(id_cur) ) then

                if( master ) then

                    write(uout,*)
                    write(uout,*)"------------------------------------------------"
                    write(uout,'(1x,a,i3)')"Initial state for FED calculation in box No. ",ib
                    write(uout,'(1x,a,e14.7,a,i4)')"FED calculation starts with order parameter = ",par_val," in bin ",id_cur
                    write(uout,*)"------------------------------------------------"

                end if

            else

                write(word,'(i3,1x,e14.7)')id_cur, par_val

                call cry(uout,'', &
                         "ERROR: FED order parameter index out of bounds: "//trim(word)// &
                         " - either amend or revoke FED input!!!",51)

            end if

        else

            write(word,'(e16.7,1x,i3)')par_val,id_cur

            call cry(uout,'', &
                 "ERROR: FED order parameter value out of range: "//trim(word)// &
                 " - either amend or revoke FED input!!!",51)

        end if

        param_old = par_val
        param_cur = par_val

        id_old = id_cur

    end do

end subroutine fed_check_range


!> some other routine
!subroutine some_routine(params)
!
!end subroutine


!AB: *** (RE-/DE-) ALLOCATING SPECIFIC ORDER PARAMETER VECTORS & ARRAYS - START ***


!> (re-)allocating FED arrays for molecule indices (for complex order parameters only)
subroutine reallocate_grp_ids(ncols_mol,nrows_mol,ncols_atm,nrows_atm)

    integer, intent(in) :: ncols_mol,nrows_mol,ncols_atm,nrows_atm

    call reallocate_grp_mol_ids(ncols_mol,nrows_mol)
    call reallocate_grp_atm_ids(ncols_atm,nrows_atm)

end subroutine reallocate_grp_ids


!> deallocating FED arrays for molecule/atom indices (for complex order parameters only)
subroutine deallocate_grp_ids()

    call deallocate_grp_mol_ids
    call deallocate_grp_atm_ids

end subroutine deallocate_grp_ids


!> (re-)allocating FED arrays for molecule indices (for complex order parameters only)
subroutine reallocate_grp_mol_ids(ncols,nrows)

    use constants_module, only : uout

    integer, intent(in) :: ncols,nrows

    integer :: fail

    fail = 0

    call reallocate_quiet(grp_mol_ids,ncols,nrows,fail)

    if( fail > 0 ) then

        call cry(uout,'',"Could not (re-)allocate FED COM array for molecule indices!!!",999)

       !write(uout,*)
       !write(uout,*)"Could not (re-)allocate FED COM array for molecule indices!!!"
       !call error(999)

    end if

end subroutine reallocate_grp_mol_ids


!> deallocating FED arrays for molecule indices (for complex order parameters only)
subroutine deallocate_grp_mol_ids()

    call deallocate_quiet(grp_mol_ids)

end subroutine deallocate_grp_mol_ids


!> (re-)allocating FED arrays for atom indices (for complex order parameters only)
subroutine reallocate_grp_atm_ids(ncols,nrows)

    use constants_module, only : uout

    integer, intent(in) :: ncols,nrows

    integer :: fail

    fail = 0

    call reallocate_quiet(grp_atm_ids,ncols,nrows,fail)

    if( fail > 0 ) then

        call cry(uout,'',"Could not (re-)allocate FED COM array for atom indices!!!",999)

        !write(uout,*)
        !write(uout,*)"Could not (re-)allocate FED COM array for atom indices!!!"
        !call error(999)

    end if

end subroutine reallocate_grp_atm_ids


!> deallocating FED arrays for atom indices (for complex order parameters only)
subroutine deallocate_grp_atm_ids()

    call deallocate_quiet(grp_atm_ids)

end subroutine deallocate_grp_atm_ids


!> (re-)allocating FED matrix `coms_pos` for COM:s (for complex order parameters only)
subroutine reallocate_coms_pos(nrows, nboxes)

    use constants_module, only : uout

    !AB: the allocation of the matrix must result in this sort of array:
    !real(kind=wp), dimension(0:3, 0:ngrp_ids, 1:nconfig)  :: coms_pos

    integer, intent(in) :: nrows, nboxes

    integer :: fail

    fail = 0

    if( allocated(coms_pos) ) deallocate(coms_pos)

    allocate( coms_pos(0:3, 0:nrows, 1:nboxes), stat=fail )

    if( fail > 0 ) &
        call cry(uout,'',"Could not (re-)allocate FED matrix for COM positions!!!",999)
    
    !if( .not.allocated(coms_xyz) ) allocate(coms_xyz(3), stat=fail)
    !if( fail > 0 ) &
    !    call cry(uout,'',"Could not (re-)allocate XYZ vector for COMs!!!",999)

end subroutine reallocate_coms_pos


!> deallocating FED arrays for atom indices (for complex order parameters only)
subroutine deallocate_coms_pos()

    deallocate(coms_pos)

end subroutine deallocate_coms_pos


!AB: *** (RE-/DE-) ALLOCATING SPECIFIC ORDER PARAMETER VECTORS & ARRAYS - END ***


end module fed_order_module

