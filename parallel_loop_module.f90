! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!> @brief
!> - Groupping of MPI threads over the available nodes/cores
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor grouping of MPI threads over the available nodes/cores

module parallel_loop_module

    use kinds_f90

    implicit none

    !AB: moved to comms_mpi_module.f90 to allow error() messages not only for idnode==0

    !identifies the node(s) that will print out information
    !logical, save :: master

    !the start and step counters for the two-body routines
    !TU: Note that the '_mol' variables also apply to cells if cell lists are in operation
    !TU: without neighbour lists in order to calculate energies.
    integer, save :: tb_start_mol, tb_step_mol
    integer, save :: tb_start_atm, tb_step_atm
    integer, save :: rcp_start, rcp_step

    !the group number for this core
    integer, save :: idgrp

    !the rank of a node within a group
    integer, save :: grp_rank

    !the number of nodes in each workgroup
    integer, save :: wkgrp_size

    real(kind = wp), save :: base_temp, repx_dtemp

    !list of nodes in a workgroup
    integer, allocatable, dimension(:) :: workgroup, grp_masters


    !$OMP THREADPRIVATE (tb_start_mol, tb_step_mol, tb_start_atm, tb_step_atm, rcp_start, rcp_step)

contains


!> standard/default initialisation of MPI_COMM_WORLD
! this routine is not called from anywhere (why ???)
subroutine initialise_communication()

    use comms_omp_module, only : idthread, mxthread
    use comms_mpi_module, only : master, idnode, mxnode, mpi_comm_group, mpi_comm_master

    implicit none

    include 'mpif.h'

    tb_start_atm = 1
    tb_step_atm = 1
    tb_start_mol = 1
    tb_step_mol = 1
    rcp_start = 1
    rcp_step = 1

    idgrp = 0
    grp_rank = 1
    wkgrp_size = 1

    if (mxnode > 1) then

        idgrp = 0
        grp_rank = idnode
        wkgrp_size = mxnode

        mpi_comm_group  = MPI_COMM_WORLD
        mpi_comm_master = MPI_COMM_NULL

    endif

    !set all processors to slave (except for single processor jobs or node 0) - this will be changed later
    master = .false.
    if (mxnode == 1 .or. idnode == 0) master = .true.

end subroutine


!> actual initialisation of MPI_COMM_WORLD
subroutine initialise_loops(coultypes, distribgvec, par_atm)

    use constants_module, only : uout
    use comms_omp_module, only : idthread, mxthread
    use comms_mpi_module, only : master, idnode, mxnode, mpi_comm_group, mpi_comm_master

    implicit none

    include 'mpif.h'

        !> types of Coulomb treatment (per replica)
    integer, intent(in) :: coultypes(:)

        !> flag controlling if k-vectors in reciprocal Ewald sums are distributed within a workgroup
    logical, intent(inout) :: distribgvec

        !> flag controlling parallelisation of loops over species: either over atoms or molecules (otherwise)
    logical, intent(in) :: par_atm

    tb_start_atm = 1
    tb_step_atm  = 1
    tb_start_mol = 1
    tb_step_mol  = 1
    rcp_start    = 1
    rcp_step     = 1

    idgrp      = 0
    grp_rank   = 1
    wkgrp_size = 1

    if( mxnode > 1 ) then

        idgrp      = 0
        grp_rank   = idnode
        wkgrp_size = mxnode

        mpi_comm_group  = MPI_COMM_WORLD
        mpi_comm_master = MPI_COMM_NULL

        if (par_atm) then

            tb_start_atm = idnode + 1
            tb_step_atm  = mxnode

        else

            tb_start_mol = idnode + 1
            tb_step_mol  = mxnode

        endif

    endif

    if( mxnode < 2 .and. par_atm ) then

        call cry(uout,'(/,1x,a,/)', "WARNING: 'paratom' directive to parallelise loops over atoms "//&
                         &" not applicable with one node (serial mode) and hence is ignored!",0)

    end if

    !check for errors/limitations due to 'coultype' and 'distribgvec'
    if( distribgvec ) then

        if( all(coultypes /= 1) ) then

            distribgvec = .false.
            call cry(uout,'(/,1x,a,/)',"WARNING: distributed g-vectors make no sense "//&
                         &" without full Ewald sums (switched off)!",0)

        else if( mxnode < 2 ) then

            distribgvec = .false.
            call cry(uout,'(/,1x,a,/)',"WARNING: 'distewald' directive to distribute g-vectors "//&
                         &" not applicable with one node (serial mode) and hence is ignored!",0)

        else

            !if( mxnode < 2 ) call error(339)

            if( mxthread > 1 ) call error(340)

            !rcp_start = idnode + 1
            !rcp_step  = mxnode

        end if

    else if( mxnode > 1 ) then
    
        rcp_start = idnode + 1
        rcp_step  = mxnode

    !if( .not.distribgvec .and. mxnode > 1 ) then
        !call error(371)
    endif

    !$OMP PARALLEL

    !$ tb_start_mol = idnode * mxthread + idthread * mxnode + 1
    !$ tb_step_mol = mxthread * mxnode
    !$ rcp_start = idnode * mxthread + idthread * mxnode + 1
    !$ rcp_step = mxthread * mxnode

    !$OMP END PARALLEL

    !set all processors to slave (except for single processor jobs or node 0) - this will be changed later
    master = .false.
    if (mxnode == 1 .or. idnode == 0) master = .true.

    if( master .and. mxnode > 1 ) then

        write(uout,*)
        write(uout,*)"=============================================="
        write(uout,*)"Initial setup for energy loop(s) distribution:"
        write(uout,*)"----------------------------------------------"

        !if( par_atm ) then

            write(uout,*)" - first atom  in loop(s) = idnode + 1  = ",tb_start_atm
            write(uout,*)" - atom stride in loop(s) = no of nodes = ",tb_step_atm

        !else

            write(uout,*)" - first molecule/cell  in loop(s) = idnode + 1  = ",tb_start_mol
            write(uout,*)" - molecule/cell stride in loop(s) = no of nodes = ",tb_step_mol

        !endif

            write(uout,*)" - first gvec  in loop(s) = idnode + 1  = ",rcp_start
            write(uout,*)" - gvec stride in loop(s) = no of nodes = ",rcp_step

        write(uout,*)"=============================================="
        write(uout,*)

    endif

end subroutine


!> setting up the replica exchange topology with MPI thread workgroups
subroutine setup_workgroups(numreplicas, repexch_inc, systemp, distribgvec, par_atm)

    use kinds_f90
    use constants_module, only : uout
    use comms_mpi_module, only : master, idnode, mxnode, mpi_comm_group, mpi_comm_master, &
                                 gsum, split_com, gsum_world, create_cartesian_topology

    implicit none

    include 'mpif.h'

        !> total number of replicas
    integer, intent(in) :: numreplicas

        !> temperature increment (dT) between consecutive replicas
    real (kind = wp), intent(in) :: repexch_inc

        !> base temperature
    real (kind = wp), intent(inout) :: systemp

        !> flag controlling if k-vectors in reciprocal Ewald sums are distributed within a workgroup
    logical, intent(inout) :: distribgvec

        !> flag controlling parallelisation of loops over species: either over atoms or molecules (otherwise)
    logical, intent(in) :: par_atm

    ! internal variables go below

    integer :: rep_node

    integer :: ierr, colour, key, i,freddd

    base_temp  = systemp
    repx_dtemp = repexch_inc

    if( numreplicas < 2 .or. numreplicas > mxnode .or. &
      ( numreplicas /= mxnode .and. mod(mxnode,numreplicas) /= 0 ) ) call error(335)

    !the number of nodes must be distributed evenly over replicas
    !if( numreplicas < 2 .or. mod(mxnode,numreplicas) /= 0 ) call error(335)

    wkgrp_size = mxnode / numreplicas

    if (allocated(workgroup)) deallocate(workgroup)
    allocate (workgroup(0:wkgrp_size))
    workgroup = 0

    if (allocated(grp_masters)) deallocate(grp_masters)
    allocate (grp_masters(0:numreplicas))
    grp_masters = 0

    !the workgroups are "split" using colour-and-key method
    !colour is the workgroup and key is the node number within a group
    !page 61 "Using MPI" Gropp, Lusk Skjellum

    colour = idnode / wkgrp_size

    key = mod(idnode, wkgrp_size)

    !split the WORLD communicator into workgroups
    call split_com(colour, key)

    idgrp = colour
    grp_rank = key

    if (grp_rank == 0) then

        !AB: create the communicator for group masters 
        call split_com(0, idgrp, mpi_comm_master)

        master = .true.
        
        !the array has the world rank of each master and is comminicated to all nodes
        grp_masters(idgrp) = idnode 

        !AB: up to now there was only one master (idnode=0)
        !AB: but with replica-exchange there are more (see above)
        if( idgrp == 0 ) then

            call open_nodefiles('OUTPUT', uout, ierr, 'add')

        else

            call open_nodefiles('OUTPUT', uout, ierr, 'write') !, 'add')

        endif

        write(uout,'(/,1x,a,2i5,a,l5,a)') 'setup_workgroups : ', &
              idnode, grp_rank, "   (master : ", master,")"

    else

        !AB: fake the masters' communicator for non-masters ( mpi_comm_master = MPI_COMM_NULL )
        call split_com(MPI_UNDEFINED, idnode, mpi_comm_master)

    endif

    call gsum_world(grp_masters)

    !record the node within each workgroup
    workgroup(key) = idnode
    call gsum(workgroup)

    if (master) then

        write(uout,"(/,1x,'nodes being used for workgroup ',i6,/)") idgrp

        do i = 0, wkgrp_size - 1

            write(uout,"(1x,'worker ',i6,' at node ',i6)") i, workgroup(i)
            !write(uout,"(1x,'workgroup rank ',i6,' node ',4i10)") i, workgroup(i), &
            !     mpi_comm_world, mpi_comm_group, mpi_comm_master

        enddo

        flush(uout)

    endif

    !create topology linking nodes to neighbours
    call create_cartesian_topology(idgrp, numreplicas, wkgrp_size)

    if( .not.distribgvec ) then

        rcp_start = grp_rank + 1
        rcp_step  = wkgrp_size

    !else
        !call error(371)
    endif

    !the temperature of this replica

    if( idgrp /= 0 ) then

        systemp = base_temp + (repexch_inc * idgrp)

    endif

    if( par_atm ) then

        tb_start_atm = grp_rank + 1
        tb_step_atm  = wkgrp_size

    else

        tb_start_mol = grp_rank + 1
        tb_step_mol  = wkgrp_size

    endif

    if (master) then
    
        write(uout,"(/,1x,'rep exchange : temperature for this workgroup (K)     = ',f10.5)") systemp

        write(uout,*)
        write(uout,*)"=============================================="
        write(uout,*)"Working setup for energy loop(s) distribution:"
        write(uout,*)"----------------------------------------------"

        write(uout,*)" - first atom  in loop(s) = ",tb_start_atm
        write(uout,*)" - atom stride in loop(s) = ",tb_step_atm

        write(uout,*)" - first molecule/cell  in loop(s) = ",tb_start_mol
        write(uout,*)" - molecule/cell stride in loop(s) = ",tb_step_mol

        write(uout,*)"=============================================="
        write(uout,*)

        flush(uout)

    endif

end subroutine


subroutine open_nodefiles(fname, iopen, istat, state)

    use constants_module, only : uout
    use comms_mpi_module, only : master
    use parse_module,     only : int_2_char3

    implicit none

    character(*), intent(in)  :: fname
    integer, intent(in)       :: iopen
    integer, intent(inout)    :: istat

    character(3), optional, intent(in) :: state

    logical :: is_open, safe

    character(256) :: filename, fcname
    character(10)  :: fcstate
    character(3)   :: cnum

    istat = 0
    
    safe = .false.
    call int_2_char3(idgrp, cnum, safe)

    if( .not.safe ) &
        call cry(uout,'', &
                "ERROR: open_nodefiles() - workgroup ID is out of range (must be between 0 and 999) !!!",999)

    if( master ) then

        if( idgrp < 1000 ) then

            filename = trim(adjustL(fname//'.'//cnum))

            is_open = .false.

            inquire (unit=iopen, name=fcname, opened=is_open, action=fcstate)

            fcname  = trim(adjustL(fcname))
            fcstate = trim(adjustL(fcstate))

            if( is_open ) then 

              if( present(state) ) then

                if( state == 'add' .or. state == 'ADD' .or. state == 'Add' ) then

                    !write(uout,*)
                    !write(uout,*)
                    !write(uout,*)"NOTE: Reopening file '",trim(fcname),"' for appending in '",trim(fcstate), &
                    !             "' mode (i.e. flushing to disk)"
                    !write(uout,*)

                    !return

                end if

              else

                !if( iopen /= uout .and. trim(filename) /= trim(fcname) ) then
                if( trim(filename) /= trim(fcname) ) then

                    write(uout,*)
                    write(uout,*)
                    !write(uout,*)"ERROR: Tried to reopen file unit ",iopen, &
                    write(uout,*)"WARNING: Trying to reopen file unit ",iopen, &
                                 " under a different name '",trim(filename), &
                                 "' =/= '",trim(fcname),"' currently in mode = '",trim(fcstate),"'"
                    write(uout,*)

                    !call error(999)

                endif

              end if

              close(iopen)

            end if

            if( present(state) ) then 

                if( state == 'add' .or. state == 'ADD' .or. state == 'Add' ) then 

                    open(unit = iopen, file = filename, status='old', position='append', iostat=istat)

                else if( state == 'old' .or. state == 'OLD' .or. state == 'Old' ) then

                    open(unit = iopen, file = filename, status='old', iostat=istat)

                else if( state == 'new' .or. state == 'NEW' .or. state == 'New' ) then

                    open(unit = iopen, file = filename, status='new', iostat=istat)

                else

                    open(unit = iopen, file = filename, iostat=istat)

                end if

            else

                open(unit = iopen, file = filename, iostat=istat)

            end if

        else

            call error(334)

        endif

    endif 

end subroutine open_nodefiles


end module
