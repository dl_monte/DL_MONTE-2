!***************************************************************************
!   Copyright (C) 2022 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Contains Monte Carlo moves associated with changing the Hamiltonian coupling parameter lambda, and
!>   counters for such moves
!> @usage 
!> - @stdusage 
!> - @stdspecs
!>
!> @modulecontaining lambda Monte Carlo moves
module lambda_moves

    use kinds_f90
    use lambda_module

    implicit none

        !> Total number of lambda moves
    integer :: total_lambda_moves = 0
    
        !> Number of successful lambda moves
    integer :: successful_lambda_moves = 0

        !> Total number of lambda insert
    integer :: total_lambda_inserts = 0

        !> Number of successful lambda insert moves
    integer :: successful_lambda_inserts = 0

        !> Total number of lambda delete
    integer :: total_lambda_deletes = 0

        !> Number of successful lambda delete moves
    integer :: successful_lambda_deletes = 0


contains



!> @brief
!> - Performs a lambda Monte Carlo move: attempts to change lambda for the whole system (which may be comprised
!>   of two boxes - for the case of a Gibbs simulation), and accepts or rejects the move based on the resulting
!>   energy change.
subroutine move_lambda_system(ib, job, beta, betainv, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                       energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                       emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                       emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)


    use kinds_f90
    use species_module, only : number_of_molecules
    use control_type
    use cell_module, only : cfgs, nconfigs
    use field_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM

    use metpot_module, only : nmetpot
    use random_module, only : duni
    use slit_module, only : slit_zfrac1, slit_zfrac2

    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject


        !> Current box number (as opposed to the `other' box, which can happen in PSMC or Gibbs simulations). Note 
        !> however that since lambda is currently implemented as a global quantity, the energy change due to 
        !> changing lambda in ALL boxes is considered in the acceptance/rejection of the change. Designating the
        !> boxes as active (ib) and 'passive' (ib_pas - see below) is an arbitrary labelling which I've kept in
    integer, intent(in) :: ib

        !> Simulation control parameters
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, betainv, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                     energyext(:), energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                     emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: im

    real(kind=wp)   :: oldlambda, &
                       deltav, deltavb, deltavb_can, ntot, newreal, newrcp, newvdw, newthree, &
                       newpair, newext, newmfa, newang, newfour, newmany, newtotal, newenth, newvir, volold, volnew, &
                       oldtotal, oldenth, delta_vol



    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), ncrct(number_of_molecules), &
                       next(number_of_molecules)


    ! Relevant variables for the passive box (variables ending with '_pas') - for Gibbs
    integer :: ib_pas
    real(kind=wp)   :: ntot_pas, newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, &
                       newfour_pas, newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas, &
                       volold_pas, volnew_pas, oldtotal_pas, oldenth_pas!, scale_pas

    real(kind = wp) :: nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       nself_pas(number_of_molecules), ncrct_pas(number_of_molecules), &
                       next_pas(number_of_molecules)

    logical :: out_of_range

    logical :: is_fed_param_lambda, is_gibbs

    ! Flag specifying whether FED is in operation with the lambda order parameter
    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA

    ! Flag specifying whether the Gibbs ensemble is in operation or not - we assume it is the case if there are two
    ! boxes. While PSMC uses two boxes, we forbid this from being used with this move elsewhere
    is_gibbs = ( nconfigs == 2 )

    oldlambda = lambda

    newenth  = 0.0_wp
    newvir   = 0.0_wp
    newreal  = 0.0_wp
    newrcp   = 0.0_wp
    newvdw   = 0.0_wp
    newvdw   = 0.0_wp
    newthree = 0.0_wp
    newpair  = 0.0_wp
    newang   = 0.0_wp
    newfour  = 0.0_wp
    newmany  = 0.0_wp
    newext   = 0.0_wp
    newmfa   = 0.0_wp

    volold = cfgs(ib)%vec%volume
    oldtotal = energytot(ib)
    oldenth  = oldtotal + extpress * volold


    ! In  Gibbs do the same as above for the other box
    if( is_gibbs ) then
       
        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        volold_pas = cfgs(ib_pas)%vec%volume
        oldtotal_pas = energytot(ib_pas)
        oldenth_pas = oldtotal_pas + extpress * volold_pas

    end if

    ! Change lambda

    ! Code for continuous change in lambda
    !lambda = oldlambda + (2.0_wp * duni() - 1.0_wp) * job%maxlambdachange

    ! Code for discrete changes in lambda
    if( duni() < 0.5_wp ) then
    
        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda - job%maxlambdachange)

    else

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda + job%maxlambdachange)
 
    end if

    ! Change any lambda-dependent charges in the system
    call set_lambda_charges_cfg(cfgs(ib))
    if( is_gibbs ) call set_lambda_charges_cfg(cfgs(ib_pas))

    total_lambda_moves = total_lambda_moves + 1

    ! First check if lambda is moved outwith its natural range of between 0 and 1

    !TU**: Choose a better precision
    ! Or reject the move if lambda is outwith the hard limits on the paramter range
    if( lambda < -1.0E-9_wp .or. lambda > 1.000000001_wp ) then

        if( is_fed_param_lambda ) then

           !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_old'
           !TU: and 'param_cur'.
           !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
           ! Note that deltavb is just a dummy real here so the function can be called
           deltavb = fed_param_dbias2(oldlambda, oldlambda, .true.)

           !TU: Update the transition matrix as a certain move from oldlambda to oldlambda
           if( fed%method == FED_TM ) call fed_tm_inc(0.0_wp)

        end if

        goto 1000

    end if

    ! Note that we update the transition matrix above for a rejection due to moving outwith the
    ! natural range for lambda. However below we treat a move which takes lambda outside the specified
    ! FED range differently...

    deltavb = 0.0_wp

    ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
    ! of the FED range then there is a rejection at this point
    if( is_fed_param_lambda ) then

        deltavb = fed_param_dbias(ib,cfgs(ib))

        out_of_range = ( deltavb > job%toler )

        !TU**: Transition matrix is ignored for this move in this case! Same for other moves. Count
        !TU**: it differently?
        if( out_of_range ) goto 1000

    end if


    ! Get energy of system with new lambda

    if(job%uselist) then

        call total_energy(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                          npair, nthree, nang, nfour, nmany, next)

        ! Do the same as above for the passive box in Gibbs
        if( is_gibbs ) &
            call total_energy(ib_pas, job%coultype(ib_pas), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, newmany_pas, &
                          newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, nself_pas, ncrct_pas, &
                          nvdwn_pas, nvdwb_pas, npair_pas, nthree_pas, nang_pas, nfour_pas, nmany_pas, next_pas)

    else

        call total_energy_nolist(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec,  &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                          npair, nthree, nang, nfour, nmany, next, job%clist)

        ! Do the same as above for the passive box in Gibbs
        if( is_gibbs ) &
            call total_energy_nolist(ib_pas, job%coultype(ib_pas), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, newmany_pas, &
                          newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, nself_pas, ncrct_pas, &
                          nvdwn_pas, nvdwb_pas, npair_pas, nthree_pas, nang_pas, nfour_pas, nmany_pas, next_pas, job%clist)

    endif

    !if( newvdw + newext > vdwcap ) goto 1000 ! reject - new energy is too high

    if( job%coultype(ib) == 1 ) call total_recip(ib, newrcp, nrcp, newvir)

    volnew = volold

    newtotal = newrcp + newreal + newvdw + newthree + newpair + newang + newfour + newmany + newext + newmfa
    newenth  = newtotal + extpress * volnew*(slit_zfrac2-slit_zfrac1)

    !TU: Do the same as above for the passive box in Gibbs
    if( is_gibbs ) then

        volnew_pas = volold_pas

        if( job%coultype(ib_pas) == 1 ) call total_recip(ib_pas, newrcp_pas, nrcp_pas, newvir_pas)

        newtotal_pas = newrcp_pas + newreal_pas + newvdw_pas + newthree_pas + newpair_pas + newang_pas + &
                       newfour_pas + newmany_pas + newext_pas + newmfa_pas

        newenth_pas  = newtotal_pas + extpress * volnew_pas*(slit_zfrac2-slit_zfrac1)

    end if

    deltav  = newtotal - oldtotal

    ! deltavb if there were no biasing ('canonical') is deltavb_can...
    if( is_gibbs ) then

        !TU: In the Gibbs ensemble volume move we must also consider the contribution from both
        !TU: the active and the passive boxes
        deltav = deltav + newtotal_pas - oldtotal_pas
        deltavb_can = beta * deltav

    else

        deltavb_can = beta * deltav

    end if

    !TU: deltavb which includes biasing if biasing is in use
    deltavb = deltavb + deltavb_can

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltavb_can)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. is_fed_param_lambda ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window .and. is_fed_param_lambda ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    ! Acceptance/rejection
    if( duni() < exp(-deltavb) .and. abs(deltav) <= job%toler ) then

        ! Move is accepted...


        !AB: update the FED histogram & bias if needed (e.g. WL)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.true.)

        end if

        !accept update energies etc
        energytot(ib) = newtotal
        enthalpytot(ib) = newenth
        virialtot(ib) = newvir
        energyrcp(ib) = newrcp
        energyreal(ib) = newreal
        energyvdw(ib) = newvdw
        energypair(ib) = newpair
        energythree(ib) = newthree
        energyang(ib) = newang
        energyfour(ib) = newfour
        energymany(ib) = newmany
        energyext(ib) = newext
        energymfa(ib) = newmfa
        volume(ib) = volnew   !TU**: Need to remove volume stuff here since it is not changed

        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable.
        if( job%coultype(ib) == 1 ) call update_rcpsums(ib)

        !TU**: Need this??
        if( nmetpot > 0 ) call copy_density(ib)

        !TU: Do the same as above for the passive box in Gibbs
        if( is_gibbs ) then
            
            energytot(ib_pas) = newtotal_pas
            enthalpytot(ib_pas) = newenth_pas
            virialtot(ib_pas) = newvir_pas
            energyrcp(ib_pas) = newrcp_pas
            energyreal(ib_pas) = newreal_pas
            energyvdw(ib_pas) = newvdw_pas
            energypair(ib_pas) = newpair_pas
            energythree(ib_pas) = newthree_pas
            energyang(ib_pas) = newang_pas
            energyfour(ib_pas) = newfour_pas
            energymany(ib_pas) = newmany_pas
            energyext(ib_pas) = newext_pas
            energymfa(ib_pas) = newmfa_pas
            volume(ib_pas) = volnew_pas  !TU**: Need to remove volume stuff here since it is not changed

            !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
            !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
            !TU: variable.
            if( job%coultype(ib_pas) == 1 ) call update_rcpsums(ib_pas)


            !TU**: Need this??
            if( nmetpot > 0 ) call copy_density(ib_pas)
           
        end if

        successful_lambda_moves = successful_lambda_moves + 1

        !update component molecule energies
        if(job%lmoldata) then

            do im = 1, number_of_molecules

                emolrealnonb(ib,im) = nrealnonb(im)
                emolrealbond(ib,im) = nrealbond(im)
                emolrealself(ib,im) = nself(im)
                emolrealcrct(ib,im) = ncrct(im)
                emolrcp(ib,im) = nrcp(im)
                emolvdwn(ib,im) = nvdwn(im)
                emolvdwb(ib,im) = nvdwb(im)
                emolpair(ib,im) = npair(im)
                emolthree(ib,im) = nthree(im)
                emolang(ib,im) = nang(im)
                emolfour(ib,im) = nfour(im)
                emolmany(ib,im) = nmany(im)
                emolext(ib,im) = next(im)

                ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im) &
                        + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

                emoltot(ib,im) = ntot

                !TU: Do the same as above for the passive box in Gibbs
                if( is_gibbs ) then

                    emolrealnonb(ib_pas,im) = nrealnonb_pas(im)
                    emolrealbond(ib_pas,im) = nrealbond_pas(im)
                    emolrealself(ib_pas,im) = nself_pas(im)
                    emolrealcrct(ib_pas,im) = ncrct_pas(im)
                    emolrcp(ib_pas,im) = nrcp_pas(im)
                    emolvdwn(ib_pas,im) = nvdwn_pas(im)
                    emolvdwb(ib_pas,im) = nvdwb_pas(im)
                    emolpair(ib_pas,im) = npair_pas(im)
                    emolthree(ib_pas,im) = nthree_pas(im)
                    emolang(ib_pas,im) = nang_pas(im)
                    emolfour(ib_pas,im) = nfour_pas(im)
                    emolmany(ib_pas,im) = nmany_pas(im)
                    emolext(ib_pas,im) = next_pas(im)
                    
                    ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + &
                         nvdwb_pas(im) + npair_pas(im) + nthree_pas(im) + nang_pas(im) + nfour_pas(im) + &
                         nmany_pas(im) + next_pas(im)
                    
                    emoltot(ib_pas,im) = ntot_pas

                 end if

            enddo

        endif

        return

    end if


        ! Move rejected.... restore the initial state

1000    continue

        ! Restore lambda

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda)

        ! Restore any lambda-dependent charges in the system
        call set_lambda_charges_cfg(cfgs(ib))
        if( is_gibbs ) call set_lambda_charges_cfg(cfgs(ib_pas))

        !AB: update the FED histogram & bias if needed (e.g. WL)
        !AB: but revert the parameter state (value & id/index)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.false.)

        end if

        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable. We do NOT do this here since upon restoring lambda to the initial value we return
        !TU: to the initial rcpsum values.

end subroutine move_lambda_system


!> @brief
!> - Performs a lambda Monte Carlo move: attempts to change lambda assuming that the energy change of the whole
!>   system is equal to the energy change of a single (lambda-dependent) molecule, specified elsewhere. The target
!>   molecule is the first molecule in the box belonging to the target species - it is assumed that there is only
!>   one molecule belonging to the target species!
subroutine move_lambda_molecule(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext)


    use kinds_f90
    use species_module, only : number_of_molecules
    use control_type
    use cell_module, only : cfgs, nconfigs, select_molecule
    use field_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM, uout

    use metpot_module, only : nmetpot
    use random_module, only : duni
    use slit_module, only : slit_zfrac1, slit_zfrac2

    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject

    use molecule_module, only : store_mol_charges, restore_mol_charges

    use vdw_module, only : vdw_lrc_energy

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                      energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                      emolrealself(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules)

    integer :: im

    real(kind=wp) :: oldlambda, &
                     deltav, deltavb, deltavb_can, otot, ntot, &
                     oldreal, oldrcp, oldvdw, oldthree, oldpair, oldselfcoul, oldselfvdw, &
                     oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
                     newreal, newrcp, newvdw, newthree, newpair, newselfcoul, newselfvdw, &
                     newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), oself(number_of_molecules), &
                       ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), &
                       ncrct(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, do_ewald_rcp

    logical :: out_of_range, is_fed_param_lambda

    ! Flag specifying whether FED is in operation with the lambda order parameter
    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA

    oldlambda = lambda

    !TU: Select molecule to move: find the first molecule belonging to the target species
    mol_found = .false.
    do im = 1, cfgs(ib)%num_mols

        if(cfgs(ib)%mols(im)%mol_label == job%lambdamoltargettyp) then

            mol_found = .true.
            exit

        end if

    end do

    if( .not.mol_found ) then

        call cry(uout,'', &
            "ERROR: In lambda move molecule not found with target lambda-dependent species!", 999)

    endif

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.

    oldtotal = 0.0_wp
    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    oldrcp = energyrcp(ib)
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldfour = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp
    oldselfcoul = 0.0_wp
    oldselfvdw = 0.0_wp

    newtotal = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    !TU**: Possible bug?? What if do_ewald is not in effect but the rcp energy is not 0. This could cause issues below if newrcp is not initialised to energyrcp(ib)?
    newrcp = 0.0_wp      ! or oldrcp??
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newfour = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newselfcoul = 0.0_wp
    newselfvdw = 0.0_wp


    do_ewald_rcp = ( job%coultype(ib) == 1 )

    total_lambda_moves = total_lambda_moves + 1

    !get initial energy
    if(job%uselist) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                     oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext)


    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                     job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                     oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext, job%clist)


    endif

    !TU: Include the current TOTAL long-range contribution to the VdW energy to 'oldvdw'.
    !TU: Note that the long-range contribution will change with lambda, and hence this 
    !TU: change must be accounted for in the VdW energy change associated with the move 
    !TU: (newvdw-oldvdw). 
    oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)

    !TU**: Required for reciprocal energy calculation. Put in a conditional??
    call store_mol_charges(cfgs(ib)%mols(im))

    ! Change lambda

    ! Code for discrete changes in lambda
    if( duni() < 0.5_wp ) then
    
        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda - job%maxlambdachange)

    else

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda + job%maxlambdachange)
 
    end if

    ! Change any lambda-dependent charges in the molecule
    call set_lambda_charges_mol(cfgs(ib)%mols(im))


    ! First check if lambda is moved outwith its natural range of between 0 and 1

    !TU**: Choose a better precision
    ! Or reject the move if lambda is outwith the hard limits on the paramter range
    if( lambda < -1.0E-9_wp .or. lambda > 1.000000001_wp ) then

        if( is_fed_param_lambda ) then

           !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_old'
           !TU: and 'param_cur'.
           !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
           ! Note that deltavb is just a dummy real here so the function can be called
           deltavb = fed_param_dbias2(oldlambda, oldlambda, .true.)

           !TU: Update the transition matrix as a certain move from oldlambda to oldlambda
           if( fed%method == FED_TM ) call fed_tm_inc(0.0_wp)

        end if

        goto 1000

    end if

    ! Note that we update the transition matrix above for a rejection due to moving outwith the
    ! natural range for lambda. However below we treat a move which takes lambda outside the specified
    ! FED range differently...

    deltavb = 0.0_wp

    ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
    ! of the FED range then there is a rejection at this point
    if( is_fed_param_lambda ) then

        deltavb = fed_param_dbias(ib,cfgs(ib))

        out_of_range = ( deltavb > job%toler )

        !TU**: Transition matrix is ignored for this move in this case! Same for other moves. Count
        !TU**: it differently?
        if( out_of_range ) goto 1000

    end if

    ! Get  energy of molecule with new lambda

    !AB: use NL or not
    if( job%uselist ) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                     newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                     npair, nthree, nang, nfour, nmany, next)


    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                     job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                     newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                     npair, nthree, nang, nfour, nmany, next, job%clist)


    endif

    !TU: As with oldvdw above, add the long-range contribution to the VdW energy
    newvdw = newvdw + vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)


    !TU: move_molecule_charges_recip returns the new reciprocal energy given the changes in molecule charges, and sets
    !TU: the tmprcpsum variables to reflect the current charges (i.e. the charges in the system corresponding
    !TU: to the NEW lambda)
    if( do_ewald_rcp ) call move_molecule_charges_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    !sum the energies
    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldselfcoul &
             + oldpair + oldang + oldfour + oldext + oldmfa + oldselfvdw
    oldenth = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newmany + newselfcoul &
             + newpair + newang + newfour + newext + newmfa + newselfvdw
    newenth = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.
    deltav  = (newtotal - oldtotal)
    ! deltavb if there were no biasing ('canonical') is deltavb_can...
    deltavb_can = beta * deltav 
    ! deltavb which includes biasing if biasing is in use
    deltavb = deltavb + deltavb_can

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltavb_can)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. is_fed_param_lambda ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window .and. is_fed_param_lambda ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    ! Acceptance/rejection
    if( duni() < exp(-deltavb) .and. abs(deltav) <= job%toler ) then

        ! Move is accepted...

        !AB: update the FED histogram & bias if needed (e.g. WL)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.true.)

        end if

        !accept update energies etc
        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
        virialtot(ib) = virialtot(ib) + (newvir - oldvir)
        !energyrcp(ib) = newrcp ! Updated below...
        energyreal(ib) = energyreal(ib) + (newreal - oldreal) + (newselfcoul - oldselfcoul)
        energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw) + (newselfvdw - oldselfvdw)
        energypair(ib)  = energypair(ib)  + (newpair - oldpair)
        energyfour(ib)  = energyfour(ib)  + (newfour - oldfour)
        energythree(ib) = energythree(ib) + (newthree - oldthree)
        energyang(ib) = energyang(ib) + (newang - oldang)
        energymany(ib) = energymany(ib) + (newmany - oldmany)
        energyext(ib) = energyext(ib) + (newext - oldext)
        energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

        successful_lambda_moves = successful_lambda_moves + 1


        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable.

        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        endif

         if(job%lmoldata) then

             do im = 1, number_of_molecules

                 emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                 emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                 emolrealself(ib,im) = emolrealself(ib,im) + nself(im) - oself(im)
                 emolrealcrct(ib,im) = emolrealcrct(ib,im) + ncrct(im) - ocrct(im)
                 !orcp(im) = emolrcp(ib,im) !??
                 emolrcp(ib,im) = nrcp(im)
                 emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                 emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                 emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                 emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                 emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                 emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                 emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                 emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                 otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im)  &
                        + othree(im) + oang(im) + ofour(im) + omany(im) + oself(im) + ocrct(im) + oext(im)
                 ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)  &
                        + nthree(im) + nang(im) + nfour(im) + nmany(im) + nself(im) + ncrct(im) + next(im)

                 emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)


             enddo

         endif

         return

    endif


        ! Move rejected.... restore the initial state

1000    continue

        ! Restore lambda

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda)

        ! Restore any lambda-dependent charges in the system
        call set_lambda_charges_mol(cfgs(ib)%mols(im))

        !AB: update the FED histogram & bias if needed (e.g. WL)
        !AB: but revert the parameter state (value & id/index)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.false.)

        end if

        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable. We do NOT do this here since upon restoring lambda to the initial value we return
        !TU: to the initial rcpsum values.


end subroutine move_lambda_molecule



! Code for obsolete version of the fractional insertion move which was separate to normal lambda moves. This
! move worked, but was convoluted compared to its replacement below. I leave it commented out below in case
! it proves useful in the future - perhaps for some problems this approach is better?
!
!!> Move for insertion of a fractional molecule versus transformation of a real molecule into a fractional one. 
!!> If lambda is 1 then this move changes the fractional molecule to a real molecule, sets lambda to 0, and 
!!> inserts a new fractional molecule (with lambda=0) into the system. If lambda is 0 then this move deletes
!!> the fractional molecule, sets lambda=1, and changes a real molecule into a fractional molecule (with 
!!> lambda=1). **This move assumes that the two species tagged as fractional and real for this move have 
!!> identical interactions in the system if lambda=1, in which case the move yields no energy change in the 
!!> system, only a change in lambda and the number of real and fractional molecules.**
!subroutine move_lambda_insert_molecule_old(ib, job)
!
!    use kinds_f90
!    use control_type
!    use cell_module, only : insert_molecule, remove_molecule, select_molecule, transform_molecule
!    use config_type
!    use random_module, only : duni
!    use species_module, only : uniq_mol
!    use cell_list_wrapper_module
!    use constants_module, only :  FED_PAR_LAMBDA, FED_TM
!    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
!    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject
!
!        !> Box number
!    integer, intent(in) :: ib
!
!        !> Simulation control parameters
!    type(control), intent(in) :: job
!
!        ! Tolerence on checks for lambda to be 0 or 1
!    real(kind = wp), parameter :: tol = 1.0E-9
!
!    integer :: typreal, typfrac, nmols, imfrac, imreal, fail, ii
!
!    real(kind = wp) :: arg, deltavb
!
!    logical :: mol_found, rejection, is_fed_param_lambda
!
!    ! Required by insert_molecule procedure
!    logical :: by_com, atom_out
!
!    ! Flag specifying whether FED is in operation with the lambda order parameter
!    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA
!
!
!    total_lambda_inserts = total_lambda_inserts + 1
!
!    typreal = job%lambdainsert_realtyp
!    typfrac = job%lambdainsert_fractyp
!    nmols = cfgs(ib)%mtypes(typreal)%num_mols
!
!
!    ! Locate the fractional molecule
!    mol_found = .false.
!    do imfrac = 1, cfgs(ib)%num_mols
!    
!        if(cfgs(ib)%mols(imfrac)%mol_label == typfrac) then
!    
!            mol_found = .true.
!            exit
!    
!        end if
!    
!    end do
!    if( .not.mol_found ) then
!    
!        !TU**: THROW ERROR!
!        write(0,*) "ERROR: fractional molecule not found!"
!        stop 1            
!    
!    endif
!
!
!    if( lambda > 1.0_wp-tol ) then
!
!        ! lambda = 1: transform fractional molecule to real molecule and insert new lambda=0 fractional molecule
!
!
!        ! Ensure we don't exceed the array limits
!        if( nmols > cfgs(ib)%mxmol-1 .or. nmols > cfgs(ib)%mxmol_type(typreal)-1 ) then
!        
!            !TU**: THROW ERROR
!            write(0,*) "ERROR: maxmol exceeded!"
!            stop 1            
!
!        end if
!
!
!        ! The move in earnest...
!
!        ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
!        ! of the FED range then there is a rejection at this point
!        deltavb = 0.0_wp
!        rejection = .false.
!
!        if( is_fed_param_lambda ) then
!
!            deltavb = fed_param_dbias2(lambda, 0.0_wp,.true.)
!
!            !TU**: Is it possible that out_of_range could be true even if the move is not out of range?
!            rejection = ( deltavb > job%toler )
!
!            !TU**: NB: Below the transition matrix is ignored for this move in this case! Same for other moves.
!            !TU**: Count it differently?
!
!        end if
!
!        ! Decide whether or not the move is to be accepted, and if it is, perform the species transformation
!        ! and insertion of a new fractional molecule below. This is possible because the change in energy
!        ! associated with the move is 0.
!
!        ! Below 'arg' is the probability of acceptance WITHOUT BIASING...
!        arg = ( cfgs(ib)%vec%volume / (nmols + 1) ) * job%lambdainsert_activity
!
!        ! ... which we use to update the transition matrix if it is in use...
!
!        ! Update transition matrix if it is in use regardless of the  acceptance/rejection of the move
!        ! Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
!        ! and hence the transition matrix is only updated for in bounds trial states.
!        if( fed % method == FED_TM .and. is_fed_param_lambda .and. .not. rejection ) then
!        
!            call fed_tm_inc(  min( 1.0_wp, arg )  )
!
!        end if
!
!        ! Reject the move if it leaves the window or takes us further from the window                          
!        if( fed % is_window .and. is_fed_param_lambda ) then
!       
!           if( fed_window_reject() ) rejection = .true.
!       
!        end if
!
!        ! Below 'arg' is the probability of acceptance WITH BIASING, which is used to determine acceptance/
!        ! rejection below
!        arg = arg * exp(-deltavb)
!
!        if( duni() < arg .and. .not. rejection ) then
!
!            ! The (insertion) move is accepted, so make the changes...
!
!            ! Update the FED histogram & bias if needed (e.g. WL)
!            if( is_fed_param_lambda ) then
!
!                call fed_param_hist_inc(ib,.true.)
!
!            end if
!
!            successful_lambda_inserts = successful_lambda_inserts + 1
!            ! Transform the fractional molecule into a real molecule
!            call transform_molecule(ib, imfrac, typreal)
!
!            ! Insert a new fractional molecule into the system
!
!            ! by_com determines the nature of the internal degrees of freedom of the molecule upon insertion.
!            ! Here I follow the convention used in gcmc_mols in gcmc_module
!            by_com = ( uniq_mol(typfrac)%natom > 1 .and. &
!                     ( uniq_mol(typfrac)%rigid_body .or. uniq_mol(typfrac)%blist%npairs > 0 ) )
!
!            ! ** Note that imfrac is set to the molecule index of the inserted molecule by the below line **
!            call insert_molecule(ib, typfrac, imfrac, atom_out, by_com)
!
!            if(atom_out) then
!
!                !TU**: THROW ERROR!
!                write(0,*) "ERROR: atom_out!"
!                stop 1            
!
!            end if
!
!            ! Set the cells of the atoms in the molecule and insert them into the cell list if applicable
!            if(job%clist) call molecule_set_cells(ib, imfrac, .true.)
!
!            ! Change lambda
!            call set_lambda(0.0_wp)
!            ! Change any lambda-dependent charges in the molecule
!            call set_lambda_charges_mol(cfgs(ib)%mols(imfrac))
!
!
!        else
!
!
!            ! Move is rejected; gather statistics but do not change the system...
!
!            ! Update the FED histogram & bias if needed (e.g. WL)
!            ! but revert the parameter state (value & id/index)
!            if( is_fed_param_lambda ) then
!
!                call fed_param_hist_inc(ib,.false.)
!
!            end if
!
!        end if
!
!
!    else if( lambda < tol ) then
!
!        ! lamdba = 0: delete lambda=0 fractional molecule and transform a real molecule to lambda=1 
!        ! fractional molecule
!
!        if( nmols > 0 ) then
!
!            ! If nmols==0 then do nothing: there are no real molecules to transform
!
!            !TU**: Need to worry about transition matrix in this case?
!
!            ! The move in earnest...
!
!            ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
!            ! of the FED range then there is a rejection at this point
!            deltavb = 0.0_wp
!            rejection = .false.
!
!            if( is_fed_param_lambda ) then
!
!                deltavb = fed_param_dbias2(lambda, 1.0_wp,.true.)
! 
!                !TU**: Is it possible that out_of_range could be true even if the move is not out of range?
!                rejection = ( deltavb > job%toler )
!
!                !TU**: NB: Below the transition matrix is ignored for this move in this case! Same for other moves.
!                !TU**: Count it differently?
!
!            end if
!
!
!            ! Decide whether or not the move is to be accepted, and if it is, perform the species transformation
!            ! and deletion of a the fractional molecule. This is possible because the change in energy associated
!            ! with the move is 0.
!
!            ! Below 'arg' is the probability of acceptance WITHOUT BIASING...
!            arg = ( nmols / cfgs(ib)%vec%volume ) / job%lambdainsert_activity
!
!            ! ... which we use to update the transition matrix if it is in use...
!
!            ! Update transition matrix if it is in use regardless of the  acceptance/rejection of the move
!            ! Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
!            ! and hence the transition matrix is only updated for in bounds trial states.
!            if( fed % method == FED_TM .and. is_fed_param_lambda .and. .not. rejection ) then
!        
!                call fed_tm_inc(  min( 1.0_wp, arg )  )
!
!            end if
!
!            ! Reject the move if it leaves the window or takes us further from the window                          
!            if( fed % is_window .and. is_fed_param_lambda ) then
!       
!               if( fed_window_reject() ) rejection = .true.
!       
!            end if
!
!            ! Below 'arg' is the probability of acceptance WITH BIASING, which is used to determine acceptance/
!            ! rejection below
!            arg = arg * exp(-deltavb)
!
!            if( duni() < arg .and. .not. rejection) then
!
!                ! The (deletion) move is accepted, so make the changes...
!
!                ! Update the FED histogram & bias if needed (e.g. WL)
!                if( is_fed_param_lambda ) then
!
!                    call fed_param_hist_inc(ib,.true.)
!
!                end if
!
!                successful_lambda_inserts = successful_lambda_inserts + 1
!
!                ! Delete the fractional molecule
!
!                ! Remove atoms in molecule from cell list (before the molecule is deallocated!)
!                if(job%clist) call molecule_remove_from_cell_list(ib, imfrac, .true.)
!
!                call remove_molecule(ib, imfrac, fail)
!
!                if(fail > 0) then
!
!                    !TU**: THROW ERROR!
!                    write(0,*) "ERROR: failure upon removing molecule!",fail
!                    stop 1            
!
!                end if
!
!                ! Transform a random real molecule into a fractional molecule
!                call select_molecule(typreal, ib, imreal)
!
!                call transform_molecule(ib, imreal, typfrac)
!
!                ! Change lambda
!                call set_lambda(1.0_wp)
!                ! Change any lambda-dependent charges in the molecule
!                call set_lambda_charges_mol(cfgs(ib)%mols(imreal))
!
!
!            else
!
!
!                ! Move is rejected; gather statistics but do not change the system...
!
!
!                ! Update the FED histogram & bias if needed (e.g. WL)
!                ! but revert the parameter state (value & id/index)
!                if( is_fed_param_lambda ) then
!
!                    call fed_param_hist_inc(ib,.false.)
! 
!                end if
!
!             end if
!
!          end if
!        
!
!    else
!
!        ! lambda is not 0 or 1: this is logged as an empty move
!
!        empty_lambda_inserts = empty_lambda_inserts + 1
!
!
!        if( is_fed_param_lambda ) then
!
!            ! Update statistics pertaining to FED...
!
!            !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_old'
!            !TU: and 'param_cur'.
!            !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
!            ! Note that deltavb is just a dummy real here so the function can be called
!            deltavb = fed_param_dbias2(lambda, lambda, .true.)
!
!            !TU: Update the transition matrix as a certain move from lambda to lambda
!            if( fed%method == FED_TM ) call fed_tm_inc(0.0_wp)
!
!            ! Update the FED histogram & bias if needed (e.g. WL)
!            ! but revert the parameter state (value & id/index)
!            call fed_param_hist_inc(ib,.false.)
!
!        end if
!
!
!     end if
!
!
!end subroutine move_lambda_insert_molecule_old



!> @brief
!> - Performs a lambda move for continuous fractional component Monte Carlo in the grand canonical ensemble. 
!>   If a change in lambda takes lambda outwith the range [0,1] then a molecule is inserted or deleted... If
!>   the change in lambda brings lambda above 1, then the fractional molecule is promoted to a 'real' molecule 
!>   and a new molecule with lambda=0 is inserted into the system. If the change in lambda brings lambda to
!>   below 0, then the (lambda=0) fractional molecule is deleted from the system and a randomly chosen 'real'
!>   molecule is 'demoted' to a fractional molecule with lambda=1. If the change in lambda takes lambda 
!>   within the range [0,1] then a move akin to 'move_lambda_molecule' above is performed.
!> - Note that it is assumed here that the allowed lambda (allowed by the choice of initial lambda and
!>   maximum change in lambda for a move) are equally spaced real numbers between 0 and 1 (inclusive), and
!>   include 0 and 1, e.g. [0.0,0.25,0.5,0.75,1.0] or [0.0,0.5,1.0].
subroutine move_lambda_insert_molecule(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext)


    use kinds_f90
    use species_module, only : number_of_molecules
    use control_type
    use cell_module, only : cfgs, nconfigs, select_molecule
    use field_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM

    use metpot_module, only : nmetpot
    use random_module, only : duni
    use slit_module, only : slit_zfrac1, slit_zfrac2

    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject

    use molecule_module, only : store_mol_charges, restore_mol_charges

    use vdw_module, only : vdw_lrc_energy

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                      energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                      emolrealself(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules)

        ! Tolerence on checks for lambda to be 0 or 1
    real(kind = wp), parameter :: tol = 1.0E-9

    real(kind = wp) :: newlambda

    ! Generate new lambda and decide on move type: 1) lambda insertion if we go above lambda=1;
    ! 2) lambda deletion if we go below lambda=0; 3) standard lambda change otherwise

    ! Code for discrete changes in lambda
    if( duni() < 0.5_wp ) then

        newlambda = lambda -  job%maxlambdachange   

    else

        newlambda = lambda + job%maxlambdachange
 
    end if

    if( newlambda > 1.0_wp+tol ) then

        call lambda_insert_molecule(ib, job)

    else if( newlambda < -tol ) then

        call lambda_delete_molecule(ib, job)

    else

        call lambda_standard_molecule(newlambda, &
                      ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext)
        
    end if

end subroutine move_lambda_insert_molecule


!> @brief
!> - Branch of a lambda insert/delete move for the case where lambda goes above 1, in which
!>   case the (lambda=1) fractional molecule is promoted to a real molecule and a new fractional molecule with 
!>   lambda=0 is inserted into the system.
!> - Assumes lambda=1 currently, and assumes that the energy of the lambda=0 fractional molecule is 0
!>   upon insertion, and the energy change associated with promoting the lambda=1 fractional molecule to a
!>   real molecule is exactly 0 (including tail corrections)!
subroutine lambda_insert_molecule(ib, job)

    use kinds_f90
    use control_type
    use cell_module, only : insert_molecule, remove_molecule, select_molecule, transform_molecule
    use config_type
    use random_module, only : duni
    use species_module, only : uniq_mol
    use cell_list_wrapper_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM
    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject

        !> Box number
    integer, intent(in) :: ib

        !> Simulation control parameters
    type(control), intent(in) :: job

        ! Tolerence on checks for lambda to be 0 or 1
    real(kind = wp), parameter :: tol = 1.0E-9

    integer :: typreal, typfrac, nmols, imfrac, imreal, fail, ii

    real(kind = wp) :: arg, deltavb

    logical :: mol_found, rejection, is_fed_param_lambda

    ! Required by insert_molecule procedure
    logical :: by_com, atom_out

    ! Flag specifying whether FED is in operation with the lambda order parameter
    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA


    total_lambda_inserts = total_lambda_inserts + 1

    typreal = job%lambdainsert_realtyp
    typfrac = job%lambdainsert_fractyp
    nmols = cfgs(ib)%mtypes(typreal)%num_mols


    ! Locate the fractional molecule
    mol_found = .false.
    do imfrac = 1, cfgs(ib)%num_mols
    
        if(cfgs(ib)%mols(imfrac)%mol_label == typfrac) then
    
            mol_found = .true.
            exit
    
        end if
    
    end do
    if( .not.mol_found ) then
    
        call cry(uout, '', &
            "ERROR: In lambda insert move molecule with target lambda-dependent species not found!",999)
    
    endif


        ! lambda = 1: transform fractional molecule to real molecule and insert new lambda=0 fractional molecule


        ! Ensure we don't exceed the array limits
        if( nmols > cfgs(ib)%mxmol-1 .or. nmols > cfgs(ib)%mxmol_type(typreal)-1 ) then
        
            call cry(uout, '', &
                "ERROR: In lambda insert move molecule maxmol was exceeded!",999)

        end if


        ! The move in earnest...

        ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
        ! of the FED range then there is a rejection at this point
        deltavb = 0.0_wp
        rejection = .false.

        if( is_fed_param_lambda ) then

            deltavb = fed_param_dbias2(lambda, 0.0_wp,.true.)

            !TU**: Is it possible that out_of_range could be true even if the move is not out of range?
            rejection = ( deltavb > job%toler )

            !TU**: NB: Below the transition matrix is ignored for this move in this case! Same for other moves.
            !TU**: Count it differently?

        end if

        ! Decide whether or not the move is to be accepted, and if it is, perform the species transformation
        ! and insertion of a new fractional molecule below. This is possible because the change in energy
        ! associated with the move is 0.

        ! Below 'arg' is the probability of acceptance WITHOUT BIASING...
        arg = ( cfgs(ib)%vec%volume / (nmols + 1) ) * job%lambdainsert_activity

        ! ... which we use to update the transition matrix if it is in use...

        ! Update transition matrix if it is in use regardless of the  acceptance/rejection of the move
        ! Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
        ! and hence the transition matrix is only updated for in bounds trial states.
        if( fed % method == FED_TM .and. is_fed_param_lambda .and. .not. rejection ) then
        
            call fed_tm_inc(  min( 1.0_wp, arg )  )

        end if

        ! Reject the move if it leaves the window or takes us further from the window                          
        if( fed % is_window .and. is_fed_param_lambda ) then
       
           if( fed_window_reject() ) rejection = .true.
       
        end if

        ! Below 'arg' is the probability of acceptance WITH BIASING, which is used to determine acceptance/
        ! rejection below
        arg = arg * exp(-deltavb)

        if( duni() < arg .and. .not. rejection ) then

            ! The (insertion) move is accepted, so make the changes...

            ! Update the FED histogram & bias if needed (e.g. WL)
            if( is_fed_param_lambda ) then

                call fed_param_hist_inc(ib,.true.)

            end if

            successful_lambda_inserts = successful_lambda_inserts + 1
            ! Transform the fractional molecule into a real molecule
            call transform_molecule(ib, imfrac, typreal)

            ! Insert a new fractional molecule into the system

            ! by_com determines the nature of the internal degrees of freedom of the molecule upon insertion.
            ! Here I follow the convention used in gcmc_mols in gcmc_module
            by_com = ( uniq_mol(typfrac)%natom > 1 .and. &
                     ( uniq_mol(typfrac)%rigid_body .or. uniq_mol(typfrac)%blist%npairs > 0 ) )

            ! ** Note that imfrac is set to the molecule index of the inserted molecule by the below line **
            call insert_molecule(ib, typfrac, imfrac, atom_out, by_com)

            if(atom_out) then

                call cry(uout, '', &
                    "ERROR: Atom inserted outside slit in lambda insert move!",999)
           

            end if

            ! Set the cells of the atoms in the molecule and insert them into the cell list if applicable
            if(job%clist) call molecule_set_cells(ib, imfrac, .true.)

            ! Change lambda
            call set_lambda(0.0_wp)
            ! Change any lambda-dependent charges in the molecule
            call set_lambda_charges_mol(cfgs(ib)%mols(imfrac))


        else


            ! Move is rejected; gather statistics but do not change the system...

            ! Update the FED histogram & bias if needed (e.g. WL)
            ! but revert the parameter state (value & id/index)
            if( is_fed_param_lambda ) then

                call fed_param_hist_inc(ib,.false.)

            end if

        end if

end subroutine lambda_insert_molecule


!> @brief
!> - Branch of a lambda insert/delete move for the case where lambda goes below 0, in which
!>   case the (lambda=0) fractional molecule is deleted and a real molecule is transformed into a fractional
!>   molecule with lambda=1.
!> - Assumes lambda=0 currently, and assumes that the energy of the lambda=0 fractional molecule is 0.
!>   Also assumes that the energy change associated with demonting the real molecule to a lambda=1 fractional 
!>   molecule real molecule is exactly 0 (including tail corrections)!
subroutine lambda_delete_molecule(ib, job)

    use kinds_f90
    use control_type
    use cell_module, only : insert_molecule, remove_molecule, select_molecule, transform_molecule
    use config_type
    use random_module, only : duni
    use species_module, only : uniq_mol
    use cell_list_wrapper_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM
    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject

        !> Box number
    integer, intent(in) :: ib

        !> Simulation control parameters
    type(control), intent(in) :: job

        ! Tolerence on checks for lambda to be 0 or 1
    real(kind = wp), parameter :: tol = 1.0E-9

    integer :: typreal, typfrac, nmols, imfrac, imreal, fail, ii

    real(kind = wp) :: arg, deltavb

    logical :: mol_found, rejection, is_fed_param_lambda

    ! Required by insert_molecule procedure
    logical :: by_com, atom_out

    ! Flag specifying whether FED is in operation with the lambda order parameter
    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA


    total_lambda_deletes = total_lambda_deletes + 1

    typreal = job%lambdainsert_realtyp
    typfrac = job%lambdainsert_fractyp
    nmols = cfgs(ib)%mtypes(typreal)%num_mols


    ! Locate the fractional molecule
    mol_found = .false.
    do imfrac = 1, cfgs(ib)%num_mols
    
        if(cfgs(ib)%mols(imfrac)%mol_label == typfrac) then
    
            mol_found = .true.
            exit
    
        end if
    
    end do
    if( .not.mol_found ) then
    
        call cry(uout,'', &
            "ERROR: In lambda delete move molecule not found with target lambda-dependent species!", 999)

    
    endif



        ! lamdba = 0: delete lambda=0 fractional molecule and transform a real molecule to lambda=1 
        ! fractional molecule

        if( nmols > 0 ) then

            ! If nmols==0 then do nothing: there are no real molecules to transform

            !TU**: Need to worry about transition matrix in this case?

            ! The move in earnest...

            ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
            ! of the FED range then there is a rejection at this point
            deltavb = 0.0_wp
            rejection = .false.

            if( is_fed_param_lambda ) then

                deltavb = fed_param_dbias2(lambda, 1.0_wp,.true.)
 
                !TU**: Is it possible that out_of_range could be true even if the move is not out of range?
                rejection = ( deltavb > job%toler )

                !TU**: NB: Below the transition matrix is ignored for this move in this case! Same for other moves.
                !TU**: Count it differently?

            end if


            ! Decide whether or not the move is to be accepted, and if it is, perform the species transformation
            ! and deletion of a the fractional molecule. This is possible because the change in energy associated
            ! with the move is 0.

            ! Below 'arg' is the probability of acceptance WITHOUT BIASING...
            arg = ( nmols / cfgs(ib)%vec%volume ) / job%lambdainsert_activity

            ! ... which we use to update the transition matrix if it is in use...

            ! Update transition matrix if it is in use regardless of the  acceptance/rejection of the move
            ! Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
            ! and hence the transition matrix is only updated for in bounds trial states.
            if( fed % method == FED_TM .and. is_fed_param_lambda .and. .not. rejection ) then
        
                call fed_tm_inc(  min( 1.0_wp, arg )  )

            end if

            ! Reject the move if it leaves the window or takes us further from the window                          
            if( fed % is_window .and. is_fed_param_lambda ) then
       
               if( fed_window_reject() ) rejection = .true.
       
            end if

            ! Below 'arg' is the probability of acceptance WITH BIASING, which is used to determine acceptance/
            ! rejection below
            arg = arg * exp(-deltavb)

            if( duni() < arg .and. .not. rejection) then

                ! The (deletion) move is accepted, so make the changes...

                ! Update the FED histogram & bias if needed (e.g. WL)
                if( is_fed_param_lambda ) then

                    call fed_param_hist_inc(ib,.true.)

                end if

                successful_lambda_deletes = successful_lambda_deletes + 1

                ! Delete the fractional molecule

                ! Remove atoms in molecule from cell list (before the molecule is deallocated!)
                if(job%clist) call molecule_remove_from_cell_list(ib, imfrac, .true.)

                call remove_molecule(ib, imfrac, fail)

                if(fail > 0) then

                    call cry(uout,'', &
                        "ERROR: Failure encountered during lambda delete move while removing molecule!", 999)     

                end if

                ! Transform a random real molecule into a fractional molecule
                call select_molecule(typreal, ib, imreal)

                call transform_molecule(ib, imreal, typfrac)

                ! Change lambda
                call set_lambda(1.0_wp)
                ! Change any lambda-dependent charges in the molecule
                call set_lambda_charges_mol(cfgs(ib)%mols(imreal))


            else


                ! Move is rejected; gather statistics but do not change the system...


                ! Update the FED histogram & bias if needed (e.g. WL)
                ! but revert the parameter state (value & id/index)
                if( is_fed_param_lambda ) then

                    call fed_param_hist_inc(ib,.false.)
 
                end if

             end if

          end if


end subroutine lambda_delete_molecule


!TU**: Repeated code: The 'lambda_standard_molecule' procedure is identical to most of 'move_lambda_molecule',
!TU**: except that it uses job%lambdainsert_fractyp as the fractional species instead of job%lambdamoltargettyp.
!TU**: This repeated code could easily be removed by refactoring.

!> @brief
!> - Branch of a lambda insert/delete move for the case where lambda does not go below 0 or above 1. Here,
!>   it is assumed that there is only one fractional molecule in the system - something exploited to calculate the
!>   energy change efficiently.
!> - The code for this move is almost identical to that in 'move_lambda_molecule'
!> - Assumes lambda=0 currently, and assumes that the energy of the lambda=0 fractional molecule is 0.
!>   Also assumes that the energy change associated with demonting the real molecule to a lambda=1 fractional 
!>   molecule real molecule is exactly 0 (including tail corrections)!
subroutine lambda_standard_molecule(newlambda, ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext)


    use kinds_f90
    use species_module, only : number_of_molecules
    use control_type
    use cell_module, only : cfgs, nconfigs, select_molecule
    use field_module
    use constants_module, only :  FED_PAR_LAMBDA, FED_TM

    use metpot_module, only : nmetpot
    use random_module, only : duni
    use slit_module, only : slit_zfrac1, slit_zfrac2

    use fed_calculus_module, only : fed, fed_tm_inc      , id_old, id_cur
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_window_reject

    use molecule_module, only : store_mol_charges, restore_mol_charges

    use vdw_module, only : vdw_lrc_energy

    implicit none

    real(kind = wp) :: newlambda

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                      energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                      emolrealself(nconfigs,number_of_molecules), emolrealcrct(nconfigs,number_of_molecules)

    integer :: im

    real(kind=wp) :: oldlambda, &
                     deltav, deltavb, deltavb_can, otot, ntot, &
                     oldreal, oldrcp, oldvdw, oldthree, oldpair, oldselfcoul, oldselfvdw, &
                     oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
                     newreal, newrcp, newvdw, newthree, newpair, newselfcoul, newselfvdw, &
                     newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir

     !temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), oself(number_of_molecules), &
                       ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), &
                       ncrct(number_of_molecules), next(number_of_molecules)

    logical :: mol_found, do_ewald_rcp

    logical :: out_of_range, is_fed_param_lambda

    ! Flag specifying whether FED is in operation with the lambda order parameter
    is_fed_param_lambda = fed%par_kind == FED_PAR_LAMBDA

    oldlambda = lambda

    !TU: Select molecule to move: find the first molecule belonging to the target species
    mol_found = .false.
    do im = 1, cfgs(ib)%num_mols
                     
        if(cfgs(ib)%mols(im)%mol_label == job%lambdainsert_fractyp) then

            mol_found = .true.
            exit

        end if

    end do

    if( .not.mol_found ) then

        call cry(uout,'', &
            "ERROR: In lambda move molecule not found with target lambda-dependent species!", 999)

    endif

    !AB: reduce the number of repetitive checks
    do_ewald_rcp = .false.

    oldtotal = 0.0_wp
    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    oldrcp = energyrcp(ib)
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldfour = 0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp
    oldselfcoul = 0.0_wp
    oldselfvdw = 0.0_wp

    newtotal = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    !TU**: Possible bug?? What if do_ewald is not in effect but the rcp energy is not 0. This could cause issues below if newrcp is not initialised to energyrcp(ib)?
    newrcp = 0.0_wp      ! or oldrcp??
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newfour = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newselfcoul = 0.0_wp
    newselfvdw = 0.0_wp


    do_ewald_rcp = ( job%coultype(ib) == 1 )

    total_lambda_moves = total_lambda_moves + 1

    !get initial energy
    if(job%uselist) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                     oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext)


    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                     job%dielec, job%vdw_cut, job%shortrangecut, &
                     oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                     oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                     orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                     opair, othree, oang, ofour, omany, oext, job%clist)


    endif

    !TU: Include the current TOTAL long-range contribution to the VdW energy to 'oldvdw'.
    !TU: Note that the long-range contribution will change with lambda, and hence this 
    !TU: change must be accounted for in the VdW energy change associated with the move 
    !TU: (newvdw-oldvdw). 
    oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)

    !TU**: Required for reciprocal energy calculation. Put in a conditional??
    call store_mol_charges(cfgs(ib)%mols(im))

    ! Change lambda

    call set_lambda(newlambda)

    ! Change any lambda-dependent charges in the molecule
    call set_lambda_charges_mol(cfgs(ib)%mols(im))

    deltavb = 0.0_wp

    ! Calculate the FED bias associated with moving from the old to new lambda. If the new lambda is out
    ! of the FED range then there is a rejection at this point
    if( is_fed_param_lambda ) then

        deltavb = fed_param_dbias(ib,cfgs(ib))

        out_of_range = ( deltavb > job%toler )

        !TU**: Transition matrix is ignored for this move in this case! Same for other moves. Count
        !TU**: it differently?
        if( out_of_range ) goto 1000

    end if

    ! Get  energy of molecule with new lambda

    !AB: use NL or not
    if( job%uselist ) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                     newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                     npair, nthree, nang, nfour, nmany, next)


    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                     job%dielec, job%vdw_cut, job%shortrangecut, &
                     newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                     newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                     npair, nthree, nang, nfour, nmany, next, job%clist)


    endif

    !TU: As with oldvdw above, add the long-range contribution to the VdW energy
    newvdw = newvdw + vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)


    !TU: move_molecule_charges_recip returns the new reciprocal energy given the changes in molecule charges, and sets
    !TU: the tmprcpsum variables to reflect the current charges (i.e. the charges in the system corresponding
    !TU: to the NEW lambda)
    if( do_ewald_rcp ) call move_molecule_charges_recip(ib, im, newrcp, nrcp)!, job%lmoldata)

    !sum the energies
    oldtotal = oldreal + oldrcp + oldvdw + oldthree + oldmany + oldselfcoul &
             + oldpair + oldang + oldfour + oldext + oldmfa + oldselfvdw
    oldenth = oldtotal + extpress * cfgs(ib)%vec%volume

    newtotal = newreal + newrcp + newvdw + newthree + newmany + newselfcoul &
             + newpair + newang + newfour + newext + newmfa + newselfvdw
    newenth = newtotal + extpress * cfgs(ib)%vec%volume

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not COM or PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. volume as the order parameter, since
    !TU: the volume is unchanged by a translation move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.
    deltav  = (newtotal - oldtotal)
    ! deltavb if there were no biasing ('canonical') is deltavb_can...
    deltavb_can = beta * deltav 
    ! deltavb which includes biasing if biasing is in use
    deltavb = deltavb + deltavb_can

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltavb_can)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. is_fed_param_lambda ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window .and. is_fed_param_lambda ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    ! Acceptance/rejection
    if( duni() < exp(-deltavb) .and. abs(deltav) <= job%toler ) then

        ! Move is accepted...

        !AB: update the FED histogram & bias if needed (e.g. WL)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.true.)

        end if

        !accept update energies etc
        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
        virialtot(ib) = virialtot(ib) + (newvir - oldvir)
        !energyrcp(ib) = newrcp ! Updated below...
        energyreal(ib) = energyreal(ib) + (newreal - oldreal) + (newselfcoul - oldselfcoul)
        energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw) + (newselfvdw - oldselfvdw)
        energypair(ib)  = energypair(ib)  + (newpair - oldpair)
        energyfour(ib)  = energyfour(ib)  + (newfour - oldfour)
        energythree(ib) = energythree(ib) + (newthree - oldthree)
        energyang(ib) = energyang(ib) + (newang - oldang)
        energymany(ib) = energymany(ib) + (newmany - oldmany)
        energyext(ib) = energyext(ib) + (newext - oldext)
        energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

        successful_lambda_moves = successful_lambda_moves + 1


        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable.

        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        endif

         if(job%lmoldata) then

             do im = 1, number_of_molecules

                 emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                 emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                 emolrealself(ib,im) = emolrealself(ib,im) + nself(im) - oself(im)
                 emolrealcrct(ib,im) = emolrealcrct(ib,im) + ncrct(im) - ocrct(im)
                 !orcp(im) = emolrcp(ib,im) !??
                 emolrcp(ib,im) = nrcp(im)
                 emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                 emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                 emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                 emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                 emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                 emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                 emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                 emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                 otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im)  &
                        + othree(im) + oang(im) + ofour(im) + omany(im) + oself(im) + ocrct(im) + oext(im)
                 ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)  &
                        + nthree(im) + nang(im) + nfour(im) + nmany(im) + nself(im) + ncrct(im) + next(im)

                 emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)


             enddo

         endif

         return

    endif


        ! Move rejected.... restore the initial state

1000    continue

        ! Restore lambda

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(oldlambda)

        ! Restore any lambda-dependent charges in the system
        call set_lambda_charges_mol(cfgs(ib)%mols(im))

        !AB: update the FED histogram & bias if needed (e.g. WL)
        !AB: but revert the parameter state (value & id/index)
        if( is_fed_param_lambda ) then

            call fed_param_hist_inc(ib,.false.)

        end if

        !TU: The charges of the atoms have changed, and hence so also have the rcpsums (currently stored
        !TU: in tmprcpsum calculated by total_recip above). update_rcpsums transfers them to the rcpsums
        !TU: variable. We do NOT do this here since upon restoring lambda to the initial value we return
        !TU: to the initial rcpsum values.


end subroutine lambda_standard_molecule







end module lambda_moves
