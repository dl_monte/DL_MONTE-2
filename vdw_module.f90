! *******************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                                   *
! *   john.purton[@]stfc.ac.uk                                                  *
! *                                                                             *
! *   Contributors:                                                             *
! *   -------------                                                             *
! *   A.V.Brukhno (C) 2015-2016                                                 *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *   - overall optimizations (general MC scheme, energy calculus and MC steps) *
! *   - Free Energy Difference (FED) & order parameters (fed_*_module.f90)      *
! *   - Replica Exchange (RE) algorithm (rep_exchange_module.f90)               *
! *   - VdW and general potential forms (vdw_*_module.f90 etc)                  *
! *   - planar pore constraint (slit_module.f90)                                *
! *   - USER defined analytical forms for force-fields (vdw_*.f90 & user_*.inc) *
! *                                                                             *
! *   T.L.Underwood (C) 2015-2016                                               *
! *   t.l.Underwood[@]bath.ac.uk                                                *
! *   - Lattice/Phase-Switch MC methodology                                     *
! *   - algorithm optimizations (gcmc_*_module.90, MC steps, random generator)  *
! *                                                                             *
! *   R.J.Grant (C) 2016                                                        *
! *   r.j.grant[@]bath.ac.uk                                                    *
! *   - extra potential forms (VDW, vdw_direct_module.f90)                      *
! *                                                                             *
! *******************************************************************************

module vdw_module

    use kinds_f90

    implicit none

    !> the number of parameters for vdw
    integer, parameter :: mxpvdw = 7

    !> number of vdw potentials
    integer, save :: ntpvdw

    !> flag for the implementation branch: 'direct'/'tables' (1/0)
    logical, save :: is_direct = .true.
    logical, save :: is_tables = .false.
    logical, save :: is_lrcorr = .false.
    logical, save :: is_shift  = .false.

    !AB: HC_VDW is the core diameter to avoid a VdW energy overflow beyond the range of Real(kind=wp)
    !AB: due to steep repulsive sholder (otherwise inf or spooky negative energy may occur)
    !AB: used only in VdW calculations and resulting in definite rejection of an MC move
    !AB: should be replaced by a VdW term dependendent definition in vdw_read(..)

    real(kind = wp), save ::  HC_MIN    = 0.001_wp     ! Angstrom
    real(kind = wp), save ::  HC_MIN2   = 0.000001_wp  ! Angstrom^2
    real(kind = wp), save ::  HC_VDW    = 0.5_wp       ! Angstrom
    real(kind = wp), save ::  HC_VDW2   = 0.25_wp      ! Angstrom^2
    real(kind = wp), save ::  HS_ENERGY = 1.e7_wp      ! Internal units
    real(kind = wp), save ::  CUT_SHIFT = 0.e0_wp      ! Internal units

    !> look up table for vdw + types of vdw potential
    integer, allocatable, save :: lstvdw(:,:)
    integer, allocatable, save :: ltpvdw(:)

    !> Matrix of coefficients for calculating long-range corrections between pairs of atomic species
    real(kind = wp), allocatable, save :: lrcvdw(:,:)

    !> vdw parameters
    real( Kind = wp ), allocatable, save :: prmvdw(:,:)

    !AB: what is the use of 'rvdw'??? - 'rcut' is used in routines as an external parameter
    real( Kind = wp ) :: rvdw 


contains


!> allocate vdw arrays and zero
subroutine allocate_vdw_arrays(nvdw)

    use kinds_f90
    use constants_module, only : uout
    use species_module, only : number_of_elements

    implicit none

    integer, intent(in) :: nvdw

    integer, dimension(4) :: fail
    integer :: maxint, ntpatm

    fail = 0

    ntpatm = number_of_elements
    maxint = (ntpatm * (ntpatm + 1)) / 2

    allocate (lstvdw(ntpatm, ntpatm),    stat = fail(1))
    allocate (ltpvdw(nvdw),              stat = fail(2))
    allocate (prmvdw(-1:mxpvdw, maxint), stat = fail(3))

    allocate( lrcvdw(ntpatm,ntpatm),  stat = fail(4))

    if (any(fail > 0)) then

        call error(122)

    endif

    ntpvdw = nvdw

    lstvdw = 0
    ltpvdw = 0

    lrcvdw = 0.0_wp

    prmvdw = 0.0_wp

end subroutine allocate_vdw_arrays


!> read in vdw potentials and convert to internal units, also generate the look-up tables
!> and the factors for the long-range corrections
!AB: reading VDW section from FIELD file - unified routine for 'direct' and 'tabulated' versions

include "vdw_read_field.f90" ! - include the entire procedure (new)


subroutine fh_correct_vdw(temperature, rvdw)

    use kinds_f90

    implicit none

    real (kind = wp), intent(in) :: temperature, rvdw

    call error(176)

end subroutine


!> convert potentials to look up tables
!> also factors in coulomb sum parts
subroutine vdw_generate(rvdw)

    use kinds_f90
    use constants_module
    use species_module

    implicit None

    real(kind = wp), intent(in) :: rvdw

    call cry(uout,'',"ERROR: Trying to generate interpolation tables "//&
                     &"on the 'direct' VdW branch (switch to the 'tables' branch)!!!",999)

end subroutine


subroutine vdw_energy(ai, aj, rrr, rsq, rcut, eunit, evdw, volinv, rvec, spini, spinj)

    use kinds_f90
    use constants_module, only : uout, PI
    use gb_potential_module
    use lambda_module, only : lambda_vdw

    implicit None

    integer, intent(in) :: ai, aj
    real(kind = wp), intent(in) :: rrr, rsq, rcut, eunit,volinv
    real(kind = wp), intent(out) :: evdw

    !TU: Additional parameters required for anisotropic VdW interactions
    
        !> Separation vector between the particles (after accounting for PBCs)
    real(kind = wp), intent(in) :: rvec(3)

       !> Orientation/spin of atom 1
    real(kind = wp), intent(in) :: spini(3)

       !> Orientation/spin of atom 2
    real(kind = wp), intent(in) :: spinj(3)
    
    integer         :: ivdw, keyvdw
    real(kind = wp) :: gk1, gk2, vk1, vk2
    real(kind = wp) :: dhc, dsh, dsh2, dsh3, dsh4, dsh5, dsh6, dsh12

    !AB: << potential form definitions

    include "potential_forms.inc"

    !AB: >> potential form definitions

    evdw = 0.0_wp
    if (rrr > rcut) return 
    !AB: above is NOT a duplicate check! 
    !AB: the check outside (in field.f90) is for global cutoff, and here we check for VDW cutoff

    !get potential function number
    ivdw = lstvdw(ai,aj)
    if (ivdw == 0) return

    keyvdw = ltpvdw(ivdw)

    !AB: the check for a too small separation (RCORE) is done externally! (see field.f90)
    !AB: as a matter of fact, it must be redundant due to the check for dhc = HC_VDW below
    !AB: this is where one can improve on the efficiency!

    !if (k > 0 .and. rrr > RCORE .and. rrr <= rcut) then

    !AB: the hard core (overlap) distance to avoid an overflow of VdW energy beyond the range of Real(kind=wp)
    !AB: due to steep repulsive shoulder, otherwise inf or spooky negative energy may occur
    !dhc = HC_VDW

    dhc = prmvdw(0,ivdw)
    
    select case (keyvdw)

        case (-1)

            !TU: Code for hard-sphere potential ('hs')
           
            !TU: Note that, unlike the above potentials, the energy is set very high if r
            !TU: is less than the overlap separation; 'dhc' is never used
            !if( rrr < prmvdw(1,ivdw) ) then
            if( rrr < dhc ) then

                evdw = HS_ENERGY

                return

            else if ( rrr < prmvdw(2,ivdw) ) then

                evdw = prmvdw(3,ivdw)

                return

            end if

        !AB: NOTE FOR THE REST
        !AB: enabling potentials starting at the effective particle surfaces
        !AB: rather than from the particle centers (this is a new feature!)
        !AB: although the surface distance definition is a little complicated...

        !AB: dsh = prmvdw(max+1,ivdw) ! where max is the number of compulsory parameters
        !AB: dhc = dhc+dsh            ! this is effective hard-core / overlap distance

        case (1) ! [<-2] Lennard-Jones (LJ) potential

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            !AB: check for overlapping cores due to steep short-range repulsion
            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the LJ core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Lennard-Jones potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)

                dsh6 = b/dsh
                dsh6 = dsh6*dsh6*dsh6
                !dsh6 = dsh6*dsh6

                !TU: The below line was for AB's implementation of long-range VdW corrections,
                !TU: which is no longer in use. Note that prmvdw(-1,idvw) is the shift to
                !TU: apply for shifted potentials
                !evdw = vlj( dsh6*dsh6, a ) + prmvdw(-1,ivdw)*volinv  ! vlj(sri6,a)
                evdw = vlj( dsh6*dsh6, a ) + prmvdw(-1,ivdw)

                !parpot(-1) = -vlj( ( parpot(2) / (vdw_rcut-parpot(3)) )**6 , parpot(1) )  ! vlj(sri6,a)

                !evdw = 4.0_wp * a *(b / dsh)**6 * ((b / dsh)**6 - 1.0_wp)

                !JG: could the calculation be optimised with
                ! tmpvar = (b / r)**6
                ! vk1 = 4.0_wp * a (tmpvar * (tmpvar - 1.0_wp))
                ! vk1 = 4.0_wp * a *(b / r)**6 * ((b / r)**6 - 1.0_wp)
            
                !JG: vk2 is a constant and could be calculated at initialisation
                ! vk2 = 4.0_wp * a *(b / rcut)**6 * ((b / rcut)**6 - 1.0_wp)
                ! evdw = vk1 - vk2

                return

            end if

        case (2) ! [<-1] 12-6 potential

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            !AB: check for overlapping cores due to steep short-range repulsion
            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the 12-6 core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! 12-6 potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)

                dsh6 = dsh*dsh*dsh
                dsh6 = dsh6*dsh6
                !dsh6 = 1.0_wp/dsh6

                !TU: The below line was for AB's implementation of long-range VdW corrections,
                !TU: which is no longer in use. Note that prmvdw(-1,idvw) is the shift to
                !TU: apply for shifted potentials
                !evdw = v126( 1.0_wp/dsh6, a , b ) + prmvdw(-1,ivdw)*volinv ! v126(ri6,a,b)
                evdw = v126( 1.0_wp/dsh6, a , b ) + prmvdw(-1,ivdw)

                !evdw = (a / dsh**6 - b) / dsh**6

                return

            end if

        case (3) ! [<-12] Simple Powers potential (9-3) potential (JG/AB)

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = dsh*dsh*dsh

                evdw = v93( 1.0_wp/c, a, b ) != (a*ri3*ri3-b)*ri3

                !evdw = vpnm( a*d, b*c ) != aric - brid
                !evdw = a/dsh**c - b/dsh**d

                return

            end if

        case (4) ! [<-13] Simple Powers potential (10-4) potential (JG/AB)

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)

                dsh = 1.0_wp/dsh*dsh
                c = dsh*dsh
                d = dsh*c

                evdw = v104( d, c, a, b ) != (a*ri6-b)*ri4

                !evdw = vpnm( a*d, b*c ) != aric - brid
                !evdw = a/dsh**c - b/dsh**d

                return

            end if

        case (5) ! [<-9] Weeks-chandler-Andersen (WCA), based around "shifted & truncated" LJ, potential by I.T.Todorov

            !AB: the initial definition included shifting to the effective particle surfaces via 'c'
            !AB: now most of the potentials have similar generalised definitions

            !AB: NOTE: we have a *custom* definition of WCA based on *full* LJ (prone to confusion)
            !AB: whereas the standard WCA definiton is includes LJ-repulsion only!
            !AB: keeping it for legacy considerations - everybody seems happy with this in DL_POLY-4 anyway

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the WCA (LJ) core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Weeks-chandler-Anderson (shifted & truncated Lenard-Jones) (i.t.todorov)
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                !c = prmvdw(3,ivdw)
                d = prmvdw(4,ivdw)

                dsh6 = b/dsh
                dsh6 = dsh*dsh*dsh
                dsh6 = dsh6*dsh6

                if( dsh < d ) evdw = vwca( a, dsh6 ) !=  4.0_wp*a* brc6 * (brc6 - 1.0_wp) + a

                !if( dsh < d ) evdw = 4.0_wp * a * (b / dsh)**6 * ((b / dsh)**6 - 1.0_wp) + a
                !evdw = 4.0_wp * a * (b / (r - c))**6 * ((b / (r - c))**6 - 1.0_wp) + a

                return

            end if

        case (6) ! Hydrogen-bond 12-10 potential

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Hydrogen-bonding core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Hydrogen-bond 12-10 potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)

                dsh2 = dsh*dsh
                dsh5 = dsh2*dsh2*dsh
                dsh6 = dsh2*dsh2*dsh2

                evdw = vhbd( a/(dsh6*dsh6), b/(dsh5*dsh5) ) != ri12a - ri10b

                !evdw = a / dsh**12 - b / dsh**10

                return

            end if

        case (7) ! [<-10] Espanol-Warren (EW) potential

            dsh = prmvdw(3,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Espanol-Warren core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! espanol - warren
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)

                if( dsh < b ) evdw = vew( a, b-dsh ) != 0.5_wp*a*br*br

                !if( dsh < b ) evdw = 0.5_wp * a * (b-dsh)**2

                return

            end if

        case (8) ! [<-4] Buckingham exp-6  potential

            dsh = prmvdw(4,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Buckingham core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Buckingham exp-6 potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)

                dsh2 = dsh*dsh

                evdw = vbuk( a*exp(-dsh/b), c/(dsh2*dsh2*dsh2) ) != aexprb - sri6

                !vk1  = a * exp(-rrr / b)
                !vk2  = - c / (rsq * rsq * rsq)
                !evdw = vk1 + vk2

                return

            end if

        case (9) ! [<-8] Morse plus D*r^{-12} potential (JP/JG)

            dsh = prmvdw(5,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Morse core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Morse potential (general form)
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)
                d = prmvdw(4,ivdw)

                !evdw = a * ((1.0_wp - exp( -c * (dsh - b) ))**2 - 1.0_wp) ! + d/dsh**12

                evdw = vmrs( dsh, a, b, c ) != a*( (1.0_wp - exp(-c * (r - b)))**2 - 1.0_wp ) 

                if( d > 0.0_wp ) then

                    dsh6 = dsh*dsh*dsh
                    dsh6 = dsh6*dsh6

                    evdw = evdw + d/(dsh6*dsh6)

                end if

                return

            end if

        case (10) ! [<-11] Simple Powers potential (n > m) potential (JG/AB)

            dsh = prmvdw(5,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the power (LJ) n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! simple power (LJ) n-m potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)
                d = prmvdw(4,ivdw)

                evdw = vpnm( a/dsh**int(c), b/dsh**int(d) ) != aric - brid

                !evdw = a/dsh**c - b/dsh**d

                return

            end if

        case (11) ! [<-3] Two Powers (n > m) potential

            dsh = prmvdw(5,ivdw)
            dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the n-m core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! n-m potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)
                d = prmvdw(4,ivdw)

                evdw = vnm( (d/dsh)**int(b), (d/dsh)**int(c), a/(b-c), c, b )  ! vnm(drb,drc,abc,c,b)

                !abc  = a / (b - c)
                !dsh2 = (d / dsh)**int(b)
                !dsh3 = (d / dsh)**int(c)

                !evdw = vnm(drb,drc,abc,c,b)   ! vnm(drb,drc,abc,c,b) = abc*(c*drb-b*drc)

                !evdw = a / (b - c) * (c * (d / dsh)**b - b * (d / dsh)**c)

                return

            end if

        case (12) ! [<-7] Shifted & Force Corrected Two-Powers (n > m) potential by W.Smith

!AB: does not make sense to shift the origin to the surface (???)

            !dsh = prmvdw(5,ivdw)
            !dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                !dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the shifted n-m (W.Smith) core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! shifted and force corrected n-m potential (w.smith)

                eps = prmvdw(1,ivdw)
                an  = prmvdw(2,ivdw)
                am  = prmvdw(3,ivdw)
                r0  = prmvdw(4,ivdw)

!                if (am > an) call error(118) !AB: now checked while reading FIELD

                gama = rcut/r0
!                if (gama < 1.0_wp) call error(119) !AB: now checked while reading FIELD

                gamai = r0/rcut

                beta = gama*((gama**(am+1.0_wp)-1.0_wp) /                       &
                          (gama**(an+1.0_wp)-1.0_wp))**(1.0_wp/(an-am))
                alpha= -(an-am) /                                                 &
                           ( am*(beta**an)*(1.0_wp+(an*gamai-an-1.0_wp)*gamai**an)   &
                           - an*(beta**am)*(1.0_wp+(am*gamai-am-1.0_wp)*gamai**am) )
                eps  = eps*alpha

                evdw = vsnm( eps/(an-am), an, am, beta, gamai, r0/rrr ) ! vsnm(expanam,an,am,beta,gamai,sri)
                !evdw = vsnm( eps/(an-am), an, am, beta, gamai, r0/dsh ) 

                !evdw = eps/(an-am)*( am*(beta**an)*((r0/rrr)**an-(1.0_wp/gama)**an)    &
                !                -an*(beta**am)*((r0/rrr)**am-(1.0_wp/gama)**am)    &
                !      + an*am *((rrr/(gama*r0)-1.0_wp)*((beta/gama)**an-(beta/gama)**am)) )

                return

            end if

        case (13) ! [<-5] Born-Huggins-Meyer exp-6-8  potential

!AB: does not make sense to shift the origin to the surface (???)

            !AB: currently no room for the optional surface shifting parameter (maybe no need either)

            !dsh = prmvdw(6,ivdw) !AB: non-existent!!!
            !dhc = dhc+dsh

            if( rrr < dhc ) then

                !AB: set the energy very high but below the upper bound of Real(kind=wp)
                !AB: so that an MC move would be definitely rejected

                evdw = HS_ENERGY

                return

            else

                !dsh = rrr-dsh
                !if( dsh < 0.0_wp ) then
                !    write(uout,*)'SOS: negative distance to the Born-Huggins-Meyer core surface!!!',r,dsh
                !    call error(999)
                !end if

                ! Born-Huggins-Meyer exp-6-8 potential
                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)
                d = prmvdw(4,ivdw)
                e = prmvdw(5,ivdw)

                evdw = vbhm( a*exp(b*(c-rrr)), rsq, d, e ) != aexpbcr - ri2*ri2*ri2*d - ri2*ri2*ri2*ri2*e

                !evdw = a * exp(b * (c - rrr)) - d / rrr**6 - e / rrr**8

                return

            end if

        case (14) ! A-O potential beyond hard-sphere core (JG/AB)

!AB: does not make sense to shift the origin to the surface (???)

            !dsh = prmvdw(4,ivdw)
            !dhc = dhc+dsh

            !JG: A-O potential derived from !TU: Code for hard-sphere potential ('hs')

            if( rrr < dhc ) then

                evdw = HS_ENERGY

                return

            else if( rrr < prmvdw(2,ivdw) ) then

                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)

                evdw = vao( a, b, c, rrr ) != a*( 1.0_wp + b*r*(1.0_wp + c*r*r) )

                !JG: A-O potential params initialised to allow optimisation
                !evdw = prmvdw(3,ivdw) * ( 1.0_wp + prmvdw(4,ivdw) * rrr + prmvdw(5,ivdw) * rrr**3 )

                return

            end if

        case (15) ! Yukawa potential beyond hard-sphere core (JG/AB)

!AB: does not make sense to shift the origin to the surface (???)

            !dsh = prmvdw(4,ivdw)
            !dhc = dhc+dsh

            !JG: Yukawa potential derived from !TU: Code for hard-sphere potential ('hs')

            if( rrr < dhc ) then

                evdw = HS_ENERGY

                return

            else 

                a = prmvdw(1,ivdw)
                b = prmvdw(2,ivdw)
                c = prmvdw(3,ivdw)

                evdw = vyuk( a, b, c, rrr ) != a*c*exp(b*(a-r))/r

                !JG: Yukawa potential
                !evdw = prmvdw(1,ivdw) * prmvdw(3,ivdw) * exp( -prmvdw(2,ivdw) * (r - prmvdw(1,ivdw)) ) / r 

                !evdw = c*a*exp( -b*(rrr-a) )/rrr

                return

            end if


        case(16)

            !TU: LAMBDA-DEPENDENT hard-sphere potential

            !TU: Form: -epsilon_0*ln(1-lambda) if r<sigma; 0 if r>sigma
            !
            !    Meaning of 'prmvdw' variables for this potential:
            !    prmvdw(1,ivdw) = epsilon_0
            !    prmvdw(2,ivdw) = sigma
            !
            !    Note that there is no hard-core repulsion for very small r for
            !    lambda-dependent potentials, hence the lack of check for rrr < dhc

            if(rrr < prmvdw(2,ivdw)) then

                !TU**: Return the hard-sphere energy if we are essentially at lambda=1. This is necessary
                !TU**: due to floating point precision: lambda=0.99999999999999989 does not yield a high
                !TU**: enough energy which is guaranteed to be rejected!
                if(lambda_vdw > 0.999999999_wp )then

                    evdw = HS_ENERGY

                else

                    evdw = -log(1.0_wp-lambda_vdw)*prmvdw(1,ivdw)
    
                end if

                return

             end if
                
        case(17)

            !TU: LAMBDA-DEPENDENT Lennard-Jones potential

            !TU: Form: lambda^a*4*epsilon*( (alpha*(1-lambda)^b+(r/sigma)^c)^(-12/c) - (alpha*(1-lambda)^b+(r/sigma)^c)^(-6/c) )
            !   
            !    Meaning of 'prmvdw' variables for this potential:
            !    prmvdw(1,ivdw) = epsilon
            !    prmvdw(2,ivdw) = sigma
            !    prmvdw(3,ivdw) = alpha
            !    prmvdw(4,ivdw) = a
            !    prmvdw(5,ivdw) = b
            !    prmvdw(6,ivdw) = c
            !
            !    Note that there is no hard-core repulsion for very small r for
            !    lambda-dependent potentials, hence the lack of check for rrr < dhc

            evdw = lambdalj(rrr, &
                            prmvdw(1,ivdw), &
                            prmvdw(2,ivdw), &
                            prmvdw(3,ivdw), &
                            prmvdw(4,ivdw), &
                            prmvdw(5,ivdw), &
                            prmvdw(6,ivdw), &
                            lambda_vdw)

            return

        case(18)

            !TU: Gaussian potential

            !TU: Form: epsilon*exp(-0.5*(r/sigma)^2)
            !
            !    Meaning of 'prmvdw' variables for this potential:
            !    prmvdw(1,ivdw) = epsilon
            !    prmvdw(2,ivdw) = sigma

            evdw = prmvdw(1,ivdw) * exp( -0.5_wp * (rrr/prmvdw(2,ivdw))**2 )

            return

        case (19)

            !TU: Pair potential component of Stillinger-Weber potential (TU)

            !TU: Meaning of 'prmvdw' variables for the pair Stillinger-Weber potential (same as LAMMPS form):
            ! prmvdw(1,ivdw) = epsilon
            ! prmvdw(2,ivdw) = sigma
            ! prmvdw(3,ivdw) = a
            ! prmvdw(4,ivdw) = A
            ! prmvdw(5,ivdw) = B
            ! prmvdw(6,ivdw) = p
            ! prmvdw(7,ivdw) = q

            if( rrr < dhc ) then

                evdw = HS_ENERGY

                return


            else 

                evdw = vsw2( prmvdw(1,ivdw), prmvdw(2,ivdw), prmvdw(3,ivdw), prmvdw(4,ivdw), &
                             prmvdw(5,ivdw), prmvdw(6,ivdw), prmvdw(7,ivdw), rrr )

                return

            end if

            
        case(50)
            
            !TU: Gay-Berne potential - depends on pair separation AND atoms' spins

            !TU: Meaning of 'prmvdw' variables for the Gay-Berne potential
            ! prmvdw(1,ivdw) = epsilon_0
            ! prmvdw(2,ivdw) = chi_prime
            ! prmvdw(3,ivdw) = sigma_s
            ! prmvdw(4,ivdw) = chi
            ! prmvdw(5,ivdw) = mu
            ! prmvdw(6,ivdw) = nu
            
            !TU: Note that there is no hard-core repulsion for very small r here because
            !TU: I haven't worked out a good hard-core radius for this yet!

            evdw = gb_potential( spini, spinj, rvec, &
                                 prmvdw(1,ivdw), prmvdw(2,ivdw), prmvdw(3,ivdw), &
                                 prmvdw(4,ivdw), prmvdw(5,ivdw), prmvdw(6,ivdw) )

            return


        !>>>>>>>>>>>>>> TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL >>>>>>>>>>>>>>

        ! Change the number '9999' here to the 'code' for your potential which you
        ! ultimately choose. Note that this code must not be the same as any code used
        ! above for an existing potential in DL_MONTE. Note that this code must match
        ! the code for the potential specified in 'vdw_read_field.f90' (which is '9999'
        ! for this example.
        case(9999)

            ! On the right-hand side of this equation is where you specify the
            ! function corresponding to your pair potential. Key quantities you
            ! may need:
            ! - 'rrr' is the separation between the two atoms
            ! - 'prmvdw(n,ivdw)' is the 'n'th parameter for the potential read in
            !   from the FIELD file. See the code in 'vdw_read_field.f90' for further
            !   details regarding the parameters for your potential, noting that
            !   there they are stored in the 'parpot(:)' array rather than
            !   'prmvdw(:,ivdw)'
            ! Below, as an example, the potential is a cubic function of the interatomic
            ! separation: A*r + B*r^2 + C*r^3, where A, B and C are the 1st, 2nd and 3rd
            ! parameters read from the 'VDW' block in the FIELD file (read into DL_MONTE
            ! in 'vdw_read_field.f90')
            evdw =  prmvdw(1,ivdw)*rrr + prmvdw(2,ivdw)*rrr**2 + prmvdw(3,ivdw)*rrr**3

            ! IMPORTANT: In Fortran double-precision must be specified explicitly for
            ! floating-point constants; they are assumed to be single-precision by
            ! default. In DL_MONTE's source code double-precision for a constant can 
            ! by adding a '_wp' to the constant. E.g. '0.1' is the floating point number
            ! 0.1 in single precision, while '0.1_wp' is the number in double-precision.
            ! If 'x' is a double-precision variable, then the statement 'x=0.1' will
            ! result in a casting of the 0.1 in single-precision to double precision,
            ! resulting in a loss of precision. However the statement 'x=0.1_wp' will
            ! not have this issue. Hence always use '_wp' with constants in DL_MONTE's
            ! source code to prevent loss of precision.
            
            ! ADVANCED: The form of the potential is not limited to what can fit on the
            ! right-hand side of a 'evdw = ' expression - see above. It may be better to
            ! spread the expression over multiple lines of code. All that matters is
            ! that the 'evdw' variable contains the energy for the given interatomic
            ! separation by the end of the code block in this 'case'. For example the
            ! above potential form could equivalently be implemented via:
            !
            ! evdw = prmvdw(1,ivdw)*rrr
            ! evdw = evdw + prmvdw(2,ivdw)*rrr**2
            ! evdw = evdw + prmvdw(3,ivdw)*rrr**3
            !
            ! The potential form may also include conditional statements. E.g. a
            ! potential which has the form above for separations less than 5, and 0
            ! for separations above 5, one possible implementation is a s follows:
            !
            ! if( rrr < 5 ) then
            !     evdw = prmvdw(1,ivdw)*rrr + prmvdw(2,ivdw)*rrr**2 + prmvdw(3,ivdw)*rrr**3
            ! else
            !     evdw = 0.0_wp
            ! end if

            
        !<<<<<<<<<<<<<< TEMPLATE CODE FOR ADDING A NEW PAIR POTENTIAL <<<<<<<<<<<<<<
            
            
        case default

            !call error(124)

            !write(uout,*) "*** got wrong potential ***"
            !stop

            if( keyvdw < 100 ) call error(124)
            if( keyvdw > 105 ) call error(124)

            if( rrr < dhc ) then

                evdw = HS_ENERGY

                return

            else 

                ru  = rrr        ! r   (user)
                rui = 1.0_wp/ru  ! 1/r (user)

                !AB: parameterization convention for USER potentials:

                a = prmvdw(1,ivdw) ! ref-distance (but users are free in their definitions!)
                b = prmvdw(2,ivdw) ! custom
                c = prmvdw(3,ivdw) ! custom
                d = prmvdw(4,ivdw) ! custom
                e = prmvdw(5,ivdw) ! custom

                include "user_vdw_insert.inc"

                return

            end if

    end select

end subroutine vdw_energy




!> Returns the long-range correction energy given a specified inverse volume and
!> array containing the number of atoms of each atomic species in the system
!> Returns 0 if long-range corrections are not in use or if VdW potentials are
!> not in use.
real(kind=wp) function vdw_lrc_energy(volinv, natomtypes)

    use kinds_f90
    use lambda_module, only : using_lambda_ff, lambda_vdw    
    use species_module, only : number_of_elements

    implicit none

        !> Inverse volume of the system
    real (kind = wp), intent(in) :: volinv

        !> Array containing the number of atoms of each species in the system
    integer, dimension(:), intent(in) :: natomtypes

        !> Matrix of long-range correction coefficients for pairs of atomic species adjusted
        !> to account for lambda-dependent VdW interactions (while lrcvdw pertains to lambda_vdw=1).
    real (kind = wp), dimension(number_of_elements,number_of_elements) :: lrcvdwlambda

    integer :: i, j, ivdw

    vdw_lrc_energy = 0.0_wp

    if( ntpvdw > 0 ) then

        if( using_lambda_ff ) then

            !TU: For lambda-dependent potentials the long-range correction is scaled by lambda_vdw.
            !TU: The lambda-dependent VdW potentials which can have long-range corrections have 
            !TU: ltpvdw(lstvdw(i,j)) for pairs of atomic species i & j equal to one of the following 
            !TU: possibilities:
            !TU:   17 - corresponds to the LAMBDA-DEPENDENT Lennard-Jones potential
            lrcvdwlambda = lrcvdw

            do i = 1, number_of_elements

                do j = 1, number_of_elements

                    ! ivdw is the VdW potential label associated with elements i and j, and
                    ! ltpvdw(ivdw) is the key associated with the potential which determines the functional
                    ! form of the interaction (e.g. Lennard-Jones)
                    ivdw = lstvdw(i,j)
                    if(ivdw /= 0) then

                        !TU**: Does this lead to lambda^2 dependence or linear dependence on long-range correction
                        !TU**: if lambda is in use?
                        if( ltpvdw(ivdw) == 17 ) lrcvdwlambda(i,j) = lrcvdw(i,j) * lambda_vdw

                    end if

                end do

            end do

            vdw_lrc_energy = volinv * dot_product( natomtypes, matmul( lrcvdwlambda, natomtypes) )

        else

            vdw_lrc_energy = volinv * dot_product( natomtypes, matmul( lrcvdw, natomtypes) )

        end if

    end if

end function vdw_lrc_energy



!> lambda-dependent Lennard-Jones potential - general form
real(kind=wp) function lambdalj(r, epsilon, sigma, alpha, a, b, c, lambda)

    real(kind=wp), intent(in) :: r

    real(kind=wp), intent(in) :: epsilon

    real(kind=wp), intent(in) :: sigma

    real(kind=wp), intent(in) :: alpha

    real(kind=wp), intent(in) :: a

    real(kind=wp), intent(in) :: b

    real(kind=wp), intent(in) :: c

    real(kind=wp), intent(in) :: lambda

    lambdalj = ( alpha*(1.0_wp-lambda)**b+(r/sigma)**c )**(-6.0_wp/c)

    lambdalj = lambda**a * 4.0_wp*epsilon * lambdalj * ( lambdalj - 1.0_wp)

end function lambdalj



end module vdw_module
