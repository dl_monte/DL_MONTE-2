! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

module coul_module

    use kinds_f90
    use constants_module

    implicit none

    real(kind=wp), allocatable, dimension(:) :: erfc_e, erfc_g
    real(kind=wp), save :: aa, bb, rfld0, rfld1, b0, cdlrpot, crdr
    real(kind=wp), save :: rr3, r10, r42, r216, a1, a2, a3, a4, a5, pp
    real(kind=wp), save :: rcut, rct2, sc_rcut, se_rcut, sg_rcut, alpha0


contains


!> explicit calculation of bare Coulomb term: ~ `q1*q2/r`
subroutine set_coul_shift(rc)

    use kinds_f90

    implicit none

    real(kind=wp), intent(in) :: rc

    !AB: bare Coulomb shift at cutoff
    !AB: corresponds to force-shifted Coulomb: ~ q1*q2*[(1/r + r/Rc) - (1/Rc + Rc/Rc^2) ]

    rcut = rc
    rct2 = rc*rc

    sc_rcut = 2.0_wp/rcut

end subroutine set_coul_shift


!> explicit calculation of bare Coulomb term: ~ `q1*q2/r`
subroutine explicitCoulomb(chgi, chgj, r , dielec, sigmac, gammac)

    use kinds_f90
    use constants_module, only : CTOINTERNAL

    implicit none

    real(kind = wp), intent(in) :: chgi, chgj, r, dielec
    real(kind = wp), intent(out) :: sigmac, gammac

    real(kind = wp) :: chgprd

    sigmac = 0.0_wp
    gammac = 0.0_wp

    chgprd = chgi * chgj

    if( chgprd == 0.0_wp ) return

    chgprd = chgprd * CTOINTERNAL / dielec

    sigmac = chgprd / r

    return

    !AB: virial(r) = force(r)*r = -(dU(r)/dr)*r
    !AB: but in DL_POLY/DL_MONTE "scalar force" implies multiplying by \vec{r}  (outside)
    !AB: so the summation for virial uses: virc = virc + gammac*r^2 (everywhere in field)
    !AB: i.e. gammac = force/r = -(dU(r)/dr)/r (whence "scalar force" ~ 1/r^3)

    gammac =-sigmac / (r*r)

end subroutine explicitCoulomb


!> create the error function lookup table: ~ `erfc(eta*r)/r` \n
!> by numerical expansion approximation
subroutine erfcgen(cl,job)

    use kinds_f90
    use constants_module
    use control_type
    use coul_type

    type(control), intent(in) :: job
    type(coul), intent(in) :: cl

    integer :: i, ncut

    real(kind = wp) :: tt, exp1, rrr, rsq, delmesh
    real(kind = wp) :: epsq, alph, ecut, gcut

    integer :: fail(2)

    epsq = job%dielec 

    !AB: coultype = 1 <= "ewald" - normal Ewald summation using erfc AND including reciprocal space terms (XYZ-PBC with cutoff)
    !AB: coultype = 2 <= "ewald real" - real part same as in Ewald, screened by *erfc/r, but skipping reciprocal terms (XYZ-PBC with cutoff)
    !AB: coultype = 3 <= "shift[ed]"  - shifted-force bare Coulomb, no damping by erfc/r (XYZ-PBC with cutoff)
    !AB: coultype = 4 <= "shift[ed] damp[ed]" - damped by *erfc/r plus shifted-force Coulomb (XYZ-PBC with cutoff)
    !AB: coultype =-2 <= "explicit" or "direct" in slit or bulk

    !AB: assuming job%coultype > 0 here

    if( any(abs(job%coultype) < 3) ) then
    !AB: Ewald real space Coulomb term

        alph = cl%eta

    else if( any(abs(job%coultype) == 3) ) then
    !AB: "shifted-force" bare Coulomb

        alpha0 = job%ewald_alpha 
        return

    else if( any(abs(job%coultype) == 4) ) then
    !AB: "shifted damped" Coulomb using *erfc/r BUT NO reciprocal space term

        alph = job%ewald_alpha 

    endif
    
    alpha0 = alph

    a1 =  0.254829592_wp
    a2 = -0.284496736_wp
    a3 =  1.421413741_wp
    a4 = -1.453152027_wp
    a5 =  1.061405429_wp
    pp =  0.3275911_wp

    rr3 = 0.333333333333e0_wp
    r10 = 0.1e0_wp
    r42 = 0.02380952381e0_wp
    r216 = 4.62962962963e-3_wp

    rcut = job%shortrangecut
    rct2 = rcut*rcut

    ncut = MAXMESH-4

    cdlrpot = rcut/real(ncut,kind=wp)
    crdr    = 1.0_wp/cdlrpot

!   if(job % ewald_alpha == 0.0) return

    fail = 0
    if(.not.allocated(erfc_e)) allocate(erfc_e(0:maxmesh), stat = fail(1))
    if(.not.allocated(erfc_g)) allocate(erfc_g(0:maxmesh), stat = fail(2))

    if (any(fail > 0)) then
        call error(310)
    endif

    delmesh = rcut/real(ncut,kind=wp)

    do i = 1, MAXMESH
        rrr = real(i) * delmesh
        rsq = rrr * rrr

        tt = 1.0_wp / (1.0_wp + pp * alph * rrr)
        exp1 = exp(-(alph * rrr)**2)

        !AB: numerical expansion approximation of erfc(eta*r)/r
        erfc_e(i) = tt * (a1 + tt * (a2 + tt *(a3 + tt * (a4 + tt * a5)))) * exp1 / rrr

        !AB: do not skip force here as it is needed for damped & shifted potential
        erfc_g(i) = ( erfc_e(i) + 2.0_wp* (alph / ROOTPI) * exp1 ) / rsq

    end do

    erfc_e(0) = erfc_e(1)
    erfc_g(0) = erfc_g(1)

    se_rcut = erfc_e(ncut)
    !      == erfc(alpha*rcut)/rcut

    sg_rcut = erfc_g(ncut)
    !      == ( erfc(alpha*rcut)/rcut + 2.0_wp*(alpha/ROOTPI)*exp(-(alpha*rcut)**2) ) / rcut**2

    !AB: the rest is deprecated!

    !aa = erfc_g(ncut) * rcut
    !bb = -(erfc_e(ncut) + aa * rcut)

    !AB: the cutoff values for erfc(eta*r)/r
    !AB: used below in `shiftrealspaceenergy` & `shiftdamprealspaceenergy`

    !rfld0 = erfc_e(ncut)
    !rfld1 = rcut * erfc_g(ncut)
 
    !AB: `aa` and `bb` above in terms of `rfld0` and `rfld1`:
    !AB: aa === rfld1
    !AB: bb === -(rfld0 + rfld1 * rcut)

    !AB: reaction field parameters at cutoff - ???
    !b0 = 2.0_wp* (epsq - 1.0_wp) / (2.0_wp* epsq + 1.0_wp)
    !rfld0 = b0 / rcut**3
    !rfld1 = (1.0_wp+ 0.5_wp * b0) / rcut

end subroutine erfcgen


!> calculates the real space Coulomb term times `erfc(eta*r)/r` (given by tabulated expansion)
!> no shift to zero at cutoff
subroutine realspaceenergy(chgi, chgj, r , dielec, sigmac, gammac)

!TU-: 'sigmac' is the energy, and 'gammac' is the force (virial). 

    use kinds_f90
    use constants_module, only : CTOINTERNAL
    implicit none

    real(kind = wp), intent(in) :: chgi, chgj, r, dielec
    real(kind = wp), intent(out) :: sigmac, gammac

    integer :: l1, l2, ll
    real(kind = wp) :: chgprd, ppp, vk0, vk1, vk2, t1, t2, erfcr

    sigmac = 0.0_wp
    gammac = 0.0_wp

    chgprd = chgi * chgj

    if( chgprd == 0.0_wp ) return

    !sigmac = (chgprd * CTOINTERNAL / dielec ) * ERFC(alpha0*r)/r
    !return
    
    chgprd = chgprd * CTOINTERNAL / dielec

    !AB: using linear 3-point interpolation based on tabulated data for erfc(eta*r)/r (see erfcgen)

!AB: strange runtime error is captured in psmc_sync_mol-main test (other tests seem alright)
!AB: referrring to the above line, i.e. 'll' is wrong somehow (in only one test)
!--------------------------------------------------------------------
!At line 166 of file coul_module.f90
!Fortran runtime error: Index '0' of dimension 1 of array 'erfc_e' below lower bound of 1
!--------------------------------------------------------------------
!AB: looks like a numerical precision bug in taking integer value: 'll = int(r * crdr)'
!AB: but then similar bug is likely in vdw_tables_module.f90 when using tabules for VDW potentials
!AB: one solution would be to check for ll==0 and do something different, e.g. setting vk0=vk1,
!AB: but that would certainly add an unwanted overhead

!AB: I fixed this bug by providing the zero-th element while setting it up (see erfcgen):
!    erfc_e(0) = erfc_e(1)
!    erfc_g(0) = erfc_g(1)

    ll = int(r * crdr)
    l1 = ll + 1
    l2 = ll + 2
    ppp = r * crdr - dble(ll)

    vk0 = erfc_e(ll)

    vk1 = erfc_e(l1)
    vk2 = erfc_e(l2)

    t1 = vk0 + (vk1 - vk0) * ppp
    t2 = vk1 + (vk2 - vk1) * (ppp - 1.0d0)

    erfcr = (t1 + (t2 - t1) * ppp * 0.5d0) ! == erfc(alpha*r)/r

    !TU-: Presumably 'erfcr' is erfc(eta*r)/r? It is not clear. 
    !AB-: Yes, it is interpolated from numerical (expansion) approximation of erfc(eta*r)/r (see erfcgen)

    sigmac = chgprd * erfcr 

    return

    !TU-: It looks like this is the force calculation - see above.
    !AB-: Yes, it is, which should be done separately and not as often

    vk0 = erfc_g(ll)
    vk1 = erfc_g(l1)
    vk2 = erfc_g(l2)

    t1 = vk0 + (vk1 - vk0) * ppp
    t2 = vk1 + (vk2 - vk1) * (ppp - 1.0d0)

    erfcr = (t1 + (t2 - t1) * ppp * 0.5d0) ! == ( erfc(alpha*r)/r + 2*(alpha/ROOTPI)*exp(-(alpha*r)**2) )/r**2

    !AB: note that previously the sign was not correct
    gammac =-chgprd * erfcr

end subroutine realspaceenergy


subroutine shiftrealspaceenergy(chgi, chgj, r, rsq, dielec, sigmac, gammac)

    use kinds_f90
    use constants_module, only : CTOINTERNAL

    implicit none

    real(kind = wp), intent(in) :: chgi, chgj, r, rsq, dielec
    real(kind = wp), intent(out) :: sigmac, gammac

    real(kind = wp) :: chgprd

    sigmac = 0.0_wp
    gammac = 0.0_wp

    chgprd = chgi * chgj

    if( chgprd == 0.0_wp ) return

    chgprd = chgprd * CTOINTERNAL / dielec

    sigmac = chgprd * (1.0_wp/r + r/rct2 - 2.0_wp/rcut) !sc_rcut)

    return

    gammac =-chgprd * (1.0_wp/rsq - 1.0_wp/rct2) / r

    !AB: original code (below) implicitly relied on the limit: alpha -> 0 
    !AB: yet it does not seem to be correct

    !sigmac = chgprd * (1.0 / r + aa * r + bb)
    !gammac = rsq * chgprd * (1.0 / rsq - aa) / r

    !AB: for reference:
    !aa = erfc_g(ncut) * rcut
    !bb = -(erfc_e(ncut) + aa * rcut)

end subroutine shiftrealspaceenergy


subroutine shiftdamprealspaceenergy(chgi, chgj, r, rcut, dielec, sigmac, gammac)

    use kinds_f90
    use constants_module, only : CTOINTERNAL

    implicit none

    real(kind = wp), intent(in) :: chgi, chgj, r, rcut, dielec
    real(kind = wp), intent(out) :: sigmac, gammac

    integer :: ll
    real(kind = wp) :: chgprd, vk0, vk1, vk2, gk0, gk1, gk2, t1, t2, ppp, temp

    sigmac = 0.0_wp
    gammac = 0.0_wp

    chgprd = chgi * chgj

    if( chgprd == 0.0_wp ) return

    chgprd = chgprd * CTOINTERNAL / dielec

    ll   = int(r*crdr)
    ppp = r*crdr - real(ll,wp)

    vk0 = erfc_e(ll)
    vk1 = erfc_e(ll+1)
    vk2 = erfc_e(ll+2)

    t1 = vk0 + (vk1 - vk0)*ppp
    t2 = vk1 + (vk2 - vk1)*(ppp - 1.0_wp)

    !AB: this is erfc(alpha*r)/r
    temp = t1 + (t2-t1) * ppp * 0.5_wp 

    !AB: shifted-force erfc-screened (damped) Coulomb:
    sigmac = chgprd * (temp - se_rcut + sg_rcut*rcut * (r - rcut))

    !sigmac = chgprd * (temp - rfld0 + rfld1 * (r - rcut))

    return

    gk0 = erfc_g(ll)
    gk1 = erfc_g(ll+1)
    gk2 = erfc_g(ll+2)

    t1 = gk0 + (gk1 - gk0)*ppp
    t2 = gk1 + (gk2 - gk1)*(ppp - 1.0_wp)

    !AB: ( erfc(alpha*r)/r + 2*(alpha/ROOTPI)*exp(-(alpha*r)**2) )/r**2
    temp = t1 + (t2-t1) * ppp * 0.5_wp 

    !AB: virial
    gammac =-chgprd * (temp - sg_rcut)

    !AB: it does not seem to be correct below

    !gammac = ((t1 + (t2 - t1) * ppp * 0.5_wp) - aa / r - rfld0) * chgprd

    !aa = erfc_g(ncut) * rcut
    !bb = -(erfc_e(ncut) + aa * rcut)

    !rfld0 = erfc_e(ncut)
    !rfld1 = rcut * erfc_g(ncut)

end subroutine shiftdamprealspaceenergy


subroutine ewald_correct(chgi, chgj, r , rsq, alpha, dielec, sigmac, gammac)

    use kinds_f90
    use constants_module, only : CTOINTERNAL, ROOTPI

    implicit none

    real(kind = wp), intent(in) :: chgi, chgj, r, rsq, alpha, dielec
    real(kind = wp), intent(out) :: sigmac, gammac

    integer :: ll,l1,l2
    real(kind = wp) :: chgprd, ralpha, ralpha2, tt, exp1, erfcr, ppp, t1, t2, vk0, vk1, vk2, rrr

    sigmac = 0.0_wp
    gammac = 0.0_wp

    chgprd = chgi * chgj

    if( chgprd == 0.0_wp ) return

    chgprd = chgprd * CTOINTERNAL / dielec

    ralpha  = r * alpha
    ralpha2 = ralpha * ralpha

    ! calculate error function and derivative

    if(ralpha <  1.0e-2_wp) then

        sigmac = 2.0e0_wp* chgprd * (alpha / ROOTPI) * (1.0e0_wp+ ralpha2 * (-rr3 + ralpha2 * &
               (r10 + ralpha2 * (-r42 + ralpha2 * r216))))

        !AB: skipping force
        !gammac = -4.0e0_wp* chgprd * (alpha**3 / ROOTPI) * (rr3 + ralpha2 * &
        !        (-2.0e0_wp* r10 + ralpha2 * (3.0e0_wp* r42 - 4.0e0_wp* ralpha2 * r216)))

    else

        tt = 1.0e0_wp/ (1.0e0_wp + pp * ralpha)
        exp1 = exp(-ralpha2)
        !exp1 = exp(-(alpha * r)**2)

        sigmac = (1.0e0_wp- tt * (a1 + tt * (a2 + tt * (a3 + tt * (a4 + tt * a5)))) * exp1) * chgprd / r

        !AB: skipping force
        !gammac = - (sigmac*r - 2.0e0_wp* chgprd * (alpha / ROOTPI) * exp1) / rsq
        !gammac = - (sigmac - 2.0e0_wp* chgprd * (alpha / ROOTPI) * exp1) / rsq

    endif
    
    return

    sigmac = (chgprd * CTOINTERNAL / dielec ) * ERF(alpha0*r)/r
    !sigmac = (chgprd * CTOINTERNAL / dielec ) * (1.0_wp - ERFC(alpha0*r))/r
    
    return

    ll = int(r * crdr)
    l1 = ll + 1
    l2 = ll + 2
    ppp = r * crdr - dble(ll)

    vk0 = erfc_e(ll)

    vk1 = erfc_e(l1)
    vk2 = erfc_e(l2)

    t1 = vk0 + (vk1 - vk0) * ppp
    t2 = vk1 + (vk2 - vk1) * (ppp - 1.0d0)

    erfcr = (t1 + (t2 - t1) * ppp * 0.5d0) ! == erfc(alpha*r)/r
    
    !tt = 1.0_wp / (1.0_wp + pp * alpha * r)
    !exp1 = exp(-(alpha * r)**2)

    !TU-: Presumably 'erfcr' is erfc(eta*r)/r? It is not clear. 
    !AB-: Yes, it is interpolated from numerical (expansion) approximation of erfc(eta*r)/r (see erfcgen)

    sigmac = chgprd * (1.0_wp/r - erfcr)

end subroutine ewald_correct


!> calculates the self interaction term for Ewald sums
subroutine atomself_energy(chgi, eta, dielec, engself)

    use kinds_f90
    use constants_module, only : CTOINTERNAL, ROOTPI
    implicit none

    real(kind = wp), intent(in) :: chgi, eta, dielec
    real(kind = wp), intent(out) :: engself

!!$!TU**: DEBUGGING
!!$    engself = 0.0_wp
!!$return

    engself = chgi * chgi * eta 

    engself = -CTOINTERNAL * engself / (ROOTPI * dielec)

end subroutine atomself_energy


!> determine the maximum number of k vectors and reciprocal space cutoff in the Ewald summation
subroutine setewald(job, lv, cl)

    use coul_type
    use kinds_f90
    use latticevectors_module
    use latticevectors_type
    use control_type
    use constants_module, only : PI
    use comms_mpi_module, only : idnode, is_parallel
    use comms_omp_module, only : idthread, mxthread

    implicit none

    type(control), intent(in) :: job
    type(latticevectors), intent(in) :: lv
    type(coul), intent(inout) :: cl

    real(kind = wp) :: cutoff, delta, eps, tol, celprp(10), tol1

    cutoff = job%shortrangecut
    delta  = job%verletshell

    eps = min(abs(job%madelungacc), 0.5_wp)
    tol = sqrt(abs(log(eps*cutoff)))

    cl%realspacecut = cutoff
    call set_coul_shift(cutoff)

    if(job%ewald_alpha_given) then

        cl%eta = job%ewald_alpha

    else

        cl%eta = sqrt(abs(log(eps*cutoff*tol)))/cutoff

    endif

    if(job%ewald_alpha_given) then

        cl%gcellx = job%kmax1
        cl%gcelly = job%kmax2
        cl%gcellz = job%kmax3

        !JG: dcell returns perpendicular vector lengths ordered in greatest x, greatest y, greatest z components.

        call dcell(lv%rcpvector, celprp)

        cl%rcpspacecut = min(dble(cl%gcellx) * celprp(7), dble(cl%gcelly) * celprp(8), dble(cl%gcellz) * celprp(9))

    else

        call dcell (lv%latvector, celprp)
        tol1=sqrt(-log(eps* cutoff *(2.d0*tol*cl%eta)**2))

        cl%gcellx = nint(0.25 + celprp(1) * cl%eta * tol1 / PI)
        cl%gcelly = nint(0.25 + celprp(2) * cl%eta * tol1 / PI)
        cl%gcellz = nint(0.25 + celprp(3) * cl%eta * tol1 / PI)


        call dcell(lv%rcpvector, celprp)

        cl%rcpspacecut = min(dble(cl%gcellx) * celprp(7), dble(cl%gcelly) * celprp(8), dble(cl%gcellz) * celprp(9))

    endif

    cl%gsumrecip = is_parallel !.and. job%distribgvec

    !if( job%repexch .and. is_parallel ) then
    !    cl%gsumrecip = .true.
    !else
    !    cl%gsumrecip = .false.
    !    if( is_parallel ) cl%gsumrecip = .true.
    !endif

end subroutine setewald


!> prints ewald parameters that were set by `setewald`
subroutine printewald(ib, cl)

    use constants_module, only : uout
    use coul_type
    use comms_mpi_module, only : master

    implicit none

    integer, intent(in) :: ib
    type(coul), intent(in) :: cl

    if (master) then

        write(uout,"(/,/,1x,'cutoffs for lattice summation in box: ',i2)" ) ib
        write(uout,"(/,1x,'eta                                 : ',f10.5)" ) cl%eta
        write(uout,"(1x,'real space cutoff A                 : ',f10.5)" ) cl%realspacecut
        write(uout,"(1x,'reciprocal space cutoff 1/A         : ',f10.5)" ) cl%rcpspacecut
        write(uout,"(1x,'kmax1, kmax2, kmax3                 : ',3i3)") cl%gcellx, cl%gcelly, cl%gcellz

    endif

end subroutine printewald


!************************************************
!* code that sets up k-vector parameters 
!************************************************
!* calculates g-vectors within a given radius.
!* uses symmetry of crystal (as most other programs)
!* nx is from 0 to gcellx
!* ny is from 0 to gcelly if nx = 0 or -gcelly to gcelly if nx != 0
!* nz is from 1 to gcellz if nx=ny=0 or -gcellz to gcellz if nx=ny != 0
!* nb update every time volume called

!> setting up k-vectors etc
subroutine findgvector(lv, cl, dielec, flag, distribute)

    use latticevectors_type
    use constants_module, only : TWOPI, PI, CTOINTERNAL
    use parallel_loop_module, only : grp_rank, wkgrp_size
    use coul_type
    use kinds_f90

    implicit none

    type(latticevectors), intent(in) :: lv

    type(coul), intent(inout) :: cl

    real(kind = wp), intent(in) :: dielec

    !TU: 'flag' is apparently for calculating the stress; I don't know what that means though
    logical, intent(in) :: flag, distribute

    integer :: nx, ny, nz, i, j, k, nc

    real(kind = wp) :: gz, gw, etasq, gsqfct, gfct0, gfct1
    real(kind = wp) :: x, y, z, xx, yy, zz, gsq, rcpcutsq

    gz = 0.0
    gw = 0.0
    etasq = cl%eta * cl%eta
    gsqfct = 1.0 /(4.0 * etasq)
    gfct0 = CTOINTERNAL * PI / (etasq * lv%volume * dielec)
    gfct1 = gsqfct * gfct0

    gsq = 0.0 

    nc = 0

    rcpcutsq = cl%rcpspacecut * cl%rcpspacecut

    cl%numgvec = 0 ! zero number of vectors

    do nx = 0, cl%gcellx

        do ny = -cl%gcelly, cl%gcelly

            do nz = -cl%gcellz, cl%gcellz

                x = nx * lv%rcpvector(1,1) + ny * lv%rcpvector(2,1) + &
                    nz * lv%rcpvector(3,1)
                y = nx * lv%rcpvector(1,2) + ny * lv%rcpvector(2,2) + &
                    nz * lv%rcpvector(3,2)
                z = nx * lv%rcpvector(1,3) + ny * lv%rcpvector(2,3) + &
                    nz * lv%rcpvector(3,3)

                gsq = x * x + y * y + z * z

                if( gsq < rcpcutsq .and. gsq > 1.0e-8 ) then

                    if( distribute ) then

                        !add the new vector and increase counter
                        if( nc == grp_rank ) then

                            cl%numgvec = cl%numgvec + 1

                            if( cl%numgvec > cl%gvector_size ) then

                                call error(315)

                            endif

                            cl%gx(cl%numgvec) = x
                            cl%gy(cl%numgvec) = y
                            cl%gz(cl%numgvec) = z
                            cl%nxyz(1,cl%numgvec) = nx
                            cl%nxyz(2,cl%numgvec) = ny
                            cl%nxyz(3,cl%numgvec) = nz

                        endif

                        nc = nc + 1

                        if( nc >= wkgrp_size ) nc = nc - wkgrp_size

                    else

                        !add the new vector and increase counter
                        cl%numgvec = cl%numgvec + 1

                        if( cl%numgvec > cl%gvector_size ) then

                            call error(315)
                        
                        endif

                        cl%gx(cl%numgvec) = x
                        cl%gy(cl%numgvec) = y
                        cl%gz(cl%numgvec) = z

                        cl%nxyz(1,cl%numgvec) = nx
                        cl%nxyz(2,cl%numgvec) = ny
                        cl%nxyz(3,cl%numgvec) = nz

                    endif

                endif

            enddo

        enddo

    enddo


    do i = 1, cl%numgvec                  

        cl%gx(i) = cl%gx(i) * TWOPI
        cl%gy(i) = cl%gy(i) * TWOPI
        cl%gz(i) = cl%gz(i) * TWOPI

        if (flag) then

            cl%gxx(i) = cl%gx(i) * cl%gx(i)
            cl%gyy(i) = cl%gy(i) * cl%gy(i)
            cl%gzz(i) = cl%gz(i) * cl%gz(i)
                    
            cl%gxy(i) = cl%gx(i) * cl%gy(i)
            cl%gxz(i) = cl%gx(i) * cl%gz(i)
            cl%gyz(i) = cl%gy(i) * cl%gz(i)

         endif

         xx = cl%gx(i)
         yy = cl%gy(i)
         zz = cl%gz(i)
         gsq = xx * xx + yy * yy + zz * zz
         !TU: 'x' here is G^2/(4\eta^2), where G is the magnitude of the reciprocal lattice vector.
         x = gsq * gsqfct

         !TU: This is (2\eta^2) * \exp(-G^2/(4\eta^2)) / G^2
         gz = 0.5 * exp(-x) / x

         !TU: From above, gfct0 = CTOINTERNAL * PI / (etasq * lv%volume * dielec); hence 
         !TU: gz * gfct0 is k_e * [(2\pi) / V ] * \exp(-G^2/(4\eta^2)) / G^2, where k_e is the
         !TU: Coulomb constant 1/(4\pi\epsilon), and V is the volume.
         if(cl%nxyz(1,i) == 0) then 
             cl%g0(i) = gz * gfct0
         else
             cl%g0(i) = gz * gfct0 * 2.0
         endif

         if (flag) then
             gw = -2.0 * gz * (1.0 + 1.0 / x)
             cl%g1(i) = gw * gfct1
         endif

    end do

end subroutine findgvector


!> prints the number of k-vectors in Ewald summation (called after `findgvector`)
subroutine printnumgvec(cl, flag, distrib)

    use coul_type
    use constants_module, only : uout
    use comms_mpi_module, only : master, gsum
    use parallel_loop_module, only : grp_rank, wkgrp_size

    implicit none

    logical, intent(in) :: flag, distrib
    type(coul), intent(in) :: cl

    integer :: buf(wkgrp_size), buf1(wkgrp_size), id

    if (distrib) then

        buf  = 0
        buf1 = 0

        id = grp_rank + 1
        buf(id)  = cl%numgvec
        buf1(id) = cl%gvector_size

        call gsum(buf)
        call gsum(buf1)

        !if( flag .or. master ) then
        if( master ) then

            write(uout,"(/,/,1x,'total & max number of reciprocal lattice vectors: ',2i10,/,/)") &
                  sum(buf), sum(buf1)

            do id = 1, wkgrp_size

                write(uout,"(1x,'total & max number of reciprocal lattice vectors on node ',3i10)") &
                      id, buf(id), buf1(id)

            enddo

        endif

    !else if( flag .or. master ) then
    else if( master ) then
    
        write(uout,"(/,/,1x,'total & max number of reciprocal lattice vectors:',2i10)") &
              cl%numgvec, cl%gvector_size

    endif

end subroutine printnumgvec


subroutine estimate_gvector_size(lv, cl, dielec, flag, distribute)

    use latticevectors_type
    use constants_module, only : TWOPI, PI, CTOINTERNAL
    use parallel_loop_module, only : grp_rank, wkgrp_size
    use coul_type
    use kinds_f90

    implicit none

    type(latticevectors), intent(in) :: lv

    type(coul), intent(inout) :: cl

    real(kind = wp), intent(in) ::  dielec

    logical, intent(in) :: flag, distribute

    integer :: nx, ny, nz, i, nc

    real(kind = wp) :: gz, gw, etasq, gsqfct, gfct0, gfct1
    real(kind = wp) :: x, xx, y, yy, z, zz, gsq, rcpcutsq

    gz = 0.0
    gw = 0.0
    etasq = cl%eta * cl%eta
    gsqfct = 1.0 /(4.0 * etasq)
    gfct0 = CTOINTERNAL * PI / (etasq * lv%volume * dielec)
    gfct1 = gsqfct * gfct0

    gsq = 0.0 

    nc = 0

    rcpcutsq = cl%rcpspacecut * cl%rcpspacecut


    cl%gvector_size = 0 ! zero number of vectors

    do nx = 0, cl%gcellx

        do ny = -cl%gcelly, cl%gcelly

            do nz = -cl%gcellz, cl%gcellz

                x = nx * lv%rcpvector(1,1) + ny * lv%rcpvector(2,1) + &
                    nz * lv%rcpvector(3,1)
                y = nx * lv%rcpvector(1,2) + ny * lv%rcpvector(2,2) + &
                    nz * lv%rcpvector(3,2)
                z = nx * lv%rcpvector(1,3) + ny * lv%rcpvector(2,3) + &
                    nz * lv%rcpvector(3,3)

                gsq = x * x + y * y + z * z

                if (gsq < rcpcutsq.and.gsq > 1.0e-8) then

                    if (distribute) then

                        !add the new vector and increase counter
                        if (nc == grp_rank) then

                            cl%gvector_size = cl%gvector_size + 1

                        endif

                        nc = nc + 1

                        if (nc >= wkgrp_size) nc = nc - wkgrp_size

                    else

                        !add the new vector and increase counter
                        cl%gvector_size = cl%gvector_size + 1

                    endif

                endif

            enddo

        enddo

    enddo


    if( flag ) then

        !add a little padding - just incase the volume increases a little
        !these may need to be adjusted for large volume changes
        if( distribute ) then

            cl%gvector_size = cl%gvector_size + 1000/wkgrp_size

        else

            cl%gvector_size = cl%gvector_size + 1000

        endif

    endif

end subroutine estimate_gvector_size


subroutine allocate_ewald_arrays(flag, cl, mxatom)

    use coul_type

    implicit none

    integer, intent(in) :: mxatom
    logical, intent(in) :: flag

    type(coul), intent(inout) :: cl

    integer :: im,i

    allocate(cl%g0(cl%gvector_size))
    allocate(cl%gx(cl%gvector_size))
    allocate(cl%gy(cl%gvector_size))
    allocate(cl%gz(cl%gvector_size))

    allocate(cl%nxyz(3,cl%gvector_size))
!    allocate(cl%ny(cl%gvector_size))
!    allocate(cl%nz(cl%gvector_size))

    if (flag) then

        allocate(cl%gxx(cl%gvector_size))
        allocate(cl%gyy(cl%gvector_size))
        allocate(cl%gzz(cl%gvector_size))
        allocate(cl%gxy(cl%gvector_size))
        allocate(cl%gxz(cl%gvector_size))
        allocate(cl%gyz(cl%gvector_size))
        allocate(cl%g1(cl%gvector_size))

    endif
    
    allocate(cl%rcpsum(cl%gvector_size))

    cl%rcpsum = 0.0_wp + 0.0_wp * COMPLEXI

end subroutine allocate_ewald_arrays


end module

