! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Bond storage, manipulation and energy calculation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor bonds in a molecule

module bond_module

    use kinds_f90

    implicit none

        !> total (max) number of bonds allowed
    integer, save :: mxbond

        !> number of bond potential types
    integer, save :: ntpbond

        !> max number of parameters for bond
    integer, parameter :: mxpbond = 6

        !> look up table for bond + types of bond potential(s)
    integer, allocatable, save :: lstbond(:,:)

        !> @auxarray for potential types
    integer, allocatable, save :: ltpbond(:)

        !> array for bond potential parameters
    real( Kind = wp ), allocatable, save :: prmbond(:,:)

contains

!> @brief
!> - allocates all arrays needed for bond potentials and forces/virials
!> @using 
!> - `kinds_f90`
!> - `constants_module, only : maxmesh, uout`
!> - `species_module, only : number_of_elements`
subroutine allocate_bondpot_arrays(nbond)

    use kinds_f90
    use constants_module, only : maxmesh, uout
    use species_module, only : number_of_elements

    implicit none

        !> total (max) number of bonds
    integer, intent(in) :: nbond

    integer, dimension( 1:5 ) :: fail
    integer :: maxint, ntpatm

    fail = 0

    ntpatm = number_of_elements
    maxint = (ntpatm * (ntpatm + 1)) / 2

    allocate (lstbond(mxpbond, 2),       stat = fail(1))
    allocate (ltpbond(nbond),            stat = fail(2))
    allocate (prmbond(mxpbond, maxint),  stat = fail(3))

    if (any(fail > 0)) then

        call error(117)

    endif

    ntpbond = nbond

    lstbond = 0
    ltpbond = 0

    prmbond = 0.0_wp

end subroutine allocate_bondpot_arrays

!> @brief
!> - reads in, prints out, and converts bond potentials to internal units
!> - also generates the look-up tables (numerical representation on grid)
!> @using 
!> - `kinds_f90`
!> - `parse_module`
!> - `species_module`
!> - `constants_module`
!> - `comms_mpi_module, only : master`
subroutine read_bond(shortrangecut, energyunit)

    use kinds_f90
    use parse_module
    use species_module
    use constants_module
    use comms_mpi_module, only : master !, idnode

    implicit none

        !> cutoff distance (for bonded[?] and non-bonded[?] interactions)
    real(kind = wp), intent(in) :: shortrangecut

        !> energy unit conversion factor
    real(kind = wp), intent(in) :: energyunit

    real(kind = wp) :: parpot(mxpbond), engunit

    integer :: katom1, katom2, jtpatm, itpbond, itp, j, i, atm1typ, atm2typ
    integer :: keypot, nread, maxpar

    character*8, atom1, atom2
    character :: line*100, word*40, word1*40, keyword*8

    logical :: safe,is_float

    nread = ufld
    engunit = energyunit
    
    maxpar = 6

    !if (idnode == 0) write(uout,"(/,/,' bond potentials ',/)")

    if( master ) write(uout,"(/,/,20('-'),/,' bond potentials: ',/,20('-'))")

    do itpbond = 1, ntpbond

        parpot=0.0_wp

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        !AB: check if the atom pair is specified
        call get_word(line,word)
        call get_word(line,word1)

        is_float = .false.
        if( indexnum(word1,is_float) == 0 ) then 
        !AB: two atoms' specs are present
        !AB: the following does NOT ensure that the atom pair is correct!
        !AB: but it checks if the atoms are known (at least)

            atom1   = word
            atm1typ = get_species_type(word1)

            call get_word(line,word)
            atom2   = word

            call get_word(line,word)
            atm2typ = get_species_type(word)

            katom1=0
            katom2=0

            do jtpatm = 1, number_of_elements
               if (atom1 == element(jtpatm) .and. atm1typ == eletype(jtpatm)) katom1=jtpatm
               if (atom2 == element(jtpatm) .and. atm2typ == eletype(jtpatm)) katom2=jtpatm
            end do

            if (katom1 == 0 .or. katom2 == 0) call error(116)

            ! store the type of atom for each bond potential- use to add coulomb terms
            !AB: lstbond(itpbond, katom) is not used anywhere, so redundant...
            !AB: hence, it is unnecessary to read and check...
            !lstbond(itpbond, 1) = katom1
            !lstbond(itpbond, 2) = katom2

            call get_word(line,word)
            call get_word(line,word1)

        end if

        !call get_word(line,word)
        call lower_case(word)

        keyword=word
        if (keyword == '12-6' .or. keyword == '-12-6') then
            keypot=1
            maxpar=2
        else if (keyword == 'lj' .or. keyword == '-lj' ) then
            keypot=2
            maxpar=2
        else if (keyword == 'nm' .or. keyword == '-nm') then
            keypot=3
            maxpar=4
        else if (keyword == 'buck' .or. keyword == '-buck') then
            keypot=4
            maxpar=3
        else if (keyword == 'bhm' .or. keyword == '-bhm') then
            keypot=5
        else if (keyword == 'hbnd'.or. keyword == '-hbnd') then
            keypot=6
            maxpar=2
        else if (keyword == 'snm' .or. keyword == '-snm') then
            keypot=7
        else if (keyword == 'hcnm' .or. keyword == '-hcnm') then
            keypot=7
        else if (keyword == 'mors' .or. keyword == '-mors') then
            keypot=8
        else if (keyword == 'wca' .or. keyword == '-wca') then
            keypot=9
        else if (keyword == 'flj' .or. keyword == '-flj') then
            keypot=10
        else if (keyword == 'fwca' .or. keyword == '-fwca') then
            keypot=11
        else if (keyword == 'fene' .or. keyword == '-fene') then
            keypot=12
        else if (keyword == 'harm' .or. keyword == '-harm') then
            keypot = 13
        !else if (keyword == 'tab' ) then
        !    keypot=0
        else
            call error(115)
        endif

        !AB: the keyword and 1-st parameter have been read-in above
        !call get_word(line,word)
        parpot(1)=word_2_real(word1)

        !AB: read-in all the compulsory parameters
        do i = 2,maxpar

           call get_word(line,word)
           !if( word(1:1) == '#' ) exit
           
           parpot(i) = word_2_real(word)

        end do

        !call get_word(line,word)
        !parpot(2)=word_2_real(word)
        !call get_word(line,word)
        !parpot(3)=word_2_real(word)
        !call get_word(line,word)
        !parpot(4)=word_2_real(word)
        !call get_word(line,word)
        !parpot(5)=word_2_real(word)
        !call get_word(line,word)
        !parpot(6)=word_2_real(word)

        !if( master ) write(uout,"(1x,2a8,3x,a4,3x,10f15.6)") &
        !     atom1,atom2,keyword,(parpot(j),j=1,mxpbond)

        if( master ) write(uout,"(1x,i5,3x,a8,3x,10f15.6)") &
            itpbond,keyword,(parpot(j),j=1,mxpbond)

        ! convert energies to internal unit
        parpot(1) = parpot(1)*engunit

        if (keypot == 1) then
            parpot(2)=parpot(2)*engunit
        else if (keypot == 4) then
            parpot(3)=parpot(3)*engunit
        else if (keypot == 5) then
            parpot(4)=parpot(4)*engunit
            parpot(5)=parpot(5)*engunit
        else if (keypot == 6) then
            parpot(2)=parpot(2)*engunit
        else if (keypot == 9) then
            parpot(2)=Abs(parpot(2))
            if (parpot(3) > parpot(2)/2.0_wp) &
                 parpot(3)=Sign(1.0_wp,parpot(3))*parpot(2)/2.0_wp
            parpot(4)=2.0_wp**(1.0_wp/6.0_wp)*parpot(2)+parpot(3)
        else if (keypot == 10) then
            parpot(3) = parpot(3) * engunit
        else if (keypot == 11) then
            parpot(4) = parpot(4) * engunit
        end if

        if( keyword(1:1)=='-' ) keypot = -keypot

        ltpbond(itpbond) = keypot
        do i = 1, mxpbond
            prmbond(i,itpbond)=parpot(i)
        end do

    end do   ! end loop over potentials

    return

1000 call error(114)                    

end subroutine

!> @brief
!> - calculates bond energy and force terms using Verlet neighbour list
!> - originates from `dl_poly_3`
!> @using 
!> - `kinds_f90`
subroutine bond_energy (pot, r, rsq, engbond, forc)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for calculating bond energy and force terms using
! verlet neighbour list
!
! copyright - daresbury laboratory
! author    - w.smith august 1998
! amended   - i.t.todorov september 2004
! re-written for dl-monte2 John Purton 2016
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    use kinds_f90
    use constants_module, only : uout

    implicit None

      !> potential type identifier
    integer, intent(in)                                       :: pot

      !> bond length (atom separation) and its square
    real( Kind = wp ),                        intent( in    ) :: r, rsq

      !> calculated bond energy
    real( Kind = wp ),                        intent(   Out ) :: engbond, forc

    integer :: typ, k
    real(kind = wp) ::  a, b, c, d, e, f, r0, rc, dr, dra, drsq

    character :: chbond*100

    engbond = 0.0_wp
    forc = 0.0_wp

!AB: the check is done outside: if( ntpbond*exclude /= 0 ) ... (see field.f90)
!    if( ntpbond < 1 .or. pot == 0 ) return ! then
!        write(chbond,*)pot
!        call cry(uout,'', &
!                "WARNING: bond "//trim(adjustL(chbond))//" is skipped...",0)
!        return
!    end if

    !AB: negative value of 'pot' implies inclusion of VDW & Culomb outside
    k = abs(pot)

    !AB: due to bond type 'pot' stored in exclusion list where MAXINTD is for non-bonded pairs
    !check that the bond is within bounds

    if( k > ntpbond ) return

    typ = abs(ltpbond(k))

    select case(typ)

        case(1)

            ! 12-6 potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            engbond = (a / r**6 - b) / r**6

        case(2)

            ! lennard jones
            a = prmbond(1,k)
            b = prmbond(2,k)
            engbond = 4.0_wp*a*(b/r)**6*((b/r)**6-1.0_wp)

        case(3)

            ! n-m potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            d = prmbond(4,k)
            engbond = a / (b - c) * (c * (d / r)**b - b * (d / r)**c)

        case(4)

            ! Buckingham exp-6 potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            engbond = a * exp(-r / b) - c / r**6

        case(5)

            ! Born-Huggins-Meyer exp-6-8 potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            d = prmbond(4,k)
            e = prmbond(5,k)
            engbond = a * exp( b *(c - r)) - d / r**6 - e / r**8

        case(6)

            ! Hydrogen-bond 12-10 potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            engbond = a/r**12 - b/r**10

        case(7)

            !  quartic

            a = prmbond(1,k) / 2.0_wp
            b = prmbond(2,k) / 3.0_wp
            c = prmbond(3,k) / 4.0_wp
            dr = r - prmbond(4,k)
            engbond = a * dr**2 + b * dr**3 + c * dr**4

        case(8)

            ! Morse potential
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            engbond = a * ((1.0_wp - exp(-c * (r - b)))**2 - 1.0_wp)

        case(9)

            ! Weeks-chandler-Anderson (shifted & truncated Lenard-Jones) (i.t.todorov)
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            engbond = 4.0_wp * a *(b / (r - c))**6 * ((b / (r - c))**6 - 1.0_wp) + a

        case(10)

            ! Lennard -Jones coupled with FENE
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            d = prmbond(4,k)
            e = prmbond(5,k)
            engbond = 4.0_wp * a * (b / r)**6 * ((b / r)**6 - 1.0_wp) -0.5d0 * c * d**2 &
                        * log(1.d0 - ((r - e )/ d )**2)

        case(11)

            ! Weeks-chandler-Anderson coupled with FENE
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            d = prmbond(4,k)
            e = prmbond(5,k)
            f = prmbond(6,k)
            engbond = 4.0_wp * a * (b / (r - c))**6 * ((b / (r - c))**6 - 1.0_wp) + a -0.5_wp * d * e**2 &
                        * log(1.0_wp - ((r - f ) / e)**2)

        case(12)

            ! FENE
            a = prmbond(1,k)
            b = prmbond(2,k)
            c = prmbond(3,k)
            engbond = -0.5_wp * a * b**2 * log(1.0_wp - ((r - c ) / b)**2)

        case(13)

            ! harmonic
            a = prmbond(1,k)
            b = prmbond(2,k)
            engbond = 0.5_wp * a * (r - b)**2

        case(14)

            ! restrained harmonic
            a = prmbond(1,k)
            r0 = prmbond(2,k)
            dr = r - r0
            dra= abs(dr)
            rc = prmbond(3,k)
            engbond = a * (0.5_wp * min(dra,rc)**2 + rc * max(dra - rc,0.0_wp))

        case(15)

            ! amoeba
            a = prmbond(1,k)
            r0 = prmbond(2,k)
            dr = r - r0
            drsq = dr * dr

            engbond = a * drsq * (1.0_wp - 2.55_wp * dr + 7.0_wp/12.0_wp * 2.55_wp * drsq)

    end select

end subroutine bond_energy

end module
