!***************************************************************************
!   Copyright (C) 2021 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Module containing low-level procedures and variables pertaining to cell list objects.
!> - Errors and warnings are output to unit `uout` in `constants_module`
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @module for cell lists
module cell_list_module

    use kinds_f90
    use cell_list_type
    use constants_module, only : uout

    implicit none


contains


!> @brief
!> - Initialises the specified cell list object, given simulation box dimensions
!>   and minimal cell size
!> - Does NOT assign atoms to the cells
subroutine initialise_cell_list(cl, Lx, Ly, Lz, rmin, maxpercell)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Simulation box x dimension
    real(wp), intent(in) :: Lx

        !> Simulation box y dimension
    real(wp), intent(in) :: Ly

        !> Simulation box z dimension
    real(wp), intent(in) :: Lz

        !> Minimal size of a cell in the x, y or z directions
    real(wp), intent(in) :: rmin

        !> Maximum number of atoms allowed in a cell
    integer, intent(in) :: maxpercell

    integer :: i, j, k, n, nnbr, ishift, jshift, kshift
    integer :: ishiftmax, jshiftmax, kshiftmax, inbr, jnbr, knbr    
    integer :: status

    cl%Lx = Lx
    cl%Ly = Ly
    cl%Lz = Lz
    cl%rmin = rmin

    cl%Nx = int(Lx/rmin)
    cl%Ny = int(Ly/rmin)
    cl%Nz = int(Lz/rmin)

    ! Use a minimum of 1 cell in each Cartesian direction
    if(cl%Nx < 1) then 

        cl%Nx = 1
        call cry(uout,'', &
                    "WARNING: Cell lists used with only one cell along x direction!",0)

    end if

    if(cl%Ny < 1) then

        cl%Ny = 1
        call cry(uout,'', &
                    "WARNING: Cell lists used with only one cell along y direction!",0)
    end if

    if(cl%Nz < 1) then

        cl%Nz = 1
        call cry(uout,'', &
                    "WARNING: Cell lists used with only one cell along z direction!",0)

    end if

    cl%Ncells = cl%Nx * cl%Ny * cl%Nz

    cl%dx = Lx / cl%Nx
    cl%dy = Ly / cl%Ny
    cl%dz = Lz / cl%Nz

    cl%maxpercell = maxpercell

    allocate(cl%list(2,maxpercell,cl%Ncells), stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to allocate cell list object (cl%list)!",999)

    allocate(cl%natoms(cl%Ncells), stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to allocate cell list object (cl%Ncells)!",999)

    ! Cell list is empty for now - no atoms have been assigned to any cells
    cl%list = 0
    cl%natoms = 0

    ! Set up the cell neighbour lists, assuming 3D periodic boundary conditions
    cl%maxneighbours = 27
    allocate(cl%nbrlist(0:cl%maxneighbours,cl%Ncells), stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to allocate cell list object (cl%nbrlist)!",999)
    cl%nbrlist = 0    

    ! (i,j,k) refers to the cell at position (i,j,k) along the (x,y,z)-dimensions
    n = 0    
    do k = 1, cl%Nz
        do j = 1, cl%Ny
            do i = 1, cl%Nx

                ! 'n' is the cell index for this (i,j,k)
                n = n + 1

                ! Calculate the indices of the cells connected to cell 'n' <=> (i,j,k)
                ! by looping over the neighbouring cells, taking care not to over-count
                ! cells if we have nx<3, ny<3 or nz<3

                ishiftmax = 1
                jshiftmax = 1
                kshiftmax = 1

                ! The below modifies the upper bound of the loop over neighbouring 
                ! cells to (i,j,k) to only, e.g. have 1 non-self neighbouring cell in the 
                ! x direction if nx==2, and 0 non-self neighbouring cell in the x direction
                ! if nx==1.
                if(cl%nx<3) ishiftmax = ishiftmax - (3-cl%nx)
                if(cl%ny<3) jshiftmax = jshiftmax - (3-cl%ny)
                if(cl%nz<3) kshiftmax = kshiftmax - (3-cl%nz)

                do kshift = -1, kshiftmax
                    do jshift = -1, jshiftmax
                        do ishift = -1, ishiftmax

                            inbr = i + ishift
                            jnbr = j + jshift
                            knbr = k + kshift

                            ! Apply periodic boundary conditions
                            inbr = mod(inbr,cl%nx)
                            if(inbr == 0) inbr = cl%nx
                            jnbr = mod(jnbr,cl%ny)
                            if(jnbr == 0) jnbr = cl%ny
                            knbr = mod(knbr,cl%nz)
                            if(knbr == 0) knbr = cl%nz

                            nnbr = cell_from_ijk(cl, inbr, jnbr, knbr)

                            call add_neighbouring_cell(cl, n, nnbr)

                        end do
                    end do
                end do


            end do
        end do
    end do

end subroutine initialise_cell_list




!> @brief
!> - Deallocates the specified cell list object, and sets all scalar variables
!>   to zero
subroutine deallocate_cell_list(cl)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

    integer :: status

    cl%Lx = 0.0_wp
    cl%Ly = 0.0_wp
    cl%Lz = 0.0_wp

    cl%Nx = 0
    cl%Ny = 0
    cl%Nz = 0
    cl%Ncells = 0

    cl%dx = 0.0_wp
    cl%dy = 0.0_wp
    cl%dz = 0.0_wp

    cl%maxneighbours = 0

    deallocate(cl%list, stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to deallocate cell list object (cl%list)!",999)
    deallocate(cl%natoms, stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to deallocate cell list object (cl%natom)!",999)
    deallocate(cl%nbrlist, stat=status)
    if( status /= 0 ) call cry(uout,'', &
                              "ERROR: Failed to deallocate cell list object (cl%nbrlist)!",999)

end subroutine deallocate_cell_list



!> Prints information about a specific cell to the specified unit
subroutine print_cell_info(cl, unit, n)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(in) :: cl

        !> Unit number to output information to
    integer, intent(in) :: unit

        !> Cell number to print information for
    integer, intent(in) :: n

    integer :: m, intvec(3), nnbr

    intvec = ijk_from_cell(cl,n)

    write(unit,*) "Information for cell ",n,":"
    write(unit,*) 
    write(unit,*) "  Integer coordinates (i,j,k) : ",intvec
    write(unit,*) "  No. of neighbour cells      : ",cl%nbrlist(0,n)

    if( cl%nbrlist(0,n) > 0 ) then

        write(unit,*) "  List of neighbour cells :"

        do m = 1, cl%nbrlist(0,n)

             nnbr = cl%nbrlist(m, n)
             intvec = ijk_from_cell(cl,nnbr)

            write(unit,*) "    cell index, i, j, k : ", nnbr, intvec

         end do

    else

        write(unit,*) "   WARNING: Cell has no neighbour cells!"

    end if

    write(unit,*) "  No. of atoms in cell        : ",cl%natoms(n)

    if( cl%natoms(n) > 0 ) then

        write(unit,*) "  List of atoms in cell :"

        do m = 1, cl%natoms(n)

            write(unit,*) "    molecule, atom : ",cl%list(1,m,n)," , ",cl%list(2,m,n)

        end do

    else

        write(unit,*) "  Cell is empty!"

    end if

end subroutine print_cell_info



!> Prints information about the specified cell list to the specified unit
subroutine print_cell_list_info(cl, unit, cellinfo)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(in) :: cl

        !> Unit number to output information to
    integer, intent(in) :: unit

        !> True if information about each individual cell is to be output, false if not
    logical, intent(in) :: cellinfo

    integer :: n

    write(unit,*)
    write(unit,*) 
    write(unit,*) "No. of cells in total         : ", cl%Ncells
    write(unit,*) 
    write(unit,*) "No. of cells in x dimension   : ", cl%Nx
    write(unit,*) 
    write(unit,*) "No. of cells in y dimension   : ", cl%Ny
    write(unit,*) 
    write(unit,*) "No. of cells in z dimension   : ", cl%Nz
    write(unit,*) 
    write(unit,*) "Simulation box x dimension    : ", cl%Lx
    write(unit,*) 
    write(unit,*) "Simulation box y dimension    : ", cl%Ly
    write(unit,*) 
    write(unit,*) "Simulation box z dimension    : ", cl%Lz
    write(unit,*) 
    write(unit,*) "Specified min. cell dimension : ", cl%rmin
    write(unit,*) 
    write(unit,*) "x dimension of a cell         : ", cl%dx
    write(unit,*) 
    write(unit,*) "y dimension of a cell         : ", cl%dy
    write(unit,*) 
    write(unit,*) "z dimension of a cell         : ", cl%dz
    write(unit,*) 
    write(unit,*) "Max. no. of cell neighbours   : ", cl%maxneighbours
    write(unit,*) 
    write(unit,*) "Max. no. of atoms per cell    : ", cl%maxpercell
    write(unit,*) 
    write(unit,*) 

    if( cellinfo ) then

        do n = 1, cl%Ncells

            call print_cell_info(cl, unit, n)
            write(unit,*) 

        end do

    end if

end subroutine print_cell_list_info




!> @brief
!> - Adds a cell to the cell neighbour list of a specified cell
subroutine add_neighbouring_cell(cl, n, nnbr)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Cell whose neighbour list we are considering
    integer, intent(in) :: n

        !> Cell which we are adding to the neighbour list of 'n'
    integer, intent(in) :: nnbr

    character :: chnum1*10, chnum2*10

    ! Add 1 to the count of neighbouring cells for cell 'n'
    cl%nbrlist(0,n) = cl%nbrlist(0,n) + 1

    if( cl%nbrlist(0,n) > cl%maxneighbours ) then

        write(chnum1,'(i10)') n
        write(chnum2,'(i10)') cl%maxneighbours

        call cry(uout,'', &
                    "ERROR: Max. number of neighbours "//trim(adjustl(chnum2))// &
                    " for cell "//trim(adjustl(chnum1))//" has been exceeded!",999)

    end if

    cl%nbrlist( cl%nbrlist(0,n), n ) = nnbr

end subroutine add_neighbouring_cell




!>@brief
!> - Returns the cell number for a cell at given integer coordinates along the x, y and z dimensions
integer function cell_from_ijk(cl, i, j,k)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(in) :: cl

        !> The cell under consideration is in the 'i'th row of cells along the x-dimension, where
        !> 'i' is between 1 and 'cl%nx'
    integer, intent(in) :: i

        !> The cell under consideration is in the 'j'th row of cells along the y-dimension, where
        !> 'j' is between 1 and 'cl%ny'
    integer, intent(in) :: j

        !> The cell under consideration is in the 'k'th row of cells along the z-dimension, where
        !> 'k' is between 1 and 'cl%nz'
    integer, intent(in) :: k

    cell_from_ijk = i + (j-1)*cl%nx + (k-1)*cl%nx*cl%ny

end function cell_from_ijk



!> @brief
!> - Returns the integer coordinates along the x, y and z dimensions of a cell given the index of the cell
!> - Inverse of the `cell_from_ijk` function
function ijk_from_cell(cl, n)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(in) :: cl

        !> The index of the cell
    integer, intent(in) :: n

    integer :: ijk_from_cell(3)

    integer :: i, j, k, nprime

    ijk_from_cell = 0

    ! I deliberately use integer division here to floor the fraction
    k = 1 + (n-1)/(cl%Nx*cl%Ny)

    nprime = n - (k-1)*cl%Nx*cl%Ny

    ! I deliberately use integer division here to floor the fraction
    j = 1 + (nprime-1)/cl%Nx

    i = nprime - (j-1)*cl%Nx

    ijk_from_cell(1) = i
    ijk_from_cell(2) = j
    ijk_from_cell(3) = k

end function ijk_from_cell




!>@brief
!> - Returns the cell number for a given position in Cartesian coordinates 
!> - x coordinate is assumed to be in between 0 (inclusive) and Lx (exclusive),
!>   where Lx is the x dimension of the box, and similarly for the y and z coordinates
integer function cell_from_position(cl, xpos, ypos, zpos)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(in) :: cl

        !> x coordinate in the range [0,Lx)
    real(wp), intent(in) :: xpos

        !> y coordinate in the range [0,Ly)
    real(wp), intent(in) :: ypos

        !> z coordinate in the range [0,Lz)
    real(wp), intent(in) :: zpos

    real(wp) :: x, y, z
    integer :: i, j, k

    x = xpos
    y = ypos
    z = zpos

    ! Convert to units of the cell dimension for each dimension
    x = x / cl%dx
    y = y / cl%dy
    z = z / cl%dz

    ! Get the integer position of the cell
    i = int(x) + 1
    j = int(y) + 1
    k = int(z) + 1

    ! Apply the mapping from the integer position (x,y,z) to the cell index
    cell_from_position = cell_from_ijk(cl, i, j, k)

end function cell_from_position




!> @brief
!> - Adds a specified atom to a specified cell
subroutine add_atom_to_cell(cl, cell, im, ia)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Cell number to add the atom to
    integer, intent(in) :: cell

        !> Molecule index for atom
    integer, intent(in) :: im

        !> Atom index within the molecule
    integer, intent(in) :: ia

    integer :: natoms
    character :: chnum1*10, chnum2*10

    natoms = cl%natoms(cell) + 1
    cl%natoms(cell) = natoms

    if( natoms > cl%maxpercell ) then

        write(chnum1,'(i10)') cell
        write(chnum2,'(i10)') cl%maxpercell

        call cry(uout,'', &
                    "ERROR: Max. number of atoms "//trim(adjustl(chnum2))//" for cell "// &
                    trim(adjustl(chnum1))//" has been exceeded!",999)

    end if

    cl%list(1,natoms,cell) = im
    cl%list(2,natoms,cell) = ia

end subroutine add_atom_to_cell




!> @brief
!> - Removes a specified atom from a specified cell
!> - If the specified cell is 0 (or any other cell index out of bounds) then
!>   this subroutine will fail
!> - Only removes the first encountered instance of the atom, which is
!>   fine since there should never be two instances of an atom in a cell!
subroutine remove_atom_from_cell(cl, cell, im, ia)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Cell number to remove the atom from
    integer, intent(in) :: cell

        !> Molecule index for atom
    integer, intent(in) :: im

        !> Atom index within the molecule
    integer, intent(in) :: ia

    integer :: m, natoms
    character :: chnum1*10, chnum2*10, chnum3*10

    natoms = cl%natoms(cell)

    ! Loop through atoms in the cell to find the one we want
    do m = 1, natoms

        if( cl%list(1,m,cell) == im .and. cl%list(2,m,cell) == ia ) then        

            ! Move the last atom in the cell list into the slot of the atom
            ! to be removed
            cl%list(:,m,cell) = cl%list(:,natoms,cell)

            ! Clear the last slot
            cl%list(:,natoms,cell) = 0

            ! Amend the number of atoms in the list to reflect the deletion
            cl%natoms(cell) = natoms - 1

            return

        end if

    end do

    ! If we don't find the atom in the cell...

    write(chnum1,'(i10)') cell
    write(chnum2,'(i10)') im
    write(chnum3,'(i10)') ia

    call cry(uout,'', &
                "ERROR: Cannot remove atom "//trim(adjustl(chnum3))//" in mol "//trim(adjustl(chnum2))// &
                " from cell "//trim(adjustl(chnum1))//" since atom is not in cell!",999)

end subroutine remove_atom_from_cell




!> Returns a logical corresponding to whether or not the specified atom is in the specified
!> cell. This function is safe for cell indices which are out of bounds (e.g. 0), in which
!> case the logical value returned will be .false.
logical function is_atom_in_cell(cl, cell, im, ia)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Cell number to query
    integer, intent(in) :: cell

        !> Molecule index for atom to query
    integer, intent(in) :: im

        !> Atom index within the molecule to query
    integer, intent(in) :: ia

    integer :: m, natoms
    character :: chnum1*10

    natoms = cl%natoms(cell)

    is_atom_in_cell = .false.

    if( cell < 1 .or. cell > cl%Ncells) then

        write(chnum1,'(i10)') cell

        call cry(uout,'', &
                "ERROR: is_atom_in_cell: Cell index "//trim(adjustl(chnum1))//" is not within valid range!",999)

    end if


    ! Loop through atoms in the cell to find the one we are looking for
    do m = 1, natoms

        if( cl%list(1,m,cell) == im .and. cl%list(2,m,cell) == ia ) then

            is_atom_in_cell = .true.
            return

        end if

    end do

end function is_atom_in_cell




!> Relabels an atom in a cell list object. This is intended to be used when an
!> atom i is deleted from a system and the 'last' atom in the system 
!> is relabelled as i to keep continuity in the molecule array.
subroutine relabel_atom(cl, imtarget, iatarget, imdest, iadest)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> Atom with (molecule index, atom index) (imtarget, iatarget) is to be relabelled (imdest,iadest)
    integer, intent(in) :: imtarget

        !> Atom with (molecule index, atom index) (imtarget, iatarget) is to be relabelled (imdest,iadest)
    integer, intent(in) :: iatarget

        !> Atom with (molecule index, atom index) (imtarget, iatarget) is to be relabelled (imdest,iadest)
    integer, intent(in) :: imdest

        !> Atom with (molecule index, atom index) (imtarget, iatarget) is to be relabelled (imdest,iadest)
    integer, intent(in) :: iadest

    integer :: cell, m

    do cell = 1, cl%Ncells

        do m = 1, cl%natoms(cell)

            if( cl%list(1,m,cell) == imtarget .and. cl%list(2,m,cell) == iatarget ) then

                cl%list(1,m,cell) = imdest
                cl%list(2,m,cell) = iadest

            end if

        end do

    end do

end subroutine relabel_atom




!> Relabels a molecule in a cell list object. This is intended to be used when a
!> molecule i is deleted from a system and the 'last' molecule in the system 
!> is relabelled as i to keep continuity in the molecule array.
subroutine relabel_molecule(cl, imtarget, imdest)

    implicit none

        !> Cell list under consideration
    type(cell_list), intent(inout) :: cl

        !> The molecule `imtarget` is to be relabelled as `imdest`
    integer, intent(in) :: imtarget

        !> The molecule `imtarget' is to be relabelled as `imdest`
    integer, intent(in) :: imdest

    integer :: cell, m

    do cell = 1, cl%Ncells

        do m = 1, cl%natoms(cell)

            if( cl%list(1,m,cell) == imtarget ) then

                cl%list(1,m,cell) = imdest

            end if

        end do

    end do

end subroutine relabel_molecule


end module cell_list_module
