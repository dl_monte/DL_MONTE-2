!***************************************************************************
!   Copyright (C) 2015 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Contains Monte Carlo moves for phase-switch Monte Carlo, and counters for the moves
!> @usage 
!> - @stdusage 
!> - @stdspecs
!>
!> @modulecontaining phase-switch Monte Carlo moves
module psmc_moves


    implicit none


        !> Total number of switch moves
    integer :: total_switch_moves
    
        !> Number of successful switch moves
    integer :: successful_switch_moves


contains


!> @brief
!> - Sets PSMC counters to zero
subroutine initialise_psmc_counters()

    implicit none

    total_switch_moves = 0
    successful_switch_moves = 0

end subroutine initialise_psmc_counters



!> @brief
!> - Performs a switch Monte Carlo move: attempts to swap the active and passive box numbers
!> - Supports switches which increase/decrease the unit cell volume, and assumes that the
!>   displacements are increased/decreased in the switch commensurately with the unit cell
!>   (the acceptance probability is given by Eqn. A14 in G. C. McNeil-Watson & N. B. Wilding,
!>   Journal of Chemical Physics 124, 064504 (2006)) 
!> @using
!> - `kinds_f90`
!> - `config_type`
!> - `config_type`
!> - `random_module, only : duni`
!> - `constants_module, only : FED_PAR_PSMC, FED_PAR_PSMC_HS`
!> - `fed_calculus_module, only : fed`
!> - `fed_order_module, only : fed_param_dbias, fed_param_hist_inc`
subroutine switch_move(job, ib_act, ib_pas, beta, betainv, extpress, energytot, cfgs)

    use kinds_f90
    use control_type
    use config_type
    use random_module, only : duni
    use constants_module, only : FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_param_dbias_psmc, fed_param_hist_inc, fed_window_reject
    use psmc_module, only : psmc

        !> Control parameters for simulation
    type(control), intent(in) :: job

        !> Active box number
    integer, intent(inout) :: ib_act

        !> Passive box number
    integer, intent(inout) :: ib_pas

        !> Thermodynamic beta
    real(kind=wp), intent(in) :: beta

        !> Inverse of thermodynamic beta
    real(kind=wp), intent(in) :: betainv

        !> External pressure (unimportant for volume preserving moves)
    real(kind=wp), intent(in) :: extpress

        !> Array containing the total energies of all boxes (only 1 and 2 are used)
    real(kind=wp), intent(in) :: energytot(:)

        !> Array containing the config objects associated with all boxes (only 1 and 2 are used).
        !> Note that this procedure does not intend to alter 'cfgs', despite this variable's 
        !> 'inout' status.
    type(config), intent(inout) :: cfgs(:)


        ! Energy change associated with switch, and 'energy change' used to evaluate acceptance probability
    real(kind=wp) :: deltav, deltavb

        ! Analogue of 'deltavb' without any biasing (the 'canonical' value)
    real(kind=wp) :: deltavb_can

    real(kind=wp) :: arg, volnew, volold

    integer :: ib_temp

    logical :: is_fed_param_psmc

    total_switch_moves = total_switch_moves + 1

    deltavb = 0.0_wp
    
    ! If PSMC order parameter is in use then accordingly bias the acceptance probability
    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    !TU: WARNING: The coding henceforth will surely case problems with FED if something other than a PSMC order 
    !TU: parameter is in use.
    if( is_fed_param_psmc ) then
       
        ! The difference in the bias between the new state and the current state is given by
        ! fed_param_dbias_psmc(energy1, energy2) for PSMC order parameter, where energy1 and energy2 are the 
        ! NEW energies of phases 1 and 2
        deltavb = fed_param_dbias_psmc(energytot(1), energytot(2))

        ! Stay within the allowed range
        if( deltavb > job%toler ) goto 1000
       
    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not PSMC (in case
    !TU: lattice switch moves are used with an order parameter other than a PSMC order parameter - is this
    !TU: even allowed in DL_MONTE, or indeed practically useful?). 'In general', here is where 'deltavb' should 
    !TU: be set.    
    
    deltav = energytot(ib_pas) - energytot(ib_act) 
    volnew = cfgs(ib_pas) % vec % volume
    volold = cfgs(ib_act) % vec % volume

    ! deltavb if there were no biasing ('canonical')
    ! Core of acceptance rule: note that I implicitly assume here that num_vol_sites is the same for the active
    ! and passive boxes - which should always be the case for PSMC
    if( psmc%cart_disp_switch ) then

        if( psmc%ignore_vol_scaling ) then

            deltavb_can = beta * deltav

        else

            !TU: This is the acceptance rule for scaling of volume (isotropically) and NO scaling of displacements 
            !TU: (of particles from their lattice sites) during the switch (i.e. preservation of Cartesian displacements)
            deltavb_can = beta * ( deltav + extpress * (volnew - volold) - betainv * log(volnew / volold) )


        end if

    else

        if( psmc%ignore_vol_scaling ) then

            !TU: volnew/volold still enters here because of the scaling of the atom displacements during the switch
            !TU: move... which implicitly depends on the volumes of the phase-1 and phase-2 simulation boxes
            deltavb_can = beta * (deltav -  betainv * log(volnew / volold))

        else
    
            !TU: This is the acceptance rule for scaling of volume (isotropically) and scaling of displacements 
            !TU: (of particles from their lattice sites) during the switch
            deltavb_can = beta * (deltav + extpress * (volnew - volold) - (cfgs(ib_act)%num_vol_sites + 1)  &
                   * betainv * log(volnew / volold))

        end if

    end if

    ! deltavb which includes biasing if biasing is in use
    deltavb = deltavb + deltavb_can

    ! If we are using an extra bias between phases 1 and 2...
    if( ib_act == 1 ) then

        deltavb = deltavb + psmc%switchbias

    else

        deltavb = deltavb - psmc%switchbias

    end if

    ! Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    ! min(1,exp(-deltavb_can)).
    if( fed % method == FED_TM .and. is_fed_param_psmc ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us fuether from the window                          
    if( fed % is_window .and. is_fed_param_psmc ) then
               
        if( fed_window_reject() ) goto 1000
               
    end if


    ! Random number
    arg = duni()

    if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high

    if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then

        ! Move accepted...

        successful_switch_moves = successful_switch_moves + 1

        ! Swap the active and passive box labels
        ib_temp = ib_act
        ib_act = ib_pas
        ib_pas = ib_temp

        !TU: update the FED histogram & bias if needed (e.g. WL)
        !TU: I am assuming this is only ever called with a PSMC order parameter - there is no 'general'
        !TU: use for switch moves
        if( fed%is_on ) call fed_param_hist_inc(ib_act,.true.)

        return

    end if



! Goto line if move is rejected...

          !TU: update the FED histogram & bias if needed (e.g. WL) for PSMC order parameter
          !TU: I am assuming this is only ever called with a PSMC order parameter - there is no 'general'
          !TU: use for switch moves
1000      if( fed%is_on ) call fed_param_hist_inc(ib_act,.false.)

end subroutine switch_move




end module psmc_moves
