! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module molecule_module

    implicit none

contains

!> allocates the arrays for book-keeping lists: neighbours, bonds, angles, dihedrals, exclusions etc
subroutine alloc_and_copy_mol_arrays(mol1, mol2)

! NOTE: `nbrlist` routine ONLY allocates non-bonded neighbour-list and exclist routine only does `exclist`, 
! so both need to be called if nbrlist in use.

    use kinds_f90
    use molecule_type
    use bondlist_module, only : alloc_and_copy_bondlist_arrays
    use anglist_module, only : alloc_and_copy_anglist_arrays
    use invlist_module, only : alloc_and_copy_invlist_arrays
    use dihlist_module, only : alloc_and_copy_dihlist_arrays

    implicit none

    type(molecule), intent(inout) :: mol1,mol2

    call alloc_and_copy_bondlist_arrays(mol1%blist, mol2%blist)
    call alloc_and_copy_anglist_arrays(mol1%alist, mol2%alist)
    call alloc_and_copy_invlist_arrays(mol1%ilist, mol2%ilist)
    call alloc_and_copy_dihlist_arrays(mol1%dlist, mol2%dlist)

end subroutine alloc_and_copy_mol_arrays

subroutine alloc_mol_atms_arrays(mol, num, nmax, lauto_nbrs, ldispl)

    use control_type
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: num, nmax
    logical, intent(in) :: lauto_nbrs, ldispl

    integer :: i, fail(3)

    fail = 0

    if (nmax == 0) then 

        mol%mxatom = num

    else

        mol%mxatom = nmax

    endif

    mol%natom = num

    !create sufficient no of atoms for this molecule
    allocate(mol%atms(nmax), stat = fail(1))
    allocate(mol%glob_no(nmax), stat = fail(2))

    if (any(fail > 0)) then

        call error(314)

    endif

    do i = 1, num

        mol%glob_no(i) = 0

        mol%atms(i)%atlabel = -1
        mol%atms(i)%site = -1
        mol%atms(i)%atype = -1

    !if automatic nbr list set position store
    if (lauto_nbrs .or. ldispl) allocate(mol%atms(i)%r_zero(3), stat = fail(3))

    if (any(fail > 0)) then

        call error(314)

    endif

    if (allocated(mol%atms(i)%r_zero)) mol%atms(i)%r_zero(:) = 0.0_wp

    enddo

    !setup neighbourlist arrays as required
    mol%blist%npairs = 0
    mol%alist%nang = 0
    mol%ilist%ninv = 0
    mol%dlist%ndih = 0

end subroutine

subroutine alloc_mol_smden_arrays(mol)

    use molecule_type

    implicit none

    type (molecule), intent(inout) :: mol

    integer :: fail

    fail = 0
    allocate(mol%smden(mol%mxatom), stat = fail)

    if (fail > 0) then

        call error(314)

    endif

    mol%has_smden = .true.
    mol%smden = 0.0_wp

end subroutine

subroutine set_max_number_of_atoms(mol, num)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: num

    mol%mxatom = num

end subroutine

subroutine set_molecule_mass(mol)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol

    integer :: j

    mol%mass = 0.0_wp

    do j = 1, mol%natom

        mol%mass = mol%mass + mol%atms(j)%mass

    enddo

end subroutine

subroutine set_molecule_charge(mol)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol

    integer :: j

    mol%has_charge = .false.

    mol%charge = 0.0_wp

    do j = 1, mol%natom

        mol%has_charge = ( mol%has_charge .or. abs(mol%atms(j)%charge) > 1.0e-8_wp)

        mol%charge = mol%charge + mol%atms(j)%charge

    enddo

end subroutine

subroutine add_molecule_mass(mol, dmass)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol

    real(kind=wp) :: dmass

    mol%mass = mol%mass + dmass

end subroutine

subroutine add_molecule_charge(mol, dcharge)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol

    real(kind=wp) :: dcharge

    mol%charge = mol%charge + dcharge

    mol%has_charge = ( mol%has_charge .or. abs(dcharge) > 1.0e-8 )

end subroutine

subroutine set_molecule_name(mol, mlname)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    character*8, intent(in) :: mlname

    mol%molname = mlname

end subroutine

subroutine set_molecule_label(mol, i)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: i

    mol%mol_label = i

end subroutine

subroutine set_molecule_idcom(mol, i)

    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: i

    mol%idcom = i

end subroutine

subroutine set_molecule_bondlist(mol, bnd)

    use molecule_type
    use bondlist_type
    use bondlist_module, only : alloc_and_copy_bondlist_arrays

    implicit none

    type(molecule), intent(inout) :: mol
    type(bondlist), intent(in)    :: bnd

    call alloc_and_copy_bondlist_arrays(mol%blist, bnd)

end subroutine

subroutine set_molecule_anglist(mol, ang)

    use molecule_type
    use anglist_type
    use anglist_module, only : alloc_and_copy_anglist_arrays

    implicit none

    type(molecule), intent(inout) :: mol
    type(anglist), intent(in)    :: ang

    call alloc_and_copy_anglist_arrays(mol%alist, ang)

end subroutine

subroutine set_molecule_invlist(mol, inv)

    use molecule_type
    use invlist_type
    use invlist_module, only : alloc_and_copy_invlist_arrays

    implicit none

    type(molecule), intent(inout) :: mol
    type(invlist), intent(in)    :: inv

    call alloc_and_copy_invlist_arrays(mol%ilist, inv)

end subroutine

subroutine set_molecule_dihlist(mol, dih)

    use molecule_type
    use dihlist_type
    use dihlist_module, only : alloc_and_copy_dihlist_arrays

    implicit none

    type(molecule), intent(inout) :: mol
    type(dihlist), intent(in)    :: dih

    call alloc_and_copy_dihlist_arrays(mol%dlist, dih)

end subroutine

subroutine realloc_mol_atms_arrays(mol, num, nmax)

    use constants_module, only : uout
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: num, nmax

    integer :: fail(8)

    fail = 0

!   if(allocated(mol%atlabel)) deallocate(mol%atlabel)
!   if(allocated(mol%site)) deallocate(mol%site)
!   if(allocated(mol%atype)) deallocate(mol%atype)
!   if(allocated(mol%glob_no)) deallocate(mol%glob_no)
!   if(allocated(mol%rpos)) deallocate(mol%rpos)
!   if(allocated(mol%store_rpos)) deallocate(mol%store_rpos)
!   if(allocated(mol%mass)) deallocate(mol%mass)
!   if(allocated(mol%charge)) deallocate(mol%charge)

    if (nmax == 0) then
        mol%mxatom = num
    else
        mol%mxatom = nmax
    endif

    mol%natom = num

!   allocate(mol%atlabel(mol%mxatom), stat = fail(2))
!   allocate(mol%site(mol%mxatom), stat = fail(3))
!   allocate(mol%atype(mol%mxatom), stat = fail(4))
!   allocate(mol%glob_no(mol%mxatom), stat = fail(5))
!   allocate(mol%rpos(mol%mxatom,3), stat = fail(6))
!   allocate(mol%store_rpos(mol%mxatom,3), stat = fail(6))
!   allocate(mol%mass(mol%mxatom), stat = fail(7))
!   allocate(mol%charge(mol%mxatom), stat = fail(8))

    if (any(fail > 0)) then

        call error(314)

    endif


!   mol%atlabel = -1
!   mol%site = -1
!   mol%atype = -1

end subroutine

subroutine dealloc_mol_atms_arrays(mol)

    use constants_module, only : uout
    use molecule_type
    use nbrlist_module, only : dealloc_nbrlist_arrays, dealloc_exclist_array
    use bondlist_module, only : dealloc_bondlist_arrays

    implicit none

    type(molecule), intent(inout) :: mol

    if(allocated(mol%atms)) deallocate(mol%atms)
    if(allocated(mol%glob_no)) deallocate(mol%glob_no)
    if(allocated(mol%smden)) deallocate(mol%smden)

    mol%mxatom = 0

    mol%natom = 0

    !nbrlists
    call dealloc_bondlist_arrays(mol%blist)
    call dealloc_nbrlist_arrays(mol%nlist)
    if(mol%blist%npairs>0)call dealloc_exclist_array(mol%nlist)

end subroutine

!allocate some of the arrays
subroutine alloc_small_mol_arrays(mol, num, nmax)

    use constants_module, only : uout
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    integer, intent(in) :: num, nmax

    integer :: fail(1)

    fail = 0

    if (nmax == 0) then
        mol%mxatom = num
    else
        mol%mxatom = nmax
    endif

    mol%natom = num

    allocate(mol%atms(mol%mxatom), stat = fail(1))

    if (any(fail > 0)) then

        call error(314)

    endif

end subroutine


!> the aim is to 'undo' PBC for a molecule \n
!> BUT beware of atoms more than half any box dimension away from the first one 
subroutine reform_molecule_frac(ml)

! put the atoms of a molecule outside the box 
! to allow calculation of the centre of mass prior to rotation of the molecule

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: ml

    integer :: j
    real(kind = wp) :: orig(3), xx, yy, zz, xxx, yyy, zzz

    !the first molecule acts as origin
    orig(:) = ml%atms(1)%rpos(:)

    do j = 2, ml%natom

        xx = ml%atms(j)%rpos(1) - orig(1)
        yy = ml%atms(j)%rpos(2) - orig(2)
        zz = ml%atms(j)%rpos(3) - orig(3)

        xx = xx-anint(xx)
        yy = yy-anint(yy)
        zz = zz-anint(zz)

        ml%atms(j)%rpos(1) = orig(1) + xx
        ml%atms(j)%rpos(2) = orig(2) + yy
        ml%atms(j)%rpos(3) = orig(3) + zz

    enddo

end subroutine


!> the aim is to 'undo' PBC for a molecule \n
!> BUT beware of atoms more than half any box dimension away from the first one 
subroutine reform_molecule_ortho(ml, latvec)

! put the atoms of a molecule outside the box 
! to allow calculation of the centre of mass prior to rotation of the molecule

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: ml
    real(kind = wp), intent (in) :: latvec(3,3)
    
    integer :: j
    real(kind = wp) :: orig(3), xx, yy, zz, xxx, yyy, zzz

    !the first molecule acts as origin
    orig(:) = ml%atms(1)%rpos(:)

    do j = 2, ml%natom

        xx = (ml%atms(j)%rpos(1) - orig(1))/latvec(1,1)
        yy = (ml%atms(j)%rpos(2) - orig(2))/latvec(2,2)
        zz = (ml%atms(j)%rpos(3) - orig(3))/latvec(3,3)

        xx = xx-anint(xx)
        yy = yy-anint(yy)
        zz = zz-anint(zz)

        ml%atms(j)%rpos(1) = orig(1) + xx*latvec(1,1)
        ml%atms(j)%rpos(2) = orig(2) + yy*latvec(2,2)
        ml%atms(j)%rpos(3) = orig(3) + zz*latvec(3,3)

    enddo

!    do j = 1, ml%natom
!
!        if( ml%atms(j)%rpos(1) < 0.0_wp ) ml%atms(j)%rpos(1) = ml%atms(j)%rpos(1) + latvec(1,1)!!
!        if( ml%atms(j)%rpos(2) < 0.0_wp ) ml%atms(j)%rpos(2) = ml%atms(j)%rpos(2) + latvec(2,2)!!
!        if( ml%atms(j)%rpos(3) < 0.0_wp ) ml%atms(j)%rpos(3) = ml%atms(j)%rpos(3) + latvec(3,3)!!
!
!    enddo

end subroutine reform_molecule_ortho


!> the aim is to 'undo' PBC for a molecule \n
!> BUT beware of atoms more than half any box dimension away from the first one 
subroutine full_reform_molecule(mol, invlatvec, latvector)

! put the atoms of a molecule outside the box 
! to allow calculation of the centre of mass prior to rotation of the molecule

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: mol

    real(kind = wp), intent(in) :: invlatvec(3,3)

    real(kind = wp), intent(in) :: latvector(3,3)

    real(kind = wp) :: orig(3), rx, ry, rz, xx, yy, zz

    integer :: j
    !real(kind = wp) :: orig(3), xx, yy, zz, xxx, yyy, zzz

    rx = mol%atms(1)%rpos(1)
    ry = mol%atms(1)%rpos(2)
    rz = mol%atms(1)%rpos(3)

    !the first molecule acts as origin
    !orig(:) = mol%atms(1)%rpos(:)

    !AB: fractional (transformed) coordinates
    orig(1) = invlatvec(1,1) * rx + invlatvec(2,1) * ry + invlatvec(3,1) * rz
    orig(2) = invlatvec(1,2) * rx + invlatvec(2,2) * ry + invlatvec(3,2) * rz
    orig(3) = invlatvec(1,3) * rx + invlatvec(2,3) * ry + invlatvec(3,3) * rz

    do j = 2, mol%natom

        !xx = mol%atms(j)%rpos(1) - orig(1)
        !yy = mol%atms(j)%rpos(2) - orig(2)
        !zz = mol%atms(j)%rpos(3) - orig(3)

        !xx = nint(xx)
        !yy = nint(yy)
        !zz = nint(zz)

        !mol%atms(j)%rpos(1) = mol%atms(j)%rpos(1) - xx
        !mol%atms(j)%rpos(2) = mol%atms(j)%rpos(2) - yy
        !mol%atms(j)%rpos(3) = mol%atms(j)%rpos(3) - zz

        rx = mol%atms(j)%rpos(1)
        ry = mol%atms(j)%rpos(2)
        rz = mol%atms(j)%rpos(3)

        !AB: fractional (transformed) coordinates
        xx = invlatvec(1,1) * rx + invlatvec(2,1) * ry + invlatvec(3,1) * rz !- orig(1)
        yy = invlatvec(1,2) * rx + invlatvec(2,2) * ry + invlatvec(3,2) * rz !- orig(2)
        zz = invlatvec(1,3) * rx + invlatvec(2,3) * ry + invlatvec(3,3) * rz !- orig(3)

        !AB: unwrapped (reformed) coordinates in the fractional space
        rx = xx - anint(xx-orig(1))
        ry = yy - anint(yy-orig(2))
        rz = zz - anint(zz-orig(3))

        !if( abs(xx) > 0.5_wp ) &
        !    xx = xx - anint(xx)

        !if( abs(yy) > 0.5_wp ) &
        !    yy = yy - anint(yy)

        !if( abs(zz) > 0.5_wp ) &
        !    zz = zz - anint(zz)

        !AB: Cartesian (transformed back) coordinates
        xx = latvector(1,1) * rx + latvector(2,1) * ry + latvector(3,1) * rz
        yy = latvector(1,2) * rx + latvector(2,2) * ry + latvector(3,2) * rz
        zz = latvector(1,3) * rx + latvector(2,3) * ry + latvector(3,3) * rz

        mol%atms(j)%rpos(1) = xx
        mol%atms(j)%rpos(2) = yy
        mol%atms(j)%rpos(3) = zz

    enddo

end subroutine


!> calculates the centre of mass of a molecule \n
!> NOTE: need to 'reform' the molecule, i.e. 'undo PBC', before doing this
subroutine mol_com(ml)

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: ml

    integer :: j
    real(kind = wp) :: wght, xsum, ysum, zsum, msum

    xsum = 0.0_wp
    ysum = 0.0_wp
    zsum = 0.0_wp
    msum = 0.0_wp

    do j = 1, ml%natom

        wght = ml%atms(j)%mass
        msum = msum + wght

        xsum = xsum + ml%atms(j)%rpos(1) * wght
        ysum = ysum + ml%atms(j)%rpos(2) * wght
        zsum = zsum + ml%atms(j)%rpos(3) * wght

    enddo

    ml%rcom(1) = xsum / msum
    ml%rcom(2) = ysum / msum
    ml%rcom(3) = zsum / msum

end subroutine


!> fully recalculates mass and centre of mass (COM) of a molecule (atom positions remain intact!) \n
!> BUT beware of atoms more than half any box dimension away from the first one (while 'undoing PBC')
subroutine full_mol_com(mol, invlatvec, latvector)

    use kinds_f90
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol

    real(kind = wp), intent(in) :: invlatvec(3,3)

    real(kind = wp), intent(in) :: latvector(3,3)

    real(kind = wp) :: orig(3), rx, ry, rz, xx, yy, zz

    real(kind = wp) :: mass, wght, xsum, ysum, zsum

    integer :: i

    !AB: do Cartesian-fractional-Cartesian transformations and PBC unwrapping within one loop

    rx = mol%atms(1)%rpos(1)
    ry = mol%atms(1)%rpos(2)
    rz = mol%atms(1)%rpos(3)

    !AB: the first atom sets the origin in the fractional space

    !AB: fractional (transformed) coordinates
    orig(1) = invlatvec(1,1) * rx + invlatvec(2,1) * ry + invlatvec(3,1) * rz
    orig(2) = invlatvec(1,2) * rx + invlatvec(2,2) * ry + invlatvec(3,2) * rz
    orig(3) = invlatvec(1,3) * rx + invlatvec(2,3) * ry + invlatvec(3,3) * rz

    mass = mol%atms(1)%mass
    xsum = rx*mass
    ysum = ry*mass
    zsum = rz*mass

    do i = 2, mol%natom

        rx = mol%atms(i)%rpos(1)
        ry = mol%atms(i)%rpos(2)
        rz = mol%atms(i)%rpos(3)

        !AB: fractional (transformed) coordinates
        xx = invlatvec(1,1) * rx + invlatvec(2,1) * ry + invlatvec(3,1) * rz
        yy = invlatvec(1,2) * rx + invlatvec(2,2) * ry + invlatvec(3,2) * rz
        zz = invlatvec(1,3) * rx + invlatvec(2,3) * ry + invlatvec(3,3) * rz

        !AB: unwrapped (reformed) coordinates in the fractional space
        rx = xx - anint(xx-orig(1))
        ry = yy - anint(yy-orig(2))
        rz = zz - anint(zz-orig(3))

        !AB: Cartesian (transformed back) coordinates
        xx = latvector(1,1) * rx + latvector(2,1) * ry + latvector(3,1) * rz
        yy = latvector(1,2) * rx + latvector(2,2) * ry + latvector(3,2) * rz
        zz = latvector(1,3) * rx + latvector(2,3) * ry + latvector(3,3) * rz

        wght = mol%atms(i)%mass
        mass = mass + wght

        xsum = xsum + xx * wght
        ysum = ysum + yy * wght
        zsum = zsum + zz * wght

    enddo

    mol%rcom(1) = xsum / mass
    mol%rcom(2) = ysum / mass
    mol%rcom(3) = zsum / mass

    mol%mass = mass

end subroutine

!> calculates the centre of mass of a molecule using backed-up atom positions \n
!> NOTE: need to 'reform' the molecule, i.e. 'undo PBC', before doing this
subroutine mol_com_stored(ml)

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: ml

    integer :: j
    real(kind = wp) :: wght, xsum, ysum, zsum, msum

    xsum = 0.0_wp
    ysum = 0.0_wp
    zsum = 0.0_wp
    msum = 0.0_wp

    do j = 1, ml%natom

        wght = ml%atms(j)%mass
        msum = msum + wght

        xsum = xsum + ml%atms(j)%store_rpos(1) * wght
        ysum = ysum + ml%atms(j)%store_rpos(2) * wght
        zsum = zsum + ml%atms(j)%store_rpos(3) * wght

    enddo

    ml%store_rcom(1) = xsum / msum
    ml%store_rcom(2) = ysum / msum
    ml%store_rcom(3) = zsum / msum

end subroutine

!TU*: Not yet compatible with cell lists
subroutine swap_mol_positions(m1, m2)

    use molecule_type
    use kinds_f90

    implicit none

    type(molecule), intent(inout) :: m1, m2

    integer :: i
    real (kind = wp) :: cm(3)

    !remove the com of molecules
    do i = 1, m1%natom

        m1%atms(i)%rpos(:) = m1%atms(i)%rpos(:) - m1%rcom(:)

    enddo

    do i = 1, m2%natom

!JG: second molcule com reoval incorrect, added instead of subtracted,
!correcting
!        m2%atms(i)%rpos(:) = m2%atms(i)%rpos(:) + m2%rcom(:)
        m2%atms(i)%rpos(:) = m2%atms(i)%rpos(:) - m2%rcom(:)

    enddo

    !swap com of first molecule with that of second
    cm(:) = m1%rcom(:)
    m1%rcom(:) = m2%rcom(:)
    m2%rcom(:) = cm(:)

    !move to new com
    do i = 1, m1%natom

        m1%atms(i)%rpos(:) = m1%atms(i)%rpos(:) + m1%rcom(:)

    enddo

    do i = 1, m2%natom

        m2%atms(i)%rpos(:) = m2%atms(i)%rpos(:) + m2%rcom(:)

    enddo


end subroutine


!TU: This procedure sets the size of the phase arrays to reflect the number of g-vectors in m2's phase
!TU: arrays - if m1%mxatom/=m2%mxatom.
subroutine copy_molecule(m1, m2)

    use molecule_type
    use kinds_f90
    use atom_module, only : copy_atom
    use bondlist_module, only : copy_bondlist, alloc_and_copy_bondlist_arrays
    use anglist_module, only : copy_anglist, alloc_and_copy_anglist_arrays
    use invlist_module, only : copy_invlist, alloc_and_copy_invlist_arrays
    use dihlist_module, only : copy_dihlist, alloc_and_copy_dihlist_arrays
    use nbrlist_module, only : copy_nbrlist, alloc_nbrlist_arrays, alloc_and_copy_nbrlist_arrays, &
         copy_exclist, dealloc_nbrlist_arrays, alloc_exclist_array, alloc_and_copy_exclist_array, &
         dealloc_exclist_array

    implicit none

    type(molecule), intent(inout) :: m1
    type(molecule), intent(in) :: m2

    integer :: i, fail(2), gvecsize

    integer :: coultype
    logical :: lauto_nbrs, ldisp

    lauto_nbrs = .false.
    fail = 0

    if (m1%natom == 0) then ! the arrays in this molecule have not been activated

        call alloc_mol_atms_arrays(m1, m2%natom, m2%mxatom, lauto_nbrs, ldisp)

        do i = 1, m2%natom

            call copy_atom(m1%atms(i), m2%atms(i))

        enddo

        call alloc_and_copy_bondlist_arrays(m1%blist, m2%blist)

        call alloc_and_copy_anglist_arrays(m1%alist, m2%alist)

        call alloc_and_copy_invlist_arrays(m1%ilist, m2%ilist)

        call alloc_and_copy_dihlist_arrays(m1%dlist, m2%dlist)

        call alloc_and_copy_nbrlist_arrays(m1%nlist, m2%nlist, m2%natom)

        if( m2%blist%npairs > 0 ) &
            call alloc_and_copy_exclist_array(m1%nlist, m2%nlist, m2%natom)

        if( m2%has_smden ) call alloc_mol_smden_arrays(m1)

    elseif (m1%mxatom == m2%mxatom) then !then assume arrays activated and the same size

        call copy_nbrlist(m1%nlist, m2%nlist)

        if (m2%blist%npairs > 0) call copy_exclist(m1%nlist, m2%nlist)

        call copy_bondlist(m1%blist, m2%blist)

        call copy_anglist(m1%alist, m2%alist)

        call copy_invlist(m1%ilist, m2%ilist)

        call copy_dihlist(m1%dlist, m2%dlist)

        do i = 1, m2%natom

            call copy_atom(m1%atms(i), m2%atms(i))

        enddo

    elseif (m1%mxatom /= m2%mxatom) then !then assume arrays activated and of different size

        call dealloc_mol_atms_arrays(m1)
        call alloc_mol_atms_arrays(m1, m2%natom, m2%mxatom, lauto_nbrs, ldisp)

        call dealloc_nbrlist_arrays(m1%nlist)
        if (m1%blist%npairs > 0) call dealloc_exclist_array(m1%nlist)

        call alloc_and_copy_nbrlist_arrays(m1%nlist, m2%nlist, m2%natom)
        if (m2%blist%npairs > 0) call alloc_and_copy_exclist_array(m1%nlist, m2%nlist, m2%natom)

!       call dealloc_bondlist_arrays(m1%blist)
        call alloc_and_copy_bondlist_arrays(m1%blist, m2%blist)

!       call dealloc_anglist_arrays(m1%alist)
        call alloc_and_copy_anglist_arrays(m1%alist, m2%alist)

        call alloc_and_copy_invlist_arrays(m1%ilist, m2%ilist)

        call alloc_and_copy_dihlist_arrays(m1%dlist, m2%dlist)

        !TU**: What if there are different numbers of atoms in the two molecules?
        do i = 1, m2%natom
        
            call copy_atom(m1%atms(i), m2%atms(i))
            
        enddo

    else

        call error(357)

    endif

    m1%molname   = m2%molname
    m1%mol_label = m2%mol_label
    m1%mass      = m2%mass
    m1%charge    = m2%charge

    m1%has_charge = m2%has_charge
    m1%has_smden  = m2%has_smden

    m1%rigid_body = m2%rigid_body
    m1%exc_coul_ints = m2%exc_coul_ints

    m1%idcom   = m2%idcom
    m1%rcom(:) = m2%rcom(:)
    m1%store_rcom(:) = m2%store_rcom(:)

    m1%orientation(:,:) = m2%orientation(:,:)
    
    !m1%store_xcom = m2%store_xcom
    !m1%store_ycom = m2%store_ycom
    !m1%store_zcom = m2%store_zcom

    !m1% = m2%

end subroutine


subroutine add_atom(mol, typ, pos, chg, amass, eletyp, asite, lauto, i)

    use kinds_f90
    use constants_module, only : uout
    use atom_module, only : set_atom_type, set_atom_site, set_atom_pos
    use molecule_type

    implicit none

    integer, intent(in)  :: typ, eletyp, asite
    integer, intent(out) :: i

    real (kind = wp), intent(in) :: pos(3), chg, amass

    type(molecule), intent(inout) :: mol

    character*(21) :: nums

    logical, intent(in) :: lauto

    real(kind = wp) :: duni

    mol%natom = mol%natom + 1
    i = mol%natom

    !check arrays are not going to be exceeded
    if (mol%natom > mol%mxatom) then

        write(nums,"(i8,a,i8,a)") mol%natom, ' > ',mol%mxatom,' ?'

        call cry(uout,'', &
                 "ERROR: exceeding max number of atoms in molecule: "//nums//" (while adding an atom)!!!",999)

        !call error(202)

    endif

    call set_atom_type(mol%atms(i), typ, chg, amass, eletyp)
    call set_atom_pos(mol%atms(i), pos)
    call set_atom_site(mol%atms(i), asite)

    !AB: add the atom charge and mass to molecule charge and mass
    call add_molecule_mass(mol, amass)
    call add_molecule_charge(mol, chg)

    if(lauto) mol%atms(i)%r_zero(:) = mol%atms(i)%rpos(:)

end subroutine


subroutine molecule_quaternion_rotation(mol, rot)

    use molecule_type
    use constants_module, only : ATM_TYPE_SPIN

    implicit none

    real (kind = wp), intent(in) :: rot(9)

    type(molecule), intent(inout) :: mol

    integer :: i
    real (kind = wp) :: xx, yy, zz

    do i = 1, mol%natom

        xx = mol%atms(i)%rpos(1)
        yy = mol%atms(i)%rpos(2)
        zz = mol%atms(i)%rpos(3)

        mol%atms(i)%rpos(1) = rot(1) * xx + rot(4) * yy + rot(7) * zz
        mol%atms(i)%rpos(2) = rot(2) * xx + rot(5) * yy + rot(8) * zz
        mol%atms(i)%rpos(3) = rot(3) * xx + rot(6) * yy + rot(9) * zz

        !TU: Rotate the orientation of the atom if it is of type 'spin
        if( mol%atms(i)%atype == ATM_TYPE_SPIN ) then

            xx = mol%atms(i)%spin(1)
            yy = mol%atms(i)%spin(2)
            zz = mol%atms(i)%spin(3)

            mol%atms(i)%spin(1) = rot(1) * xx + rot(4) * yy + rot(7) * zz
            mol%atms(i)%spin(2) = rot(2) * xx + rot(5) * yy + rot(8) * zz
            mol%atms(i)%spin(3) = rot(3) * xx + rot(6) * yy + rot(9) * zz     

        end if

    enddo

end subroutine

subroutine shift_molecule(mol, shft)

    use molecule_type
    !use slit_module, only : in_bulk

    implicit none

    real (kind = wp), intent(in) :: shft(3)

    type(molecule), intent(inout) :: mol

    integer :: j

    !move com
    mol%rcom(:) = mol%rcom(:) + shft(:)

    ! move atoms
    do j = 1, mol%natom

        !move molecule
        mol%atms(j)%rpos(:) = mol%atms(j)%rpos(:) + shft(:)

    enddo

end subroutine

subroutine position_molecule(mol, pos)

    use molecule_type

    implicit none

    real (kind = wp), intent(in) :: pos(3)

    type(molecule), intent(inout) :: mol

    integer :: j

    do j = 1, mol%natom

        !remove center of mass
        mol%atms(j)%rpos(:) = mol%atms(j)%rpos(:) - mol%rcom(:)
        mol%atms(j)%rpos(:) = mol%atms(j)%rpos(:) + pos(:)

    enddo

    mol%rcom(:) = pos(:)

end subroutine

subroutine zero_molecule(mol)

    use molecule_type
    use atom_module, only : zero_atom

    implicit none

    type(molecule), intent(inout) :: mol

    integer :: j

    do j = 1, mol%natom

        call zero_atom(mol%atms(j))

    enddo

    mol%natom = 0
    mol%glob_no = 0
    mol%mol_label = 0
    mol%idcom = 0

end subroutine

!**************************************************
! converts fractional to cartesian coordinates of a single molecule
! *************************************************/
subroutine fractocart_mol(mol, latvec)

    use kinds_f90
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    real(kind = wp), intent(in) :: latvec(3,3)

    real(kind = wp) :: rx, ry, rz

    integer :: i

    do i = 1, mol%natom

        rx = mol%atms(i)%rpos(1)
        ry = mol%atms(i)%rpos(2)
        rz = mol%atms(i)%rpos(3)

        mol%atms(i)%rpos(1) = latvec(1,1) * rx + &
                              latvec(2,1) * ry + &
                              latvec(3,1) * rz

        mol%atms(i)%rpos(2) = latvec(1,2) * rx + &
                              latvec(2,2) * ry + &
                              latvec(3,2) * rz

        mol%atms(i)%rpos(3) = latvec(1,3) * rx + &
                              latvec(2,3) * ry + &
                              latvec(3,3) * rz

    enddo

end subroutine

!**************************************************
! converts fractional to cartesian coordinates of a single molecule
! *************************************************/
subroutine carttofrac_mol(mol, invec)

    use kinds_f90
    use molecule_type

    implicit none

    type(molecule), intent(inout) :: mol
    real(kind = wp), intent(in) :: invec(3,3)

    real(kind = wp) :: rx, ry, rz

    integer :: i

    do i = 1, mol%natom

        rx = mol%atms(i)%rpos(1)
        ry = mol%atms(i)%rpos(2)
        rz = mol%atms(i)%rpos(3)

        mol%atms(i)%rpos(1) = invec(1,1) * rx + &
                              invec(2,1) * ry + &
                              invec(3,1) * rz

        mol%atms(i)%rpos(2) = invec(1,2) * rx + &
                              invec(2,2) * ry + &
                              invec(3,2) * rz

        mol%atms(i)%rpos(3) = invec(1,3) * rx + &
                              invec(2,3) * ry + &
                              invec(3,3) * rz

    enddo

end subroutine



!TU: Added by me...
!> Sets the charges of atoms in molecule `m1` to a prescribed scale factor `frac`
!> of the charges of the atoms in `m2`. The charge of atom 'i' in `m1` is set to 
!> 'frac*q', where 'q' is the charge of atom 'i' in `m2`. Note that the number 
!> of atoms in `m1` and `m2` should match, otherwise there may be unexpected 
!> behaviour or an array out-of-bounds error: there is no safety check that the
!> number of atoms match here!
subroutine scale_mol_charges_from_template(m1, m2, frac)

    use kinds_f90
    use molecule_type

    implicit none

        !> Molecule which will have its atomic charges changed
    type(molecule), intent(inout) :: m1

        !> Template molecule: the charges of atoms in this molecule will be used to
        !> set the charges in `m1`
    type(molecule), intent(in) :: m2

        !> Scale factor to apply to charges of atoms in `m2` when assigning them to
        !> atoms in `m1`
    real (kind = wp), intent(in) :: frac

    integer :: i

    do i = 1, m1%natom

        m1%atms(i)%charge = frac * m2%atms(i)%charge

        ! Uses the same criteria for determining 'is_charged' as 'set_atom_type' in 'atom_module'
        m1%atms(i)%is_charged = ( abs(m1%atms(i)%charge) > 1.0e-8_wp ) 

    end do

    call set_molecule_charge(m1)

end subroutine scale_mol_charges_from_template



!TU**: Added by me...
!> Sets the charges and species of atoms in molecule `m1` to reflect those of
!> analogous atoms in `m2`. Moreover the species, mass and charge of the molecule `m1` will be set to
!> that of `m2`. Note that the number of atoms in `m1` and `m2` should match, 
!> otherwise there may be unexpected behaviour or an array out-of-bounds error: 
!> there is no safety check that the number of atoms match here! Moreover it is
!> assumed that the molecules have the same 'structure', e.g the same bonds, angles,
!> rigidity. These aspects of `m1` are not altered by this procedure.
subroutine set_mol_species_from_template(m1, m2)

    use kinds_f90
    use molecule_type
    use atom_module, only : set_atom_type

    implicit none

        !> Molecule which will have its species changed
    type(molecule), intent(inout) :: m1

        !> Template molecule; the charges and species of atoms in this molecule will be applied
        !> to atoms in `m1`
    type(molecule), intent(in) :: m2

    integer :: i

    m1%molname   = m2%molname
    m1%mol_label = m2%mol_label
    m1%mass      = m2%mass
    m1%charge    = m2%charge

    m1%has_charge = m2%has_charge

    do i = 1, m1%natom

        call set_atom_type(m1%atms(i), m2%atms(i)%atlabel, m2%atms(i)%charge, m2%atms(i)%mass, m2%atms(i)%atype)

    end do

end subroutine set_mol_species_from_template




!TU**:
!> Sets the 'store' variables for charges in the molecule to reflect their current value
subroutine store_mol_charges(m1)

    use kinds_f90
    use molecule_type

    implicit none

        !> Molecule which will have its atomic charges changed
    type(molecule), intent(inout) :: m1

    integer :: i

    do i = 1, m1%natom

        m1%atms(i)%store_charge = m1%atms(i)%charge

    end do

end subroutine store_mol_charges


!TU**: Need to update lower level modules to reflect addition of store_charge
!> 
subroutine restore_mol_charges(m1)

    use kinds_f90
    use molecule_type

    implicit none

        !> Molecule which will have its atomic charges changed
    type(molecule), intent(inout) :: m1

    integer :: i

    !TU**: Can do as array operation?
    do i = 1, m1%natom

        m1%atms(i)%charge = m1%atms(i)%store_charge

    end do

end subroutine restore_mol_charges


end module
