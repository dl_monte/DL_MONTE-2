! *******************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                                   *
! *   john.purton[@]stfc.ac.uk                                                  *
! *                                                                             *
! *   Contributors:                                                             *
! *   -------------                                                             *
! *   A.V.Brukhno (C) 2015-2016                                                 *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *   - overall optimizations (general MC scheme, energy calculus and MC steps) *
! *   - Free Energy Difference (FED) & order parameters (fed_*_module.f90)      *
! *   - Replica Exchange (RE) algorithm (rep_exchange_module.f90)               *
! *   - VdW and general potential forms (vdw_*_module.f90 etc)                  *
! *   - planar pore constraint (slit_module.f90)                                *
! *   - USER defined analytical forms for force-fields (vdw_*.f90 & user_*.inc) *
! *                                                                             *
! *   T.L.Underwood (C) 2015-2016                                               *
! *   t.l.Underwood[@]bath.ac.uk                                                *
! *   - Lattice/Phase-Switch MC methodology                                     *
! *   - algorithm optimizations (gcmc_*_module.90, MC steps, random generator)  *
! *                                                                             *
! *******************************************************************************

! ==============================================
! General potential forms as in-lined functions
!       - to be included where necessary -
!              (singled out by AB)
! ==============================================

    real(Kind = wp) :: r,ri,ru,rui,r0,ri2,ri3,ri4,ri6,ri12,ri12a,ri10b,  &
                       a,b,c,d,e,f,g,b1,c1,abc,am,an,alpha,beta,gama,gamai,eps, &
                       sri,sri2,sri3,sri6,sri12,br,brc,brc6,aric,brid,rac,  &
                       drb,drc,aexprb,aexpbcr,rabexpbcr,expanam,dexpanam,expcrb

    real(Kind = wp) :: vv1,vv2,vv3,vv4,vv5,vv6,vv7,vv8,vv9,vv10, &
                       vv11,vv12,vv13,vv14,vv15,vu0,vu1,vu2,vu3,vu4,vu5, &
                       gg1,gg2,gg3,gg4,gg5,gg6,gg7,gg8,gg9,gg10, &
                       gg11,gg12,gg13,gg14,gg15,gu0,gu1,gu2,gu3,gu4,gu5

    real(Kind = wp) :: vlj,vljlrc,v126,v126lrc,dc126,v93,v104,vwca,vhbd,vew,vbuk, &
                       vmrs,vmrl,vpnm,vnm,vsnm,vbhm,vao,vyuk,vsw2, &
                       glj,g126,r126,g93,g104,gwca,ghbd,gew,gbuk, &
                       gmrs,gmrl,gpnm,gnm,gsnm,gbhm,gao,gyuk 

    !AB: common convention for parameterisation:

    ! a = prmvdw(1,ivdw) - ref energy   (epsilon, e_0)
    ! b = prmvdw(2,ivdw) - ref distance (sigma, r_0)
    ! c = prmvdw(3,ivdw) - custom       (often distance shift, r_surf)
    ! d = prmvdw(4,ivdw) - custom
    ! e = prmvdw(5,ivdw) - custom

    !AB: extra (optional parameters) after the max number of compulsory ones

    ! prmvdw(max+1,ivdw)    :=: r_surf (distance shift to the surface interaction)
    ! long-range correction :=: 1 / 0  (dispersion correction on/off, if possible)

    !AB: shifting of the potential origin to the effective "surface" is done outside:
    !AB: r :=: r_true - r_surf

! <<--

    ! Stepwise non-analytical potentials
    ! key < 0 ; keywords: 'hs'/'sw'/'hsqw'

    ! Lennard-Jones (LJ) potential
    ! key = 1 [<-2] ; keywords: 'lj'/'slj'/'surflj'

    vv1(r,a,b) = 4.0_wp*a*(b/r)**6 * ( (b/r)**6-1.0_wp )
    gg1(r,a,b) = 24.0_wp*a*(b/r)**6 * ( 2.0_wp*(b/r)**6-1.0_wp )

    vlj(sri6,a) = 4.0_wp*a*sri6 * (sri6-1.0_wp)
    glj(sri6,a) = 24.0_wp*a*sri6 * (2.0_wp*sri6-1.0_wp)

    ! short-range hard-core (minimum overlap) separation at the repuslion cap (AB)
    ! DC_VDW 12-6/LJ are set via this function (AB)

    dc126(a,b,c) = ( (-b+sqrt(b*b+4.0_wp*a*c))/(2.0_wp*c) )**(1.0_wp/6.0_wp)

    !TU: Coefficient for the long-range correction to the LJ potential: this is
    !TU: multiplied by Ni*Nj/V to get the total long-range correction energy, where
    !TU: Ni and Nj are the number of atoms in the system for the two interacting
    !TU: species in the potential, and V is the volume of the sytem. The function
    !TU: here takes rc (a) and sigma (b) as the argument, and the output is the 
    !TU: coefficient in units of epsilon
    vljlrc(a,b) =  PI * b**3 * ( (8.0_wp/9.0_wp)/((a/b)**9) - (8.0_wp/3.0_wp)/((a/b)**3) )

    ! 12-6 potential
    ! key = 2 [<-1] ; keywords: '12-6'/'s12-6'/'surf12-6'

    vv2(r,a,b) = (a/r**6-b)/r**6      
    gg2(r,a,b) = 6.0_wp*(2.0_wp*a/r**6-b)/r**6

    v126(ri6,a,b) = (a*ri6-b)*ri6
    g126(ri6,a,b) = 6.0_wp*(2.0_wp*a*ri6-b)*ri6

    !TU: Coefficient for the long-range correction to the 12-6 potential: this is
    !TU: multiplied by Ni*Nj/V to get the total long-range correction energy, where
    !TU: Ni and Nj are the number of atoms in the system for the two interacting
    !TU: species in the potential, and V is the volume of the sytem. The function
    !TU: here takes rc (r), A (a) and B (b) as the arguments
    v126lrc(r,a,b) =  (2.0_wp*PI*a)/(9.0_wp*r**9) - (2.0_wp*PI*b)/(3.0_wp*r**3)


    ! 9-3 potential
    ! key = 3 [new] ; keywords: '9-3'/'s9-3'/'surf9-3'

    vv3(r,a,b) = (a/r**6-b)/r**3
    gg3(r,a,b) = 3.0_wp*(3.0_wp*a/r**6-b)/r**3

    v93(ri3,a,b) = (a*ri3*ri3-b)*ri3
    g93(ri3,a,b) = 3.0_wp*(3.0_wp*a*ri3*ri3-b)*ri3

    ! 10-4 potential
    ! key = 4 [new] ; keywords: '10-4'/'s10-4'/'surf10-4'

    vv4(r,a,b) = (a/r**6-b)/r**4
    gg4(r,a,b) = 4.0_wp*(2.5_wp*a/r**6-b)/r**4

    v104(ri6,ri4,a,b) = (a*ri6-b)*ri4
    g104(ri6,ri4,a,b) = 4.0_wp*(2.5_wp*a*ri6-b)*ri4

    ! Weeks-chandler-Andersen (WCA), based around "shifted & truncated" LJ, potential by I.T.Todorov (as in DL_POLY)
    ! key = 5 [<-9] ; keywords: 'wca'/'wcalj'

    vv5(r,a,b,c) =  4.0_wp*a* (b/(r-c))**6 * ( (b/(r-c))**6 - 1.0_wp ) + a
    gg5(r,a,b,c) = 24.0_wp*a* (b/(r-c))**6 * ( 2.0_wp*(b/(r-c))**6 - 1.0_wp )

    vwca(a,brc6) =  4.0_wp*a* brc6 * (brc6 - 1.0_wp) + a
    gwca(a,brc6) = 24.0_wp*a* brc6 * (2.0_wp*brc6 - 1.0_wp)

    ! Hydrogen-bond 12-10 potential
    ! key = 6 [<-6] ; keywords: 'hbnd'

    vv6(r,a,b) = a/r**12 - b/r**10
    gg6(r,a,b) = 12.0_wp*a/r**12 - 10.0_wp*b/r**10

    vhbd(ri12a,ri10b) = ri12a - ri10b
    ghbd(ri12a,ri10b) = 12.0_wp*ri12a - 10.0_wp*ri10b

    ! Espanol-Warren (EW) potential
    ! key = 7 [<-10] ; keywords: 'ew'

    vv7(r, a, b) = 0.5 * a * (b- r)**2
    gg7(r, a, b) = a * (b - r)

    vew(a,br) = 0.5_wp*a*br*br
    gew(a,br) = a*br

    ! Buckingham exp-6  potential
    ! key = 8 [<-4] ; keywords: 'buck'

    vv8(r,a,b,c) = a*exp(-r/b) - c/r**6
    gg8(r,a,b,c) = r*a*exp(-r/b)/b - 6.0_wp*c/r**6

    vbuk(aexprb,sri6) = aexprb - sri6
    gbuk(aexprb,sri6) = aexprb - 6.0_wp*sri6
!    gbuk(raexprb,sri6) = raexprb - 6.0_wp*sri6

    ! Morse plus D*r^{-12} potential (JP/JG)
    ! key = 9 [<-8] ; keywords: 'mors'/'morl'

    vv9(r,a,b,c) = a*( (1.0_wp - exp(-c*(r-b)))**2 - 1.0_wp )
    gg9(r,a,b,c) = -2.0_wp*r*a*c*( 1.0_wp-exp(-c*(r-b)) ) * exp(-c*(r-b))

    vmrs(r,a,b,c)   = a*( (1.0_wp - exp(-c * (r - b)))**2 - 1.0_wp ) 
    vmrl(r,a,b,c,d) = a*( (1.0_wp - exp(-c * (r - b)))**2 - 1.0_wp ) + d/r**12

    gmrs(rac,expcrb) = -2.0_wp*rac*( 1.0_wp - expcrb ) * expcrb 
    gmrl(rac,expcrb) = -2.0_wp*rac*( 1.0_wp - expcrb ) * expcrb + 12.0_wp*d/r**12

    ! Simple Powers potential (n > m) potential (JG/AB)
    ! key = 10 [<-11] ; keywords: 'pnm'/'pwnm'/'ljnm'

    vv10(r,a,b,c,d) = a/r**c - b/r**d
    gg10(r,a,b,c,d) = c*a/r**c - d*b/r**d

    vpnm(aric,brid) = aric - brid
    gpnm(aric,brid,c,d) = c*aric - d*brid

    ! Two Powers (n > m) potential
    ! key = 11 [<-3] ; keywords: 'nm'

    vv11(r,a,b,c,d) = a/(b-c) * ( c*(d/r)**b - b*(d/r)**c )
    gg11(r,a,b,c,d) = a*c*b/(b-c) * ( (d/r)**b - (d/r)**c )

    vnm(drb,drc,abc,c,b) = abc*(c*drb-b*drc)
    gnm(drb,drc,abc,c,b) = abc*c*b*(drb-drc)

    ! Shifted & Force Corrected Two-Powers (n > m) potential by W.Smith
    ! key = 12 [<-7] ; keywords: 'snm'

    vv12(r,a,b,c,d,b1,c1) = a/(b-c) *   &
                         ( c*(b1**b)*( (d/r)**b - (1.0_wp/c1)**b ) -  &
                           b*(b1**c)*( (d/r)**c - (1.0_wp/c1)**c ) +  &
                           b*c*( (r/(c1*d) - 1.0_wp) * ((b1/c1)**b - (b1/c1)**c) )  &
                         )

    gg12(r,a,b,c,d,b1,c1) = a*c*b/(b-c) *  &
                         ( (b1**b)*(d/r)**b - (b1**c)*(d/r)**c -  &
                           r/(c1*d) * ((b1/c1)**b - (b1/c1)**c)   &
                         )

    vsnm(expanam,an,am,beta,gamai,sri) = expanam *     &
            ( am*(beta**an) * (sri**an - gamai**an) -  &
              an*(beta**am) * (sri**am - gamai**am) +  &
              an*am*( (gamai/sri-1.0_wp )*( (beta*gamai)**an-(beta*gamai)**am) ) ) ! &

    gsnm(expanam,an,am,beta,gamai,sri) = expanam * &
            ( (beta*sri)**an-(beta*sri)**am ) -    &
               gamai/sri * ( (beta*gamai)**an - (beta*gamai)**am )

!    gsnm(dexpanam,an,am,beta,gamai,sri) = dexpanam *  &
!            ( (beta*sri)**an - (beta*sri)**am ) -   &
!              gamai/sri*( (beta*gamai)**an - (beta*gamai)**am )

    ! Born-Huggins-Meyer exp-6-8  potential
    ! key = 13 [<-5] ; keywords: 'bhm'

    vv13(r,a,b,c,d,e) = a*exp(b*(c-r)) - d/r**6-e/r**8
    gg13(r,a,b,c,d,e) = r*a*b*exp(b*(c-r)) - 6.0_wp*d/r**6 - 8.0_wp*e/r**8

    vbhm(aexpbcr,ri2,d,e)   = aexpbcr - ri2*ri2*ri2*d - ri2*ri2*ri2*ri2*e
    gbhm(rabexpbcr,ri2,d,e) = rabexpbcr - 6.0_wp*ri2*ri2*ri2*d - 8.0_wp*ri2*ri2*ri2*ri2*e

    ! A-O potential beyond hard-sphere core (JG/AB)
    ! key = 14 [<-12] ; keywords: 'a-o'

    vv14(r,a,b,c) = a*( 1.0_wp + b*r + c*r**3 )
    gg14(r,a,b,c) = a*( b*r + 3.0_wp*c*r**3 )

    vao(a,b,c,r) = a*( 1.0_wp + b*r*(1.0_wp + c*r*r) )
    gao(a,b,c,r) = a*r*( b + 3.0_wp*c*r*r )

    ! Yukawa potential beyond hard-sphere core (JG/AB)
    ! key = 15 [<-13] ; keywords: 'yuka'/'yukw'/'yukawa'

    vv15(r,a,b,c) = a*c*exp(-b*(r-a))/r
    gg15(r,a,b,c) = a*b*c*exp(-b*(r-a))/r ! - check

    vyuk(a,b,c,r) = a*c*exp(b*(a-r))/r
    gyuk(a,b,c,r) = a*b*c*exp(b*(a-r))/r ! - check

	! Pair component of Stillinger-Weber potential (TU)
    ! key = 16 ; keywords: 'stillweb'

    ! Significance of arguments to function:
    ! a <- epsilon
    ! b <- sigma
    ! c <- a
    ! d <- A
    ! e <- B
    ! f <- p
    ! g <- q
    vsw2(a,b,c,d,e,f,g,r) = d*a*(e*(b/r)**f-(b/r)**g)*exp(b/(r-c*b))
    

! any extra force fields - ???

! ...

include "user_potentials.inc"

! -->>

