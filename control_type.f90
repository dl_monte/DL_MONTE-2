! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

!> @brief
!> - Data type defining a simulation `job` via control switches and parameters
!> @usage 
!> - call as `<control_type>%<attribute>` where normally `<control_type> = 'job'`
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `molecule_type`

!> @typefor simulation `job`: control switches & parameters

module control_type

    use kinds_f90
    use molecule_type

    implicit none

type control

        !> flag to determine if the cell type is slit (planar pore) default: .false.
    logical ::    is_slit = .false.
    logical ::    is_XY_npt = .false.

        !> flag to determine if the cell type is tube (cylindrical nanotube) default: .false.
!    logical ::    is_tube = .false.

        !> include intra mol bonds in non-bond list default: .false.
    logical ::    include_mol = .false.

        !> flag to include calculation of forces on atoms
    logical ::    calcforce = .false.

        !> flag to include calculation of stress
    logical ::    calcstress = .false.

        !> flag to include calculation of velocity
    logical ::    calcvel = .false.

        !> flag for movement of atoms default .false.
    logical ::    moveatm = .false.

        !> flag for movement of molecules default .false.
    logical ::    movemol = .false.

        !> flag for rotation of molecules default is .false.
    logical ::    rotatemol = .false.

        !> flag for swap of atoms default .false.
    logical ::    swapatoms = .false.

        !> flag for swap of mols default .false.
    logical ::    swapmols = .false.

        !> flag for any gcmc 
    logical ::    is_gcmc = .false.

        !> flag for gcmc insertion of atom
    logical ::    gcmcatom = .false.

        !> flag for gcmc insert a molecule
    logical ::    gcmcmol = .false.

        !> flag for indepndent volume moves in gibbs ensemble
    logical ::    gibbs_indvol = .false.

        !> flag for gibbs enesmble for atom transfers
    logical ::    gibbsatomtran = .false.

        !> flag for gibbs ensemble for molecules transfers
    logical ::    gibbsmoltran = .false.

        !> flag for gibbs enesmble for atom transfers
    logical ::    gibbsatomexch = .false.

        !> flag for gibbs ensemble for molecules transfers
    logical ::    gibbsmolexch = .false.

        !> flag for whether Gibbs ensemble is in play
    logical ::    is_gibbs_ensemble = .false.

        !> indicates alpha for ewald sum is provided in input
    logical ::    ewald_alpha_given = .false.

        !> flag for thermodynamic integration default: .false.
    logical ::    thintegration = .false.

        !> flag to indicate semi grand ensemble default: .false.
    logical ::    semigrandatms = .false.

        !> flag to engage semi-Widom moves for atoms default: .false.
    logical ::    semiwidomatms = .false.

        !> flag to indicate transmutation ensemble default: .false.
    logical ::    transmutate = .false.

        !> cant remember what this does
    logical ::    substitute = .false.

        !> free energy perturbation default: .false.
    logical ::    perturbation = .false.

        !> use exchange bias when swapping atoms or molecules default .false.
    logical ::    exchangebias = .false.

        !> flag to indicate increase in size of lattice vectors default .false.
    logical ::    scalevec = .false.

        !> flag to increase number of cells in a box default .false.
    logical ::    scalesize = .false.

        !> use the gas pressure in grand monte carlo simulations
    logical ::    usegaspres = .false.

        !> use chem. pot in kT units in grand monte carlo simulations (AB)
    logical ::    usechempotkt = .false.

        !> use chem. pot in kJ/mol units in grand monte carlo simulations (AB)
    logical ::    usechempotkj = .false.

        !> use cavity bias for gcmc
    logical ::    usecavitybias = .false.
    logical ::    cavity_grid   = .false.
    logical ::    cavity_mesh   = .false.

        !> Exclude a slab region from consideration for GCMC insertions. Default = .false.
    logical :: usegcexcludeslab = .false.

        !> Dimension for slab region to exclude for GCMC insertions (1,2 or 3). The slab
        !> is perpendicular to the cell vector with this index. Default = 3
    integer :: gcexcludeslab_dim = 3

        !> Lower bound for slab region to exclude for GCMC insertions (fractional). 
        !> Default = 0.0
    real(wp) :: gcexcludeslab_lbound = 0.0_wp

        !> Upper bound for slab region to exclude for GCMC insertions (fractional). 
        !> Default = 0.0
    real(wp) :: gcexcludeslab_ubound = 0.0_wp


        !> use orientbias rotation
    logical ::    useobias_rot = .false.

        !> use orientbias insert/delete for gcmc
    logical ::    useobias_gcmc = .false.

        !> use orthogonalilty if possible
    logical ::    useorthogonal = .false.

        !> use sequential move for atoms
    logical ::    useseqmove = .false.

        !> use sequential random move for atoms
    logical ::    useseqmovernd = .false.

        !> use sequential move of molecules
    logical ::    useseqmolmove = .false.

        !> use sequential random move of molecules
    logical ::    useseqmolmovernd = .false.

        !> use sequential rotation of molecules
    logical ::    useseqmolrot = .false.

        !> use sequential random rotation of molecules
    logical ::    useseqmolrotrnd = .false.

        !> use quaternions to rotate molecules rather than Euler angles
    logical ::    usequaternion = .false.

        !> use faster ewald method for molecule rotation (depretiated)
    logical ::    useapproxewald = .false.

        !> use to calculate and print molecular energy contributions (slow!)
    logical ::    lmoldata = .false.
    
        !> output the rolling stats for various moves
    logical ::    lmovstats = .false.

        !> restart flag default: .false.
    logical ::    sysrestart = .false.

        !> glag for automatic recalculation of the nbr list
    logical ::    lauto_nbrs = .false.

        !> flag to indicate use of nbr list
    logical ::    uselist = .false.

        !> flag for domain decomposition mc
    logical ::    useddmc = .false.

        !> flag to distribute g-vector storage over processors
    logical ::    distribgvec = .false.

        !> flag to switch on sampling of energies
    logical ::    lsample_energy = .false.

        !> flag to switch on accumulation of P(V) distribution in NpT
    logical ::    lsample_volume = .false.

        !> flag to put parallel loop over atoms not molecule
    logical ::    lparallel_atom = .false.

        !> flag to reset atom positions within periodic box
    logical ::    usereset = .false.

        !> use Feynman-Hibbs potential adjustment for lennard-Jones potentials
    logical ::    usefeynmanhibbs = .false.

        !> number of configuration/cell replicas
    integer ::    nconfigs = 1

        !> defines the cell type: 0 -> \b 3D-bulk (default), or less than 0 -> \b 2D-slit (planar pore) \n
        !> \b 2D-slit case:\n
        !> [ -9, -1] -> slit with  no charged wall  (reserved range for VdW adsorption) \n
        !> [-19,-10] -> slit with one charged wall  (at bottom in Z) \n
        !> [-29,-20] -> slit with two charged walls (at bottom and top) \n
        !> -?0 -> simple "in-box" calculations for electrostatics (el-st), no MFA \n
        !> -?1 -> mean-field approximation (MFA) outside rectangular "in-box" cut-off(s) \n
        !> -?2 -> mean-field approximation (MFA) outside cylindrical cut-off (Rcut := Rcut_xy) \n
        !> -?3 -> mean-field approximation (MFA) outside spherical cut-off (Rcut := Rcut_xyz)
    integer ::    cell_type = 0

        !> type of accounting for Coulomb interactions (electrostatics): \n
        !> \b 3D-bulk (default): subject to `is_slit = .false.` & `cell_type >= 0` \n
        !> 0 -> no electrostatics (???) \n
        !> 1 -> Ewald (default), \n
        !> 2 -> direct calculation (only real-space Ewald, i.e. reciprocal part is skipped), \n
        !> 3 -> shifted Coulomb (at cut-off) \n
        !> 3 -> shifted + damped Coulomb (at cut-off) \n
        !> \b 2D-slit (planar pore): subject to `is_slit = .true.` & `cell_type < 0` \n
        !>  0 -> no electrostatics -> `cell_type` is reset to within [-9,-1] \n
        !> -1 -> initial MFA iteration (no input MFA tables; subject to `cell_type < 10`) \n
        !> -2 -> continuation MFA iteration (use input MFA tables; subject to `cell_type < 10`) \n
        !> -3 -> no MFA is used (i.e. simple explicit "in-box" electrostatics only!) \n
        !> if in CONTROL `cell_type = -10 or -20` and `coultype < 0` then `coultype` is reset to -3
    integer, allocatable, dimension(:) ::    coultype

        !> frequency at which the box is reset
    integer ::    reset_freq = 100

        !> frequency of sampling energies
    integer ::    sample_energy_freq = 1000

        !> cavity grid spacing in x, y and z directions
    real (kind = wp) ::    cavity_x, cavity_y, cavity_z

        !> frequency in which cavity bias grid is recalculated (default = 1000)
    integer ::    cavity_update

        !> the number of energy calculations for orient bias rotations
    integer ::    obias_rot_k

        !> dimension of the stack size: default 100
    integer ::    stacksize

        !TU*: Added by me...
        !> Frequency of samples for average and standard deviation of energies, volume, enthalpies etc.
        !> and number of each species: default 1. Note that the 'stacksize' variable is in units of this,
        !> e.g. if 'stacksize=10' and 'sample_stats_freq=5' then the rolling averages are over a period of
        !> 50 MC steps (i.e. 5 samples at 10 MC step intervals).
    integer ::    sample_stats_freq = 1

        !> freq for recalculation of atom distnce accept ratio default: 100
    integer ::    acc_atmmve_update

        !> freq for recalculation of mol distance accept ratio default: 100
    integer ::    acc_molmve_update

        !> freq for recalculation of mol roation accept ratio default: 100
    integer ::    acc_molrot_update

        !> freq for recalculation of vol accept ratio default: 100
    integer ::    acc_vol_update

        !> how the volume is sampled in the case of npt
    integer ::    type_vol_move

        !> freqency of volume steps in detailed balance
    integer ::    vol_change_freq

        !> frequency of sampling volume for P(V) distribution
    integer ::    sample_volume_freq = 1

        !> number of atom pairs to be swapped
    integer ::    numswap

        !> atom move frequency
    integer ::    atmmovefreq

        !> molecule move frequency
    integer ::    molmovefreq

        !> molecule rotation frequency
    integer ::    molrotfreq

        !> the frequency of swaps to occur default: 1
    integer ::    swapfreq

        !> freqency for insertion of atoms in gcmc
    integer ::    gcmcatomfreq

        !> number of atoms to be inserted in gcmc
    integer ::    numgcmcatom

        !> number of molecules to be inserted in  gcmc
    integer ::    numgcmcmol

        !> freqency for insertion of mols in gcmc
    integer ::    gcmcmolfreq

        !> frequency of transfers of atoms in gibbs ensemble
    integer ::    gibbs_atmtran_freq

        !> frequency of transfers of molecules in gibbs ensemble
    integer ::    gibbs_moltran_freq

        !> number of atoms in transfers in gibbs ensemble
    integer ::    num_atmtran_gibbs

        !> number of molecules in transfers in gibbs ensemble
    integer ::    num_moltran_gibbs

        !> frequency of excahnges of atoms in gibbs ensemble
    integer ::    gibbs_atmexch_freq

        !> frequency of excahnges of molecules in gibbs ensemble
    integer ::    gibbs_molexch_freq

        !> number of atoms in excahnges in gibbs ensemble
    integer ::    num_atmexch_gibbs

        !> number of molecules in excahnges in gibbs ensemble
    integer ::    num_molexch_gibbs

        ! not sure
    integer ::    numintegrate

        !> number of atom pairs for semi grand ensemble
    integer ::    numsemigrandatms

        !> number of atoms subjected to semi-Widom moves
    integer ::    numsemiwidomatms

        !> number of atom types being moved
    integer ::    numatmmove

        !> number of mol types being moved
    integer ::    nummolmove

        !> number of mol types being rotated
    integer ::    nummolrot

        !> frequency of nonbonded nbr list updates  default: 50
    integer ::    nbrlist

        !> frequency of nonbonded three body nbr list updates (not working)  default: 50
    integer ::    thblist

        !> the maximum number of 3-body nbrs in lookup default: 6 
    integer ::    max_thb_nbrs

        !> max allowed nonbonded interactions default tot_no_atoms/2
    integer ::    max_nonb_nbrs

        !> frequency of printing stats data default: 1000
    integer ::    statsout

        !> frequency of print of energy data default: 1000
    integer ::    sysprint

        !> frequency of dump of coordinates default: 1000
    integer ::    sysdump

        !> frequency of checks/prints for accumulated energy: 10000
    integer ::    syscheck

        !> Level of precision which will trigger an error in energy checks: this
        !> is used in the 'check_energy' procedure in 'field.f90' and in the 
        !> 'rep_exch_config' and 'rep_exch_beta' procedures in 
        !> 'rep_exchange_module.f90'. The significance of this value varies.
        !> E.g. sometimes it is the threshold precision for energy differences to 
        !> be considered the same (in internal units); sometimes it is applied to
        !> relative energy difference. Sometimes also a slightly larger threshold
        !> is used, e.g. 100 times `checkenergyprec`.
        !> Default: 1.0e-10.
    real(kind=wp) :: checkenergyprec = 1.0e-10


        !> Flag enabling periodic checks that neighbour lists are correct. Default = .false.
    logical :: nbrlistcheck = .false.

        !> Frequency of checks that neighbour lists are correct. Default = 10000
    integer :: nbrlistcheckfreq = 10000

        !> Flag enabling periodic checks that counts of atomic and molecular species are correct. Default = .false.
    logical :: speciescountcheck = .false.

        !> Frequency of checks that species counts are correct. Default = 10000
    integer :: speciescountcheckfreq = 10000


        !> output style for revcon file 0=mc, 1=hybrid, 2,4=dlpoly2/4
    integer ::    revcon_format

        !> Flag enambling extra dumps periodically to REVCON. Note that these dumps are
        !> in addition to dumps to REVCON performed at frequency 'sysdump'
    logical ::    extrarevcon = .false.

        !> Frequency of exta dumps to REVCON only. Note that these dumps are
        !> in addition to dumps to REVCON performed at frequency 'sysdump'
    integer ::    extrarevconfreq = 1000

        !> output style for archive file 0=mc, 1=hybrid, 2-5=dlpoly2/4, 3,5,6=dcd
    integer ::    archive_format

        !> input style for trajectory file 0=mc, 1=hybrid, 2-5=dlpoly2/4, 3,5,6=dcd
    integer ::    traj_format

        !> first & last frames to take from trajectory, and step for doing so
    integer ::    traj_beg, traj_end, traj_step

    integer :: nfltprec

        !> flag for single/double precision of DCD frames
    integer ::    dcd_form

        !> flag for storing (or not) the PDB cell info with DCD frames
    integer ::    dcd_cell

        !> total number of iterations default 0
    integer ::    maxiterations

        !> number of unit cells to increase box size
    integer ::    scalex, scaley, scalez

        !> freqency of semi grand ensemble default: 0
    integer ::    semigrandatmsfreq

        !> freqency of semi-Widom attempts
    integer ::    semiwidomatmsfreq

        !> substitution type of atom 1
    integer ::    subtype1

        !> substitution type of atom 2
    integer ::    subtype2

        !>  number atoms in the hybrid atom in fep
    integer ::    hybridno

        !> type 1 in free energy perturbation (integer)
    integer ::    feptype1

        !> type 2 in free energy perturbation (integer)
    integer ::    feptype2

        !> number of equilibration steps default: 0
    integer ::    sysequil

        !> number of steps to heat system prior to equilibration
    integer ::    sysheat

        !> max number of recip cells in ewald sum for x vector
    integer ::    kmax1

        !> max number of recip cells in ewald sum for y vector
    integer ::    kmax2

        !> max number of recip cells in ewald sum for z vector
    integer ::    kmax3

        !> energy unit for conversion of potentials default: eV
    real(kind=wp) ::    energyunit

        !> alpha for ewald sum (if provided)
    real(kind=wp) ::    ewald_alpha

        !> medium dielectric permittivity
    real(kind=wp) ::    dielec

        !> max volume change for monte carlo default: 0.1
    real(kind=wp) ::    maxvolchange

        !> volume bin size for V-grid in NpT, default: 0.0 (NVT)
    real(kind=wp) ::    vol_bin

        !> power/exponent for bin growth on V-grid in NpT, [1 .. 2] default: 1.0 (linear)
    real(kind=wp) ::    vol_bin_power

        !> main system temperature (workgroup 0)
    real(kind=wp) ::    systemp

        !> external pressure
    real(kind=wp) ::    syspres

        !> Flag for if the system pressure has been specified in units of k_B*K/Angstrom^3
    logical :: syspres_kunits = .false.

        !> accuracy for ewald sums default: 1e-6
    real(kind=wp) ::    madelungacc

        !> velet skin distance (ang) default: 0.5
    real(kind=wp) ::    verletshell

        !> max displacement of an atom default 0.01
    real(kind=wp) ::    iondisp

        !> max displacement of a mol default 0.01
    real(kind=wp) ::    moldisp

        !> max angle of rotation for a molecule default 0.1
    real(kind=wp) ::    molrot

        !> acceptance ratio for atom moves default 0.37
    real(kind=wp) ::    acc_atmmve_ratio

        !> acceptance ratio for mol moves default 0.37
    real(kind=wp) ::    acc_molmve_ratio

        !> acceptance ratio for mol rotations: default 0.37
    real(kind=wp) ::    acc_molrot_ratio

        !> acceptance ratio for vol moves default 0.37
    real(kind=wp) ::    acc_vol_ratio

        !> min distance between atoms in gcmc insert
    real(kind=wp) ::    gcmcmindist

        !> min distance for insert in gibbs ensemble
    real(kind=wp) ::    gibbsmindist

        !> radius for cavity bias
    real(kind=wp) ::    cavity_radius

        !> distance cutoff for bonded interactions default: =nonbonded cutoff
    real(kind=wp) ::    bondcut

        !> distance cutoff for non-bonded interactions and real space interactions default: 0.0
    real(kind=wp) ::    shortrangecut

        !> distance cutoff for vdw energies
    real(kind = wp) ::  vdw_cut

        !> the Adams B parameter           
    real(kind=wp), allocatable, dimension(:) ::    gcmc_atompot

        !> energy / lambda for molecules in gcmc
    real(kind=wp), allocatable, dimension(:) ::    gcmc_molpot

        !> Chemical potentials used in transmutation (semi-grand) 
    real(kind=wp), allocatable, dimension(:) ::    deltatrans

        !> csaling for coordinates and lattice vectors (not used)
    real(kind=wp) ::    vecscalex, vecscaley, vecscalez

        !> tolerance used to reject stupid moves 
    real(kind=wp) ::    toler

        !> used to bias movement for molecular translations along each
        !> Cartesian dimension
    real(kind=wp), dimension(:,:), allocatable :: molmove

        !> used to bias movement for atomic translations along each
        !> Cartesian dimension
    real(kind=wp), dimension(:,:), allocatable :: atmmove

        !> molecule ids (for archive file 0=mc, 1=dlpoly only!)
    character*128 :: hist_molids = ""
    character*128 :: zcom_molids = ""

        !> simulation mode - always monte
    character*8 ::    simulationmode

        !> names of molecules to be inserted
    character*8, dimension(:), allocatable ::    gcmcmolinsert

        !> names of molecules to move
    character*8, dimension(:), allocatable ::    molmover

        !> names of molecule allowed to rotate
    character*8, dimension(:), allocatable ::    molroter

    !element 

        !> atom types to be moved
    character*8, dimension(:), allocatable ::    atmmover

        !> type of atom to be moved
    integer, dimension(:), allocatable ::        atmmover_type

        !> transmutation element type 1
    character*8, dimension(:), allocatable ::    trans1

        !> transmutation element type 1
    integer, dimension(:), allocatable ::        trans1_type

        !> transmutation element type 2
    character*8, dimension(:), allocatable ::    trans2

        !> transmutation element type 2
    integer, dimension(:), allocatable ::        trans2_type

        !> insertion of atom type
    character*8, dimension(:), allocatable ::    gcmcatminsert

        !> atom type to be inserted
    integer, dimension(:), allocatable ::        gcmcatminsert_type

        !> atoms to be transfered in gibbs ensemble
    character*8, dimension(:), allocatable ::    gibbsatmtrans

        !> atom types to be transferred
    integer, dimension(:), allocatable ::        gibbsatmtrans_type

        !> atoms to be exchanged in gibbs ensemble
    character*8, dimension(:), allocatable ::    gibbsatmexchange1, gibbsatmexchange2

        !> atom types to be exchanged *.
    integer, dimension(:), allocatable ::        gibbsatmexchange_type1, gibbsatmexchange_type2

        !> semi grand element type for forward change
    character*8, dimension(:), allocatable ::    semiwidomele1

        !> atm type for forward change
    integer, dimension(:), allocatable ::        semiwidom1_type

        !> semi grand element type for backward change
    character*8, dimension(:), allocatable ::    semiwidomele2

        !> atm type for back change
    integer, dimension(:), allocatable ::        semiwidom2_type

       !> semi grand element type for forward change
    character*8, dimension(:), allocatable ::    semigrandele1

        !> atm type for forward change
    integer, dimension(:), allocatable ::        semigrand1_type

        !> semi grand element type for backward change
    character*8, dimension(:), allocatable ::    semigrandele2

        !> atm type for back change
    integer, dimension(:), allocatable ::        semigrand2_type

        !> element type to be swapped
    character*8, dimension(:), allocatable ::    swapele1

        !> atm type to be swapped
    integer, dimension(:), allocatable ::        swapele1_type

        !> element type to be swapped
    character*8, dimension(:), allocatable ::    swapele2

        !> atm type to be swapped
    integer, dimension(:), allocatable ::        swapele2_type

        !> relative frequency of different types of atom swap moves
    integer, dimension(:), allocatable ::        atmswapfreq
    
        !> the molecule data for the gcmc ensemble
    character*8, dimension(:), allocatable ::    gcinsert_mols

        !> flag for semigrand ensemble for molecules
    logical ::                                   semigrandmols

        !> number of molecules involved in semi grand ensemble
    integer ::                                   numsemigrandmol

        !> frequency of calculation
    integer ::                                   semigrandmolfreq

        !> molecule data for semigrand
    character*8, dimension(:), allocatable ::    semigrand1_mols, semigrand2_mols

        !> molecules to be transfered in gibbs ensemble
    character*8, dimension(:), allocatable ::    gibbsmolinsert

        !> molecules to be exchanged in gibbs ensemble
    character*8, dimension(:), allocatable ::    gibbsmolexchange1, gibbsmolexchange2

        !> flag for FED calculation
    logical :: fedcalc = .false.

        !> number of steps between attempted FED steps (default 10000000 - set in control_module)
    integer :: fed_trial_freq = 1

        !> flag for replica exchange
    logical :: repexch = .false.

        !> the number of replicas
    integer :: numreplicas = 1

        !> number of steps between attempted exchanges
    integer :: repexch_freq

        !> temperature increment for replica exchange
    real (kind = wp) :: repexch_inc

        !> z-density flag
    logical  :: lzdensity = .false.
    logical  :: lzcomzero = .false.

        !> no z-density points
    integer  :: nzden

        !> frequency of z-density calculation
    integer  :: zden_freq

        !> rdf-sampling flag
    logical  :: lrdfs = .false.

        !> rdf-sampling flag
    logical  :: lrdfx = .false.

        !> no z-density points
    integer  :: nrdfs

        !> frequency of z-density calculation
    integer  :: rdfs_freq

        !> max dostance for rdfs
    real(kind = wp) :: rdf_max

        !sjc: adding ability to specify seeds 
    logical :: fseeds  ! if true (default) fixed seeds are used - helps debugging
    logical :: lseeds  ! the seeds are supplied in input
    logical :: rseeds  ! the seeds are determined from the clock
    integer, dimension(4) :: seeds_ijkl

        !> the total run time, and the time for the current job to stop smoothly
    real (kind = wp) :: jobtime, closetime

        !> variables for displacement sampling
    logical :: lsample_displacement = .false.

    integer:: sample_disp_avg = 1000
    
        !> analyse bilayer properties
    logical ::    lanabil = .false.

    integer :: lipid_idx, tails_num
    integer, dimension(:,:), allocatable :: tails

    !TU: Variables for the orientation functionality

        !> Flag determining whether orientation functionality is in use
    logical :: useorientation = .false.

        !> Flag determining whether orientations and COMSs will be periodically output to ORIENT
    logical :: sample_orientation = .false.

        !> Frequency of output of orientations and COMs
    integer :: sample_orientation_freq = 1000

        !> Flag determining whether the initial orientations are output to ORIENT
    logical :: sample_orientation_init = .false.


    !TU: Variables determining nature of output to YAMLDATA

        !> Flag determining whether data is to be output to YAMLDATA
    logical :: useyamldata = .false.

        !> Frequency of output to YAMLDATA
    integer :: yamldata_freq = 1000

        !> Flag determining whether to output the scalar nematic order parameter to YAMLDATA
        !> using the 2nd Legendre polynomial
    logical :: yamldata_output_scalarop2 = .false.

        !> Flag determining whether to output the scalar nematic order parameter to YAMLDATA
        !> using the 4th Legendre polynomial
    logical :: yamldata_output_scalarop4 = .false.
    

    !TU: Variables pertaining to spin-type atoms and atom rotation moves
    
        !> Flag if atoms with 'spin' type are present in the system
    logical :: is_spin = .false.

        !> Flag determining whether atomic rotation moves are used
    logical :: rotateatm = .false.

        !> Atom rotation move frequency
    integer :: atmrotfreq

        !> Number of atomic types being rotated
    integer :: numatmrot

        !> Frequency for recalculation of atom rotation acceptance ratio (default 100)
    integer :: acc_atmrot_update

        !> Acceptance ratio for atom rotations
    real(kind=wp) :: acc_atmrot_ratio
    
        !> Maximum angle of rotation for an atom
    real(kind=wp) ::    atmrot
    
        !> Names of atom allowed to rotate
    character*8, dimension(:), allocatable :: atmroter

        !> Types of atom allowed to rotate
    integer, dimension(:), allocatable :: atmroter_type
    
    !scp: move which will perform systematic scan of a molecule
        logical :: scanmol = .false.
        logical :: scan_slab = .false.
        integer  ::  num_scan_start = 0
        integer  ::  num_scan_stop = 0
        integer  ::  scan_count = 0
        real (kind = wp) :: scan_step 
        integer  ::  scan_nstp(3) 
        integer  ::  max_scan = 0
        real (kind = wp) :: scan_sstp(3)
        real (kind = wp) :: scan_svec(3)    



    ! Control variables for cell list functionality


        !> Flag determining if cell lists are in use
    logical :: clist = .false.

        !> Minimal size of a cell in the x, y or z directions
    real(wp) :: clist_rmin = 0.0_wp

        !> Maximum number of atoms allowed in a cell
    integer :: clist_maxpercell = 1000

        !> Flag to print fine details of cell lists (for debugging primarily)
    logical :: clist_printdetails = .false.

        !> Flag to periodically check the integrity of cell lists
    logical :: clist_check = .false.

        !> Frequency for checks of cell list integrity
    integer :: clist_checkfreq = 1000


    
    ! Control variables for lambda moves

        !> Flag determining if lambda moves are in use
    logical :: lambdamoves = .false.

        !> Frequency of lambda moves
    integer :: lambdamovefreq = 0

        !> Flag determining if only the energy of a specified (lambda-dependent) molecule is to be calculated 
        !> during a lambda move, as opposed to the whole system
    logical :: lambdaonemolecule = .false.

        !> Name of fractional molecule species for energy calculation if 'lambdaonemolecule' is in effect
    character*8 :: lambdamoltarget

        !> Target molecular species for energy calculation if 'lambdaonemolecule' is in effect
    integer :: lambdamoltargettyp = 0

        !> Initial value of lambda
    real(kind=wp) :: initiallambda = 0.0_wp

        !> Change in lambda during lambda move (in positive or negative direction)
    real(kind=wp) :: maxlambdachange = 0.0_wp


        !> Flag determining if lambda insert/delete moves for molecules are in use
    logical :: lambdainsertmol = .false.

        !> Frequency of lambda insert/delete moves
    integer :: lambdainsertfreq = 0

        !> Thermodynamic activity (exp(beta*mu)) associated with lambda insert/delete moves
    real(kind=wp) :: lambdainsert_activity = 0.0_wp

        !> Name of species for lambda insert/delete moves earmarked as the 'real' molecules in the movee
    character*8 :: lambdainsert_real

        !> Name of species for lambda insert/delete moves earmarked as the 'fractional' molecules in the movee
    character*8 :: lambdainsert_frac

        !> Molecular species for lambda insert/delete moves earmarked as the 'real' molecules in the move
    integer :: lambdainsert_realtyp = 0

        !> Molecular species for lambda insert/delete moves earmarked as the 'fractional' molecule in the move
    integer :: lambdainsert_fractyp = 0



        !> Flag determining if moves and rotations will be performed in the x/y plane only
    logical :: usexymoves = .false.


end type

end module
