! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module nbrlist_type

    use kinds_f90
    implicit none

type nbrlist

        !> the max no of nonbonded nbrs in the list
    integer ::    max_nonb

        !nonbonded list

        !> actual nbr list - 3rd index 1= molecule no, 2 = atom no in mol*/
        !TU-: Each molecule has a neighbour list over atoms in that molecule: 
        !TU-: 'nbr_list(i,n,:)' gives the 'n'th neighbouring atom of atom 'i' 
        !TU-: for the molecule; 'nbr_list(i,n,1)' is the index of the molecule 
        !TU-: containing the neighbouring atom and 'nbr_list(i,n,2)' is the 
        !TU-: index of the atom within that molecule.
        !TU-:
        !TU-: The dimensions of 'nbr_list' should be reversed to improve 
        !TU-: efficiency. I think JG has already been toying with this.
    integer, dimension (:,:,:), allocatable ::    nbr_list

        !> the number of nbrs an atom has
    integer, dimension (:), allocatable ::    numnbrs

        !> excluded list
        !TU-: 'exclist(i,j)' determines whether atoms 'i' and 'j' within the molecule
        !TU-: interact. It's not clear what the different values of 'exclist' correspond
        !TU-: to. 'set_atom_exclist(ic, im, i)' in 'field.f90' sets it to the bond potential 
        !TU-: label for bonded pairs of atoms, 0 for non-bonded pairs of atoms, and 9999
        !TU-: for non-bonded pairs of atoms within the same molecule if intra-molecular Coulomb 
        !TU-: energies are to be excluded.
    integer, dimension (:,:), allocatable ::    exclist

end type

end module
