! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

module thbpotential_module

    use kinds_f90

    implicit none

    integer, parameter :: npar = 12

    integer :: numthb = 0   ! number of thb potentials
    integer, dimension(:,:), allocatable :: thbtypes

    real(kind = wp), allocatable, dimension(:,:) :: prmthb
    real(kind = wp), allocatable, dimension(:) :: ltpthb

    !> Global cut-offs for setting three-body lists. For an atom i, only atoms j and k within
    !> this cut-off of i (and each other) are considered for triplets for i's list: rij, rik
    !> and rjk must all be less than the corresponding cut-offs for inclusion within i's list
    real(kind = wp) :: thblistcutoffij = 0.0_wp
    real(kind = wp) :: thblistcutoffik = 0.0_wp
    real(kind = wp) :: thblistcutoffjk = 0.0_wp


contains

subroutine alloc_thbpar_arrays(num)

    use kinds_f90
    use constants_module, only : uout

    implicit none

    integer, intent(in) :: num

    integer :: fail(3)

    fail = 0
    numthb = num

    allocate(prmthb(num, npar), stat = fail(1))
    allocate(thbtypes(num, 3), stat = fail(2))
    allocate(ltpthb(num), stat = fail(3))

    if(any(fail > 0)) then

        call error(161)

    endif

    prmthb(:,:) = 0.0
    thbtypes(:,:) = 0
    ltpthb(:) = 0

end subroutine

subroutine read_thb_par(unit)

    use kinds_f90
    use constants_module, only : uout, ufld, TORADIANS
    use parse_module
    use species_module, only : number_of_elements, element, eletype, get_species_type
    use comms_mpi_module, only : master

    implicit none

    real(kind = wp), intent(in) :: unit

    integer :: nread

    integer :: i, j, typi, typj, typk, typthb

    real(kind = wp) :: fc, cz, rij, rik, rjk, rho1, rho2
    character :: line*100, word*40, atmi*8, atmj*8, atmk*8, keyword*12

    logical safe, found

    safe = .true.

    nread = ufld


    if( master ) then 

        write(uout,"(/,/,1x,100('-'))")
        write(uout,"(' three-body potentials ')")
        write(uout,"(1x,100('-'))")

    end if


    do i = 1, numthb

        call get_line(safe, nread, line)
        if (.not.safe) go to 1000

        call get_word(line, keyword)

        if (keyword(1:3) == 'cos') then

            ltpthb(i) = 1

        elseif (keyword(1:4) == 'harm') then

            ltpthb(i) = 2

        elseif (keyword(1:4) == 'thrm') then

            ltpthb(i) = 3

        elseif (keyword(1:4) == 'shrm') then

            ltpthb(i) = 4


        elseif (keyword(1:8) == 'stillweb') then

            !TU: 3-body component of Stillinger-Weber potential

            ltpthb(i) = 5

        else

            call error(162)

        endif

        typthb = ltpthb(i)

        !TU: Note that the atoms are read in order such that the middle atom in a triplet is the middle atom in
        !TU: the list of three read in
        
        call get_word(line,atmj)
        call get_word(line,word)
        typj = get_species_type(word)
        !typi = get_species_type(word) !AB: DL_POLY convention is to have the central atom index (i) in the middle

        call get_word(line,atmi)
        call get_word(line,word)
        typi = get_species_type(word)
        !typj = get_species_type(word) !AB: DL_POLY convention is to have the central atom index (i) in the middle

        call get_word(line,atmk)
        call get_word(line,word)
        typk = get_species_type(word)


        found = .false.
        do j = 1, number_of_elements

            if(atmi == element(j) .and. typi == eletype(j)) then

                thbtypes(i,1) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(163)
            
        endif

        found = .false.
        do j = 1, number_of_elements

            if(atmj == element(j) .and. typj == eletype(j)) then

                thbtypes(i,2) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(164)
            
        endif

        found = .false.
        do j = 1, number_of_elements

            if(atmk == element(j) .and. typj == eletype(j)) then

                thbtypes(i,3) = j
                found = .true.

            endif

        enddo

        if(.not.found) then

            call error(164)
            
        endif


        select case(typthb)

            case(1)

                !TU: Harmonic cos three-body potential

                !TU*: rij, rik and rjk will be set to 0 if not explicitly set in FIELD! The manual is wrong regarding the
                !TU*: form of input for 3-body potentials, and should be updated.
               
                !TU*: Catch if rij, rik or rjk is set to 0 and return an error - for any of the below potentials
                !TU*: Standardise output style

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if( master ) then
                        
                    write(uout,'(1x,"Harmonic Cosine Three body Potential")')
                    write(uout,"(1x,3a8,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rij, rik, rjk

                endif

                !convert force constant to internal units
                fc = fc * unit

                ! convert angle to radians then to cos(angle)
                cz = cz * TORADIANS
                cz = cos(cz)

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,5) = rij  !5,6,7 are always the distance for the cutoff 
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(2)

                !TU: Harmonic three-body potential

                !TU*: rij, rik and rjk will be set to 0 if not explicitly set in FIELD. The manual is wrong regarding the
                !TU*: form of input for 3-body potentials, and should be updated.

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if( master ) then

                    write(uout,'(1x,"Harmonic Three body Potential")')
                    write(uout,"(1x,3a8,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rij, rik, rjk

                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(3)

                !TU: Truncated harmonic three-body potential

                !TU*: rij, rik and rjk will be set to 0 if not explicitly set in FIELD. The manual is wrong regarding the
                !TU*: form of input for 3-body potentials, and should be updated.

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rho1 = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if( master ) then

                    write(uout,'(1x,"Truncated Harmonic Three body Potential")')
                    write(uout,"(1x,3a8,1x,3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rho1

                    !TU*: What is this warning for??
                    call warning(26)

                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,3) = rho1
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk

            case(4)

                !TU: Screened harmonic three-body potential

                !TU*: rij, rik and rjk will be set to 0 if not explicitly set in FIELD. The manual is wrong regarding the
                !TU*: form of input for 3-body potentials, and should be updated.

                call get_word(line,word)
                fc = word_2_real(word)
                call get_word(line,word)
                cz = word_2_real(word)
                call get_word(line,word)
                rho1 = word_2_real(word)
                call get_word(line,word)
                rho2 = word_2_real(word)
                call get_word(line,word)
                rij = word_2_real(word)
                call get_word(line,word)
                rik = word_2_real(word)
                call get_word(line,word)
                rjk = word_2_real(word)

                if( master ) then

                    write(uout,'(1x,"Screened Harmonic Three body Potential")')
                    write(uout,"(1x,3(a8,1x),3x,10f15.6)") &
                        atmi, atmj, atmk, fc, cz, rho1, rho2

                    !TU*: What is this warning for?
                    call warning(26)

                endif

                !convert force constant to internal units
                fc = fc * unit
                ! convert angle_0 to radians
                cz = cz * TORADIANS

                prmthb(i,1) = fc
                prmthb(i,2) = cz
                prmthb(i,3) = rho1
                prmthb(i,4) = rho2
                prmthb(i,5) = rij
                prmthb(i,6) = rik
                prmthb(i,7) = rjk


            case(5)

                !TU: 3-body component of Stillinger-Weber potential

                !TU*: Breaks with convention above that 5-7 are rij, rik rjk cut-offs. Have just one cut-off? Store these
                !TU*: as indices -1,-2 and -3, perhaps as a duplicate to the other prmthb variables? Also have index 0 as
                !TU*: the 3-body list threshold? Or have an extra parameter in the THB line which is a 3-body list cut-off length

                !TU: Parameters read from FIELD (same significance as in LAMMPS Stillinger-Weber documentation):
                !TU: 1) epsilon      [energy]
                !TU: 2) lambda       [dimensionless]
                !TU: 3) cos(theta0)  [dimensionless]
                !TU: 4) sigma_ij     [length]
                !TU: 5) gamma_ij     [dimensionless]
                !TU: 6) a_ij         [dimensionless]
                !TU: 7) sigma_ik     [length]
                !TU: 8) gamma_ik     [dimensionless]
                !TU: 9) a_ik         [dimensionless]
                !TU: 10) rij         [length]
                !TU: 11) rik         [length]
                !TU: 12) rjk         [length]

                do j = 1, 12
                    call get_word(line,word)
                    prmthb(i,j) = word_2_real(word)
                end do

                !TU*: Catch stupid input parameters - in particular rij, rik, rjk = 0!

                if( master ) then

                    write(uout,'(1x,"Stillinger-Weber three-body Potential")')
                    write(uout,"(1x,3(a8,1x),3x,12f15.6)") &
                        atmi, atmj, atmk, prmthb(i,:)

                endif

                !TU: Convert epsilon parameter into internal units
                prmthb(i,1) = prmthb(i,1) * unit


        end select


    enddo

    if( master ) then

        write(uout,'(/,1x,a,3(x,e12.6),/)') &
              'Cut-offs used for setting three-body lists (rij, rik, rjk) (Angstroms) = ', &
              thblistcutoffij, thblistcutoffik, thblistcutoffjk

    end if


    return

1000 call error(166)

end subroutine



!TU*: Take costheta as argument here instead of theta - take the inverse cos if necessary within this function.

!> calculates the force for a triplet
subroutine calc_thb_energy(pot, theta, rij, rik, rjk, eng)

    use kinds_f90

    implicit none

    integer, intent(in) :: pot
    real(kind = wp), intent(in) :: theta, rij, rik, rjk
    real(kind = wp), intent(out) :: eng

    integer :: typ
    real(kind = wp) :: rijmax, rikmax, rjkmax, fc, cz, costh, scrn, dtheta, rho1, rho2

    eng = 0.0_wp

    if(pot == 0) return

    typ = ltpthb(pot)

    select case(typ)

        case(1)

            !TU: Harmonic cos three-body potential

            fc = prmthb(pot, 1)
            cz = prmthb(pot, 2)
            rijmax = prmthb(pot, 5)
            rikmax = prmthb(pot, 6)
            rjkmax = prmthb(pot, 7)

            if (rij <= rijmax .and. rik <= rikmax .and. rjk <= rjkmax) then

                !cos
                costh = cos(theta)
                eng = 0.5_wp * fc * (costh - cz)**2

            else

                !TU*: Disabled
                !call error(131)

            endif

        case(2)

            !TU: Harmonic three-body potential

            fc = prmthb(pot, 1)
            cz = prmthb(pot, 2)
            rijmax = prmthb(pot, 5)
            rikmax = prmthb(pot, 6)
            rjkmax = prmthb(pot, 7)

            if (rij <= rijmax .and. rik <= rikmax .and. rjk <= rjkmax) then
                !TU: Code for 'harm'. Note that (theta-cz) is in radians, which is the convention
                !TU: (despite angles being read in degrees in FIELD)
            
                eng = 0.5_wp * fc * (theta - cz)**2

            else

                !TU*: Disasbled
                !call error(131)

            endif

        case(3)

            ! truncated Harmonic potential

            !TU*: No check for rij<=rijmax etc. Is this a problem?

            fc = prmthb(pot,1)
            cz = prmthb(pot,2)
            rho1 = prmthb(pot,3)
            dtheta = theta - cz
            scrn = ((rij**8 + rik**8) / rho1)

            !TU*: Missing 0.5 prefactor?
            eng = fc * dtheta * dtheta * exp(-scrn)

        case(4)

            !TU*: No check for rij<=rijmax etc. Is this a problem?

            ! screened Harmonic potential
            fc = prmthb(pot,1)
            cz = prmthb(pot,2)
            rho1 = prmthb(pot,3)
            rho2 = prmthb(pot,4)
            dtheta = theta - cz
            scrn = rij / rho1 + rik / rho2

            !TU*: Missing 0.5 prefactor?
            eng = fc * dtheta * dtheta * exp(-scrn)

        case(5)
 
            !TU: Stillinger-Weber three-body component

            !TU: List of parameters in prmthb (same significance as in LAMMPS Stillinger-Weber documentation):
            !TU: prmthb(pot,1) = epsilon      [energy]
            !TU: prmthb(pot,2) = lambda       [dimensionless]
            !TU: prmthb(pot,3) = cos(theta0)  [dimensionless]
            !TU: prmthb(pot,4) = sigma_ij     [length]
            !TU: prmthb(pot,5) = gamma_ij     [dimensionless]
            !TU: prmthb(pot,6) = a_ij         [dimensionless]
            !TU: prmthb(pot,7) = sigma_ik     [length]
            !TU: prmthb(pot,8) = gamma_ik     [dimensionless]
            !TU: prmthb(pot,9) = a_ik         [dimensionless]
            !TU: prmthb(pot,10) = rij         [length]
            !TU: prmthb(pot,11) = rik         [length]
            !TU: prmthb(pot,12) = rjk         [length]

            rijmax = prmthb(pot, 10)
            rikmax = prmthb(pot, 11)
            rjkmax = prmthb(pot, 12)

            if (rij <= rijmax .and. rik <= rikmax .and. rjk <= rjkmax) then

                eng = vsw3( prmthb(pot,1), prmthb(pot,2), prmthb(pot,3), prmthb(pot,4), &
                            prmthb(pot,5), prmthb(pot,6), prmthb(pot,7), prmthb(pot,8), &
                            prmthb(pot,9), theta, rij, rik )
           
            end if

    end select

end subroutine


!> Function for the 3-body component of the Stillinger-Weber potential.
!> Note that ths significance of the parameters is the same as in the LAMMPS
!> Stillinger-Weber implementation (and documentation).
real(kind = wp) function vsw3( epsilon, lambda, costheta0, sigmaij,  &
                               gammaij, aij,    sigmaik,   gammaik,  &
                               aik,     theta,  rij,       rik      )

    use kinds_f90

    implicit none

    real(kind = wp), intent(in) :: epsilon, lambda, costheta0, sigmaij,  &
                                   gammaij, aij,    sigmaik,   gammaik,  &
                                   aik,     theta,  rij,       rik      


    vsw3 = lambda * epsilon * (cos(theta)-costheta0)**2 * &
           exp( gammaij*sigmaij / (rij-aij*sigmaij) + &
                gammaik*sigmaik / (rik-aik*sigmaik) ) 

end function vsw3


end module
