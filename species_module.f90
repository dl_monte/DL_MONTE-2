! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Molecular species specs (atom-wise): initialisation & manipulation, based on input (FIELD file)
!> @usage
!> - `readspecies(..)` reads in specs for atoms within molecular species from FIELD file
!> -  other routines deal with the memory and species manipulations
!> - @stdusage
!> - @stdspecs
!> @using
!> - `kinds_f90`
!> - many other modules within routines

!> @modulefor molecular species: reading species data from FIELD file & allocating needed memory
module species_module

    use kinds_f90
    use molecule_type

    implicit none

    integer :: number_of_elements  ! number of elements
    integer :: number_of_molecules ! no of molecules

        !> Element name
    character*8, dimension(:), allocatable :: element

        !> type ie core/semiconductor/metal
    integer, dimension(:), allocatable :: eletype

        !> mass of this element
    real(kind=wp), dimension(:), allocatable ::    atm_mass

        !> atomic charge of this element
    real(kind=wp), dimension(:), allocatable ::    atm_charge

    type(molecule), dimension(:), allocatable ::   uniq_mol

    logical :: charges_present = .false.

        !> Flag signifying the use of 'unique' charges for atoms in a molecular species, i.e.
        !> atoms whose charges do not reflect the 'global' values for each atomic species given
        !> at the top of the FIELD file
    logical, dimension(:), allocatable :: unique_charges

        !> Flag signifying the use of lambda-dependent charges for atoms in a molecular species, i.e.
        !> atoms whose charges depend on the Hamiltonian coupling parameter lambda (specifically the
        !> Couloumb-specific lambda, lambda_coul). Hence the charges of the atoms in the molecule do 
        !> not reflect the 'global' values for each atomic species given at the top of the FIELD file,
        !> nor charges given by the 'unique charges' functionality. In fact the charge for atom 'i' in
        !> the molecule will be 'lambda_coul*q_i', where 'q_i' is the charge for the atom on the template
        !> molecule in FIELD (possibly determined by the 'unique charges' functionality).
    logical, dimension(:), allocatable :: lambda_dep_charges


contains


!> read in the molecular species/atoms data from FIELD file & allocate the needed memory
subroutine readspecies()

    use kinds_f90
    use parse_module
    use atom_module, only : set_atom_type, set_atom_pos, set_atom_site, set_atom_spin
    use constants_module
    use bondlist_module
    use anglist_module
    use invlist_module
    use invlist_type
    use dihlist_type
    use dihlist_module
    use molecule_module
    use comms_mpi_module, only : master, idnode

    implicit none

    integer :: i, num, nmax, nread
    integer :: nang, nbond, ninv, ndih, j, k, atmtype
    integer, allocatable, dimension(:) :: atomi, atomj, atomk, atoml, pot

    real (kind = wp) :: pos(3), spin(3)

    type (bondlist), allocatable, dimension(:) :: bnds
    type (anglist), allocatable, dimension(:) :: angls
    type (invlist), allocatable, dimension(:) :: invsn
    type (dihlist), allocatable, dimension(:) :: dihrl

    character :: record*100, word*40, word1*40, symbol*8
    logical :: safe, finish, founda

    logical :: is_float

    nread = ufld

    call get_line(safe, nread, record)
    if (.not.safe) go to 1000

    call lower_case(record)
    call get_word(record, word)

    if (word(1:4) == 'atom') then

        call get_word(record, word)
        if (word(1:5) == "types") call get_word(record, word)
        num = nint(word_2_real(word))

    else

        call error(112)

    endif


    !allocate species arrays and read in elements
    allocate(element(num))
    allocate(eletype(num))
    allocate(atm_mass(num))
    allocate(atm_charge(num))

    number_of_elements = num;

    do i = 1, number_of_elements

        call get_line(safe, nread, record)
        if (.not.safe) go to 1000

        call get_word(record,element(i))
        call get_word(record,word)

        eletype(i) = get_species_type(word)

        call get_word(record, word)
        atm_mass(i) = word_2_real(word)

        call get_word(record, word)
        atm_charge(i) = word_2_real(word)

        charges_present = charges_present .or. (abs(atm_charge(i)) >= 1.0e-6)
        
    enddo

    ! molecule names
    call get_line(safe, nread, record)
    if (.not.safe) go to 1000

    num = 0

    call lower_case(record)
    call get_word(record, word)

    if (word(1:3) == 'mol') then

        call get_word(record, word)
        if (word(1:4) == "spec" .or. word(1:5) == "types") call get_word(record, word)
        num = nint(word_2_real(word))

    else

        call error(137)

    endif

    allocate(uniq_mol(num))
    allocate(bnds(num))
    allocate(angls(num))
    allocate(invsn(num))
    allocate(dihrl(num))

    allocate(unique_charges(num))
    unique_charges = .false.

    allocate(lambda_dep_charges(num))
    lambda_dep_charges = .false.

    
    number_of_molecules = num

    do i = 1, number_of_molecules

        call get_line(safe, nread, record)
        if (.not.safe) go to 1000

        call get_word(record,symbol)
        call set_molecule_name(uniq_mol(i), symbol)
        call set_molecule_label(uniq_mol(i), i)

        call get_line(safe, nread, record)
        if (.not.safe) go to 1000
        call lower_case(record)
        call get_word(record, word)

        uniq_mol(i)%is_field = .false.
        uniq_mol(i)%rigid_body = .false.
        uniq_mol(i)%exc_coul_ints = .false.

        if (word == 'maxatom') then 
        ! if maxatom is used no further information will be read in
        ! this assumes it is an ionic compound

            uniq_mol(i)%is_field = .true.

            call get_word(record, word)
            num = nint(word_2_real(word))
            call set_max_number_of_atoms(uniq_mol(i), num)

            if( master ) then 
                !write(uout,"(/,18('-'))")
                write(uout,"(/,1x,a,i3,3a,i10,a)") &
                      "reading species: ",i," - atomic field '", &
                      trim(uniq_mol(i)%molname),"' of ",num," atoms (max)"
                write(uout,"(/,1x,18('-'))")
            endif

        else if (word == 'atoms') then ! molecule data is read in

            call get_word(record, word)
            num = nint(word_2_real(word))
            call get_word(record, word)
            nmax = nint(word_2_real(word))

            if( master ) then 
                !write(uout,"(/,18('-'))")
                write(uout,"(/,1x,a,i3,3a,2(i10,a))") &
                      "reading species: ",i," - molecule '", &
                      trim(uniq_mol(i)%molname),"' of ",num," atoms (",nmax," max)"
                flush(uout)
            endif

            !call get_word(record, word)
            !if( len_trim(word) > 0 ) then
                !AB: take the template from CONFIG
            !end if

            !allocate arrays in molecule
            call alloc_mol_atms_arrays(uniq_mol(i), num, nmax, .false., .false.)

            !read in types and positions
            do j = 1, uniq_mol(i)%natom

                call get_line(safe, nread, record)
                if (.not.safe) go to 1000

                !call lower_case(record)
                call get_word(record, symbol)
                call get_word(record, word)

                !write(uout,*)'readspecies()::uniq_mol(i)%natom read: ',j,' ',word,' ',symbol

                !atmtype = get_species_type(word)
                !call get_word(record, word)

                !AB: word cannot be of zero length here (it would mean the reminder of record is empty)
                !if(len_trim(word) == 0) then

                !AB: check if the atom type is given or omitted (default is 'core')
                is_float = .false.
                if( indexnum(word,is_float) == 1 ) then !AB: word already contains the first numeric
                    word = "core"
                    atmtype = get_species_type(word)
                else
                    atmtype = get_species_type(word)
                    call get_word(record, word)
                endif

                founda = .false.
                do k = 1, number_of_elements

                    if(symbol == element(k) .and. atmtype == eletype(k)) then

                        call set_atom_type(uniq_mol(i)%atms(j), k, atm_charge(k), atm_mass(k), atmtype)
                        founda = .true.

                    endif

                enddo

                if(.not.founda) call error(108)
                
                !write(uout,*)'readspecies()::uniq_mol(i)%natom read: ',j,' ',atmtype,' ',symbol,' word = "',word,'"'
                !flush(uout)

                !AB: the first numeric has been extracted above
                !call get_word(record, word)
                pos(1) = word_2_real(word)
                call get_word(record, word)
                pos(2) = word_2_real(word)
                call get_word(record, word)
                pos(3) = word_2_real(word)

                call set_atom_pos(uniq_mol(i)%atms(j), pos)

                !TU: If the atom is of type spin then read the orientation, immediately after the positions (and
                !TU: before the site index)
                if( atmtype == ATM_TYPE_SPIN ) then

                    spin(1) = word_2_real(word)
                    call get_word(record, word)
                    spin(2) = word_2_real(word)
                    call get_word(record, word)
                    spin(3) = word_2_real(word)

                    call set_atom_spin(uniq_mol(i)%atms(j), spin)

                end if                   

                call get_word(record, word)

                !TU: Read the unique atomic charge if specified
                if( word(1:6) == "charge" ) then

                    unique_charges(i) = .true.

                    call get_word(record, word)
                    uniq_mol(i)%atms(j)%charge = word_2_real(word)

                    charges_present = charges_present .or. (abs(uniq_mol(i)%atms(j)%charge) >= 1.0e-6)

                    if(master) then
                   
                        write(uout,"(/,1x,a,i3,a,i3,a,f10.4)") "charges for all atoms with index ",j, &
                            " in mol. species ",i," will be set to ",uniq_mol(i)%atms(j)%charge

                    end if

                    call get_word(record, word)

                end if


                if( Index(word,'#')>0 ) word = ''
                call set_atom_site(uniq_mol(i)%atms(j), nint(word_2_real(word)))

            enddo

            !AB: set molecule total mass and charge
            call set_molecule_mass(uniq_mol(i))
            call set_molecule_charge(uniq_mol(i))

            !start processing the number of bonds etc
            finish = .true.
            do while (finish)

                call get_line(safe, nread, record)
                if (.not.safe) go to 1000

                call lower_case(record)
                call get_word(record, word)

                if(word(1:5) == 'bonds') then

                    call get_word(record, word)
                    nbond = nint(word_2_real(word))

                    !alloc enough space to read the bonds in
                    if (allocated(atomi)) deallocate(atomi)
                    if (allocated(atomj)) deallocate(atomj)
                    if (allocated(pot))   deallocate(pot)

                    allocate(atomi(nbond))
                    allocate(atomj(nbond))
                    allocate(pot(nbond))

                    !read bonds to temp storage
                    do j = 1, nbond

                        call get_line(safe, nread, record)
                        if (.not.safe) go to 1000

                        !call get_word(record, word)  ! miss out pot type in dlpoly

                        call get_word(record, word)
                        atomi(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomj(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        pot(j) = nint(word_2_real(word))

                    enddo

                    !add to the list
                    call alloc_bondlist_arrays(bnds(i), nbond)
                    call add_bondpairs(bnds(i), atomi, atomj, pot, nbond)

                    !create the bondlist for the molecule
                    call set_molecule_bondlist(uniq_mol(i), bnds(i))

                    !if( master ) then 
                    !    write(uout,"(/,/,10('-'),/,' bonds: ',/,10('-'))")
                    !    do j = 1, nbond
                    !       write(uout,"(3i10)")bnds(i)%bpair(j,:)
                    !    enddo
                    !endif

                else if(word(1:5) == 'angle') then

                    call get_word(record, word)
                    nang = nint(word_2_real(word))

                    if (allocated(atomi)) deallocate(atomi)
                    if (allocated(atomj)) deallocate(atomj)
                    if (allocated(atomk)) deallocate(atomk)
                    if (allocated(pot)) deallocate(pot)

                    allocate(atomi(nang))
                    allocate(atomj(nang))
                    allocate(atomk(nang))
                    allocate(pot(nang))

                    do j = 1, nang

                        call get_line(safe, nread, record)
                        if (.not.safe) go to 1000

                        !call get_word(record, word)  ! miss out pot type in dlpoly

                        call get_word(record, word)
                        atomj(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomi(j) = nint(word_2_real(word)) !AB: DL_POLY convention is to have the central atom index (i) in the middle
                        call get_word(record, word)
                        atomk(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        pot(j) = nint(word_2_real(word))

                    enddo

                    !add to the list
                    call alloc_anglist_arrays(angls(i), nang)
                    call add_triples(angls(i), atomi, atomj, atomk, pot, nang)

                    !copy the list to that of molecule
                    call set_molecule_anglist(uniq_mol(i), angls(i))

                    !if( master ) then 
                    !    write(uout,"(/,/,10('-'),/,' angles: ',/,10('-'))")
                    !    do j = 1, nang
                    !       write(uout,"(4i10)")angls(i)%apair(j,:)
                    !    enddo
                    !endif

                else if(word(1:5) == 'inver') then

                    call get_word(record, word)
                    ninv = nint(word_2_real(word))

                    if (allocated(atomi)) deallocate(atomi)
                    if (allocated(atomj)) deallocate(atomj)
                    if (allocated(atomk)) deallocate(atomk)
                    if (allocated(atoml)) deallocate(atoml)
                    if (allocated(pot)) deallocate(pot)

                    allocate(atomi(ninv))
                    allocate(atomj(ninv))
                    allocate(atomk(ninv))
                    allocate(atoml(ninv))
                    allocate(pot(ninv))

                    do j = 1, ninv

                        call get_line(safe, nread, record)
                        if (.not.safe) go to 1000

                        call get_word(record, word)
                        atomi(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomj(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomk(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atoml(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        pot(j) = nint(word_2_real(word))

                    enddo

                    !add to the list
                    call alloc_invlist_arrays(invsn(i), ninv)
                    call add_quads(invsn(i), atomi, atomj, atomk, atoml, pot, ninv)

                    !copy the list to that of molecule
                    call set_molecule_invlist(uniq_mol(i), invsn(i))

                    !if( master ) then 
                    !    write(uout,"(/,/,10('-'),/,' inversions: ',/,10('-'))")
                    !    do j = 1, ninv
                    !       write(uout,"(5i10)")invsn(i)%ivpair(j,:)
                    !    enddo
                    !endif

                else if(word(1:5) == 'dihed') then

                    call get_word(record, word)
                    ndih = nint(word_2_real(word))

                    if (allocated(atomi)) deallocate(atomi)
                    if (allocated(atomj)) deallocate(atomj)
                    if (allocated(atomk)) deallocate(atomk)
                    if (allocated(atoml)) deallocate(atoml)
                    if (allocated(pot)) deallocate(pot)

                    allocate(atomi(ndih))
                    allocate(atomj(ndih))
                    allocate(atomk(ndih))
                    allocate(atoml(ndih))
                    allocate(pot(ndih))

                    do j = 1, ndih

                        call get_line(safe, nread, record)
                        if (.not.safe) go to 1000

                        call get_word(record, word)
                        atomi(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomj(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atomk(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        atoml(j) = nint(word_2_real(word))
                        call get_word(record, word)
                        pot(j) = nint(word_2_real(word))

                    enddo

                    !add to the list
                    call alloc_dihlist_arrays(dihrl(i), ndih)
                    call add_dih_quads(dihrl(i), atomi, atomj, atomk, atoml, pot, ndih)

                    !copy the list to that of molecule
                    call set_molecule_dihlist(uniq_mol(i), dihrl(i))

                    !if( master ) then 
                    !    write(uout,"(/,/,10('-'),/,' dihedrals: ',/,10('-'))")
                    !    do j = 1, ndih
                    !       write(uout,"(5i10)")dihrl(i)%dipair(j,:)
                    !    enddo
                    !endif

                else if (word(1:5) == 'exclu') then

                    uniq_mol(i)%exc_coul_ints = .true.

                else if (word(1:5) == 'rigid') then

                    uniq_mol(i)%rigid_body = .true.

                else if (word(1:12) ==  'lambdacharge') then

                    lambda_dep_charges(i) = .true.

                    if(master) then
                   
                        write(uout,"(/,1x,a,i3,a)") "charges for all atoms in mol. species ",i, &
                            " will be lambda-dependent"


                    end if

                else if(word(1:5) == 'finis') then

                    finish = .false.
                    
                    if( uniq_mol(i)%blist%npairs < 1 .and. .not.uniq_mol(i)%rigid_body ) then 

                        call cry(uout,'(/,a)', &
                        " WARNING: molecule '"//trim(adjustL(uniq_mol(i)%molname))//&
                        &"' has no bond and is not 'rigid', so will be treated as 'atomic field'!",0)

                    endif
                    
                    if(master) write(uout,"(/,1x,18('-'))")

                else 

                    call cry(uout,'', &
                    "ERROR: unexpected keyword '"//trim(adjustL(word))//&
                    &"' in specs for molecule '"//trim(adjustL(symbol))//"'!!!",167)

                    !call error(167)

                endif

            enddo

        !else if(word(1:5) == 'finis') then
        !    finish = .false.
        else

            call cry(uout,'', &
            "ERROR: expected keyword 'MAXATOM' or 'ATOMS' for molecule '"//trim(adjustL(symbol))//&
            &"' but got '"//trim(adjustL(word))//"'!!!",167)

            !call error(167)

        endif

    enddo

    call printspecies()

    if (allocated(atomi)) deallocate(atomi)
    if (allocated(atomj)) deallocate(atomj)
    if (allocated(atomk)) deallocate(atomk)
    if (allocated(pot)) deallocate(pot)
    if (allocated(bnds)) deallocate(bnds)
    if (allocated(angls)) deallocate(angls)

    return

1000 call error(113)

end subroutine


!**
!  function to print species data
!
subroutine printspecies()
    use constants_module
    use comms_mpi_module, only : idnode, master

    implicit none

    integer :: i, nitem, j

    !if(idnode /= 0) return
    if( .not. master ) return

    !write(uout,"(/,/,1x,'----------------------------------------------------------------------------------------------------')")
    write(uout,"(/,/,1x,100('='))")
    write(uout,"(' atom data :')")
    write(uout,"(1x,100('='))")
    !write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")

    write(uout,"(1x,'element           mass             charge')")
    write(uout,"(1x,50('-'))")

    do i = 1, number_of_elements

        write(uout,'(2x,a8,2(5x,f10.4))') element(i),atm_mass(i), atm_charge(i)

    enddo

    !write(uout,"(/,/,1x,'----------------------------------------------------------------------------------------------------')")
    write(uout,"(/,/,1x,100('='))")
    write(uout,"(' molecule data :')")
    write(uout,"(1x,100('='))")
    !write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")

    write(uout,"(1x,'molecule name / rigid / exclude VDW & Coulomb')")

    do  i = 1, number_of_molecules

        write(uout,"(1x,50('-'))")
        write(uout,'(1x,a,a8,2(a,l))')"'",uniq_mol(i)%molname, &
                              "'    /   ",uniq_mol(i)%rigid_body, &
                                 "   /  ",uniq_mol(i)%exc_coul_ints
        write(uout,"(1x,50('-'))")

        nitem = 0
        if( allocated(uniq_mol(i)%blist%bpair) ) nitem = size(uniq_mol(i)%blist%bpair,1)

        if( nitem > 0 ) then

            write(uout,"(/,10('-'),/,' bonds: ',/,10('-'))")

            do j = 1, nitem
               write(uout,"(3i10)")uniq_mol(i)%blist%bpair(j,:)
            enddo

        endif

        nitem = 0
        if( allocated(uniq_mol(i)%alist%apair) ) nitem = size(uniq_mol(i)%alist%apair,1)

        if( nitem > 0 ) then

            write(uout,"(/,10('-'),/,' angles: ',/,10('-'))")

            do j = 1, nitem
               write(uout,"(4i10)")uniq_mol(i)%alist%apair(j,:)
            enddo

        endif

        nitem = 0
        if( allocated(uniq_mol(i)%dlist%dipair) ) nitem = size(uniq_mol(i)%dlist%dipair,1)

        if( nitem > 0 ) then

            write(uout,"(/,10('-'),/,' dihedrals: ',/,10('-'))")

            do j = 1, nitem
               write(uout,"(5i10)")uniq_mol(i)%dlist%dipair(j,:)
            enddo

        endif

        nitem = 0
        if( allocated(uniq_mol(i)%ilist%ivpair) ) nitem = size(uniq_mol(i)%ilist%ivpair,1)

        if( nitem > 0 ) then

            write(uout,"(/,10('-'),/,' inversions: ',/,10('-'))")

            do j = 1, nitem
               write(uout,"(5i10)")uniq_mol(i)%ilist%ivpair(j,:)
            enddo

        endif

        write(uout,*)
        !write(uout,"(/,1x,50('-'))")

    enddo

end subroutine

integer function get_species_type(wrd)

    use constants_module
    use parse_module

    implicit none

    character, intent(in) :: wrd*40

    integer :: typ
    character :: word*40

    word = wrd
    call lower_case(word)
    
    if (word(1:4) == "core" .or. word(1:1) == "c") then

        typ = ATM_TYPE_CORE

    !TU: Note that the check for type 'spin' must proceed that for 'semi' otherwise word='s' will
    !TU: be interpreted as type 'semi'
    else if (word(1:4) == 'spin' .or. word(1:1) == "p") then

        typ = ATM_TYPE_SPIN
        
    else if (word(1:4) == "semi" .or. word(1:1) == "s") then
    
        typ = ATM_TYPE_SEMI

    else if (word(1:5) == 'metal' .or. word(1:1) == 'm') then

        typ = ATM_TYPE_METAL
        
    else
        call cry(uout,'', &
                 "ERROR: unrecognised element type: '"//word//"'!!!",0)

        call error(138)

    endif

    get_species_type = typ

end function


!> Checks whether or not the character string in the argument is valid, i.e.
!> that it corresponds to a element/atom type in DL_MONTE. For instance 'c'
!> and 'core' are valid, since they correspond to the 'core' atom type. The
!> function returns 'true' if it is valid, and false if it is not.
logical function valid_species_type(wrd)

    use parse_module
    
    implicit none

        !> Character string to be checked
    character, intent(in) :: wrd*40

    character :: word*40
    
    valid_species_type = .false.
    
    word = wrd
    call lower_case(word)

    if( word(1:4) == "core" .or. word(1:1) == "c" .or. &
        word(1:4) == 'spin' .or. word(1:1) == "p" .or. &
        word(1:4) == "semi" .or. word(1:1) == "s" .or. &
        word(1:5) == 'metal' .or. word(1:1) == 'm' ) then

        valid_species_type = .true.

    end if
    
end function valid_species_type


character function set_species_type(id)

    use constants_module

    implicit none

    integer, intent(in) :: id

    character*1 tag

    if (id == ATM_TYPE_CORE) then

        tag = "c"

    else if (id == ATM_TYPE_SEMI) then
    
        tag = "s"

    else if (id == ATM_TYPE_METAL) then

        tag = "m"

    else if (id == ATM_TYPE_SPIN) then

        tag = "p"
        
    else

        write(uout,*)'*** trying to set wrong element type'

    endif

    set_species_type = tag

end function

subroutine check_for_charge()

    implicit none

    if (any(atm_charge > 1.0e-6_wp)) call error(329)
    
end subroutine


!> Checks if the atomic species in the arguments was defined in FIELD
!> already. The function returns 'true' if it is already defined,
!> and false otherwise
logical function check_atom_defined(ele, typ)

        !> The character label / element name for the atomic species to
        !> be checked
    character*8, intent(in) :: ele

        !> The integer type label (corresponding to, e.g. core) for
        !> atomic species to be checked
    integer, intent(in)  :: typ

    integer :: j

    check_atom_defined = .false.
    
    do j = 1, number_of_elements
        
        if( element(j) == ele  .and. eletype(j) == typ ) then
            check_atom_defined = .true.
        end if
        
    end do

end function check_atom_defined




end module


