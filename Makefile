#==============================================
# Makefile for DL_MONTE_2.01 (serial version)
# Author: Dr Andrey Brukhno (c) May 2016
#==============================================

SHELL=/bin/sh

.SUFFIXES:
.SUFFIXES: .f90 .f .o

SRLDIR="./SERIAL"

BINROOT="./bin"

##TYPE=master
##TYPE=parallel
TYPE=serial

##EX=DLMONTE-PRL.X
EX=DLMONTE-SRL.X

EXE=$(BINROOT)/$(EX)

FC=undefined
LD=undefined

# Define object files
#=====================================================================

OBJ_CONST = kinds_f90.o constants_module.o 

OBJ_SRL = mpi_module.o comms_mpi_module-SRL.o

OBJ_PRL = comms_mpi_module-PRL.o

OBJ_MOD = comms_omp_module.o  \
	signals.o error.o warning.o parse_module.o parallel_loop_module.o arrays_module.o \
	bondlist_type.o anglist_type.o atom_type.o nbrlist_type.o dihlist_type.o invlist_type.o  \
	coul_type.o lattice_type.o thblist_type.o molecule_type.o control_type.o cell_list_type.o config_type.o \
	statistics_type.o random_module.o fed_interface_type.o \
	psmc_control_type.o 

OBJ_ALL = dcd_format_module.o dihlist_module.o invlist_module.o inversion_module.o bondlist_module.o cell_list_module.o \
	 anglist_module.o nbrlist_module.o atom_module.o molecule_module.o species_module.o bond_module.o gb_potential_module.o\
	 metpot_module.o lattice_module.o coul_module.o config_module.o thblist_module.o thbpotential_module.o tersoff_module.o \
	 lambda_module.o vdw_module.o slit_module.o external_potential_module.o cell_module.o cell_list_wrapper_module.o angle_module.o dihedral_module.o field.o psmc_module.o \
	 orientation_module.o fed_calculus_module.o lc_order_module.o fed_order_module.o fed_interface_module.o control_module.o \
	 statistics_module.o rep_exchange_module.o spin_module.o mc_moves.o gcmc_moves.o gibbs_moves.o vol_moves.o psmc_moves.o lambda_moves.o \
     yamldata_module.o montecarlo_module.o traj_convert.o dl_monte.o


# Define MPI-SERIAL files
#=====================================================================

FILES_SERIAL = mpif.h 


# Examine targets manually
#=====================================================================

.PHONY: all
all:
	@echo
	@echo "You MUST specify a target platform!"
	@echo
	@echo "Please examine Makefile for permissible targets!"
	@echo
	@echo "If no target suits your system create your own"
	@echo "using the generic target template provided in"
	@echo "this Makefile at entry 'unknown_platform:'."
	@echo

# Fetch MPI-SERIAL subroutines
#=====================================================================

$(FILES_SERIAL):
	$(MAKE) links_serial

.PHONY: links_serial
links_serial:
	@for file in ${FILES_SERIAL} ; do \
	  if [ -h "$$file" ]; then \
	    rm -f $$file ; \
	  fi ; \
	  echo "linking to $(SRLDIR)/$$file" ; \
	  ln -s $(SRLDIR)/$$file $$file ; \
	done

.PHONY: clean_serial
clean_serial:
	@for file in ${FILES_SERIAL} ; do \
	  if [ -h "$$file" ]; then \
	    echo "removing $$file (link)" ; \
	    rm -f $$file ; \
	  fi ; \
	done ; \
	for file in ${OBJ_SRL} ; do \
	  if [ -e "$$file" ]; then \
	    echo "removing $$file (object) *** IF JUST SWITCHED SERIAL/PARALLEL NEED TO CLEAN ALL ***" ; \
	    rm -f $$file ; \
	  fi ; \
	done

.PHONY: clean_parallel
clean_parallel:
	@for file in ${OBJ_PRL} ; do \
	  if [ -e "$$file" ]; then \
	    echo "removing $$file (object) *** IF JUST SWITCHED PARALLEL/SERIAL NEED TO CLEAN ALL ***" ; \
	    rm -f $$file ; \
	  fi ; \
	done

# Clean up the source directory
#=====================================================================

.PHONY: clean
clean:  clean_serial clean_parallel
	rm -f *.o *.mod *.i

# Generic target template
#=====================================================================
.PHONY: unknown_platform
unknown_platform:
	$(MAKE) LD="path to Fortran 90 Linker-loaDer" \
	LDFLAGS="appropriate flags for LD" \
	FC="path to Fortran 90 compiler" \
	FCFLAGS="appropriate flags for FC" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

# System specific targets follow:
#=====================================================================

#====================== LINUX GFORTRAN ===============================
.PHONY: gf
gf: gfortran

.PHONY: gfort
gfort: gfortran

.PHONY: gfdo
gfdo: gfortrando

.PHONY: gfortdo
gfortdo: gfortrando

.PHONY: gfdbg
gfdbg: gfortrandbg

.PHONY: gfortdbg
gfortdbg: gfortrandbg

.PHONY: gfdbg
gfdebug: gfortrandebug

#======= LINUX Cray - ARCHER 2 ===================================
# Requires the PrgEnv-cray module (which is the default)
.PHONY: archer2
archer2:
	 $(MAKE) LD="ftn -o " \
	 LDFLAGS=" " \
	 FC="ftn -c -eZ -U DEBUG -U GNUFORTRAN" \
	 FCFLAGS=" -O3" \
	 EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - ARCHER 2 ===================================
# Requires the PrgEnv-gnu module to be loaded: try 'module swap PrgEnv-cray PrgEnv-gnu'
.PHONY: archer2
archer2gnu:
	 $(MAKE) LD="ftn -o " \
	 LDFLAGS=" " \
	 FC="ftn -c -cpp -U DEBUG -D GNUFORTRAN" \
	 FCFLAGS=" -O3" \
	 EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX CRAY - Isambard =======================================
.PHONY: cray
cray:
	 $(MAKE) LD="ftn -o " \
	 LDFLAGS="-O2 -en" \
	 FC="ftn -c -U DEBUG -U GNUFORTRAN" \
	 FCFLAGS="-O2 -en -eZ" \
	 EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX ARM - Isambard ========================================
.PHONY: arm
arm:
	 $(MAKE) LD="armflang -o " \
	 LDFLAGS=" " \
	 FC="armflang -c -cpp -U DEBUG -U GNUFORTRAN" \
	 FCFLAGS="-Ofast -march=armv8.1-a -mcpu=native -ffast-math " \
	 EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - Isambard ===================================
.PHONY: isambard-gfortran
isambard-gfortran:
	 $(MAKE) LD="gfortran -o " \
	 LDFLAGS=" " \
	 FC="gfortran -c -cpp -U DEBUG -D GNUFORTRAN -ffpe-summary=none" \
	 FCFLAGS="-Ofast -march=armv8.1-a -mcpu=thunderx2t99 -mtune=thunderx2t99 -C" \
	 EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - OPTIMUM (quickest) =========================
.PHONY: gfortran
gfortran:
	$(MAKE) LD="gfortran -o " \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -U DEBUG -D GNUFORTRAN -ffpe-summary=none" \
	FCFLAGS="-O3 -C" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - OPTIMUM (quickest) PROFILING =========================
.PHONY: gfortranprof
gfortranprof:
	$(MAKE) LD="gfortran -o " \
	LDFLAGS="-pg " \
	FC="gfortran -c -cpp -U DEBUG -ffpe-summary=none" \
	FCFLAGS="-O3 -C -pg " \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======  LINUX GFORTRAN - REPRODUCIBLE ON DIFFERENT PLATFORMS ========
.PHONY: gfortranrepro
gfortranrepro:
	$(MAKE) LD="gfortran -o " \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -U DEBUG -D GNUFORTRAN -ffpe-summary=none" \
	FCFLAGS="-frounding-math -fsignaling-nans -fno-unsafe-math-optimizations -C" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - OPTIMUM + DEBUG OUTPUT =====================
.PHONY: gfortran
gfortrando:
	$(MAKE) LD="gfortran -o " \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -D DEBUG -D GNUFORTRAN -ffpe-summary=none" \
	FCFLAGS="-O3 -C" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - DEBUG OPTIONS + DEBUG OUTPUT (slow) ========
#	FCFLAGS="-fbounds-check -g "
.PHONY: gfortrandbg
gfortrandbg:
	$(MAKE) LD="gfortran -o" \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -D DEBUG -D GNUFORTRAN" \
	FCFLAGS="-g -fbounds-check -Wall -fbacktrace -fcheck=all -ffpe-trap=zero,overflow,underflow" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - EXHAUSTIVE DEBUG OPTIONS + DEBUG OUTPUT (slow) ========
#	FCFLAGS="-fbounds-check -g "
.PHONY: gfortrandebug
gfortrandebug:
	$(MAKE) LD="gfortran -o" \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -D DEBUG -D GNUFORTRAN" \
	FCFLAGS="-Wall -g -fbounds-check -fbacktrace -ffpe-trap=invalid,zero,overflow,underflow \
		  -finit-integer=-9999 -finit-real=nan -finit-logical=true \
		  -finit-character=42 -fdump-core -fcheck=all -frecord-gcc-switches" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)
	
#======= LINUX GFORTRAN - EXHAUSTIVE DEBUG OPTIONS + DEBUG OUTPUT (slow) ========
#	FCFLAGS="-fbounds-check -g "
.PHONY: gfddd
gfddd:
	$(MAKE) LD="gfortran -o" \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -D DEBUG -D GNUFORTRAN" \
	FCFLAGS="-O3 -Wall -Wextra -pedantic -g -fbounds-check -fbacktrace \
	          -finit-integer=-9999 -finit-real=nan -finit-logical=true \
	          -finit-character=42 -fdump-core -fcheck=all \
	          -ffpe-trap=invalid,zero,overflow -frecord-gcc-switches" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - OPENMP =====================================
.PHONY: gfortranmp
gfortranmp:
	$(MAKE) LD="gfortran -fopenmp -o " \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -U DEBUG -D GNUFORTRAN" \
	FCFLAGS="-O3 -fopenmp " \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======= LINUX GFORTRAN - OPENMP + DEBUG OPTIONS (slow) ==============
.PHONY: gfortranmpdbg
gfortranmpdbg:
	$(MAKE) LD="gfortran -fopenmp -o " \
	LDFLAGS=" " \
	FC="gfortran -c -cpp -D DEBUG -D GNUFORTRAN" \
	FCFLAGS="-g -C -fopenmp -ffree-form -ffree-line-length-none" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#====================== PGI ======================================
.PHONY: pgi
pgi:
	$(MAKE) LD="pgf90 -o" \
	LDFLAGS=" " \
	FC="pgf90 -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======================= intel compiler =========================
.PHONY: intelmp
intelmp:
	$(MAKE) LD="ifort -openmp -o " \
	LDFLAGS=" " \
	FC="ifort -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3 -openmp " \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)

#======================= intel compiler =========================
.PHONY: intel
intel:
	$(MAKE) LD="ifort -o " \
	LDFLAGS=" " \
	FC="ifort -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3 -auto -m64 -xHost" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)


#======================= intel compiler =========================
.PHONY: inteldbg
inteldbg:
	$(MAKE) LD="ifort -o " \
	LDFLAGS=" " \
	FC="ifort -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3 -auto -m64 -xHost -g -qopt-report=4" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)


#======================= lf95 compiler =========================
.PHONY: lf95
lf95:
	$(MAKE) LD="lf95 -o" \
	LDFLAGS=" " \
	FC="lf95 -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)


#======================= nag f95 compiler =========================
.PHONY: nag
nag:
	$(MAKE) LD="f95 -o" \
	LDFLAGS=" " \
	FC="f95 -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-O3" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)


#======================= nag f95 compiler =========================
.PHONY: nagdbg
nagdbg:
	$(MAKE) LD="f95 -o" \
	LDFLAGS=" " \
	FC="f95 -c -cpp -U DEBUG -U GNUFORTRAN" \
	FCFLAGS="-g -C" \
	EX=$(EX) BINROOT=$(BINROOT) $(TYPE)



# Default code
#=====================================================================

.PHONY: parallel
parallel: clean_serial master

.PHONY: serial
serial: clean_parallel message_serial check_serial check $(OBJ_CONST) $(OBJ_SRL) $(OBJ_MOD) $(OBJ_ALL)
	$(LD) $(EXE) $(LDFLAGS) $(OBJ_CONST) $(OBJ_SRL) $(OBJ_MOD) $(OBJ_ALL)

.PHONY: master
master: message check $(OBJ_CONST) $(OBJ_PRL) $(OBJ_MOD) $(OBJ_ALL)
	$(LD) $(EXE) $(LDFLAGS) $(OBJ_CONST) $(OBJ_PRL) $(OBJ_MOD) $(OBJ_ALL)

# Message
.PHONY: message
message:
	@echo
	@echo "DL_MONTE_2 compilation in parallel mode (Makefile_PRL)"
	@echo

.PHONY: message_serial
message_serial:
	@echo
	@echo "DL_MONTE_2 compilation in serial mode (Makefile_SRL)"
	@echo

# Check that a platform has been specified
.PHONY: check
check:
	@if test "${FC}" = "undefined"; then \
	echo; echo "*** Fortran 90 compiler unspecified!"; \
	echo; echo "*** Please edit your Makefile entries!"; \
	echo; exit 99; \
	fi; \
	\
	if test "${LD}" = "undefined"; then \
	echo; echo "*** Fortran 90 Linker-loaDer unspecified!"; \
	echo; echo "*** Please edit your Makefile entries!"; \
	echo; exit 99; \
	fi; \
	\
	mkdir -p $(BINROOT) ; touch dl_monte.f90

# Check that a platform has been specified
.PHONY: check_serial
check_serial: links_serial
	@for file in ${FILES_SERIAL} ; do \
	  if [ ! -e "$$file" ]; then \
	    echo; echo "*** Could not find file '$$file' needed for serial compilation!"; \
	    echo; echo "*** Please make sure the file is present!"; \
	    echo; exit 99; \
	  fi ; \
	done 

# Declare rules
#=====================================================================

.f90.o:
	$(FC) $(FCFLAGS) $*.f90

# Declare dependencies
#=====================================================================

$(OBJ_ALL): $(OBJ_SRL) $(OBJ_MOD) $(OBJ_CONST) 
