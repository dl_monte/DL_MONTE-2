! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

module rep_exchange_module

    use kinds_f90
    use control_type
    use constants_module, only : uout, BOLTZMAN
    use comms_mpi_module, only : master, idnode, gsync_world, gsum_world, & !grsum_all_vector, &
                                 msg_send_blocked, msg_receive_blocked, msg_bcast_world, exit_mpi_comms, &
                                 map_temp_down, map_temp_up
    use parallel_loop_module, only : idgrp, wkgrp_size, base_temp, repx_dtemp, open_nodefiles

    implicit none

    integer, parameter :: step = 4

        !> attempetd replica exchange moves
    integer, save :: attempted_rep_exch = 0

        !> rejected replica exchange moves
    integer, save :: rejected_rep_exch = 0

        !> successful replica exchange moves
    integer, save :: successful_rep_exch(2) = 0

    !AB: the base (lowest) temperature
    !real (kind = wp) :: base_temp

        !> storage for inverse temperatures (ordered; to be replaced by parm(:) array from FED modules)
    real (kind = wp), allocatable :: repx_betas(:)

        !> temperature/replicas ids by idgrp & vice versa
    integer, allocatable :: repx_idrep(:), repx_idgrp(:)


contains


subroutine initialise_rep_exchange(job, beta)

    use kinds_f90

    use control_type
    use arrays_module

    implicit none

    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta

    integer :: fail(3), ierr

    fail = 0

    if( reallocating_safe(repx_betas,job%numreplicas) .and. & 
        reallocating_safe(repx_idrep,job%numreplicas) .and. & 
        reallocating_safe(repx_idgrp,job%numreplicas) ) then

        repx_betas = 0.0_wp
        repx_idrep = 0
        repx_idgrp = 0

    else

        fail(1) = 1

    end if

    call gsum_world(fail)
    if (any(fail /= 0)) call error(354)

    if( base_temp < 1.0e-10_wp ) base_temp = 1.0e-10_wp
    !if( idgrp == 0 ) base_temp = job%systemp

    !AB: initially temperatures are set in parallel_loop_module.f90 as
    !AB: systemp = base_temp + (repexch_inc * idgrp)
    !AB: i.e. linearly increasing with idgrp

    repx_betas(idgrp+1) = 1.0_wp / ( BOLTZMAN * ( base_temp + repx_dtemp * real(idgrp,wp) ) )
    repx_idrep(idgrp+1) = idgrp
    repx_idgrp(idgrp+1) = idgrp

    !AB: collect & share
    call gsum_world(repx_betas)
    call gsum_world(repx_idrep)
    call gsum_world(repx_idgrp)

    if( wkgrp_size > 1 ) then

        repx_betas = repx_betas / real(wkgrp_size,wp)
        repx_idrep = repx_idrep / wkgrp_size
        repx_idgrp = repx_idgrp / wkgrp_size

    end if

!debugging-<<<
    !if(master) write(uout,*)'RE set-up on node ',idnode,' - initialised (3) in workgroup ',repx_idgrp(idgrp+1)

    if( abs(beta - repx_betas(idgrp+1) ) > 1.0e-10_wp ) then

        if(master) &
            write(uout,'(/,/,1x,2(a,f20.10),a,i3,/)') &
                  "ERROR: setting RE 1/kT range failed - beta = ", beta, &
                  " =/= ", repx_betas(idgrp+1), " in workgroup ",idgrp

        !AB: flush the uout buffer to the disk (due to re-opening the file)
        !call open_nodefiles('OUTPUT', uout, ierr, 'add')

        fail(2) = 1

    else if( idgrp /= repx_idgrp(idgrp+1) .or. idgrp /= repx_idrep(idgrp+1) ) then

        if(master) &
            write(uout,'(/,/,1x,3(a,i3),a,/)') &
                  "ERROR: initial RE setup is not valid - idgrp = ", idgrp, &
                  " =/= ", repx_idgrp(idgrp+1), " (", repx_idrep(idgrp+1),") "

        !AB: flush the uout buffer to the disk (due to re-opening the file)
        !call open_nodefiles('OUTPUT', uout, ierr, 'add')

        fail(3) = 1

    end if

    call gsum_world(fail)
    if (any(fail /= 0)) call cry(uout,'(/,1x,a,/)', &
                                "ERROR: initialise_rep_exchange - gsum_world failed !!!",999)

    if(master) write(uout,*)
    if(master) write(uout,*)'RE set-up on node ',idnode,' - initialised (4) in workgroup ',repx_idgrp(idgrp+1)
!debugging->>>

    flush(uout)

end subroutine initialise_rep_exchange


!TU-: Currently this is not called anywhere; it is in development
!AB: version based on swapping pairs of T (beta) values (it gets very messy when it comes to collecting stats!)
subroutine rep_exch_beta(ib, job, energytot, beta, betainv)

    use control_type
    use random_module, only : duni

    integer, intent(in) :: ib

    type(control), intent(inout) :: job

    real(kind = wp), intent(in) :: energytot(:)

    real(kind = wp), intent(inout) :: beta, betainv

    real(kind = wp) :: repx_total_energy(job%numreplicas) ! total energies of system and beta for each replica

    integer ::  id_inc(job%numreplicas) ! T increments (+/-1*dT) by idgrp indices

    real(kind = wp) :: eng1, eng2, b1, b2, delta, deltav, rn!, vol

    integer :: num_reps, idgrp1, idrep, idrep1, my_inc, ig, ir, fail(3)

    call gsync_world()

    num_reps = job%numreplicas

    idgrp1 = idgrp+1
    idrep  = repx_idrep(idgrp1)
    idrep1 = idrep+1

!debugging-<<<
    fail = 0
    !if( master ) then

    if( abs(beta - repx_betas(idrep1) ) > 1.0e-10_wp ) then

        if(master) write(uout,'(/,/,1x,2(a,f20.10),2(a,i3),/)') &
              "ERROR: current RE 1/kT value is off - beta = ", beta, &
              " =/= ", repx_betas(idrep1), " (", idrep,") in workgroup ",idgrp

        fail(1) = 1

    else if( idgrp /= repx_idgrp(idrep1) .or. idrep /= repx_idrep(idgrp1) ) then

        if(master) write(uout,'(/,/,1x,3(a,i3),a,/)') &
              "ERROR: current RE state is not valid - idgrp = ", idgrp, &
              " =/= ", repx_idgrp(idrep1), " (", repx_idrep(idgrp1),") "

        fail(2) = 1

    end if

    !end if

    !call gsum_world(fail)
    !if (any(fail /= 0)) call cry(uout,'(/,1x,a,/)', &
    !                            "ERROR: rep_exch_beta - gsum_world failed !!!",999)

    !if( master ) then
    !    write(uout,*)
    !    write(uout,*)'RE prepared on node  ',idgrp,' - current rep set(idgrp): ',repx_idrep(:)
    !    write(uout,*)'RE prepared on node  ',idgrp,' - current grp set(idrep): ',repx_idgrp(:)
    !end if

!debugging->>>

    !total energy & beta arrays: set to zero, allocate for workgroups then use gsum to get this on all nodes
    repx_total_energy = 0.0_wp

    repx_total_energy(idrep1) = energytot(ib)
    !repx_total_energy(idgrp1) = energytot(ib)

    !AB: collect and share all the energies by idrep (in T order)
    call gsum_world(repx_total_energy)
    repx_total_energy = repx_total_energy / real(wkgrp_size,wp)

!debugging-<<<
    if( master ) then

        if( abs(energytot(ib) - repx_total_energy(idrep1) ) > job%checkenergyprec ) then

            write(uout,'(/,/,1x,2(a,f20.10),2(a,i3),/)') &
                  "ERROR: current RE energy value is off - IN = ", energytot(ib), &
                  " =/= ", repx_total_energy(idrep1), " (", idrep,") in workgroup ",idgrp

            fail(3) = 1

            write(uout,*)
            write(uout,*)'RE prepared on node  ',idgrp,' - current energy (idrep): ',repx_total_energy/job%energyunit
            !write(uout,*)'RE prepared on node  ',idgrp,' - current energy (idgrp): ',repx_total_energy/job%energyunit

        end if

    end if

    call gsum_world(fail)
    if (any(fail /= 0)) call cry(uout,'(/,1x,a,/)', &
                                "ERROR: rep_exch_beta - gsum_world failed !!!",999)
!debugging->>>

    id_inc = 0
    my_inc = 0

    !AB: advance randmon number selection to preserve the random sequence (for same workgroup!)
    rn = duni()

    if( idnode == 0 ) then

        my_inc = 1
        if( rn < 0.5 ) my_inc =-1

        if( my_inc > 0 ) then
        !AB: prepare the "even-up/odd-down" T-swaps (the last even, if any, won't swap)

            !AB: loop over idrep+1, T (beta) indices/replicas (NOT idgrp!)
            do ir = 1, num_reps-1, 2
               id_inc(ir)   =  my_inc
               id_inc(ir+1) = -my_inc
            end do

        else
        !AB: prepare the "even-down/odd-up" T-swaps (the first even, i.e. idrep=0, and the last odd, if any, won't swap)

            !AB: loop over idrep+1, T (beta) indices/replicas (NOT idgrp!)
            do ir = 2, num_reps-1, 2
               id_inc(ir)   = -my_inc
               id_inc(ir+1) =  my_inc
            end do

        end if

        my_inc = 0

    end if

    call msg_bcast_world(id_inc, num_reps)

!debugging-<<<
    !if( master ) then
    !    write(uout,*)
    !    write(uout,*)'RE prepared on node  ',idgrp,' - current inc set(idrep): ',id_inc(:)
    !end if
!debugging->>>

    my_inc = id_inc(idrep1)

    id_inc = 0

    !AB: advance randmon number selection to preserve the random sequence (for same workgroup!)
    rn = duni()

    if( my_inc > 0 ) then

        if( master ) then

            attempted_rep_exch = attempted_rep_exch + 1

            !write(uout,*)'RE attempted on node ',idgrp,' - swapping replicas: ',idrep,' <-> ',idrep1,' (up)*'

            b1   = repx_betas(idrep1)
            b2   = repx_betas(idrep1+1)

            eng1 = repx_total_energy(idrep1)
            eng2 = repx_total_energy(idrep1+1)

            delta = exp(-((b1 - b2) * (eng2 - eng1)))

            if( rn < delta ) then 

                id_inc(idrep1)   = 1
                id_inc(idrep1+1) =-1

            else

                rejected_rep_exch = rejected_rep_exch + 1

            end if

        end if

!debugging-<<<
    !else if( my_inc < 0 ) then
    !    if( master ) & 
    !        write(uout,*)'RE attempted on node ',idgrp,' - swapping replicas: ',idrep,' <-> ',idrep-1,' (dn)'
    !    !attempted_rep_exch = attempted_rep_exch + 1
!debugging->>>
    end if

    call gsum_world(id_inc)

!debugging-<<<
    !if( master ) then
    !    write(uout,*)
    !    write(uout,*)'RE accepted on node  ',idgrp,' - current acc set(idrep): ',id_inc(:)
    !end if
!debugging->>>

    !AB: loop over idrep+1, T (beta) indices/replicas
    do ir = 1,num_reps

       ig = repx_idgrp(ir)+1
       repx_idrep(ig) = repx_idrep(ig) + id_inc(ir)

    end do 

    !AB: loop over idgrp+1, workgroup indices
    do ig = 1,num_reps

       repx_idgrp(repx_idrep(ig)+1) = ig-1

    end do 

!debugging-<<<
    !if( master ) then
    !    write(uout,*)
    !    write(uout,*)'RE accepted on node  ',idgrp,' - current rep set(idgrp): ',repx_idrep(:)
    !    write(uout,*)'RE accepted on node  ',idgrp,' - current grp set(idrep): ',repx_idgrp(:)
    !end if
!debugging->>>

    my_inc = id_inc(idrep1)

    if( my_inc > 0 ) then

        successful_rep_exch(2) = successful_rep_exch(2) + 1

        beta = repx_betas(idrep1+1)

        betainv = 1.0_wp / beta

!debugging-<<<
        !if( master ) then
        !    write(uout,*)'RE accepted on node  ',idgrp,' - swapping replicas: ', idrep,' --> ',idrep1
        !    write(uout,*)'RE accepted on node  ',idgrp,' - swapping T-values: ', job%systemp,' --> ',betainv/BOLTZMAN
        !endif
!debugging->>>

        job%systemp = betainv / BOLTZMAN

    else if( my_inc < 0 ) then

        successful_rep_exch(1) = successful_rep_exch(1) + 1

        beta = repx_betas(idrep)

        betainv = 1.0_wp / beta

!debugging-<<<
        !if( master ) then
        !    write(uout,*)'RE accepted on node  ',idgrp,' - swapping replicas: ', idrep-1,' <-- ',idrep
        !    write(uout,*)'RE accepted on node  ',idgrp,' - swapping T-values: ', job%systemp,' --> ',betainv/BOLTZMAN
        !endif
!debugging->>>

        job%systemp = betainv / BOLTZMAN

    endif

end subroutine rep_exch_beta


!AB: hybrid version based on exchange of configuration (box) pairs
subroutine rep_exch_config(ib, job, beta, energytot, enthalpytot, energyrcp, energyreal, energyvdw, &
                        energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                        emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                        emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    !use kinds_f90
    use constants_module, only : uout, FED_PAR_DIST2 !, FED_PAR_DIST1
    use atom_module, only : set_atom_type
    use species_module
    use cell_module
    use latticevectors_module, only : cellsize, checkorthogonality
    use field_module, only : total_energy, total_recip, total_energy_nolist, update_rcpsums, copy_density, &
                             setnbrlists, set_thblist, set_rzero, set_exclist
    use thbpotential_module, only : numthb
    use metpot_module, only : nmetpot
    use random_module, only : duni

    use fed_calculus_module, only : fed, bias_vec, param_old, param_cur, id_old, id_cur
    use fed_order_module, only : fed_param_value, fed_param_dbias2, fed_param_hist_inc, fed_param_reset

    use cell_list_wrapper_module, only : tear_down_cell_list, set_up_cell_list

    !implicit none

    integer, intent(in) :: ib
    type(control), intent(inout) :: job

    real(kind = wp), intent(inout) :: beta
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyfour(:), energyext(:), energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                      emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), ncrct(number_of_molecules), &
                       next(number_of_molecules)

    integer :: im, i, k, irep, idx, ndim, ndim2, fail(3)

    real(kind=wp) :: ntot, newreal, newrcp, newvdw, newthree, newpair, &
                     newang, newfour, newmany, newext, newmfa, newtotal, newenth, oldtotal, newvir

    real(kind = wp) :: repx_total_energy(job%numreplicas) !total energies of system and beta for each replica
    real(kind = wp) :: eng1, eng2, b1, b2, delta, deltav, rn, vol, fed_par_own(3), fed_par_ally(3)

    integer ::  id_inc(job%numreplicas) ! T increments (+/-1*dT) by idgrp indices

    integer :: num_reps, idgrp1, my_inc, ir, id_fed_own, id_fed_ally 

    real(kind = wp), allocatable, dimension(:) :: buffout, buffin1 !, bias_ally 

    logical :: founda, new_structure, is_fed_com, is_conf_ortho
    
    fail = 0

!TU@@:
!    call gsync_world()

    !AB: activate FED if needed (checking fed%par_kind is sufficient)
    is_fed_com = ( fed%par_kind == FED_PAR_DIST2 )

    num_reps = job%numreplicas
    idgrp1   = idgrp+1

    !total energy & beta arrays: set to zero, allocate for workgroups then use gsum to get this on all nodes
    repx_total_energy = 0.0_wp

    !repx_total_energy(idrep1) = energytot(ib)
    repx_total_energy(idgrp1) = energytot(ib)

    !AB: collect and share all the energies by idrep (in T order)
    call gsum_world(repx_total_energy)
    repx_total_energy = repx_total_energy / real(wkgrp_size,wp)

!debugging-<<<
    if( master ) then

        !write(uout,*)
        !write(uout,*)'RE exchange on node  ',idgrp,' - current energy = ',&
        !            repx_total_energy(:)*repx_betas(:) !/job%energyunit

        if( abs(energytot(ib) - repx_total_energy(idgrp1) ) > job%checkenergyprec ) then

            write(uout,'(/,/,1x,2(a,f20.10),a,i3,/)') &
                  "ERROR: current RE energy value is off - IN = ", energytot(ib)*beta, &
                  " =/= ", repx_total_energy(idgrp1)*repx_betas(idgrp1), " in workgroup ",idgrp

            fail(3) = 1

        end if

        !call set_mol_coms(ib)
        !call check_mol_coms(ib)

        if( is_fed_com ) call fed_param_reset(ib, cfgs(ib), .false.)

        flush(uout)

    end if

    call gsum_world(fail)
    if (any(fail /= 0)) call cry(uout,'(/,1x,a,/)', &
                                "ERROR: rep_exch_config - energy collection failure !!!",999)
!debugging->>>

    id_inc = 0
    my_inc = 0

    !AB: advance randmon number selection to preserve the random sequence (for same workgroup!)
    rn = duni()

    if( idnode == 0 ) then

        my_inc = 1
        if( rn < 0.5 ) my_inc =-1

        if( my_inc > 0 ) then
        !AB: prepare the "even-up/odd-down" T-swaps 
        !AB: the last even, if any, won't swap

            !AB: loop over idgrp+1, T (beta) indices/replicas (NOT idgrp!)
            do ir = 1, num_reps-1, 2
               id_inc(ir)   =  my_inc
               id_inc(ir+1) = -my_inc
            end do

        else
        !AB: prepare the "even-down/odd-up" T-swaps 
        !AB: the first even, i.e. idgrp=0, and the last odd, if any, won't swap

            !AB: loop over idrep+1, T (beta) indices/replicas (NOT idgrp!)
            do ir = 2, num_reps-1, 2
               id_inc(ir)   = -my_inc
               id_inc(ir+1) =  my_inc
            end do

        end if

        my_inc = 0

    end if

    call msg_bcast_world(id_inc, num_reps)

!debugging-<<<
!    if( master ) then
!        !write(uout,*)
!        write(uout,*)'RE prepared on node  ',idgrp,' - current inc set(idgrp): ',id_inc(:)
!    end if
!debugging->>>

    !my_inc = id_inc(idrep1)
    my_inc = id_inc(idgrp1)

    id_inc = 0

    !AB: advance randmon number selection to preserve the random sequence (for same workgroup!)
    rn = duni()
    
    !AB: only one process in an exchanging pair decides on the move acceptance
    if( my_inc > 0 ) then

        if( is_fed_com ) then

            fed_par_own  = 0.0_wp
            fed_par_ally = 0.0_wp

            id_fed_own   = 0
            id_fed_ally  = 0

            fed_par_own(1) = fed_param_value(id_fed_own, ib, cfgs(ib), .false.)
            fed_par_own(2) = real(id_fed_own, kind=wp)

            !AB: send own data upwards & receive ally's config (downwards)

            call msg_send_blocked(4010, fed_par_own, 2, map_temp_up)
            call msg_receive_blocked(4011, fed_par_ally, 2)

            id_fed_ally = fed_par_ally(2)

            fed_par_own(3) = fed_param_dbias2(fed_par_own(1), fed_par_ally(1), .false.)

            call msg_send_blocked(5010, fed_par_own(3:3), 1, map_temp_up)
            call msg_receive_blocked(5011, fed_par_ally(3:3), 1)

            fed_par_own(3) = fed_par_own(3) + fed_par_ally(3)

        end if

        if( master ) then

            attempted_rep_exch = attempted_rep_exch + 1

            b1   = repx_betas(idgrp1)
            b2   = repx_betas(idgrp1+1)

            eng1 = repx_total_energy(idgrp1)
            eng2 = repx_total_energy(idgrp1+1)

            delta = exp( -((b1 - b2) * (eng2 - eng1) + fed_par_own(3)) )

            if( rn < delta ) then 

                id_inc(idgrp1)   = 1
                id_inc(idgrp1+1) =-1

            else

                rejected_rep_exch = rejected_rep_exch + 1

            end if

        end if

    else if( my_inc < 0 ) then

        if( is_fed_com ) then

            fed_par_own  = 0.0_wp
            fed_par_ally = 0.0_wp

            id_fed_own   = 0
            id_fed_ally  = 0

            fed_par_own(1) = fed_param_value(id_fed_own, ib, cfgs(ib), .false.)
            fed_par_own(2) = real(id_fed_own, kind=wp)

            !AB: send own data downwards & receive ally's config (upwards)

            call msg_receive_blocked(4010, fed_par_ally, 2)
            call msg_send_blocked(4011, fed_par_own, 2, map_temp_down)

            id_fed_ally = fed_par_ally(2)

            fed_par_own(3) = fed_param_dbias2(fed_par_own(1), fed_par_ally(1), .false.)

            call msg_receive_blocked(5010, fed_par_ally(3:3), 1)
            call msg_send_blocked(5011, fed_par_own(3:3), 1, map_temp_down)

        end if

!debugging-<<<
!    if( master ) & 
!        write(uout,*)'RE attempted on node ',idgrp,' - swapping replicas: ',idgrp,' <-> ',idgrp-1,' (dn)'
!    !attempted_rep_exch = attempted_rep_exch + 1
!debugging->>>

    end if

    call gsum_world(id_inc)

!debugging-<<<
!    if( master ) then
!        write(uout,*)
!        write(uout,*)'RE attempted on node  ',idgrp,' - current acc set(idgrp): ',id_inc(:)
!    end if
!debugging->>>

    my_inc = id_inc(idgrp1)

    fail = 0

    if( my_inc /= 0 ) then

        !allocate work arrays - these allow for lattice vectors to be added
        ndim  = (cfgs(ib)%maxno_of_atoms * 4) + 9

        allocate(buffout(ndim), stat = fail(1))
        allocate(buffin1(ndim), stat = fail(2))

        if (any(fail > 0)) then
            fail(1) = 354
            goto 999
        end if

        buffout(:) = 0.0_wp
        buffin1(:) = 0.0_wp

        !copy the atoms into buffer
        idx = 1
        do im = 1, cfgs(ib)%num_mols
            do i = 1, cfgs(ib)%mols(im)%natom

                buffout(idx)   = cfgs(ib)%mols(im)%atms(i)%rpos(1)
                buffout(idx+1) = cfgs(ib)%mols(im)%atms(i)%rpos(2)
                buffout(idx+2) = cfgs(ib)%mols(im)%atms(i)%rpos(3)
                buffout(idx+3) = real(cfgs(ib)%mols(im)%atms(i)%atlabel,wp)
                idx = idx + step

            end do
        end do

        !add the lattice vectors to the buffer
        do im = 1, 3
            do i = 1, 3

                buffout(idx) = cfgs(ib)%vec%latvector(im,i)
                idx = idx + 1

            end do
        end do

        if( my_inc > 0 ) then

            successful_rep_exch(2) = successful_rep_exch(2) + 1

            !AB: send own config (up) & receive ally's config (dn)

            call msg_send_blocked(6010, buffout, ndim, map_temp_up)
            call msg_receive_blocked(6011, buffin1, ndim)

!debugging-<<<
!            if( master ) then
!              write(uout,*)'RE accepted on node  ',idgrp,' - swapping replicas: ', idgrp,' --> ',idgrp1
!            endif
!debugging->>>

        else if( my_inc < 0 ) then

            successful_rep_exch(1) = successful_rep_exch(1) + 1

            !AB: send own config (dn) & receive ally's config (up)

            call msg_receive_blocked(6010, buffin1, ndim)
            call msg_send_blocked(6011, buffout, ndim, map_temp_down)

!debugging-<<<
!            if( master ) then
!              write(uout,*)'RE accepted on node  ',idgrp,' - swapping replicas: ', idgrp-1,' <-- ',idgrp
!            endif
!debugging->>>

        end if

        newrcp = 0.0_wp
        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newfour = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        !new_structure = .true.
        !print*,'rep exch accept 1 : ',idgrp-1,' -> ',idgrp,' (1-st? - ',new_structure,') ',rn,successful_rep_exch(1)

        oldtotal = repx_total_energy(idgrp1)
        newtotal = repx_total_energy(idgrp1+my_inc)

        !debugging-<<<
        !       if( master ) then
        !          write(uout,*)'RE accepted on node  ',idgrp,' - swapping replicas: ', idgrp,' <--> ',idgrp+my_inc
        !          write(uout,*)'RE accepted on node  ',idgrp,' - swapped  energies: ', oldtotal' <--> ',newtotal
        !       endif
        !debugging->>>

        idx = 1
        do im = 1, cfgs(ib)%num_mols
            do i = 1, cfgs(ib)%mols(im)%natom

                cfgs(ib)%mols(im)%atms(i)%rpos(1) = buffin1(idx)
                cfgs(ib)%mols(im)%atms(i)%rpos(2) = buffin1(idx+1)
                cfgs(ib)%mols(im)%atms(i)%rpos(3) = buffin1(idx+2)
                cfgs(ib)%mols(im)%atms(i)%atlabel = nint(buffin1(idx+3))
                idx = idx + step

                founda = .false.
                do k = 1, number_of_elements

                    if( k == cfgs(ib)%mols(im)%atms(i)%atlabel ) then

                       call set_atom_type(cfgs(ib)%mols(im)%atms(i), k, atm_charge(k), atm_mass(k), eletype(k))
                       founda = .true.

                    end if

                end do

                if( .not.founda ) then
                    !print*,'oops', im,i,cfgs(ib)%mols(im)%atms(i)%atlabel

                    fail(2) = 356
                    goto 999
                    
                    !call cry(uout,'(/,1x,a,/)', &
                    !    "ERROR: rep_exch_config - atom label not found upon config exchange !!!",356)
                endif

            end do
        end do

        !add the lattice vectors to the buffer
        do im = 1, 3
            do i = 1, 3

                cfgs(ib)%vec%latvector(im,i) = buffin1(idx)
                idx = idx + 1

            end do
        end do

        is_conf_ortho = job%useorthogonal

        !Check orthogonality of lattice vectors, or not
        call checkorthogonality( cfgs(ib)%vec, is_conf_ortho )

        if( is_conf_ortho .neqv. job%useorthogonal ) then
            job%useorthogonal = is_conf_ortho
            call cry(uout,'', &
                     "WARNING: RE - cell matrix orthogonality flag has been reset (unlikely to be set again)! ",0)
        endif

        call cellsize(cfgs(ib)%vec, vol)
        call invert_latticevectors(ib)

        !AB: initialise the molecule COM:s
        call set_mol_coms(ib)

        call setup_ewald(ib, job)

        !TU: Set cells for all atoms in box from scratch
        if(job%clist) then

            call tear_down_cell_list(ib)
            call set_up_cell_list(job, ib)

        end if

        
        !recalculate total energies including setup of ewald sum and g-vectors as a check of program integrity!
        if( job%uselist ) then

            !TU: We need to reset the neighbour lists every time a config is accepted from another replica
            call setnbrlists(ib, job%shortrangecut, job%verletshell, job%clist)
            if(job%lauto_nbrs) call set_rzero(ib)
            if(numthb > 0) call set_thblist(ib)
            call set_exclist(ib)

            call total_energy(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                npair, nthree, nang, nfour, nmany, next)

        else

            call total_energy_nolist(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec,  &
                newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                npair, nthree, nang, nfour, nmany, next, job%clist)

        end if

        if( job%coultype(ib) == 1 ) call total_recip(ib, newrcp, nrcp, newvir)

        oldtotal = newtotal
        newtotal = newrcp + newreal + newvdw + newthree + newpair + newang + newfour + newmany + newext + newmfa
        newenth  = newtotal + job%syspres * cfgs(ib)%vec%volume

        !compare the energies to test that the exchange was done correctly

        deltav = oldtotal - newtotal

        if( abs(newtotal) > job%checkenergyprec ) then

            if( master .and. abs(deltav/newtotal) > job%checkenergyprec ) then

                write(uout,"(/,'WARNING: replica exch_config (energy mismatch 1)',/)") 
                write(uout,"(/,'old total  ',f25.10)") oldtotal/job%energyunit
                write(uout,"(/,'new total  ',f25.10)") newtotal/job%energyunit
                write(uout,"(/,'abs(delta) ',f25.10)") abs(deltav)/job%energyunit

                fail(3) = 355

            end if

        else if( master .and. abs(deltav) > 100.0 * job%checkenergyprec ) then

            write(uout,"(/,'WARNING: replica exch_config (energy mismatch 2)',/)") 
            write(uout,"(/,'old total  ',f25.10)") oldtotal/job%energyunit
            write(uout,"(/,'new total  ',f25.10)") newtotal/job%energyunit
            write(uout,"(/,'abs(delta) ',f25.10)") abs(deltav)/job%energyunit

            fail(3) = 355

        end if

        !call gsum_world(fail)
        !if (any(fail /= 0)) call cry(uout,'(/,1x,a,/)', &
        !                        "ERROR: rep_exch_config - energy update failure !!!",355)

        !AB: if jumped into another box -> reset old and current parameter values (COM:s) and id/indices
        if( is_fed_com ) then

            call fed_param_reset(ib, cfgs(ib), .true.)
!--<
            if( master ) then
              if( abs(fed_par_ally(1)) > 1e-10 ) then

                  if( abs((param_old - fed_par_ally(1))/fed_par_ally(1)) > 1e-10 ) then

                      write(uout,"(/,'WARNING: replica exch_config (order mismatch 1)',/)") 
                      write(uout,"(/,'old param  ',f25.10,i10)") fed_par_ally(1),nint(fed_par_ally(2))
                      write(uout,"(/,'new param  ',f25.10,i10)") param_old, id_old
                      write(uout,"(/,'abs(delta) ',2f25.10)") &
                            abs((param_old - fed_par_ally(1))/fed_par_ally(1)),fed_par_own(3)

                      fail(3) = 355

                  endif

                  if( abs((param_cur - fed_par_ally(1))/fed_par_ally(1)) > 1e-10 ) then

                      write(uout,"(/,'WARNING: replica exch_config (order mismatch 1)',/)") 
                      write(uout,"(/,'cur param  ',f25.10,i10)") fed_par_ally(1),nint(fed_par_ally(2))
                      write(uout,"(/,'new param  ',f25.10,i10)") param_cur, id_cur
                      write(uout,"(/,'abs(delta) ',2f25.10)") &
                            abs((param_cur - fed_par_ally(1))/fed_par_ally(1)),fed_par_own(3)

                      fail(3) = 355

                  endif

              else
 
                  if( abs(param_old - fed_par_ally(1)) > 1.e-8 ) then

                      write(uout,"(/,'WARNING: replica exch_config (order mismatch 2)',/)") 
                      write(uout,"(/,'old param  ',f25.10,i10)") fed_par_ally(1),nint(fed_par_ally(2))
                      write(uout,"(/,'new param  ',f25.10,i10)") param_old, id_old
                      write(uout,"(/,'abs(delta) ',2f25.10)") abs(param_old - fed_par_ally(1)),fed_par_own(3)

                      fail(3) = 355

                  endif

                  if( abs(param_cur - fed_par_ally(1)) > 1.e-8 ) then

                      write(uout,"(/,'WARNING: replica exch_config (order mismatch 2)',/)") 
                      write(uout,"(/,'cur param  ',f25.10,i10)") fed_par_ally(1),nint(fed_par_ally(2))
                      write(uout,"(/,'new param  ',f25.10,i10)") param_cur, id_cur
                      write(uout,"(/,'abs(delta) ',2f25.10)") abs(param_cur - fed_par_ally(1)),fed_par_own(3)

                      fail(3) = 355

                  endif

              endif
            endif
!-->
            !AB: bias is also updated if needed (e.g. WL)
            call fed_param_hist_inc(ib,.true.)

        end if

        !accept update energies etc
        energytot(ib) = newtotal
        enthalpytot(ib) = newenth
        energyrcp(ib) = newrcp
        energyreal(ib) = newreal
        energyvdw(ib) = newvdw
        energypair(ib) = newpair
        energythree(ib) = newthree
        energyang(ib) = newang
        energyfour(ib) = newfour
        energymany(ib) = newmany
        energyext(ib) = newext
        energymfa(ib) = newmfa
        volume(ib) = cfgs(ib)%vec%volume

#ifdef MOLENG
        do im = 1, number_of_molecules

            emolrealnonb(ib,im) = nrealnonb(im)
            emolrealbond(ib,im) = nrealbond(im)
            emolrealself(ib,im) = nself(im)
            emolrealcrct(ib,im) = ncrct(im)
            emolrcp(ib,im) = nrcp(im)
            emolvdwn(ib,im) = nvdwn(im)
            emolvdwb(ib,im) = nvdwb(im)
            emolpair(ib,im) = npair(im)
            emolthree(ib,im) = nthree(im)
            emolang(ib,im) = nang(im)
            emolfour(ib,im) = nfour(im)
            emolmany(ib,im) = nmany(im)
            emolext(ib,im) = next(im)

            ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
               + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

            emoltot(ib,im) = ntot

        end do
#endif

        if( job%coultype(ib) == 1 ) call update_rcpsums(ib)

        if( nmetpot > 0 ) call copy_density(ib)

        if( allocated(buffout) ) deallocate(buffout)
        if( allocated(buffin1) ) deallocate(buffin1)

    end if

!TU@@:
!999 call gsync_world()
999 continue
    
    call gsum_world(fail)
    if (any(fail == 354)) call error(354)
    if (any(fail == 355)) call error(355)
    if (any(fail == 356)) call error(356)

    return

354 call error(354)

    return

355 call error(355)

    return

356 call error(356)

end subroutine rep_exch_config


!TU-: The below function is not called anywhere - it is obsolete

!AB: original version by JP, based on exchange of configuration/box pairs
subroutine rep_exchange(ib, numreplicas, job, beta, energytot, enthalpytot, energyrcp, energyreal, energyvdw, &
                        energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                        emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                        emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    !use kinds_f90
    use atom_module, only : set_atom_type
    use species_module
    use cell_module
    use latticevectors_module, only : cellsize
    use field_module, only : total_energy, total_recip, total_energy_nolist, update_rcpsums, copy_density
    use metpot_module, only : nmetpot
    use random_module, only : duni

    !implicit none

    integer, intent(in) :: ib, numreplicas
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyfour(:), energyext(:), energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                      emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), ncrct(number_of_molecules), &
                       next(number_of_molecules)

    integer :: im, i, k, irep, idx, ndim, fail(3)

    real(kind=wp) :: deltav, ntot, newreal, newrcp, newvdw, newthree, newpair, &
                     newang, newfour, newmany, newext, newmfa, newtotal, newenth, oldtotal, newvir

    real(kind = wp) :: rep_total_energy(0:numreplicas), rep_beta(0:numreplicas) !total energies of system and beta for each replica
    real(kind = wp) :: eng1, eng2, b1, b2, delta, vol, rn

    integer ::  accept(0:numreplicas) !array of flags indicating whether successful exchange is expected

    logical :: founda, new_structure

    real(kind = wp), allocatable, dimension(:) :: buffout, buffin1, buffin2

    newreal = 0.0_wp
    newrcp = 0.0_wp
    newvdw = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newfour = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    new_structure = .false.

!    return
!   call gsync_world()

    attempted_rep_exch = attempted_rep_exch + 1

    !allocate work arrays
    ndim = (cfgs(ib)%maxno_of_atoms * 4) + 9   !this allows for lattice vectors to be added

    allocate (buffout(ndim), stat = fail(1))
    allocate (buffin1(ndim), stat = fail(2))
    allocate (buffin2(ndim), stat = fail(3))

    if (any(fail > 0)) call error(354)

    !total energy array, set to zero, allocate values for workgroups then use gsum to get this on all nodes
    !do this for values of beta
    rep_total_energy = 0.0_wp
    rep_beta = 0.0_wp

    oldtotal = energytot(ib)

    rep_total_energy(idgrp) = energytot(ib)
    rep_beta(idgrp) = beta

    call gsum_world(rep_total_energy)
    rep_total_energy = rep_total_energy / wkgrp_size
    call gsum_world(rep_beta)
    rep_beta = rep_beta / wkgrp_size

    accept = 0

    rn = duni()

    if (idgrp /= numreplicas - 1) then  ! numreplicas - 1 is the highest temperature replica and can not access a new configuration

        eng1 = rep_total_energy(idgrp)
        eng2 = rep_total_energy(idgrp+1)
        b1 = rep_beta(idgrp)
        b2 = rep_beta(idgrp+1)
        delta = exp(-((b1 - b2) * (eng2 - eng1)))

        if ( rn < delta) accept(idgrp) = 1

    endif

    call gsum_world(accept)
!   call gsync_world()

    if( accept(idgrp)>=1 ) then
        print*,'RE accepted on node ',idgrp,' with RN = ',rn,attempted_rep_exch
    endif

    !copy the atoms into buffer
    idx = 1
    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            buffout(idx) = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            buffout(idx+1) = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            buffout(idx+2) = cfgs(ib)%mols(im)%atms(i)%rpos(3)
            buffout(idx+3) = real(cfgs(ib)%mols(im)%atms(i)%atlabel)
            idx = idx + step

        enddo

    enddo

    !add the lattice vectors to the buffer
    do im = 1, 3

        do i = 1, 3

            buffout(idx) = cfgs(ib)%vec%latvector(im,i)
            idx = idx + 1

        enddo

    enddo

    !do a paired send recieve form low temperature to higher temperature
    call msg_send_blocked(6010, buffout, ndim, map_temp_up)

    if (idgrp /= 0) then  !the highest temperature does not receive data

        call msg_receive_blocked(6010, buffin1, ndim)

    endif


    !do a second paired send/receive to send high temperature to low temperature

    if(idgrp > 0) then

        call msg_send_blocked(6011, buffout, ndim, map_temp_down)

    endif

    if (idgrp /= numreplicas - 1) then  !the highest temperature does not receive data

        call msg_receive_blocked(6011, buffin2, ndim)

    endif

    !now the data is loaded into buffers it is now coppied into structures when it is appropriate to do so

    !buffin1 contains data from low temperature
    if (idgrp > 0) then

        if(accept(idgrp-1) >= 1) then

            oldtotal = rep_total_energy(idgrp-1)

            successful_rep_exch(1) = successful_rep_exch(1) + 1

!    new_structure = .true.
!    print*,'rep exch accept 1'

    new_structure = .true.
    print*,'rep exch accept 1 : ',idgrp-1,' -> ',idgrp,' (1-st? - ',new_structure,') ',rn,successful_rep_exch(1)

            idx = 1
            do im = 1, cfgs(ib)%num_mols

                do i = 1, cfgs(ib)%mols(im)%natom

                    cfgs(ib)%mols(im)%atms(i)%rpos(1) = buffin1(idx)
                    cfgs(ib)%mols(im)%atms(i)%rpos(2) = buffin1(idx+1)
                    cfgs(ib)%mols(im)%atms(i)%rpos(3) = buffin1(idx+2)
                    cfgs(ib)%mols(im)%atms(i)%atlabel = nint(buffin1(idx+3))
                    idx = idx + step

                enddo

            enddo

            !add the lattice vectors to the buffer
            do im = 1, 3

                do i = 1, 3

                    cfgs(ib)%vec%latvector(im,i) = buffin1(idx)
                    idx = idx + 1

                enddo

            enddo

        endif

    endif

!   call gsync_world()

    !buffin2 contains data from high temperature
    if (accept(idgrp) >= 1) then

        oldtotal = rep_total_energy(idgrp+1)

        successful_rep_exch(2) = successful_rep_exch(2) + 1

!    new_structure = .true.
!    print*,'rep exch accept 2'

    print*,'rep exch accept 2 : ',idgrp+1,' -> ',idgrp,' (2-nd? - ',new_structure,') ',rn,successful_rep_exch(2)
    new_structure = .true.

        idx = 1
        do im = 1, cfgs(ib)%num_mols

            do i = 1, cfgs(ib)%mols(im)%natom

                cfgs(ib)%mols(im)%atms(i)%rpos(1) = buffin2(idx)
                cfgs(ib)%mols(im)%atms(i)%rpos(2) = buffin2(idx+1) 
                cfgs(ib)%mols(im)%atms(i)%rpos(3) = buffin2(idx+2) 
                cfgs(ib)%mols(im)%atms(i)%atlabel = nint(buffin2(idx+3))
                idx = idx + step

            enddo

        enddo

        !add the lattice vectors to the buffer
        do im = 1, 3

            do i = 1, 3

                cfgs(ib)%vec%latvector(im,i) = buffin2(idx) 
                idx = idx + 1

            enddo

        enddo

    endif

!   call gsync_world()
    if(new_structure) then

    !need to make sure the identity based properties are mapped since atoms/molecules may be swapping
    do im = 1, cfgs(ib)%num_mols

        do i = 1, cfgs(ib)%mols(im)%natom

            founda = .false.
            do k = 1, number_of_elements

                if(k == cfgs(ib)%mols(im)%atms(i)%atlabel) then

                    call set_atom_type(cfgs(ib)%mols(im)%atms(i), k, atm_charge(k), atm_mass(k), eletype(k))
                    founda = .true.

                endif

            enddo

        enddo

    enddo

    if(.not.founda) then
print*,'oops', im,i,cfgs(ib)%mols(im)%atms(i)%atlabel
        call error(356)

    endif

    !recalculate total energies including setup of ewald sum and g-vectors as a check of program integrity!
    call invert_latticevectors(ib)
    call cellsize(cfgs(ib)%vec, vol)
    call setup_ewald(ib, job)

    if(job%uselist) then

        call total_energy(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                          npair, nthree, nang, nfour, nmany, next)

    else

        call total_energy_nolist(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec,  &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                          npair, nthree, nang, nfour, nmany, next, job%clist)

    endif

    if(job%coultype(ib) == 1) then

        call total_recip(ib, newrcp, nrcp, newvir)

    endif

    newtotal = newrcp + newreal + newvdw + newthree + newpair + newang + newfour + newmany + newext + newmfa
    newenth = newtotal + job%syspres * cfgs(ib)%vec%volume


    !compare the energies to test that the exchange was done correctly

    deltav = oldtotal - newtotal

    if (master .and. abs(deltav) > 1e-6) then

        write(uout,"(/,'WARNING: replica exchange',/)") 
        write(uout,"(/,'old total  ',f25.10)") oldtotal/9648.530821
        write(uout,"(/,'new total  ',f25.10)") newtotal/9648.530821
        write(uout,"(/,'abs(delta) ',f25.10)") abs(deltav)/9648.530821

        call error(355)

    endif

    !accept update energies etc
    energytot(ib) = newtotal
    enthalpytot(ib) = newenth
    energyrcp(ib) = newrcp
    energyreal(ib) = newreal
    energyvdw(ib) = newvdw
    energypair(ib) = newpair
    energythree(ib) = newthree
    energyang(ib) = newang
    energyfour(ib) = newfour
    energymany(ib) = newmany
    energyext(ib) = newext
    energymfa(ib) = newmfa
    volume(ib) = cfgs(ib)%vec%volume

    do im = 1, number_of_molecules

        emolrealnonb(ib,im) = nrealnonb(im)
        emolrealbond(ib,im) = nrealbond(im)
        emolrealself(ib,im) = nself(im)
        emolrealcrct(ib,im) = ncrct(im)
        emolrcp(ib,im) = nrcp(im)
        emolvdwn(ib,im) = nvdwn(im)
        emolvdwb(ib,im) = nvdwb(im)
        emolpair(ib,im) = npair(im)
        emolthree(ib,im) = nthree(im)
        emolang(ib,im) = nang(im)
        emolfour(ib,im) = nfour(im)
        emolmany(ib,im) = nmany(im)
        emolext(ib,im) = next(im)

        ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
                    + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

        emoltot(ib,im) = ntot

    enddo

    if(job%coultype(ib) == 1) then
        call update_rcpsums(ib)
    endif

    if(nmetpot > 0) call copy_density(ib)
    endif
    new_structure = .false.

    call gsync_world()

    if (allocated(buffout)) deallocate (buffout)
    if (allocated(buffin1)) deallocate (buffin1)
    if (allocated(buffin2)) deallocate (buffin2)

end subroutine

end module
