!***************************************************************************
!   Copyright (C) 2021 by Vincent Ballenegger                              *
!   vincent.ballenegger@univ-fcomte.fr                                     *
!                                                                          *
!   Contributers to this file:                                             *
!   - Tom L. Underwood - Basic tidying up and documentation                *
!***************************************************************************

!> @brief
!> - Flag and subroutine for halting program via the GNU Extension function 'signal'
!> @usage 
!> - @stdusage 
!> - @stdspecs

module signals

    implicit none

    logical :: stop_iter

contains

    subroutine sigint_handler

        stop_iter=.true.
        !print *, 'Received SIGINT or SIGTERM signal: exiting...'

    end subroutine sigint_handler

end module signals
