! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

module gcmc_moves

    use kinds_f90
    use statistics_type
    use mc_moves

    implicit none

        !> attempted inserts for gcmc
    integer ::    attemptedgcatminsert

        !> no attempted removes for gcmc
    integer ::    attemptedgcatmremove

        !> no successful inserts using gcmc
    integer ::    sucessfulgcatminsert

        !> no successful removals using gcmc
    integer ::    sucessfulgcatmremove

        !> Number of empty atom removals (removal from a system with 0 atoms) 
    integer ::     emptygcatmremove

        !> attempted inserts for gcmc
    integer ::    attemptedgcmolinsert

        !> no attempted removes for gcmc
    integer ::    attemptedgcmolremove

        !> no successful inserts using gcmc
    integer ::    sucessfulgcmolinsert

        !> no successful removals using gcmc
    integer ::    sucessfulgcmolremove

        !> Number of empty molecule removals (removal from a system with 0 molecules) 
    integer ::     emptygcmolremove

        !> lookup table of types for gcmc
    integer, allocatable ::    gcmclookup(:)

        !> the no of molecules avalaible for gcmc
    integer              ::    num_gcmc_mols

        !> chemical potential for gcmc similations
    real (kind=wp), dimension(:), allocatable ::    gcmc_pot

        !> number of forward mutations attempted
    integer ::    forwardmutations

        !> number of backward mutations attempted
    integer ::    backmutations

        !> number of attempted forward mutations attempted
    integer ::    attforwardmutations

        !> number of attempted backward mutations attempted
    integer ::    attbackmutations

        !> Number of empty mutations (attempted mutations from a configuration with
        !> 0 molecules/atoms of the species earmarked for mutation) 
    integer ::     emptymutations

        !> vector holding types for forward transformation
    integer, allocatable ::    transtype1(:)

        !> vector holding types for backward transformation
    integer, allocatable ::    transtype2(:)

        !> vector holding types for forward semigrand ensemble
    integer, allocatable ::    semitype1(:)

        !> vector holding types for back semigrand ensemble
    integer, allocatable ::    semitype2(:)
    integer, allocatable ::    muttype1(:)
    integer, allocatable ::    mutlookup1(:)
    integer, allocatable ::    muttype2(:)
    integer, allocatable ::    mutlookup2(:)

        !> chemical potential differences for transmutation (semi-grand) enesmble
    real (kind=wp), allocatable ::    deltamu(:)

        !> number of mutations
    real (kind=wp) ::    nummutations

        !> chemical potential of atoms - (forward mutation for semigrand)
    real (kind=wp), dimension(:,:), allocatable ::    chem_pot_atm1

        !> chemical potential of atoms - (backward mutation for semigrand)
    real (kind=wp), dimension(:,:), allocatable ::    chem_pot_atm2

        !> molecule type to be inserted in gcmc
    integer, dimension(:), allocatable :: gcinsert_molecules


contains

!> GCMC (insert/delete) of allowed atoms - no biasing
!> the atom is always added/deleted to/from the last molecule
!> note there is no recip space as charged atoms are not allowed!
subroutine gcmc_atoms(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    !use constants_module, only : uout
    use control_type
    use cell_module
    use field_module
    use nbrlist_module
    use species_module, only : number_of_molecules, number_of_elements
    use comms_mpi_module, only : is_parallel, gsum!_world !, mxnode
    use random_module, only : duni
    use vdw_module, only : vdw_lrc_energy
    use cell_list_wrapper_module, only : atom_set_cell, atom_remove_from_cell_list

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                      energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), volume(:)


    integer :: im, atm, typ, ii, k, ngas, igrid, fail, buf(2)
    real(kind = wp) :: rn, deltavb, arg, choice

    real(kind = wp) :: newreal, newvdw, newthree, newpair, newang, newfour, &
                       newmany, newext, newmfa, newtotal, newenth, newvir, ntot, pcav

     ! temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), ncrct(number_of_molecules), next(number_of_molecules)

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: old_nums_elemts(number_of_elements), new_nums_elemts(number_of_elements)

    logical :: atom_out, overlap, early_out

    !AB: NOTE: Ewald sums are not treated for simgle atom GCMC ! 
    !AB: no reciprocal space charge contribution is calculated !
    !AB: because single charge GCMC violates electroneutrality !
    !AB: Hence the input checks must not allow for such a GCMC !

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    ncrct = 0.0_wp
    next = 0.0_wp

    ! zero energies just in case
    newtotal = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newfour = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts

!AB: Although the fix below works for all our GCMC regression tests,
!AB: I am not convinced that it would work as expected in general.
!AB: NOTE: The original version presumed that the last molecule in CONFIG
!AB: was always of "atomic field" type, but also that the number of (species)
!AB: *molecular types* in FIELD was always equal to the actual number of (instances)
!AB: *molecules* in CONFIG (otherwise this routine would not work correctly).
!AB: In general, however, the number of molecules (instances) can be greater 
!AB: than the number of molecular types (species), for which case my patch makes
!AB: this routine to work correctly - in the vein of the restriction that only 
!AB: the atoms in the last molecule (i.e. not species!) can be deleted and inserted.
!AB: Once again: all this only makes sense if the last molecular species in FIELD
!AB: is defined as *atomic field* (no connectivity = no internal structure), and 
!AB: at the same time there is only one molecule of that type in CONFIG - the last molecule.

!AB: This is NOT the number of molecules in the system (CONFIG)
!AB: This is the number of DIFFERENT molecular types / species (FIELD)
!    im = number_of_molecules

!AB: I replaced the above with the actual number of molecules in the system (below)
!AB: However if same atom type is present in several species (i.e. all of their instances),
!AB: how about inserting an atom in a randomly chosen molecule rather than always in the last one - ???
    im = cfgs(ib)%num_mols

    fail = 0

    !AB: make sure ALL processes in the workgroup aware of the local error and call error(..) -> MPI_FINALIZE(..)
    call gsum(fail) !_world(fail)
    if( fail > 0 ) call error(317)

    arg = 0.0_wp

    choice = duni()

    early_out = .false.

    if (choice <= 0.5_wp) then  ! attempt insert

        attemptedgcatminsert = attemptedgcatminsert + 1

        !TU-: Here is where the atomic species to be inserted is determined. How is this done?
        !TU-: Note that job%numgcmcatom is the number of different atom species which could
        !TU-: be inserted/deleted using GCMC
        !TU-:
        !TU-: gcmclookup(i) is the atomic species number in FIELD corresponding to the ith
        !TU-: species of atom which could be inserted/deleted using GCMC
        !TU-:
        !TU-: Currently all atomic species are inserted/deleted with the same relative frequency
        !TU-: 
        !TU-: TO DO: Generalise GCMC to allow different relative frequencies for different species
        !TU-:  Follow how I've done this for swap moves; this includes generalising the output
        !TU-:  to give the number of insertions/deletions for each type of atom individually. Then
        !TU-:  I'll need to do the same for molecular GCMC, atomic SGCMC, molecular SGCMC, atomic
        !TU-:  translation moves, molecular translation moves (in both the sequential and
        !TU-:  non-sequential branches - We should delete the sequential branch and use a variable
        !TU-:  to implement it in the non-sequantial procedure), molecular rotational moves,
        !TU-:  atomic rotational moves. This is a lot of work; I'll need to be selective.
        
        ! atm is the type
        k = int(duni() * job%numgcmcatom + 1)

        typ = gcmclookup(k)

        !TU-: What should ngas be? Should it use cfgs(ib)%num_elemts?
        !TU: After this ngas is the number of atoms of the considered type in molecule im (the last
        !TU: molecule in the configuration ib)
        call findnumtype_in_mol(ib, typ, im, ngas)

        early_out = .true.

        ! make sure dont go over array limits
        if( ngas > cfgs(ib)%mols(im)%mxatom-1 ) then 
        
            call dump_revcon(job%revcon_format, -999, energytot)

            fail = 203 !call error(203)
            goto 2000

        endif

        !AB: advance randmon number selection to preserve the random sequence
        !rn = duni()

        !find a position to place atom - it is always placed at the end of the list
        if(job%usecavitybias) then

            call insert_atom_atcavity(ib, typ, im, atm, igrid, pcav, job%lauto_nbrs, atom_out)

            ! if atom got out the cell/slit => reject insertion!
            !if( atom_out ) goto 1000

            !AB: if atom_out=.true. at this stage, no atom has been inserted yet!
            !AB: so no atom is to be removed, nor cavity grid reset
            if( atom_out ) goto 2000

            if (pcav < 0.0_wp) then ! < 0.0 means no free cavity found/hit - do a random insertion

                write(*,*)'gcmc_moves::gcmc_atoms(..) WARNING: failed to find a free cavity!'

                call insert_atom(ib, typ, im, atm, job%lauto_nbrs, atom_out)

                ! if atom got out the cell/slit => reject insertion!
                !if( atom_out ) goto 1000

                !AB: if atom_out=.true. at this stage, no atom has been inserted yet!
                !AB: so no atom is to be removed, nor cavity grid reset
                if( atom_out ) goto 2000

                pcav = 1.0

            endif

            !TU**: Why do we do this here? Are all nodes not in sync, in which case this is unnecessary?
            !TU**: This calls a gsync, causing a slow-down. 
            !TU**: This can be deleted I think.
            if( is_parallel ) then

                call broadcast_atom_pos(ib, im, atm)
                call broadcast_atom_type(ib, im, atm)

            endif

        else

            call insert_atom(ib, typ, im, atm, job%lauto_nbrs, atom_out)

            ! if atom got out the cell/slit => reject insertion!
            !if( atom_out ) goto 1000

            !AB: if atom_out=.true. at this stage, no atom has been inserted yet!
            !AB: so no atom is to be removed, nor cavity grid reset
            if( atom_out ) goto 2000

            pcav = 1.0

            !TU**: Why do we do this here? Are all nodes not in sync, in which case this is unnecessary?
            !TU**: This calls a gsync, causing a slow-down.
            !TU**: This can be deleted I think.
            if( is_parallel ) then

                call broadcast_atom_pos(ib, im, atm)
                call broadcast_atom_type(ib, im, atm)

            endif

        endif


        !TU: Set the cell of the atom and put it into the cell list
        if(job%clist) call atom_set_cell(ib, im, atm, .true.)

        !TU: The insert_atom procedure used above updates cfgs(ib)%nums_elemts to reflect insertion
        new_nums_elemts = cfgs(ib)%nums_elemts

        early_out = .false.

        if(job%uselist) then

!AB: GCMC does not work with neighborlists because the arrays are not allocated nor initiated
!AB: this is in its turn because a new molecule is created by copying an instance of uniq_mol 
!AB: which does not have NL nor exclusion lists initiated - where is the best place to do this?

            ! calculate nbrlist + check for overlap
            call atom_nbrlist_overlap(ib, im , atm, job%shortrangecut, job%verletshell, job%gcmcmindist, overlap)

            !if overlap then reject immediately
            if (overlap) goto 1000

            ! evaluate energy
            call atom_energy(ib, im, atm, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, job%verletshell, &
                     newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                     .false., .false.)

        else

            call atom_overlap(ib, im , atm, job%gcmcmindist, overlap, job%clist)

            !if overlap then reject immediately
            if (overlap) goto 1000

            call atom_energy_nolist(ib, im, atm, job%coultype(ib), job%dielec, &
                     job%vdw_cut, job%shortrangecut, &
                     newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                     nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                     .false., .false., job%clist)

        endif

        !TU: newvdw is actually the change in VdW relative to the old configuration. Hence add
        !TU: the CHANGE in long-range VdW correction to this
        newvdw = newvdw + ( vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                          - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts) )

        if( newvdw + newext > vdwcap ) goto 1000 ! reject - new energy is too high

        newtotal = newreal + newvdw + newthree + newpair + newang &
                 + newmany + newfour + newext + newmfa !+ newrcp
        newenth  = newtotal + extpress * volume(ib)
             
        deltavb = beta * newtotal

        if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high

!AB: original version:
!        if(job%usegaspres) then
!            arg = (beta * pcav * volume(ib) * gcmc_pot(k) / (ngas + 1)) * exp(-beta * newtotal);
!        else
!            ! Adams/Mezei
!!           arg = exp((gcmc_pot(k) - newtotal) * beta) * (pcav / (ngas + 1))
!
!            ! Muller/Fuchs/Frenkel&Smit
!            arg = ((pcav * volume(ib) * gcmc_pot(k)) / (ngas + 1)) * exp(-beta * newtotal)
!        endif

!AB: new version with more options:
        if( job%usegaspres ) then
            !AB: this is the same as below using Muller/Fuchs scheme - why scaling by beta?

            arg = (beta * pcav * volume(ib) * gcmc_pot(k) / (ngas + 1)) * exp(-beta * newtotal)

        elseif( job%usechempotkt ) then
            !AB: input chem. pot. in kT (as is the oroginal input, i.e. no conversion to internal units)
            !AB: this is how I have it coded in my own (FES-MS) code...
            !AB: the quantum factor for ideal gas entropy is skipped but it is constant anyway

            arg = pcav * exp(gcmc_pot(k) - newtotal*beta) * volume(ib) / (ngas + 1)

        elseif( job%usechempotkj) then
            !AB: input chem. pot. in kJ/mol (but input must be converted to internal units) 
            !AB: this is essentially as in my own (FES-MC) code but chem.pot. is in internal units
            !AB: what about the ideal gas entropy contribution? - avoiding the quantum factor?

            arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) * volume(ib) / (ngas + 1)

            ! Adams/Mezei 
            ! AB: input chem. pot. in internal units? (but it was never converted to!)
            !arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) / (ngas + 1)

        else!AB: use "chemical activity"
            !AB: this is the same as above "partial gas pressure" scheme - why not scaled by beta?

            ! Muller/Fuchs/Frenkel&Smit

            arg = ((pcav * volume(ib) * gcmc_pot(k)) / (ngas + 1)) * exp(-beta * newtotal)

        endif

        !test for accept or rejection
        if( duni() < arg) then

            !if accept add permanently to list
            energytot(ib) = energytot(ib) + newtotal
            enthalpytot(ib) = enthalpytot(ib) + newenth
            virialtot(ib) = virialtot(ib) + newvir
            energyreal(ib) = energyreal(ib) + newreal
            energyvdw(ib) = energyvdw(ib) + newvdw
            energypair(ib) = energypair(ib) + newpair
            energythree(ib) = energythree(ib) + newthree
            energyang(ib) = energyang(ib) + newang
            energyfour(ib) = energyfour(ib) + newfour
            energymany(ib) = energymany(ib) + newmany
            energyext(ib) = energyext(ib) + newext
            energymfa(ib) = energymfa(ib) + newmfa

            !energyrcp(ib) = energyrcp(ib) + newrcp

            sucessfulgcatminsert = sucessfulgcatminsert + 1

            if(job%lmoldata) then

                do ii = 1, number_of_molecules
                    emolrealnonb(ib,ii) = emolrealnonb(ib,ii) + nrealnonb(ii)
                    emolrealbond(ib,ii) = emolrealbond(ib,ii) + nrealbond(ii)
                    emolvdwn(ib,ii) = emolvdwn(ib,ii) + nvdwn(ii)
                    emolvdwb(ib,ii) = emolvdwb(ib,ii) + nvdwb(ii)
                    emolpair(ib,ii) = emolpair(ib,ii) + npair(ii)
                    emolthree(ib,ii) = emolthree(ib,ii) + nthree(ii)
                    emolang(ib,ii) = emolang(ib,ii) + nang(ii)
                    emolfour(ib,ii) = emolfour(ib,ii) + nfour(ii)
                    emolmany(ib,ii) = emolmany(ib,ii) + nmany(ii)
                    emolext(ib,ii) = emolext(ib,ii) + next(ii)

                    ntot = nrealnonb(ii) + nrealbond(ii)  + nvdwn(ii) + nvdwb(ii) + npair(ii) &
                           + nthree(ii) + nang(ii) + nfour(ii) + nmany(ii) + next(ii)

                    emoltot(ib,ii) = emoltot(ib,ii) + ntot
                enddo

            endif

            if(job%uselist) call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist)

            goto 2000

	    return

        endif
        !else

            !reject - delete atom and clean up
1000        continue ! AB: cannot remove atom before updating/resetting the cavity arrays!!!
!            call remove_atom(ib, im, atm, job%uselist, job%lauto_nbrs)

            !AB: accounting for a single cavity at insertion can only work if {dx,dy,dz} >= d_particle
            !if( job%usecavitybias ) call reset_cavity0(ib, igrid)

            if( job%usecavitybias ) then

                if( early_out ) then
                    call cry(uout,'', &
                            "WARNING: gcmc_atoms(insert) - cavity resetting at early stage !!!",0)
                else
                    call reset_pcav_insert(ib, igrid, job%cavity_radius, cfgs(ib)%mols(im)%atms(atm)%rpos)
                    !call reset_pcav_insert(ib, im, atm, job%cavity_radius, igrid)
                endif

            endif

            !TU: Remove atom from cell list (before the atom is deallocated!)
            if(job%clist) call atom_remove_from_cell_list(ib, im, atm, .false.)            

            call remove_atom(ib, im, atm, job%uselist, job%lauto_nbrs)

        !endif

        goto 2000

    else                     ! attempt remove 

        attemptedgcatmremove = attemptedgcatmremove + 1

        !select atom to insert and check we have an atom of correct type
        k = int(duni() * job%numgcmcatom + 1)

        typ = gcmclookup(k)

        !TU-: What should ngas be? Should it use cfgs(ib)%num_elemts?
        !TU: After this ngas is the number of atoms of the considered type in molecule im (the last
        !TU: molecule in the configuration ib)
        call findnumtype_in_mol(ib, typ, im, ngas)

        !if( ngas < 1 ) then
        !    fail = 207 !call error(207)
        !    goto 2000
        !endif

        if(ngas == 0 ) then

            emptygcatmremove =  emptygcatmremove + 1

        else if (ngas > 0) then ! only if we have an atom to remove

            !TU: select_molatom would hang if there are no atoms of the specified type - won't happen due to above check
            call select_molatom(typ, ib, im, atm)

            !calculate pcav on removal of an atom
            pcav = 1.0_wp
            if( job%usecavitybias ) &
                call calc_pcav_remove(ib, job%cavity_radius, &
                cfgs(ib)%mols(im)%atms(atm)%rpos, pcav, .true.)

            !if(job%usecavitybias) call calc_pcav_remove(ib, im, atm, job%cavity_radius, pcav)

            !TU: Note that we haven't yet removed the atom (which happens only upon acceptance)
            !TU: and so must manually set new_nums_elemts (instead of just setting it to 
            !TU: cfgs(ib)%nums_elemts as was done for insertion)
            new_nums_elemts(typ) = new_nums_elemts(typ) - 1

            !get energy of the atom
            if(job%uselist) then

                call atom_energy(ib, im, atm, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                         .false., .false.)

            else

                call atom_energy_nolist(ib, im, atm, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newvdw, newthree, newpair, newang, newfour, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, npair, nthree, nang, nfour, next, &
                        .false., .false., job%clist)

            endif

            !TU: newvdw is actually the negative of the change in VdW relative to the old configuration. Hence
            !TU: add the negative of the CHANGE in long-range VdW correction to this
            newvdw = newvdw - ( vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                              - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts) )

            newtotal = newreal + newvdw + newthree + newpair + newang &
                     + newmany + newfour + newext + newmfa
            newenth  = newtotal + extpress * volume(ib)

!AB: original version:
!            if(job%usegaspres) then
!                arg = ((ngas) / (pcav * gcmc_pot(k) * volume(ib) * beta)) * exp(beta * newtotal);
!            else
!                ! Adams/Mezei
!!               arg = ((ngas) / pcav) * exp((-gcmc_pot(k) + newtotal) * beta)
!
!                !form used by Fuchs/Muller
!                arg = ((ngas) / (gcmc_pot(k) * volume(ib) * pcav)) * exp(-beta * newtotal)
!            endif

!AB: new version with more options:
            if(job%usegaspres) then

                arg = ( (ngas) / (gcmc_pot(k) * pcav * volume(ib) * beta) ) * exp( beta*newtotal )

            elseif(job%usechempotkt) then

                arg = exp( -gcmc_pot(k) + beta*newtotal ) * (ngas) / (volume(ib) * pcav)

            elseif(job%usechempotkj) then

                arg = exp( (-gcmc_pot(k) + newtotal) * beta ) * (ngas) / (volume(ib) * pcav)

                !original method by Adams/Mezei
                !arg = exp( (-gcmc_pot(k) + newtotal) * beta ) * (ngas) / pcav

            else

                !form used by Fuchs/Muller/Frenkel&Smit

                arg = ((ngas) / (gcmc_pot(k) * volume(ib) * pcav)) * exp(beta * newtotal)

            endif

            if(duni() < arg)  then

                !TU: Atom deletion is successful...

                energytot(ib) = energytot(ib) - newtotal
                enthalpytot(ib) = enthalpytot(ib) - newenth
                virialtot(ib) = virialtot(ib) - newvir
                energyreal(ib) = energyreal(ib) - newreal
                energyvdw(ib) = energyvdw(ib) - newvdw
                energypair(ib) = energypair(ib) - newpair
                energythree(ib) = energythree(ib) - newthree
                energyang(ib) = energyang(ib) - newang
                energyfour(ib) = energyfour(ib) - newfour
                energymany(ib) = energymany(ib) - newmany
                energyext(ib) = energyext(ib) - newext
                energymfa(ib) = energymfa(ib) - newmfa

                sucessfulgcatmremove = sucessfulgcatmremove + 1

                if(job%lmoldata) then

                    do ii = 1, number_of_molecules
                        emolrealnonb(ib,ii) = emolrealnonb(ib,ii) - nrealnonb(ii)
                        emolrealbond(ib,ii) = emolrealbond(ib,ii) - nrealbond(ii)
                        emolvdwn(ib,ii) = emolvdwn(ib,ii) - nvdwn(ii)
                        emolvdwb(ib,ii) = emolvdwb(ib,ii) - nvdwb(ii)
                        emolpair(ib,ii) = emolpair(ib,ii) - npair(ii)
                        emolthree(ib,ii) = emolthree(ib,ii) - nthree(ii)
                        emolang(ib,ii) = emolang(ib,ii) - nang(ii)
                        emolfour(ib,ii) = emolfour(ib,ii) - nfour(ii)
                        emolmany(ib,ii) = emolmany(ib,ii) - nmany(ii)
                        emolext(ib,ii) = emolext(ib,ii) - next(ii)

                        ntot = nrealnonb(ii) + nrealbond(ii) + nvdwn(ii) + nvdwb(ii) + npair(ii) &
                               + nthree(ii) + nang(ii) + nfour(ii) + nmany(ii) + next(ii)

                        emoltot(ib,ii) = emoltot(ib,ii) - ntot
                    enddo

                endif

                !TU: Remove atom from cell list (before the atom is deallocated!)
                if(job%clist) call atom_remove_from_cell_list(ib, im, atm, .true.)

                !delete atom and clean up
                call remove_atom(ib, im, atm, job%uselist, job%lauto_nbrs)
                if(job%uselist) call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist)

            else

                if( job%usecavitybias .and. pcav < 1.0_wp ) &
                    call reset_pcav_remove(ib, job%cavity_radius, cfgs(ib)%mols(im)%atms(atm)%rpos)
                    !call reset_pcav_remove(ib, im, atm, job % cavity_radius)

            endif

        endif

    endif

      !AB: make sure ALL processes in the workgroup aware of any local error and call error(..) -> MPI_FINALIZE(..)
2000  call gsum(fail)!_world(fail)

      if( fail > 0 ) then
          call error(fail) !AB: error: maximum number of atoms reached (about to exceed)
          !call error(207) !AB: error: no gas atoms has been reached - but never checked for that!
      endif

end subroutine gcmc_atoms


!> GCMC moves for allowed molecules (insertions/deletions)
!> the molecules can only be added/deleted when there is at least one "stable" molecular type
!> otherwise the system might end up empty
!> note non-neutral (overall) molecules are not allowed!
subroutine gcmc_mols(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use constants_module, only : uout
    use control_type
    use cell_module
    use field_module
    use species_module
    use atom_module, only : swap_atom_positions
    use molecule_module, only : copy_molecule
    use comms_mpi_module, only : gsum!_world
    use random_module, only : duni
    use species_module, only : uniq_mol
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, &
                                 fed_window_reject
    use vdw_module, only : vdw_lrc_energy
    use cell_list_wrapper_module, only : molecule_set_cells, molecule_remove_from_cell_list

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                     energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                     emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), volume(:)

    integer :: typ, imol, ii, k, kk, n, ngas, nmols, natms, nxch, nsalt, prdct, igrid, last, fail

    real(kind=wp) :: rn, arg, ntot, newreal, newrcp, newvdw, newthree, newpair, newselfcoul, newselfvdw, &
                     newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir, choice, &
                     oldrcp, pcav, deltavb, deltavb_can

    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules),orcp(number_of_molecules), nself(number_of_molecules), &
                       ncrct(number_of_molecules), next(number_of_molecules)

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: old_nums_elemts(number_of_elements), new_nums_elemts(number_of_elements)
    integer :: iatom, atlabel

    logical :: atom_out, overlap, is_salt, do_mol_rcp, reinsert, by_com, relevant_fed_orderparam

    !TU: The value of the bias if required
    real(wp) :: fedbias
    !TU: The 'canonical' acceptance probability in the absence of biasing - for the TM method
    real(wp) :: arg_tm

    type (molecule) :: tmp_mol

    atom_out = .false.
    overlap  = .false.
    is_salt  = .false.
    reinsert = .false.
    by_com   = .false.

    !TU: Flag determining if the order parameter pertains to the number of molecules of the species earmarked
    !TU: for insertion/deletion
    relevant_fed_orderparam = .false.
    relevant_fed_orderparam = ( fed%par_kind == FED_PAR_NMOLS )

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    nrcp = 0.0_wp
    orcp = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    nself = 0.0_wp
    ncrct = 0.0_wp
    next = 0.0_wp

    ! zero energies just in case
    newtotal = 0.0_wp
    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    newrcp = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newfour = 0.0_wp
    newmany = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp
    newselfcoul = 0.0_wp
    newselfvdw = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts

    fail = 0

    !select to either insert or delete
    choice = duni()

    nxch = 0
    ngas = 0

    nmols = 0
    natms = 0

    arg = 0.0_wp

    !TU: Note that this is set to 0 here, and this remains 0 in the event of an overlap so that
    !TU: the transition matrix is correctly updated in the event of a rejection due to an overlap
    arg_tm = 0.0_wp


    if (choice <= 0.5_wp) then



        !TU: Branch for inserting a molecule...
       
        attemptedgcmolinsert = attemptedgcmolinsert + 1

        k = int(duni() * job%numgcmcmol + 1)

        typ = gcinsert_molecules(k)

        !TU: If the order parameter is the number of the specific type of molecule to be 
        !TU: inserted then the order parameter is 'relevant'
        if( fed%par_kind == FED_PAR_NMOLS_SPEC .and. fed%par_nmols_spec_label == typ ) &
            relevant_fed_orderparam = .true.

        !TU: Get the number of molecules of the considered type, and atoms within those molecules
        nmols = cfgs(ib)%mtypes(typ)%num_mols
        natms = nmols * uniq_mol(typ)%natom

        !TU-: What should ngas be?
        ngas = nmols

        nxch = 1
        if( .not. uniq_mol(typ)%rigid_body ) then 

            ngas = natms
            nxch = uniq_mol(typ)%natom

            !TU**: Need to worry about this for lambda charge-dependent functionality?
            if( nxch == 2 ) then
                is_salt = ( uniq_mol(typ)%atms(1)%charge *  uniq_mol(typ)%atms(2)%charge < 0.0_wp )
            endif

        endif

        ! make sure dont go over array limits
        !if( ngas > cfgs(ib)%mxmol .or. ngas > cfgs(ib)%mxmol_type(typ) ) then
        if( nmols > cfgs(ib)%mxmol-1 .or. nmols > cfgs(ib)%mxmol_type(typ)-1 ) then
        
            call dump_revcon(job%revcon_format, -999, energytot)

            fail = 204 !call error(204)
            goto 2000

        endif

        by_com = ( uniq_mol(typ)%natom > 1 .and. &
                 ( uniq_mol(typ)%rigid_body .or. uniq_mol(typ)%blist%npairs > 0 ) )

        !find a position to place atom - it is always placed at the end of the list
        if(job%usecavitybias) then

            pcav = -1.0_wp
            call insert_molecule_atcavity(ib, gcinsert_molecules(k), &
                                          imol, typ, igrid, pcav, atom_out, by_com)

            if (pcav < 0.0) then ! < 0.0 means no free cavity found/hit - do a random insertion

                write(*,*)'gcmc_moves::gcmc_mols(..) WARNING: failed to find a free cavity!'

                call insert_molecule(ib, typ, imol, atom_out, by_com)

                pcav = 1.0

                reinsert = .true.

            endif

        else

            call insert_molecule(ib, typ, imol, atom_out, by_com)

            pcav = 1.0

        endif

        !TU: Set the cells of the atoms in the molecule and insert them into the cell list
        if(job%clist) call molecule_set_cells(ib, imol, .true.)

        !TU: The insert_molecule procedure used above updates cfgs(ib)%nums_elemts to reflect insertion
        new_nums_elemts = cfgs(ib)%nums_elemts
                

#ifdef DEBUG
!debugging!
        write(uout,*)'gcmc_moves::insert_mols(..) - attempted insertion of molecule ', &
                  typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols
#endif
  
        !TU: Note that the 'insert_molecule' and 'insert_molecule_atcavity' procedures
        !TU: add one to the cfgs(ib)%mtypes(typ)%num_mols counter to reflect the fact a
        !TU: molecule has been added.

        !AB: advance randmon number selection to preserve the random sequence
        !rn = duni()

        !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
        if( atom_out ) then

#ifdef DEBUG
!debugging!
            write(uout,*)'gcmc_moves::insert_mols(..) rejection due to atom_out = ', atom_out
#endif
            !if (job%usecavitybias) call reset_cavity0(ib, igrid)
            if( job%usecavitybias .and. .not.reinsert ) &
                call reset_pcav_insert(ib, igrid, job%cavity_radius, cfgs(ib)%mols(imol)%rcom)

            !TU: Remove molecule from cell list (before the molecule is deallocated!)
            if(job%clist) call molecule_remove_from_cell_list(ib, imol, .false.)

            call remove_molecule(ib, imol, fail) !job%uselist, job%lauto_nbrs)

            return

            !TU: WARNING: In the case of an 'atom_out' rejection the usual bookkeeping for a rejection
            !TU: is not done - elsewhere in this procedure there is a 'go to 1000' in the case of a rejection,
            !TU: where the aforementioned bookkeeping is done. Is this bad? For the TM method it means
            !TU: such rejections are not taken into account in the transition matrix!

        end if

        !TU: Calculate the bias, if needed. ** It is important to do this before any prospect of rejection. **
        !TU: Calling 'fed_param_dbias' updates the FED variables 'param_cur', 'id_cur', 'param_old'
        !TU: and 'id_old'. One MUST call 'fed_param_hist_inc' later to either revert these variables in
        !TU: the case of a rejection, or keep them in the case of an acceptance. This is done below.
        if( relevant_fed_orderparam ) fedbias = fed_param_dbias(ib,cfgs(ib))

        !TU: If fedbias is very high here the move takes us out of the considered order parameter
        !TU: range (i.e. above the upper bound for the number of molecules), and hence we reject 
        !TU: the move. In this case we should not update the transition matrix since there is no 
        !TU: FED state corresponding to above the upper bound. Hence we go to lines 1100, which
        !TU: is the code block for rejection but bypassing the tm_inc call
        if( fedbias > vdwcap ) goto 1100

        ! calculate distances and nbrlist
        if(job%uselist) then

            !AB: sets up the exclusions too
            call set_mol_nbrlist_overlap(ib, imol, &
                     job%shortrangecut, job%verletshell, job%gcmcmindist, overlap)

            if (overlap) then

#ifdef DEBUG
!debugging!
                write(uout,*)'gcmc_moves::insert_mols(..) rejection due to dist-overlap = ', &
                          overlap
#endif

                goto 1000 ! reject - intermolecular overlap!

            else

                call molecule_energy2(ib, imol, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir, &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next)

            endif

        else

            call molecule_overlap(ib, imol, job%gcmcmindist, overlap, job%clist)

            if (overlap) then
#ifdef DEBUG
!debugging!
                write(uout,*)'gcmc_moves::insert_mols(..) rejection due to dist-overlap = ', &
                          overlap
#endif

                goto 1000 ! reject - intermolecular overlap!

            else

                call set_molecule_exclist(ib, imol)

                call molecule_energy2_nolist(ib, imol, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next, job%clist)

            endif

        endif

        !TU: newvdw is actually the change in VdW relative to the old configuration. Hence
        !TU: add the CHANGE in long-range VdW correction to this
        newvdw = newvdw + ( vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                          - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts) )


        if( newvdw + newselfvdw + newext > vdwcap ) goto 1000 ! reject - new energy is too high

#ifdef DEBUG
!debugging!
        if( cfgs(ib)%mols(imol)%has_charge .neqv. uniq_mol(typ)%has_charge ) &
            write(*,*)"gcmc_moves::insert_mols(..): charge discrepancies: ", &
                      cfgs(ib)%mols(imol)%has_charge," =?= ",uniq_mol(typ)%has_charge, &
                      cfgs(ib)%mols(imol)%charge," =?= ",uniq_mol(typ)%charge
#endif

        do_mol_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(imol)%has_charge )

        if( do_mol_rcp ) then

            call insert_molecule_recip(ib, imol, newrcp, nrcp, job%lmoldata)

            newrcp  = newrcp - energyrcp(ib)
            nrcp(:) = nrcp   - emolrcp(ib,:)

        endif

        !we have here U_n - U_o
        newtotal = newreal + newrcp + newvdw + newthree + newmany + newselfcoul &
                 + newpair + newang + newfour + newext + newmfa + newselfvdw

        newenth = newtotal + extpress * volume(ib)

#ifdef DEBUG
!debugging!
!        write(uout,*)'gcmc_moves::insert_mols(..) - energy terms upon insertion of molecule ', &
!                   typ," no.",imol,", ngas =",ngas,", nxch =",nxch
!        write(uout,*)'gcmc_moves::insert_mols(..) - newvdw   = ',newvdw,' (int)',newvdw*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newselfv = ',newselfvdw,' (int)',newselfvdw*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newrealc = ',newreal,' (int)',newreal*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newrcpc  = ',newrcp,' (int)',newrcp*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newselfc = ',newselfcoul,' (int)',newselfcoul*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newthree = ',newthree,' (int)',newthree*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newmany  = ',newmany,' (int)',newmany*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newpair  = ',newpair,' (int)',newpair*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newang   = ',newang,' (int)',newang*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newext   = ',newext,' (int)',newext*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newmfa   = ',newmfa,' (int)',newmfa*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newtotal = ',newtotal,' (int)',newtotal*beta,' (kT)'
!        write(uout,*)'gcmc_moves::insert_mols(..) - newlnNV  = ',log(volume(ib)**nxch/(ngas + nxch)
!        write(uout,*)'gcmc_moves::insert_mols(..) - pcav     = ',pcav
!        write(uout,*)'gcmc_moves::insert_mols(..) - chem-pot = ',gcmc_pot(k)
!        write(uout,*)'gcmc_moves::insert_mols(..) - newpv    = ',extpress * volume(ib)*beta
!        write(uout,*)'gcmc_moves::insert_mols(..) -  = ',
#endif
        !write(uout,*)'gcmc_moves::inserting a molecule - attempted (kT)... ',ngas+nxch,nmols+1,imol

        deltavb = beta * newtotal

        if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high

        if( job%usechempotkt ) then
            !AB: input chem. pot. in kT (as is the oroginal input, i.e. no conversion to internal units)
            !AB: the quantum factor for ideal gas entropy is skipped but it only adds a constant shift anyway

            !AB: ideal gas permutation change upon inserting a flexible polymer chain
            if( is_salt ) then

                prdct = (nmols+1)**2

            else
                nxch  = 1
                prdct = nmols+1
!
!                prdct = 1
!                do  n = 1,nxch
!                   prdct = prdct*(ngas+n)
!                enddo
!
            endif

            arg = pcav * exp(gcmc_pot(k) - deltavb) * volume(ib)**nxch  / (prdct)

        elseif( job%usechempotkj) then
            !AB: input chem. pot. in kJ/mol (but input must be converted to internal units) 

            !AB: ideal gas permutation change upon deleting a flexible polymer chain
            if( is_salt ) then

                prdct = (nmols+1)**2

            else
                nxch  = 1
                prdct = nmols+1
!
!                prdct = 1
!                do  n = 1,nxch
!                   prdct = prdct*(ngas+n)
!                enddo
!
            endif

            !AB: ideal gas & permutation contributions added (flexible molecules also covered)
            !arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) * volume(ib)**nxch / (prdct)
            arg = pcav * exp(gcmc_pot(k)*beta - deltavb) * volume(ib)**nxch / (prdct)

            ! Adams/Mezei 
            !AB: what about the ideal gas entropy (V^N+1/V^N) contribution?
            !arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) / (ngas + 1)

        elseif( job%usegaspres ) then
            !AB: molecules are always treated as rigid-bodies
            !AB: this is the same as below using Muller/Fuchs scheme where activity = P/RT
            
            !AB: here input is the partial (ideal gas) pressure for a species
            !AB: a more general approximation would be: P_i = RT*a_i*(1-B2_i*a_i)

            !arg = (beta * pcav * volume(ib) * gcmc_pot(k) / (ngas + 1)) * exp(-beta * newtotal)
            arg = (beta * pcav * volume(ib) * gcmc_pot(k) / (nmols + 1)) * exp(-deltavb)

        else!AB: use "chemical activity" = exp(mu_i/kT)/Lambda_i^3
            !AB: molecules are always treated as rigid-bodies

            ! Muller/Fuchs

            !arg = ((pcav * volume(ib) * gcmc_pot(k)) / (ngas + 1)) * exp(-beta * newtotal)
            arg = ((pcav * volume(ib) * gcmc_pot(k)) / (nmols + 1)) * exp(-deltavb)

        endif

        !TU: Set the unbiased/canonical value of 'arg' if required for updating the transition matrix
        !TU: to the value calculated above, which is suitable...
        if( fed % method == FED_TM .and. relevant_fed_orderparam ) arg_tm = arg
        
        !TU: ... unless we are biasing over certain order parameters, in which case update 'arg' 
        !TU: accordingly
        if( relevant_fed_orderparam ) then

           arg = arg * exp( - fedbias )

        end if

        !TU: Reject the move if it leaves the window or takes us further from the window
        if( fed % is_window .and. relevant_fed_orderparam) then

           if( fed_window_reject() ) goto 1000
           
        end if

        rn = duni()

        !test for accept or rejection
        !if( duni() < arg ) then
        if( rn < arg ) then


            !TU: Code for insertion accepted...


#ifdef DEBUG
!debugging!
            if( job%usegaspres ) then !AB: in katms (pressumably)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(GasPressure) - insertion accepted (energy in kT) : ', &
                pcav,' * ',beta,' * exp(',log(volume(ib)*gcmc_pot(k)/(nmols+1)), &
                ' - ',newtotal*beta,') = ', arg 

            !arg = (beta * pcav * volume(ib) * gcmc_pot(k) / ((nmols) + 1)) * exp(-beta * newtotal)

            elseif( job%usechempotkt ) then !AB: in kT (as is input apparently!)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(ChemPotkT) - insertion accepted (energy in kT) : ', &
                pcav,' * exp(',log(volume(ib)**nxch/(ngas + nxch)),' + ',gcmc_pot(k), &
                ' - ',newtotal*beta,') = ', arg 

            !arg = pcav * (volume(ib)**nxch  / (ngas + nxch)) * exp(gcmc_pot(k) - newtotal*beta)

            elseif( job%usechempotkj ) then !AB: in kJ/mol (but input must be converted!)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(ChemPotkJ) - insertion accepted (energy in kT) : ', &
                pcav,' * exp(',gcmc_pot(k)*beta,' - ',newtotal*beta,') / ',(nmols+1),' = ', arg 

            !arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) / (nmols + 1)

            else

              write(uout,'(4(a,e13.5))') &
                'gcmc_moves::insert_mols(Activity) - insertion accepted (energy in kT) : ', &
                pcav,' * exp(',log(volume(ib)*gcmc_pot(k)/(nmols+1)),' - ',newtotal*beta,') = ', arg 

            !arg = ((pcav * volume(ib) * gcmc_pot(k)) / (nmols + 1)) * exp(-beta * newtotal)

            endif

            write(uout,*)'gcmc_moves::insert_mols(..) - accepted insertion of molecule ', &
                  typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols
#endif

            !TU: Update the transition matrix using the 'canonical' probability of acceptance, arg_tm,
            !TU: set above.
            if( fed % method == FED_TM .and. relevant_fed_orderparam ) call fed_tm_inc(  min( 1.0_wp, arg_tm  ) )

            !TU: If the move is accepted then update FED variables accordingly
            if( relevant_fed_orderparam ) then

                call fed_param_hist_inc(ib,.true.)

            end if

            !if accept add permanently to list
            energytot(ib)   = energytot(ib)   + newtotal
            enthalpytot(ib) = enthalpytot(ib) + newenth
            virialtot(ib)   = virialtot(ib)   + newvir
            energyreal(ib)  = energyreal(ib)  + newreal + newselfcoul
            energyvdw(ib)   = energyvdw(ib)   + newvdw  + newselfvdw
            energypair(ib)  = energypair(ib)  + newpair
            energyfour(ib)  = energyfour(ib)  + newfour
            energythree(ib) = energythree(ib) + newthree
            energyang(ib)   = energyang(ib)   + newang
            energymany(ib)  = energymany(ib)  + newmany
            energyext(ib)   = energyext(ib)   + newext
            energymfa(ib)   = energymfa(ib)   + newmfa

            sucessfulgcmolinsert = sucessfulgcmolinsert + 1

            if(job%lmoldata) then

                do ii = 1, number_of_molecules
                    emolrealnonb(ib,ii) = emolrealnonb(ib,ii) + nrealnonb(ii)
                    emolrealbond(ib,ii) = emolrealbond(ib,ii) + nrealbond(ii)
                    emolrealself(ib,ii) = emolrealself(ib,ii) + nself(ii)
                    emolrealcrct(ib,ii) = emolrealcrct(ib,ii) + ncrct(ii)
                    emolrcp(ib,ii)      = emolrcp(ib,ii) + nrcp(ii)
                    emolvdwn(ib,ii)     = emolvdwn(ib,ii) + nvdwn(ii)
                    emolvdwb(ib,ii)     = emolvdwb(ib,ii) + nvdwb(ii)
                    emolpair(ib,ii)     = emolpair(ib,ii) + npair(ii)
                    emolthree(ib,ii)    = emolthree(ib,ii) + nthree(ii)
                    emolang(ib,ii)      = emolang(ib,ii) + nang(ii)
                    emolfour(ib,ii)     = emolfour(ib,ii) + nfour(ii)
                    emolmany(ib,ii)     = emolmany(ib,ii) + nmany(ii)
                    emolext(ib,ii)      = emolext(ib,ii) + next(ii)

                    ntot = nrealnonb(ii) + nrealbond(ii) + nrcp(ii) + nvdwn(ii) + nvdwb(ii) + npair(ii)   &
                         + nthree(ii) + nang(ii) + nfour(ii) + nmany(ii) + nself(ii) + ncrct(ii) + next(ii)

                    emoltot(ib,ii) = emoltot(ib,ii) + ntot
                enddo

            endif

            if( job%uselist ) call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist)

            if( do_mol_rcp ) then
                energyrcp(ib) = energyrcp(ib) + newrcp
                call update_rcpsums(ib)
            endif

            goto 2000

            return

        !else
        endif


            !TU: Code for insertion rejected...


            !reject - delete molecule and clean nbrlist/exclist and recalculate ewald sums

1000        continue

            !TU: Update the transition matrix if required with the 'canonical' probability of acceptance, arg_tm,
            !TU: set above. Note that 'arg_tm' is 0 if the move has been rejected due to an overlap
            if( fed % method == FED_TM .and. relevant_fed_orderparam ) call fed_tm_inc(  min( 1.0_wp, arg_tm  ) )


1100        continue  !TU: 1100 is used for rejection when we don't want to update the transition-matrix (see above)

            !TU: Update histogram and bias if needed. Note that the second argument to 'fed_param_hist_inc'
            !TU: is whether or not the move was accepted or not, and that if .false. then the internal FED
            !TU: order parameter variables are reverted to their old values.
            if( relevant_fed_orderparam ) then

                call fed_param_hist_inc(ib,.false.)

            end if

            !if (job%usecavitybias) call reset_cavity0(ib, igrid)
            if( job%usecavitybias .and. .not.reinsert ) &
                call reset_pcav_insert(ib, igrid, job%cavity_radius, cfgs(ib)%mols(imol)%rcom)

            !TU: Remove atoms in molecule from cell list (before the molecule is deallocated!)
            if(job%clist) call molecule_remove_from_cell_list(ib, imol, .false.)

            call remove_molecule(ib, imol, fail) !, job%uselist, job%lauto_nbrs)

            if(job%uselist) then

                call clear_mol_nbrlist(ib, imol)

            endif

            call clear_mol_exclist(ib, imol)




#ifdef DEBUG
!debugging!
            if( job%usegaspres ) then !AB: in katms (pressumably)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(GasPressure) - insertion rejected (energy in kT) : ', &
                pcav,' * ',beta,' * exp(',log(volume(ib)*gcmc_pot(k)/((nmols)+1)), &
                ' - ',newtotal*beta,') = ', arg 

            !arg = (beta * pcav * volume(ib) * gcmc_pot(k) / ((nmols) + 1)) * exp(-beta * newtotal)

            elseif( job%usechempotkt ) then !AB: in kT (as is input apparently!)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(ChemPotkT) - insertion rejected (energy in kT) : ', &
                pcav,' * exp(',log(volume(ib)**nxch/(ngas + nxch)),' + ',gcmc_pot(k), &
                ' - ',newtotal*beta,') = ', arg 

            !arg = pcav * (volume(ib)**nxch  / (ngas + nxch)) * exp(gcmc_pot(k) - newtotal*beta)

            elseif( job%usechempotkj ) then !AB: in kJ/mol (but input must be converted!)

              write(uout,'(5(a,e13.5))') &
                'gcmc_moves::insert_mols(ChemPotkJ) - insertion rejected (energy in kT) : ', &
                pcav,' * exp(',gcmc_pot(k)*beta,' - ',newtotal*beta,') / ',(nmols+1),' = ', arg 

            !arg = pcav * exp((gcmc_pot(k) - newtotal) * beta) / (nmols + 1)

            else

              write(uout,'(4(a,e13.5))') &
                'gcmc_moves::insert_mols(Activity) - insertion rejected (energy in kT) : ', &
                pcav,' * exp(',log(volume(ib)*gcmc_pot(k)/((nmols)+1)),' - ',newtotal*beta,') = ', arg 

            !arg = ((pcav * volume(ib) * gcmc_pot(k)) / ((nmols)+1)) * exp(-beta * newtotal)

            endif

            write(uout,*)'gcmc_moves::insert_mols(..) - rejected insertion of molecule ', &
                  typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols
#endif
        !endif

        goto 2000

        return


    else  ! remove a molecule


        !TU: Branch for removing a molecule...


        attemptedgcmolremove = attemptedgcmolremove + 1

        k = int(duni() * job%numgcmcmol + 1)

        typ = gcinsert_molecules(k)

        !TU: If the order parameter is the number of the specific type of molecule to be 
        !TU: inserted then the order parameter is 'relevant'
        if( fed%par_kind == FED_PAR_NMOLS_SPEC .and. fed%par_nmols_spec_label == typ ) &
            relevant_fed_orderparam = .true.


        !TU: Get the number of molecules of the considered type, and atoms within those molecules
        !TU: (Note that 'nmols' henceforth is the number of molecules BEFORE any deletion)
        nmols = cfgs(ib)%mtypes(typ)%num_mols
        natms = nmols * uniq_mol(typ)%natom

        !TU-: What should ngas be? Should it use cfgs(ib)%num_elemts?
        ngas = nmols
        
        !if( ngas < 1 ) then
        !    fail = 208 !call error(208)
        !    goto 2000
        !endif

        nxch = 1
        if( .not. uniq_mol(typ)%rigid_body ) then 

            ngas = natms
            nxch = uniq_mol(typ)%natom

            !TU**: Need to worry about this for lambda charge-dependent functionality?
            if( nxch == 2 ) then
                is_salt = ( uniq_mol(typ)%atms(1)%charge * uniq_mol(typ)%atms(2)%charge < 0.0_wp )
            endif

        endif


        if( ngas == 0 ) then

            emptygcmolremove = emptygcmolremove + 1

            !TU: If there are no molecules then the rejection move should be rejected. Note that this still
            !TU: counts as a legitimate move for the purposes of gathering statistics. Hence we must update
            !TU: the transition matrix, among other things.

            if( relevant_fed_orderparam ) then

                !TU**: Is this the best way to do this? It seems contrary to the spirit of Andrey's FED interface,
                !TU**: and not easily extensible to other relevant order parameters. Find a better way? Can we just
                !TU**: use the current state as the 'new' state and reject it implicitly - as we usually do?

                if( fed%par_kind == FED_PAR_NMOLS ) then

                    !TU: Order parameter is total number of molecules in the system...

                    !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_olf'
                    !TU: and 'param_cur'. The arguments reflect the fact that this is a move from a microstate 
                    !TU: with 0 molecules to another microstate with 0 molecules; the current and trial order 
                    !TU: parameters (both 0) are given explicitly via the 'fed_param_dbias2' procedure.
                    !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
                    !TU: NB: For systems with more than 1 species, ngas=0 does not necessarily correspond to
                    !TU: 0 TOTAL molecules in the system. In this case the transition is from a microstate with
                    !TU: cfgs(ib)%num_mols to another with the same total number of molecules
                    fedbias = fed_param_dbias2(cfgs(ib)%num_mols*1.0_wp, cfgs(ib)%num_mols*1.0_wp, .true.)

                else if( fed%par_kind == FED_PAR_NMOLS_SPEC ) then

                    !TU: Based on the logic above the order parameter must be the current number of molecules belonging
                    !TU: to the species under consideration....

                    !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_olf'
                    !TU: and 'param_cur'. The arguments reflect the fact that this is a move from a microstate 
                    !TU: with 0 molecules to another microstate with 0 molecules; the current and trial order 
                    !TU: parameters (both 0) are given explicitly via the 'fed_param_dbias2' procedure.
                    !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
                    !TU: Recall that 'nmols' is the number of molecules belonging to the species under consideration.
                    fedbias = fed_param_dbias2(nmols*1.0_wp, nmols*1.0_wp, .true.)

                end if

            end if

            !TU: Update the transition matrix as a rejection
            !TU: (If FED_PAR_NMOLS is the order parameter then counting the move as a rejection or acceptance
            !TU: have the same effect on the transition matrix - they are equivalent).
            if( fed%method == FED_TM .and. relevant_fed_orderparam ) call fed_tm_inc(0.0_wp)

            if( relevant_fed_orderparam ) then
               
               !TU: The move is counted as a rejected FED move
               call fed_param_hist_inc(ib,.false.)
               
            end if

        !TU: Should this just be an 'else' instead of 'else if'?
        else if( ngas > 0 ) then ! only if we have an mol to remove
            
            !TU: select_molecule would hang if there are no atoms of the specified type - won't happen due to above check
            call select_molecule(typ, ib, imol)

            if( is_salt ) then

                call select_molecule(typ, ib, ii)
                call swap_atom_positions(cfgs(ib)%mols(imol)%atms(1),cfgs(ib)%mols(ii)%atms(1))

            endif

            if( nxch > 1 ) nxch = cfgs(ib)%mols(imol)%natom

            by_com = ( uniq_mol(typ)%natom > 1 .and. &
                     ( uniq_mol(typ)%rigid_body .or. uniq_mol(typ)%blist%npairs > 0 ) )

            pcav = 1.0_wp

            !if(job%usecavitybias) call calc_pcav_molremove(ib, imol, job % cavity_radius, pcav)
            if( job%usecavitybias ) &
                call calc_pcav_remove_mol(ib, imol, job%cavity_radius, pcav, by_com)

            !TU: Note that we haven't yet removed the molecule (which happens only upon acceptance)
            !TU: and so must manually set new_nums_elemts (instead of just setting it to 
            !TU: cfgs(ib)%nums_elemts as is done in insertion)
            do iatom = 1, cfgs(ib)%mols(imol)%natom

                atlabel = cfgs(ib)%mols(imol)%atms(iatom)%atlabel
                new_nums_elemts(atlabel) = new_nums_elemts(atlabel) - 1

            end do
            

            ! evaluate energy
            if(job%uselist) then

                call molecule_energy2(ib, imol, job%coultype(ib), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, & 
                             newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                             newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                             npair, nthree, nang, nfour, nmany, next)

            else

                call molecule_energy2_nolist(ib, imol, job%coultype(ib), &
                             job%dielec, job%vdw_cut, job%shortrangecut, &
                             newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                             newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                             nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                             npair, nthree, nang, nfour, nmany, next, job%clist)

            endif

#ifdef DEBUG
!debugging!
            if( cfgs(ib)%mols(imol)%has_charge .neqv. uniq_mol(typ)%has_charge ) &
                write(*,*)"gcmc_moves::remove_mols(..): charge discrepancies: ", &
                      cfgs(ib)%mols(imol)%has_charge," =?= ",uniq_mol(typ)%has_charge, &
                      cfgs(ib)%mols(imol)%charge," =?= ",uniq_mol(typ)%charge, &
                      imol, cfgs(ib)%num_mols
#endif

            do_mol_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(imol)%has_charge )

            if( do_mol_rcp ) then

                call remove_molecule_recip(ib, imol, newrcp, nrcp, job%lmoldata)

                newrcp  = energyrcp(ib) - newrcp
                nrcp(:) = emolrcp(ib,:) - nrcp(:)

            endif

            !TU: newvdw is actually the negative of the change in VdW relative to the old configuration. Hence 
            !TU: add the negative of the CHANGE in long-range VdW correction to this
            newvdw = newvdw - ( vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                              - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts) )

            !we have here U_o - U_n
            newtotal = newreal + newrcp + newvdw + newthree + newmany + newselfcoul &
                     + newpair + newang + newext + newmfa + newselfvdw

            newenth = newtotal + extpress * volume(ib)


            !TU: Calculate the bias, if needed. ** It is important to do this before any prospect of rejection. **
            !TU: Calling 'fed_param_dbias' updates the FED variables 'param_cur', 'id_cur', 'param_old'
            !TU: and 'id_old'. One MUST call 'fed_param_hist_inc' later to either revert these variables in
            !TU: the case of a rejection or keep them in the case of an acceptance. This is done below.
            if( relevant_fed_orderparam ) then

                if( fed%par_kind == FED_PAR_NMOLS ) then

                    !TU: Order parameter is total number of molecules in the system...

                    !TU: Note that we specify the trial order parameter explicitly via the 'fed_param_dbias2' procedure
                    !TU: (the second argument in that procedure). This is because the trial configuration, in terms of 
                    !TU: the data structure, still has the same number of molecules as the 'current' configuration - the 
                    !TU: molecule to be removed is not actually removed until the move is accepted.
                    fedbias = fed_param_dbias2(1.0_wp*cfgs(ib)%num_mols, 1.0_wp*(cfgs(ib)%num_mols - 1), &
                                               .true.)

                else if( fed%par_kind == FED_PAR_NMOLS_SPEC ) then

                    !TU: Based on the logic above the order parameter must be the current number of molecules belonging
                    !TU: to the species under consideration....

                    !TU: Note that we specify the trial order parameter explicitly via the 'fed_param_dbias2' procedure
                    !TU: (the second argument in that procedure). This is because the trial configuration, in terms of 
                    !TU: the data structure, still has the same number of molecules as the 'current' configuration - the 
                    !TU: molecule to be removed is not actually removed until the move is accepted.
                    !TU: Recall that 'nmols' is the number of molecules belonging to the species under consideration.
                    fedbias = fed_param_dbias2(1.0_wp*nmols, 1.0_wp*(nmols - 1), .true.)

                end if

            end if

#ifdef DEBUG
!debugging!
            write(uout,*)'gcmc_moves::insert_mols(..) - attempted deletion of molecule ', &
                      typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols

!            write(uout,*)'gcmc_moves::insert_mols(..) - energy terms upon deletion of molecule ', &
!                       typ," no.",imol,", ngas =",ngas,", nxch =",nxch
!            write(uout,*)'gcmc_moves::insert_mols(..) - newvdw   = ',-newvdw,' (int)',-newvdw*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newselfv = ',-newselfvdw,' (int)',-newselfvdw*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newrealc = ',-newreal,' (int)',-newreal*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newrcpc  = ',-newrcp,' (int)',-newrcp*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newselfc = ',-newselfcoul,' (int)',-newselfcoul*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newthree = ',-newthree,' (int)',-newthree*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newmany  = ',-newmany,' (int)',-newmany*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newpair  = ',-newpair,' (int)',-newpair*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newang   = ',-newang,' (int)',-newang*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newext   = ',-newext,' (int)',-newext*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newmfa   = ',-newmfa,' (int)',-newmfa*beta,' (kT)'
!            write(uout,*)'gcmc_moves::insert_mols(..) - newtotal = ',-newtotal,' (int)',-newtotal*beta,' (kT)'
!           write(uout,*)'gcmc_moves::insert_mols(..) - newlnNV  = ',log((ngas)/volume(ib)**nxch)
!           write(uout,*)'gcmc_moves::insert_mols(..) - pcav     = ',pcav
!           write(uout,*)'gcmc_moves::insert_mols(..) - chem-pot = ',gcmc_pot(k)
!           write(uout,*)'gcmc_moves::insert_mols(..) - newpv    = ',extpress * volume(ib)*beta
#endif
            !write(uout,*)'gcmc_moves::deleting a molecule - attempted (kT)... ',ngas,nmols,imol,ii


            deltavb = beta * newtotal

            if(job%usegaspres) then
                !AB: activity = P/RT 
                !AB: more generally P = RT*a_i*(1 - B2_i*a_i)

                !arg = ( (ngas) / (gcmc_pot(k) * pcav * volume(ib) * beta) ) * exp( beta*newtotal )
                arg = ( (nmols) / (gcmc_pot(k) * pcav * volume(ib) * beta) ) * exp( deltavb )

            elseif(job%usechempotkt) then

                !AB: ideal gas permutation change upon deleting a flexible polymer chain (covers rigid-bodies too)
                if( is_salt ) then
                    !write(uout,*)'gcmc_moves::deleting a salt pair - attempted (kT)... ',nmols,imol,ii

                    prdct = nmols**2

                else

                    nxch  = 1
                    prdct = nmols
!
!                    prdct = 1
!                    do  n = ngas-nxch+1,ngas
!                       prdct = prdct*n
!                    enddo
!
                endif

                !arg = exp( -gcmc_pot(k) + beta*newtotal ) * (ngas) / (volume(ib)**nxch * pcav)
                arg = exp( -gcmc_pot(k) + deltavb ) * (prdct) / (volume(ib)**nxch * pcav)

            elseif(job%usechempotkj) then

                !AB: ideal gas permutation change upon deleting a flexible polymer chain (covers rigid-bodies too)
                if( is_salt ) then

                    prdct = nmols**2

                else

                    nxch  = 1
                    prdct = nmols
!
!                    prdct = 1
!                    do  n = ngas-nxch+1,ngas
!                       prdct = prdct*n
!                    enddo
!
                endif

                !AB: ideal gas permutation corrected for flexible molecules
                arg = exp( -gcmc_pot(k)*beta + deltavb ) * (prdct) / (volume(ib)**nxch * pcav)

                !original method by Adams/Mezei
                !arg = exp( (-gcmc_pot(k) + newtotal) * beta ) * (ngas) / pcav

            else!AB: use "chemical activity" = exp(mu_i/kT)/Lambda_i^3
                !Fuchs/Muller/Frenkel&Smit

                !arg = ((ngas) / (gcmc_pot(k) * volume(ib) * pcav)) * exp(beta * newtotal)
                arg = ((nmols) / (gcmc_pot(k) * volume(ib) * pcav)) * exp(deltavb)

            endif

            !TU: Set the unbiased/canonical value of 'arg' if required for updating the transition matrix
            !TU: to the value calculated above, which is suitable...
            if( fed % method == FED_TM .and. relevant_fed_orderparam ) arg_tm = arg
            
            !TU: ... unless we are biasing over certain order parameters, in which case update 'arg' 
            !TU: accordingly
            if( relevant_fed_orderparam ) then
               
               arg = arg * exp( -fedbias )
               
            end if


            !TU: Reject the move if it leaves the window or takes us further from the window                          
            if( fed % is_window .and. relevant_fed_orderparam ) then
               
                if( fed_window_reject() ) goto 3000
               
            end if

            rn = duni()

            !test for accept or rejection
            !if( duni() < arg ) then
            if( rn < arg ) then


                !TU: Deletion move is accepted...


#ifdef DEBUG
!debugging!
                if( job%usegaspres ) then !AB: in katms (pressumably)

                  write(uout,'(4(a,e13.5))') &
                    'gcmc_moves::insert_mols(GasPressure) - deletion accepted (energy in kT) : exp(', &
                    log( (nmols) / (volume(ib) * gcmc_pot(k) * beta) ), &
                    ' + ',beta*newtotal,') / ',pcav,' = ',arg 

                !arg = ( (nmols) / (gcmc_pot(k) * volume(ib) * beta) ) * exp( beta*newtotal ) / pcav

                elseif( job%usechempotkt ) then !AB: in kT (as is input apparently!)

                  write(uout,'(5(a,e13.5))') &
                    'gcmc_moves::insert_mols(ChemPotkT) - deletion accepted (energy in kT) : exp(', &
                    log((ngas) / volume(ib)**nxch),' - ',gcmc_pot(k), &
                    ' + ',beta*newtotal,') / ',pcav,' = ', arg 

                !arg = ( (ngas) / volume(ib)**nxch ) * exp( gcmc_pot(k) + beta*newtotal ) / pcav

                elseif( job%usechempotkj ) then !AB: in kJ/mol (but input must be converted!)

                  write(uout,'(5(a,e13.5))') &
                    'gcmc_moves::insert_mols(ChemPotkJ) - deletion accepted (energy in kT) : exp(', &
                    -beta*gcmc_pot(k),' + ',beta*newtotal,') * ',(nmols),' / ',pcav,' = ', arg 

                !arg = exp( (gcmc_pot(k) + newtotal)*beta ) * (nmols) / pcav

                else

                  write(uout,'(4(a,e13.5))') &
                    'gcmc_moves::insert_mols(Activity) - deletion accepted (energy in kT) : exp(', &
                    log((nmols) / (volume(ib)*gcmc_pot(k)) ),' + ',beta*newtotal,') / ',pcav,' = ', arg 

                !arg = ( (nmols) / (volume(ib) * gcmc_pot(k)) ) * exp( beta * newtotal ) / pcav

                endif
#endif

                !if accept add permanently to list
                energytot(ib) = energytot(ib) - newtotal
                enthalpytot(ib) = enthalpytot(ib) - newenth
                virialtot(ib) = virialtot(ib) - newvir
                energyreal(ib) = energyreal(ib) - (newreal + newselfcoul)
                energyvdw(ib) = energyvdw(ib) - (newvdw + newselfvdw)
                energypair(ib) = energypair(ib) - newpair
                energythree(ib) = energythree(ib) - newthree
                energyang(ib) = energyang(ib) - newang
                energyfour(ib) = energyfour(ib) - newfour
                energymany(ib) = energymany(ib) - newmany
                energyext(ib) = energyext(ib) - newext
                energymfa(ib) = energymfa(ib) - newmfa

                sucessfulgcmolremove = sucessfulgcmolremove + 1

                if(job%lmoldata) then

                     do ii = 1, number_of_molecules
                         emolrealnonb(ib,ii) = emolrealnonb(ib,ii) - nrealnonb(ii)
                         emolrealbond(ib,ii) = emolrealbond(ib,ii) - nrealbond(ii)
                         emolrealself(ib,ii) = emolrealself(ib,ii) - nself(ii)
                         emolrealcrct(ib,ii) = emolrealcrct(ib,ii) - ncrct(ii)
                         emolrcp(ib,ii) = emolrcp(ib,ii) - nrcp(ii)
                         emolvdwn(ib,ii) = emolvdwn(ib,ii) - nvdwn(ii)
                         emolvdwb(ib,ii) = emolvdwb(ib,ii) - nvdwb(ii)
                         emolpair(ib,ii) = emolpair(ib,ii) - npair(ii)
                         emolthree(ib,ii) = emolthree(ib,ii) - nthree(ii)
                         emolang(ib,ii) = emolang(ib,ii) - nang(ii)
                         emolfour(ib,ii) = emolfour(ib,ii) - nfour(ii)
                         emolmany(ib,ii) = emolmany(ib,ii) - nmany(ii)
                         emolext(ib,ii) = emolext(ib,ii) - next(ii)

                         ntot = nrealnonb(ii) + nrealbond(ii) + nrcp(ii) + nvdwn(ii) + nvdwb(ii) + npair(ii)   &
                                + nthree(ii) + nang(ii) + nfour(ii) + nmany(ii) + nself(ii) + ncrct(ii) + next(ii)

                         emoltot(ib,ii) = emoltot(ib,ii) - ntot
                     enddo

                endif

                !TU: Remove atoms in molecule from cell list (before the molecule is deallocated!)
                if(job%clist) call molecule_remove_from_cell_list(ib, imol, .true.)
                
                call remove_molecule(ib, imol, fail) !, job%uselist, job%lauto_nbrs)


                !TU: Update the transition matrix if required with the 'canonical' probability of acceptance, arg_tm,
                !TU: set above.
                if( fed % method == FED_TM .and. relevant_fed_orderparam ) call fed_tm_inc(  min( 1.0_wp, arg_tm  ) )

                if( relevant_fed_orderparam ) then

                    call fed_param_hist_inc(ib,.true.)

                end if

                if(job%uselist) then

                    call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist)

                    if(job%lauto_nbrs) call set_rzero(ib)

                endif

                !else

                    !AB: the exclusion lists have been copied already in remove_molecule
                    !call set_exclist(ib)
                    !call update_glob_no(ib)

                !endif

                if( do_mol_rcp ) then
                    energyrcp(ib) = energyrcp(ib) - newrcp
                    call update_rcpsums(ib)
                endif

#ifdef DEBUG
!debugging!
                write(uout,*)'gcmc_moves::insert_mols(..) - accepted deletion of molecule ', &
                      typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols
#endif

                goto 2000

            end if


            !TU: Code if deletion move is rejected...


3000            continue

                
                !if(job%usecavitybias) call reset_pcav_molremove(ib, imol, job % cavity_radius)
                if( job%usecavitybias ) &
                    call reset_pcav_remove(ib, job%cavity_radius, cfgs(ib)%mols(imol)%rcom)

                !TU: Update the transition matrix if required with the 'canonical' probability of acceptance, arg_tm,
                !TU: set above. Note that 'arg_tm' is 0 if the move has been rejected due to an overlap
                if( fed % method == FED_TM .and. relevant_fed_orderparam ) call fed_tm_inc(  min( 1.0_wp, arg_tm  ) )

                !TU: Update histogram and bias if needed
                if( relevant_fed_orderparam ) then

                    call fed_param_hist_inc(ib,.false.)

                end if

#ifdef DEBUG
!debugging!
                if( job%usegaspres ) then !AB: in katms (pressumably)

                  write(uout,'(4(a,e13.5))') &
                    'gcmc_moves::insert_mols(GasPressure) - deletion rejected (energy in kT) : exp(', &
                    log( nmols / (volume(ib) * gcmc_pot(k) * beta) ), &
                    ' + ',beta*newtotal,') / ',pcav,' = ',arg 

                !arg = ( nmols / (gcmc_pot(k) * volume(ib) * beta) ) * exp( beta*newtotal ) / pcav

                elseif( job%usechempotkt ) then !AB: in kT (as is input apparently!)

                  write(uout,'(5(a,e13.5))') &
                    'gcmc_moves::insert_mols(ChemPotkT) - deletion rejected (energy in kT) : exp(', &
                    log((ngas) / volume(ib)**nxch),' - ',gcmc_pot(k), &
                    ' + ',beta*newtotal,') / ',pcav,' = ', arg 

                !arg = ( ngas / volume(ib)**nxch ) * exp( gcmc_pot(k) + beta*newtotal ) / pcav

                elseif( job%usechempotkj ) then !AB: in kJ/mol (but input must be converted!)

                  write(uout,'(5(a,e13.5))') &
                    'gcmc_moves::insert_mols(ChemPotkJ) - deletion rejected (energy in kT) : exp(', &
                    -beta*gcmc_pot(k),' + ',beta*newtotal,') * ',(nmols),' / ',pcav,' = ', arg 

                !arg = exp( (gcmc_pot(k) + newtotal)*beta ) * nmols / pcav

                else

                  write(uout,'(4(a,e13.5))') &
                    'gcmc_moves::insert_mols(Activity) - deletion rejected (energy in kT) : exp(', &
                    log((nmols) / (volume(ib)*gcmc_pot(k)) ),' + ',beta*newtotal,') / ',pcav,' = ', arg 

                !arg = ( nmols / (volume(ib) * gcmc_pot(k)) ) * exp( beta * newtotal ) / pcav

                endif 

                write(uout,*)'gcmc_moves::insert_mols(..) - rejected deletion of molecule ', &
                      typ," no.",imol,", ngas =",ngas,", nxch =",nxch," nmols_tot =",cfgs(ib)%num_mols
#endif
            
        
          endif    !TU: End of 'if(ngas>0)' branch

      endif     !TU: End of deletion branch

      !AB: make sure ALL processes in the workgroup aware of any local error and call error(..) -> MPI_FINALIZE(..)
2000  call gsum(fail) !_world(fail)

      if( fail > 0 ) then 
          call error(fail) !AB: error: maximum number of molecules reached (about to exceed)
          !call error(208) !AB: error: no gas molecules has been reached - but never checked for that!
      endif


end subroutine gcmc_mols


!> semi-grand cononical ensemble for atoms
!> note no reciprocal space and no charge mutations are allowed!
subroutine semiwidom_atoms(ib, job, iter, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use cell_module
    use field_module
    use control_type
    use species_module
    use statistics_module
    use random_module, only : duni
    use constants_module, only : ATM_TYPE_SEMI
    use vdw_module, only : vdw_lrc_energy

    implicit none

    integer, intent(in) :: ib, iter
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                     energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                     volume(nconfigs)

    integer :: im, i, k, numtype1, numtype2, atyp

    real(kind = wp) :: deltav,  deltavdwlrc, &
       oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
       newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir


    ! temp vectors for molecular energies
    real(kind = wp) :: trealnonb(number_of_molecules), trealbond(number_of_molecules), &
                       tvdwn(number_of_molecules), tvdwb(number_of_molecules), tpair(number_of_molecules), &
                       tthree(number_of_molecules), tang(number_of_molecules), tfour(number_of_molecules), &
                       tmany(number_of_molecules), tcrct(number_of_molecules), text(number_of_molecules)

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: old_nums_elemts(number_of_elements), new_nums_elemts(number_of_elements)


    nummutations = nummutations + 1

    !select mutaion
    k = int(duni() * job%numsemiwidomatms + 1)

    !TU: Obsolete function call; replace with cfgs(ib)%num_elemts. See 'findnumtype' code for details
    call findnumtype(ib, semitype1(k), numtype1)
    call findnumtype(ib, semitype2(k), numtype2)

    if(numtype2 == 0 .or. numtype1 == 0) return

    !TU: select_atom will hang if there are none of specified type - won't happen due to above check
    !select the atom to be mutated
    call select_atom(semitype1(k), ib, im, i)

    atyp = cfgs(ib)%mols(im)%atms(i)%atype

    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldmany = 0.0_wp
    oldfour = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts


    !energy of atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, &
                             oldfour, oldext, oldmfa, oldvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false.)

        else

            call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                             job%dielec, job%vdw_cut, job%shortrangecut, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, &
                             oldfour, oldext, oldmfa, oldvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false., job%clist)

        endif

    else

        call atom_tersoff_energy(ib, im, i, job%shortrangecut, oldmany, oldvir)

    endif

    oldtotal = oldreal + oldvdw + oldthree + oldpair + oldang &
             + oldmany + oldfour + oldext + oldmfa

    !mutate atom
    call mutate_atom(ib, im, i, semitype2(k))

    !TU: The mutate_atom procedure used above updates cfgs(ib)%nums_elemts to reflect mutation
    new_nums_elemts = cfgs(ib)%nums_elemts

    atyp = cfgs(ib)%mols(im)%atms(i)%atype

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newmany = 0.0_wp
    newfour = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    !calc energy of mutated atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, &
                             newreal, newvdw, newthree, newpair, newang, &
                             newfour, newext, newmfa, newvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false.)

        else

            call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                             job%dielec, job%vdw_cut, job%shortrangecut, &
                             newreal, newvdw, newthree, newpair, newang, &
                             newfour, newext, newmfa, newvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false., job%clist)

        endif

    else

        call atom_tersoff_energy(ib, im, i, job%shortrangecut, newmany, newvir)

    endif

    !TU: Calculate the change in long-range VdW correction (not accounted for in atom_energy)
    deltavdwlrc = vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                  - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts)


    newtotal = newreal + newvdw + newthree + newpair + newang &
             + newmany + newfour + newext + newmfa

    !change the atom back before I forget
    call mutate_atom(ib, im, i, semitype1(k))

    atyp = cfgs(ib)%mols(im)%atms(i)%atype

    !TU: Remember to fold the long-range correction into the energy difference
    deltav = (newtotal - oldtotal + deltavdwlrc) * beta

    chem_pot_atm1(ib,k) = exp(-deltav)

    !TU: select_atom will hang if there are no atoms of the specified type! Above safety
    !TU: checks ensure this won't happen.
    !select the atom to be mutated
    call select_atom(semitype2(k), ib, im, i)

    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldreal = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang = 0.0_wp
    oldmany = 0.0_wp
    oldfour = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts

    !energy of atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, &
                             oldfour, oldext, oldmfa, oldvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false.)

        else

            call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                             job%dielec, job%vdw_cut, job%shortrangecut, &
                             oldreal, oldvdw, oldthree, oldpair, oldang, &
                             oldfour, oldext, oldmfa, oldvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false., job%clist)
        
        endif

    else

        call atom_tersoff_energy(ib, im, i, job%shortrangecut, oldmany, oldvir)

    endif

    oldtotal = oldreal + oldvdw + oldthree + oldpair + oldang &
             + oldmany + oldfour + oldext + oldmfa

    !change the identity of the atom
    call mutate_atom(ib, im, i, semitype1(k))

    !TU: The mutate_atom procedure used above updates cfgs(ib)%nums_elemts to reflect mutation
    new_nums_elemts = cfgs(ib)%nums_elemts

    atyp = cfgs(ib)%mols(im)%atms(i)%atype

    newenth = 0.0_wp
    newvir = 0.0_wp
    newreal = 0.0_wp
    newvdw = 0.0_wp
    newthree = 0.0_wp
    newpair = 0.0_wp
    newang = 0.0_wp
    newmany = 0.0_wp
    newfour = 0.0_wp
    newext = 0.0_wp
    newmfa = 0.0_wp

    !calc energy of mutated atom
    if (atyp /= ATM_TYPE_SEMI) then

        if (job%uselist) then

            call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                             job%vdw_cut, job%shortrangecut, job%verletshell, &
                             newreal, newvdw, newthree, newpair, newang, &
                             newfour, newext, newmfa, newvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false.)

        else

            call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                             job%dielec, job%vdw_cut, job%shortrangecut, &
                             newreal, newvdw, newthree, newpair, newang, &
                             newfour, newext, newmfa, newvir,  &
                             trealnonb, trealbond, tcrct, tvdwn, tvdwb, &
                             tpair, tthree, tang, tfour, text, .false., .false., job%clist)

        endif

    else

        call atom_tersoff_energy(ib, im, i, job%shortrangecut, newmany, newvir)
    endif

    !TU: Calculate the change in long-range VdW correction (not accounted for in atom_energy)
    deltavdwlrc = vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts) &
                  - vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts)

    newtotal = newreal + newvdw + newthree + newpair + newang &
             + newmany + newfour + newext + newmfa

    ! change the atom back
    call mutate_atom(ib, im, i, semitype2(k))

    !TU: Remember to fold the long-range correction into the energy difference
    deltav = (newtotal - oldtotal + deltavdwlrc) * beta

    chem_pot_atm2(ib,k) = exp(-deltav)

end subroutine semiwidom_atoms



subroutine semigrand_atoms(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrcp, emolvdwn, emolvdwb, emolthree, &
                      emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use cell_module
    use control_type
    use field_module
    use species_module
    !use comms_mpi_module, only : mxnode, gsync
    use comms_mpi_module, only : gsum
    use random_module, only : duni
    use constants_module, only : ATM_TYPE_SEMI
    use vdw_module, only : vdw_lrc_energy
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_param_hist_inc2, &
                                 fed_window_reject

    
    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                     energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                     volume(nconfigs)

    integer :: im, i, k, typebefore, typeafter, numtypebefore, numtypeafter, atyp, buf(2), fail

    logical :: forwardmutation

    real(kind = wp) :: deltav,deltavb, otot, ntot, arg, &
       oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
       newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir, &
       choice, prob


     ! temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), ocrct(number_of_molecules), oext(number_of_molecules), &
                       nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), ncrct(number_of_molecules), next(number_of_molecules)

    !TU: The value of the bias if required
    real(wp) :: fedbias

    !TU: Flag which is true if the order parameter tracks the number of atoms for the species
    !TU: of the selected atom before the mutation or after the mutation
    logical :: fed_track_before, fed_track_after

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: old_nums_elemts(number_of_elements), new_nums_elemts(number_of_elements)


    fed_track_before = .false.
    fed_track_after = .false.

    fail = 0

    orealnonb = 0.0_wp
    orealbond = 0.0_wp
    ocrct = 0.0_wp
    ovdwn = 0.0_wp
    ovdwb = 0.0_wp
    opair = 0.0_wp
    othree = 0.0_wp
    oang = 0.0_wp
    ofour = 0.0_wp
    omany = 0.0_wp
    oext = 0.0_wp

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    ncrct = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    next = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts

    choice = duni()

    ! select mutation
    k = int(duni() * job%numsemigrandatms + 1)

    forwardmutation = .false.

    if (choice <= 0.5_wp) then

       forwardmutation = .true.

       attforwardmutations = attforwardmutations + 1

       typebefore = semitype1(k)
       typeafter = semitype2(k)

    else

       attbackmutations = attbackmutations + 1

       typebefore = semitype2(k)
       typeafter = semitype1(k)       

    end if

    !TU: Actually we need two branches; one for the before type and one for the after type
    fed_track_before = ( fed%par_kind == FED_PAR_NATOMS_SPEC ) .and. &
                       ( fed%par_natoms_spec_label == typebefore )
    fed_track_after  = ( fed%par_kind == FED_PAR_NATOMS_SPEC ) .and. &
                       ( fed%par_natoms_spec_label == typeafter )

    !TU: Obsolete; replace with cfgs(ib)%num_elemets; see code for 'findnumtype' for details
    call findnumtype(ib, typebefore, numtypebefore)
    call findnumtype(ib, typeafter, numtypeafter)

    if(numtypebefore == 0) then

        !TU: If there are no atoms of the type to be mutated then the move should be rejected.
        !TU: Note that this still counts as a legitimate move though for the purposes of gathering
        !TU: statistics. Hence we must update the transition matrix, among other things.

        if( fed_track_before ) then

           !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_old'
           !TU: and 'param_cur'.
           !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
           fedbias = fed_param_dbias2(numtypebefore*1.0_wp, numtypebefore*1.0_wp, .true.)

        else if( fed_track_after ) then

           fedbias = fed_param_dbias2(numtypeafter*1.0_wp, numtypeafter*1.0_wp, .true.)

        end if


        if( fed_track_before .or. fed_track_after ) then

            !TU: Update the transition matrix as a rejection
            !TU: (If nspecatoms is the order parameter then counting the move as a rejection or acceptance
            !TU: have the same effect on the transition matrix - they are equivalent).
            if( fed%method == FED_TM ) call fed_tm_inc(0.0_wp)

                           
            !TU: The move is counted as a rejected FED move
            call fed_param_hist_inc(ib,.false.)
               
        end if

        emptymutations = emptymutations + 1

        return
       
    else


        !TU: Proceed with the move...


        !select the atom to be mutated
        call select_atom(typebefore, ib, im, i)

    
        atyp = cfgs(ib)%mols(im)%atms(i)%atype
    
        oldenth = 0.0_wp
        oldvir = 0.0_wp
        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang = 0.0_wp
        oldmany = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp
        
        !energy of atom
        if (atyp /= ATM_TYPE_SEMI) then
    
            if (job%uselist) then
    
                call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                                 job%vdw_cut, job%shortrangecut, job%verletshell, &
                                 oldreal, oldvdw, oldthree, oldpair, oldang, &
                                 oldfour, oldext, oldmfa, oldvir,  &
                                 orealnonb, orealbond, ocrct, ovdwn, ovdwb, &
                                 opair, othree, oang, ofour, oext, .false., .false.)
    
            else
    
                call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                                 job%dielec, job%vdw_cut, job%shortrangecut, &
                                 oldreal, oldvdw, oldthree, oldpair, oldang, &
                                 oldfour, oldext, oldmfa, oldvir,  &
                                 orealnonb, orealbond, ocrct, ovdwn, ovdwb, &
                                 opair, othree, oang, ofour, oext, .false., .false., job%clist)
    
            endif
    
        else
    
            call atom_tersoff_energy(ib, im, i, job%shortrangecut, oldmany, oldvir)
    
        endif

        !TU: Add long-range corrections 
        oldvdw = oldvdw +  vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts)
    
        oldtotal = oldvdw + oldthree + oldpair + oldang &
                 + oldmany + oldfour + oldext + oldmfa
    
        oldenth = oldtotal + extpress * cfgs(ib)%vec%volume
    
        call mutate_atom(ib, im, i, typeafter)
    
        !TU: The mutate_atom procedure updates cfgs(ib)%nums_elemts to reflect insertion
        new_nums_elemts = cfgs(ib)%nums_elemts

        atyp = cfgs(ib)%mols(im)%atms(i)%atype
    
        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newmany = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp
        
        if (atyp /= ATM_TYPE_SEMI) then
    
            if (job%uselist) then
    
                call atom_energy(ib, im, i, job%coultype(ib), job%dielec, &
                                 job%vdw_cut, job%shortrangecut, job%verletshell, &
                                 newreal, newvdw, newthree, newpair, newang, &
                                 newfour, newext, newmfa, newvir,  &
                                 nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, &
                                 npair, nthree, nang, nfour, next, .false., .false.)
    
            else
    
                call atom_energy_nolist(ib, im, i, job%coultype(ib), &
                                 job%dielec, job%vdw_cut, job%shortrangecut, &
                                 newreal, newvdw, newthree, newpair, newang, &
                                 newfour, newext, newmfa, newvir,  &
                                 nrealnonb, nrealbond, ncrct, nvdwn, nvdwb, &
                                 npair, nthree, nang, nfour, next, .false., .false., job%clist)
    
            endif
    
        else
    
            call atom_tersoff_energy(ib, im, i, job%shortrangecut, newmany, newvir)
    
        endif

        !TU: Add long-range corrections
        newvdw = newvdw + vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts)
    
        newtotal = newvdw + newthree + newpair + newang &
                 + newmany + newfour + newext + newmfa

        newenth = newtotal + extpress * cfgs(ib)%vec%volume

        !TU: Calculate the bias, if needed. ** It is important to do this before any prospect of rejection. **
        !TU: Calling 'fed_param_dbias' updates the FED variables 'param_cur', 'id_cur', 'param_old'
        !TU: and 'id_old'. One MUST call 'fed_param_hist_inc' later to either revert these variables in
        !TU: the case of a rejection or keep them in the case of an acceptance. This is done below.
        if( fed_track_before ) then
        
            !TU: The mutation will reduce the number of before species by 1
            fedbias = fed_param_dbias2(1.0_wp*numtypebefore, 1.0_wp*(numtypebefore - 1), &
                                       .true.)

        else if( fed_track_after ) then 
    
            !TU: The mutation will increase the number of after species by 1
            fedbias = fed_param_dbias2(1.0_wp*numtypeafter, 1.0_wp*(numtypeafter + 1), &
                                       .true.)
            
        end if

        !TU: If fedbias is very high here the move takes us out of the considered order parameter
        !TU: range, and hence we reject the move. In this case we should not update the transition matrix 
        !TU: since there is no FED state corresponding to the trail configuration. Hence we go straight 
        !TU: to the code corresponding to rejection, bypassing the tm_inc call
        if( fedbias > vdwcap ) goto 3000


        deltav = newtotal - oldtotal
    
        chem_pot_atm1(ib,k) = deltav * beta
    
        !TU: Is the sign of deltamu correct? Check this 
        if( forwardmutation ) then 

            deltavb = beta * (deltav - deltamu(k))
    
        else

            deltavb = beta * (deltav + deltamu(k))
    
        end if
    
        prob = numtypebefore * exp(-deltavb) / (numtypeafter + 1)

        !TU: ... unless we are biasing over certain order parameters, in which case update 'prob' 
        !TU: accordingly
        if( fed_track_before .or. fed_track_after ) then
           
            !TU: Set the unbiased/canonical value of 'prob' if required for updating the transition matrix
            !TU: to the value calculated above, which is suitable...
            if( fed % method == FED_TM ) then 
               
                call fed_tm_inc(  min( 1.0_wp, prob  ) )
    
            end if

            !TU: Update 'prob' using the bias
            prob = prob * exp( -fedbias )

            !TU: Reject the move if it leaves the window or takes us further from the window                          
            if( fed % is_window ) then
               
                if( fed_window_reject() ) goto 3000
               
            end if
           
        end if

        !TU*: This deltav condition I feel shouldn't be here. See what AB has done elsewhere and copy him
        if( abs(deltav) <= job%toler .and. duni() < prob ) then
    
            !TU: Semigrand move accepted...
    
    
            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + (newenth - oldenth)
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            energyvdw(ib) = energyvdw(ib) + (newvdw - oldvdw)
            energypair(ib) = energypair(ib) + (newpair - oldpair)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energyang(ib) = energyang(ib) + (newang - oldang)
            energyfour(ib) = energyfour(ib) + (newfour - oldfour)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)
    
            if(forwardmutation) then 
    
                forwardmutations = forwardmutations + 1
    
            else
    
                backmutations = backmutations + 1
    
            end if

            if( fed_track_before .or. fed_track_after ) then

                call fed_param_hist_inc(ib,.true.)

            end if

    
            if(job%lmoldata) then
    
                do im = 1, number_of_molecules
                    emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                    emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                    emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                    emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                    emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                    emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                    emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                    emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))
    
                    otot = orealnonb(im) + orealbond(im) + ovdwn(im) + ovdwb(im) + opair(im) &
                           + othree(im) + oang(im) + ofour(im) + omany(im) + oext(im)
                    ntot = nrealnonb(im) + nrealbond(im) + nvdwn(im) + nvdwb(im) + npair(im) &
                           + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)
    
                    emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)
                enddo
    
            endif
 
            goto 2000   
        
        end if

    
        
3000    continue
    
    
        !TU: Code for semigrand move rejected...

    
        !  change back
        call mutate_atom(ib, im, i, typebefore)

        !TU: Update histogram and bias if needed
        if( fed_track_before .or. fed_track_after ) then

            call fed_param_hist_inc(ib,.false.)

        end if
  

          
        !TU: Checks performed after every energy calculation...

        !AB: make sure ALL processes in the workgroup aware of any local error and call error(..) -> MPI_FINALIZE(..)
2000    call gsum(fail)!_world(fail)

        if( fail > 0 ) then
            call error(fail) !AB: error: maximum number of atoms reached (about to exceed)
           !call error(207) !AB: error: no gas atoms has been reached - but never checked for that!
        end if

    end if

end subroutine semigrand_atoms




!TU*: Old version...
!TU*:
!TU*: This surely has bugs! For one thing it calls 'add_molecule_recip' in 'field.f90', which I think is buggy.
subroutine semigrand_mols_old(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, &
                      emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use cell_module
    use control_type
    use field_module
    use species_module
    use molecule_type
    use molecule_module, only : copy_molecule, mol_com
    use random_module, only : duni
    use vdw_module, only : vdw_lrc_energy

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                     energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), &
                     emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                     emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules), &
                     volume(nconfigs)

    integer :: im, i, k, numtype1, numtype2, natomdiff

    real(kind = wp) :: deltav, deltavb, otot, ntot, arg, prob, &
       oldreal, oldvdw, oldthree, oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
       newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, newtotal, newenth, newvir, newrcp, &
       newselfcoul, newselfvdw, choice, oldselfcoul, oldselfvdw, oldrcp


     ! temp vectors for molecular energies
    real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), &
                       ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                       othree(number_of_molecules),  oang(number_of_molecules), ofour(number_of_molecules), &
                       omany(number_of_molecules), oself(number_of_molecules), ocrct(number_of_molecules), &
                       oext(number_of_molecules), &
                       orcp(number_of_molecules), nrealnonb(number_of_molecules), nrealbond(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), &
                       nrcp(number_of_molecules), ncrct(number_of_molecules), next(number_of_molecules)

    type (molecule) :: tmp_mol

    logical :: atom_out, do_mol_rcp

    !TU: Number of each type of atom, for calculating the long-range VdW correction
    integer :: old_nums_elemts(number_of_elements), new_nums_elemts(number_of_elements)


    orealnonb = 0.0_wp
    orealbond = 0.0_wp
    ovdwn = 0.0_wp
    ovdwb = 0.0_wp
    opair = 0.0_wp
    othree = 0.0_wp
    oang = 0.0_wp
    ofour = 0.0_wp
    omany = 0.0_wp
    oext = 0.0_wp

    nrealnonb = 0.0_wp
    nrealbond = 0.0_wp
    nvdwn = 0.0_wp
    nvdwb = 0.0_wp
    npair = 0.0_wp
    nthree = 0.0_wp
    nang = 0.0_wp
    nfour = 0.0_wp
    nmany = 0.0_wp
    next = 0.0_wp

    old_nums_elemts = cfgs(ib)%nums_elemts
    new_nums_elemts = old_nums_elemts

    choice = duni()

    if (choice <= 0.5_wp) then

        !converts type 1 to type 2 molecule
        attforwardmutations = attforwardmutations + 1

        ! select mutaion
        k = int(duni() * job%numsemigrandmol + 1)

        numtype1 = cfgs(ib)%mtypes(semitype1(k))%num_mols
        numtype2 = cfgs(ib)%mtypes(semitype2(k))%num_mols

        if(numtype1 == 0) return

        ! make sure dont go over array limits
        !if (numtype2 > cfgs(ib)%mxmol_type(semitype2(k))) call error(208)

        !TU: select_molecule would hang if there were no atoms of the specified type - won't happen due to safety check above
        !select the molecule to be mutated
        call select_molecule(semitype1(k), ib, im)

        !energy of molecule - including reciprocal space
        ! evaluate energy
        oldenth = 0.0_wp
        oldvir = 0.0_wp
        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang = 0.0_wp
        oldfour = 0.0_wp
        oldmany = 0.0_wp
        oldselfvdw = 0.0_wp
        oldselfcoul = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp

        !energy of molecule
        if(job%uselist) then

            call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                         oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                         orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                         opair, othree, oang, ofour, omany, oext)

        else

            call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                         oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                         orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                         opair, othree, oang, ofour, omany, oext, job%clist)

        endif

        !TU: Add long-range correction
        oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts)

        oldtotal = oldvdw + oldthree + oldpair + oldang + oldfour + oldmany &
                 + oldreal + oldselfcoul + oldselfvdw + oldext + oldmfa

        do_mol_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(im)%has_charge )

        if( do_mol_rcp ) then

            ! update reciprocal sums (newrcp is ignored)
            call remove_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

        endif

        !AB: advance randmon number selection to preserve the random sequence
        arg = duni()

        !mutate the molecule after making a copy of it
        call copy_molecule(tmp_mol, cfgs(ib)%mols(im))

        call mutate_molecule(ib, im, semitype2(k), atom_out)

        !TU: The mutate_molecule procedure above updates cfgs(ib)%nums_elemts to reflect insertion
        new_nums_elemts = cfgs(ib)%nums_elemts

        !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
        if( atom_out ) goto 1000

        call set_molecule_exclist(ib, im)
        call update_glob_no(ib)

        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newfour = 0.0_wp
        newmany = 0.0_wp
        newselfvdw = 0.0_wp
        newselfcoul = 0.0_wp
        newrcp = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        if(job%uselist) then

            call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next)

        else

            call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir,  &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next, job%clist)

        endif

        if( do_mol_rcp ) then

            oldrcp  = energyrcp(ib)
            orcp(:) = emolrcp(ib,:)

            call add_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

        endif

        !add recip space energy to old total
        oldtotal = oldtotal + oldrcp

        !TU: Add long-range correction
        newvdw = newvdw + vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts)

        !energy of mutated molecule
        newtotal = newrcp + newvdw + newthree + newpair + newang + newfour + newmany &
                 + newreal + newselfcoul + newselfvdw + newext + newmfa

        deltav = newtotal - oldtotal

        chem_pot_atm1(ib,k) = deltav * beta

        deltavb =  beta * (deltav - deltamu(k))

        prob = numtype1 * exp(-deltavb) / (numtype2 + 1)

        !arg = duni()

        if( abs(deltav) <= job%toler .and. arg < prob ) then

            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + deltav
            energyrcp(ib) = newrcp
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            energyreal(ib) = energyreal(ib) + (newreal + newselfcoul) - (oldreal + oldselfcoul)
            energyvdw(ib) = energyvdw(ib) + (newvdw + newselfvdw) - (oldvdw + oldselfvdw)
            energypair(ib) = energypair(ib) + (newpair - oldpair)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energyang(ib) = energyang(ib) + (newang - oldang)
            energyfour(ib) = energyfour(ib) + (newfour - oldfour)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

            forwardmutations = forwardmutations + 1
            
            natomdiff = cfgs(ib)%mols(im)%natom - tmp_mol%natom
            cfgs(ib)%number_of_atoms = cfgs(ib)%number_of_atoms + natomdiff

            if( do_mol_rcp ) call update_rcpsums(ib)

            if(job%lmoldata) then

                do im = 1, number_of_molecules
                    emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                    emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                    emolrealself(ib,im) = emolrealself(ib,im) + (nself(im) - oself(im))
                    emolrealcrct(ib,im) = emolrealcrct(ib,im) + (ncrct(im) - ocrct(im))
                    emolrcp(ib,im) = nrcp(im)
                    emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                    emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                    emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                    emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                    emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                    emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                    emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                    emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                    otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im)   &
                           + othree(im) + oang(im) + ofour(im) + omany(im) + ocrct(im) + oext(im)
                    ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
                           + nthree(im) + nang(im) + nfour(im) + nmany(im) + ncrct(im) + next(im)

                    emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)
                enddo

            endif

            goto 2000

        endif
        !else

            !  change back
1000        call copy_molecule(cfgs(ib)%mols(im), tmp_mol)

            call set_molecule_exclist(ib, im)
            call update_glob_no(ib)

        !endif

2000    continue

    else       !reverse mutation

        !attemp to convert type 2 to type 1
        attbackmutations = attbackmutations + 1

        ! select mutaion
        k = int(duni() * job%numsemigrandatms + 1)

        numtype1 = cfgs(ib)%mtypes(semitype1(k))%num_mols
        numtype2 = cfgs(ib)%mtypes(semitype2(k))%num_mols

        if(numtype2 == 0) return

        ! make sure dont go over array limits
        !if (numtype1 > cfgs(ib)%mxmol_type(semitype1(k))) call error(208)

        !TU: select_molecule would hang if there were no atoms of the specified type - won't happen due to safety check above
        !select the molecule to be mutated
        call select_molecule(semitype2(k), ib, im)

        oldenth = 0.0_wp
        oldvir = 0.0_wp
        oldreal = 0.0_wp
        oldvdw = 0.0_wp
        oldthree = 0.0_wp
        oldpair = 0.0_wp
        oldang = 0.0_wp
        oldfour = 0.0_wp
        oldmany = 0.0_wp
        oldselfvdw = 0.0_wp
        oldselfcoul = 0.0_wp
        oldext = 0.0_wp
        oldmfa = 0.0_wp

        old_nums_elemts = cfgs(ib)%nums_elemts
        new_nums_elemts = old_nums_elemts

        !energy of molecule
        if(job%uselist) then

            call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                         oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir,  &
                         orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                         opair, othree, oang, ofour, omany, oext)

        else

            call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         oldreal, oldselfcoul, oldvdw, oldselfvdw, oldthree, &
                         oldpair, oldang, oldfour, oldmany, oldext, oldmfa, oldvir, &
                         orealnonb, orealbond, oself, ocrct, ovdwn, ovdwb, &
                         opair, othree, oang, ofour, omany, oext, job%clist)

        endif

        !TU: Add long-range correction
        oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/volume(ib),old_nums_elemts)

        oldtotal = oldvdw + oldthree + oldpair + oldang + oldfour + oldmany &
                 + oldreal + oldselfcoul + oldselfvdw + oldext + oldmfa

        
        do_mol_rcp = ( job%coultype(ib) == 1 .and. cfgs(ib)%mols(im)%has_charge )

        if( do_mol_rcp ) then

            ! update reciprocal sums (newrcp is ignored)
            call remove_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

        endif

        !AB: advance randmon number selection to preserve the random sequence
        arg = duni()

        !copy and change molecule type
        call copy_molecule(tmp_mol, cfgs(ib)%mols(im))

        call mutate_molecule(ib, im, semitype1(k), atom_out)

        !TU: The mutate_molecule procedure above updates cfgs(ib)%nums_elemts to reflect insertion
        new_nums_elemts = cfgs(ib)%nums_elemts


        !if in slit, make sure all atoms are still within the cell, otherwise reject the move!
        if( atom_out ) goto 3000

        call set_molecule_exclist(ib, im)
        call update_glob_no(ib)

        newenth = 0.0_wp
        newvir = 0.0_wp
        newreal = 0.0_wp
        newvdw = 0.0_wp
        newthree = 0.0_wp
        newpair = 0.0_wp
        newang = 0.0_wp
        newfour = 0.0_wp
        newmany = 0.0_wp
        newselfvdw = 0.0_wp
        newselfcoul = 0.0_wp
        newrcp = 0.0_wp
        newext = 0.0_wp
        newmfa = 0.0_wp

        if(job%uselist) then

            call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                         job%vdw_cut, job%shortrangecut, job%verletshell, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir, &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next)

        else

            call molecule_energy2_nolist(ib, im, job%coultype(ib), &
                         job%dielec, job%vdw_cut, job%shortrangecut, &
                         newreal, newselfcoul, newvdw, newselfvdw, newthree, &
                         newpair, newang, newfour, newmany, newext, newmfa, newvir, &
                         nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                         npair, nthree, nang, nfour, nmany, next, job%clist)

        endif

        if( do_mol_rcp ) then

            oldrcp = energyrcp(ib)
            orcp(:) = emolrcp(ib,:)

            call add_molecule_recip(ib, im, newrcp, nrcp, job%lmoldata)

        endif

        !add recip space energy to old total
        oldtotal = oldtotal + oldrcp

        !TU: Add long-range correction
        newvdw = newvdw + vdw_lrc_energy(1.0_wp/volume(ib),new_nums_elemts)

        !energy of mutated molecule
        newtotal = newrcp + newvdw + newthree + newpair + newang + newfour + newmany &
                 + newreal + newselfcoul + newselfvdw + newext + newmfa

        deltav = (newtotal - oldtotal) 

        chem_pot_atm2(ib,k) = deltav * beta

        deltavb = beta * (deltav + deltamu(k))

        prob = numtype2 * exp(-deltavb) / (numtype1 + 1)

        !arg = duni()

        if( abs(deltav) <= job%toler .and. arg < prob ) then

            energytot(ib) = energytot(ib) + deltav
            enthalpytot(ib) = enthalpytot(ib) + deltav
            energyrcp(ib) = newrcp
            virialtot(ib) = virialtot(ib) + (newvir - oldvir)
            energyreal(ib) = energyreal(ib) + (newreal + newselfcoul) - (oldreal + oldselfcoul)
            energyvdw(ib) = energyvdw(ib) + (newvdw + newselfvdw) - (oldvdw + oldselfvdw)
            energypair(ib) = energypair(ib) + (newpair - oldpair)
            energythree(ib) = energythree(ib) + (newthree - oldthree)
            energyang(ib) = energyang(ib) + (newang - oldang)
            energyfour(ib) = energyfour(ib) + (newfour - oldfour)
            energymany(ib) = energymany(ib) + (newmany - oldmany)
            energyext(ib) = energyext(ib) + (newext - oldext)
            energymfa(ib) = energymfa(ib) + (newmfa - oldmfa)

            backmutations = backmutations + 1
            natomdiff = cfgs(ib)%mols(im)%natom - tmp_mol%natom
            cfgs(ib)%number_of_atoms = cfgs(ib)%number_of_atoms + natomdiff

            if( do_mol_rcp ) call update_rcpsums(ib)

            if(job%lmoldata) then

                 do im = 1, number_of_molecules
                     emolrealnonb(ib,im) = emolrealnonb(ib,im) + (nrealnonb(im) - orealnonb(im))
                     emolrealbond(ib,im) = emolrealbond(ib,im) + (nrealbond(im) - orealbond(im))
                     emolrealself(ib,im) = emolrealself(ib,im) + nself(im)
                     emolrealcrct(ib,im) = emolrealcrct(ib,im) + (ncrct(im) - ocrct(im))
                     emolrcp(ib,im) = nrcp(im)
                     emolvdwn(ib,im) = emolvdwn(ib,im) + (nvdwn(im) - ovdwn(im))
                     emolvdwb(ib,im) = emolvdwb(ib,im) + (nvdwb(im) - ovdwb(im))
                     emolpair(ib,im) = emolpair(ib,im) + (npair(im) - opair(im))
                     emolthree(ib,im) = emolthree(ib,im) + (nthree(im) - othree(im))
                     emolang(ib,im) = emolang(ib,im) + (nang(im) - oang(im))
                     emolfour(ib,im) = emolfour(ib,im) + (nfour(im) - ofour(im))
                     emolmany(ib,im) = emolmany(ib,im) + (nmany(im) - omany(im))
                     emolext(ib,im) = emolext(ib,im) + (next(im) - oext(im))

                     otot = orealnonb(im) + orealbond(im) + orcp(im) + ovdwn(im) + ovdwb(im) + opair(im)   &
                            + othree(im) + oang(im) + ofour(im) + omany(im) + ocrct(im) + oext(im)
                     ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im)   &
                            + nthree(im) + nang(im) + nfour(im) + nmany(im) + ncrct(im) + next(im)

                     emoltot(ib,im) = emoltot(ib,im) + (ntot - otot)
                 enddo

            endif

            goto 4000

        endif
        !else

            !  change back
3000        call copy_molecule(cfgs(ib)%mols(im), tmp_mol)

            call set_molecule_exclist(ib, im)
            call update_glob_no(ib)

        !endif

4000    continue

    endif

end subroutine semigrand_mols_old


subroutine semigrand_mols(ib, job, beta, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, &
                      emolvdwn, emolvdwb, emolthree, emolpair, emolang, emolfour, emolmany, emolext)

    use kinds_f90
    use field_module
    use cell_module
    use control_type
    use species_module
    use random_module, only : duni
    use comms_mpi_module, only : gsum
    use molecule_module, only : store_mol_charges
    use lambda_module, only : set_lambda_charges_mol
    use vdw_module, only : vdw_lrc_energy
    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_param_dbias, fed_param_dbias2, fed_param_hist_inc, fed_param_hist_inc2, &
                                 fed_window_reject

    implicit none

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), &
                      energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energyfour(:), &
                      energymany(:), energyext(:), energymfa(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                      emolrealbond(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                      emolrealcrct(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                      emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                      emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                      emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                      emolmany(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: im, typ, ii, buf(4), numtype1, numtype2, fail

    real(kind = wp) :: prob

    !TU**: Need to tidy this up: 'old' energies are actually energy differences here EXCEPT for rcp energies. Better name for it??
    real(kind = wp) :: deltav,deltavb, otot, ntot,  &
                      oldreal, oldvdw, oldthree, oldpair, oldang, oldmany, oldext, oldmfa, oldtotal, oldenth, oldvir, &
                      oldfour, oldselfcoul, oldselfvdw, &
                      tmpreal, tmpvdw, tmpthree, tmppair, tmpang, tmpmany, tmpext, tmpmfa, tmpvir, arg, oldrcp, newrcp, &
                      tmpfour, tmpselfcoul, tmpselfvdw


     ! temp vectors for molecular energies
     real(kind = wp) :: orealnonb(number_of_molecules), orealbond(number_of_molecules), orcp(number_of_molecules), &
                        ovdwn(number_of_molecules), ovdwb(number_of_molecules), opair(number_of_molecules), &
                        othree(number_of_molecules), oang(number_of_molecules), ofour(number_of_molecules), &
                        omany(number_of_molecules),  oext(number_of_molecules), nrcp(number_of_molecules), &
                        oself(number_of_molecules), ocrct(number_of_molecules), &
                        trealnonb(number_of_molecules), trealbond(number_of_molecules), &
                        tvdwn(number_of_molecules), tvdwb(number_of_molecules), tpair(number_of_molecules), &
                        tthree(number_of_molecules), tang(number_of_molecules), tfour(number_of_molecules), &
                        tmany(number_of_molecules), text(number_of_molecules), &
                        tself(number_of_molecules), tcrct(number_of_molecules)

    !TU: The value of the bias if required
    real(wp) :: fedbias

    !TU: Flag which is true if the order parameter tracks the number of molecules for the species
    !TU: of the selected molecule before the semi-grand move or after the move
    logical :: fed_track_before, fed_track_after

    integer :: typebefore, typeafter, numtypebefore, numtypeafter


    logical :: accept, overlap, do_ewald_rcp

    ! direction of the semigrand move: forward is a transformation of a species 1 molecule into a species 2 molecule
    logical :: forward

    fail = 0

    fed_track_before = .false.
    fed_track_after = .false.
    numtypebefore = 0
    numtypeafter = 0

    do_ewald_rcp = .false.
    oldrcp = energyrcp(ib)
    !TU**: Possible bug?? What if do_ewald is not in effect but the rcp energy is not 0. This could cause issues below if newrcp is not initialised to energyrcp(ib)?
    newrcp = 0.0_wp

    nrcp = 0.0_wp
    orcp = 0.0_wp

    orealnonb = 0.0_wp
    orealbond = 0.0_wp
    ovdwn = 0.0_wp
    ovdwb = 0.0_wp
    opair = 0.0_wp
    othree = 0.0_wp
    oang = 0.0_wp
    ofour = 0.0_wp
    omany = 0.0_wp
    oext = 0.0_wp
    oself = 0.0_wp
    ocrct = 0.0_wp

    trealnonb = 0.0_wp
    trealbond = 0.0_wp
    tvdwn = 0.0_wp
    tvdwb = 0.0_wp
    tpair = 0.0_wp
    tthree = 0.0_wp
    tang = 0.0_wp
    tfour = 0.0_wp
    tmany = 0.0_wp
    text = 0.0_wp
    tself = 0.0_wp
    tcrct = 0.0_wp

    do_ewald_rcp = ( job%coultype(ib) == 1 )


    ! must zero as accumulators
    oldreal = 0.0_wp
    oldvdw = 0.0_wp
    oldthree = 0.0_wp
    oldpair = 0.0_wp
    oldang =  0.0_wp
    oldmany = 0.0_wp
    oldext = 0.0_wp
    oldmfa = 0.0_wp
    oldenth = 0.0_wp
    oldvir = 0.0_wp
    oldfour = 0.0_wp
    oldselfcoul = 0.0_wp
    oldselfvdw = 0.0_wp
       
    tmpreal = 0.0_wp
    tmpvdw = 0.0_wp
    tmpthree = 0.0_wp
    tmppair = 0.0_wp
    tmpang =  0.0_wp
    tmpmany = 0.0_wp
    tmpext = 0.0_wp
    tmpmfa = 0.0_wp
    tmpvir = 0.0_wp
    tmpfour = 0.0_wp
    tmpselfcoul = 0.0_wp
    tmpselfvdw = 0.0_wp


    !TU: Select which semigrand 'reaction' to consider

    typ = int (duni() * job%numsemigrandmol + 1) ! randomly select which semigrand reaction

    numtype1 = cfgs(ib)%mtypes(semitype1(typ))%num_mols
    numtype2 = cfgs(ib)%mtypes(semitype2(typ))%num_mols

    !TU: Select the direction of the reaction 
    forward = .true.
    if( duni() < 0.5_wp ) forward = .false.

    !TU: Check that there is at least one molecule of species 1(2) for a forward(backward) move
    if(forward) then

        attforwardmutations = attforwardmutations + 1

        typebefore = semitype1(typ)
        typeafter = semitype2(typ)
  
        numtypebefore = cfgs(ib)%mtypes(semitype1(typ))%num_mols
        numtypeafter = cfgs(ib)%mtypes(semitype2(typ))%num_mols

    else

        attbackmutations = attbackmutations + 1

        typebefore = semitype2(typ)
        typeafter = semitype1(typ)

        numtypebefore = cfgs(ib)%mtypes(semitype2(typ))%num_mols
        numtypeafter = cfgs(ib)%mtypes(semitype1(typ))%num_mols

    end if

    !TU: Actually we need two branches; one for the before type and one for the after type
    fed_track_before = ( fed%par_kind == FED_PAR_NMOLS_SPEC ) .and. &
                       ( fed%par_nmols_spec_label == typebefore )
    fed_track_after  = ( fed%par_kind == FED_PAR_NMOLS_SPEC ) .and. &
                       ( fed%par_nmols_spec_label == typeafter )

    if( numtypebefore == 0 ) then

        !TU: If there are no molecules of the type to be mutated then the move should be rejected.
        !TU: Note that this still counts as a legitimate move though for the purposes of gathering
        !TU: statistics. Hence we must update the transition matrix, among other things.

        if( fed_track_before ) then

           !TU: This procedure serves to update the internal FED variables 'id_old','id_cur','param_old'
           !TU: and 'param_cur'.
           !TU: This is needed to define the old and trial order parameters for 'fed_tm_inc' called below.
           fedbias = fed_param_dbias2(numtypebefore*1.0_wp, numtypebefore*1.0_wp, .true.)

        else if( fed_track_after ) then

           fedbias = fed_param_dbias2(numtypeafter*1.0_wp, numtypeafter*1.0_wp, .true.)

        end if

        if( fed_track_before .or. fed_track_after ) then

            !TU: Update the transition matrix as a rejection
            !TU: (If nspecmols is the order parameter then counting the move as a rejection or acceptance
            !TU: have the same effect on the transition matrix - they are equivalent).
            if( fed%method == FED_TM ) call fed_tm_inc(0.0_wp)
                      
            !TU: The move is counted as a rejected FED move
            call fed_param_hist_inc(ib,.false.)
               
        end if

        emptymutations = emptymutations + 1

        return

    end if


    !TU: Select the molecule. Note that select_molecule will hang if there are no molecules
    !TU: belonging to the target species. The check above ensures this won't happen
    if(forward) then

        call select_molecule(semitype1(typ), ib, im)

    else

        call select_molecule(semitype2(typ), ib, im)
            
    end if

    !TU**: Required for reciprocal energy calculation. Put in a conditional??
    call store_mol_charges(cfgs(ib)%mols(im))


    ! Calculate change in energy of changing the species of the molecule to semitype2(typ) (for 
    ! forward reaction) or semitype1(typ) for backwards reaction).

    ! Get initial energy of molecule im

    if(job%uselist) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                 job%vdw_cut, job%shortrangecut, job%verletshell, &
                 tmpreal, tmpselfcoul, tmpvdw, tmpselfvdw, tmpthree, &
                 tmppair, tmpang, tmpfour, tmpmany, tmpext, tmpmfa, tmpvir,  &
                 trealnonb, trealbond, tself, tcrct, tvdwn, tvdwb, &
                 tpair, tthree, tang, tfour, tmany, text)

    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), job%dielec, &
                 job%vdw_cut, job%shortrangecut, &
                 tmpreal, tmpselfcoul, tmpvdw, tmpselfvdw, tmpthree, &
                 tmppair, tmpang, tmpfour, tmpmany, tmpext, tmpmfa, tmpvir,  &
                 trealnonb, trealbond, tself, tcrct, tvdwn, tvdwb, &
                 tpair, tthree, tang, tfour, tmany, text, job%clist)

    endif

    ! SUBTRACT energy of molecule im from running total (we will add energy of im with new species later - yielding the
    ! change in energy associated with the species change of im)

    oldreal = oldreal - tmpreal
    oldvdw = oldvdw - tmpvdw
    oldthree = oldthree - tmpthree
    oldpair = oldpair - tmppair
    oldang = oldang - tmpang
    oldmany = oldmany - tmpmany
    oldext = oldext - tmpext
    oldmfa = oldmfa - tmpmfa
    oldvir = oldvir - tmpvir
    oldselfcoul = oldselfcoul - tmpselfcoul
    oldselfvdw = oldselfvdw - tmpselfvdw
    oldfour = oldfour - tmpfour

    !TU: Subtract also the TOTAL long-range VdW contribution, which depends on the number of atoms belonging
    !TU: each element and the system volume. We add the total contribution after the species change later,
    !TU: leaving only the DIFFERENCE in the long-range VdW contribution due to the species change
    oldvdw = oldvdw - vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)

    do ii = 1, number_of_molecules

        orealnonb(ii) = orealnonb(ii) - trealnonb(ii)
        orealbond(ii) = orealbond(ii) - trealbond(ii)
        ovdwn(ii) = ovdwn(ii) - tvdwn(ii)
        ovdwb(ii) = ovdwb(ii) - tvdwb(ii)
        opair(ii) = opair(ii) - tpair(ii)
        othree(ii) = othree(ii) - tthree(ii)
        oang(ii) = oang(ii) - tang(ii)
        ofour(ii) = ofour(ii) - tfour(ii)
        omany(ii) = omany(ii) - tmany(ii)
        oext(ii) = oext(ii) - text(ii)
        oself(ii) = oself(ii) - tself(ii)
        ocrct(ii) = ocrct(ii) - tcrct(ii)

    enddo


    !TU: Make the change: change the species of molecule im to semitype2(typ) or semitype1(typ)
    if(forward) then

        call transform_molecule(ib, im, semitype2(typ) )

    else

        call transform_molecule(ib, im, semitype1(typ) )

    end if

    ! Set any lambda-dependent charges in the molecule to reflect the current lambda if applicable
    call set_lambda_charges_mol(cfgs(ib)%mols(im))

    ! Get final energy of molecule im

    if(job%uselist) then

        call molecule_energy2(ib, im, job%coultype(ib), job%dielec, &
                 job%vdw_cut, job%shortrangecut, job%verletshell, &
                 tmpreal, tmpselfcoul, tmpvdw, tmpselfvdw, tmpthree, &
                 tmppair, tmpang, tmpfour, tmpmany, tmpext, tmpmfa, tmpvir,  &
                 trealnonb, trealbond, tself, tcrct, tvdwn, tvdwb, &
                 tpair, tthree, tang, tfour, tmany, text)

    else

        call molecule_energy2_nolist(ib, im, job%coultype(ib), job%dielec, &
                 job%vdw_cut, job%shortrangecut, &
                 tmpreal, tmpselfcoul, tmpvdw, tmpselfvdw, tmpthree, &
                 tmppair, tmpang, tmpfour, tmpmany, tmpext, tmpmfa, tmpvir,  &
                 trealnonb, trealbond, tself, tcrct, tvdwn, tvdwb, &
                 tpair, tthree, tang, tfour, tmany, text, job%clist)

    endif

    ! ADD energy of molecule im to running total. After this oldreal, oldvdw, etc. is the change in energy associated 
    ! with the species change of im

    oldreal = oldreal + tmpreal
    oldvdw = oldvdw + tmpvdw
    oldthree = oldthree + tmpthree
    oldpair = oldpair + tmppair
    oldang = oldang + tmpang
    oldmany = oldmany + tmpmany
    oldext = oldext + tmpext
    oldmfa = oldmfa + tmpmfa
    oldvir = oldvir + tmpvir
    oldselfcoul = oldselfcoul + tmpselfcoul
    oldselfvdw = oldselfvdw + tmpselfvdw
    oldfour = oldfour + tmpfour

    !TU: Similarly to above, add the long-range contribution to the VdW energy. 
    oldvdw = oldvdw + vdw_lrc_energy(1.0_wp/cfgs(ib)%vec%volume,cfgs(ib)%nums_elemts)


    do ii = 1, number_of_molecules

        orealnonb(ii) = orealnonb(ii) + trealnonb(ii)
        orealbond(ii) = orealbond(ii) + trealbond(ii)
        ovdwn(ii) = ovdwn(ii) + tvdwn(ii)
        ovdwb(ii) = ovdwb(ii) + tvdwb(ii)
        opair(ii) = opair(ii) + tpair(ii)
        othree(ii) = othree(ii) + tthree(ii)
        oang(ii) = oang(ii) + tang(ii)
        ofour(ii) = ofour(ii) + tfour(ii)
        omany(ii) = omany(ii) + tmany(ii)
        oext(ii) = oext(ii) + text(ii)
        oself(ii) = oself(ii) + tself(ii)
        ocrct(ii) = ocrct(ii) + tcrct(ii)

    enddo


    ! Get the reciprocal energy after the species change due to the fact that the charges in molecule
    ! im have changed (but not the atomic positions)
    !TU: move_molecule_charges_recip returns the new reciprocal energy given the changes in molecule charges, and sets
    !TU: the tmprcpsum variables to reflect the current charges (i.e. the charges in the system corresponding te new species)
    if( do_ewald_rcp ) call move_molecule_charges_recip(ib, im, newrcp, nrcp)

    !TU: Recall that, except for the rcp energy, the 'old' energies are actually changes in the energy due to the move
    oldtotal = oldreal + oldvdw + oldthree + oldpair + oldang + oldmany + oldext + oldmfa &
               + oldselfcoul + oldselfvdw + oldfour &
               + (newrcp - oldrcp)
    oldenth  = oldtotal + extpress * cfgs(ib)%vec%volume


    !TU: Calculate the bias, if needed. ** It is important to do this before any prospect of rejection. **
    !TU: Calling 'fed_param_dbias' updates the FED variables 'param_cur', 'id_cur', 'param_old'
    !TU: and 'id_old'. One MUST call 'fed_param_hist_inc' later to either revert these variables in
    !TU: the case of a rejection or keep them in the case of an acceptance. This is done below.
    if( fed_track_before ) then
        
        !TU: The mutation will reduce the number of before species by 1
        fedbias = fed_param_dbias2(1.0_wp*numtypebefore, 1.0_wp*(numtypebefore - 1), &
                                       .true.)

    else if( fed_track_after ) then 
    
        !TU: The mutation will increase the number of after species by 1
        fedbias = fed_param_dbias2(1.0_wp*numtypeafter, 1.0_wp*(numtypeafter + 1), &
                                   .true.)
            
    end if

    !TU: If fedbias is very high here the move takes us out of the considered order parameter
    !TU: range, and hence we reject the move. In this case we should not update the transition matrix 
    !TU: since there is no FED state corresponding to the trail configuration. Hence we go straight 
    !TU: to the code corresponding to rejection, bypassing the tm_inc call
    if( fedbias > vdwcap ) goto 3000


    deltav = oldtotal

    deltavb = deltav * beta

    if(forward) then

        deltavb = deltavb - beta * deltamu(typ)
        prob = numtype1 * exp(-deltavb) / (numtype2 + 1)

    else

        deltavb = deltavb + beta * deltamu(typ)
        prob = numtype2 * exp(-deltavb) / (numtype1 + 1)              

    end if

    !TU: Now 'prob' is the probability of acceptance
    !TU: ... unless we are biasing over certain order parameters, in which case update 'prob' 
    !TU: accordingly
    if( fed_track_before .or. fed_track_after ) then
           
        !TU: Set the unbiased/canonical value of 'prob' if required for updating the transition matrix
        !TU: to the value calculated above, which is suitable...
        if( fed % method == FED_TM ) then 
               
            call fed_tm_inc(  min( 1.0_wp, prob  ) )
    
        end if

        !TU: Update 'prob' using the bias
        prob = prob * exp( -fedbias )

        !TU: Reject the move if it leaves the window or takes us further from the window                          
        if( fed % is_window ) then
               
            if( fed_window_reject() ) goto 3000
               
        end if
           
    end if

    arg = duni()

    !TU*: This deltav condition I feel shouldn't be here. See what AB has done elsewhere and copy him
    if( abs(deltav) <= job%toler .and. arg < prob ) then

        !TU: Semigrand move accepted...

        if(forward) then

            forwardmutations = forwardmutations + 1

        else

            backmutations = backmutations + 1

        end if

        if( fed_track_before .or. fed_track_after ) then

            call fed_param_hist_inc(ib,.true.)

        end if

        energytot(ib) = energytot(ib) + deltav
        enthalpytot(ib) = enthalpytot(ib) + oldenth
        virialtot(ib) = virialtot(ib) + oldvir
        energyreal(ib) = energyreal(ib) + oldreal + oldselfcoul
        energyvdw(ib) = energyvdw(ib) + oldvdw + oldselfvdw
        energypair(ib) = energypair(ib) + oldpair
        energythree(ib) = energythree(ib) + oldthree
        energyang(ib) = energyang(ib) + oldang
        energymany(ib) = energymany(ib) + oldmany
        energyext(ib) = energyext(ib) + oldext
        energymfa(ib) = energymfa(ib) + oldmfa
        energyfour(ib) = energyfour(ib) + oldfour

        successfulswaps = successfulswaps + 1



        if( do_ewald_rcp ) then
            energyrcp(ib) = newrcp
            call update_rcpsums(ib)
        endif

        do ii = 1, number_of_molecules
            emolrealnonb(ib,ii) = emolrealnonb(ib,ii) + orealnonb(ii)
            emolrealbond(ib,ii) = emolrealbond(ib,ii) + orealbond(ii)
            emolvdwn(ib,ii) = emolvdwn(ib,ii) + ovdwn(ii)
            emolvdwb(ib,ii) = emolvdwb(ib,ii) + ovdwb(ii)
            emolpair(ib,ii) = emolpair(ib,ii) + opair(ii)
            emolthree(ib,ii) = emolthree(ib,ii) + othree(ii)
            emolang(ib,ii) = emolang(ib,ii) + oang(ii)
            emolfour(ib,ii) = emolfour(ib,ii) + ofour(ii)
            emolmany(ib,ii) = emolmany(ib,ii) + omany(ii)
            emolext(ib,ii) = emolext(ib,ii) + oext(ii)
            emolrealself(ib,ii) = emolrealself(ib,ii) + oself(ii)
            emolrealcrct(ib,ii) = emolrealcrct(ib,ii) + ocrct(ii)

            !TU**: Need to double-check this is all correct for the molecular contributions, especially w.r.t. the rcp energy
            orcp(ii) = emolrcp(ib,ii)
            emolrcp(ib,ii) = nrcp(ii)

            otot = orealnonb(ii) + orealbond(ii) + ovdwn(ii) + ovdwb(ii) + opair(ii)   &
                   + othree(ii) + oang(ii) + ofour(ii) + omany(ii) + oext(ii) &
                   + oself(ii) + ocrct(ii) &
            !TU**: THIS LINE CAUSES A SILENT CRASH WITH gfortrandbg! NEEDS FIXING
                   + (nrcp(ii) - orcp(ii)) !TU**: Is this right??
            emoltot(ib,ii) = emoltot(ib,ii) +  otot

        enddo

        goto 2000

    end if

3000 continue

        !TU: Code for semigrand move rejected...

        !TU: Restore the original species of the molecules
        if(forward) then 

            call transform_molecule(ib, im, semitype1(typ) )

        else

            call transform_molecule(ib, im, semitype2(typ) )

        end if

        !TU: Update histogram and bias if needed
        if( fed_track_before .or. fed_track_after ) then

            call fed_param_hist_inc(ib,.false.)

        end if
 
        ! Set any lambda-dependent charges in the molecule to reflect the current lambda if applicable
        call set_lambda_charges_mol(cfgs(ib)%mols(im))


        !TU: Checks performed after every energy calculation...

        !AB: make sure ALL processes in the workgroup aware of any local error and call error(..) -> MPI_FINALIZE(..)
2000    call gsum(fail)!_world(fail)

        if( fail > 0 ) then
            call error(fail)
        end if


end subroutine semigrand_mols




end module
