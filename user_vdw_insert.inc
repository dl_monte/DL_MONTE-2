! *******************************************************************************
! *   DL_MONTE-2 software (STFC/DL)                                             *
! *                                                                             *
! *   A.V.Brukhno (C) 2016                                                      *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                          *
! *                                                                             *
! *   - USER defined analytical forms for force-fields (vdw_*.f90 & user_*.inc) *
! *                                                                             *
! *******************************************************************************

    ! USER convention for parameterisation:

    ! a = prmvdw(1,ivdw) - ref distance (sigma, r_0)
    ! b = prmvdw(2,ivdw) - custom  (ref energy, e_0)
    ! c = prmvdw(3,ivdw) - custom
    ! d = prmvdw(4,ivdw) - custom
    ! e = prmvdw(5,ivdw) - custom

! << - user-defined : key > 100 ; keywords: 'usr1'/'usr2'/'usr3'/...

              if( keyvdw == 100 ) then       ! User-defined potential: 12-6 (test example)

                ! to optimize, do manipulations on distance: ru & rui

                !rui  = 1.0_wp/ru  ! inverse
                rui  = rui*rui*rui ! inv cubic power

                evdw = vu0(rui*rui,a,b) ! = (a*rui-b)*rui = (a/r**6-b)/r**6

              else if( keyvdw == 101 ) then  ! User-defined potential: 10-4 (example 1)

                ! to optimize, do manipulations on distance: ru & rui

                !rui  = 1.0_wp/ru  ! inverse
                ru   = rui*rui     ! inv square
                rui  = ru*ru       ! inv 4-th power
                ru   = ru*rui      ! inv 6-th power

                evdw = vu1(ru,rui,a,b)       ! = (a*ru-b)*rui = (a/r**6-b)/r**4

              else if( keyvdw == 102 ) then  ! User-defined potential:  9-3 (example 2)

                ! to optimize, do manipulations on distance: ru & rui

                !rui  = 1.0_wp/ru  ! inverse
                ru   = rui*rui     ! inv square
                rui  = rui*ru      ! inv 3-rd power
                ru   = rui*rui     ! inv 6-th power

                evdw = vu2(ru,rui,a,b)       ! = (a*ru-b)*rui = (a/r**6-b)/r**3

              else if( keyvdw == 103 ) then  ! User-defined potential: Morse + LJ (example 3)

                ! to optimize, do manipulations on distance: ru & rui

                rui  = rui*rui*rui ! inv 3-rd power
                rui  = rui*rui     ! inv 6-th power

                evdw = vu3(ru,rui,a,b,c,d,e) ! Morse + LJ

              else if( keyvdw == 104 ) then  ! User-defined potentials - undefined

                ! to optimize, do manipulations on distance: ru & rui

                evdw = vu4(ru,rui,a,b,c,d,e) ! Undefined

              else if( keyvdw == 105 ) then  ! User-defined potentials - undefined

                ! to optimize, do manipulations on distance: ru & rui

                evdw = vu5(ru,rui,a,b,c,d,e) ! Undefined

              !else if( keyvdw == 106 ) then  ! User-defined potentials - undefined

                ! to optimize, do manipulations on distance: ru & rui

                !evdw = vu6(ru,rui,a,b,c,d,e) ! Undefined

              end if

! user-defined - >>

