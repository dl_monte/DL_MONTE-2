! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   J.Grant - orthogonal set/print flags                                 *
! *   r.j.granti[@]bath.ac.uk                                               *
! ***************************************************************************

!> @brief
!> - Module for manipulating simulation cell matrix (aka lattice vectors)
!> @usage 
!> - @stdusage
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor simulation cell 3D matrix (lattice vectors): reading, printing & manipulating

module latticevectors_module

    use kinds_f90
    implicit none


contains


!> printing cell vectors
subroutine printlatticevectors(lv)

    use latticevectors_type
    use constants_module, only : uout
    use comms_mpi_module, only : master

    implicit none

        !> lattice/cell vectors container
    type(latticevectors), intent(in) :: lv

    if (master) then

        write(uout,"(/,10x,'lattice vectors')")

        write(uout,'(3f12.5)')lv%latvector(1,1), lv%latvector(1,2), lv%latvector(1,3)

        write(uout,'(3f12.5)')lv%latvector(2,1), lv%latvector(2,2), lv%latvector(2,3)

        write(uout,'(3f12.5)')lv%latvector(3,1), lv%latvector(3,2), lv%latvector(3,3)

        write(uout,"(/,10x,'Using: is_simple_orthogonal:',4x,l1,/)")lv%is_simple_orthogonal
        write(uout,"(/,10x,'Using: is_orthogonal:',4x,l1,/)")lv%is_orthogonal
    endif

end subroutine printlatticevectors


!> calculate reciprocal lattice vectors
subroutine setrcpvec(lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> lattice/cell vectors container
    type(latticevectors), intent(inout) :: lv

    real(kind = wp) :: vol

      lv%rcpvector(1,1)=lv%latvector(2,2)*lv%latvector(3,3)-lv%latvector(3,2)*lv%latvector(2,3)
      lv%rcpvector(2,1)=lv%latvector(3,2)*lv%latvector(1,3)-lv%latvector(1,2)*lv%latvector(3,3)
      lv%rcpvector(3,1)=lv%latvector(1,2)*lv%latvector(2,3)-lv%latvector(2,2)*lv%latvector(1,3)

      vol=lv%latvector(1,1)*lv%rcpvector(1,1)+lv%latvector(2,1)*lv%rcpvector(2,1)+lv%latvector(3,1)*lv%rcpvector(3,1)

      lv%rcpvector(1,1)=lv%rcpvector(1,1)/vol
      lv%rcpvector(2,1)=lv%rcpvector(2,1)/vol
      lv%rcpvector(3,1)=lv%rcpvector(3,1)/vol

      lv%rcpvector(1,2)=(lv%latvector(2,3)*lv%latvector(3,1)-lv%latvector(3,3)*lv%latvector(2,1))/vol
      lv%rcpvector(2,2)=(lv%latvector(3,3)*lv%latvector(1,1)-lv%latvector(1,3)*lv%latvector(3,1))/vol
      lv%rcpvector(3,2)=(lv%latvector(1,3)*lv%latvector(2,1)-lv%latvector(2,3)*lv%latvector(1,1))/vol

      lv%rcpvector(1,3)=(lv%latvector(2,1)*lv%latvector(3,2)-lv%latvector(3,1)*lv%latvector(2,2))/vol
      lv%rcpvector(2,3)=(lv%latvector(3,1)*lv%latvector(1,2)-lv%latvector(1,1)*lv%latvector(3,2))/vol
      lv%rcpvector(3,3)=(lv%latvector(1,1)*lv%latvector(2,2)-lv%latvector(2,1)*lv%latvector(1,2))/vol

      vol=ABS(vol)

end subroutine setrcpvec


!> print reciprocal cell vectors
subroutine printrcpvec(lv)

    use kinds_f90
    use latticevectors_type
    use constants_module, only : uout
    use comms_mpi_module, only : master

    implicit none

        !> lattice/cell vectors container
    type(latticevectors), intent(in) :: lv

    if (master) then

        write(uout,"(/,10x,'reciprocal lattice vectors')")

        write(6,'(3f12.5)')lv%rcpvector(1,1), lv%rcpvector(1,2), lv%rcpvector(1,3)

        write(6,'(3f12.5)')lv%rcpvector(2,1), lv%rcpvector(2,2), lv%rcpvector(2,3)

        write(6,'(3f12.5,/)')lv%rcpvector(3,1), lv%rcpvector(3,2), lv%rcpvector(3,3)

    endif

end subroutine printrcpvec


!> calculate the volume from lattice vectors
subroutine cellsize(lv, vol)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> lattice/cell vectors container
    type(latticevectors), intent(inout) :: lv

        !> calculated cell volume
    real(kind = wp), intent(out) :: vol

    real(kind = wp) :: bc1, bc2, bc3

    bc1 = lv%latvector(2,2) * lv%latvector(3,3) - lv%latvector(2,3) * lv%latvector(3,2)
    bc2 = lv%latvector(2,3) * lv%latvector(3,1) - lv%latvector(2,1) * lv%latvector(3,3)
    bc3 = lv%latvector(2,1) * lv%latvector(3,2) - lv%latvector(2,2) * lv%latvector(3,1)

    vol = abs( lv%latvector(1,1) * bc1 + lv%latvector(1,2) * bc2 + lv%latvector(1,3) * bc3 )

    lv%volume = vol

end subroutine cellsize


!TU-: This procedure is obsolete and could be deleted during a refactor: expandcell_ortho
!TU-: could be used to do the same as expandcell

!> Volume step for isotropic expansion/contraction (NPT for CUBIC cells; not possible with slit XY PBC)
subroutine expandcell(lv, scaling, volnew, maxvolchange, movetype)

    use kinds_f90
    use latticevectors_type
    use constants_module, only : THIRD, NPT_LOG, NPT_INV, NPT_LIN
    use random_module, only : duni

    implicit none

        !> cell matrix (set of cell vectors) to work on
    type(latticevectors), intent(inout) :: lv

        !> resulting scaling factor
    real (kind = wp), intent(inout) :: scaling

        !> resulting attempted volume
    real (kind = wp), intent(inout) :: volnew

        !> current max V-step (displacement)
    real (kind = wp), intent(in) :: maxvolchange

        !> type of V-step to attempt: ln(V), 1/L, L (see `constants_module.f90`)
    integer, intent(in) :: movetype

        ! local auxilary variables
    real (kind = wp) :: volstep, oldvol, olddim, newdim

    integer :: i, j

    scaling = 1.0_wp
    volstep = (2.0_wp * duni() - 1.0_wp) * maxvolchange

    oldvol = lv%volume
    volnew = oldvol

    olddim = lv%latvector(1,1)
    newdim = olddim

!AB: two volume variation types: sampling 1/L or L, where V=L^3 (for cubic cells only!)

    select case (movetype)

           case (NPT_INV)

               newdim = 1.0_wp / ( 1.0_wp/olddim + volstep )

           case (NPT_LIN)

               newdim = olddim + volstep

           case default

               continue

    end select

    scaling = newdim / olddim

    lv%latvector(1,1) = newdim
    lv%latvector(2,2) = newdim
    lv%latvector(3,3) = newdim

    volnew = newdim**3

    lv%volume = volnew

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine expandcell


!> Shape-preserving cell expansion/contraction V-step (NPT for ORTHORHOMBIC cells; possible with slit XY PBC).
subroutine expandcell_ortho(lv, scaling, volnew, maxvolchange, movetype, is_semi)

    use kinds_f90
    use latticevectors_type
    use constants_module, only : THIRD, HALF, NPT_LOG, NPT_INV2, NPT_LIN2, NPT_ORTHOANI
    use random_module, only : duni

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> resulting scaling factor (for each dimension of the cell)
    real (kind = wp), intent(inout) :: scaling

        !> resulting attempted volume
    real (kind = wp), intent(inout) :: volnew

        !> current max V-step (displacement)
    real (kind = wp), intent(in) :: maxvolchange

        !> type of V-step to attempt: e.g., NPT_LOG_ORTHO (see `constants_module.f90`)
    integer, intent(in) :: movetype

        !> flag for slit case
    logical, intent(in) :: is_semi

        ! local auxilary variables
    real(kind = wp) :: volstep, olddim, newdim

    integer :: i, j

    scaling = 1.0_wp
    volstep = (2.0_wp * duni() - 1.0_wp) * maxvolchange

    olddim = lv%latvector(1,1)
    newdim = olddim

    select case (movetype)

           case(NPT_LOG)

               if( is_semi ) then
                   !volnew  = lv%latvector(3,3) * exp( log(lv%volume/lv%latvector(3,3))+volstep ) 
                   !      ! = lv%latvector(3,3) * lv%volume * exp(volstep) / lv%latvector(3,3)
                   !volnew  = exp( log(lv%volume)+volstep ) ! = lv%volume * exp(volstep)
                   !scaling = sqrt(volnew/lv%volume)        ! = exp(volstep*HALF)

                   scaling = exp( volstep * HALF )

                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
               else
                   scaling = exp( volstep * THIRD )

                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
                   lv%latvector(3,3) = lv%latvector(3,3) * scaling
               end if

           case (NPT_INV2)

               newdim = 1.0_wp / ( 1.0_wp/olddim + volstep )

               scaling = newdim / olddim

               if( is_semi ) then
                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
               else
                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
                   lv%latvector(3,3) = lv%latvector(3,3) * scaling
               end if

           case (NPT_LIN2)

               newdim = olddim + volstep

               scaling = newdim / olddim

               if( is_semi ) then
                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
               else
                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
                   lv%latvector(3,3) = lv%latvector(3,3) * scaling
               end if

           ! Add more cases here later if desired
           !case(????)
           !    scaling = ????

           case default

               if( is_semi ) then
                   scaling = (( lv%volume + volstep ) / lv%volume)**HALF

                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
               else
                   scaling = (( lv%volume + volstep ) / lv%volume)**THIRD

                   lv%latvector(1,1) = lv%latvector(1,1) * scaling
                   lv%latvector(2,2) = lv%latvector(2,2) * scaling
                   lv%latvector(3,3) = lv%latvector(3,3) * scaling
               end if

               continue

    end select

    volnew = lv%latvector(1,1) * lv%latvector(2,2) * lv%latvector(3,3)

    lv%volume = volnew

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine expandcell_ortho




!> Anisotropic cell expansion/contraction V-step - NPT for ORTHORHOMBIC cells
subroutine expandcell_orthoani(lv, indx, scaling, volnew, maxvolchange)

    use kinds_f90
    use latticevectors_type
    use constants_module, only : THIRD, HALF, NPT_LOG, NPT_INV2, NPT_LIN2, NPT_ORTHOANI
    use random_module, only : duni

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> dimension of the box to expand/contract (1=x,2=y,3=z)
    integer, intent(in) :: indx
    
        !> resulting scaling factor (to the specified dimension of the cell)
    real (kind = wp), intent(inout) :: scaling

        !> resulting attempted volume
    real (kind = wp), intent(inout) :: volnew

        !> current max V-step (displacement)
    real (kind = wp), intent(in) :: maxvolchange

        ! local auxilary variables
    real(kind = wp) :: volstep

    integer :: i, j

    scaling = 1.0_wp
    volstep = (2.0_wp * duni() - 1.0_wp) * maxvolchange
    
    scaling = exp( volstep )

    lv%latvector(indx,indx) = lv%latvector(indx,indx) * scaling        

    volnew = lv%latvector(1,1) * lv%latvector(2,2) * lv%latvector(3,3)

    lv%volume = volnew

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine expandcell_orthoani




!TU: Added by me...
!> Shape-preserving cell expansion/contraction V-step (NPT for ORTHORHOMBIC cells; possible with slit XY PBC),
!> where the scaling applied to each cell dimension is SPECIFIED (i.e. not random)
subroutine expandcell_ortho_by(lv, scaling, volnew, is_semi)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> scaling factor to be applied to each dimension of the cell
    real (kind = wp), intent(in) :: scaling

        !> resulting attempted volume
    real (kind = wp), intent(inout) :: volnew

        !> flag for slit case
    logical, intent(in) :: is_semi

    integer :: i, j
    
    if( is_semi ) then
       
       lv%latvector(1,1) = lv%latvector(1,1) * scaling
       lv%latvector(2,2) = lv%latvector(2,2) * scaling

    else
       
       lv%latvector(1,1) = lv%latvector(1,1) * scaling
       lv%latvector(2,2) = lv%latvector(2,2) * scaling
       lv%latvector(3,3) = lv%latvector(3,3) * scaling

    end if               

    volnew = lv%latvector(1,1) * lv%latvector(2,2) * lv%latvector(3,3)

    lv%volume = volnew

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine expandcell_ortho_by




!> V-step for most general cell distortion (NPT ensemble with non-orthogonal cell vectors)
subroutine distortcell(indx, maxvolchange, vol, lv, bulks)

    use kinds_f90
    use latticevectors_type
    use random_module, only : duni

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    integer, intent(in) ::  indx
    real (kind = wp), intent(in) :: maxvolchange
    real (kind = wp), intent(out) :: vol, bulks(9)

    integer :: i, j
    real (kind = wp) :: latn(3,3)

    latn = lv%latvector

    bulks = 0.0_wp

    bulks(indx) = (2.0 * duni() - 1.0) * maxvolchange

    do j = 1, 3

        lv%latvector(j,1) = (1.0_wp + bulks(1))*latn(j,1) + 0.5_wp*bulks(6)*latn(j,2) + 0.5_wp*bulks(5)*latn(j,3)
        lv%latvector(j,2) = (1.0_wp + bulks(2))*latn(j,2) + 0.5_wp*bulks(6)*latn(j,1) + 0.5_wp*bulks(4)*latn(j,3)
        lv%latvector(j,3) = (1.0_wp + bulks(3))*latn(j,3) + 0.5_wp*bulks(5)*latn(j,1) + 0.5_wp*bulks(4)*latn(j,2)

    enddo

    call cellsize(lv, vol)

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine distortcell


!> returns the minimum dimensions of lattice vectors
subroutine mindimension(lv, cellmin)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(in) :: lv
    real (kind = wp), intent(out) :: cellmin

    real (kind = wp) :: cellx, celly, cellz

    cellx = lv%latvector(1,1)**2 + lv%latvector(2,1)**2 + lv%latvector(3,1)**2
    celly = lv%latvector(1,2)**2 + lv%latvector(2,2)**2 + lv%latvector(3,2)**2
    cellz = lv%latvector(1,3)**2 + lv%latvector(2,3)**2 + lv%latvector(3,3)**2

    cellmin = cellx

    if (celly < cellx) cellmin = celly

    if (cellz < celly) cellmin = cellz

    cellmin = sqrt(cellmin)

end subroutine mindimension


!> read in lattice vectors from CONFIG file
subroutine inputlatticevectors(lv)

    use kinds_f90
    use constants_module, only : ucfg
    use latticevectors_type
    use parse_module

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    integer :: i, j, counts, nread

    character :: record*100, word*40

    logical :: safe, founda

    nread = ucfg

    !TU: The convention used throughout the code is that
    !TU: latvector(i,j) is the jth component (x,y,z) of lattice vector i.
    !TU: The assumption is that the first line in CONFIG corresponds to the
    !TU: components of the first cell vector, the second to the second, etc.
    do i = 1, 3

        call get_line(safe, nread, record)
        if (.not.safe) call error(200)

        do j = 1, 3

            call get_word(record, word)
            lv%latvector(i,j) = word_2_real(word)

        end do

    end do

end subroutine inputlatticevectors


!> Check orthogonality of lattice vectors (cell matrix)
subroutine checkorthogonality(lv,ortho_flag)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> orthogonality flag read from CONTROL
    logical, intent(inout) :: ortho_flag

    integer :: i, j, counts, nread

    if ( ortho_flag ) then
!JG: set is_simple_orthogonal and is_orthogonal flags

        lv%is_simple_orthogonal = .true.
        lv%is_orthogonal = .true.

        do i = 1, 3
            do j = 1,3
                if( j/=i .and. lv%latvector(j,i)/=0.0_wp ) lv%is_simple_orthogonal = .false.
            end do

            counts = count( lv%latvector(i,:) == 0.0_wp )
            if( counts /= 2) lv%is_orthogonal = .false.

            counts = count( lv%latvector(:,i) == 0.0_wp )
            if( counts /= 2) lv%is_orthogonal = .false.

        end do

        if( lv%is_orthogonal ) then
            call setorthovec(lv)
        end if

    else

        lv%is_simple_orthogonal = .false. 
        lv%is_orthogonal = .false. 

    end if

    ortho_flag = lv%is_orthogonal

end subroutine checkorthogonality


!> Sets orthogonal vector lengths and half vector lengths for orthogonal PBC and PIC
subroutine setorthovec(lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    integer :: i, j


    do i = 1, 3
        do j = 1, 3
            if( lv%latvector(j,i) /= 0.0_wp ) then
                lv%fullvector(j) = lv%latvector(j,i)
                lv%halfvector(j) = lv%latvector(j,i)*0.5_wp
            end if
        end do
    end do


end subroutine setorthovec


!TU: Added by me... A function to calculate the inverse of a 3x3 matrix,
!TU: modified from a function found at http://fortranwiki.org/fortran/show/Matrix+inversion
function matinv3(A) result(B)
    
    ! The matrix to invert
    real(wp), intent(in), dimension(3,3) :: A
    ! The calculated inverse matrix
    real(wp), dimension(3,3) :: B

    ! Local variables
    real(wp) :: det, detinv

    ! Calculate the determinant of the matrix
    det = (  A(1,1) * A(2,2) * A(3,3) - A(1,1) * A(2,3) * A(3,2)  &
           - A(1,2) * A(2,1) * A(3,3) + A(1,2) * A(2,3) * A(3,1)  &
           + A(1,3) * A(2,1) * A(3,2) - A(1,3) * A(2,2) * A(3,1) )

    ! Calculate the inverse determinant of the matrix
    detinv = 1.0_wp / det

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * ( A(2,2) * A(3,3) - A(2,3) * A(3,2) )
    B(2,1) = -detinv * ( A(2,1) * A(3,3) - A(2,3) * A(3,1) )
    B(3,1) = +detinv * ( A(2,1) * A(3,2) - A(2,2) * A(3,1) )
    B(1,2) = -detinv * ( A(1,2) * A(3,3) - A(1,3) * A(3,2) )
    B(2,2) = +detinv * ( A(1,1) * A(3,3) - A(1,3) * A(3,1) )
    B(3,2) = -detinv * ( A(1,1) * A(3,2) - A(1,2) * A(3,1) )
    B(1,3) = +detinv * ( A(1,2) * A(2,3) - A(1,3) * A(2,2) )
    B(2,3) = -detinv * ( A(1,1) * A(2,3) - A(1,3) * A(2,1) )
    B(3,3) = +detinv * ( A(1,1) * A(2,2) - A(1,2) * A(2,1) )

end function


!> invert lattice/cell matrix (vectors)
subroutine invertlatvec(lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    !TU: A note on the order of dimensions in latvector and invlat...
    !    Usage throughout DL_MONTE is as follows. To yield rpos in Cartesian from fractional 
    !    (xx,yy,zz are fractional coords)...
    !
    !       cfgs(ib)%mols(im)%atms(ia)%rpos(i) = cfgs(ib)%vec%latvector(1,i) * xx + &
    !                                            cfgs(ib)%vec%latvector(2,i) * yy + &
    !                                            cfgs(ib)%vec%latvector(3,i) * zz
    !    or explicitly...
    !
    !       cfgs(ib)%mols(im)%atms(ia)%rpos(1) = latvector(1,1)*xx+latvector(2,1)*yy+latvector(3,1)*zz
    !       cfgs(ib)%mols(im)%atms(ia)%rpos(2) = latvector(1,2)*xx+latvector(2,2)*yy+latvector(3,2)*zz
    !       cfgs(ib)%mols(im)%atms(ia)%rpos(3) = latvector(1,3)*xx+latvector(2,3)*yy+latvector(3,3)*zz
    !
    !    This implies that latvector(i,1), latvector(i,2), latvector(i,3) is the ith cell vector. 
    !      
    !    Note that in matrix multiplication notation cpos = latvector^T fpos, where cpos and fpos are 
    !    the positions in Cartesian and fractional coordinates, respectively, and T denotes the
    !    transpose (and we are assuming that row i and column j in the matrix corresponding to latvector
    !    is latvector(i,j))
    !
    !    Usage for the inverse operation to yield fractional coordinates from Cartesian is
    !
    !       xx = cfgs(ib)%vec%invlat(1,1) * rx + &
    !            cfgs(ib)%vec%invlat(2,1) * ry + &
    !            cfgs(ib)%vec%invlat(3,1) * rz
    !      
    !    Similarly to above, this corresponds to fpos = invlat^T cpos. Setting invlat to the inverse of
    !    latvec gives the correct behaviour for the above usage.

    lv%invlat = matinv3(lv%latvector)

end subroutine invertlatvec


!> store lattice/cell matrix (vectors)
subroutine storelatticevectors(lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    lv%latstore = lv%latvector
    lv%invstore = lv%invlat
    lv%rcpstore = lv%rcpvector
    lv%volstore = lv%volume

end subroutine storelatticevectors


!> restore lattice/cell matrix (vectors) saved previously
subroutine restorelatticevectors(lv)

    use kinds_f90

    use latticevectors_type
    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

    integer :: i, j

    lv%latvector = lv%latstore
    lv%invlat = lv%invstore
    lv%rcpvector = lv%rcpstore

    lv%volume = lv%volstore

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine restorelatticevectors


!> scale lattice/cell matrix (lattice vectors) by dimension scaling factors
subroutine scalevectors(sx, sy, sz, lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> scaling dimension factors (X, Y, Z)
    real(kind=wp), intent(in) :: sx, sy, sz

    integer :: i, j

    lv%latvector(1,1) = lv%latvector(1,1) * sx
    lv%latvector(1,2) = lv%latvector(1,2) * sx
    lv%latvector(1,3) = lv%latvector(1,3) * sx

    lv%latvector(2,1) = lv%latvector(2,1) * sy
    lv%latvector(2,2) = lv%latvector(2,2) * sy
    lv%latvector(2,3) = lv%latvector(2,3) * sy

    lv%latvector(3,1) = lv%latvector(3,1) * sz
    lv%latvector(3,2) = lv%latvector(3,2) * sz
    lv%latvector(3,3) = lv%latvector(3,3) * sz

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine scalevectors


!> scale the cell matrix (lattice vectors) via reduced dimension increments
subroutine growvectors(sx, sy, sz, lv)

    use kinds_f90
    use latticevectors_type

    implicit none

        !> cell matrix (cell vectors container) to work on
    type(latticevectors), intent(inout) :: lv

        !> reduced dimension increments (X, Y, Z)
    integer, intent(in) :: sx, sy, sz

    real (kind = wp) :: gx, gy, gz

    integer :: i, j

    gx = real(sx) + 1.0
    gy = real(sy) + 1.0
    gz = real(sz) + 1.0

    lv%latvector(1,1) = lv%latvector(1,1) * gx
    lv%latvector(1,2) = lv%latvector(1,2) * gx
    lv%latvector(1,3) = lv%latvector(1,3) * gx

    lv%latvector(2,1) = lv%latvector(2,1) * gy
    lv%latvector(2,2) = lv%latvector(2,2) * gy
    lv%latvector(2,3) = lv%latvector(2,3) * gy

    lv%latvector(3,1) = lv%latvector(3,1) * gz
    lv%latvector(3,2) = lv%latvector(3,2) * gz
    lv%latvector(3,3) = lv%latvector(3,3) * gz

    if( lv%is_orthogonal ) call setorthovec(lv)

end subroutine growvectors


!> convert coordinates: fractional to cartesian
subroutine frac_to_cart(cfg)

    use kinds_f90
    use config_type

    implicit none

        !> configuration container to work on
    type(config), intent(inout) :: cfg

    integer :: i, j
    real(kind = wp) :: rx, ry, rz

    do j = 1, cfg%num_mols

        do i = 1, cfg%mols(j)%natom

            rx = cfg%mols(j)%atms(i)%rpos(1)
            ry = cfg%mols(j)%atms(i)%rpos(2)
            rz = cfg%mols(j)%atms(i)%rpos(3)

            cfg%mols(j)%atms(i)%rpos(1) = cfg%vec%latvector(1,1) * rx + cfg%vec%latvector(2,1) * ry +   &
                                         cfg%vec%latvector(3,1) * rz
            cfg%mols(j)%atms(i)%rpos(2) = cfg%vec%latvector(1,2) * rx + cfg%vec%latvector(2,2) * ry +   &
                                         cfg%vec%latvector(3,2) * rz
            cfg%mols(j)%atms(i)%rpos(3) = cfg%vec%latvector(1,3) * rx + cfg%vec%latvector(2,3) * ry +   &
                                         cfg%vec%latvector(3,3) * rz

        enddo

    enddo

end subroutine frac_to_cart


!> convert coordinates: cartesian to fractional
subroutine cart_to_frac(cfg)

    use kinds_f90
    use config_type

    implicit none

        !> configuration container to work on
    type(config), intent(inout) :: cfg

    integer :: i,j
    real(kind = wp) :: rx, ry, rz

    ! should no longer be necessary as a current version of invlat
    ! is held in memory
    ! call invertlatvec(cfg%vec)

    do j = 1, cfg%num_mols

        do i = 1, cfg%mols(j)%natom

            rx = cfg%mols(j)%atms(i)%rpos(1)
            ry = cfg%mols(j)%atms(i)%rpos(2)
            rz = cfg%mols(j)%atms(i)%rpos(3)

            cfg%mols(j)%atms(i)%rpos(1) = cfg%vec%invlat(1,1) * rx + cfg%vec%invlat(2,1) * ry +    &
                                         cfg%vec%invlat(3,1) * rz
            cfg%mols(j)%atms(i)%rpos(2) = cfg%vec%invlat(1,2) * rx + cfg%vec%invlat(2,2) * ry +    &
                                         cfg%vec%invlat(3,2) * rz
            cfg%mols(j)%atms(i)%rpos(3) = cfg%vec%invlat(1,3) * rx + cfg%vec%invlat(2,3) * ry +    &
                                         cfg%vec%invlat(3,3) * rz

        enddo

    enddo

end subroutine cart_to_frac


!> calculate the dimensional properies of simulation cell (lattice vectors)
Subroutine dcell(aaa,bbb)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to calculate the dimensional properies of a
! simulation cell specified by the input 3x3 matrix aaa (cell vectors in
! rows, the matrix is in the form of one dimensional reading
! (row1,row2,row3).
!
! The results are returned in the array bbb, with:
!
! bbb(1 to 3) - lengths of cell vectors
! bbb(4 to 6) - cosines of cell angles
! bbb(7 to 9) - perpendicular cell widths
! bbb(10)     - cell volume
!
! copyright - daresbury laboratory
! author    - w.smith july 1992
! amended   - i.t.todorov may 2008
!ammended again for monte 
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Use kinds_f90
  use constants_module, only : TORADIANS

  implicit none


  real(kind = wp), intent(in) :: aaa(3,3)
  Real( Kind = wp ), Dimension( 1:10 ), Intent(   Out ) :: bbb

  Real( Kind = wp ) :: axb1,axb2,axb3,bxc1,bxc2,bxc3,cxa1,cxa2,cxa3, &
                       x(1:3),y(1:3),z(1:3),d(1:3)

! calculate lengths of cell vectors

  bbb(1)=Sqrt(aaa(1,1)**2+aaa(1,2)**2+aaa(1,3)**2)
  bbb(2)=Sqrt(aaa(2,1)**2+aaa(2,2)**2+aaa(2,3)**2)
  bbb(3)=Sqrt(aaa(3,1)**2+aaa(3,2)**2+aaa(3,3)**2)

! calculate cosines of cell angles

  bbb(4)=(aaa(1,1)*aaa(2,1)+aaa(1,2)*aaa(2,2)+ &
         aaa(1,3)*aaa(2,3))/(bbb(1)*bbb(2))
  bbb(5)=(aaa(1,1)*aaa(3,1)+aaa(1,2)*aaa(3,2)+ &
         aaa(1,3)*aaa(3,3))/(bbb(1)*bbb(3))
  bbb(6)=(aaa(2,1)*aaa(3,1)+aaa(2,2)*aaa(3,2)+ &
         aaa(2,3)*aaa(3,3))/(bbb(2)*bbb(3))

! calculate vector products of cell vectors

  axb1=aaa(1,2)*aaa(2,3)-aaa(1,3)*aaa(2,2)
  axb2=aaa(1,3)*aaa(2,1)-aaa(1,1)*aaa(2,3)
  axb3=aaa(1,1)*aaa(2,2)-aaa(1,2)*aaa(2,1)

  bxc1=aaa(2,2)*aaa(3,3)-aaa(2,3)*aaa(3,2)
  bxc2=aaa(2,3)*aaa(3,1)-aaa(2,1)*aaa(3,3)
  bxc3=aaa(2,1)*aaa(3,2)-aaa(2,2)*aaa(3,1)

  cxa1=aaa(3,2)*aaa(1,3)-aaa(1,2)*aaa(3,3)
  cxa2=aaa(1,1)*aaa(3,3)-aaa(1,3)*aaa(3,1)
  cxa3=aaa(1,2)*aaa(3,1)-aaa(1,1)*aaa(3,2)

! calculate volume of cell

  bbb(10)=Abs(aaa(1,1)*bxc1+aaa(1,2)*bxc2+aaa(1,3)*bxc3)

! calculate cell perpendicular widths

  d(1)=bbb(10)/Sqrt(bxc1*bxc1+bxc2*bxc2+bxc3*bxc3)
  d(2)=bbb(10)/Sqrt(cxa1*cxa1+cxa2*cxa2+cxa3*cxa3)
  d(3)=bbb(10)/Sqrt(axb1*axb1+axb2*axb2+axb3*axb3)

  x(1)=Abs(aaa(1,1))/bbb(1) ; y(1)=Abs(aaa(1,2))/bbb(1) ; z(1)=Abs(aaa(1,3))/bbb(1)
  x(2)=Abs(aaa(2,1))/bbb(2) ; y(2)=Abs(aaa(2,2))/bbb(2) ; z(2)=Abs(aaa(2,3))/bbb(2)
  x(3)=Abs(aaa(3,1))/bbb(3) ; y(3)=Abs(aaa(3,2))/bbb(3) ; z(3)=Abs(aaa(3,3))/bbb(3)

! distribute widths
  If      (x(1) >= x(2) .and. x(1) >= x(3)) Then
     bbb(7)=d(1)
     If (y(2) >= y(3)) Then
        bbb(8)=d(2)
        bbb(9)=d(3)
     Else
        bbb(8)=d(3)
        bbb(9)=d(2)
     End If
  Else If (x(2) >= x(1) .and. x(2) >= x(3)) Then
     bbb(7)=d(2)
     If (y(1) >= y(3)) Then
        bbb(8)=d(1)
        bbb(9)=d(3)
     Else
        bbb(8)=d(3)
        bbb(9)=d(1)
     End If
  Else
     bbb(7)=d(3)
     If (y(1) >= y(2)) Then
        bbb(8)=d(1)
        bbb(9)=d(2)
     Else
        bbb(8)=d(2)
        bbb(9)=d(1)
     End If
  End If

  bbb(4) = acos(bbb(4)) / TORADIANS
  bbb(5) = acos(bbb(5)) / TORADIANS
  bbb(6) = acos(bbb(6)) / TORADIANS

End Subroutine dcell


!> calculate the dimensional properies of simulation cell (lattice vectors)
Subroutine mydcell(aaa,bbb)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to calculate the dimensional properies of a
! simulation cell specified by the input 3x3 matrix aaa (cell vectors in
! rows, the matrix is in the form of one dimensional reading
! (row1,row2,row3).
!
! The results are returned in the array bbb, with:
!
! bbb(1 to 3) - lengths of cell vectors
! bbb(4 to 6) - cosines of cell angles
! bbb(7 to 9) - perpendicular cell widths
! bbb(10)     - cell volume
!
! copyright - daresbury laboratory
! author    - w.smith july 1992
! amended   - i.t.todorov may 2008
!ammended again for monte 
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Use kinds_f90
  use constants_module, only : TORADIANS

  implicit none


  real(kind = wp), intent(in) :: aaa(3,3)
  Real( Kind = wp ), Dimension( 1:10 ), Intent(   Out ) :: bbb

  Real( Kind = wp ) :: axb1,axb2,axb3,bxc1,bxc2,bxc3,cxa1,cxa2,cxa3, &
                       x(1:3),y(1:3),z(1:3),d(1:3)

! calculate lengths of cell vectors

  bbb(1)=Sqrt(aaa(1,1)**2+aaa(1,2)**2+aaa(1,3)**2)
  bbb(2)=Sqrt(aaa(2,1)**2+aaa(2,2)**2+aaa(2,3)**2)
  bbb(3)=Sqrt(aaa(3,1)**2+aaa(3,2)**2+aaa(3,3)**2)

! calculate cosines of cell angles

  bbb(4)=(aaa(1,1)*aaa(2,1)+aaa(1,2)*aaa(2,2)+ &
         aaa(1,3)*aaa(2,3))/(bbb(1)*bbb(2))
  bbb(5)=(aaa(1,1)*aaa(3,1)+aaa(1,2)*aaa(3,2)+ &
         aaa(1,3)*aaa(3,3))/(bbb(1)*bbb(3))
  bbb(6)=(aaa(2,1)*aaa(3,1)+aaa(2,2)*aaa(3,2)+ &
         aaa(2,3)*aaa(3,3))/(bbb(2)*bbb(3))

! calculate vector products of cell vectors

  axb1=aaa(1,2)*aaa(2,3)-aaa(1,3)*aaa(2,2)
  axb2=aaa(1,3)*aaa(2,1)-aaa(1,1)*aaa(2,3)
  axb3=aaa(1,1)*aaa(2,2)-aaa(1,2)*aaa(2,1)

  bxc1=aaa(2,2)*aaa(3,3)-aaa(2,3)*aaa(3,2)
  bxc2=aaa(2,3)*aaa(3,1)-aaa(2,1)*aaa(3,3)
  bxc3=aaa(2,1)*aaa(3,2)-aaa(2,2)*aaa(3,1)

  cxa1=aaa(3,2)*aaa(1,3)-aaa(1,2)*aaa(3,3)
  cxa2=aaa(1,1)*aaa(3,3)-aaa(1,3)*aaa(3,1)
  cxa3=aaa(1,2)*aaa(3,1)-aaa(1,1)*aaa(3,2)

! calculate volume of cell

  bbb(10)=Abs(aaa(1,1)*bxc1+aaa(1,2)*bxc2+aaa(1,3)*bxc3)

! calculate cell perpendicular widths

  d(1)=bbb(10)/Sqrt(bxc1*bxc1+bxc2*bxc2+bxc3*bxc3)
  d(2)=bbb(10)/Sqrt(cxa1*cxa1+cxa2*cxa2+cxa3*cxa3)
  d(3)=bbb(10)/Sqrt(axb1*axb1+axb2*axb2+axb3*axb3)

  x(1)=Abs(aaa(1,1))/bbb(1) ; y(1)=Abs(aaa(1,2))/bbb(1) ; z(1)=Abs(aaa(1,3))/bbb(1)
  x(2)=Abs(aaa(2,1))/bbb(2) ; y(2)=Abs(aaa(2,2))/bbb(2) ; z(2)=Abs(aaa(2,3))/bbb(2)
  x(3)=Abs(aaa(3,1))/bbb(3) ; y(3)=Abs(aaa(3,2))/bbb(3) ; z(3)=Abs(aaa(3,3))/bbb(3)

!JG:  don't distribute widths

     bbb(7)=d(1)
     bbb(8)=d(2)
     bbb(9)=d(3)
! distribute widths
!  If      (x(1) >= x(2) .and. x(1) >= x(3)) Then
!     bbb(7)=d(1)
!     If (y(2) >= y(3)) Then
!        bbb(8)=d(2)
!        bbb(9)=d(3)
!     Else
!        bbb(8)=d(3)
!        bbb(9)=d(2)
!     End If
!  Else If (x(2) >= x(1) .and. x(2) >= x(3)) Then
!     bbb(7)=d(2)
!     If (y(1) >= y(3)) Then
!        bbb(8)=d(1)
!        bbb(9)=d(3)
!     Else
!        bbb(8)=d(3)
!        bbb(9)=d(1)
!     End If
!  Else
!     bbb(7)=d(3)
!     If (y(1) >= y(2)) Then
!        bbb(8)=d(1)
!        bbb(9)=d(2)
!     Else
!        bbb(8)=d(2)
!        bbb(9)=d(1)
!     End If
!  End If

  bbb(4) = acos(bbb(4)) / TORADIANS
  bbb(5) = acos(bbb(5)) / TORADIANS
  bbb(6) = acos(bbb(6)) / TORADIANS

End Subroutine mydcell


end module
