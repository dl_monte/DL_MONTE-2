! ***************************************************************************
! *   Copyright (C) 2015 by A.V.Brukhno                                     *
! *   andrey.brukhno[@]stfc.ac.uk                                           *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Abstract interface(s) for FED calculations: reading CONTROL/FIELD files, setting defaults, switches & parameters
!> @usage 
!> - most of the control is done via `<fed_interface_type>` instance `'fed'` normally
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `fed_order_module`
!> - `fed_calculus_module` (via `fed_order_module`)
!> - `fed_interface_type` & `arrays_module` (via `fed_calculus_module`)

!> @modulefor abstract interface(s) for FED calculations: setting defaults & reading CONTROL/FIELD files

module fed_interface_module

    use kinds_f90
    use constants_module
    use fed_order_module

    !AB: the following comes with the use of fed_order_module
    !use fed_calculus_module
    !use fed_interface_type
    !use comms_mpi_module, only : master
    !use parallel_loop_module, only : idgrp

    implicit none

    !real(kind = wp), parameter ::  HALF  = 0.5_wp
    !real(kind = wp), parameter ::  DHALF = HALF-1.e-12


    !AB: *** FED CALCULUS SCOPE *** - all the commented-out variables below are found in fed_calculus_module.f90


        ! FED control structure
    !type(fed_interface),save :: fed

        ! FED names container
    !type(fed_interface_name),save :: fed_name

        ! generic FED order parameter vector
    !real(kind=wp), allocatable, save :: parm_vec(:)

        ! generic FED histogram vector
    !real(kind=wp), allocatable, save :: hist_vec(:)

        ! generic FED bias vector
    !real(kind=wp), allocatable, save :: bias_vec(:)

        ! numbers of attempted & accepted FED moves (for each box/config)
    !integer, dimension(:), allocatable :: num_attempted_fedmoves, num_accepted_fedmoves

        ! previous & current order parameter values
    !real(kind=wp), save :: param_old, param_cur

        ! previous and current ID/index of (sub)state in FED sampling
    !integer, save :: id_old, id_cur

        ! running numbers of attempted & accepted FED moves
    !integer, save :: num_total_fedmoves, num_successful_fedmoves


    !AB: *** FED ORDER SCOPE *** - all the commented-out variables below are found in fed_order_module.f90


        ! molecule indices for complex order parameters
    !integer, allocatable, save :: grp_mol_ids(:,:)

        ! atom indices (within molecule) for complex order parameters
    !integer, allocatable, save :: grp_atm_ids(:,:)

        ! matrix to store molecule(s)/cluster(s) COM:s for complex order parameters
    !real(kind=wp), allocatable, save :: coms_pos(:,:,:)

    !!AB: the allocation of the above matrix must result in this sort of array:
    !!real(kind=wp), dimension(0:3, 0:ngrp_ids, 1:nconfig)  :: coms_pos

        ! numbers of entries read from FED input for molecules & atoms in COM specs
    !integer, save :: grp_nmols, grp_natms, ngrp_ids

        ! flags to indicate if molecule/atom indices were found in the input (CONTROL)
    !logical, save :: is_mol_grp, is_atm_grp


    !AB: *** LOCAL SCOPE *** - all local variables go below


        !> number of extra subsections within `use fed ...` block in CONTROL file
    integer, save :: num_specs, num_extras

    !logical, save :: is_bias_input

Private :: num_specs, num_extras !, is_bias_input


contains


!> check the input values of FED switches and parameters
subroutine check_fed_flavor(word)

    use parse_module

    implicit none

        !> keyword read from FED directive in CONTROL file
    character*(*), intent(inout) :: word

    if (word(1:3) == "gen") then

        fed % flavor = FED_GN
        fed_name % flavor = 'gen - Generic FED'

    else if (word(1:2) == "ps") then

        fed % flavor = FED_PS
        fed_name % flavor = 'psmc - Phase Switch'

    else if (word(1:2) == "hr") then ! .or. word(1:4) == "hist" ) then

        fed % flavor = FED_HW
        fed_name % flavor = 'hr - Histogram Reweighting'

    !else if (word(1:3) == "???") then

        !fed % flavor = FED_??
        !fed_name % flavor = '???'

    else

        call cry(uout,'', &
                 "ERROR: FED specification with unknown flavor '"//trim(word)// &
                 "' - either amend or revoke FED specs!!!",51)

        return

    endif

end subroutine check_fed_flavor


!> check the values of FED switches and parameters
subroutine check_fed_method(word)

    use parse_module

    implicit none

        !> keyword read from FED directive in CONTROL file
    character*(*), intent(inout) :: word

    logical :: is_float

    if( indexnum(word,is_float) == 1 ) then

        if( is_float ) then 

            call cry(uout,'', &
                 "ERROR: FED specification with unknown method '"//trim(word)// &
                 "' - either amend or revoke FED specs!!!",51)

            return

        endif

        fed % method = nint(word_2_real(word))

    endif

    if (word(1:2) == "us" .or. fed % method == FED_US ) then

        fed % method = FED_US
        fed_name % method = 'us - Umbrella Sampling'

        !call cry(uout,'', &
        !         "ERROR: FED-US is currently unsupported - stopping program!!!",51)

        !AB: FED US subsection is expected
        !num_specs = num_specs + 1

    else if (word(1:2) == "ee" .or. fed % method == FED_EE) then

        fed % method = FED_EE
        fed_name % method = 'ee - Expanded Ensemble'

        !AB: FED EE subsection is expected
        !num_specs = num_specs + 1

    else if (word(1:2) == "wl" .or. fed % method == FED_WL) then

        fed % method = FED_WL
        fed_name % method = 'wl - Wang-Landau'

        !AB: FED WL subsection is expected
        !num_specs = num_specs + 1

    else if (word(1:2) == "tm" .or. fed % method == FED_TM) then

        fed % method = FED_TM
        fed_name % method = 'tm - Transition Matrix'

        !AB: FED TM subsection is expected
        !num_specs = num_specs + 1
        
    !else if (word(1:2) == "??" .or. fed % method == FED_??) then

        !fed % method = FED_??
        !fed_name % method = '????'

    else 

        call cry(uout,'', &
                "ERROR: FED specification with unknown method '"//trim(word)// &
                "' - either amend or revoke FED specs!!!",51)

        return

    endif

end subroutine check_fed_method


!> check the values of FED switches and parameters
subroutine check_fed_param(word)

    use parse_module

    implicit none

        !> keyword read from FED directive in CONTROL file
    character*(*), intent(inout) :: word

    logical :: is_float

        if( indexnum(word,is_float) == 1 ) then 

            if( is_float ) then 

                call cry(uout,'', &
                     "ERROR: FED specification with unknown order parameter type '"// &
                     trim(word)//"' - either amend or revoke FED specs!!!",51)

                return

            endif

            fed % par_kind = nint(word_2_real(word))

        end if

        if( word(1:4) == "com2" .or. fed % par_kind == FED_PAR_DIST2 ) then

            fed % par_kind = FED_PAR_DIST2
            fed_name % parameter = 'com2 - 2 COMs distance'

        else if( word(1:4) == "com1" .or. fed % par_kind == FED_PAR_DIST1 ) then

            fed % par_kind = FED_PAR_DIST1
            fed_name % parameter = 'com1 - 1 COM distance'

        else if( word(1:4) == "temp" .or. fed % par_kind == FED_PAR_TEMP ) then

            fed % par_kind = FED_PAR_TEMP
            fed_name % parameter = 'temp - Temperature'

        else if( word(1:4) == "beta" .or. fed % par_kind == FED_PAR_BETA ) then

            fed % par_kind = FED_PAR_BETA
            fed_name % parameter = 'beta - 1/(k_B*T)'

        else if( word(1:3) == "vol" .or. fed % par_kind == FED_PAR_VOLM ) then

            fed % par_kind = FED_PAR_VOLM
            fed_name % parameter = 'volm - Volume'

        else if( word(1:4) == "dens" .or. fed % par_kind == FED_PAR_DENS ) then

            fed % par_kind = FED_PAR_DENS
            fed_name % parameter = 'dens - Density'

            !AB: An order parameter 'dens' extra subsection is expected (if any)
            !num_extras = num_extras+1
        
        else if( word(1:2) == "ps" .or. fed % par_kind == FED_PAR_PSMC ) then

            fed % par_kind = FED_PAR_PSMC
            fed_name % parameter = 'ps - Phase Switch'

            !TU: Return an error if this is used with any non-PSMC flavor
            !TU: Note that this does not forbid using PSMC flavor with non-PSMC
            !TU: order parameter, which someone crazy may wish to do. (See warning
            !TU: below)
            if( fed % flavor /= FED_PS )  then

                call cry(uout,'', &
                    "ERROR: PSMC order parameter MUST be used in conjunction with "// &
                    "PSMC FED flavor (i.e., 'use fed ps')!",51)

             end if

        else if( word(1:6) == "hardps" .or. fed % par_kind == FED_PAR_PSMC_HS ) then

            fed % par_kind = FED_PAR_PSMC_HS
            fed_name % parameter = 'hardps - Phase Switch for hard spheres'

            !TU: Return an error if this is used with any non-PSMC flavor
            !TU: Note that this does not forbid using PSMC flavor with non-PSMC
            !TU: order parameter, which someone crazy may wish to do. (See warning
            !TU: below)
            if( fed % flavor /= FED_PS )  then

                call cry(uout,'', &
                    "ERROR: PSMC order parameter MUST be used in conjunction with "// &
                    "PSMC FED flavor (i.e., 'use fed ps')!",51)

             end if

        else if( word(1:5) == "nmols" .or. fed % par_kind == FED_PAR_NMOLS ) then

            fed % par_kind = FED_PAR_NMOLS
            fed_name % parameter = 'nmols - Total number of molecules'

        else if( word(1:8) == "nspecmol" .or. fed % par_kind == FED_PAR_NMOLS_SPEC ) then

            fed % par_kind = FED_PAR_NMOLS_SPEC
            fed_name % parameter = 'nspecmol - Number of given species of mol'

        else if( word(1:6) == "natoms" .or. fed % par_kind == FED_PAR_NATOMS ) then

            fed % par_kind = FED_PAR_NATOMS
            fed_name % parameter = 'natoms - Total number of atoms'

        else if( word(1:10) == "nspecatom" .or. fed % par_kind == FED_PAR_NATOMS_SPEC ) then

            fed % par_kind = FED_PAR_NATOMS_SPEC
            fed_name % parameter = 'nspecatom - Number of given species of atom'

        else if( word(1:9) == "lcscalar2" .or. fed % par_kind == FED_PAR_LCS2 ) then

            fed % par_kind = FED_PAR_LCS2
            fed_name % parameter = 'lcscalar2 - Liquid crystal scalar, 2nd order Legendre'

        else if( word(1:9) == "lcscalar4" .or. fed % par_kind == FED_PAR_LCS4 ) then

            fed % par_kind = FED_PAR_LCS4
            fed_name % parameter = 'lcscalar4 - Liquid crystal scalar, 4th order Legendre'

        else if( word(1:4) == "lamb" .or. fed % par_kind == FED_PAR_LAMBDA ) then

            fed % par_kind = FED_PAR_LAMBDA
            fed_name % parameter = 'lamb - Lambda'
        
        !else if( word(1:4) == "????" .or. fed % par_kind == FED_PAR_???? ) then
        !    fed % par_kind = FED_PAR_????
        !    fed_name % parameter = '????'

        else 

            call cry(uout,'', &
                 "ERROR: FED specification with unknown order parameter type "// &
                 trim(word)//" - either amend or revoke FED specs!!!",51)

            return

        end if

        !TU: Print a warning if PSMC flavor is used with non-PSMC order parameter
        if( fed % flavor == FED_PS .and. &
            fed % par_kind /= FED_PAR_PSMC .and. fed % par_kind /= FED_PAR_PSMC_HS ) then

            call cry(uout,'', &

                "WARNING: PSMC flavor is not used with a PSMC order parameter. I hope you"// &
                "know what you are doing...",0)

        end if

end subroutine check_fed_param


!> initialise the values of FED switches and parameters
subroutine initialise_fed(job, line, input)

    !use kinds_f90
    use control_type
    use parse_module
    use psmc_module, only : read_psmc_extras
    use species_module, only : set_species_type

    implicit none

        !> simulation control structure: global switches & parameters
    type(control), intent(inout) :: job

        !> line/record read in from CONTROL file (see `control_module::read_control(job)`)
    character*(*), intent(inout) :: line

        !> input file unit identifier (assigned to CONTROL file)
    integer, intent(in) :: input

    ! internal variables go below

        ! temporary keyword & value storage
    character :: word*40, fname*80

        ! temporary variable storage
    real (kind = wp) :: param, delta, power

        ! indices & counters
    integer :: k, kmax, km, ko, kout

    logical :: is_float!, is_bias_input

    logical :: is_input_ready

    !AB: number of molecule/atom indices (id's) & COMs for complex order parameters
    grp_nmols = 0
    grp_natms = 0
    ngrp_ids  = 0

    !AB: two compulsory FED directives: 'fed method <name> <specs>', 'fed order [param] <name> <grid_specs>'
    !AB: but maybe more present/needed
    num_specs  = 2

    !AB: number of extra FED subsections within 'use fed ...' block, if any - to be replaced by num_specs!
    num_extras = 0

    !AB: flag/switch for reading (or not) the initial FED bias from input (FEDDAT.0)
    fed%is_bias_input  = .false.

    !AB: flag for readiness ('opened' state) of the input file - a precaution measure
    is_input_ready = .false.

    !AB: if 'input' file unit is ready for reading (CONTROL),
    !AB: first, extract FED flavor and frequency of attempts
    !AB: then,  read in 'fed method <name> <specs>' and 'fed order [param] <name> <grid_specs>'
    !AB: also,  read in any extra subsections, like 'fed lsmc/psmc ...'
    !AB: the order of directives and subsections should not matter!

    if( master ) then

        inquire (input, NAME=fname, opened=is_input_ready, action=word)

        fname = adjustL(fname)
        word  = adjustL(word)

        is_input_ready = ( is_input_ready .and. word(1:4)=='READ' )

        if( .not. is_input_ready ) then

            write(uout,*)
            write(uout,*)"initialise_fed(): input file unit ",input, &
                         " named '",trim(fname)," is not ready for reading!!!"

            write(uout,*)"initialise_fed(): input file state - ",is_input_ready,trim(word)

            call error(4)  ! "problem reading CONTROL file"

!debugging
        !else
        !    write(uout,*)"initialise_fed(): input file unit ",input," named '",trim(fname),"' will be read further..."
        !    write(uout,*)"initialise_fed(): input file state - ",is_input_ready,trim(adjustL(word))
!debugging
        end if

    end if

    fed % is_on   = .true.
    job % fedcalc = .true.

    id_old = 1
    id_cur = 1

    !get FED flavor
    call get_word(line, word)
    call check_fed_flavor(word)

    !get frequency of FED attepts (in MC steps/sweeps)
    call get_word(line, word)

    if( word /= "" .and. word(1:1) /= "#" ) then

        fed % numfedskip = nint(word_2_real(word))
        if( fed % numfedskip < 1 ) fed % numfedskip = 1

    end if

    !TU: For the PSMC flavor read the PSMC sub-block immediately after the 'use fed ps' directive
    if( fed % flavor == FED_PS ) call read_psmc_extras(input)

    !AB: two compulsory FED directives: 'fed method <name> <specs>', 'fed order [param] <type> <grid_specs>'
    !AB: but maybe more present/needed: 'fed lsmc ...' etc

    !AB: get 'specs' for FED method and 'grid' bins/min/max/type for order parameter - new way of doing it
    call read_fed_specs(input)

    !TU: Note that this allocates 'tm_matrix' and sets it to 0 if TM is in use
    call fed_reallocate_vectors(job%nconfigs)

    !write(*,*)'initialise_fed():: allocated FED vectors: ',fed%par_min,' ... ',fed%par_max,' ',fed%numstates,' ',fed%par_kind

    !flush(uout)

    !AB: check and amend parameter settings

    kmax = fed%numstates
    km   = 0

    !AB: discrete sets are a bit different (as opposed to a continuous bin-centered grid)
    if( fed%par_kind == FED_PAR_TEMP .or. &
        fed%par_kind == FED_PAR_BETA .or. &
        fed%par_kind == FED_PAR_LAMB ) then

        km = 1

    else if( fed % numfedskip > 1 ) then 

        fed % numfedskip = 1

        call cry(uout,'', &
                 "*WARNING*: natural order parameter (FED) sampling is always done at every relevant MC step...",0)

    end if

    fed%im_cont = 1-km

    ! for Beta-variation convert input to 1/T
    if( fed%par_kind == FED_PAR_BETA )  then

        delta = fed%par_min
        fed%par_min = 1.0_wp/(fed%par_max*BOLTZMAN)
        fed%par_max = 1.0_wp/(delta*BOLTZMAN)

    end if

    delta = (fed%par_max - fed%par_min) / (kmax-km)

    power = fed%inc_kind

    if( fed%inc_kind > 1 ) then

        if( fed%par_kind /= FED_PAR_TEMP .and. &
            fed%par_kind /= FED_PAR_BETA .and. &
            fed%par_kind /= FED_PAR_LAMB ) then

            call cry(uout,'', &
                 "ERROR: non-linear growth of FED order parameter is "//&
                 "only allowed for discrete sets (T or Lambda) "//&
                 " - either amend or revoke FED specs!!!",51)

        end if

        delta = (fed%par_max - fed%par_min) / (kmax-km)**power

        do k = 1, kmax

            !AB: take care of the half-bin shift for discretised continuous ranges
            parm_vec(k) = fed%par_min + delta * ( real(k-km,wp) - HALF*fed%im_cont )**power

            ! for T-variation use 1/T
            if( fed%par_kind == FED_PAR_TEMP )  then

                parm_vec(k) = 1.0_wp/(parm_vec(k)*BOLTZMAN)

            end if

        end do

    else

        do k = 1, kmax

            !AB: take care of the half-bin shift for discretised continuous ranges
            parm_vec(k) = fed%par_min + delta * ( real(k-km,wp) - HALF*fed%im_cont )

            ! for T-variation use 1/T
            if( fed % par_kind == FED_PAR_TEMP ) then

                parm_vec(k) = 1.0_wp/(parm_vec(k)*BOLTZMAN)

            end if

        end do

    end if

    fed % par_inc = delta

    if( fed % nitr_smooth > 0 ) then
        if( fed % nbeg_smooth < 2 .or. &
            fed % nend_smooth > fed % numstates - 1 ) fed % nbeg_smooth = 2

        if( fed % nend_smooth < fed % nbeg_smooth .or. &
            fed % nend_smooth > fed % numstates - 1 ) fed % nend_smooth = fed % numstates - 1
    end if

    !AB: done - check and amend parameter settings

    if( master ) then

        write(uout,*)
        write(uout,'(a)')       " ----------------------------"
        write(uout,'(a)')       " Check the FED specification:"
        write(uout,'(a)')       " ----------------------------"
        write(uout,'(a,i10,3a)')" - the FED flavor to apply  = ",fed % flavor," '",trim(fed_name%flavor),"' "
        write(uout,'(a,i10,3a)')" - the FED method to apply  = ",fed % method," '",trim(fed_name%method),"' "
        write(uout,'(a,i10)')   " - the FED trials frequency = ",fed % numfedskip
        write(uout,'(a,i10)')   " - number of (sub-) states  = ",fed % numstates
        write(uout,'(a)')       " ----------------------------"

        !flush(uout)

        if( fed % method == FED_US ) then
        !AB: define the center and strength for Umbrella Sampling 

            write(uout,'(a,f20.9)')" - umbrella minimum point   = ",fed % ums_mid
            write(uout,'(a,f20.9)')" - umbrella force constant  = ",fed % ums_frc
            write(uout,'(a,i10)')  " - umbrella stats frequency = ",fed % numupdskip
            write(uout,'(a)')      " ----------------------------"

            !flush(uout)

        else if( fed % method == FED_EE ) then

            write(uout,'(a,f20.9)')" - EE bias feedback weight  = ",fed % upd_val
            write(uout,'(a,f20.9)')" - EE bias tune-up scaling  = ",fed % upd_scl
            write(uout,'(a,i10)')  " - EE feed update frequency = ",fed % numupdskip
            write(uout,'(a,i10)')  " - EE bias smoothing cycles = ",fed % nitr_smooth

            if( fed % nitr_smooth > 0 ) then

                write(uout,'(a,2i10)')  " - EE bias smoothing range  = ",fed % nbeg_smooth,fed % nend_smooth
                write(uout,'(a,f20.9)') " - EE bias smoothing weight = ",fed % wgt_smooth

            end if

            write(uout,'(a)')      " ----------------------------"

        else if( fed % method == FED_WL ) then

            write(uout,'(a,f20.9)')" - WL bias feedback droplet = ",fed % upd_val
            write(uout,'(a,f20.9)')" - WL bias tune-up scaling  = ",fed % upd_scl
            write(uout,'(a,i10)')  " - WL feed update frequency = ",fed % numupdskip
            write(uout,'(a,i10)')  " - WL bias smoothing cycles = ",fed % nitr_smooth

            if( fed % nitr_smooth > 0 ) then

                write(uout,'(a,2i10)')  " - WL bias smoothing range  = ",fed % nbeg_smooth,fed % nend_smooth
                write(uout,'(a,f20.9)') " - WL bias smoothing weight = ",fed % wgt_smooth

            end if

            write(uout,'(a)')      " ----------------------------"

        else if( fed % method == FED_TM ) then

            write(uout,'(a,l10)')  " - Tridiagonal-format TMATRX files in use  = ",fed % tm_tridiag
            write(uout,'(a,l10)')  " - TM matrix initialised using TMATRX file = ",fed % tm_resume
            write(uout,'(a,i10)')  " - TM bias update frequency with output    = ",fed % numupdskip
            write(uout,'(a,i10)')  " - TM bias update frequency without output = ",fed % numupdskipfine

            write(uout,'(a)')      " ----------------------------"


        !else if( fed % method == FED_?? ) then
        !AB: anything for other methods etc - ???

        end if

        write(uout,'(a,i10,3a)')" - FED order parameter type     = ",fed%par_kind, &
                                " '",trim(fed_name%parameter),"' "

        if( fed % par_kind == FED_PAR_NATOMS_SPEC ) then

            write(uout,'(a,20a)')" - FED parameter target species = ", &
                 trim(fed%par_natoms_spec_name)//" "//set_species_type(fed%par_natoms_spec_type)

        else if( fed % par_kind == FED_PAR_NMOLS_SPEC ) then

            write(uout,'(a,20a)')" - FED parameter target species = ", trim(fed%par_nmols_spec_name)

        end if


        write(uout,'(a,i10)')   " - FED parameter grid type      = ",fed%inc_kind
        write(uout,'(a,f20.9)') " - FED parameter minimum        = ",fed%par_min
        write(uout,'(a,f20.9)') " - FED parameter maximum        = ",fed%par_max
        write(uout,'(a,f20.9)') " - FED parameter delta          = ",fed%par_inc
        
        if( fed % is_window ) then

            write(uout,'(a)')        " - FED parameter constrained to a window"
            write(uout,'(a,f20.9)')  " - FED window minimum           = ",fed%window_min
            write(uout,'(a,f20.9)')  " - FED window maximum           = ",fed%window_max

        end if

        if( fed % soft_edges ) then

             write(uout,'(a)')        " - FED parameter range edges are soft: parameters outside specified range are allowed!"

        end if

        !write(uout,'(a)')       " ----------------------------"

        !flush(uout)

    end if

    !AB: output the initial FED vectors etc

    kmax = fed%numstates
    kout = min(5,kmax-1)
    write(word,'(a,i1,a,i1)') '(',kout,'e16.7,a,e16.7)'

    if( master ) &
        write(uout,*) &
             "----------------------------------------------------------------------------------------------------"

    if( fed % method == FED_TM ) then

        !TU: Import 'tm_matrix' from the file TMATRX if necessary, and recalculate the bias 
        !TU: function so that it reflects this matrix
        if( fed % tm_resume ) then
            
            call read_fed_tm()
            call fed_bias_upd_tm()

        end if

        if( master ) then
     
            write(uout,'(a)') "  - FED TM matrix diagonal elements :"

            if( fed%tm_tridiag ) then

                write(uout,trim(word)) (tm_matrix(k,2),k=1,kout),' ... ',tm_matrix(kmax,2)
                
            else

                write(uout,trim(word)) (tm_matrix(k,k),k=1,kout),' ... ',tm_matrix(kmax,kmax)

            end if
            
            write(uout,*) &
                "----------------------------------------------------------------------------------------------------"

        end if

    end if

    if( fed%is_bias_input ) then
    !AB: read in the bias input (free 3 column format, same as in output)

        call read_fed_bias()
        
        if( fed%method == FED_TM ) then

            if( master ) write(uout,'(a)') "  - FED biasing vector (bias input file) :"

        else

            if( master ) write(uout,'(a)') "  - FED biasing vector (input file) :"

        end if

    else

        if( fed%method == FED_TM .and. fed % tm_resume ) then

            if( master ) write(uout,'(a)') "  - FED biasing vector (from TM matrix) :"

        else

            if( master ) write(uout,'(a)') "  - FED biasing vector (no input)   :"

        end if

    end if

    !flush(uout)

    if( master ) then

        write(uout,trim(word)) (bias_vec(k),k=1,kout),' ... ',bias_vec(kmax)

        write(uout,*)&
             "----------------------------------------------------------------------------------------------------"
        write(uout,'(a)') "  - FED histogram vector :"

        write(uout,trim(word)) (hist_vec(k),k=1,kout),' ... ',hist_vec(kmax)

        write(uout,*)&
             "----------------------------------------------------------------------------------------------------"
        write(uout,'(a)') "  - FED parameter vector :"

        write(uout,trim(word)) (parm_vec(k),k=1,kout),' ... ',parm_vec(kmax)

        if( fed % par_kind == FED_PAR_TEMP .or. fed % par_kind == FED_PAR_BETA ) then

            write(uout,*)&
             "----------------------------------------------------------------------------------------------------"
            write(uout,'(a)') "  - FED original T-range :"

            write(uout,trim(word)) (1.0_wp/parm_vec(k)/BOLTZMAN,k=1,kout), &
                               ' ... ',1.0_wp/parm_vec(kmax)/BOLTZMAN

        endif

        write(uout,*)&
             "----------------------------------------------------------------------------------------------------"
        write(uout,*)

    endif

    !AB: done - reading additional *compulsory* specs for generic methods

    job % fedcalc = fed % is_on
    job % fed_trial_freq = fed % numfedskip

    !id_old = 1
    id_cur = id_old

    param_old = parm_vec(id_old)
    param_cur = param_old

    num_total_fedmoves = 0
    num_successful_fedmoves = 0

    !AB: temporary STOP
    !call error(999)

end subroutine initialise_fed


!> reading common specs for generic FED methods
subroutine read_fed_specs(nread)

    !use constants_module  ! due to module dependence
    use parse_module

    !AB: no need to arrange the 'call read_psmc_extras()' from here!
    !use psmc_module, only : read_psmc_extras

        !> input file unit (normally for `CONTROL` file)
    integer, intent(in) :: nread

        ! temporary keyword & value storage
    character :: line*100, word*40, word1*40 !, keyword*40

    integer :: keys_specs = 0
    integer :: keys_count = 0

    integer :: lname = 0

    logical :: is_fed_done, is_opt_done
    logical :: is_set_method, is_set_order, is_set_extra

    logical :: more
    logical :: safe

    keys_specs  = 0
    keys_count  = 0

    !is_fed_done = .false.
    is_fed_done = ( keys_specs == num_specs )
    is_opt_done = .false.

    is_set_method = .false.
    is_set_order  = .false.
    is_set_extra  = ( num_specs < 3 )

    !write(uout,*)"read_fed_specs(): expected number of specs = ",num_specs," keys_specs = ",keys_specs !,is_fed_done

    more = .not.is_fed_done
    safe = .true.

    fed: do while( safe .and. more )

        call get_line(safe,nread,line)
        if (.not.safe) call error(51)

        call lower_case(line)

        !write(uout,*)"read_fed_specs(): read line '",trim(line),"'"

        call get_word(line, word)

        !AB: allow/skip the commented out lines starting with '#' 
        if( word == "" .or. word(1:1) == "#" ) cycle fed

        !write(uout,*)"read_fed_specs(): read word 1 '",trim(word),"'"

        if( word(1:3) == "fed" ) then

            call get_word(line, word)

            !write(uout,*)"read_fed_specs(): read word 2 '",trim(word),"'"

            if( word(1:4) == "done" ) then

                !AB: make sure all the necessary subsections 'fed ...' have been read before 'fed done'
                if( is_fed_done ) then

                    more = .false.

                    if( .not.is_set_method ) &
                        call cry(uout,'', &
                             "ERROR: incomplete 'fed ...' subsection(s) in 'use fed ...' block - "//&
                             "'fed method ...' subsection not found!!!",51)

                    if( .not.is_set_order ) &
                        call cry(uout,'', &
                             "ERROR: incomplete 'fed ...' subsection(s) in 'use fed ...' block - "//&
                             "'fed order ...' subsection not found!!!",51)

                    if( .not.is_set_extra ) &
                        call cry(uout,'', &
                             "ERROR: incomplete 'fed ...' subsection(s) in 'use fed ...' block - "//&
                             "some extra subsection not found (PSMC related?)!!!",51)

                    call cry(uout,'(/,1x,a,/)', &
                         "All 'fed ...' subsection(s) successfully read in 'use fed ...' block",0)

                    return

                else 

                    more = .false.

                    write(word,'(i2,a,i2)')keys_specs," / ",num_specs

                    call cry(uout,'', &
                         "ERROR: incomplete 'fed ...' subsection(s) in 'use fed ...' block - "//&
                         trim(word)//" subsections found only!!!",51)

                    return

                end if

            else 
            !if( is_fed_key(word, keyword) ) then !AB: the check appears not necessary - done in read_common_specs(..)

                call get_word(line, word1)

                !AB: allow two-word name for the subsection, like "order parameter ..."
                if( word(1:5) == "order" .and. word1(1:5) == "param" ) &
                    call get_word(line, word1)

                if( word1(1:4) == "done" ) then

                    !AB: make sure 'fed order/param <name>' subsection have been read in full before 'fed done'
                    if( is_opt_done ) then

                        is_opt_done = .false.

                        cycle fed

                    else 

                        call cry(uout,'', &
                             "ERROR: incomplete or unknown 'fed "//trim(word)//&
                             "' subsection in 'use fed ...' block!!!",51)

                    end if

                else if( word(1:5) == "order" .and. is_set_order ) then 

                    call cry(uout,'', &
                         "ERROR: repetitive 'fed "//trim(word)//&
                         "' subsection in 'use fed ...' block!!!",51)

                else if( word(1:6) == "method" .and. is_set_method ) then

                    call cry(uout,'', &
                         "ERROR: repetitive 'fed "//trim(word)//&
                         "' subsection in 'use fed ...' block!!!",51)

                end if

                is_opt_done = .false.

                if( word1 /= "" ) line = trim(word1)//" "//trim(line)

                !flush(uout)

                call read_common_specs(nread, word, line, is_opt_done)

                keys_specs = keys_specs+1

                if( is_opt_done ) then

                    call cry(uout,'(/,1x,a,/)',"'fed "//trim(word)//&
                         "' subsection has been successfully read in 'use fed ...' block",0)

                    !flush(uout)

                    is_fed_done = ( keys_specs == num_specs )

                    if( .not.is_set_method ) is_set_method = ( word(1:6) == "method" )
                    if( .not.is_set_order  ) is_set_order  = ( word(1:5) == "order" )
                    if( .not.is_set_extra  ) is_set_extra  = ( is_set_method .and. is_set_order .and. is_fed_done )

                end if

            end if ! done - if( word(1:4) ?= "done" ) ... else if( word(1:?) ?= <any_known_keyword> ) 

        else ! same as if( word(1:3) /= "fed" )

            more = .false.

            call cry(uout,'', &
                 "ERROR: inconsistent 'use fed ...' block - "//&
                 "'fed done' directive not reached (too many lines?)!!!",51)

            return

        end if ! done - if( word(1:3) ?= "fed" )

    end do fed ! done - while( more )

end subroutine read_fed_specs


!> reading known FED specs (within 'use fed ...' block)
subroutine read_common_specs(nread, word, line, is_done)

    !use constants_module  ! due to module dependence
    use parse_module

    integer, intent(in) :: nread

    character(*), intent(in) :: word

    character(*), intent(inout) :: line

    logical, intent(inout) :: is_done

    character :: word1*40

        ! temporary variable storage
    real (kind = wp) :: param, delta, power

        ! indices & counters
    integer :: k, kmax, km, ko, kout

    logical :: is_float, more

    is_done  = .false.
    is_float = .false.

    if( word(1:6) == "method" ) then 

        call get_word(line, word1)
        call check_fed_method(word1)

        if( fed % method == FED_US ) then
        !AB: define the center and strength for Umbrella Sampling 

            !extract the umbrella mid value for FED order parameter (reaction coordinate)
            call get_word(line, word1)
            fed % ums_mid = word_2_real(word1)

            !extract the umbrella force constant 
            call get_word(line, word1)
            fed % ums_frc = word_2_real(word1)

            !extract the number of steps to skip between the bias feedback updates
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % numupdskip = nint(word_2_real(word1))
                if( fed % numupdskip < 1 ) fed % numupdskip = 1000

            end if

        else if( fed % method == FED_EE ) then

            !extract the EE bias feedback weight
            call get_word(line, word1)
            fed % upd_val = word_2_real(word1)

            !extract the EE bias feedback tune-up scaling factor
            call get_word(line, word1)
            fed % upd_scl = word_2_real(word1)

            !extract the number of steps to skip between the bias feedback updates
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % numupdskip = nint(word_2_real(word1))
                if( fed % numupdskip < 1 ) fed % numupdskip = 1000

            end if

            !extract the number of bias smoothing iterations (currently implemented via 3-point running averages)
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % nitr_smooth = nint(word_2_real(word1))

            end if

            if( fed % nitr_smooth < 1 ) then

                fed % nitr_smooth = 0

                call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Number of bias smoothing iterations < 1 for method 'EE' - skipping...",0)

            else if( fed % nitr_smooth > 5 ) then

                call cry(uout,'', &
                     "ERROR: Number of bias smoothing iterations > 5 for method 'EE' is not allowed!!!",51)

            else

                fed % nbeg_smooth = 2
                fed % nend_smooth = fed % numstates-1

                !extract the starting bin id for bias smoothing 
                call get_word(line, word1)

                if( word1 == "" .or. word1(1:1) == "#" ) then

                    is_done = .true.
                    return

                else

                    fed % nbeg_smooth = nint(word_2_real(word1))
                    if( fed % nbeg_smooth < 2 ) fed % nbeg_smooth = 2

                end if

                is_float = .false.

                !extract the ending bin id for bias smoothing 
                call get_word(line, word1)

                !if( word1 == "" .or. word1(1:1) == "#" ) then
                if( indexnum(adjustL(word1),is_float) < 1) then

                    is_done = .true.
                    return

                else if( is_float ) then

                    fed % wgt_smooth = word_2_real(word1)
                    if( fed % wgt_smooth > 1.0_wp   ) fed % wgt_smooth = 1.0_wp
                    if( fed % wgt_smooth < 0.333_wp ) fed % wgt_smooth = THIRD

                    is_done = .true.
                    return

                else 

                    fed % nend_smooth = nint(word_2_real(word1))
                    if( fed % nend_smooth < fed % nbeg_smooth ) fed % nend_smooth = fed % nbeg_smooth

                end if

                is_float = .false.

                !extract the weight of the central bin in bias smoothing 
                call get_word(line, word1)

                !if( word1 == "" .or. word1(1:1) == "#" ) then
                if( indexnum(adjustL(word1),is_float) < 1 ) then

                    is_done = .true.
                    return

                else if( is_float ) then

                    fed % wgt_smooth = word_2_real(word1)
                    if( fed % wgt_smooth > 1.0_wp   ) fed % wgt_smooth = 1.0_wp
                    if( fed % wgt_smooth < 0.333_wp ) fed % wgt_smooth = THIRD

                end if

            end if

        else if( fed % method == FED_WL ) then

            !extract the WL bias droplet
            call get_word(line, word1)
            fed % upd_val = word_2_real(word1)

            !extract the EE bias feedback tune-up scaling factor
            call get_word(line, word1)
            fed % upd_scl = word_2_real(word1)

            !extract the number of steps to skip between the bias feedback updates
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % numupdskip = nint(word_2_real(word1))
                if( fed % numupdskip < 1 ) fed % numupdskip = 1000

            end if

            !extract the number of bias smoothing iterations (currently implemented via 3-point running averages)
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % nitr_smooth = nint(word_2_real(word1))

            end if

            if( fed % nitr_smooth < 1 ) then

                fed % nitr_smooth = 0

                call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Number of bias smoothing iterations < 1 for method 'WL' - skipping...",0)

            else if( fed % nitr_smooth > 5 ) then

                call cry(uout,'', &
                     "ERROR: Number of bias smoothing iterations > 5 for method 'WL' is not allowed!!!",51)

            else

                fed % nbeg_smooth = 2
                fed % nend_smooth = fed % numstates-1

                !extract the starting bin id for bias smoothing 
                call get_word(line, word1)

                if( word1 == "" .or. word1(1:1) == "#" ) then

                    is_done = .true.
                    return

                else

                    fed % nbeg_smooth = nint(word_2_real(word1))
                    if( fed % nbeg_smooth < 2 ) fed % nbeg_smooth = 2

                end if

                is_float = .false.

                !extract the ending bin id for bias smoothing 
                call get_word(line, word1)

                !if( word1 == "" .or. word1(1:1) == "#" ) then
                if( indexnum(adjustL(word1),is_float) < 1) then

                    is_done = .true.
                    return

                else if( is_float ) then

                    fed % wgt_smooth = word_2_real(word1)
                    if( fed % wgt_smooth > 1.0_wp   ) fed % wgt_smooth = 1.0_wp
                    if( fed % wgt_smooth < 0.333_wp ) fed % wgt_smooth = THIRD

                    is_done = .true.
                    return

                else 

                    fed % nend_smooth = nint(word_2_real(word1))
                    if( fed % nend_smooth < fed % nbeg_smooth ) fed % nend_smooth = fed % nbeg_smooth

                end if

                is_float = .false.

                !extract the weight of the central bin in bias smoothing 
                call get_word(line, word1)

                !if( word1 == "" .or. word1(1:1) == "#" ) then
                if( indexnum(adjustL(word1),is_float) < 1 ) then

                    is_done = .true.
                    return

                else if( is_float ) then

                    fed % wgt_smooth = word_2_real(word1)
                    if( fed % wgt_smooth > 1.0_wp   ) fed % wgt_smooth = 1.0_wp
                    if( fed % wgt_smooth < 0.333_wp ) fed % wgt_smooth = THIRD

                end if

            end if

        else if( fed % method == FED_TM ) then

            !TU: Extract the number of steps to skip between the bias feedback updates WITH output to
            !TU: FEDDAT.???_??? -- optional
            call get_word(line, word1)

            if( word1(1:3) == "tri" ) then

                fed % tm_tridiag = .true.
                
                call get_word(line, word1)
                
            end if
            
            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % numupdskip = nint(word_2_real(word1))
                if( fed % numupdskip < 1 ) fed % numupdskip = 1

            end if

            !TU: Extract the number of steps to skip between the bias feedback updates WITHOUT output to
            !TU: FEDDAT.???_???  -- optional
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else

                fed % numupdskipfine = nint(word_2_real(word1))
                if( fed % numupdskipfine < 1 ) fed % numupdskipfine = 1

            end if

            !TU: Extract the mode: "resume" imports the 'tm_matrix' from the file 'TMATRX', and
            !TU: initialises the bias function according to that matrix; "new" ignores the file
            !TU: 'TMATRIX' and initialises 'tm_matrix' to be 0 -- optional
            call get_word(line, word1)

            if( word1 == "" .or. word1(1:1) == "#" ) then

                is_done = .true.
                return

            else if( word1(1:3) == "res" ) then

                fed % tm_resume = .true.

            else if( word1(1:3) == "new" ) then

                fed % tm_resume = .false.

            else

                call cry(uout,'', &
                    "ERROR: Invalid 3rd argument '"//trim(word1)//"' to 'fed tm', which should be 'resume' or 'new'.",51)

            end if

            !TU: Add smoothing options here later...

            
        !else if( fed % method == FED_?? ) then
        !AB: anything for other methods etc - ???

        end if

    else if( word(1:5) == "order" ) then 

         !extract the order parameter type
         call get_word(line, word1)
         call check_fed_param(word1)

         !get the number of (sub)states for FED (grid points/bins)
         call get_word(line, word1)
         fed % numstates = nint(word_2_real(word1))

         if( fed%numstates < 2 ) then 

             fed%numstates = 1
             fed%is_on = .false.

             call cry(uout,'', &
                  "ERROR: FED specification with number of (sub)states < 2"// &
                  " - either amend or revoke FED specs!!!",51)

             return

         end if

         !get the min value of FED order parameter (reaction coordinate)
         call get_word(line, word1)
         fed % par_min = word_2_real(word1)

         !get the max value of FED order parameter (reaction coordinate)
         call get_word(line, word1)
         fed % par_max = word_2_real(word1)

         param = fed % par_min
         if( param > fed % par_max ) then 

             fed % par_min = fed % par_max
             fed % par_max = param

         end if

         fed % window_min = fed % par_min
         fed % window_max = fed % par_max

         !get the grid kind/type for FED order parameter (reaction coordinate)
         call get_word(line, word1)
         fed % inc_kind = nint(word_2_real(word1))

         !AB: the sign of inc_kind serves as a flag for reading the initial bias data from input (FEDDAT.000)
         if( fed % inc_kind < 0 ) then

             fed % is_bias_input  = .true.
             fed % inc_kind = abs(fed % inc_kind)

         end if

         if( fed % inc_kind > 3 ) then 

             call cry(uout,'', &
                  "ERROR: FED specification with order parameter grid type > 3"// &
                  " - either amend or revoke FED specs!!!",51)

         end if

         !AB: default to normal grid with linear growth
         if( fed % inc_kind < 1 ) fed % inc_kind = 1

         if( fed%par_kind == FED_PAR_LAMB ) then

             id_old = 1

             call get_word(line, word1)

             if( word1 /= "" .and. word1(1:1) /= "#" ) then

                 id_old = nint(word_2_real(word1))

                 if( id_old < 1 .or. id_old > fed%numstates ) &
                     call cry(uout,'', &
                          "ERROR: Initial FED order parameter index out of bounds"//&
                          " - either amend or revoke FED input!!!",51)

             end if

         end if


         !TU: Read some further optional parameters pertaining to the order parameter range: windows
         !TU: and 'soft edges'

         more = .true.

         do while( more )

             call get_word(line, word1)

             if( word1 == "" .or. word1(1:1) == "#" ) then

                 more = .false.

             else if( word1(1:3) == "win" ) then
    
                 fed % is_window = .true.
    
                 !TU: Extract the window lower bound
                 call get_word(line, word1)
                 fed % window_min = word_2_real(word1)
                 
                 !TU: Extract the window upper bound
                 call get_word(line, word1)
                 fed % window_max = word_2_real(word1)
    
                 if( fed % window_min > fed % window_max ) & 
                         call cry(uout,'', &
                              "ERROR: Order parameter window minimum greater than the window maximum"//&
                              " - either amend or revoke FED input!!!",51)
    
                 if( fed % window_min < fed % par_min ) & 
                         call cry(uout,'', &
                              "WARNING: Order parameter window minimum lower than the order parameter range minimum!",0)
    
                 if( fed % window_max > fed % par_max ) & 
                         call cry(uout,'', &
                              "WARNING: Order parameter window maximum greater than the order parameter range maximum!",0)
    
             else if ( word1(1:9) == "softedges" ) then

                 fed % soft_edges = .true.

             else

                 call cry(uout,'', &
                     "ERROR: Unrecognised argument '"//trim(word1)//"' to 'fed order' in FED specification",51)

             end if

         end do

         call read_order_extras(nread,is_done)

    else

         call cry(uout,'', &
                 "Uknown FED specification: '"//trim(word)//"' - stopping now!!!",51)

    end if

    is_done = .true.

end subroutine read_common_specs


!> reading specs for complex order parameters (requiring extra definitions)
subroutine read_order_extras(nread,is_done)

    !use constants_module  ! due to module dependence
    use psmc_module, only : read_psmc_extras

    integer, intent(in) :: nread

    logical, intent(inout) :: is_done

    !AB: read extra  the parameter value is fetched according to its kind
    select case ( fed % par_kind )

        case( FED_PAR_PSMC )

            call cry(uout,'(/,1x,a,/)', &
                 "The case of FED parameter 'PSMC' does not imply any extra input",0)

            !call read_psmc_extras(nread,is_done)

        case( FED_PAR_PSMC_HS )

            call cry(uout,'(/,1x,a,/)', &
                 "The case of FED parameter 'PSMC for hard spheres' does not imply any extra input",0)

            !call read_psmc_extras(nread,is_done)

        case( FED_PAR_TEMP )
            ! V-variation - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The case of FED parameter 'Temperature' does not imply any extra input",0)

        case( FED_PAR_BETA )
            ! V-variation - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The case of FED parameter 'Beta' does not imply any extra input",0)

        case( FED_PAR_LAMB )

            ! V-variation - must be known already

            !call cry(uout,'(/,1x,a,/)', &
            !     "The specs for FED parameter 'Lambda' do not imply any extra input",0)

            call read_order_groups(nread,'group',1,0,is_done)

            !call cry(uout,'', &
            !     "The case of FED parameter 'Lambda' is to be added yet...",51)

        case( FED_PAR_VOLM )
            ! V-variation - must be known already

            !call cry(uout,'', &
            !     "The case of FED parameter 'Volume' is to be added yet...",51)

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Volume' does not imply any extra input",0)

            !call read_order_volm(nread,is_done)

        case( FED_PAR_DENS )
            ! rho-variation - must be known already

            call cry(uout,'', &
                 "The case of FED parameter 'Density' is to be added yet...",51)

            !call cry(uout,'(/,1x,a,/)', &
                 !"The specs for FED parameter 'Density' do not imply any extra input",0)

            !call read_order_dens(nread,is_done)

        case( FED_PAR_DIST1 )
            ! R_i(COM) distance variation (w.r.t. a point or plane) - needs re-calculation

            call read_order_groups(nread,'com',1,0,is_done)

            call cry(uout,'', &
                 "The case of FED parameter 'COM1 distance' is to be added yet...",51)

            !call cry(uout,'(/,1x,a)', &
            !     "The case of FED parameter 'COM1 distance' - molecule/atom 1 & point/plane...",0)

            !call read_order_com1(input)

        case( FED_PAR_DIST2 )
            ! R_ij(COM) distance variation (between molecules/atoms) - needs re-calculation

            call cry(uout,'(/,1x,a,/)', &
                 "The case of FED parameter 'COM2 distance' - two sets of molecules or atoms",0)

            call read_order_groups(nread,'com',2,1,is_done)

        case( FED_PAR_ORDR1 )
            ! Structure-variation type 1 - needs re-calculation

            call cry(uout,'', &
                 "The case of FED parameter 'Structure/Order 1' is to be added yet...",51)

            !call read_order_struct1(nread,is_done)

        case( FED_PAR_ORDR2 )
            ! Structure-variation type 2 - needs re-calculation

            call cry(uout,'', &
                 "The case of FED parameter 'Structure/Order 2' is to be added yet...",51)

            !call read_order_struct2(nread,is_done)

        case( FED_PAR_NMOLS )
            ! Total number of molecules in the simulation box - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Total number of molecules' does not imply any extra input",0)

        case( FED_PAR_NMOLS_SPEC )
            ! Number of molecules of a given species

            call read_nmols_spec_extras(nread,is_done)

        case( FED_PAR_NATOMS )
            ! Total number of atoms in the simulation box - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Total number of atoms' does not imply any extra input",0)

        case( FED_PAR_NATOMS_SPEC )
            ! Number of atoms of a given species

            call read_natoms_spec_extras(nread,is_done)

        case( FED_PAR_LCS2 )
            ! Liquid crystal scalar, 2nd order Legendre polynomial - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Liquid crystal scalar, 2nd order' does not imply any extra input",0)

        case( FED_PAR_LCS4 )
            ! Liquid crystal scalar, 4th order Legendre polynomial - must be known already

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Liquid crystal scalar, 4th order' does not imply any extra input",0)

        case( FED_PAR_LAMBDA )
            ! Hamiltonian coupling parameter lambda

            call cry(uout,'(/,1x,a,/)', &
                 "The specs for FED parameter 'Lambda' does not imply any extra input",0)

        case default

            call cry(uout,'', &
                 "Uknown type of FED parameter - stopping now!!!",51)

    end select

end subroutine read_order_extras


!AB: Below are the implementations of input routines for reading extra order parameter specs, if any needed


!> reading specs for order parameter `VOLM` (type = 3)
subroutine read_some_extras(nread, done)

    use parse_module

        !> input file unit (normally for `CONTROL` file)
    integer, intent(in) :: nread

    logical, intent(inout) :: done

    !AB:  internal variables go below

        ! temporary keyword & value storage
    character :: line*100, word*40, word1*40

    character :: mssg*120, frmt*120

        ! counters 
    integer :: num_line !num_grp, num_ids, num_flg, istat

    logical :: more, safe, is_float

    done = .false.
    more = .true.
    safe = .true.

    num_line = 0

    get: do while( more )

        call get_line(safe,nread,line)

        if (.not.safe) then 

            !AB: deallocate arrays for storing molecule/atom indices participating in COM2 calculations
            !call deallocate_grp_ids()

            call error(51)

        end if

        call lower_case(line)

        !write(uout,*)"read_order_groups(): read line '",line,"'"

        call get_word(line, word)

        ! AB: due to trimming within `get_word(..)` any spaces around `word` are removed
        if( word == "" .or. word(1:1) == "#" ) cycle get

        !write(uout,*)"read_order_groups(): read word 1 '",word,"'"

        if( word(1:4) /= "some" ) then

            more = .false.

            !AB: deallocate arrays for storing molecule/atom indices participating in COM2 calculations
            !call deallocate_grp_ids()

            !call cry(uout,'', &
            !         "NOTE1: `com sampl[ing] corr[ection]` is expected once in `fed order ...` block ",0)

            !call cry(uout,'', &
            !         "NOTE2: `com mol ...` is expected twice in `fed order ...` block ",0)

            !call cry(uout,'', &
            !         "NOTE3: `fed order done` is expected to mark ending of `fed order ...` block",0)

            call cry(uout,'', &
                     "ERROR: too few `some ... ` directives in `fed extras ...` block!!!",51)

            return

        end if

        call get_word(line, word)

        ! ...

        num_line = num_line+1

        !AB: make sure at least two 'fed com ...' lines read in before 'fed com done'
        more = ( num_line < 1 )

    end do get ! done - while( more )

    done = .true.

end subroutine read_some_extras

!TU*: Do we need done?
!TU: Added by me...
!> Reads the atomic species which is the target for the 'number of atoms of a
!> given species' order parameter
subroutine read_natoms_spec_extras(nread, done)

    use parse_module
    use species_module

         !> Input file unit (normally for `CONTROL` file)
    integer, intent(in) :: nread

         !> Flag signifying procedure is complete at return
    logical, intent(inout) :: done

    character :: line*100, word*40, word1*40, errormsg*72

    logical :: safe, more

    integer :: k

    done = .false.
    more = .true. 
    safe = .true.

    do while( more )

        call get_line(safe,nread,line)

        if (.not.safe) then 

            !TU: We could add a more descriptive error message here
            call error(51)

        end if

        call get_word(line, word)

        if( word == "" .or. word(1:1) == "#" ) cycle


        !TU: Get the species name and type, and check they are sensible

        fed % par_natoms_spec_name = word

        call get_word(line, word)

        if(.not. valid_species_type(word)) then
            call cry(uout,'', &
                     "ERROR: Invalid atom type '"//trim(word)//"' in 'nspecatom' FED parameter definition",51)
        end if

        fed % par_natoms_spec_type =  get_species_type(word)

        !TU: Check the species name and type correspond to an atom given in FIELD

        safe = check_atom_defined(fed%par_natoms_spec_name, fed%par_natoms_spec_type)
        if( .not. safe ) then
            call cry(uout,'', &
                 "ERROR: Atom type '"//trim(fed%par_natoms_spec_name)//" "//set_species_type(fed%par_natoms_spec_type)// &
                 "' in 'natoms_spec' FED parameter definition not defined in FIELD", 51)
        end if

        !TU: Find the atom species number in FIELD which corresponds to the atom
        do k = 1, number_of_elements
        
            if( element(k) == fed%par_natoms_spec_name .and. eletype(k) == fed%par_natoms_spec_type ) then
               
               fed%par_natoms_spec_label = k
               exit

            end if
        
        end do


        more = .false.

    end do

    done = .true.

end subroutine read_natoms_spec_extras


!TU*: Do we need done?
!TU: Added by me...
!> Reads the molecular species which is the target for the 'number of molecules of a
!> given species' order parameter
subroutine read_nmols_spec_extras(nread, done)

    use parse_module
    use species_module

         !> Input file unit (normally for `CONTROL` file)
    integer, intent(in) :: nread

         !> Flag signifying procedure is complete at return
    logical, intent(inout) :: done

    character :: line*100, word*40

    logical :: safe, more

    integer :: k

    done = .false.
    more = .true. 
    safe = .true.

    do while( more )

        call get_line(safe,nread,line)

        if (.not.safe) then 

            !TU: We could add a more descriptive error message here
            call error(51)

        end if

        call get_word(line, word)

        if( word == "" .or. word(1:1) == "#" ) cycle

        !TU: Get the species name, cross-check it against the species defined in FIELD, and set
        !TU: the index of the species

        fed % par_nmols_spec_name = word

        !TU: '0' here signifies the species name has not been 'found' in those defined in FIELD yet
        fed % par_nmols_spec_label = 0

        do k = 1, number_of_molecules

            if (fed % par_nmols_spec_name == uniq_mol(k)%molname) then
            
                fed % par_nmols_spec_label = k
                exit

            end if

        end do

        if( fed % par_nmols_spec_label == 0) then

            call cry(uout,'', &
                 "ERROR: Molecule type '"//trim(fed%par_nmols_spec_name)// &
                 "' in 'nspecmol' FED parameter definition not defined in FIELD", 51)

        end if

        more = .false.

    end do

    done = .true.

end subroutine read_nmols_spec_extras



!> reading extra specs for order parameters based on grouping molecules/atoms
subroutine read_order_groups(nread, key, ngroups, nflags, done)

    !use constants_module  ! due to module dependence
    !use parallel_loop_module, only : master ! due to module dependence

    use parse_module

        !> input file unit (normally for `CONTROL` file)
    integer, intent(in) :: nread

        !> primary keyword for order parameter extras subsection
    character*(*), intent(in) :: key

        !> number of index groups and extra option lines to expect
    integer, intent(in) :: ngroups, nflags

    logical, intent(inout) :: done

    !AB:  internal variables go below

        ! temporary keyword & value storage
    character :: line*200, word*40, word1*40, word2*40

    character :: mssg*120, frmt*120

        ! counters of COM & molecule/atom entries (indices)
    integer :: lkey, num_grp, ids_num, num_flg, istat

    logical :: more

    logical :: safe, is_float

    lkey = len(key)

    word2 = ""

    !AB: allocate minimum arrays for storing molecule/atom indices participating in COM2 calculations
    !AB: one entry (molecule/atom) per each of the two COMs

    grp_nmols = 1
    grp_natms = 1
    ngrp_ids  = ngroups

    call reallocate_grp_ids(grp_nmols,ngrp_ids,grp_natms,ngrp_ids)

    grp_mol_ids = 0
    grp_atm_ids = 0

    num_grp = 0
    ids_num = 1
    num_flg = nflags

    coms_xyz(:) = 1.0_wp

    done = .false.
    more = .true.
    safe = .true.

    get: do while( more )

        call get_line(safe,nread,line)

        if (.not.safe) then 

            !AB: deallocate arrays for storing molecule/atom indices participating in COM2 calculations

            call deallocate_grp_ids()

            grp_nmols = 0
            grp_natms = 0
            ngrp_ids  = 0

            call error(51)

        end if

        call lower_case(line)

        !write(uout,*)"read_order_groups(): read line '",line,"'"

        call get_word(line, word)

        ! AB: due to trimming within `get_word(..)` any spaces around `word` are removed
        if( word == "" .or. word(1:1) == "#" ) cycle get

        !write(uout,*)"read_order_groups(): read word 1 '",word,"'"

        if( word(1:lkey) /= key ) then

            more = .false.

            !AB: deallocate arrays for storing molecule/atom indices participating in COM2 calculations

            call deallocate_grp_ids()

            grp_nmols = 0
            grp_natms = 0
            ngrp_ids  = 0

            call cry(uout,'', &
                     "ERROR: too few `"//key//" ... ` directive(s) in `fed order ...` block!!!",51)

            return

        end if

        call get_word(line, word1)

        !write(uout,*)"read_order_groups(): read word 2 '",word1,"'"
        !!write(uout,*)"read_order_groups(): read line ",num_grp+num_flg+1

        !if( (word1(1:5) == "sampl" .or. word1(1:4) == "corr") ) then
        if( word1(1:5) == "sampl" ) then

            if( num_flg < 1 .or. key /= 'com' ) then

                more = .false.

                call cry(uout,'', &
                     "ERROR: unexpected `com sampl[ing] corr[ection]` directive in `fed order ...` block!!!",51)

            end if

            num_flg = num_flg-1
            
            call get_word(line, word1)

            if( word1(1:4) == "corr") then

                call get_word(line, word1)

                fed%par_corr = int(word_2_real(word1))

                if( fed%par_corr == 0 ) then

                    word2 = "(no entropy correction applied)"

                else if( fed%par_corr == 1 ) then

                    word2 = "(entropy on spherical surface)"

                else if( fed%par_corr == 2 ) then

                    word2 = "(entropy in spherical volume element)"

                else

                    call cry(uout,'', &
                             "ERROR: unknown correction type in `com sampl[ing] corr[ection]` "//&
                             "directive in `fed order ...` block!!!",51)

                end if

            else if( word1(1:3) == "xyz") then

                call get_word(line, word1)
                coms_xyz(1) = word_2_real(word1)

                call get_word(line, word1)
                coms_xyz(2) = word_2_real(word1)

                call get_word(line, word1)
                coms_xyz(3) = word_2_real(word1)

                if( coms_xyz(1) /= 0.0_wp ) coms_xyz(1) = 1.0_wp
                if( coms_xyz(2) /= 0.0_wp ) coms_xyz(2) = 1.0_wp
                if( coms_xyz(3) /= 0.0_wp ) coms_xyz(3) = 1.0_wp

            else

                call cry(uout,'', &
                         "ERROR: unknown `com sampling "//trim(word1)//"` directive in `fed order ...` block!!!",51)

            end if

        else if( word1(1:3) == "mol" ) then

            !AB: attempt to allow for multiple-line definition of the same COM/group <<<-
            !AB: this has not been finalized properly yet (the last line overrides all)...
            !if( len(trim(word)) > lkey ) then
            !    ids_num = int(word_2_real(word(lkey+1:)))
            !end if

            !write(uout,*)"read_order_groups(): read word 11 '",trim(word(lkey+1:)),"' =?= ",num_grp+1,ids_num
            !flush(uout)

            !if( ids_num == num_grp+1 ) then 
            !    num_grp = num_grp+1
            !else if( ids_num /= num_grp ) then 
            !    call cry(uout,'', &
            !             "ERROR: "//key//":s are ill-ordered in `fed order ...` block!!!",51)
            !end if
            !AB: attempt to allow for multiple-line definition of the same COM/group ->>>

            !if( num_grp == ngrp_ids ) then
            !    more = .false.
            !    call cry(uout,'', &
            !         "ERROR: too many 'com mol ... [atoms ...]' directives in 'fed order ...' block!!!",51)
            !end if

            num_grp = num_grp+1

            call read_indices(line,grp_mol_ids,grp_nmols,num_grp) !+1)

            call get_word(line, word)

            if( word /= "" .and. word(1:1) /= "#" ) then

                if( word(1:4) == "atom" ) then

                    call read_indices(line,grp_atm_ids,grp_natms,num_grp) !+1)

                    call get_word(line, word)

                    safe = ( word == "" .or. word(1:1) == "#" )

                else 

                    more = .false.

                    write(word1,'(a,i1)') "COM ",num_grp !+1

                    call cry(uout,'',trim(word1)//": expected molecule index "//&
                         "or keyword `atom` but last word read `"//trim(word)//"`",0)

                    call cry(uout,'',"ERROR: incomplete `com mol ... [atoms ...]` "//&
                         "directive in `fed order ...` block!!!",51)

                    return

                end if

            end if

        else

            more = .false.

            !AB: deallocate arrays for storing molecule/atom indices participating in COM2 calculations

            call deallocate_grp_ids()

            grp_nmols = 0
            grp_natms = 0
            ngrp_ids  = 0

            call cry(uout,'', &
                     "NOTE1: two 'com mol ...' lines are expected in 'fed order ...' block",0)

            call cry(uout,'', &
                     "NOTE2: 'fed order done' is expected to mark ending of 'fed order ...' block",0)

            call cry(uout,'', &
                     "ERROR: erroneous 'com mol ... [atoms ...]' directive in 'fed order ...' block!!!",51)

            !write(uout,*)
            !write(uout,*)
            !write(uout,*)"ERROR: erroneous 'com mol ... [atoms ...]' directive in 'fed order ...' block!!!",num_grp+1
            !write(uout,*)"NOTE1: two 'com mol ...' lines are expected in 'fed order ...' block", &
            !             " due to order type '", trim(fed_name%parameter),"'"
            !write(uout,*)"NOTE2: 'fed order done' is expected to mark ending of 'fed order ...' block"

            !call error(51)

            return

        end if

        !AB: make sure at least two 'fed com ...' lines read in before 'fed com done'
        more = ( num_grp < ngrp_ids .or. num_flg > 0 )

    end do get ! done - while( more )

    done = .true.

    if( master ) then

        write(uout,*)
        write(uout,*)"------------------------------"
        write(uout,*)"The indices for the "//key//"(s) :"

        do num_grp=1,ngrp_ids
            write(uout,*)"------------------------------"
            write(uout,'(a,2(i3,a),/,1000i8)') &
                     " - molecule indices (",num_grp,",",grp_nmols,") :",grp_mol_ids(:,num_grp)
            write(uout,*)"------------------------------"
            write(uout,'(a,2(i3,a),/,1000i8)') &
                     " - atom indices     (",num_grp,",",grp_natms,") :",grp_atm_ids(:,num_grp)
            write(uout,*)"------------------------------"

            write(uout,*)
        end do

        write(uout,'(1x,a,3f3.0)')"The COM distance sampling directions : ",coms_xyz(:)

        if( key == 'com' ) &
            write(uout,'(1x,a,i1,2x,a)')"The COM distance sampling correction : ",fed%par_corr,word2

    !else
    !
    !    write(uout,'(1x,a,3f3.0)')"The COM distance sampling directions : ",coms_xyz(:)

    end if

    if( .not. safe ) then

        if( master ) then
            write(uout,*)
            write(uout,*)
            write(uout,*)"ERROR: junk found at the end of 'com mol ... [atoms ...]' directive!!! "
            write(uout,*)"NOTE1: if the indices provided are correct, simply comment out the rest of the line"
        end if

        call error(51)

        return

    end if

end subroutine read_order_groups


!> extracting molecule/atom indices for calculating COMs (for complex order parameters only)
subroutine read_indices(line, set_ids, max_ids, num_grp)

    !use constants_module  ! due to module dependence
    use parse_module

        !> line/record read in from `CONTROL` file (see control_module::read_control(job))
    character*(*), intent(inout) :: line

        !> earlier allocated 2D array containing either molecule or atom indices (`grp_mol_ids`/`grp_atm_ids`)
    integer, allocatable, intent(inout) :: set_ids(:,:)

        !> current maximum of indices in a COM row
    integer, intent(inout) :: max_ids

        !> current COM row to be read from input - `CONTROL` file
    integer, intent(in) :: num_grp

    ! internal variables go below

    character*40 :: word

    logical more, safe, is_float

    integer :: max_ids0, inc_ids0, inc_ids, num_ids !, icol, irow

    integer :: k, kc, kbot, ktop, id_bot, id_top, id_chk

    integer :: istat, id_sign

    integer, allocatable :: tmp_ids(:,:)

    !AB: input array must have been allocated externally as set_inc(1,2)
    !AB: so making sure max_ids >= 1
    if( max_ids < 1 ) max_ids = 1

    !AB: store away max_ids for the previously read in rows, if any
    max_ids0 = max_ids

    !AB: increment for max_ids when aforehand resizing set_ids array (when num_ids > max_ids)
    inc_ids0 = 5

    !AB: increment for num_ids due to read-in indices (>1 when a range encountered)
    inc_ids  = 1

    !AB: counter for indices successfully read in (for the current row)
    num_ids = 0

    !AB: bottom and top indices fpr a current range (or previously and currently read-in indices)
    id_bot = 0
    id_top = 0

    id_sign = 1

    more = .true.
    safe = .true.

    get: do while( more )

        call get_word(line,word)

            !AB: check if the word contains any numerics

            word = adjustL(word)

            is_float  = .false.

            if( indexnum(word,is_float) /= 1 ) then
            !AB: the first character found does not start a numeric

                if( num_ids < 1 ) then

                    call cry(uout,'', &
                             "ERROR: no index found in a range specification (at least one must be present)!!!",51)

                    !AB: for debugging only
                    !write(uout,*)
                    !write(uout,*)
                    !write(uout,*)"ERROR: no index found in 'com mol ... [atoms ...]' directive!!!", num_grp, num_ids
                    !write(uout,*)"NOTE1: 'com mol ...' assumes at least one molecule/atom index for COM calculation"

                    !call error(51)

                    return

                end if

                !AB: check for a range marker '..' and another numeric after it
                if( word(1:2) == ".." ) then 

                    if( indexnum(adjustL(line),is_float) == 1 ) then

                        id_sign = -1

                        cycle get

                    else

                        call cry(uout,'', &
                             "ERROR: no index found after a range marker '..' (the range end is undefined)!!!",51)

                        !call error(51)

                    end if

                else 
                !AB: provided at least one index have been read in, 
                !AB: leave analysis of the line remainder to the caller routine

                    more = .false.
                    line = trim(word)//' '//line

                end if

                if( num_ids > max_ids0 .and. num_ids < max_ids ) then

                    max_ids = num_ids

                    call resize_upto(set_ids,max_ids,2,istat)

                end if

                !AB: for debugging only
                !write(uout,*)"----------------------------"
                !write(uout,'(a,i1,a,/,10i10)') &
                !             "  - mol/atm indices  (",num_grp,") :",set_ids(:,num_grp) !,icol=1,max_ids)
                !write(uout,*)"----------------------------"

                return

            else if( is_float ) then

                call cry(uout,'', &
                         "ERROR: floating point figure found in a range specification (only integers are allowed)!!!",51)

                !AB: for debugging only
                !write(uout,*)
                !write(uout,*)
                !write(uout,*)"ERROR: floating point number found in 'com mol ... [atoms ...]' directive!!!", num_grp, num_ids
                !write(uout,*)"NOTE1: 'com mol ...' assumes at least one molecule/atom index for COM calculation"

                !call error(51)

                return

            end if

            !AB: safe to read the line further for indices

            id_bot = id_top
            id_top = int(word_2_real(word))

            num_ids = num_ids+1

            if( id_top < 1 ) then

                call cry(uout,'', &
                         "ERROR: number < 1 found in a range specification (only positive integers are allowed)!!!",51)

                !AB: for debugging only
                !write(uout,*)
                !write(uout,*)
                !write(uout,*)"ERROR: integer number < 1 found in 'com mol ... [atoms ...]' directive!!!", num_grp, num_ids
                !write(uout,*)"NOTE1: 'com mol ...' assumes at least one molecule/atom index for COM calculation"

                !call error(51)

                return

            end if

            if( num_ids > max_ids ) then

                max_ids = max_ids+inc_ids0

                call resize_upto(set_ids,max_ids,2,istat)

            end if

            set_ids(num_ids,num_grp) = id_top

            if( id_sign < 0 ) then
            !AB: previous index set the lower bound of range (occured before the range marker '..')
            !AB: cuurent index sets the upper bound of range (occured after  the range marker '..')

                id_sign = 1

                inc_ids = id_top-id_bot

                if( inc_ids > 1 ) then

                    kbot    = num_ids

                    num_ids = kbot+inc_ids-1

                    if( num_ids > max_ids ) then 

                        max_ids = num_ids

                        call resize_upto(set_ids,max_ids,2,istat)

                    end if

                    ktop   = num_ids

                    id_chk = id_bot

                    kc = kbot
                    do k=kbot,ktop

                        id_chk = id_chk+1

                        if( any( set_ids(1:kc-1,num_grp) == id_chk ) ) then

                            !AB: for debugging only
                            !write(uout,*)
                            !write(uout,'(a,i2,a,i2,a,i2,a,i6,a)') &
                            !"WARNING: repeptivie index found in the range of indices (",k,",",kc,";",num_grp,") ",id_chk," (2) ..."

                            call cry(uout,'', &
                                 "WARNING: skipping repetitive index in a range specification ...",0)

                            safe = (id_chk /= id_top)

                        else 

                            set_ids(kc,num_grp) = id_chk
                            kc = kc+1

                        end if

                    end do

                    num_ids = kc-1

                    if( kc == kbot ) then

                        call cry(uout,'', &
                                 "WARNING: skipping a number of repetitive indices in a range specification ...",0)

                        !AB: for debugging only
                        !write(uout,*)
                        !write(uout,'(a,2(i2,a))') &
                        !     "WARNING: skipped the entire range of repeptivie indices [",id_bot," .. ",id_top,"] (2) ..."

                    else if( safe .and. set_ids(num_ids,num_grp) /= id_top ) then

                        call cry(uout,'', &
                                 "ERROR: something went wrong with assigning a range of indices!!!",51)

                        !AB: for debugging only
                        !write(uout,*)
                        !write(uout,*)
                        !write(uout,*)"ERROR: something went wrong with assigning the range of indices (",kbot,num_ids,") ", &
                        !             id_bot," .. ",id_top," =?= ",set_ids(kbot-1,num_grp), &
                        !             set_ids(kbot,num_grp)," .. ",set_ids(num_ids,num_grp)

                        !call error(51)

                    end if

                else ! same as if( inc_ids <= 1 ) 

                    call cry(uout,'', &
                             "ERROR: ill-defined range of indices (the top must be > the bottom by at least 2)!!!",51)

                    !AB: for debugging only
                    !write(uout,*)
                    !write(uout,*)
                    !write(uout,*)"ERROR: ill-defined range of indices in 'com mol ... [atoms ...]' directive!!! ", & 
                    !             set_ids(num_ids-1,num_grp)," .. ",set_ids(num_ids,num_grp)
                    !write(uout,*)"NOTE1: the upper bound in a range must be greater than the lower bound by at least 2"

                    !call error(51)

                    return

                end if ! done - if( inc_ids > 1 ) 

            else if( any( set_ids(1:num_ids-1,num_grp) == id_top ) ) then

                !write(uout,*)
                !write(uout,'(a,i2,a,i2,a,i6,a)') &
                !     "WARNING: repeptivie index found in the range of indices (",num_ids,",",num_grp,") ",id_top," (1) ..."

                call cry(uout,'', &
                         "WARNING: skipping repetitive index in a range specification ...",0)

                num_ids = num_ids-1

            end if ! done - if( id_sign < 0 )

    end do get ! done - while( more )

end subroutine read_indices


!> read in the FED bias data
subroutine read_fed_bias()

    !use constants_module     ! due to module dependence
    !use parallel_loop_module ! due to module dependence

    use parse_module, only : int_2_char3, get_line, get_word, word_2_real !, strip_blanks, lower_case
    use comms_mpi_module, Only : master, is_parallel, gcheck, msg_bcast

    real(kind=wp) :: parm_in, bias_in

    character :: char_num1*3, record*80, word1*40, word2*40

    integer :: kinp, kerr

    logical :: safe, is_float

    safe = .true.

    call int_2_char3(idgrp, char_num1, safe)

    if( .not.safe ) then 

         if( master ) write(uout,*)
         if( master ) write(uout,*)"The node/workgroup ID is too large: ",idgrp," (>999) - skipping FED input!"

         return
    end if

    safe = .false.

    if( master ) then
        open( ufed, file='FEDDAT.'//trim(char_num1), status='old', err=999 )
        safe = .true.
        if( is_parallel ) call gcheck(safe)
    else
        safe = .true.
        if( is_parallel ) call gcheck(safe)
        if( .not.safe ) goto 999
    endif

    write(word1,'(i4)')fed%numstates

    call cry(uout,"(1x,a,/,1x,100('-'))", &
             "reading bias input file 'FEDDAT."//trim(char_num1)//&
             "' - two columns with expected number of rows (bins): "//trim(word1),0)

    kinp = 0
    kerr = 0

    do while( safe .and. kinp < fed%numstates )

       call get_line(safe, ufed, record)
       if (.not.safe) then 

           if( master .and. kinp < fed%numstates) &
               write(uout,"(1x,a,i4,a,/,1x,100('-'))") &
                     "WARNING: could only read FED bias input up to row (bin): ",kinp, &
                     " - padding the rest with zeros!"

           exit

       end if

       call get_word(record, word1)
       !call lower_case(word1)

       if( word1 == "" .or. word1(1:1) == "#" ) cycle

       call get_word(record, word2)
       !call lower_case(word2)

       !if( indexnum(word1,is_float)>0 .and. indexnum(word2,is_float)>0 ) then

           kinp = kinp+1

           parm_in = word_2_real(word1)
           bias_in = word_2_real(word2)

           if( fed%par_kind == FED_PAR_TEMP ) parm_in = 1.0_wp / ( parm_in * BOLTZMAN )

           if( abs( parm_in - parm_vec(kinp) ) > 1.0e-10_wp ) then

               kerr = kerr+1

             if( kerr < 4 ) then

                 if( kerr == 1 ) &
                     call cry(uout,"(1x,a,/,1x,100('-'))", &
                          "WARNING: FED parameter grid in bias input is inconsistent with CONTROL file!",0)

                 if( master ) write(uout,'(1x,a,i4,2(a,f20.10))') &
                                   "bin(", kinp, ") = ", parm_in, " =/= ",parm_vec(kinp)

             else if(kerr == 4) then

                 call cry(uout,"(1x,100('-'),/,1x,a,/,1x,100('-'))", &
                          "WARNING: more than 3 bins inconsistent - the bias input is likely a poor guess!",0)

             end if

           endif

           !parm_vec(kinp) = parm_in
           bias_vec(kinp) = -bias_in

       !end if

    end do

    if( master ) close(ufed)

    !if( (fed%numstates-kinp)>3 ) &
    if( kinp < 2 ) &
        call cry(uout,'', &
                 "ERROR: FED bias input file '"//"FEDDAT."//trim(char_num1)//&
                 "' contains insufficient data!!!",999)

    !write(uout,*)"read_fed_bias(1): read in bias input file 'FEDDAT.",trim(char_num1),"' - ",safe

    !flush(uout)

    if( is_parallel ) call msg_bcast(bias_vec,fed%numstates)

    return

999 if( is_parallel .and. master ) call gcheck(safe)

    call cry(uout,'', &
             "ERROR: failed reading FED bias input file '"//"FEDDAT."//trim(char_num1)//"'!!!",999)

    STOP

    !write(uout,*)"read_fed_bias(2): read in bias input file 'FEDDAT.",trim(char_num1),"' - ",safe

    !flush(uout)

    return

end subroutine read_fed_bias


!TU: Added by me. The file format read by this procedure corresponds to that output by 'fed_tm_matrix_output'
!TU: in 'fed_calculus_module'.
!> Read the 'tm_matrix' from the file 'TMATRX'. Note that there are two possible formats, 'tridiagonal'
!> and 'full'. Which is used depends on the value of 'fed%tm_tridiag'
subroutine read_fed_tm()

    use constants_module, only : uout, utm
    use comms_mpi_module, only : gcheck

    character :: word1*40

    logical :: safe

    integer :: i, j, err

    !TU: Say what we are doing...

    write(word1,'(i4)') fed%numstates

    if( fed%tm_tridiag ) then

         call cry(uout,"(1x,a,/,1x,100('-'))", &
             "reading TM matrix input file 'TMATRX' in TRIDIAGONAL format"// &
             " - expected number of rows: "//trim(word1),0)

    else

        call cry(uout,"(1x,a,/,1x,100('-'))", &
             "reading TM matrix input file 'TMATRX' in FULL format"// &
             " - expected number of rows and columns: "//trim(word1),0)

    end if


    !TU: Safety checks...

    !TU: For the full format, input from 'TMATRX' is only supported if fed%numstates <= 100000 (see below)
    if( (.not. fed%tm_tridiag) .and. (fed%numstates >= 100000) ) then

  !scp too many arguments
!        call cry(uout,'', &
!             "ERROR: The number of FED states is too large for reading TM matrix file 'TMMATRX': ", &
!             fed%numstates," (>100000)!",999)

        call cry(uout,'', &
             "ERROR: The number of FED states is too large for reading TM matrix file 'TMMATRX': ", &
             999)

        stop
        
    end if

    !TU: These 'safe' variables in conjunction with 'gcheck' ensures that all
    !TU: processes throw an error if one of the processes has an error (i.e., if
    !TU: safe==.false.). 'gcheck' is a logical AND over 'safe' for all processes
    !TU: and distributes the result to all processes.

    safe = .false.

    open(utm, file='TMATRX', status='old', iostat=err)
    if(err==0) safe = .true.

    call gcheck(safe)

    if( .not.safe ) then

        call cry(uout,'', &
             "ERROR: Failed to open FED TM matrix input file 'TMATRX'!!!",999)

    end if


    !TU: Actually read 'TMATRX' now...

    !TU: Note that I don't use 'parse_module' because the lines in 'TMATRX' could, and probably will be,
    !TU: very long, which would necessitate a very large buffer to store the line via the 'get_line'
    !TU: procedure. This leads to poor performance. Changing the format of 'TMATRX' and using the parser
    !TU: with a smaller line size still lead to poor performance - the parser is too slow for reading
    !TU: lots of data. 
    !TU: 
    !TU: Here is an acceptable solution. 'TMATRX' is a fixed format file: 2 comment lines, followed by the
    !TU: matrix in the expected format (as one would write it down). All processes read the file assuming
    !TU: this format to initialise 'tm_matrix'. Perhaps having master processes read the file and distribute
    !TU: the matrix to other processes would be better? This would necessiate faffing around with MPI
    !TU: procedures and writing new ones to treat the dimension(:,:) matrix 'tm_matrix'.
        
    safe = .false.

    !TU: Skip first two comment lines
    read(utm,*,iostat=err)
    !TU: If there is a problem skip reading the rest of the file and junp to line 1000...
    if(err/=0) goto 1000
    read(utm,*,iostat=err)
    if(err/=0) goto 1000
    
    !TU: Read the matrix
    do i=1, fed%numstates

        !TU: For tridiagonal-format there are only 4 elements for each FED state. The
        !TU: 1st element corresponds to transitions to the FED state below 'i'; the 2nd
        !TU: corresponds to transitions within 'i'; the 3rd corresponds to transitions
        !TU: to the state above 'i'; the 4th corresponds to transitions to all other states
        if( fed%tm_tridiag ) then

            read(utm,'(3e26.17)',iostat=err) ( tm_matrix(i,j), j=1,3 )
            if(err/=0) goto 1000
            
        else
        
            !TU: The 100000 is the maximum size which can be output using the following line
            !TU: Note that the format matches that in 'fed_tm_matrix_output'
            read(utm,'(100000e26.17)',iostat=err) ( tm_matrix(i,j), j=1,fed%numstates )
            if(err/=0) goto 1000

        end if
        
    end do
    
    close(utm)
   
    safe = .true.

1000 call gcheck(safe)
    
    if(.not. safe) call cry(uout,'', &
        "ERROR: Problem reading the content of FED TM matrix input file 'TMATRX'!!!",999)

end subroutine read_fed_tm



!> some other routine
!subroutine some_routine(params)
!
!end subroutine


end module fed_interface_module
