! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   J. Grant - Modifications to exclusion list                            *
! *   r.j.grant[@]bath.ac.uk                                                *
! ***************************************************************************

module nbrlist_module

    use kinds_f90
    
    implicit none


contains


!
!  allocates the nbr list arrays. num is total number of atoms, numb is the 
!  max bonded or nonbonded interactions allowed (if zero is passed max_nonb 
!  is set to num/2), if flag is true the excluded list is allocated memory.
!
!TU-: Allocates the neighbour list 'list%nbr_list' such that the 1st dimension is of
!TU-: size 'nn', the 2nd dimension is of size 'numn', and the third is of
!TU-: size 2. This corresponds to the molecule which the list is for containing
!TU-: up to 'nn' atoms within it, where each such atom can have up to 'numn'
!TU-: neighbours. 
!
!JG: The alloc, alloc_and_copy and deaclloc routines have been separated
! so that nbrlist arrays allocate nrlist_arrays and
! exclist arrays allocate the exclusion list arrays.
! This means that allocation of exclist is only done when a molecule has bonds,
! and the list might be necessary.  The 'flag' determining this is:
! cfgs(box)%mols(moleculeindex)%blist%npairs and the conditional is in the calling routine.
subroutine alloc_nbrlist_arrays(list, nn, numn)

    use nbrlist_type
    
    implicit none

    integer, intent (in) :: nn, numn
    type (nbrlist), intent(inout) :: list

    integer :: half, fail(3)

    fail = 0

    list%max_nonb = numn

    !allocate(list%exclist(nn, nn), stat = fail(1))
    !JG: The list should be set up so that the molecule and atom indices are consecutive
    ! i.e. the first index in the array
    ! this means reversing order from previous implementation
    ! and changing indices here and throughout
    ! allocate(list%nbr_list(nn, list%max_nonb, 2), stat = fail(2))
    
    if( allocated(list%nbr_list) ) deallocate(list%nbr_list, stat = fail(1))
    if( allocated(list%numnbrs) )  deallocate(list%numnbrs, stat = fail(1))

    !allocate(list%nbr_list(nn, list%max_nonb, 2), stat = fail(2))
    allocate(list%nbr_list(2, list%max_nonb, nn), stat = fail(2))
    allocate(list%numnbrs(nn), stat = fail(3))

    if (any(fail > 0)) call error(337) 

    list%numnbrs = 0
    list%nbr_list = 0
    !list%exclist = 0

end subroutine

!> allocates exclist array
subroutine alloc_exclist_array(list, nn)

    use nbrlist_type
    implicit none

    integer, intent (in) :: nn
    type (nbrlist), intent(inout) :: list

    integer :: fail(2)

    fail = 0

    if( allocated(list%exclist) ) deallocate(list%exclist, stat = fail(1))

    allocate(list%exclist(nn, nn), stat = fail(2))

    if( any(fail > 0) ) then

        write(*,*)
        write(*,*)"alloc_exclist_array() : failed with natom = ",nn
        
        call error(338)

    endif

    list%exclist = 0

end subroutine

!> destroys the arrays
subroutine dealloc_nbrlist_arrays(list)

    use nbrlist_type
    implicit none

    type (nbrlist), intent(inout) :: list

    integer :: fail(3)

    fail = 0

    list%max_nonb = 0

    !if (allocated(list%exclist)) deallocate (list%exclist, stat = fail(1))
    if (allocated(list%nbr_list)) deallocate (list%nbr_list, stat = fail(2))

    if (allocated(list%numnbrs)) deallocate (list%numnbrs, stat = fail(3))

    if (any(fail > 0)) call error(343)

end subroutine

!> destroys the arrays
subroutine dealloc_exclist_array(list)

    use nbrlist_type
    implicit none

    type (nbrlist), intent(inout) :: list

    integer :: fail(3)

    fail = 0

    list%max_nonb = 0

    if (allocated(list%exclist)) deallocate (list%exclist, stat = fail(1))

    if (any(fail > 0)) call error(343)

end subroutine

subroutine alloc_and_copy_nbrlist_arrays(list1, list2, nat)

    use nbrlist_type
    implicit none

    integer, intent(in) :: nat
    type (nbrlist), intent(inout) :: list1
    type (nbrlist), intent(in) :: list2

    integer :: fail(3)

    fail = 0

    list1%max_nonb = list2%max_nonb

    !assumes if the no of nbrs array is alocated then all nbrlist arrays are being used
    !else only the exclist only (ie job%usenblist = .false.
    if (allocated(list2%numnbrs)) then
    
        if( allocated(list1%nbr_list) ) deallocate(list1%nbr_list, stat = fail(1))
        if( allocated(list1%numnbrs) )  deallocate(list1%numnbrs, stat = fail(1))

        !allocate(list1%exclist(nat, nat), stat = fail(1))
        !allocate(list1%nbr_list(nat, list1%max_nonb, 2), stat = fail(2))
        allocate(list1%nbr_list(2, list1%max_nonb, nat), stat = fail(2))
        allocate(list1%numnbrs(nat), stat = fail(3))

        !list1%exclist = list2%exclist
        list1%nbr_list = list2%nbr_list
        list1%numnbrs = list2%numnbrs

    !else

        !allocate(list1%exclist(nat, nat), stat = fail(1))
        !list1%exclist = 0

    endif

    if (any(fail > 0)) call error(337)

end subroutine

!JG: The one 'bug' I was unable to solve completely satisfactorily during exclist changes lies in:
! alloc_and_copy_exclist_array subroutine.
! The work around is to check is the array is allocated and deallocate if necessary, before allocating
! The offending instance is when a molecule is being inserted/moved, the case (m1%atom==0) in the calling routine
! copy_molecule, which should be associated with inserting a molecule, the molecule exclist is set: 
! First when proposing the move and the alloc_and_copy is called.
! Why this wasn't an issue before the changes I know not. 
! But since the general pattern is to deallocate and allocate one more won't hurt, and tests pass.
subroutine alloc_and_copy_exclist_array(list1, list2, nat)

    use nbrlist_type
    
    implicit none

    integer, intent(in) :: nat
    type (nbrlist), intent(inout) :: list1
    type (nbrlist), intent(in) :: list2

    integer :: fail(3)

    fail = 0

    list1%max_nonb = list2%max_nonb

    !assumes only the exclist only (ie job%usenblist = .false.
    !JG work around requires to deallocate if already allocated.
    if (allocated(list1%exclist)) deallocate(list1%exclist, stat = fail(1))
    
    allocate(list1%exclist(nat, nat), stat = fail(2))
    
    if( allocated(list2%exclist) ) then
    
        list1%exclist = list2%exclist

    else
        list1%exclist = 0

    endif

    if( any(fail > 0) ) then

        write(*,*)
        write(*,*)"alloc_and_copy_exclist_array() : failed with natom = ",nat
        
        call error(338)

    endif

end subroutine

!> sets the neighbour list of an atom to zero
subroutine clear_atomnbrlist(list, i)

    use nbrlist_type
    implicit none

    integer, intent (in) :: i
    type (nbrlist), intent(inout) :: list

    integer :: j

    list%numnbrs(i) = 0

    do j = 1, list%max_nonb

        !list%exclist(i,j) = 0
        list%nbr_list(:,j,i) = 0

    enddo

end subroutine

!> sets the exclustion list of an atom to zero
subroutine clear_atomexclist(list, i)

    use nbrlist_type
    implicit none

    integer, intent (in) :: i
    type (nbrlist), intent(inout) :: list

    integer :: j

!    list%numnbrs(i) = 0

    do j = 1, list%max_nonb

        list%exclist(i,j) = 0

    enddo

end subroutine

subroutine copy_nbrlist(list1, list2)

    use nbrlist_type
    implicit none

    type (nbrlist), intent(inout) :: list1
    type (nbrlist), intent(in) :: list2

    integer :: j

    if (allocated(list2%numnbrs)) list1%numnbrs = list2%numnbrs
!    if (allocated(list2%exclist)) list1%exclist = list2%exclist
    if (allocated(list2%nbr_list)) list1%nbr_list = list2%nbr_list

end subroutine

subroutine copy_exclist(list1, list2)

    use nbrlist_type
    implicit none

    type (nbrlist), intent(inout) :: list1
    type (nbrlist), intent(in) :: list2

    integer :: j

    if (allocated(list2%exclist)) list1%exclist = list2%exclist

end subroutine

subroutine swap_atomnbrlist(list, i1, i2)

    use nbrlist_type
    implicit none

    integer, intent (in) :: i1, i2
    type (nbrlist), intent(inout) :: list

    integer :: j

    list%numnbrs(i1) = list%numnbrs(i2)

    do j = 1, list%max_nonb

!        list%exclist(i1,j) = list%exclist(i2,j)
        list%nbr_list(:,j,i1) = list%nbr_list(:,j,i2)

    enddo

end subroutine

subroutine swap_atomexclist(list, i1, i2)

    use nbrlist_type
    implicit none

    integer, intent (in) :: i1, i2
    type (nbrlist), intent(inout) :: list

    integer :: j

    list%numnbrs(i1) = list%numnbrs(i2)

    do j = 1, list%max_nonb

        list%exclist(i1,j) = list%exclist(i2,j)

    enddo

end subroutine




!> Checks if a specified atom is in another atom's neighbour list. If the
!> atom is in the list then .true. is returned by this function
logical function is_atom_in_list(list, i, jm, j)

    use nbrlist_type
    implicit none

        !> The neighbour list for the molecule containing the atom whose list
        !> we are interested in
    type (nbrlist), intent(inout) :: list

        !> The atom number in the molecule which `list` is for; the atom
        !> whose list we are interested in
    integer, intent(in) :: i

        !> The molecule index for the atom we to be checked
    integer, intent(in) :: jm

        !> Atom index within the molecule `jm` of the atom to be checked; the
        !> question is if atom `j` in molecule `jm` is in the neighbour list
        !> for atom `i`
    integer, intent(in) :: j

    integer :: nbr

    is_atom_in_list = .false.

    do nbr = 1, list%numnbrs(i)
  
        if( jm == list%nbr_list(1, nbr, i) .and. &
             j == list%nbr_list(2, nbr, i) ) then

            is_atom_in_list = .true. 
            return 
                        
        end if

    end do

end function is_atom_in_list




end module
