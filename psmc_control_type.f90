!***************************************************************************
!   Copyright (C) 2015 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Data type containing control parameters for phase-switch Monte Carlo (PSMC) calculations
!> @usage 
!> - @stdspecs
!> @using
!> - `kinds_f90`
!> @typecontaining phase-switch Monte Carlo control parameters

module psmc_control_type

    use kinds_f90

    implicit none


type psmc_control

        !> Frequency of phase-switch moves
    integer :: switch_freq = 0

        !> Phase which is initially active (1 or 2) 
    integer :: init_act = 1

        !> Frequency of output to PSMC data file
    integer :: data_freq = 1000
    
        !> Flag determining whether checks on the displacements will be performed
    logical :: melt_check = .false.

        !> Threshold displacement size (Angstroms) at which a displacement is considered to be 'too large',
        !> returning an error in the simulation.
    real(kind=wp) :: melt_thresh = 10
    
        !> Frequency of displacement checks
    integer :: melt_freq = 1000

        !> Hard-core radius for soft VdW potentials used in PSMC, where here 'soft VdW potentials' excludes VdW 
        !> potentials with 'hard parts', e.g. the hard-sphere potential, Yukawa potential, A-O potential. 
        !> In DL_MONTE, the VdW energy for two atoms interacting by such a potential is set to a 'very large'
        !> value if the particles are within the 'hard-core radius', as opposed to calculating the energy using
        !> the relevant formula for the VdW potential. This is to stop overflows in the energy. PSMC simulations
        !> require a different, smaller hard-core radius than would typically be used in non-PSMC simulations.
        !> This is the default for PSMC simulations. (Angstroms)
    real(kind=wp) :: hc_vdw = 0.5

        !> Additional bias to be applied between phases 1 and 2:  switch_bias is the additional bias applied when
        !> going from phase 1 to 2, i.e. it is the bias of phase 2 relative to 1. Note that the bias output to
        !> FEDDAT files will be for phase 1 only; one should add this constant to the phase 1 bias in FEDDAT to 
        !> get the phase 2 bias. By contrast all biases output in YAMLDATA and PSDATA will have switch_bias added
        !> appropriately: it is not added for phase 1 configurations, but is for phase 2 configurations. This is
        !> done to simplify data analysis. (Units of k_BT).
    real(kind=wp) :: switchbias = 0.0_wp

        !> Flag which determines whether Cartesian or fractional displacements of particles from their sites
        !> are preserved in a switch. The default is fractional displacements. This flag, if true, enforces Cartesian
        !> displacements. Note that Cartesian displacements are not yet supported in all situations: only for
        !> constant volume simulations.
   logical :: cart_disp_switch = .false.

        !> Flag which ignores volume changes (but not scaling of particle displacements) during the switch in
        !> evaluating the acceptance probability for a switch move. In this case, the energy, not enthalpy, is
        !> the salient quantity for the acceptance probability, and the difference in probability fluxes associated
        !> with moving from, say, the volume of box 1 to that of box 2, is ignored. This is useful if phase 1 is a bulk and
        !> phase 2 is a slab with a large vacuum region. In this case the volume increase upon switching from phase
        !> 1 to phase 2 is spurious, and should not be considered in the acceptance probability if one is interested
        !> in the free energy difference between a slab and surface. 
   logical :: ignore_vol_scaling = .false.

end type psmc_control


end module psmc_control_type
