!***************************************************************************
!   Copyright (C) 2021 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Data type corresponding to a cell list
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @typecorresponding to a cell list

module cell_list_type

    use kinds_f90

    implicit none



type cell_list

        !> Minimal size of a cell in the x, y or z dimension
    real(wp) :: rmin

        !> Maximum number of atoms allowed in a cell
    integer :: maxpercell

        !> Simulation box x dimension
    real(wp) :: Lx

        !> Simulation box y dimension
    real(wp) :: Ly

        !> Simulation box z dimension
    real(wp) :: Lz

        !> Number of cells in x dimension
    integer :: Nx

        !> Number of cells in y dimension
    integer :: Ny

        !> Number of cells in z dimension
    integer :: Nz

        !> Number of cells
    integer :: Ncells

        !> Cell size in x dimension
    real(wp) :: dx

        !> Cell size in y dimension
    real(wp) :: dy

        !> Cell size in z dimension
    real(wp) :: dz

        !> The number of atoms in each cell: natoms(c) is the
        !> number of atoms in cell 'c'
    integer, allocatable :: natoms(:)

        !> The cell list itself: list(:,n,c) is the nth member of
        !> cell 'c'; list(1,n,c) is the molecule number for that
        !> member, and list(2,n,c) is the atom number within the
        !> molecule
    integer, allocatable :: list(:,:,:)

        !> Stores the cells which are in the neighbours of a given cell:
        !> nbrlist(n,c) is the nth cell in the neighbour list of 'c',
        !> while nbrlist(0,c) stores the number of cells in the neighbour
        !> list of cell 'c'.
    integer, allocatable :: nbrlist(:,:)

        !> The maximum number of neighbours a cell can have in its
        !> neighbour list
    integer :: maxneighbours

end type cell_list



end module cell_list_type
