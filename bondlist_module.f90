! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Molecule bond list storage and manipulation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `bondlist_type` (within routines)

!> @modulefor molecule bond list(s)

module bondlist_module

    use kinds_f90

    implicit none

contains

!> @brief 
!> - allocates array for a bond list
!> @using 
!> - `bondlist_type`
subroutine alloc_bondlist_arrays(list, n)

    use bondlist_type

    implicit none

        !> array to store bond list
    type(bondlist), intent(inout) :: list

        !> number of bonds in the list
    integer , intent(in) :: n

    integer :: fail

    fail = 0

    list%npairs = n

    allocate (list%bpair(n,3), stat = fail)

    if (fail > 0) call error(341)    

end subroutine

!> @brief 
!> - initialises bond elements in an existing (pre-allocated) bond list array
!> @using 
!> - `bondlist_type`
subroutine add_bondpairs(list, atomi, atomj, pot, n)

    use bondlist_type

    implicit none

        !> array to store bond list
    type(bondlist), intent(inout) :: list

        !> number of bonds in the list
    integer , intent(in) :: n

        !> arrays containing atoms (indices/pointers?) of bond pairs
    integer , intent(in) :: atomi(n), atomj(n)

        !> array containing potentials (types?)
    integer , intent(in) :: pot(n)

    integer :: i

    do i = 1, list%npairs

        list%bpair(i,1) = atomi(i)
        list%bpair(i,2) = atomj(i)
        list%bpair(i,3) = pot(i)

    enddo

end subroutine

!> @brief 
!> - (re)allocates a new bond list array '`nlist`' and copies into it all elements of '`olist`'
!> @using 
!> - `bondlist_type`
subroutine alloc_and_copy_bondlist_arrays(nlist, olist)

    use bondlist_type

    implicit none

        !> old bond list
    type(bondlist), intent(in) :: olist

        !> new bond list
    type(bondlist), intent(inout) :: nlist

    integer :: i, fail

    if (olist%npairs == 0) return

    fail = 0
    if(allocated(nlist%bpair)) deallocate (nlist%bpair, stat = fail)

    if (fail > 0) call error(342)

    nlist%npairs = olist%npairs

    fail = 0
    allocate (nlist%bpair(nlist%npairs,3), stat = fail)

    if (fail > 0) call error(341)

    do i = 1, nlist%npairs

        nlist%bpair(i,1) = olist%bpair(i,1)
        nlist%bpair(i,2) = olist%bpair(i,2)
        nlist%bpair(i,3) = olist%bpair(i,3)

    enddo

end subroutine

!> @brief 
!> - copies all elements of an existing bond list '`olist`' into another existing bond list '`nlist`'
!> @using 
!> - `anglist_type`
subroutine copy_bondlist(nlist, olist)

    use bondlist_type

    implicit none

        !> old bond list
    type(bondlist), intent(in) :: olist

        !> new bond list
    type(bondlist), intent(inout) :: nlist

    integer :: i

    if (olist%npairs == 0) return

    nlist%npairs = olist%npairs

    do i = 1, nlist%npairs

        nlist%bpair(i,1) = olist%bpair(i,1)
        nlist%bpair(i,2) = olist%bpair(i,2)
        nlist%bpair(i,3) = olist%bpair(i,3)

    enddo

end subroutine

!> @brief 
!> - deallocates (destroys) a bond list array
!> @using 
!> - `bondlist_type`
subroutine dealloc_bondlist_arrays(list)

    use bondlist_type

    implicit none

        !> the bond list to be destroyed
    type(bondlist), intent(inout) :: list

    integer :: fail

    fail = 0

    if (allocated(list%bpair)) deallocate(list%bpair, stat = fail)

    if (fail > 0) call error(342)

    list%npairs = 0

end subroutine

!> @brief 
!> - prints out a bond list into file unit '`uout`'
!> @using 
!> - `bondlist_type`
!> - `constants_module, only : uout`
!> - `comms_mpi_module, only : idnode`
subroutine print_bondlist(list)

    use constants_module, only : uout
    use bondlist_type
    use comms_mpi_module, only : idnode

    implicit none

        !> the bond list to be printed out into file unit '`uout`'
    type(bondlist), intent(inout) :: list

    integer :: i

    if (idnode /= 0) return

    do i = 1, list%npairs

        write(uout,"(/,1x,'bond between atom ',i4,' and ',i4,' with potential ',i4)")list%bpair(i,:)

    enddo

end subroutine

end module
