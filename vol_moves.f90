! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

module vol_moves

    use kinds_f90

    implicit none

    integer :: unpt=21

        !> total number of volume changes
    integer :: totalvolchanges

        !> no of successful volume changes
    integer :: successfulvolchange

        !> max no of V-bins on P(V) grid
    integer :: max_vol_bin

        !> max volume on P(V) grid
    real (kind=wp) :: vol_max

        !> no of attempted volume changes
    integer, dimension(:,:), allocatable :: attemptedvolchange

        !> no of volume changes during an iteration cycle
    real (kind=wp), dimension(:,:), allocatable :: novolchange

        !> maximum volume change allowed
    real (kind=wp), dimension(:,:), allocatable :: maxvolchange

        !> volume distribution histogram(s) - to do!
    real (kind=wp), dimension(:,:), allocatable :: volhist

        !> attempted gibbs vol moves for box1
    integer :: attemptedgibbs_vol_box1

        !> attempted gibbs vol moves for box2
    integer :: attemptedgibbs_vol_box2

        !> successful vol changes for box1
    integer :: sucessfulgibbs_vol_box1

        !> successful vol changes for box2
    integer :: sucessfulgibbs_vol_box2

contains


!***********************************************************************
!> Monte Carlo cubic volume or cell shape changes via a combination of /n
!> Allen & Tildesly (prog12) and Frenkel & Smit (algorithm 11) - corrected for variable sampling
!***********************************************************************
!TU:
!> Note that in PSMC the passive box (the box other than 'ib', e.g. box 1 if ib=2) is also updated
!> by this procedure so that it is the 'conjugate' of the active box.
!> Note that if this procedure is called when the Gibbs ensemble is in use then the volume is also
!> altered in the 'other' box (i.e. box 1 if ib=2, box 2 if ib=1). I will refer to the 'other' box
!> as the 'passive' box (similarly to PSMC) since the passive box is changed deterministically in
!> response to a stochastic change to the active box as in PSMC. Note that the '_pas' variables
!> pertain to the passive box
subroutine move_volume(ib, job, beta, betainv, extpress, energytot, enthalpytot, virialtot, energyrcp, energyreal, &
                      energyvdw, energythree, energypair, energyang, energyfour, energymany, energyext, energymfa, &
                      emoltot, emolrealnonb, emolrealbond, emolrealself, emolrealcrct, emolrcp, emolvdwn, emolvdwb, &
                      emolthree, emolpair, emolang, emolfour, emolmany, emolext, volume)

    use kinds_f90
    use species_module
    use control_type
    use cell_module
    use field_module
    use latticevectors_module
    use constants_module, only : THIRD, NPT_VEC, NPT_LOG, NPT_INV2, NPT_INV, NPT_LIN, NPT_LIN2, NPT_VOL, &
                                 FED_PAR_VOLM, FED_PAR_PSMC, FED_PAR_PSMC_HS, FED_TM
    use mc_moves, only : distance_atm_max, distance_mol_max
    use metpot_module, only : nmetpot
    use random_module, only : duni
    use slit_module, only : in_slit, slit_zfrac1, slit_zfrac2

    use fed_calculus_module, only : fed, fed_tm_inc
    use fed_order_module, only : fed_param_dbias, fed_param_dbias_psmc, fed_param_hist_inc, fed_window_reject

    use psmc_module, only :  set_cfg_sites, set_cfg_u_from_pos, set_passive_cfg
    use cell_list_wrapper_module, only : tear_down_cell_list, set_up_cell_list
    use thbpotential_module, only : numthb

    integer, intent(in) :: ib
    type(control), intent(in) :: job

    real(kind = wp), intent(in) :: beta, betainv, extpress
    real(kind = wp), intent(inout) :: energytot(:), enthalpytot(:), virialtot(:), energyrcp(:), energyfour(:), &
                     energyreal(:), energyvdw(:), energythree(:), energypair(:), energyang(:), energymany(:), &
                     energyext(:), energymfa(:), volume(:)

    real(kind = wp), intent(inout) :: emoltot(nconfigs,number_of_molecules), emolrealnonb(nconfigs,number_of_molecules), &
                     emolrealbond(nconfigs,number_of_molecules), emolrcp(nconfigs,number_of_molecules), &
                     emolvdwn(nconfigs,number_of_molecules), emolvdwb(nconfigs,number_of_molecules), &
                     emolthree(nconfigs,number_of_molecules), emolpair(nconfigs,number_of_molecules), &
                     emolang(nconfigs,number_of_molecules), emolfour(nconfigs,number_of_molecules), &
                     emolmany(nconfigs,number_of_molecules), emolrealself(nconfigs,number_of_molecules), &
                     emolrealcrct(nconfigs,number_of_molecules), emolext(nconfigs,number_of_molecules)

    integer :: indx, im

    real(kind=wp)   :: arg, dimscale, dln_dim, deltav, deltavb, deltavb_can, ntot, newreal, newrcp, newvdw, newthree, &
                       newpair, newext, newmfa, newang, newfour, newmany, newtotal, newenth, newvir, volold, volnew, &
                       oldtotal, oldenth, delta_vol !, scale

    !temp vectors for molecular energies
    real(kind = wp) :: nrealnonb(number_of_molecules), nrealbond(number_of_molecules), nrcp(number_of_molecules), &
                       nvdwn(number_of_molecules), nvdwb(number_of_molecules), npair(number_of_molecules), &
                       nthree(number_of_molecules), nang(number_of_molecules), nfour(number_of_molecules), &
                       nmany(number_of_molecules), nself(number_of_molecules), ncrct(number_of_molecules), &
                       next(number_of_molecules)

    !TU: Relevant variables for the passive box (variables ending with '_pas') - for PSMC and Gibbs
    integer :: ib_pas
    real(kind=wp)   :: ntot_pas, newreal_pas, newrcp_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, &
                       newfour_pas, newmany_pas, newext_pas, newmfa_pas, newtotal_pas, newenth_pas, newvir_pas, &
                       volold_pas, volnew_pas, oldtotal_pas, oldenth_pas!, scale_pas

    real(kind = wp) :: nrealnonb_pas(number_of_molecules), nrealbond_pas(number_of_molecules), &
                       nrcp_pas(number_of_molecules), nvdwn_pas(number_of_molecules), &
                       nvdwb_pas(number_of_molecules), npair_pas(number_of_molecules), &
                       nthree_pas(number_of_molecules), nang_pas(number_of_molecules), &
                       nfour_pas(number_of_molecules), nmany_pas(number_of_molecules), &
                       nself_pas(number_of_molecules), ncrct_pas(number_of_molecules), &
                       next_pas(number_of_molecules)

    real(kind=wp) :: dimscale_pas


    logical :: is_psmc, is_gibbs, accept, is_fed_vol, out_of_range, is_fed_param_psmc, reset, reset_pas

    !TU: Flag determining whether an atom in the passive box is outside the slit - if the slit is in use.
    !TU: This is just a placeholder here: I never check it since volume moves shouldn't be used in a slit.
    logical :: atom_out_pas = .false.

    reset = .false.
    reset_pas = .false.

    !TU: Flag specifying whether PSMC is in operation; specifically, that we have 2 boxes
    !TU: which are evolved simultaneously such that they are always conjugate to each other
    is_psmc = ( fed % flavor == FED_PS )

    !TU: Flag specifying whether FED is used with a PSMC order parameter -
    !TU: for now this is always the case if 'is_psmc=.true.'
    is_fed_param_psmc = ( fed%par_kind == FED_PAR_PSMC .or. fed%par_kind == FED_PAR_PSMC_HS )

    !TU: Flag specifying whether this is a Gibbs ensemble volume move where the passive
    !TU: box's volume is amended in response to the active boxes volume change - to preserve
    !TU: their total volume
    is_gibbs = ( job%type_vol_move == NPT_GIBBS )

    newenth  = 0.0_wp
    newvir   = 0.0_wp
    newreal  = 0.0_wp
    newrcp   = 0.0_wp
    newvdw   = 0.0_wp
    newvdw   = 0.0_wp
    newthree = 0.0_wp
    newpair  = 0.0_wp
    newang   = 0.0_wp
    newfour  = 0.0_wp
    newmany  = 0.0_wp
    newext   = 0.0_wp
    newmfa   = 0.0_wp

    volold = cfgs(ib)%vec%volume
    call storelatticevectors(cfgs(ib)%vec)
    call store_all_pos(ib)

    ! the initial total energy is known!
    oldtotal = energytot(ib)
    oldenth  = oldtotal + extpress * volold

    !TU: For PSMC and Gibbs deduce the index of the passive box
    if( is_psmc .or. is_gibbs ) then
       
       select case(ib)
       case(1)

           ib_pas = 2

       case(2)

           ib_pas = 1

       case default

!scp: did not have error msg no.
           !TU: Something has gone horribly wrong...
           call cry(uout,'', &
               "ERROR: Box index 'ib' passed to 'move_volume' is not 1 or 2!",99)
       end select

    end if

    dln_dim = 0.0_wp


    !TU: AB's comment below is out of date. See constants_module.f90 for the various
    !TU: supported volume move types
    
    !AB: five different NpT sampling schemes are implemented:
    !AB: NPT_VEC =-3 ! general case - sampling cell vectors, not allowed in slit
    !AB: NPT_LOG =-2 ! ortho/cubic cell - sampling ln(V), same in slit
    !AB: NPT_INV =-1 ! cubic cell - sampling inverse linear dimension(s), same in slit
    !AB: NPT_LIN = 1 ! cubic cell - sampling linear dimension(s)
    !AB: NPT_LIN2= 2 ! cubic cell - sampling linear dimension(s), specific in slit
    !AB: NPT_VOL = 3 ! ortho/cubic cell - sampling V, same in slit
    !AB: - see constants_module.f90 and lattice_module.f90::expandcell()

    if(job%type_vol_move == NPT_VEC) then

        indx = int (duni() * 6 + 1)

        call distort_cell(ib, indx, maxvolchange(indx,ib), volnew)

    else if( job%type_vol_move == NPT_ORTHOANI ) then

        !TU: Code for anisotropic orthorhombic cell moves
        !TU: These sample ln(V)

        !TU: Choose a dimension, x, y or z (i.e. 1, 2 or 3) to expand/contract
        !TU: in the cell
        indx = int( 3.0_wp * duni() ) + 1

        call expand_cell_orthoani(ib, indx, maxvolchange(indx,ib), volnew, dimscale)

        !AB: apply the correction for sampling ln(V)
        dln_dim = 1.0_wp

        
    else if( job%type_vol_move == NPT_VOL .or. is_gibbs ) then

        !TU: Code for orthorhombic cell moves which preserve cell shape

        !AB: V-moves which sample V - only for orthorhombic or cubic cells (including slit)

        indx = 1

        call expand_cell_ortho(ib, job%type_vol_move, maxvolchange(1,ib), volnew, dimscale)

        !AB: no correction for sampling in V

    else if( job%type_vol_move == NPT_LOG ) then

        !TU: Code for orthorhombic cell moves which preserve cell shape
        !AB: V-moves which sample ln(V) - only for orthorhombic or cubic cells (including slit)

        indx = 1

        call expand_cell_ortho(ib, job%type_vol_move, maxvolchange(1,ib), volnew, dimscale)

        !AB: apply the correction for sampling ln(V)
        dln_dim = 1.0_wp

        
    else if( job%type_vol_move == NPT_INV2 ) then

        !AB: V-moves which sample 1/L_1 (& 1/L_2) - only for orthorhombic cells (including slit)

        indx = 1

        call expand_cell_ortho(ib, job%type_vol_move, maxvolchange(1,ib), volnew, dimscale)

        !AB: apply the correction for sampling 1/L_1 (& 1/L_2)
        !dln_dim = 1.0_wp - real(job%type_vol_move,wp) * HALF
        dln_dim = 3.0_wp * HALF

    else if( job%type_vol_move == NPT_LIN2 ) then

        !AB: V-moves which sample L_1 (& L_2) - only for orthorhombic cells (including slit)

        indx = 1

        call expand_cell_ortho(ib, job%type_vol_move, maxvolchange(1,ib), volnew, dimscale)

        !AB: apply the correction for sampling L_1 (& L_2)
        !dln_dim = 1.0_wp - real(job%type_vol_move,wp) * HALF
        dln_dim = HALF

    else

        !TU: Code for cubic cell moves

        indx = 1

        call expand_cell(ib, job%type_vol_move, maxvolchange(1,ib), volnew, dimscale)

        !AB: correction terms arrise if volume variations are achieved 
        !AB: by uniform sampling of other variables (L or 1/L for V=L^3)

        dln_dim = 1.0_wp - real(job%type_vol_move,wp) * THIRD

        !dln_dim = 1.0_wp
        !if( job%type_vol_move /= NPT_LOG ) dln_dim = dln_dim - real(job%type_vol_move,wp) * THIRD

    endif

    !TU: Set cells for all atoms in active box from scratch, after destroying the cell list above
    if(job%clist) then

        call tear_down_cell_list(ib)
        call set_up_cell_list(job, ib)

    end if

    !TU: In PSMC and Gibbs store the old energies and configuration
    if( is_psmc .or. is_gibbs ) then
       
        newenth_pas = 0.0_wp
        newvir_pas = 0.0_wp
        newreal_pas = 0.0_wp
        newrcp_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newvdw_pas = 0.0_wp
        newthree_pas = 0.0_wp
        newpair_pas = 0.0_wp
        newang_pas = 0.0_wp
        newfour_pas = 0.0_wp
        newmany_pas = 0.0_wp
        newext_pas = 0.0_wp
        newmfa_pas = 0.0_wp

        volold_pas = cfgs(ib_pas)%vec%volume
        call storelatticevectors(cfgs(ib_pas)%vec)
        call store_all_pos(ib_pas)

        ! the initial total energy is known!
        oldtotal_pas = energytot(ib_pas)
        oldenth_pas = oldtotal_pas + extpress * volold_pas

    end if

    !TU: In PSMC update the sites and displacements in the active box, 
    !TU: and synchronise the geometry of the passive box to the active box
    !TU: In Gibbs generate the trial configuration of the passive box.
    if( is_psmc ) then

        !TU: Update the sites and displacements in the active box
        call set_cfg_sites(ib, ib)
        call set_cfg_u_from_pos(ib)

        !TU: Set the passive box geometry to be the conjugate of the active box.
        call set_passive_cfg(ib, ib_pas, ib, atom_out_pas)

        !TU: Set cells for all atoms in the passive box from scratch - after tearing down the current cell list
        if(job%clist) then

            call tear_down_cell_list(ib_pas)
            call set_up_cell_list(job, ib_pas)

        end if
   
        volnew_pas = cfgs(ib_pas)%vec%volume

    else if( is_gibbs ) then

        if( volold_pas+(volnew-volold) <= 0.0_wp ) then

            call cry(uout, '(/,1x,a,/)', &
                 "ERROR: attempting volume change in passive box which yields negative volume!",999)

        end if

        !TU: Calculate the scaling factor for each dimension in passive box so that
        !TU: the change in volume is and opposite to that in the active box
        if( in_slit ) then

            dimscale_pas = sqrt( 1.0_wp - (volnew - volold)/volold_pas )

        else

            dimscale_pas = ( 1.0_wp - (volnew - volold)/volold_pas )**(1.0_wp/3.0_wp)

        end if
 
        !TU: This procedure alters the dimensions of 'ib_pas' by a SPECIFIED amount, 'dimescale_pas'
        !TU: Note that for slit systems only the x and y dimensions are altered
        call expand_cell_ortho_by(ib_pas, volnew_pas, dimscale_pas)

        !TU: Set cells for all atoms in passive box from scratch - after tearing down the current cell list
        if(job%clist) then

            call tear_down_cell_list(ib_pas)
            call set_up_cell_list(job, ib_pas)

        end if

        volnew_pas = cfgs(ib_pas)%vec%volume

    end if

    attemptedvolchange(indx,ib) = attemptedvolchange(indx,ib) + 1
    totalvolchanges = totalvolchanges + 1

    !AB: advance random number selection to preserve the random sequence
    arg = duni()

    deltavb = 0.0_wp

    is_fed_vol   = ( fed%par_kind == FED_PAR_VOLM )
    out_of_range = .false.

    if( is_fed_vol ) then

        deltavb = fed_param_dbias(ib,cfgs(ib))

        !AB: stay within the defined range
        !if( deltavb > job%toler ) goto 1000

        out_of_range = ( deltavb > job%toler )

        if( out_of_range ) goto 1000

    end if

    ! determine new phase factors for ewald sum
    !if( job%coultype(ib ) /= 0) then

    if( job%coultype(ib) == 1 ) call setup_ewald(ib, job)

    !endif

    !TU: Do the same as above for the passive box in PSMC
    if( is_psmc ) then

        if( job%coultype(ib_pas) == 1 ) call setup_ewald(ib_pas, job)

    end if

    !TU*: Need to update 3-body lists for the trial configuration
    if(numthb > 0 ) then

       call set_thblist(ib)

       if( is_psmc ) call set_thblist(ib_pas)

    end if

    !get new energy
    if(job%uselist) then
    
        !check NL if in auto-update mode
        if( job%lauto_nbrs ) then

            !call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist, .false.)
            call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset, job%clist)

            if( is_psmc ) &
                call update_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, reset_pas, job%clist)

        endif

        call total_energy(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb, &
                          npair, nthree, nang, nfour, nmany, next)

        !TU: Do the same as above for the passive box in PSMC and Gibbs
        if( is_psmc .or. is_gibbs ) &
            call total_energy(ib_pas, job%coultype(ib_pas), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, newmany_pas, &
                          newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, nself_pas, ncrct_pas, &
                          nvdwn_pas, nvdwb_pas, npair_pas, nthree_pas, nang_pas, nfour_pas, nmany_pas, next_pas)

    else

        call total_energy_nolist(ib, job%coultype(ib), job%vdw_cut, job%shortrangecut, job%dielec,  &
                          newreal, newvdw, newthree, newpair, newang, newfour, newmany, newext, newmfa, &
                          newvir, nrealnonb, nrealbond, nself, ncrct, nvdwn, nvdwb,  &
                          npair, nthree, nang, nfour, nmany, next, job%clist)

        !TU: Do the same as above for the passive box in PSMC and Gibbs
        if( is_psmc .or. is_gibbs ) &
            call total_energy_nolist(ib_pas, job%coultype(ib_pas), job%vdw_cut, job%shortrangecut, job%dielec, &
                          newreal_pas, newvdw_pas, newthree_pas, newpair_pas, newang_pas, newfour_pas, newmany_pas, &
                          newext_pas, newmfa_pas, newvir_pas, nrealnonb_pas, nrealbond_pas, nself_pas, ncrct_pas, &
                          nvdwn_pas, nvdwb_pas, npair_pas, nthree_pas, nang_pas, nfour_pas, nmany_pas, next_pas, job%clist)

    endif

    !if( newvdw + newext > vdwcap ) goto 1000 ! reject - new energy is too high

    if( job%coultype(ib) == 1 ) call total_recip(ib, newrcp, nrcp, newvir)

    newtotal = newrcp + newreal + newvdw + newthree + newpair + newang + newfour + newmany + newext + newmfa
    newenth  = newtotal + extpress * volnew*(slit_zfrac2-slit_zfrac1)

    !TU: Do the same as above for the passive box in PSMC and Gibbs
    if( is_psmc .or. is_gibbs ) then

        if( job%coultype(ib_pas) == 1 ) call total_recip(ib_pas, newrcp_pas, nrcp_pas, newvir_pas)

        newtotal_pas = newrcp_pas + newreal_pas + newvdw_pas + newthree_pas + newpair_pas + newang_pas + &
                       newfour_pas + newmany_pas + newext_pas + newmfa_pas

        newenth_pas  = newtotal_pas + extpress * volnew_pas*(slit_zfrac2-slit_zfrac1)

    end if

    if( is_fed_param_psmc ) then
       
        !TU: deltavb should be 0 at this point if we are using the PSMC order parameter. 
        !TU: The difference in the bias between the new state and the current state is given by
        !TU: fed_param_dbias_psmc(energy1, energy2), where energy1 and energy2 are the NEW 
        !TU: energies of phases 1 and 2
        if( ib == 1) then

            deltavb = fed_param_dbias_psmc(newtotal, newtotal_pas)

        else

            !TU: WARNING - I assume that ib==2 if ib/=1. Something has gone horribly
            !TU: wrong if this is not the case at this point, hence I don't even check.

            deltavb = fed_param_dbias_psmc(newtotal_pas, newtotal)

        end if

        !AB: stay within the defined range
        out_of_range = ( deltavb > job%toler )

        if( out_of_range ) goto 1000

    end if

    !TU: In general we need code here to set 'deltavb' for other order parameters, i.e. not PSMC.
    !TU: I understand this is unnecessary in this procedure for, e.g. COM as the order parameter, since
    !TU: the order parameter is unchanged by a volume move, and hence deltavb=0. But 'in general', here
    !TU: is where 'deltavb' should be set.

    deltav  = newtotal - oldtotal
    delta_vol = ( volnew-volold )

    !AB: correction of the volume change in slit accounting for a possible z-shift of one of the walls
    if( in_slit ) delta_vol = delta_vol*(slit_zfrac2-slit_zfrac1)

    !TU: WARNING: Does there need to be such a 'delta_vol' correction to the passive box - for Gibbs in a slit?

    !TU: deltavb if there were no biasing ('canonical')
    !deltavb_can = beta * ( deltav + extpress*( volnew-volold ) ) &
    if( is_gibbs ) then

        !TU: In the Gibbs ensemble volume move we must also consider the contribution from both
        !TU: the active and the passive boxes
        deltav = deltav + newtotal_pas - oldtotal_pas

        !TU: Note that 'dln_dim' is omitted here since here the Gibbs volume moves
        !TU: sample uniformly over V
        deltavb_can = beta * deltav &
                     - cfgs(ib)%num_vol_sites * log(volnew/volold) &
                     - cfgs(ib_pas)%num_vol_sites * log(volnew_pas/volold_pas)

    else

        deltavb_can = beta * ( deltav + extpress*delta_vol ) &
                    - (cfgs(ib)%num_vol_sites + dln_dim) * log( volnew/volold )

    end if

    !TU: deltavb which includes biasing if biasing is in use
    deltavb = deltavb + deltavb_can

    !TU: Update transition matrix if it is in use: the canonical (i.e, without biasing) probability is
    !TU: min(1,exp(-deltavb_can)).
    !TU: Note that this is only reached if the trial state is 'in bounds' for the order parameter range,
    !TU: and hence the transition matrix is only updated for in bounds trial states.
    if( fed % method == FED_TM .and. ( is_fed_vol .or. is_fed_param_psmc ) ) then
        
        call fed_tm_inc(  min( 1.0_wp, exp(-deltavb_can) )  )

    end if

    !TU: Reject the move if it leaves the window or takes us further from the window                          
    if( fed % is_window .and. ( is_fed_vol .or. is_fed_param_psmc ) ) then
       
       if( fed_window_reject() ) goto 1000
       
    end if

    !if( deltavb > job%toler ) goto 1000 ! reject - new energy is too high
    !if( deltavb < 0.0_wp .or. arg < exp(-deltavb) ) then
    
    if( arg < exp(-deltavb) .and. abs(deltav) <= job%toler ) then

        !AB: update the FED histogram & bias if needed (e.g. WL)
        if( is_fed_vol .or. is_fed_param_psmc ) then

            call fed_param_hist_inc(ib,.true.)

        end if

        !AB: rescale max displacements for improved efficiency (only in isotropic cases - uniform expansion)
        !TU: ... but don't do this for the Gibbs ensemble - WHY? During debugging I thought that it might
        !TU: violate detailed balance for the Gibbs. But I'm reconsidering this. Nevertheless I'll leave this
        !TU: rescaling disabled for the Gibbs ensemble for now.
        if( job%type_vol_move /= NPT_VEC .and. job%type_vol_move /= NPT_GIBBS .and. &
            job%type_vol_move /= NPT_ORTHOANI  ) then
            
            if( job%moveatm ) then
                    
                distance_atm_max(:,ib) = distance_atm_max(:,ib) * dimscale

            else if( job%movemol ) then
                
                distance_mol_max(:) = distance_mol_max(:) * dimscale

            end if

        end if

        !accept update energies etc
        energytot(ib) = newtotal
        enthalpytot(ib) = newenth
        virialtot(ib) = newvir
        energyrcp(ib) = newrcp
        energyreal(ib) = newreal
        energyvdw(ib) = newvdw
        energypair(ib) = newpair
        energythree(ib) = newthree
        energyang(ib) = newang
        energyfour(ib) = newfour
        energymany(ib) = newmany
        energyext(ib) = newext
        energymfa(ib) = newmfa
        volume(ib) = volnew

        !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
        !AB: NOT ANYMORE
        !if( job%uselist .and. job%lauto_nbrs .and. reset ) then
        !if( reset ) then
        !    call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist, .false.)
        !    call set_rzero(ib)
        !endif

        if( job%coultype(ib) == 1 ) call update_rcpsums(ib)

        if( nmetpot > 0 ) call copy_density(ib)

        !TU: Do the same as above for the passive box in PSMC and Gibbs
        if( is_psmc .or. is_gibbs ) then
            
            energytot(ib_pas) = newtotal_pas
            enthalpytot(ib_pas) = newenth_pas
            virialtot(ib_pas) = newvir_pas
            energyrcp(ib_pas) = newrcp_pas
            energyreal(ib_pas) = newreal_pas
            energyvdw(ib_pas) = newvdw_pas
            energypair(ib_pas) = newpair_pas
            energythree(ib_pas) = newthree_pas
            energyang(ib_pas) = newang_pas
            energyfour(ib_pas) = newfour_pas
            energymany(ib_pas) = newmany_pas
            energyext(ib_pas) = newext_pas
            energymfa(ib_pas) = newmfa_pas
            volume(ib_pas) = volnew_pas

            !must recalc whole nbrlist as other atoms will be out of sync on accepting the move
            !AB: NOT ANYMORE
            !if( reset_pas ) then
            !    call setnbrlists(ib_pas, job%shortrangecut, job%verletshell,  job%clist, .false.)
            !    call set_rzero(ib_pas)
            !endif

            if( job%coultype(ib_pas) == 1 ) call update_rcpsums(ib_pas)

            if( nmetpot > 0 ) call copy_density(ib_pas)
           
        end if

        novolchange(indx,ib) = novolchange(indx,ib) + 1
        successfulvolchange = successfulvolchange + 1

        !update component molecule energies
        if(job%lmoldata) then

            do im = 1, number_of_molecules

                emolrealnonb(ib,im) = nrealnonb(im)
                emolrealbond(ib,im) = nrealbond(im)
                emolrealself(ib,im) = nself(im)
                emolrealcrct(ib,im) = ncrct(im)
                emolrcp(ib,im) = nrcp(im)
                emolvdwn(ib,im) = nvdwn(im)
                emolvdwb(ib,im) = nvdwb(im)
                emolpair(ib,im) = npair(im)
                emolthree(ib,im) = nthree(im)
                emolang(ib,im) = nang(im)
                emolfour(ib,im) = nfour(im)
                emolmany(ib,im) = nmany(im)
                emolext(ib,im) = next(im)

                ntot = nrealnonb(im) + nrealbond(im) + nrcp(im) + nvdwn(im) + nvdwb(im) + npair(im) &
                        + nthree(im) + nang(im) + nfour(im) + nmany(im) + next(im)

                emoltot(ib,im) = ntot

                !TU: Do the same as above for the passive box in PSMC and Gibbs
                if( is_psmc .or. is_gibbs ) then

                    emolrealnonb(ib_pas,im) = nrealnonb_pas(im)
                    emolrealbond(ib_pas,im) = nrealbond_pas(im)
                    emolrealself(ib_pas,im) = nself_pas(im)
                    emolrealcrct(ib_pas,im) = ncrct_pas(im)
                    emolrcp(ib_pas,im) = nrcp_pas(im)
                    emolvdwn(ib_pas,im) = nvdwn_pas(im)
                    emolvdwb(ib_pas,im) = nvdwb_pas(im)
                    emolpair(ib_pas,im) = npair_pas(im)
                    emolthree(ib_pas,im) = nthree_pas(im)
                    emolang(ib_pas,im) = nang_pas(im)
                    emolfour(ib_pas,im) = nfour_pas(im)
                    emolmany(ib_pas,im) = nmany_pas(im)
                    emolext(ib_pas,im) = next_pas(im)
                    
                    ntot_pas = nrealnonb_pas(im) + nrealbond_pas(im) + nrcp_pas(im) + nvdwn_pas(im) + &
                         nvdwb_pas(im) + npair_pas(im) + nthree_pas(im) + nang_pas(im) + nfour_pas(im) + &
                         nmany_pas(im) + next_pas(im)
                    
                    emoltot(ib_pas,im) = ntot_pas

                 end if

            enddo

        endif

        return

    end if
    !else

        ! fail - put everything back
1000    call restorelatticevectors(cfgs(ib)%vec)
        call restore_all_pos(ib)

        !TU: Restore the cells for the active box (before the neighbour list is updated) by 
        !TU: recalculating the cell list from scratch.
        !TU: (Could be faster if stored cells?)
        if(job%clist) then

            call tear_down_cell_list(ib)
            call set_up_cell_list(job, ib)

        end if

        !check NL if in auto-update mode
        !call setnbrlists(ib, job%shortrangecut, job%verletshell,  job%clist, .false.)
        if( reset ) call update_verlet_cell(ib, job%verletshell, job%shortrangecut, reset, job%clist)

        !TU*: Reset the 3-body lists
        if(numthb > 0 ) then
           
            call set_thblist(ib)

        end if


        !TU: Do the same as above for the passive box in PSMC and Gibbs, and update PSMC geometrical variables
        if( is_psmc .or. is_gibbs ) then

            call restorelatticevectors(cfgs(ib_pas)%vec)
            call restore_all_pos(ib_pas)

            !TU: Restore the cells for the passive box (before the neighbour list is updated) by 
            !TU: recalculating the cell list from scratch
            !TU: (Could be faster if stored cells?)
            if(job%clist) then

                call tear_down_cell_list(ib_pas)
                call set_up_cell_list(job, ib_pas)

            end if

            if( reset_pas ) &
                call update_verlet_cell(ib_pas, job%verletshell, job%shortrangecut, reset_pas, job%clist)

        end if

        if( is_psmc ) then

            !TU: Update the site positions and displacements in the active box
            call set_cfg_sites(ib, ib)
            call set_cfg_u_from_pos(ib)

            !TU: Update the site positions and displacements in the passive box
            call set_cfg_sites(ib_pas, ib_pas)
            call set_cfg_u_from_pos(ib_pas)

            !TU*: Reset the 3-body list in the passive box if need be
            if( numthb > 0 ) call set_thblist(ib_pas)

        end if

        !AB: update the FED histogram & bias if needed (e.g. WL)
        !AB: but revert the parameter state (value & id/index)
        if( is_fed_vol .or. is_fed_param_psmc ) then

            call fed_param_hist_inc(ib,.false.)

        end if

        if(job%coultype(ib) == 1) call setup_ewald(ib, job)

        !TU: Do the same as above for the passive box in PSMC and Gibbs
        if( is_psmc .or. is_gibbs ) then

            if( job%coultype(ib_pas) == 1 ) call setup_ewald(ib_pas, job)

        end if

    !endif

end subroutine




!> collect P(V) histogram during V-sampling in NpT ensemble
subroutine collect_volhist(ibox, job, volume)

    use kinds_f90
    use control_type
    use cell_module
    use constants_module, only : uout!, unpt
    use arrays_module,    only : resize_dim_to
    use comms_mpi_module, only : is_parallel, gsum_world

    type(control),   intent(in) :: job

    real(kind = wp), intent(in) :: volume(:)

    integer,         intent(in) :: ibox

    character*40 :: word

    real(kind = wp) :: vnorm, vbin05, vscale

    integer :: ierr, mvol, vbin

    ierr = 0
    mvol = 0

    if( volume(ibox) > vol_max ) then

        mvol = 1

        max_vol_bin = int( (volume(ibox)/job%vol_bin)**(1.0_wp/job%vol_bin_power) ) + 50
        vol_max = job%vol_bin*real(max_vol_bin,wp)**job%vol_bin_power

        call resize_dim_to(volhist,max_vol_bin,nconfigs,ierr)

    end if

    if( is_parallel ) then

        call gsum_world(ierr)
        call gsum_world(mvol)

    end if

    if( mvol > 0 ) then 

        if( ierr == 0 ) then 

            write(word,*)max_vol_bin
            call cry(uout,'(/,1x,a,/)', &
                 "NOTE: hit max volume -> V-grid increased up to "//trim(word),0)

        else

            write(word,*)max_vol_bin
            call cry(uout,'', &
                 "ERROR: hit max volume, but failed to increase V-grid up to "//trim(word),999)

        end if

    end if

    vbin = int((volume(ibox)/job%vol_bin)**(1.0_wp/job%vol_bin_power))+1
    volhist(vbin,ibox) = volhist(vbin,ibox) + 1

end subroutine collect_volhist

!> write out the collected P(V) histogram normalised as probability density
subroutine write_volhist(job, iter)

    use kinds_f90
    use control_type
    use cell_module
    use constants_module,     only : uout!, unpt
    use parallel_loop_module, only : idgrp, open_nodefiles

    type(control), intent(in) :: job

    integer,       intent(in) :: iter

    real(kind = wp) :: vnorm, vbin05, vscale

    integer :: ib, ic, vsamples, ierr, mvol, vbin

    call open_nodefiles('NPTDAT', unpt, ierr)

    write(unpt,'(a,i4)')    "# P(V) distribution histogram - Workgroup No ",idgrp
    write(unpt,'(2(a,i10))')"# MC step No ",iter,", V-steps No ",totalvolchanges

    write(unpt,'(a,i4,2(a,e15.8),a,f10.5)') &
                        '# N_bins = ',max_vol_bin, &
                         ', V_max = ',vol_max, &
                         ', V_bin = ',job%vol_bin, &
                         ', V_pwr = ',job%vol_bin_power

    do ib = 1,nconfigs

       !vsamples = 0.0_wp
       !do ic = 1, 9
       !   vsamples = vsamples + attemptedvolchange(ic,ib)
       !enddo
       !write(unpt,'(/,a,i4,a,i10)')'# cell No ',ib,', V-samples ',vsamples

       write(unpt,'(/,a,i4)')'# cell No ',ib

       vnorm = 0.0_wp
       do vbin = 1, max_vol_bin

          vnorm = vnorm + volhist(vbin,ib)

       enddo

       if( vnorm > 0.0_wp ) then

           do vbin = 1, max_vol_bin

              vbin05 = job%vol_bin*(real(vbin,wp)**job%vol_bin_power &
                                  + real(vbin-1,wp)**job%vol_bin_power)*0.5_wp

              vscale = job%vol_bin*(real(vbin,wp)**job%vol_bin_power &
                                  - real(vbin-1,wp)**job%vol_bin_power)

              write(unpt,'(2f20.10)') &
                    vbin05, volhist(vbin,ib)/(vnorm*vscale)

           enddo

       else

           call cry(uout,'(/,1x,a,/)', &
                    "WARNING: no valuable P(V) data collected !!!", 0)

       endif

    enddo

    close(unpt)

end subroutine write_volhist

end module
