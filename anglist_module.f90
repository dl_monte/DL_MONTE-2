! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Molecule angle list storage and manipulation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `anglist_type` (within routines)

!> @modulefor molecule angle-list(s)
module anglist_module

    use kinds_f90

    implicit none

contains

!> @brief 
!> - allocates array for an angle list
!> @using 
!> - `anglist_type`
subroutine alloc_anglist_arrays(list, n)

    use anglist_type

    implicit none

        !> array to store angle list
    type(anglist), intent(inout) :: list

        !> number of angles in the list
    integer , intent(in) :: n

    list%nang = n

    if(allocated(list%apair)) deallocate (list%apair)

    allocate (list%apair(n,4))

end subroutine

!> @brief 
!> - initialises angle elements in an existing (pre-allocated) angle list array
!> @using 
!> - `anglist_type`
subroutine add_triples(list, atomi, atomj, atomk, pot, n)

    use anglist_type

    implicit none

        !> array to store angle list
    type(anglist), intent(inout) :: list

        !> number of angles in the list
    integer , intent(in) :: n

        !> arrays containing atoms (indices/pointers?) of angle triplets
    integer , intent(in) :: atomi(n), atomj(n), atomk(n)

        !> array containing potentials (types?)
    integer , intent(in) :: pot(n)

    integer :: i

    do i = 1, n
        list%apair(i,1) = atomi(i)
        list%apair(i,2) = atomj(i)
        list%apair(i,3) = atomk(i)
        list%apair(i,4) = pot(i)
    enddo

end subroutine

!> @brief 
!> - (re)allocates a new angle list array '`nlist`' and copies into it all elements of '`olist`'
!> @using 
!> - `anglist_type`
subroutine alloc_and_copy_anglist_arrays(nlist, olist)

    use anglist_type

    implicit none

        !> new angle list
    type(anglist), intent(inout) :: nlist

        !> old angle list
    type(anglist), intent(in) :: olist

    integer :: i, fail

    if (olist%nang == 0) return

    fail = 0
    if(allocated(nlist%apair)) deallocate (nlist%apair, stat = fail)

    if (fail > 0) call error(342)

    nlist%nang = olist%nang

    fail = 0
    allocate (nlist%apair(nlist%nang,4), stat = fail)

    if (fail > 0) call error(341)

    do i = 1, nlist%nang

        nlist%apair(i,1) = olist%apair(i,1)
        nlist%apair(i,2) = olist%apair(i,2)
        nlist%apair(i,3) = olist%apair(i,3)
        nlist%apair(i,4) = olist%apair(i,4)

    enddo

end subroutine

!> @brief 
!> - copies all elements of an existing angle list '`olist`' into another existing angle list '`nlist`'
!> @using 
!> - `anglist_type`
subroutine copy_anglist(nlist, olist)

    use anglist_type

    implicit none

        !> new angle list
    type(anglist), intent(inout) :: nlist

        !> old angle list
    type(anglist), intent(in) :: olist

    integer :: i

    if (olist%nang == 0) return

    nlist%nang = olist%nang

    do i = 1, nlist%nang

        nlist%apair(i,1) = olist%apair(i,1)
        nlist%apair(i,2) = olist%apair(i,2)
        nlist%apair(i,3) = olist%apair(i,3)
        nlist%apair(i,4) = olist%apair(i,4)

    enddo

end subroutine

!> @brief 
!> - deallocates (destroys) an angle list array
!> @using 
!> - `anglist_type`
subroutine deallocate_anglist_arrays(list)

    use anglist_type

    implicit none

        !> the angle list to be destroyed
    type(anglist), intent(inout) :: list

    integer :: i, fail

    if( list%nang == 0 ) return

    fail = 0
    if(allocated(list%apair)) deallocate (list%apair, stat = fail)

    if (fail > 0) call error(342)

    list%nang = 0

end subroutine

!> @brief 
!> - prints out an angle list into file unit '`uout`'
!> @using 
!> - `anglist_type`
!> - `constants_module, only : uout`
!> - `comms_mpi_module, only : idnode`
subroutine print_anglist(list)

    use anglist_type
    use constants_module, only : uout
    use comms_mpi_module, only : idnode

    implicit none

        !> the angle list to be printed out into file unit '`uout`'
    type(anglist), intent(inout) :: list

    integer :: i

    if (idnode /= 0) return

    do i = 1, list%nang

        write(uout,"(/,1x,'angle (i,j,k) between atoms ',3i4,' with potential ',i4)")list%apair(i,:)

    enddo

end subroutine

end module
