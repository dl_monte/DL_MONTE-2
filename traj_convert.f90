
subroutine traj_convert(job, nconfs)

    use constants_module!,     only : uout
    use control_type
    use molecule_type
    use statistics_type

    use control_module !,        only : printandcheck
    use config_module !,        only : inputconfig
    use cell_module !,          only : inputcells, read_config         ! wrapper for reading HISTORY or ARCHIVE files
    use species_module,       only : number_of_molecules ! number of unique molecular species from FIELD
    use montecarlo_module,    only : setup_rigid_bodies
    use statistics_module,    only : zerostats, zerotypes, zero_zdens, zero_rdf, zero_displacements

    use comms_omp_module,     only : mptime
    use comms_mpi_module,     only : master !, idnode
    use parallel_loop_module, only : setup_workgroups, wkgrp_size, idgrp, open_nodefiles
    use random_module,        only : duni, init_duni

    !use comms_mpi_module,     only : master, is_parallel, idnode, gsync_world, gsum_world, write_mpi_stats !, mxnode
    !use parallel_loop_module, only : setup_workgroups, wkgrp_size, idgrp, open_nodefiles
    !use rep_exchange_module,  only : attempted_rep_exch, rejected_rep_exch, successful_rep_exch, &
    !                                initialise_rep_exchange, rep_exch_beta !, rep_exchange

    implicit none

    type(control), intent(inout) :: job
    integer,       intent(inout) :: nconfs

    !AB: <<- Extract from montecarlo_module.f90:normalmc(job, numcfgs)

    real(kind=wp) :: betainv, beta, blng
    real(kind=wp) :: tzero, eltime, dummy

    real(kind=wp) :: cfg_energy

    type(statistics), allocatable, dimension (:) :: stats

    logical, save :: is_GCMC = .false.

    !AB: ->> Extract from montecarlo_module.f90:normalmc(job, numcfgs)

    character(7), save :: fname="HISTORY"

    integer, save :: i, ib, iform, iframe, itermc0, itermc1, istat, &
                     natoms, nframes, mframes, matoms, mfirst, mstep

    integer, save :: iodcd = 33

    integer, save :: icell_dcd, matoms_dcd, mframes_dcd, ifstart_dcd, ifstep_dcd, iclose_dcd

    integer :: istep, istore, nstore

    logical :: nlflag, is_gibbs_ens

    iform   = job % traj_format
    iframe  = 0
    itermc0 = 0
    itermc1 = 0
    istat   = 0

    natoms  = 0
    matoms  = 0
    nframes = job % traj_end
    mframes = nframes

    !AB: frame stepping in input trajectory is normally unknown, so taking it from CONTROL
    istep = job % traj_step * job % sysdump

    istore = 0
    nstore = 0

    matoms_dcd  = 0
    mframes_dcd = 0
    ifstart_dcd = 0
    ifstep_dcd  = 0
    iclose_dcd  = 0

!AB: *** prepare the config & stats structures - allocate arrays etc ***

!AB: <<- Extract from config_module.f90:inputconfig(job, ib, cfg, cfgfmt)
!AB: ... perhaps not necessary (see below) ...
!AB: ->> Extract from config_module.f90:inputconfig(job, ib, cfg, cfgfmt)

!AB: <<- Extract from montecarlo_module.f90:normalmc(job, numcfgs)

    !AB: when work-groups are used (replica-exchange) 'master' exists for each group
    !AB: the output data from every master/group go into separate files (<NAME>.<NUM> where <NUM>=idgrp)

    !before reading the configurations - setup the rigid bodies
    call setup_rigid_bodies(job)

    !read config file - the staring config is loaded to all nodes
    !nconfig = numcfgs
    if(nconfs == 0) call error(206)

    call inputcells(job, nconfs)

    !TU: Read CONFIG.1 and CONFIG.2 files to get PSMC ideal phases, and initialise PSMC variables
    !if( fed%flavor == FED_PS ) call initialise_psmc(job)

    !for replica exchange the workgroups need to be established
    !if( job % repexch ) &
    !    call setup_workgroups(job%numreplicas, job%repexch_inc, job%systemp, job%distribgvec, job%lparallel_atom)

    !allocate(interrupt(0:job%numreplicas))
    !interrupt = 0

    ! ...

    !TU: Set seeds in random_module.f90 to those in 'job', thus initialising the random number generator.
    !TU: Note that setupmc(job) (called above) sets the seed variables in 'job', and must be called before
    !TU: init_duni(job), which transfers those variables to random_module.f90
    call init_duni(job)
    dummy = duni()

    if( master ) then

        !write(uout,"(/,/,1x,'*********************************************************************')")
        write(uout,"(/,/,1x,50('='))")
        write(uout,"(a)")"                simulation parameters "
        write(uout,"(1x,50('='))")
        !write(uout,"(1x,'*********************************************************************',/)")

    endif

    ! print run parameters
    call printandcheck(job)

    ! get starting time
    call mptime(tzero)

    ! create statistics object for each box and zero arrays
    allocate(stats(nconfs))

    ! set beta & Bjerrum length
    !betainv = job % systemp * BOLTZMAN
    !beta = 1.0_wp / betainv
    !blng = CTOINTERNAL*beta / job%dielec
    !if( master ) then
    !    write(uout,"(/,1x,'beta & Bjerrum length                            =',2f20.10)") beta, blng
    !    write(uout,"(/,1x,50('='),/)")
    !end if

    if(job%type_vol_move /= 0) then
        nlflag = .true.
        job%dcd_cell = 1
    !    icell_dcd = 1
    !else
    !    nlflag = .false.
    !    icell_dcd = 0
    endif
    icell_dcd = job%dcd_cell

    matoms     = 0
    matoms_dcd = 0
    cfg_energy = 0.0_wp

    ! zero total energies
    do i = 1, nconfs

        matoms      = max(matoms_dcd,cfgs(i)%maxno_of_atoms)
        matoms_dcd  = max(matoms_dcd,cfgs(i)%number_of_atoms)

        call zerostats(job%stacksize, job%energyunit, stats(i), job%repexch)

        call zerotypes(stats(i))

        ! ...

        if (job%lzdensity) call zero_zdens(stats(i), job%nzden)

        if (job%lrdfs) call zero_rdf(stats(i), job%nrdfs, job%rdf_max)

        if (job%lsample_displacement) call zero_displacements(stats(i), cfgs(i)%maxno_of_atoms)

    enddo

    is_GCMC = ( job%gcmcatom .or. job%gcmcmol .or. job%gibbsatomtran .or. job%gibbsmoltran & 
                             .or. job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch )

    if( is_GCMC ) matoms_dcd = matoms

!AB: ->> Extract from montecarlo_module.f90:normalmc(job, numcfgs)

!AB: *** prepare the config & stats structures - allocate arrays etc - DONE ***

    if( master ) then 

        write(uout,"(/,1x,50('='),/)")

        write(uout,'(a,/)')  "      *** Specs for trajectory conversion *** "
        write(uout,'(a,5i8)')"   Frames: begin, end, step, total, skip(tot) = ", &
                                 job%traj_beg,job%traj_end,job%traj_step, &
                                (job%traj_end-job%traj_beg)/job%traj_step+1,istep

        write(uout,"(/,1x,50('='),/)")

    end if

    IF( job % traj_format == DCD ) THEN

!AB: reference from control_module.f90:printandcheck(job)
!
!        if( job % archive_format == DLP2 ) then
!            call cry(uout,'(/,1x,a,/)', &
!                 "TRAJECTORY [DCD] -> HISTRAJ [DL_POLY-2]"//&
!                &" - converting existing trajectory",0)
!        else if( job % archive_format == DLP4 ) then
!            call cry(uout,'(/,1x,a,/)', &
!                 "TRAJECTORY [DCD] -> HISTRAJ [DL_POLY-4]"//&
!                &" - converting existing trajectory",0)
!        else if( job % archive_format == DLP ) then
!            call cry(uout,'(/,1x,a,/)', &
!                 "TRAJECTORY [DCD] -> ARCTRAJ [DL_POLY style]"//&
!                &" - converting existing trajectory",0)
!        else if( job % archive_format == DLM ) then
!            call cry(uout,'(/,1x,a,/)', &
!                 "TRAJECTORY [DCD] -> ARCTRAJ [DL_MONTE style]"//&
!                &" - converting existing trajectory",0)
!        end if

        !AB: to be read from the DCD TRAJECTORY HEADER
        matoms_dcd  = 0  ! to be read in
        mframes_dcd = 0  ! to be read in
        ifstart_dcd = 0  ! to be read in
        ifstep_dcd  = 0  ! to be read in
        iclose_dcd  = 0  ! flag (not) to close the DCD file

        !AB: defined above
        !nframes = job % traj_end
        !mframes = nframes

        ib = idgrp+1

        !AB: declaration for reference:
        !call read_config_dcd(fname, iodcd, iform, ib, icell, matoms, mframes, iframe, ifstep, iclose)

        !AB: read the DCD HEADER first
        call read_config_dcd("TRAJECTORY", iodcd, job%dcd_form, ib, icell_dcd, &
                             matoms_dcd, mframes_dcd, ifstart_dcd, ifstep_dcd, iclose_dcd)

        if( iclose_dcd == 99 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed opening the input DCD file !!!",999)

        else if( iclose_dcd == 999 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed reading the input DCD HEADER !!!",999)

        else if( matoms_dcd < 1 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"# atoms in the input HEADER < 1 (no atoms) !!!",999)

        else if( matoms_dcd > matoms ) then

                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: Trajectory conversion failed - "//&
                        &"# atoms in the input HEADER > max # atoms in CONFIG !!!",999)

        else if( mframes_dcd < 1 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"# frames in the input HEADER < 1 (no frames) !!!",999)

        else if( mframes_dcd < mframes ) then

            call cry(uout,'(/,1x,a,/)', &
                     "WARNING: Trajectory conversion started - "//&
                    &"# frames in the input HEADER < # frames required (resetting) !!!",0)

        end if

        !iclose_dcd  = 0  ! flag not to close the DCD file next time

        itermc0 = ifstart_dcd
        itermc1 = ifstart_dcd

        nframes = min(mframes, mframes_dcd)
        mframes = itermc0 + nframes*ifstep_dcd

        !nstore = nframes/job%traj_step + 1
        nstore = (job%traj_end - job%traj_beg)/job%traj_step + 1

        if( job%archive_format < DLP2 ) then
            !call open_nodefiles('ARCTRAJ', uarch, istat)
            fname="ARCTRAJ"

        else if( job%archive_format < DCD ) then
            !call open_nodefiles('HISTRAJ', uarch, istat)
            fname="HISTRAJ"

        else
            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"unsupported format of the output trajectory in CONTROL !!!",999)
        end if

        call open_nodefiles(fname, uarch, istat)

        if( istat /= 0 ) &
            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed opening the output file !!!",999)

        !write(uout,*) "Opened the input & output trajectory files: beg/end/step/frames = ",&
        !              itermc0,mframes,ifstep_dcd,nframes

        istore = 0
        do  iframe = 1, nframes

            if( itermc1+1 > mframes ) iclose_dcd = 1 ! end of file - close the DCD file next time

            !AB: declaration for reference:
            !call read_config_dcd(fname, iodcd, iform, ib, icell, matoms, mframes, iframe, ifstep, iclose)
            call read_config_dcd("TRAJECTORY", iodcd, job%dcd_form, ib, icell_dcd, &
                             matoms_dcd, mframes_dcd, ifstart_dcd, ifstep_dcd, iclose_dcd)

            if( iclose_dcd > 90 ) exit

    !write(uout,*)
    !write(uout,*) 'Read in another frame from input trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

            if( iframe >= job%traj_beg .and. mod(iframe-job%traj_beg,job%traj_step) == 0 ) then

                call dump_config(uarch, ib, job%archive_format, cfg_energy, itermc1)

                flush(uarch)

                istore = istore+1

    write(uout,*)
    write(uout,*) 'Appended a new frame to output trajectory: # frame / frames (tot) = ',istore,' / ',nstore,itermc1

            end if

            itermc1 = itermc1+ifstep_dcd

        end do

        close(uarch)

        if( iframe == nframes+1 ) iframe = nframes

        write(uout,'(/,2(a,i10),a)') &
                   ' Number of frames read in: ',iframe,' (out of expected',nframes,')'
        write(uout,'(/,2(a,i10),a)') &
                   ' Number of frames converted: ',istore,' (out of expected',nstore,')'
        flush(uout)

        if( iclose_dcd == 99 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed opening the input DCD file !!!",999)

        else if( iclose_dcd == 999 ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed reading NEXT FRAME in the input DCD file !!!",999)

        else if( iclose_dcd == 9999 ) then

             call cry(uout,'(/,1x,a,/)', &
                      "ERROR: Trajectory conversion failed - "//&
                     &"# atoms in CONFIG > # atoms in the input DCD file !!!",999)

        else if( iframe == nframes ) then

            if( iclose_dcd == 0 ) close(iodcd)

            call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Trajectory conversion completed - "//&
                    &" all the required frames read & stored successfully",0)

        else If( iclose_dcd == 0 ) then

            close(iodcd)

            call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Trajectory conversion seems completed - "//&
                    &"the end of input file has been reached (all files closed)",0)

        end if

    ELSE IF( job % archive_format == DCD ) THEN

!AB: reference from control_module.f90:printandcheck(job)
!
!        if( job % traj_format < DLP2 ) then
!            call cry(uout,'(/,1x,a,/)', &
!                     "ARCHIVE [DL_MONTE or DL_POLY style] -> TRAJARC [DCD]"//&
!                    &" - converting existing trajectory",0)
!        else if( job % traj_format < DCD ) then
!            call cry(uout,'(/,1x,a,/)', &
!                     "HISTORY [DL_POLY 2 or 4] -> TRAJHIS [DCD]"//&
!                    &" - converting existing trajectory",0)
!        end if

        !if( job % traj_format < DLP2 ) then
        !    call cry(uout,'(/,1x,a,/)', &
        !            "ERROR: Conversion ARCHIV -> TRAJECTORY (DCD) cannot be done - "//&
        !           &"not implemented yet !!!",999)
        !end if

        mframes_dcd = (job%traj_end - job%traj_beg)/job%traj_step + 1
        ifstart_dcd = job%traj_beg
        ifstep_dcd  = job%traj_step
        iclose_dcd  = 0

        itermc0 = 0
        itermc1 = 0

        ib = idgrp+1

        !AB: read in the header ! (without the first frame)
        !call read_config(ib, iform, .false. , natoms, cfgeng, itermc0, istat)

        if( job % traj_format == DLM ) then

            call read_config(ib, iform, .true. , natoms, cfg_energy, itermc0, istat)
            fname="TRAJARC"

        else if( job % traj_format == DLP ) then

            call read_config(ib, iform, .false. , natoms, cfg_energy, itermc0, istat)
            fname="TRAJARC"

        else if( job % traj_format < DCD ) then

            call read_config(ib, iform, .false. , natoms, cfg_energy, itermc0, istat)
            fname="TRAJHIS"

        else

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: trajectory input and output formats "//&
                    &"are the same (DCD) in CONTROL - nothing to do !!!",999)
        end if

        If( istat == 0 ) Then

            if( natoms > 0 ) then

                if( natoms > matoms_dcd ) then
                    call cry(uout,'(/,1x,a,/)', &
                             "ERROR: Trajectory conversion failed - "//&
                            &"# atoms in the input HEADER > max # atoms in CONFIG !!!",999)
                end if

            else
                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: Trajectory conversion failed - "//&
                        &"# atoms is absent in the HEADER of the input (HISTORY/ARCHIVE) file !!!",999)
            end if

            !AB: if number of frames in input is known in advance (DL_POLY-4 HISTORY only):
            !if( itermc0 > 0 ) mframes = itermc0

            !nstore = nframes/job%traj_step + 1
            nstore = (job%traj_end - job%traj_beg)/job%traj_step + 1

            istore  = 0
            itermc0 = 0
            do  iframe = 1, nframes

                !AB: read in the all rest (frames)
                !call read_config(ib, iform, is_close, natoms, cfgeng, itermc1, istat)
                call read_config(ib, iform, .false., natoms, cfg_energy, itermc1, istat)

                if( istat /= 0 .or. iclose_dcd /= 0 ) exit

                if( itermc1 == 0 ) itermc1 = iframe*job%sysdump

                !AB: if number of frames in input is known in advance (DL_POLY-4 HISTORY only):
                !if( mframes > nframes .and. itermc1 > mframes ) exit

    !write(uout,*)
    !write(uout,*) 'Read in another frame from input trajectory: # frame / frames (tot) = ',iframe,' / ',nframes

                if( natoms > matoms ) &
                    call cry(uout,'(/,1x,a,/)', &
                             "ERROR: Trajectory conversion started - "//&
                            &"# atoms in the input FRAME > max read from CONFIG !!!",999)

                if( natoms > matoms_dcd ) &
                    call cry(uout,'(/,1x,a,/)', &
                             "ERROR: Trajectory conversion failed - "//&
                            &"# atoms in the input FRAME > max set in the output HEADER !!!",999)

                if( iframe >= job%traj_beg .and. mod(iframe-job%traj_beg,job%traj_step) == 0 ) then

                    !AB: if number of frames in input is known in advance (DL_POLY-4 HISTORY only):
                    !if( mframes > nframes .and. itermc1 == mframes ) iclose_dcd = 1

                    !call write_config_dcd('TRAJHIS', iodcd, job%dcd_form, ib, icell_dcd, matoms_dcd, &
                    call write_config_dcd(fname, iodcd, job%dcd_form, ib, icell_dcd, matoms_dcd, &
                                        mframes_dcd, itermc1, istep, iclose_dcd)
                                        !mframes_dcd, itermc1, itermc1-itermc0, iclose_dcd)

                    istore  = istore+1
                    itermc0 = itermc1

    write(uout,*)
    write(uout,*) 'Appended a new frame to DCD trajectory: # frame / frames (tot) = ',istore,' / ',nstore,itermc1

                    if( iclose_dcd /= 0 ) exit
                    !    call cry(uout,'(/,1x,a,/)', &
                    !             "ERROR: Trajectory conversion failed - "//&
                    !            &"failed to write NEXT FRAME to the output DCD file !!!",999)

                end if

            end do

        End If

        !AB: close the input file
        call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

        if( iframe == nframes+1 ) iframe = nframes

        write(uout,'(/,2(a,i10),a)') &
                   ' Number of frames read in: ',iframe,' (out of expected',nframes,')'
        write(uout,'(/,2(a,i10),a)') &
                   ' Number of frames converted: ',istore,' (out of expected',nstore,')'
        flush(uout)

        If( iclose_dcd > 90 ) then

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            !write(uout,*)
            !write(uout,*)'Number of frames read in: ',iframe,' (out of expected',nframes,')'
            !flush(uout)

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"failed writing to the output DCD file !!!",999)

        Else If( istat == 33 ) Then

            if( iclose_dcd == 0 ) close(iodcd)

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: Trajectory conversion failed - "//&
                    &"could not read specs in the HEADER (HISTORY/ARCHIVE) !!!",999)

        Else If( istat == 66 ) Then

            if( iclose_dcd == 0 ) close(iodcd)

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            if( iframe < 1 ) then 

                !write(uout,*)
                !write(uout,*)'Number of frames read in: ',iframe,' (out of expected',nframes,')'
                !flush(uout)

                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: Trajectory conversion failed - "//&
                        &"could not read the HEADER from the input (HISTORY/ARCHIVE) file !!!",999)

            else if( iframe < nframes ) then 

                call cry(uout,'(/,1x,a,/)', &
                         "WARNING: Trajectory conversion stopped - "//&
                        &"failed to read the 1-st line of NEXT FRAME in the input (HISTORY/ARCHIVE) file !!!",0)

            else !( iframe /= nframes ) then 

                call cry(uout,'(/,1x,a,/)', &
                         "WARNING: Trajectory conversion done for the required frames - "//&
                        &"stopped at the 1-st line of EXTRA FRAME in the input (HISTORY/ARCHIVE) file !!!",0)

            end if

        Else If( istat == 99 ) Then

            if( iclose_dcd == 0 ) close(iodcd)

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            if( iframe < 1 ) then 

                !write(uout,*)
                !write(uout,*)'Number of frames read in: ',iframe,' (out of expected',nframes,')'
                !flush(uout)

                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: Trajectory conversion failed - "//&
                        &"could not read ANY FRAME from the input (HISTORY/ARCHIVE) file !!!",999)

            else if( iframe < nframes ) then 

                call cry(uout,'(/,1x,a,/)', &
                         "WARNING: Trajectory conversion stopped - "//&
                        &"failed reading NEXT FRAME from the input (HISTORY/ARCHIVE) file !!!",0)

            else !( iframe /= nframes ) then 

                call cry(uout,'(/,1x,a,/)', &
                         "WARNING: Trajectory conversion done for the required frames - "//&
                        &"failed reading EXTRA FRAME from the input (HISTORY/ARCHIVE) file !!!",0)

            end if

        Else If( iframe == nframes ) Then

            if( iclose_dcd == 0 ) close(iodcd)

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Trajectory conversion completed - "//&
                    &" all the required frames read & stored successfully",0)

        Else If( istat == 0 .and. iclose_dcd /= 0 ) Then

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            call cry(uout,'(/,1x,a,/)', &
                     "NOTE: Trajectory conversion seems completed - "//&
                    &"the end of DCD file has been reached (all files closed)",0)

        Else

            !AB: close the input file
            !call read_config(ib, iform, .true. , natoms, cfg_energy, itermc1, istat)

            call cry(uout,'(/,1x,a,/)', &
                     "WARNING: Trajectory conversion not completed - "//&
                    &"reading the input (HISTORY/ARCHIVE) file stopped abnormally !!!",0)

            write(uout,*)'IO_stat: ',istat,', iclose_dcd: ',iclose_dcd

        End If

    ELSE

        call cry(uout,'(/,1x,a,/)', &
                 "ERROR: unkown trajectory conversion route: input -> output"//&
                &" - nothing to do (check the relevant specs in CONTROL) !!!",999)

    END IF

end subroutine traj_convert

