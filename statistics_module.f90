! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!sums energies for statistics arrays
module statistics_module

    use kinds_f90

    implicit none

   !> the number of parameters for  energies
    integer, parameter :: mxeng = 21

    integer ::   nsyst
        ! no of zum's to calculate rolling average
    integer ::       zistk

        ! size of stack accumulators
    integer ::       nstk

        ! local copy of dimensions for no atomic species in simulation
    integer ::       num_spec

        ! local copy of dimensions for no molecular species in sim
    integer ::       num_mol

        ! no of samples for transmutations
    integer ::       sampletrans

        ! no of samples taken for av energies
    integer ::       sample

        ! no of samples of types
    integer ::       sampletyp

        ! controls printing of lattice vectors + stress
    logical ::   printlat

        ! conversion factor to print out energies
    real(kind = wp) ::    print_unit

contains

subroutine zerostats(stacksize, energyunit, stat, flag)

    use kinds_f90
    use statistics_type
    !use constants_module, only : uplt
    use comms_mpi_module, only : idnode
    use parallel_loop_module, only : open_nodefiles

    implicit none

    integer, intent(in) :: stacksize
    real(kind = wp), intent(in) :: energyunit
    logical, intent(in) :: flag

    type(statistics), intent(inout) :: stat

    nstk = stacksize

    sample = 0
    sampletyp = 0
    sampletrans = 0

    print_unit = 1.0_wp / energyunit 

    allocate(stat% stpeng(mxeng))
    allocate(stat% aveeng(mxeng))
    allocate(stat% flceng(mxeng))
    allocate(stat% raveeng(mxeng))
    allocate(stat% zumeng(mxeng))
    allocate(stat% stkeng(nstk, mxeng))
    stat% stpeng  = 0.0_wp
    stat% aveeng  = 0.0_wp
    stat% flceng  = 0.0_wp
    stat% raveeng = 0.0_wp
    stat% zumeng  = 0.0_wp
    stat% stkeng  = 0.0_wp 

    allocate(stat% stk_latvec(nstk,3,3))
    allocate(stat% stk_strs(nstk,3,3))

    stat% stp_latvec  = 0.0_wp 
    stat% ave_latvec  = 0.0_wp 
    stat% rave_latvec = 0.0_wp 
    stat% zum_latvec  = 0.0_wp 
    stat% flc_latvec  = 0.0_wp 
    stat% stp_strs    = 0.0_wp 
    stat% ave_strs    = 0.0_wp 
    stat% rave_strs   = 0.0_wp 
    stat% zum_strs    = 0.0_wp 
    stat% flc_strs    = 0.0_wp 
    stat% stk_latvec  = 0.0_wp
    stat% stk_strs    = 0.0_wp

    stat% nsample   = 0
    stat% nsamp_typ = 0
    stat% nsamp_wdm = 0

    !AB: it is clearer to have all the files open in montecarlo_module.f90

    !if (idnode == 0) open(unit = uplt, FILE = 'PTFILE.000')
    !if (flag) call open_nodefiles('PTFILE', uplt)

end subroutine

!> allocates arrays for numbers of species and molecular energies
subroutine zerotypes(stat)

    use statistics_type
    use species_module, only : number_of_elements, number_of_molecules

    implicit none

    type(statistics), intent(inout) :: stat

    allocate(stat%stp_nspc(number_of_elements))
    allocate(stat%ave_nspc(number_of_elements))
    allocate(stat%rave_nspc(number_of_elements))
    allocate(stat%zum_nspc(number_of_elements))
    allocate(stat%flc_nspc(number_of_elements))
    allocate(stat%stk_nspc(nstk, number_of_elements))

    stat%stp_nspc = 0.0_wp 
    stat%ave_nspc = 0.0_wp 
    stat%rave_nspc = 0.0_wp 
    stat%zum_nspc = 0.0_wp 
    stat%flc_nspc = 0.0_wp 
    stat%stk_nspc = 0.0_wp

    allocate(stat%stp_nmol(number_of_molecules))
    allocate(stat%ave_nmol(number_of_molecules))
    allocate(stat%rave_nmol(number_of_molecules))
    allocate(stat%zum_nmol(number_of_molecules))
    allocate(stat%flc_nmol(number_of_molecules))
    allocate(stat%stk_nmol(nstk, number_of_molecules))

    stat%stp_nmol = 0.0_wp
    stat%ave_nmol = 0.0_wp
    stat%rave_nmol = 0.0_wp
    stat%zum_nmol = 0.0_wp
    stat%flc_nmol = 0.0_wp
    stat%stk_nmol = 0.0_wp

    ! variables for discrimination of energy over molecule types
    allocate(stat%stp_emol(mxeng, number_of_molecules))
    allocate(stat%ave_emol(mxeng, number_of_molecules))
    allocate(stat%rave_emol(mxeng, number_of_molecules))
    allocate(stat%zum_emol(mxeng, number_of_molecules))
    allocate(stat%flc_emol(mxeng, number_of_molecules))
    allocate(stat%stk_emol(nstk, mxeng, number_of_molecules))

    stat%stp_emol = 0.0_wp
    stat%ave_emol = 0.0_wp
    stat%rave_emol = 0.0_wp
    stat%zum_emol = 0.0_wp
    stat%flc_emol = 0.0_wp
    stat%stk_emol = 0.0_wp

end subroutine

subroutine zero_semiwidom_chempot(ib, numsemi, stat, semitype1, semitype2)

    use kinds_f90
    use statistics_type
    use cell_module, only:  findnumtype

    implicit none

    integer, intent(in) :: ib, numsemi, semitype1(:), semitype2(:)
    type(statistics), intent(inout) :: stat

    integer :: k, numtype1, numtype2
    real(kind = wp) :: n1, n2

    allocate(stat%stp_achp1(numsemi))
    allocate(stat%ave_achp1(numsemi))
    allocate(stat%rave_achp1(numsemi))
    allocate(stat%zum_achp1(numsemi))
    allocate(stat%flc_achp1(numsemi))
    allocate(stat%stk_achp1(nstk, numsemi))

    allocate(stat%stp_achp2(numsemi))
    allocate(stat%ave_achp2(numsemi))
    allocate(stat%rave_achp2(numsemi))
    allocate(stat%zum_achp2(numsemi))
    allocate(stat%flc_achp2(numsemi))
    allocate(stat%stk_achp2(nstk, numsemi))

    allocate(stat%ideal1(numsemi))
    allocate(stat%ideal2(numsemi))

    stat%stp_achp1 = 0.0_wp
    stat%stp_achp2 = 0.0_wp
    stat%ave_achp1 = 0.0_wp
    stat%ave_achp2 = 0.0_wp
    stat%rave_achp1 = 0.0_wp
    stat%rave_achp2 = 0.0_wp
    stat%zum_achp1 = 0.0_wp
    stat%zum_achp2 = 0.0_wp
    stat%flc_achp1 = 0.0_wp
    stat%flc_achp2 = 0.0_wp
    stat%stk_achp1 = 0.0_wp
    stat%stk_achp2 = 0.0_wp

    stat%ideal1 = 0.0_wp
    stat%ideal2 = 0.0_wp

    do k = 1, numsemi

        call findnumtype(ib, semitype1(k), numtype1)
        call findnumtype(ib, semitype2(k), numtype2)

        n1 = real(numtype1,wp) / (real(numtype2,wp) + 1.0_wp)
        n2 = real(numtype2,wp) / (real(numtype1,wp) + 1.0_wp)

        !TU: The below fails when n1 or n2 are 0, which occurs when the
        !TU: number of atoms of the type are 0. The result is a seg fault.
        !TU: To get passed this I've set the values to 0.0 if this occurs.
        !TU: This is not an ideal solution, just a quick-and-dirty fix!!!
        !stat%ideal1(k) = log(n1)
        !stat%ideal2(k) = log(n2)
        stat%ideal1(k) = 0.0_wp
        stat%ideal2(k) = 0.0_wp
        if(n1 > 0.0) stat%ideal1(k) = log(n1)
        if(n2 > 0.0) stat%ideal2(k) = log(n2)


    enddo

end subroutine

subroutine zero_semigrand_chempot(ib, numsemi, stat, semitype1, semitype2)

    use kinds_f90
    use statistics_type
    use cell_module, only:  findnumtype

    implicit none

    integer, intent(in) :: ib, numsemi, semitype1(:), semitype2(:)
    type(statistics), intent(inout) :: stat

    integer :: k, numtype1, numtype2
    real(kind = wp) :: n1, n2

    allocate(stat%stp_achp1(numsemi))
    allocate(stat%ave_achp1(numsemi))
    allocate(stat%rave_achp1(numsemi))
    allocate(stat%zum_achp1(numsemi))
    allocate(stat%flc_achp1(numsemi))
    allocate(stat%stk_achp1(nstk, numsemi))

    allocate(stat%stp_achp2(numsemi))
    allocate(stat%ave_achp2(numsemi))
    allocate(stat%rave_achp2(numsemi))
    allocate(stat%zum_achp2(numsemi))
    allocate(stat%flc_achp2(numsemi))
    allocate(stat%stk_achp2(nstk, numsemi))

    allocate(stat%ideal1(numsemi))
    allocate(stat%ideal2(numsemi))

    stat%stp_achp1 = 0.0_wp
    stat%stp_achp2 = 0.0_wp
    stat%ave_achp1 = 0.0_wp
    stat%ave_achp2 = 0.0_wp
    stat%rave_achp1 = 0.0_wp
    stat%rave_achp2 = 0.0_wp
    stat%zum_achp1 = 0.0_wp
    stat%zum_achp2 = 0.0_wp
    stat%flc_achp1 = 0.0_wp
    stat%flc_achp2 = 0.0_wp
    stat%stk_achp1 = 0.0_wp
    stat%stk_achp2 = 0.0_wp

    stat%ideal1 = 0.0_wp
    stat%ideal2 = 0.0_wp

    do k = 1, numsemi

        call findnumtype(ib, semitype1(k), numtype1)
        call findnumtype(ib, semitype2(k), numtype2)

        n1 = real(numtype1,wp) / (real(numtype2,wp) + 1.0_wp)
        n2 = real(numtype2,wp) / (real(numtype1,wp) + 1.0_wp)

        stat%ideal1(k) = log(n1)
        stat%ideal2(k) = log(n2)

    enddo

end subroutine

subroutine zero_zdens(stat, npoints)

    use kinds_f90
    use statistics_type
    use species_module, only : number_of_elements

    implicit none

    integer, intent(in) :: npoints

    type(statistics), intent(inout) :: stat

    integer :: fail, n

    fail = 0 
    stat%nsamp_zden = 0

    stat%zden_points = npoints

    stat%zden_r = 1.0_wp / real(npoints,wp)

    n = npoints + 1

    allocate (stat%zdens(number_of_elements, n), stat = fail)

    if (fail > 0) call error(237)

    stat%zdens = 0.0_wp

end subroutine

subroutine zero_rdf(stat, npoints, rmax)

    use kinds_f90
    use cell_module, only: myjob
    use constants_module, only : uout
    use statistics_type
    use species_module, only : number_of_elements, number_of_molecules

    implicit none

    integer, intent(in) :: npoints
    real (kind = wp), intent(in) :: rmax
    type(statistics), intent(inout) :: stat

    integer :: fail(3), n, maxint, mmxint

    fail = 0
    stat%nsamp_rdf = 0

    stat%rdf_points = npoints
    stat%rdf_max = rmax

    stat%rdf_r = rmax / real(npoints,wp)

    !account for all pairs of element/molecule types
    maxint = (number_of_elements * (number_of_elements + 1)) / 2
    mmxint = (number_of_molecules * (number_of_molecules + 1)) / 2

    n = npoints + 1

    allocate (stat%rdfs(maxint, n), stat = fail(1))
    allocate (stat%mrdf(mmxint, n), stat = fail(2))

    if( myjob%usecavitybias ) &
        allocate (stat%crdf(number_of_elements, n), stat = fail(3))

    if (any(fail > 0)) call error(237)

    stat%rdfs = 0.0_wp
    stat%mrdf = 0.0_wp
    if( myjob%usecavitybias ) stat%crdf = 0.0_wp

end subroutine

subroutine checkpointtypes(stat, nstep, nseql, moldata)

    use kinds_f90
    use constants_module, only : uout
    use statistics_type
    use species_module, only : number_of_elements, number_of_molecules, element, uniq_mol, eletype, set_species_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: nseql, nstep
    logical, intent(in) :: moldata

    type(statistics), intent(inout) :: stat

    integer :: i
    character*1 tag

    if (.not.master) return

    write(uout,"(1x,'          ')")

    do i = 1, number_of_elements

        tag = set_species_type(eletype(i))
        write(uout,"(1x,a8,1x,a1,2(1x,f15.4))") element(i), tag, stat%stp_nspc(i), stat%rave_nspc(i)

    enddo

    do i = 1, number_of_molecules
        write(uout,"(/,1x,a8,2(1x,f15.4))") uniq_mol(i)%molname, stat%stp_nmol(i), stat%rave_nmol(i)
        if(moldata) then
            write(uout,'(11x,4e20.10,/,11x,4e20.10,/,11x,4e20.10)')stat%stp_emol(:,i)
            write(uout,'(11x,4e20.10)')stat%rave_emol(:,i)
        endif
    enddo

    write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")

    !report end of equilibration period

    if(nstep == nseql) then
        write(uout,*)" equilibration period ended at step " , nstep
        write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")
    endif

end subroutine

subroutine checkpoint_semiwidom(stat, semitype1, semitype2, numsemi, temp)

    use kinds_f90
    use constants_module, only : uout, BOLTZMAN
    use statistics_type
    use species_module, only : element, eletype, set_species_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: numsemi, semitype1(numsemi), semitype2(numsemi)
    real(kind = wp), intent(in) :: temp

    type(statistics), intent(inout) :: stat

    integer :: i, typ1, typ2
    real(kind = wp) :: mu1, rmu1, mu2, rmu2, fact

    character*1, tag1, tag2

    if (.not.master) return

    fact = BOLTZMAN * temp * print_unit

    do i = 1, numsemi
        typ1 = semitype1(i)
        typ2 = semitype2(i)

        tag1 = set_species_type(eletype(typ1))
        tag2 = set_species_type(eletype(typ2))

        mu1 = 0.0_wp
        mu2 = 0.0_wp
        rmu1 = 0.0_wp
        rmu2 = 0.0_wp

        if (stat%stp_achp1(i) /= 0.0_wp .and. stat%stp_achp2(i) /= 0.0_wp) then

            mu1 = (-log(stat%stp_achp1(i)) - stat%ideal1(i)) * fact
            mu2 = (-log(stat%stp_achp2(i)) - stat%ideal2(i)) * fact
    
            rmu1 = (-log(stat%rave_achp1(i)) - stat%ideal1(i)) * fact
            rmu2 = (-log(stat%rave_achp2(i)) - stat%ideal2(i)) * fact

        endif

        write(uout,"(/,1x,'chem potential ',a8,1x,a1,1x,'to',1x,a8,1x,a1,2(1x,f10.4))")element(typ1), tag1, element(typ2), &
                           tag2, mu1, rmu1
        write(uout,"(1x,'chem potential ',a8,1x,a1,1x,'to',1x,a8,1x,a1,2(1x,f10.4))")element(typ2), tag1, element(typ1), &
                           tag2, mu2, rmu2
    enddo

end subroutine

subroutine checkpoint_semigrand(stat, semitype1, semitype2, numsemi, temp)

    use kinds_f90
    use constants_module, only : uout, BOLTZMAN
    use statistics_type
    use species_module, only : element, eletype, set_species_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: numsemi, semitype1(numsemi), semitype2(numsemi)
    real(kind = wp), intent(in) :: temp

    type(statistics), intent(inout) :: stat

    integer :: i, typ1, typ2
    real(kind = wp) :: tmp1, tmp2, rtmp1, rtmp2, fact

    character*1, tag1, tag2

    if (.not.master) return

    fact = BOLTZMAN * temp * print_unit

    do i = 1, numsemi
        typ1 = semitype1(i)
        typ2 = semitype2(i)

        tag1 = set_species_type(eletype(typ1))
        tag2 = set_species_type(eletype(typ2))

        if (stat%stp_achp1(i) /= 0.0_wp .and. stat%stp_achp2(i) /= 0.0_wp) then

            tmp1 = stat%stp_achp1(i) * fact
            tmp2 = stat%stp_achp2(i) * fact

            rtmp1 = stat%rave_achp1(i) * fact
            rtmp2 = stat%rave_achp2(i) * fact

        endif

        write(uout,"(/,1x,'diff potential ',a8,1x,a1,1x,'to',1x,a8,1x,a1,2(1x,f10.4))")element(typ1), tag1, element(typ2), &
                           tag2, tmp1, rtmp1
        write(uout,"(1x,'diff potential ',a8,1x,a1,1x,'to',1x,a8,1x,a1,2(1x,f10.4))")element(typ2), tag1, element(typ1), &
                           tag2, tmp2, rtmp2
    enddo

end subroutine

subroutine lastsummary_semiwidom(ib, stat, numsemi, semitype1, semitype2, temp)

    use kinds_f90
    use constants_module, only : uout, BOLTZMAN
    use statistics_type
    use species_module, only : element, eletype, set_species_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: ib, numsemi, semitype1(numsemi), semitype2(numsemi)
    real(kind = wp), intent(in) :: temp

    type(statistics), intent(inout) :: stat

    integer :: i, typ1, typ2
    real(kind = wp) :: mu1, mu2, fact

    character*1, tag1, tag2

    if (.not.master) return

    fact = BOLTZMAN * temp * print_unit

    do i = 1, numsemi
        typ1 = semitype1(i)
        typ2 = semitype2(i)

        tag1 = set_species_type(eletype(typ1))
        tag2 = set_species_type(eletype(typ2))

        mu1 = (-log(stat%ave_achp1(i)) - stat%ideal1(i)) * fact
        mu2 = (-log(stat%ave_achp2(i)) - stat%ideal2(i)) * fact

        write(uout,"(/,1x,a8,1x,a1,1x,'to',1x,a8,1x,a1,1(1x,f10.4))")element(typ1), tag1, element(typ2), tag2, &
                                  mu1
        write(uout,"(1x,a8,1x,a1,1x,'to',1x,a8,1x,a1,1(1x,f10.4))")element(typ2), tag1, element(typ1), tag2, &
                                  mu2
    enddo    

end subroutine

subroutine lastsummary_semigrand(ib, stat, numsemi, semitype1, semitype2, temp)

    use kinds_f90
    use constants_module, only : uout, BOLTZMAN
    use statistics_type
    use species_module, only : element, eletype, set_species_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: ib, numsemi, semitype1(numsemi), semitype2(numsemi)
    real(kind = wp), intent(in) :: temp

    type(statistics), intent(inout) :: stat

    integer :: i, typ1, typ2
    real(kind = wp) :: mu1, mu2, fact

    character*1, tag1, tag2

    if (.not.master) return

    fact = BOLTZMAN * temp * print_unit

    do i = 1, numsemi
        typ1 = semitype1(i)
        typ2 = semitype2(i)

        tag1 = set_species_type(eletype(typ1))
        tag2 = set_species_type(eletype(typ2))

        mu1 = stat%ave_achp1(i) * fact
        mu2 = stat%ave_achp2(i) * fact

        write(uout,"(/,1x,a8,1x,a1,1x,'to',1x,a8,1x,a1,1(1x,f10.4))")element(typ1), tag1, element(typ2), tag2, &
                                  mu1
        write(uout,"(1x,a8,1x,a1,1x,'to',1x,a8,1x,a1,1(1x,f10.4))")element(typ2), tag1, element(typ1), tag2, &
                                  mu2
    enddo

end subroutine



subroutine sampletypes(ib, iter, stat, equil, emoltot, emolrcp, emolrealnonb, emolrealbond, emolrealself, &
                       emolrealcrct, emolvdwn, emolvdwb, emolpair, emolthree, emolang, emolfour, emolmany, &
                       emolext)

    use kinds_f90
    use statistics_type
    use species_module, only : number_of_elements, number_of_molecules
    use cell_module, only : cfgs

    implicit none

    integer, intent(in) :: ib, equil, iter
    real (kind = wp), intent(in) :: emoltot(:), emolrcp(:), emolrealnonb(:), emolrealbond(:), emolvdwn(:), &
                                    emolvdwb(:), emolpair(:), emolthree(:), emolang(:), emolfour(:), emolmany(:), &
                                    emolrealself(:), emolrealcrct(:), emolext(:)

    type(statistics), intent(inout) :: stat

    integer i, kstk, num, j, typ
    real(kind = wp) ::  sclnv1, sclnv2, tmp(nstk)


    do i = 1, number_of_elements
        stat%stp_nspc(i) = 0.0_wp
    enddo

    do i = 1, cfgs(ib)%num_mols

        do j = 1, cfgs(ib)%mols(i)%natom

            typ = cfgs(ib)%mols(i)%atms(j)%atlabel
            stat%stp_nspc(typ) = stat%stp_nspc(typ) + 1.0

        enddo

    enddo

    do i = 1, number_of_molecules
        num = cfgs(ib)%mtypes(i)%num_mols
        stat%stp_nmol(i) = real(num,wp)
    enddo

    do i = 1, number_of_molecules

        stat%stp_emol(1,i) = emoltot(i) * print_unit
        stat%stp_emol(2,i) = emolrcp(i) * print_unit
        stat%stp_emol(3,i) = emolrealnonb(i) * print_unit
        stat%stp_emol(4,i) = emolrealbond(i) * print_unit
        stat%stp_emol(5,i) = emolrealself(i) * print_unit
        stat%stp_emol(6,i) = emolrealcrct(i) * print_unit
        stat%stp_emol(7,i) = emolvdwn(i) * print_unit
        stat%stp_emol(8,i) = emolvdwb(i) * print_unit
        stat%stp_emol(9,i) = emolthree(i) * print_unit
        stat%stp_emol(10,i) = emolpair(i) * print_unit
        stat%stp_emol(11,i) = emolang(i)  * print_unit
        stat%stp_emol(12,i) = emolfour(i) * print_unit
        stat%stp_emol(13,i) = emolmany(i) * print_unit
        stat%stp_emol(14,i) = emolext(i) * print_unit

    enddo

    sampletyp = sampletyp + 1

    kstk = mod((sampletyp - 1),nstk) + 1

    if(sampletyp > nstk) then
        if(kstk == 1) then

            do i = 1, number_of_elements
               tmp(:) = stat%stk_nspc(:,i)
               call scalsum(nstk, tmp, stat%zum_nspc(i))
            enddo

            do i = 1, number_of_molecules
                call scalsum(nstk, stat%stk_nmol(:,i), stat%zum_nmol(i))
            enddo

            do i = 1, number_of_molecules
                do j = 1, mxeng
                    call scalsum(nstk, stat%stk_emol(:,j,i), stat%zum_emol(j,i))
                enddo
            enddo
        endif

        stat%zum_nspc(:) = stat%zum_nspc(:) - stat%stk_nspc(kstk,:)
        stat%zum_nmol(:) = stat%zum_nmol(:) - stat%stk_nmol(kstk,:)

        do i = 1, mxeng
            stat%zum_emol(i,:) = stat%zum_emol(i,:) - stat%stk_emol(kstk,i,:)
        enddo
    endif

    stat%stk_nspc(kstk, :) = stat%stp_nspc(:)
    stat%zum_nspc(:) = stat%zum_nspc(:) + stat%stp_nspc(:)

    stat%stk_nmol(kstk, :) = stat%stp_nmol(:)
    stat%zum_nmol(:) = stat%zum_nmol(:) + stat%stp_nmol(:)

    do i = 1, mxeng
        stat%stk_emol(kstk, i,:) = stat%stp_emol(i,:)
        stat%zum_emol(i,:) = stat%zum_emol(i,:) + stat%stp_emol(i,:)
    enddo

    ! calculate rolling averages

    zistk = min(nstk, sampletyp)

    stat%rave_nspc(:) = stat%zum_nspc(:) / zistk
    stat%rave_nmol(:) = stat%zum_nmol(:) / zistk

    do i = 1, mxeng
        stat%rave_emol(i,:) = stat%zum_emol(i,:) / zistk
    enddo

    !  accumulate totals over steps

    stat%flc_nspc(:) = stat%flc_nspc(:) + stat%stp_nspc(:)**2
    stat%ave_nspc(:) = stat%ave_nspc(:) + stat%stp_nspc(:)

    stat%flc_nmol(:) = stat%flc_nmol(:) + stat%stp_nmol(:)**2
    stat%ave_nmol(:) = stat%ave_nmol(:) + stat%stp_nmol(:)

    do i = 1, mxeng
        stat%flc_emol(i,:) = stat%flc_emol(i,:) + stat%stp_emol(i,:)**2
        stat%ave_emol(i,:) = stat%ave_emol(i,:) + stat%stp_emol(i,:)
    enddo

    !TU**: What is this doing?
    if(sampletyp == equil) then
    !AB: avoiding NaN's in the last summary for nstep <= nequil
        sampletyp = 0
        stat%ave_emol = 0.0_wp
        stat%flc_emol = 0.0_wp
        stat%ave_nmol = 0.0_wp
        stat%ave_nspc = 0.0_wp
        stat%flc_nmol = 0.0_wp
        stat%flc_nspc = 0.0_wp
    endif

    stat%nsamp_typ = sampletyp

end subroutine

subroutine sample_semiwidom_chempot(chem_pot_atm1, chem_pot_atm2, numsemi, iter, equil, stat)

    use kinds_f90
    use statistics_type

    implicit none

    integer, intent(in) :: equil, iter, numsemi
    real (kind = wp), intent(in) :: chem_pot_atm1(numsemi), chem_pot_atm2(numsemi)
    type(statistics), intent(inout) :: stat

    integer i, kstk
    real(kind = wp) ::  sclnv1, sclnv2


    do i = 1, numsemi
        stat%stp_achp1(i) = chem_pot_atm1(i)
        stat%stp_achp2(i) = chem_pot_atm2(i)

    enddo

    kstk = mod((iter - 1),nstk) + 1

    if(iter > nstk) then
        if(kstk == 1) then

            do i = 1, numsemi
                call scalsum(nstk, stat%stk_achp1(:,i), stat%zum_achp1(i))
                call scalsum(nstk, stat%stk_achp2(:,i), stat%zum_achp2(i))
            enddo

        endif

        stat%zum_achp1(:) = stat%zum_achp1(:) - stat%stk_achp1(kstk,:)
        stat%zum_achp2(:) = stat%zum_achp2(:) - stat%stk_achp2(kstk,:)
    endif

    do i = 1, numsemi
        stat%stk_achp1(kstk, i) = stat%stp_achp1(i)
        stat%zum_achp1(i) = stat%zum_achp1(i) + stat%stp_achp1(i)
        stat%stk_achp2(kstk, i) = stat%stp_achp2(i)
        stat%zum_achp2(i) = stat%zum_achp2(i) + stat%stp_achp2(i)
    enddo

    zistk = min(nstk, iter)

    do i = 1, numsemi
        stat%rave_achp1(i) = stat%zum_achp1(i) / zistk
        stat%rave_achp2(i) = stat%zum_achp2(i) / zistk
    enddo

    !  accumulate totals over steps
 
    sampletrans = sampletrans + 1
    sclnv1 = real(sampletrans-1,wp)/real(sampletrans,wp)
    sclnv2 = 1.0_wp/real(sampletrans,wp)
    do i = 1, numsemi
        stat%flc_achp1(i) = sclnv1 * (stat%flc_achp1(i) + sclnv2 * &
                    (stat%stp_achp1(i) - stat%ave_achp1(i))**2)
        stat%ave_achp1(i) = sclnv1 * stat%ave_achp1(i) + sclnv2 * stat%stp_achp1(i)
        stat%flc_achp2(i) = sclnv1 * (stat%flc_achp2(i) + sclnv2 * &
                    (stat%stp_achp2(i) - stat%ave_achp2(i))**2)
        stat%ave_achp2(i) = sclnv1 * stat%ave_achp2(i) + sclnv2 * stat%stp_achp2(i)
    enddo

    if(iter == equil) then
    !AB: avoiding NaN's in the last summary for nstep <= nequil
        sampletrans = 0
        stat%ave_achp1 = 0.0_wp
        stat%ave_achp2 = 0.0_wp
        stat%flc_achp1 = 0.0_wp
        stat%flc_achp2 = 0.0_wp
    endif

    stat%nsamp_wdm = sampletrans

end subroutine


!TU*: Does not ignore equilibration in sampling!
subroutine samplestats(ib, nequil, nstep, energytot, enthalpytot, energyrcp, energyreal, &
                     energyvdw, energythree, energypair, energyang, energyfour, energymany, &
                     energyext, energymfa, virialtot, volume, stat)

    use kinds_f90
    use statistics_type
    use constants_module
    use latticevectors_module, only : dcell
    use cell_module, only : cfgs

    implicit none

    integer, intent(in) :: ib, nequil, nstep
    real (kind = wp), intent(in) :: energytot, enthalpytot, energyrcp, energyreal, &
                     energyvdw, energythree, energypair, energyang, energyfour, energymany, &
                     energyext, energymfa, virialtot, volume

    type(statistics), intent(inout) :: stat

    integer         :: i, kstk
    real(kind = wp) :: sclnv1, sclnv2 !, stpprs
    real(kind = wp) :: bbb(10)

    ! step values of all thermodynamic parameters
    !stpprs = (2.0 * stptke - stpvir) / (3.0 * volume)
    !stpprs = stpprs * PRSFACT

    stat%stpeng(1) = energytot * print_unit
    stat%stpeng(2) = enthalpytot * print_unit
    stat%stpeng(3) = energyrcp * print_unit
    stat%stpeng(4) = energyreal * print_unit
    stat%stpeng(5) = energyvdw * print_unit
    stat%stpeng(6) = energythree * print_unit
    stat%stpeng(7) = energypair * print_unit
    stat%stpeng(8) = energyang  * print_unit
    stat%stpeng(9) = energyfour * print_unit
    stat%stpeng(10) = energymany * print_unit
    stat%stpeng(11) = energyext * print_unit
    stat%stpeng(12) = energymfa * print_unit
    stat%stpeng(13) = volume 

    !lattice vectors
    call dcell(cfgs(ib)%vec%latvector, bbb)

    stat%stpeng(14) = bbb(1)
    stat%stpeng(15) = bbb(2)
    stat%stpeng(16) = bbb(3)
    stat%stpeng(17) = bbb(4)
    stat%stpeng(18) = bbb(5)
    stat%stpeng(19) = bbb(6)

    stat%stpeng(20) = stat%stpeng(1) * stat%stpeng(1)
    stat%stpeng(21) = stat%stpeng(2) * stat%stpeng(2)

    sample = sample + 1

    ! store quantities in stack

    kstk = mod((sample - 1),nstk) + 1

    if(sample > nstk) then
        if(kstk == 1) then

            do i = 1, mxeng
                call scalsum(nstk, stat%stkeng(:,i), stat%zumeng(i))
            enddo
        endif

        do i = 1, mxeng
            stat%zumeng(i) = stat%zumeng(i) - stat%stkeng(kstk,i)
        enddo
    endif

    do i = 1, mxeng
        stat%stkeng(kstk, i) = stat%stpeng(i)
        stat%zumeng(i) = stat%zumeng(i) + stat%stpeng(i)
    enddo

    ! calculate rolling averages

    zistk = min(nstk, sample)

    do i = 1, mxeng
        stat%raveeng(i) = stat%zumeng(i) / zistk
    enddo

    !  accumulate totals over steps

    do i = 1, mxeng
        stat%flceng(i) = stat%flceng(i) + stat%stpeng(i)**2
        stat%aveeng(i) = stat%aveeng(i) + stat%stpeng(i)
    enddo

    !TU**: What is this doing?
    if(sample == nequil) then
    !AB: avoiding NaN's in the last summary for nstep <= nequil
        sample=0
        stat%aveeng = 0.0_wp
        stat%flceng = 0.0_wp
    endif

    stat%nsample = sample

end subroutine

subroutine lastsummary(ib, stat)

    use kinds_f90
    use constants_module, only : uout
    use statistics_type
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    integer, intent(in) :: ib
    type(statistics), intent(inout) :: stat

    integer :: i, samples

    if (.not.master) return

    samples = sample
    if(samples==0) then
    !AB: avoiding NaN's in the last summary
        samples = 1
        write(uout,"(1x,'WARNING: no samples collected!!! - equilibration not finished?')")
    endif

    do i = 1, mxeng

        stat%aveeng(i) = stat%aveeng(i) / real(samples,wp)
        stat%flceng(i) = stat%flceng(i) / real(samples,wp) - stat%aveeng(i)**2

        if(stat%flceng(i) <= 0.0_wp) then
            stat%flceng(i) = 0.0_wp
        else
            stat%flceng(i) = sqrt(stat%flceng(i))
        endif

    enddo

    write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")
    write(uout,*)"     avrg      en-total            h-total             coul-rcp            coul-real"
    write(uout,*)"     avrg      en-vdw              en-three            en-pair             en-angle "
    write(uout,*)"     avrg      en-four             en-many             en-external         en-extMFA"
!    write(uout,*)"     avrg      vir-tot             pressure "
    write(uout,*)"     flct      en-total            h-total             coul-rcp            coul-real"
    write(uout,*)"     flct      en-vdw              en-three            en-pair             en-angle "
    write(uout,*)"     flct      en-four             en-many             en-external         en-extMFA"
!    write(uout,*)"     flct      vir-tot             pressure "
    write(uout,"(1x,'----------------------------------------------------------------------------------------------------')")

    write(uout,'(i10,1x,4e20.10,/,11x,4e20.10,/,11x,4e20.10,/,11x,4e20.10)')samples, (stat%aveeng(i), i = 1,12)
    write(uout,'(11x,4e20.10)')(stat%flceng(i), i = 1,12) 

    write(uout,"(///,1x,'average cell parameters and fluctuations',/)")
    write(uout,"(10x,'vol   = ',2e20.10)")stat%aveeng(13),stat%flceng(13)
    write(uout,"(10x,'a     = ',2f20.10)")stat%aveeng(14),stat%flceng(14)
    write(uout,"(10x,'b     = ',2f20.10)")stat%aveeng(15),stat%flceng(15)
    write(uout,"(10x,'c     = ',2f20.10)")stat%aveeng(16),stat%flceng(16)
    write(uout,"(10x,'alpha = ',2f20.10)")stat%aveeng(17),stat%flceng(17)
    write(uout,"(10x,'beta  = ',2f20.10)")stat%aveeng(18),stat%flceng(18)
    write(uout,"(10x,'gamma = ',2f20.10)")stat%aveeng(19),stat%flceng(19)

end subroutine

subroutine lastsummarytypes(ib, stat, moldata)
    use statistics_type
    use species_module, only : number_of_elements, number_of_molecules, element, uniq_mol, eletype, set_species_type
    use constants_module, only : uout
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: ib
    type(statistics), intent(inout) :: stat
    logical, intent(in) :: moldata

    integer :: i, j, samples
    character*1 tag

    if (.not.master) return

    samples = sampletyp
    if(samples==0) then
    !AB: avoiding NaN's in the last summary
        samples = 1

        !write(uout,*)' '
        !write(uout,"(1x,'WARNING: no samples collected!!! - equilibration not finished?')")
    endif

    write(uout,*)'   '

    do i = 1, number_of_elements

        tag = set_species_type(eletype(i))

        !TU**: Should 'samples' here actually be 'sampletyp'? Doesn't matter because they are the same 
        !TU**: number in practice - types and energy are sampled with the same frequency
        stat%ave_nspc(i) = stat%ave_nspc(i) / real(samples,wp)
        stat%flc_nspc(i) = stat%flc_nspc(i) / real(samples,wp) - stat%ave_nspc(i)**2

        if(stat%flc_nspc(i) <= 0.0_wp) then
            stat%flc_nspc(i) = 0.0_wp
         else
            stat%flc_nspc(i) = sqrt(stat%flc_nspc(i))
        endif

        write(uout,"(1x,a8,1x,a1,2(1x,f15.4))") element(i), tag, stat%ave_nspc(i), stat%flc_nspc(i)

    enddo

    do i = 1, number_of_molecules

        stat%ave_nmol(i) = stat%ave_nmol(i) / real(samples,wp)
        stat%flc_nmol(i) = stat%flc_nmol(i) / real(samples,wp) - stat%ave_nmol(i)**2
        
        if(stat%flc_nmol(i) <= 0.0_wp) then
           stat%flc_nmol(i) = 0.0_wp
        else           
           stat%flc_nmol(i) = sqrt(stat%flc_nmol(i))
        endif

        do j = 1, mxeng
            stat%ave_emol(j,i) = stat%ave_emol(j,i) / real(samples,wp)
            stat%flc_emol(j,i) = stat%flc_emol(j,i) / real(samples,wp) - stat%ave_emol(j,i)**2

            if(stat%flc_emol(j,i) <= 0.0_wp) then
                stat%flc_emol(j,i) = 0.0_wp                
            else
                stat%flc_emol(j,i) = sqrt(stat%flc_emol(j,i))
            endif
        enddo

        write(uout,"(/,1x,a8,2(1x,f15.4))") uniq_mol(i)%molname, stat%ave_nmol(i), stat%flc_nmol(i)
        if(moldata) then
            write(uout,'(11x,4e20.10,/,11x,4e20.10,/,11x,4e20.10)')stat%ave_emol(:,i)
            write(uout,'(11x,4e20.10)')stat%flc_emol(:,i)
            write(uout,"(1x,'-------------------------------------------------------------------------------------------')")
        endif

    enddo

end subroutine


subroutine checkpoint(nsbpo, lines, npage, nstep, stat)

    use kinds_f90
    use constants_module, only : uout
    use statistics_type
    use comms_mpi_module, only : master

    implicit none

    type(statistics), intent(inout) :: stat

    integer, intent(in) :: nsbpo, lines, npage, nstep

    integer :: i

    if (.not.master) return

    if(lines == 0.or.mod(nstep,nsbpo) == 0) then

        if(mod(lines,npage) == 0) then
            write(uout,"(1x,100('='))")
            write(uout,*)"     step      en-total            h-total             coul-rcp            coul-real"
            write(uout,*)"     step      en-vdw              en-three            en-pair             en-angle "
            write(uout,*)"     step      en-four             en-many             en-external         en-extMFA"
            write(uout,*)"     step      volume              cell-a              cell-b              cell-c   "
            write(uout,*)"     step      alpha               beta                gamma "
            write(uout,*)"     r-av      en-total            h-total             coul-rcp            coul-real"
            write(uout,*)"     r-av      en-vdw              en-three            en-pair             en-angle "
            write(uout,*)"     r-av      en-four             en-many             en-external         en-extMFA"
            write(uout,*)"     r-av      volume              cell-a              cell-b              cell-c   "
            write(uout,*)"     r-av      alpha               beta                gamma "
            write(uout,"(1x,100('-'))")
        endif
        write(uout,'(i10,1x,4e20.10,/,11x,4e20.10,/,11x,4e20.10)')nstep, (stat%stpeng(i), i = 1, 12)
        write(uout,'(11x,4e20.10)') (stat%stpeng(i), i = 13, 19)
        write(uout,'(/)')
        write(uout,'(11x,4e20.10)') (stat%raveeng(i), i = 1, 12)
        write(uout,'(11x,4e20.10)') (stat%raveeng(i), i = 13, 19)
    endif

end subroutine

subroutine corout(nstep, stat)

    use kinds_f90
    use constants_module
    use statistics_type
    use species_module, only : number_of_molecules, number_of_elements
    use comms_mpi_module, only : master

    implicit none

    integer, intent(in) :: nstep
    type(statistics), intent(inout) :: stat

    integer :: i, j

    !AB: avoid too small numbers in exponential representation (see the formats below)

    do i=1,mxeng
       if( abs(stat%stpeng(i)) < DZERO ) stat%stpeng(i) = 0.0_wp
    enddo

    do i=1,number_of_elements
       if( abs(stat%stp_nspc(i)) < DZERO ) stat%stp_nspc(i) = 0.0_wp
    enddo

    do i=1,number_of_molecules
       if( abs(stat%stp_nmol(i)) < DZERO ) stat%stp_nmol(i) = 0.0_wp
       do j=1,mxeng
          if( abs(stat%stp_emol(j,i)) < DZERO ) stat%stp_emol(j,i) = 0.0_wp
       enddo
    enddo

    !AB: only master(s) write into files

    if( master ) then

        write(uplt, '(i10)') nstep
        write(uplt, '(6e16.6)')(stat%stpeng(i),i=1,mxeng)

        write(uplt, '(6f16.6)')(stat%stp_nspc(i),i=1,number_of_elements)
        write(uplt, '(6f16.6)')(stat%stp_nmol(i),i=1,number_of_molecules)

        do i = 1, number_of_molecules

            write(uplt, '(6e16.6)')(stat%stp_emol(j,i), j = 1, mxeng)

        enddo

    endif

end subroutine

subroutine sample_zdensity(ib, stat)

    use kinds_f90
    use constants_module, only : uout, HALF, DHALF
    use statistics_type
    use cell_module, only : myjob, cfgs, carttofrac, fractocart
    use slit_module, only : im_bulk, in_bulk

    implicit none

    integer, intent(in) :: ib

    type(statistics), intent(inout) :: stat

    real(kind = wp) :: zpos, rx, ry, rz, zcom, rcom(3)
    integer :: i, im, typ, n
    logical :: is_subset
    
    is_subset = ( len_trim(adjustL(myjob%zcom_molids)) > 0 )

    zcom    = 0.0_wp
    rcom(:) = 0.0_wp

    if( myjob % lzcomzero ) then

        n = 0
        do im = 1, cfgs(ib)%num_mols
            if( is_subset .and. &
                index( myjob%zcom_molids, cfgs(ib)%mols(im)%molname ) < 1 ) cycle

            rcom(:) = rcom(:) + cfgs(ib)%mols(im)%rcom(:)
            n = n + 1
        enddo

        rcom(:) = rcom(:) / real(n,wp)
        zcom    = rcom(3)

!        if( cfgs(ib)%vec%is_orthogonal ) then
!            zcom = rcom(3)/cfgs(ib)%vec%latvector(3,3)
!        else 
!            zcom = cfgs(ib)%vec%invlat(1,3) * rcom(1) &
!                 + cfgs(ib)%vec%invlat(2,3) * rcom(2) &
!                 + cfgs(ib)%vec%invlat(3,3) * rcom(3)
!        end if

    end if

    do im = 1, cfgs(ib)%num_mols

        do i = 1 , cfgs(ib)%mols(im)%natom

            typ = cfgs(ib)%mols(im)%atms(i)%atlabel

            if( cfgs(ib)%vec%is_orthogonal ) then

                zpos = (cfgs(ib)%mols(im)%atms(i)%rpos(3) - zcom) &
                     / cfgs(ib)%vec%latvector(3,3)

            else

                rx = cfgs(ib)%mols(im)%atms(i)%rpos(1)
                ry = cfgs(ib)%mols(im)%atms(i)%rpos(2)
                rz = cfgs(ib)%mols(im)%atms(i)%rpos(3) - zcom

                zpos = cfgs(ib)%vec%invlat(1,3) * rx &
                     + cfgs(ib)%vec%invlat(2,3) * ry &
                     + cfgs(ib)%vec%invlat(3,3) * rz

            end if

            !AB: applying PBC
            if( abs(zpos) > HALF ) zpos = zpos - anint(zpos)*im_bulk

            zpos = zpos + HALF

            n = nint(zpos * real(stat%zden_points,wp) - HALF ) + 1
            !n = int(zpos * real(stat%zden_points,wp) ) + 1

#ifdef DEBUG
            !AB: debugging
            if( n < 1 ) then
                call cry(uout,'',"WARNING: Z-bin outside the cell, < 0 !!!",0)
                n = stat%zden_points+1
            else if( n > stat%zden_points ) then
                call cry(uout,'',"WARNING: Z-bin outside the cell, > Nmax !!!",0)
                n = stat%zden_points+1
            else
                stat%zdens(typ, n) = stat%zdens(typ, n) + 1.0_wp
            end if
#else
            if( n < 1 .or. n > stat%zden_points ) n = stat%zden_points+1
#endif

            stat%zdens(typ, n) = stat%zdens(typ, n) + 1.0_wp

        enddo

    enddo

    stat%nsamp_zden = stat%nsamp_zden + 1

#ifdef DEBUG
     !AB: debugging
    if( stat%nsamp_zden == huge(stat%nsamp_zden) ) &
        call cry(uout,'(/,1x,a)', &
                "ERROR: 'integer' overflow - number of Z-density samples > upper bound for 'integer'!!!",999)
#endif

end subroutine

subroutine print_zdens(ib, iter, stat)

    use kinds_f90
    use statistics_type
    use constants_module, only : uzdn, uout, HALF
    use species_module, only : number_of_elements, element
    use cell_module, only : cfgs, findnumtype
    use latticevectors_module, only : dcell
    use comms_mpi_module, only : master

    implicit none

    integer, intent(in) :: ib, iter

    type(statistics), intent(inout) :: stat

    real(kind = wp) :: dens(number_of_elements), celprp(10)
    real(kind = wp) :: vol, zz, tmp, gsum, rvol

    integer :: numtyp, typ, i, n

    character(4) :: atype

    if (.not.master) return

    write(uzdn,"('# ',a,/,'# Number density along Z axis in cell ',i4,'   at MC step ',i10)") &
          cfgs(ib)%cfg_title, ib,iter

    !if( sample == 0 .or. stat%nsamp_zden == 0 ) then
    if( stat%nsamp_zden == 0 ) then

        write(uzdn,"('#',/,'# No data - number of samples taken is zero')")
        return

    endif

    call dcell(cfgs(ib)%vec%latvector, celprp)

    write(uzdn,"(a,i10,2f24.10)")'# Grid: ',stat%zden_points,celprp(9),stat%zden_r*celprp(9)

    vol = celprp(10)

    if( sample > 1 ) vol = stat%aveeng(13) / real(sample,wp) !average volume

    do i = 1, number_of_elements

       if( sampletyp > 1 ) then

           !AB: averages in GCMC
           dens(i) = stat%ave_nspc(i) / (vol*real(sampletyp,wp))

       else

           call findnumtype(ib, i, numtyp)
           dens(i) = real(numtyp,wp) / vol

       endif

       write(uzdn,"('#',/,'# Number density for element',2x,a8,' (',i4,') : ',e15.7,' = ',f15.5,' / ',e15.7)") &
             element(i),i,dens(i),dens(i)*vol,vol

    enddo

    rvol = real(stat%zden_points,wp)/vol

    do typ = 1, number_of_elements

        write(uzdn,"(/,'# Z-density for element: ',i4)") typ
        write(atype,"(i4)") typ

        zz = -HALF

        gsum = 0.0_wp

        do n = 1, stat%zden_points

            tmp  = stat%zdens(typ, n) / real(stat%nsamp_zden,wp)
            gsum = gsum+tmp
            write(uzdn,"(4(' ',e15.7))") (zz + HALF*stat%zden_r)*celprp(9), tmp, tmp*rvol, gsum

            zz = zz + stat%zden_r

        enddo

        if( stat%zdens(typ, stat%zden_points+1) > 0 ) &
            call cry(uout,'',"WARNING: Z-density data outside the cell for element "//atype//" !!!",0)

    enddo

end subroutine

subroutine sample_rdfs(ib, stat, exclude_intra)

    use kinds_f90
    use statistics_type
    use cell_module !, only : cfgs, pbc_atom_pos_calc
    use parse_module, only : int_2_word, real_2_word
    use constants_module, only : DZERO, FZERO, uout

    implicit none

    integer, intent(in) :: ib

    type(statistics), intent(inout) :: stat

    logical, intent(in) :: exclude_intra

    real(kind = wp) :: rcut2, rdr_i, rsq, rrr, rmax, rcav2, cavfi
    real(kind = wp) :: ax, ay, az, bx, by, bz
    real(kind = wp) :: xx, yy, zz, rx, ry, rz

    integer :: i, im, j, jm, jb, typi, typj, n, k, ii, jj, nmax, nbcav

    rcut2 = stat%rdf_max*stat%rdf_max

    rdr_i = 1.0_wp / stat%rdf_r

    rcav2 = 0.0_wp
    cavfi = 0.0_wp
    if( myjob%usecavitybias .and. ncavity_free(ib) > 0 ) then
        rcav2 = myjob%cavity_radius**2
        cavfi = 1.0_wp/ncavity_free(ib)
    endif

    rmax = 0.0_wp
    nmax = 0

    do im = 1, cfgs(ib)%num_mols

         do i = 1, cfgs(ib)%mols(im)%natom

!AB: skip fictitious atoms if any (set mass to zero)
            if( cfgs(ib)%mols(im)%atms(i)%mass < FZERO ) cycle

            ax = cfgs(ib)%mols(im)%atms(i)%rpos(1)
            ay = cfgs(ib)%mols(im)%atms(i)%rpos(2)
            az = cfgs(ib)%mols(im)%atms(i)%rpos(3)

            typi = cfgs(ib)%mols(im)%atms(i)%atlabel

            ii = cfgs(ib)%mols(im)%glob_no(i)

            jb = im

            if( exclude_intra ) jb = jb+1

            do jm = jb, cfgs(ib)%num_mols

                do j = 1, cfgs(ib)%mols(jm)%natom

                    jj = cfgs(ib)%mols(jm)%glob_no(j)

                    if (jj <= ii) cycle  !prevent double counting

!AB: skip fictitious atoms if any (set mass to zero)
                    if( cfgs(ib)%mols(jm)%atms(j)%mass < FZERO ) cycle

                    typj = cfgs(ib)%mols(jm)%atms(j)%atlabel

                    bx = cfgs(ib)%mols(jm)%atms(j)%rpos(1)
                    by = cfgs(ib)%mols(jm)%atms(j)%rpos(2)
                    bz = cfgs(ib)%mols(jm)%atms(j)%rpos(3)

                    !calculate distance
                    rx = ax - bx
                    ry = ay - by
                    rz = az - bz

                    call pbc_cart_pos_calc(ib, rx, ry, rz)

                    rsq = rx * rx + ry * ry + rz * rz

                    !if( rsq < rcut2 ) then
                    if( rcut2-rsq > DZERO ) then

                        rrr  = sqrt(rsq)
                        rmax = max(rrr,rmax)

                        n = min(int(rrr*rdr_i),stat%rdf_points)+1

                        nmax = max(n,nmax)

                        k = max(typi,typj)
                        k = k*(k-1)/2 + min(typi,typj)

                        stat%rdfs(k, n) = stat%rdfs(k, n) + 1.0_wp

                    endif

                enddo

            enddo

            if( myjob%usecavitybias ) then

                nbcav = 0

                do j = 1,ncavity_free(ib)! ncavity_grid(ib)

                    if( cfgs(ib)%cavity(cfgs(ib)%cavids(j)) > 0 ) then
                        nbcav = nbcav+1
                        cycle
                    endif

                    bx = cfgs(ib)%cavgrid(1,cfgs(ib)%cavids(j))
                    by = cfgs(ib)%cavgrid(2,cfgs(ib)%cavids(j))
                    bz = cfgs(ib)%cavgrid(3,cfgs(ib)%cavids(j))

                    !calculate distance
                    rx = ax - bx
                    ry = ay - by
                    rz = az - bz

                    call pbc_cart_pos_calc(ib, rx, ry, rz)

                    rsq = rx * rx + ry * ry + rz * rz

                    !if (rsq >= rcut2) cycle
                    !if (rsq <= rcav2) cycle

                    !if( rsq < rcut2 ) then
                    if( rcut2-rsq > DZERO ) then

                        if( rcav2-rsq > DZERO ) cycle

                        rrr  = sqrt(rsq)
                        rmax = max(rrr,rmax)

                        n = min(int(rrr*rdr_i),stat%rdf_points)+1

                        nmax = max(n,nmax)

                        stat%crdf(typi, n) = stat%crdf(typi, n) + cavfi !1.0_wp

                    endif

                enddo

            endif

        enddo

    enddo

    stat%nsamp_rdf = stat%nsamp_rdf + 1

    if( nmax > stat%rdf_points ) &
        call cry(uout,'(/,1x,a)', &
                "WARNING: sample_rdfs() - max bin index > number of rdf_points: "//&
                 trim(int_2_word(nmax))//" (r_max = "//trim(real_2_word(rmax))//" )"//&
                 " > "//trim(int_2_word(stat%rdf_points))//" !!!",0)

    if( myjob%usecavitybias .and. nbcav > 0 ) &
        call cry(uout,'(/,1x,a)', &
                "WARNING: sample_rdfs() - encountered busy cavities "//&
                trim(int_2_word(nbcav))//" on the list of free ones !!!",0)

#ifdef DEBUG
    !AB: debugging
    if( stat%nsamp_rdf == huge(stat%nsamp_rdf) ) &
        call cry(uout,'(/,1x,a)', &
                "ERROR: 'integer' overflow - number of RDF samples > upper bound for 'integer'!!!",999)
#endif

end subroutine

subroutine print_rdfs(ib, iter, stat)

    use kinds_f90
    use statistics_type
    use cell_module, only : myjob, cfgs, findnumtype
    use species_module, only : number_of_elements, element
    use constants_module, only : PI, DZERO, FZERO, urdf, uout
    use latticevectors_module, only : dcell
    use comms_mpi_module, only : master

    implicit none

    integer, intent(in) :: ib, iter

    type(statistics), intent(inout) :: stat

    real(kind = wp) :: dens(number_of_elements), celprp(10)
    real(kind = wp) :: norm, dvol, rrr, gg, vol, gsum, rm, rp

    integer :: i, numtyp, it, jt, k

    character(8) :: atypes

    if (.not.master) return

    write(urdf,"('# ',a,/,'# Radial Distribution Functions in cell ',i4,' at MC step ',i10)") &
          cfgs(ib)%cfg_title, ib,iter

    if( sample == 0 .or. stat%nsamp_rdf == 0 ) then

        write(urdf,"('#',/,'# No data - number of samples taken is zero')")
        return

    endif

    write(urdf,"(a,i10,2f24.10)")'# Grid: ', stat%rdf_points, stat%rdf_max, stat%rdf_r

    call dcell(cfgs(ib)%vec%latvector, celprp)

    vol = celprp(10)

    if( sample > 1 ) vol = stat%aveeng(13) / real(sample,wp)  !average volume

    do i = 1, number_of_elements

       if( sampletyp > 1 ) then

           !AB: averages in GCMC
           dens(i) = stat%ave_nspc(i) / (vol*real(sampletyp,wp))

       else

           call findnumtype(ib, i, numtyp)
           dens(i) = real(numtyp,wp) / vol

       endif

       write(urdf,"('#',/,'# Number density for element',2x,a8,' (',i4,') : ',e15.7,' = ',f15.5,' / ',e15.7)") &
             element(i),i,dens(i),dens(i)*vol,vol

    enddo

    do it = 1, number_of_elements

        do jt = it, number_of_elements

            k = (jt * (jt - 1)) / 2 + it

            if( sum(stat%rdfs(k,:)) < FZERO ) cycle

            !AB: scaling factor for different elements: n_it * n_jt / vol
            norm = vol * dens(it) * dens(jt)

            !AB: scaling factor for same elements: ((n_it-1) * n_it / 2) / vol
            if( it == jt ) norm = (vol*dens(it) - 1.0_wp) * dens(it) * 0.5_wp

            !put no of samples taken in normalisation factor
            norm = norm * real(stat%nsamp_rdf,wp)

            !check all the atoms of a type have not gone (GCMC)
            if( norm < FZERO ) cycle

            write(urdf,"(/,a,i3,a,2i4)") '# RDF No. ',k,' for elements: ',it,jt
            write(atypes,"(2i4)") it,jt

            gsum = 0.0_wp

            do i = 1, stat%rdf_points

                rrr = (real(i,wp)-0.5_wp)*stat%rdf_r

                dvol = 4.0_wp*PI * stat%rdf_r * (rrr**2 + stat%rdf_r**2 / 12.0_wp)

                gg   = stat%rdfs(k,i) / norm
                gsum = gsum + gg*dens(jt)
                gg   = gg / dvol

                write(urdf,"(3(' ',e15.7))") rrr, gg, gsum

            enddo

            if( stat%rdfs(k,stat%rdf_points+1) > 0.0_wp ) then
                write(uout,"(/,a,i3,a,3f16.8,/)") &
                      'print_rdfs(',k,'): *WARNING* - RDF data outside cut-off (half-box?) !!! ', &
                      rrr+stat%rdf_r, stat%rdf_max, stat%rdfs(k,stat%rdf_points+1)
                !call cry(uout,'',"WARNING: RDF data outside the cutoff for elements "//atypes//" !!!",0)
            endif

        enddo

        if( myjob%usecavitybias ) then

            k = k+1

            if( sum(stat%crdf(it,:)) < FZERO ) cycle

            !AB: scaling factor for the element: n_it / vol
            norm = dens(it)

            !put no of samples taken in normalisation factor
            norm = norm * real(stat%nsamp_rdf,wp)

            !check all the atoms of a type have not gone (GCMC)
            if( norm < FZERO ) cycle

            write(urdf,"(/,a,i3,a,2i4)") '# RDF No. ',k,' for free cavities - element: ',it
            write(atypes,"(2i4)") it

            gsum = 0.0_wp

            do i = 1, stat%rdf_points

                rrr = (real(i,wp)-0.5_wp)*stat%rdf_r

                if( stat%crdf(it,i) < FZERO ) cycle

                dvol = 4.0_wp*PI * stat%rdf_r * (rrr**2 + stat%rdf_r**2 / 12.0_wp)

                gg   = stat%crdf(it,i) / norm ! = sum(1/cavfi) * vol / num(it)
                gsum = gsum + gg*dens(it)
                gg   = gg / dvol

                write(urdf,"(3(' ',e15.7))") rrr, gg, gsum

            enddo

            if( stat%crdf(it,stat%rdf_points+1) > 0.0_wp ) then

                !call cry(uout,'',"WARNING: RDF data outside the cutoff "//&
                !        "for free cavities - element "//atypes//" !!!",0)

                write(uout,"(/,a,i3,a,3f16.8,/)") &
                      'print_rdfs(',k,'): *WARNING* - RDF data outside cut-off (half-box?) !!! ', &
                      rrr+stat%rdf_r, stat%rdf_max, stat%crdf(it,stat%rdf_points+1)
            endif

        endif

    enddo

end subroutine

subroutine scalsum(n, a, ssum)

    use kinds_f90

    implicit none

    integer, intent(in) :: n
 
    real(kind = wp), intent(in) :: a(n)

    real(kind = wp), intent(out) :: ssum

    integer :: i

    ssum = 0.0_wp

    do i = 1, n
        ssum = ssum + a(i)
    enddo

end subroutine

!*******************************************************************************************************
!routines to sample the fluctuations in displacements
!*******************************************************************************************************
subroutine zero_displacements(stat, maxno_of_atoms)

    use kinds_f90
    use statistics_type

    implicit none

    integer, intent(in) :: maxno_of_atoms
    type(statistics), intent(inout) :: stat

    integer :: fail(6)

    fail = 0;

    allocate (stat%stp_disp( maxno_of_atoms,  maxno_of_atoms, 3), stat = fail(1))
!   allocate (stat%ave_disp( maxno_of_atoms,  max_nbrs), stat = fail(2))
!   allocate (stat%rave_disp( maxno_of_atoms,  max_nbrs), stat = fail(3))
!   allocate (stat%zum_disp( maxno_of_atoms,  max_nbrs), stat = fail(4))
!   allocate (stat%flc_disp( maxno_of_atoms,  max_nbrs), stat = fail(5))
!   allocate (stat%stk_disp( nstk, maxno_of_atoms,  max_nbrs), stat = fail(6))

    if (any(fail > 0)) call error(239)

    stat%nsamp_disp = 0
    stat%stp_disp = 0

end subroutine

subroutine sample_displacements(sample_config, iter, equil, ibox, stat)

    use kinds_f90
    use statistics_type
    use cell_module, only : cfgs, pbc_atom_pos_calc

    implicit none

    integer, intent(in) :: iter, equil, ibox, sample_config

    type(statistics), intent(inout) :: stat

    integer :: im, i, ii, jm, j, jj, nbr, kstk, max_nbrs

    real (kind = wp) :: dx, dy, dz, sclnv1, sclnv2, xx, yy, zz
    real (kind = wp) :: rx, ry, rz, r, rsq, ax, bx, ay, by, az, bz
    real (kind = wp), allocatable, dimension(:,:) :: disp

    !collect information on the position of the atoms for the first "sample_config" steps

    max_nbrs = cfgs(ibox)%number_of_atoms / 2

    if (iter < sample_config + equil) then

        !reset the zero positions
        do im = 1, cfgs(ibox)%num_mols

            do i = 1, cfgs(ibox)%mols(im)%natom

                cfgs(ibox)%mols(im)%atms(i)%r_zero(:) = cfgs(ibox)%mols(im)%atms(i)%r_zero(:) + cfgs(ibox)%mols(im)%atms(i)%rpos(:)

            enddo

        enddo

        stat%nsamp_disp = stat%nsamp_disp + 1

    else if (iter == sample_config + equil) then

        do im = 1, cfgs(ibox)%num_mols

            do i = 1, cfgs(ibox)%mols(im)%natom

                cfgs(ibox)%mols(im)%atms(i)%r_zero(:) = cfgs(ibox)%mols(im)%atms(i)%r_zero(:) / stat%nsamp_disp

            enddo

        enddo

        stat%nsamp_disp = 0

    else

        if (.not.allocated(disp)) allocate (disp(cfgs(ibox)%number_of_atoms,3))

        !loop over all atoms to get the displacements
        do im = 1, cfgs(ibox)%num_mols

            do i = 1, cfgs(ibox)%mols(im)%natom

                ii = cfgs(ibox)%mols(im)%glob_no(i)

                xx = cfgs(ibox)%mols(im)%atms(i)%rpos(1) - cfgs(ibox)%mols(im)%atms(i)%r_zero(1)
                yy = cfgs(ibox)%mols(im)%atms(i)%rpos(2) - cfgs(ibox)%mols(im)%atms(i)%r_zero(2)
                zz = cfgs(ibox)%mols(im)%atms(i)%rpos(3) - cfgs(ibox)%mols(im)%atms(i)%r_zero(3)

                !nearest image convention - just in case something is wobling on the edge of the box
                rx = cfgs(ibox)%vec%invlat(1,1) * xx + cfgs(ibox)%vec%invlat(2,1) * yy + cfgs(ibox)%vec%invlat(3,1) * zz
                ry = cfgs(ibox)%vec%invlat(1,2) * xx + cfgs(ibox)%vec%invlat(2,2) * yy + cfgs(ibox)%vec%invlat(3,2) * zz
                rz = cfgs(ibox)%vec%invlat(1,3) * xx + cfgs(ibox)%vec%invlat(2,3) * yy + cfgs(ibox)%vec%invlat(3,3) * zz

                ! AB: unified way to apply PBC/MIC
                xx = rx
                yy = ry
                zz = rz
                call pbc_atom_pos_calc(xx, yy, zz)

                ! AB: original code for PBC/MIC
                !xx = rx - anint(rx)
                !yy = ry - anint(ry)
                !zz = rz - anint(rz)

                dx = cfgs(ibox)%vec%latvector(1,1) * xx + cfgs(ibox)%vec%latvector(2,1) * yy + cfgs(ibox)%vec%latvector(3,1) * zz
                dy = cfgs(ibox)%vec%latvector(1,2) * xx + cfgs(ibox)%vec%latvector(2,2) * yy + cfgs(ibox)%vec%latvector(3,2) * zz
                dz = cfgs(ibox)%vec%latvector(1,3) * xx + cfgs(ibox)%vec%latvector(2,3) * yy + cfgs(ibox)%vec%latvector(3,3) * zz

                disp(ii,1) = dx
                disp(ii,2) = dy
                disp(ii,3) = dz
                !disp(ii) = sqrt(dx * dx + dy * dy + dz * dz)


            enddo

        enddo

        !determine whether an atom is within the radius  - this assumes that ii and jj are always more or less in
        ! the same position. This will probably hold for solids but not for liquids
        do im = 1, cfgs(ibox)%num_mols

            do i = 1, cfgs(ibox)%mols(im)%natom

                ii = cfgs(ibox)%mols(im)%glob_no(i)

                ax = cfgs(ibox)%mols(im)%atms(i)%rpos(1)
                ay = cfgs(ibox)%mols(im)%atms(i)%rpos(2)
                az = cfgs(ibox)%mols(im)%atms(i)%rpos(3)

                do jm = 1, cfgs(ibox)%num_mols

                    nbr = 0

                    do j = 1, cfgs(ibox)%mols(jm)%natom

                        jj = cfgs(ibox)%mols(jm)%glob_no(j)

                        if (ii == jj) cycle

                        bx = cfgs(ibox)%mols(jm)%atms(j)%rpos(1)
                        by = cfgs(ibox)%mols(jm)%atms(j)%rpos(2)
                        bz = cfgs(ibox)%mols(jm)%atms(j)%rpos(3)

                        !calculate distance
                        xx = ax - bx
                        yy = ay - by
                        zz = az - bz

                        rx = cfgs(ibox)%vec%invlat(1,1) * xx + cfgs(ibox)%vec%invlat(2,1) * yy + &
                             cfgs(ibox)%vec%invlat(3,1) * zz
                        ry = cfgs(ibox)%vec%invlat(1,2) * xx + cfgs(ibox)%vec%invlat(2,2) * yy + &
                             cfgs(ibox)%vec%invlat(3,2) * zz
                        rz = cfgs(ibox)%vec%invlat(1,3) * xx + cfgs(ibox)%vec%invlat(2,3) * yy + &
                             cfgs(ibox)%vec%invlat(3,3) * zz

                        ! AB: unified way to apply PBC/MIC
                        xx = rx
                        yy = ry
                        zz = rz
                        call pbc_atom_pos_calc(xx, yy, zz)

                        ! AB: original code for PBC/MIC
                        !xx = rx - anint(rx)
                        !yy = ry - anint(ry)
                        !zz = rz - anint(rz)

                        rx = cfgs(ibox)%vec%latvector(1,1) * xx + cfgs(ibox)%vec%latvector(2,1) * yy + &
                             cfgs(ibox)%vec%latvector(3,1) * zz
                        ry = cfgs(ibox)%vec%latvector(1,2) * xx + cfgs(ibox)%vec%latvector(2,2) * yy + &
                             cfgs(ibox)%vec%latvector(3,2) * zz
                        rz = cfgs(ibox)%vec%latvector(1,3) * xx + cfgs(ibox)%vec%latvector(2,3) * yy + &
                             cfgs(ibox)%vec%latvector(3,3) * zz

                        ! determine distance

                        rsq = rx * rx + ry * ry + rz * rz
                        r = sqrt(rsq)
                        !if within the radius add to the stp_displacement
                        !nbr prevents out of bounds (prevent ii=jj interaction)
!                       nbr = nbr + 1
!                       if (nbr > max_nbrs) call error(359) ! throw an error an stop

                        stat%stp_disp(ii, jj,:) = stat%stp_disp(ii, jj,:) + disp(ii,:) * disp(jj,:)

                    enddo

                enddo

            enddo

        enddo

        stat%nsamp_disp = stat%nsamp_disp + 1


    endif

    if (allocated(disp)) deallocate (disp)

end subroutine

subroutine write_displacements(ibox, stat)

    use kinds_f90
    use statistics_type
    use constants_module, only : udsp
    use cell_module, only : cfgs
    use comms_mpi_module, only : master
    !use parallel_loop_module, only : master

    implicit none

    integer, intent(in) :: ibox

    type(statistics), intent(inout) :: stat

    integer :: im, i, ii, j

    if (.not. master) return

    if (stat%nsamp_disp > 0) stat%stp_disp = stat%stp_disp / stat%nsamp_disp

    write (udsp,"('X direction')")
    do im = 1, cfgs(ibox)%num_mols

        do i = 1, cfgs(ibox)%mols(im)%natom

            ii =  cfgs(ibox)%mols(im)%glob_no(i)
            write (udsp,"('global no ', i6)") ii
            write (udsp,"(12f20.6)") (stat%stp_disp(ii,j,1), j = 1, cfgs(ibox)%number_of_atoms)

        enddo

    enddo

    write (udsp,"('Y direction')")
    do im = 1, cfgs(ibox)%num_mols

        do i = 1, cfgs(ibox)%mols(im)%natom

            ii =  cfgs(ibox)%mols(im)%glob_no(i)
            write (udsp,"('global no ', i6)") ii
            write (udsp,"(12f20.6)") (stat%stp_disp(ii,j,2), j = 1, cfgs(ibox)%number_of_atoms)

        enddo

    enddo

    write (udsp,"('Z direction')")
    do im = 1, cfgs(ibox)%num_mols

        do i = 1, cfgs(ibox)%mols(im)%natom

            ii =  cfgs(ibox)%mols(im)%glob_no(i)
            write (udsp,"('global no ', i6)") ii
            write (udsp,"(12f20.6)") (stat%stp_disp(ii,j,3), j = 1, cfgs(ibox)%number_of_atoms)

        enddo

    enddo

end subroutine

end module
