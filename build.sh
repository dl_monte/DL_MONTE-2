#!/bin/bash

##############################################
#                                            #
# Simple workaround for building DL_MONTE-2  #
# based on different Makefile(s) and modules #
#   (various compilation routes and rules)   #
#                                            #
# Author: Andrey Brukhno (c) June 2016       #
#                                            #
##############################################

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
cat <<EOF

////////////////////////////////////////////////////////////////////////
//                                                                    //
//   DL_MONTE-2 building script, version 2.02 (alpha)                 //
//   Author - Dr Andrey Brukhno (DL/STFC), June 2016                  //
//                                                                    //
////////////////////////////////////////////////////////////////////////

- Builds an executable (in ./bin) based on three compulsory arguments -

===========
Main usage: 
===========

./${0##*/} <exe_type> <vdw_type> <make_rule>

<exe_type>  = 'PRL' / 'par[allel]' or 'SRL' / 'ser[ial]'
<vdw_type>  = 'dir[ect]'   or 'tab[les]'
<make_rule> - refer to the corresponding Makefile

==========
Otherwise:
==========

./${0##*/} -h
./${0##*/} --help

- output this help message

./${0##*/} clean
./${0##*/} -clean

- 'make clean' and restore "standard" environment (Makefile_PRL and vdw_direct_module.f90)

EOF
   exit 0
fi

echo
echo "//////////////////////////////////////////////////////"
echo "//                                                  //"
echo "// DL_MONTE-2 building script, version 2.02 (alpha) //"
echo "// Author - Dr Andrey Brukhno (DL/STFC), June 2016  //"
echo "//                                                  //"
echo "//////////////////////////////////////////////////////"
echo

if [ "$1" == "clean" ] || [ "$1" == "-clean" ] ; then

    echo "Cleaning and restoring standard environment:"

    cp -v ./vdw_direct_module.f90 vdw_module.f90
    cp -v ./Makefile_PRL ./Makefile

    echo
    make clean

    echo
    exit 0

elif [ "$3" == "" ] ; then

    echo "No (i.e. empty) target rule for make command - exiting..."
    echo

    exit 0

fi

if [ "$1" == "par" ] || [ "$1" == "parallel" ] || [ "$1" == "PRL" ] ; then

    if [ -s ./Makefile_PRL ]; then 

        TARGET="$(grep "$3:" ./Makefile_PRL)"

        if [[ -n "${TARGET}" ]]; then

            echo "Preparing for building with target '$3' from ./Makefile_PRL..."
            echo

        else

            echo "Target '$3' not found in ./Makefile_PRL - exiting..."
            echo

            exit 0

        fi

    else

        echo "./Makefile_PRL not found - exiting..."
        echo

        exit 0

    fi

    if [ "$2" == "dir" ] || [ "$2" == "direct" ] ; then

        echo "Building parallel executable ./bin/DLMONTE-PRL-VDW_dir.X "
        echo

        cp -v ./vdw_direct_module.f90 ./vdw_module.f90
        cp -v ./Makefile_PRL ./Makefile

        echo

        [[ -e ./mpif.h ]] && make clean ; #_serial ; #clean_parallel #clean_serial
        make $3

        cp -v ./bin/DLMONTE-PRL.X ./bin/DLMONTE-PRL-VDW_dir.X

    elif [ "$2" == "tab" ] || [ "$2" == "tables" ] ; then

        echo "Building parallel executable ./bin/DLMONTE-PRL-VDW_tab.X "
        echo

        cp -v ./vdw_tables_module.f90 ./vdw_module.f90
        cp -v ./Makefile_PRL ./Makefile

        echo

        [[ -e ./mpif.h ]] && make clean ; #_serial ; #clean_parallel clean_serial
        make $3

        cp -v ./bin/DLMONTE-PRL.X ./bin/DLMONTE-PRL-VDW_tab.X

    else

        echo "Unknown VDW-module type for building: '$2' - use either 'dir[ect]' or 'tab[les]' "

    fi

elif [ "$1" == "ser" ] || [ "$1" == "serial" ] || [ "$1" == "SRL" ]; then

    if [ -s ./Makefile_SRL ]; then 

        TARGET="$(grep "$3:" ./Makefile_SRL)"

        if [[ -n "${TARGET}" ]]; then

            echo "Preparing for building with target '$3' from ./Makefile_SRL..."
            echo

        else

            echo "Target '$3' not found in ./Makefile_SRL - exiting..."
            echo

            exit 0

        fi

    else

        echo "./Makefile_SRL not found - exiting..."
        echo

        exit 0

    fi

    if [ "$2" == "dir" ] || [ "$2" == "direct" ] ; then

        echo "Building serial executable ./bin/DLMONTE-SRL-VDW_dir.X"
        echo

        cp -v ./vdw_direct_module.f90 ./vdw_module.f90
        cp -v ./Makefile_SRL ./Makefile

        echo

        [[ ! -e ./mpif.h ]] && make clean ; #_parallel ; #clean_serial
        make $3

        cp -v ./bin/DLMONTE-SRL.X ./bin/DLMONTE-SRL-VDW_dir.X

    elif [ "$2" == "tab" ] || [ "$2" == "tables" ] ; then

        echo "Building serial executable ./bin/DLMONTE-SRL-VDW_tab.X"
        echo

        cp -v ./vdw_tables_module.f90 ./vdw_module.f90
        cp -v ./Makefile_SRL ./Makefile

        echo

        [[ ! -e ./mpif.h ]] && make clean ; #_parallel ; #clean_serial
        make $3

        cp -v ./bin/DLMONTE-SRL.X ./bin/DLMONTE-SRL-VDW_tab.X

    else

        echo "Unknown VDW-module type for building: '$2' - use either 'dir[ect]' or 'tab[les]' "

    fi

else 

    echo "Unknown executable type for building: '$1' - use either 'par[allel]' or 'ser[ial]' "

fi

echo

