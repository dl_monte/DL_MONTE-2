! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!*!!!!!!!!!!!!!!!!!!!!!
! Originally based on !
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 module for global communication routines and functions
!
! copyright - daresbury laboratory
! author    - i.t.todorov december 2007
!
!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!> @brief
!> - MPI communication between threads over the available nodes/cores
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`
!> - `mpi_module` (if `Makefile_SRLx` is used, i.e. for "serial" type of executable)

!> @modulefor MPI communication between threads over the available nodes/cores

Module comms_mpi_module

  Use kinds_f90
  !Use mpi_module ! To uncomment if Makefile_SRLx is used

  Implicit None

  Include 'mpif.h'

  !AB: previously 'master' was declared in parallel_loop_module.f90
  !AB: moved here to allow error() messages not only for idnode=0

  !identifies the node(s) that will print out information
  logical, save :: master, is_serial, is_parallel

  Integer, Save :: mpi_wp,idnode,mxnode
  Data             mpi_wp,idnode,mxnode / 0 , 0 , 1 /

  Integer, Save :: ierr,request,status(1:MPI_STATUS_SIZE)
  Data             ierr,request,status / 0, 0, MPI_STATUS_SIZE * 0 /

      !> the communicator for workgroups - it is set to WORLD for non-replica-exchange \n
      !> NOTE: initially (by default) `mpi_comm_group = mpi_comm_world` \n
      !> but reset, when needed - later on in the course of initialisation, \n
      !> to the scope of a specific group due to `call setup_workgroups(..)`
  integer, save :: mpi_comm_group

      !> communicator to pass structure up/down groups when using replica-exchange
  integer, save :: mpi_cart_comm

      !> the communicator for masters - it is set to MPI_COMM_NULL initially \n
      !> this allows for not only splitting the WORLD into groups \n
      !> but also reducing the WORLDWIDE communication overheads \n
      !> so that messages are initially distributed within each group \n
      !> and then between all the masters, if required (e.g. when dealing with errors)
  integer, save :: mpi_comm_master

      !> replica-exchange neighbours of each processors and the coordinates of the node
  integer :: map_temp_down, map_temp_up, cart_coords(2)

      !> dummy communicator
  integer, save :: newcomm

      !> MPI-I/O representation
  Character( Len = 6), Parameter :: datarep = 'native'


      !> Message tags
  Integer, Parameter :: ICTAG   = 1100, &
                        Import_tag   = 1101, &
                        Export_tag   = 1111, &
                        Revive_tag   = 1122, &
                        Traject_tag  = 1133, &
                        Fftcomms_tag = 1144, &
                        Passunit_tag = 1155, &
                        Passpmf_tag  = 1166, &
                        Updshun_tag  = 1177, &
                        Updpmfun_tag = 1188, &
                        Metldexp_tag = 1199, &
                        DefExport_tag= 2200, &
                        DefWrite_tag = 2211

  Real(kind=wp), save :: tzero = 0.0_wp
  Real(kind=wp), save :: twait = 0.0_wp
  Real(kind=wp), save :: tcomm = 0.0_wp

  Real(kind=wp), save :: twbcast_is = 0.0_wp
  Real(kind=wp), save :: twbcast_iv = 0.0_wp
  Real(kind=wp), save :: twbcast_rs = 0.0_wp
  Real(kind=wp), save :: twbcast_rv = 0.0_wp
  Real(kind=wp), save :: twbcast_ls = 0.0_wp
  Real(kind=wp), save :: twbcast_lv = 0.0_wp

  Real(kind=wp), save :: tgbcast_is = 0.0_wp
  Real(kind=wp), save :: tgbcast_iv = 0.0_wp
  Real(kind=wp), save :: tgbcast_rs = 0.0_wp
  Real(kind=wp), save :: tgbcast_rv = 0.0_wp
  Real(kind=wp), save :: tgbcast_ls = 0.0_wp
  Real(kind=wp), save :: tgbcast_lv = 0.0_wp

  Real(kind=wp), save :: twsumm_is = 0.0_wp
  Real(kind=wp), save :: twsumm_iv = 0.0_wp
  Real(kind=wp), save :: twsumm_rs = 0.0_wp
  Real(kind=wp), save :: twsumm_rv = 0.0_wp
  Real(kind=wp), save :: twsumm_ls = 0.0_wp
  Real(kind=wp), save :: twsumm_lv = 0.0_wp

  Real(kind=wp), save :: tgsumm_is = 0.0_wp
  Real(kind=wp), save :: tgsumm_iv = 0.0_wp
  Real(kind=wp), save :: tgsumm_rs = 0.0_wp
  Real(kind=wp), save :: tgsumm_rv = 0.0_wp
  Real(kind=wp), save :: tgsumm_ls = 0.0_wp
  Real(kind=wp), save :: tgsumm_lv = 0.0_wp
  Real(kind=wp), save :: tgsumm_cv = 0.0_wp

  Real(kind=wp), save :: tgimax_is = 0.0_wp
  Real(kind=wp), save :: tgimax_iv = 0.0_wp
  Real(kind=wp), save :: tgrmax_rs = 0.0_wp
  Real(kind=wp), save :: tgrmax_rv = 0.0_wp
  Real(kind=wp), save :: tgimin_is = 0.0_wp
  Real(kind=wp), save :: tgimin_iv = 0.0_wp
  Real(kind=wp), save :: tgrmin_rs = 0.0_wp
  Real(kind=wp), save :: tgrmin_rv = 0.0_wp

  Real(kind=wp), save :: tcsend_rv = 0.0_wp
  Real(kind=wp), save :: tcrecv_rv = 0.0_wp
  Real(kind=wp), save :: tgsend_rs = 0.0_wp
  Real(kind=wp), save :: tgrecv_rs = 0.0_wp
  Real(kind=wp), save :: tgrecv_ru = 0.0_wp

  Integer, save :: nwbcast_is = 0
  Integer, save :: nwbcast_iv = 0
  Integer, save :: nwbcast_rs = 0
  Integer, save :: nwbcast_rv = 0
  Integer, save :: nwbcast_ls = 0
  Integer, save :: nwbcast_lv = 0

  Integer, save :: ngbcast_is = 0
  Integer, save :: ngbcast_iv = 0
  Integer, save :: ngbcast_rs = 0
  Integer, save :: ngbcast_rv = 0
  Integer, save :: ngbcast_ls = 0
  Integer, save :: ngbcast_lv = 0

  Integer, save :: nwsumm_is = 0
  Integer, save :: nwsumm_iv = 0
  Integer, save :: nwsumm_rs = 0
  Integer, save :: nwsumm_rv = 0
  Integer, save :: nwsumm_ls = 0
  Integer, save :: nwsumm_lv = 0

  Integer, save :: ngsumm_is = 0
  Integer, save :: ngsumm_iv = 0
  Integer, save :: ngsumm_rs = 0
  Integer, save :: ngsumm_rv = 0
  Integer, save :: ngsumm_ls = 0
  Integer, save :: ngsumm_lv = 0
  Integer, save :: ngsumm_cv = 0

  Integer, save :: ncsend_rv = 0
  Integer, save :: ncrecv_rv = 0
  Integer, save :: ngsend_rs = 0
  Integer, save :: ngrecv_rs = 0
  Integer, save :: ngrecv_ru = 0

  Integer, save :: ngimax_is = 0
  Integer, save :: ngimax_iv = 0
  Integer, save :: ngrmax_rs = 0
  Integer, save :: ngrmax_rv = 0
  Integer, save :: ngimin_is = 0
  Integer, save :: ngimin_iv = 0
  Integer, save :: ngrmin_rs = 0
  Integer, save :: ngrmin_rv = 0

  Integer, save :: ncomm = 0

  Public :: init_mpi_comms, exit_mpi_comms, write_mpi_stats, &
            gsync, gsync_world, gcheck, gsum, gsum_world, gmax, gtime, &
            msg_receive_blocked, msg_send_blocked, msg_bcast

  !AB: All communication interfaces/routines below have either groupwide or worldwide scope,
  !AB: BUT initially (by default) the groups' communicators mpi_comm_group = mpi_comm_world
  !AB: (see parallel_loop_module::initialise_loops(..) that is called from `dlmonte.f90`)

  !AB: These are reset to the scope of specific groups due to `call setup_workgroups(..)` (see parallel_loop_module.f90)
  !AB: when it is necessary - currently only if replica-exchange is invoked (after reading the input files!)

  interface msg_bcast_world
     module procedure msg_wbcast_iscalar
     module procedure msg_wbcast_ivector
     module procedure msg_wbcast_rscalar
     module procedure msg_wbcast_rvector
     module procedure msg_wbcast_lscalar
     module procedure msg_wbcast_lvector
  end interface msg_bcast_world

  interface msg_bcast
     module procedure msg_bcast_iscalar
     module procedure msg_bcast_ivector
     module procedure msg_bcast_rscalar
     module procedure msg_bcast_rvector
     module procedure msg_bcast_lscalar
     module procedure msg_bcast_lvector
  end interface msg_bcast

  Interface gsum_world
     Module Procedure grsum_all_vector
     Module Procedure grsum_all_scalar
     Module Procedure gisum_all_vector
     Module Procedure gisum_all_scalar
     !Module Procedure grsum_world_vector
     !Module Procedure grsum_world_scalar
     !Module Procedure gisum_world_vector
     !Module Procedure gisum_world_scalar
  End Interface

  Interface gsum
     Module Procedure gisum_vector
     Module Procedure gisum_scalar
     Module Procedure grsum_vector
     Module Procedure grsum_scalar
     Module Procedure gcsum_vector
  End Interface !gsum

  Interface gcheck
     Module Procedure gcheck_vector
     Module Procedure gcheck_scalar
  End Interface !gcheck

  Interface gmax
     Module Procedure gimax_vector
     Module Procedure gimax_scalar

     Module Procedure grmax_vector
     Module Procedure grmax_scalar
  End Interface !gmax

  Interface gmin
     Module Procedure gimin_vector
     Module Procedure gimin_scalar

     Module Procedure grmin_vector
     Module Procedure grmin_scalar
  End Interface !gmin

  interface msg_send_blocked
     module procedure msg_send_blocked_scalar
     module procedure msg_send_blocked_vector
  end interface !msg_send_blocked

  interface msg_receive_blocked
     module procedure msg_receive_blocked_scalar
     module procedure msg_receive_blocked_vector
  end interface !msg_receive_blocked


Contains


Subroutine init_mpi_comms()

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for initialisation communication harness
! determining the MPI precision, and node identification and count
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Call MPI_INIT(ierr)

    If      (wp == Selected_Real_Kind(6,30))  Then

       mpi_wp = MPI_REAL

    Else If (wp == Selected_Real_Kind(14,300))  Then

! MPI_REAL8 is apparently not in the strict MPI2 standard
! It is just an optional data type in the FORTRAN Bindings

       mpi_wp = MPI_DOUBLE_PRECISION

    Else If (wp == Selected_Real_Kind(30,300)) Then

       mpi_wp = MPI_REAL16

    Else

       Call error(1000)

    End If

    Call MPI_COMM_RANK(MPI_COMM_WORLD, idnode, ierr)
    Call MPI_COMM_SIZE(MPI_COMM_WORLD, mxnode, ierr)

    !AB: initially there is only one 'master'
    !AB: more are defined in parallel_loop_module::setup_workgroups()
    master = ( mxnode == 1 .or. idnode==0 )

    is_serial   = ( mxnode == 1 )
    is_parallel = .not.is_serial

End Subroutine init_mpi_comms

Subroutine exit_mpi_comms()

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine for exit from communication harness
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Call MPI_FINALIZE(ierr)

End Subroutine exit_mpi_comms


Subroutine write_mpi_stats()

    use constants_module, only : uout

    If( is_serial .or. .not.master ) Return

    write(uout,*)
    write(uout,*)"=============================================="
    write(uout,*)" Stats for MPI calls & their time footprints :"
    write(uout,*)"----------------------------------------------"

    tcomm = tcomm + twbcast_is
    ncomm = ncomm + nwbcast_is
    if( nwbcast_is > 0 ) write(uout,*) &
                 " - world BCAST of integer scalars No & time = ",nwbcast_is,twbcast_is,twbcast_is/real(nwbcast_is,wp)

    tcomm = tcomm + twbcast_iv
    ncomm = ncomm + nwbcast_iv
    if( nwbcast_iv > 0 ) write(uout,*) &
                 " - world BCAST of integer vectors No & time = ",nwbcast_iv,twbcast_iv,twbcast_iv/real(nwbcast_iv,wp)

    tcomm = tcomm + twbcast_rs
    ncomm = ncomm + nwbcast_rs
    if( nwbcast_rs > 0 ) write(uout,*) &
                 " - world BCAST of real_wp scalars No & time = ",nwbcast_rs,twbcast_rs,twbcast_rs/real(nwbcast_rs,wp)

    tcomm = tcomm + twbcast_rv
    ncomm = ncomm + nwbcast_rv
    if( nwbcast_rv > 0 ) write(uout,*) &
                 " - world BCAST of real_wp vectors No & time = ",nwbcast_rv,twbcast_rv,twbcast_rv/real(nwbcast_rv,wp)

    tcomm = tcomm + twbcast_ls
    ncomm = ncomm + nwbcast_ls
    if( nwbcast_ls > 0 ) write(uout,*) &
                 " - world BCAST of logical scalars No & time = ",nwbcast_ls,twbcast_ls,twbcast_ls/real(nwbcast_ls,wp)

    tcomm = tcomm + twbcast_lv
    ncomm = ncomm + nwbcast_lv
    if( nwbcast_lv > 0 ) write(uout,*) &
                 " - world BCAST of logical vectors No & time = ",nwbcast_lv,twbcast_lv,twbcast_lv/real(nwbcast_lv,wp)


    tcomm = tcomm + tgbcast_is
    ncomm = ncomm + ngbcast_is
    if( ngbcast_is > 0 ) write(uout,*) &
                 " - group BCAST of integer scalars No & time = ",ngbcast_is,tgbcast_is,tgbcast_is/real(ngbcast_is,wp)

    tcomm = tcomm + tgbcast_iv
    ncomm = ncomm + ngbcast_iv
    if( ngbcast_iv > 0 ) write(uout,*) &
                 " - group BCAST of integer vectors No & time = ",ngbcast_iv,tgbcast_iv,tgbcast_iv/real(ngbcast_iv,wp)

    tcomm = tcomm + tgbcast_rs
    ncomm = ncomm + ngbcast_rs
    if( ngbcast_rs > 0 ) write(uout,*) &
                 " - group BCAST of real_wp scalars No & time = ",ngbcast_rs,tgbcast_rs,tgbcast_rs/real(ngbcast_rs,wp)

    tcomm = tcomm + tgbcast_rv
    ncomm = ncomm + ngbcast_rv
    if( ngbcast_rv > 0 ) write(uout,*) &
                 " - group BCAST of real_wp vectors No & time = ",ngbcast_rv,tgbcast_rv,tgbcast_rv/real(ngbcast_rv,wp)

    tcomm = tcomm + tgbcast_ls
    ncomm = ncomm + ngbcast_ls
    if( ngbcast_ls > 0 ) write(uout,*) &
                 " - group BCAST of logical scalars No & time = ",ngbcast_ls,tgbcast_ls,tgbcast_ls/real(ngbcast_ls,wp)

    tcomm = tcomm + tgbcast_lv
    ncomm = ncomm + ngbcast_lv
    if( ngbcast_lv > 0 ) write(uout,*) &
                 " - group BCAST of logical vectors No & time = ",ngbcast_lv,tgbcast_lv,tgbcast_lv/real(ngbcast_lv,wp)


    tcomm = tcomm + twsumm_is
    ncomm = ncomm + nwsumm_is
    if( nwsumm_is > 0 ) write(uout,*) &
                 " - world SUMMS of integer scalars No & time = ",nwsumm_is,twsumm_is,twsumm_is/real(nwsumm_is,wp)

    tcomm = tcomm + twsumm_iv
    ncomm = ncomm + nwsumm_iv
    if( nwsumm_iv > 0 ) write(uout,*) &
                 " - world SUMMS of integer vectors No & time = ",nwsumm_iv,twsumm_iv,twsumm_iv/real(nwsumm_iv,wp)

    tcomm = tcomm + twsumm_rs
    ncomm = ncomm + nwsumm_rs
    if( nwsumm_rs > 0 ) write(uout,*) &
                 " - world SUMMS of real_wp scalars No & time = ",nwsumm_rs,twsumm_rs,twsumm_rs/real(nwsumm_rs,wp)

    tcomm = tcomm + twsumm_rv
    ncomm = ncomm + nwsumm_rv
    if( nwsumm_rv > 0 ) write(uout,*) &
                 " - world SUMMS of real_wp vectors No & time = ",nwsumm_rv,twsumm_rv,twsumm_rv/real(nwsumm_rv,wp)

    tcomm = tcomm + twsumm_ls
    ncomm = ncomm + nwsumm_ls
    if( nwsumm_ls > 0 ) write(uout,*) &
                 " - world SUMMS of logical scalars No & time = ",nwsumm_ls,twsumm_ls,twsumm_ls/real(nwsumm_ls,wp)

    tcomm = tcomm + twsumm_lv
    ncomm = ncomm + nwsumm_lv
    if( nwsumm_lv > 0 ) write(uout,*) &
                 " - world SUMMS of logical vectors No & time = ",nwsumm_lv,twsumm_lv,twsumm_lv/real(nwsumm_lv,wp)


    tcomm = tcomm + tgsumm_is
    ncomm = ncomm + ngsumm_is
    if( ngsumm_is > 0 ) write(uout,*) &
                 " - group SUMMS of integer scalars No & time = ",ngsumm_is,tgsumm_is,tgsumm_is/real(ngsumm_is,wp)

    tcomm = tcomm + tgsumm_iv
    ncomm = ncomm + ngsumm_iv
    if( ngsumm_iv > 0 ) write(uout,*) &
                 " - group SUMMS of integer vectors No & time = ",ngsumm_iv,tgsumm_iv,tgsumm_iv/real(ngsumm_iv,wp)

    tcomm = tcomm + tgsumm_rs
    ncomm = ncomm + ngsumm_rs
    if( ngsumm_rs > 0 ) write(uout,*) &
                 " - group SUMMS of real_wp scalars No & time = ",ngsumm_rs,tgsumm_rs,tgsumm_rs/real(ngsumm_rs,wp)

    tcomm = tcomm + tgsumm_rv
    ncomm = ncomm + ngsumm_rv
    if( ngsumm_rv > 0 ) write(uout,*) &
                 " - group SUMMS of real_wp vectors No & time = ",ngsumm_rv,tgsumm_rv,tgsumm_rv/real(ngsumm_rv,wp)

    tcomm = tcomm + tgsumm_cv
    ncomm = ncomm + ngsumm_cv
    if( ngsumm_cv > 0 ) write(uout,*) &
                 " - group SUMMS of complex vectors No & time = ",ngsumm_cv,tgsumm_cv,tgsumm_cv/real(ngsumm_cv,wp)

    tcomm = tcomm + tgsumm_ls
    ncomm = ncomm + ngsumm_ls
    if( ngsumm_ls > 0 ) write(uout,*) &
                 " - group SUMMS of logical scalars No & time = ",ngsumm_ls,tgsumm_ls,tgsumm_ls/real(ngsumm_ls,wp)

    tcomm = tcomm + tgsumm_lv
    ncomm = ncomm + ngsumm_lv
    if( ngsumm_lv > 0 ) write(uout,*) &
                 " - group SUMMS of logical vectors No & time = ",ngsumm_lv,tgsumm_lv,tgsumm_lv/real(ngsumm_lv,wp)


    tcomm = tcomm + tgimax_is
    ncomm = ncomm + ngimax_is
    if( ngimax_is > 0 ) write(uout,*) &
                 " - group iMAXS of integer scalars No & time = ",ngimax_is,tgimax_is,tgimax_is/real(ngimax_is,wp)

    tcomm = tcomm + tgimax_iv
    ncomm = ncomm + ngimax_iv
    if( ngimax_iv > 0 ) write(uout,*) &
                 " - group iMAXS of integer vectors No & time = ",ngimax_iv,tgimax_iv,tgimax_iv/real(ngimax_iv,wp)

    tcomm = tcomm + tgrmax_rs
    ncomm = ncomm + ngrmax_rs
    if( ngrmax_rs > 0 ) write(uout,*) &
                 " - group rMAXS of real_wp scalars No & time = ",ngrmax_rs,tgrmax_rs,tgrmax_rs/real(ngrmax_rs,wp)

    tcomm = tcomm + tgrmax_rv
    ncomm = ncomm + ngrmax_rv
    if( ngrmax_rv > 0 ) write(uout,*) &
                 " - group rMAXS of real_wp vectors No & time = ",ngrmax_rv,tgrmax_rv,tgrmax_rv/real(ngrmax_rv,wp)

    tcomm = tcomm + tgimin_is
    ncomm = ncomm + ngimin_is
    if( ngimin_is > 0 ) write(uout,*) &
                 " - group iMINS of integer scalars No & time = ",ngimin_is,tgimin_is,tgimin_is/real(ngimin_is,wp)

    tcomm = tcomm + tgimin_iv
    ncomm = ncomm + ngimin_iv
    if( ngimin_iv > 0 ) write(uout,*) &
                 " - group iMINS of integer vectors No & time = ",ngimin_iv,tgimin_iv,tgimin_iv/real(ngimin_iv,wp)

    tcomm = tcomm + tgrmin_rs
    ncomm = ncomm + ngrmin_rs
    if( ngrmin_rs > 0 ) write(uout,*) &
                 " - group rMINS of real_wp scalars No & time = ",ngrmin_rs,tgrmin_rs,tgrmin_rs/real(ngrmin_rs,wp)

    tcomm = tcomm + tgrmin_rv
    ncomm = ncomm + ngrmin_rv
    if( ngrmin_rv > 0 ) write(uout,*) &
                 " - group rMINS of real_wp vectors No & time = ",ngrmin_rv,tgrmin_rv,tgrmin_rv/real(ngrmin_rv,wp)


    tcomm = tcomm + tgsend_rs
    ncomm = ncomm + ngsend_rs
    if( ngsend_rs > 0 ) write(uout,*) &
                 " - blocked SENDS of real_wp scalars No & time = ",ngsend_rs,tgsend_rs,tgsend_rs/real(ngsend_rs,wp)

    tcomm = tcomm + tgrecv_rs
    ncomm = ncomm + ngrecv_rs
    if( ngrecv_rs > 0 ) write(uout,*) &
                 " - blocked RECVS of real_wp scalars No & time = ",ngrecv_rs,tgrecv_rs,tgrecv_rs/real(ngrecv_rs,wp)

    tcomm = tcomm + tcsend_rv
    ncomm = ncomm + ncsend_rv
    if( ncsend_rv > 0 ) write(uout,*) &
                 " - blocked SENDS of real_wp vectors No & time = ",ncsend_rv,tcsend_rv,tcsend_rv/real(ncsend_rv,wp)

    tcomm = tcomm + tcrecv_rv
    ncomm = ncomm + ncrecv_rv
    if( ncrecv_rv > 0 ) write(uout,*) &
                 " - blocked RECVS of real_wp vectors No & time = ",ncrecv_rv,tcrecv_rv,tcrecv_rv/real(ncrecv_rv,wp)

    tcomm = tcomm + tgrecv_ru
    ncomm = ncomm + ngrecv_ru
    if( ngrecv_ru > 0 ) write(uout,*) &
                 " - unblocked RECVS of real_wp vectors No & time = ",ngrecv_ru,tgrecv_ru,tgrecv_ru/real(ngrecv_ru,wp)

    tcomm = tcomm + twait

    write(uout,*)"----------------------------------------------"
    write(uout,*)" total number of MPI calls & total MPI time = ",ncomm,tcomm," (MPI_WAIT time: ",twait,")"
    write(uout,*)"=============================================="
    write(uout,*)

End Subroutine write_mpi_stats


Subroutine gsync()

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 barrier/synchronization routine
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    If( is_serial ) Return

    Call MPI_BARRIER(mpi_comm_group,ierr)

End Subroutine gsync

Subroutine gsync_world()

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 barrier/synchronization routine
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    If( is_serial ) Return

    Call MPI_BARRIER(MPI_COMM_WORLD,ierr)

End Subroutine gsync_world

!AB: <<< WORLDWIDE BROADCAST routines - more efficient with groups

subroutine msg_wbcast_ivector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    integer, intent(inout) :: buf(:)

    !send to other nodes
    !call MPI_BCAST(buf, len, MPI_INTEGER, 0, mpi_comm_world, ierr)

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, len, MPI_INTEGER, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, len, MPI_INTEGER, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, len, MPI_INTEGER, 0, mpi_comm_group, ierr)

    endif

    twbcast_iv = twbcast_iv + MPI_WTIME()-tzero
    nwbcast_iv = nwbcast_iv+1

end subroutine msg_wbcast_ivector

subroutine msg_wbcast_iscalar(buf)

    use kinds_f90
    implicit none

    integer, intent(inout) :: buf

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, 1, MPI_INTEGER, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, 1, MPI_INTEGER, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, 1, MPI_INTEGER, 0, mpi_comm_group, ierr)

    endif

    twbcast_is = twbcast_is + MPI_WTIME()-tzero
    nwbcast_is = nwbcast_is + 1

end subroutine msg_wbcast_iscalar


subroutine msg_wbcast_rvector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    real(kind = wp), intent(inout) :: buf(:)

    !send to other nodes
    !call MPI_BCAST(buf, len, mpi_wp, 0, mpi_comm_world, ierr)

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, len, mpi_wp, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, len, mpi_wp, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, len, mpi_wp, 0, mpi_comm_group, ierr)

    endif

    twbcast_rv = twbcast_rv + MPI_WTIME()-tzero
    nwbcast_rv = nwbcast_rv + 1

end subroutine msg_wbcast_rvector

subroutine msg_wbcast_rscalar(buf)

    use kinds_f90
    implicit none

!   integer, intent(in) :: len
    real(kind = wp), intent(inout) :: buf

    !send to other nodes
    !call MPI_BCAST(buf, 1, mpi_wp, 0, mpi_comm_world, ierr)

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, 1, mpi_wp, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, 1, mpi_wp, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, 1, mpi_wp, 0, mpi_comm_group, ierr)

    endif

    twbcast_rs = twbcast_rs + MPI_WTIME()-tzero
    nwbcast_rs = nwbcast_rs + 1

end subroutine msg_wbcast_rscalar


subroutine msg_wbcast_lvector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    logical, intent(inout) :: buf(:)

    !send to other nodes
    !call MPI_BCAST(buf, len, MPI_LOGICAL, 0, mpi_comm_world, ierr)

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, len, MPI_LOGICAL, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, len, MPI_LOGICAL, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, len, MPI_LOGICAL, 0, mpi_comm_group, ierr)

    endif

    twbcast_lv = twbcast_lv + MPI_WTIME()-tzero
    nwbcast_lv = nwbcast_lv + 1

end subroutine

subroutine msg_wbcast_lscalar(buf)

    use kinds_f90
    implicit none

    logical, intent(inout) :: buf

    !send to other nodes
    !call MPI_BCAST(buf, 1, MPI_LOGICAL, 0, mpi_comm_world, ierr)

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !send to other nodes
        call MPI_BCAST(buf, 1, MPI_LOGICAL, 0, mpi_comm_world, ierr)

    else 

        !AB: send amongst the masters
        if( master ) &
            call MPI_BCAST(buf, 1, MPI_LOGICAL, 0, mpi_comm_master, ierr)

        !AB: send within the group
        call MPI_BCAST(buf, 1, MPI_LOGICAL, 0, mpi_comm_group, ierr)

    endif

    twbcast_ls = twbcast_ls + MPI_WTIME()-tzero
    nwbcast_ls = nwbcast_ls + 1

end subroutine

!AB: >>> WORLDWIDE BROADCAST routines - more efficient with groups

!AB: <<< GROUPWIDE BROADCAST routines - original

subroutine msg_bcast_iscalar(buf)

    use kinds_f90
    implicit none

    integer, intent(inout) :: buf

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, 1, MPI_INTEGER, 0, mpi_comm_group, ierr)

    tgbcast_is = tgbcast_is + MPI_WTIME()-tzero
    ngbcast_is = ngbcast_is + 1

end subroutine

subroutine msg_bcast_ivector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    integer, intent(inout) :: buf(:)

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, len, MPI_INTEGER, 0, mpi_comm_group, ierr)

    tgbcast_iv = tgbcast_iv + MPI_WTIME()-tzero
    ngbcast_iv = ngbcast_iv + 1

end subroutine


subroutine msg_bcast_rscalar(buf)

    use kinds_f90
    implicit none

!   integer, intent(in) :: len
    real(kind = wp), intent(inout) :: buf

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, 1, mpi_wp, 0, mpi_comm_group, ierr)

    tgbcast_rs = tgbcast_rs + MPI_WTIME()-tzero
    ngbcast_rs = ngbcast_rs + 1

end subroutine

subroutine msg_bcast_rvector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    real(kind = wp), intent(inout) :: buf(:)

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, len, mpi_wp, 0, mpi_comm_group, ierr)

    tgbcast_rv = tgbcast_rv + MPI_WTIME()-tzero
    ngbcast_rv = ngbcast_rv + 1

end subroutine


subroutine msg_bcast_lscalar(buf)

    use kinds_f90
    implicit none

    logical, intent(inout) :: buf

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, 1, MPI_LOGICAL, 0, mpi_comm_group, ierr)

    tgbcast_ls = tgbcast_ls + MPI_WTIME()-tzero
    ngbcast_ls = ngbcast_ls + 1

end subroutine

subroutine msg_bcast_lvector(buf, len)

    use kinds_f90
    implicit none

    integer, intent(in) :: len
    logical, intent(inout) :: buf(:)

    tzero = MPI_WTIME()

    !send to other nodes
    call MPI_BCAST(buf, len, MPI_LOGICAL, 0, mpi_comm_group, ierr)

    tgbcast_lv = tgbcast_lv + MPI_WTIME()-tzero
    ngbcast_lv = ngbcast_lv + 1

end subroutine

!AB: >>> GROUPWIDE BROADCAST routines - original

!AB: <<< WORLDWIDE summing-up routines - more efficient with groups than the original g?sum_world()

Subroutine grsum_all_vector(aaa)

    Implicit None

    Real( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Real( Kind = wp ), Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1005)

    n_s = Size(aaa, Dim = 1)

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !AB: sum-up and distribute WORLDWIDE
        Call MPI_ALLREDUCE(aaa, bbb, n_s, mpi_wp, MPI_SUM, MPI_COMM_WORLD, ierr)

        aaa = bbb

    else 

        !AB: sum-up within the group
        Call MPI_ALLREDUCE(aaa, bbb, n_s, mpi_wp, MPI_SUM, mpi_comm_group, ierr)

        !AB: sum-up amongst the masters
        if( master ) &
            Call MPI_ALLREDUCE(bbb, aaa, n_s, mpi_wp, MPI_SUM, mpi_comm_master, ierr)

        !AB: distribute within the group
        Call MPI_BCAST(aaa, n_s, mpi_wp, 0, mpi_comm_group, ierr)

    endif

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1006)

    twsumm_rv = twsumm_rv + MPI_WTIME()-tzero
    nwsumm_rv = nwsumm_rv + 1

End Subroutine grsum_all_vector

Subroutine grsum_all_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision scalar
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Intent( InOut ) :: aaa

    Real( Kind = wp )                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !AB: sum-up and distribute WORLDWIDE
        Call MPI_ALLREDUCE(aaa, bbb, 1, mpi_wp, MPI_SUM, MPI_COMM_WORLD, ierr)

        aaa = bbb

    else 

        !AB: sum-up within the group
        Call MPI_ALLREDUCE(aaa, bbb, 1, mpi_wp, MPI_SUM, mpi_comm_group, ierr)

        !AB: sum-up amongst the masters
        if( master ) &
            Call MPI_ALLREDUCE(bbb, aaa, 1, mpi_wp, MPI_SUM, mpi_comm_master, ierr)

        !AB: distribute within the group
        Call MPI_BCAST(aaa, 1, mpi_wp, 0, mpi_comm_group, ierr)

    endif

    twsumm_rs = twsumm_rs + MPI_WTIME()-tzero
    nwsumm_rs = nwsumm_rs + 1

End Subroutine grsum_all_scalar

Subroutine gisum_all_vector(aaa)

    Implicit None

    Integer, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Integer, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1005)

    n_s = Size(aaa, Dim = 1)

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !AB: sum-up and distribute WORLDWIDE
        Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)

        aaa = bbb

    else 

        !AB: sum-up within the group
        Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_SUM,mpi_comm_group,ierr)

        !AB: sum-up amongst the masters
        if( master ) &
            Call MPI_ALLREDUCE(bbb,aaa,n_s,MPI_INTEGER,MPI_SUM,mpi_comm_master,ierr)

        !AB: distribute within the group
        Call MPI_BCAST(aaa, n_s, MPI_INTEGER, 0, mpi_comm_group, ierr)

    endif

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1006)

    twsumm_iv = twsumm_iv + MPI_WTIME()-tzero
    nwsumm_iv = nwsumm_iv + 1

End Subroutine gisum_all_vector

Subroutine gisum_all_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision scalar
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Intent( InOut ) :: aaa

    Integer                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    if( mpi_comm_group == MPI_COMM_WORLD ) then

        !AB: sum-up and distribute WORLDWIDE
        Call MPI_ALLREDUCE(aaa, bbb, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)

        aaa = bbb

    else 

        !AB: sum-up within the group
        Call MPI_ALLREDUCE(aaa, bbb, 1, MPI_INTEGER, MPI_SUM, mpi_comm_group, ierr)

        !AB: sum-up amongst the masters
        if( master ) &
            Call MPI_ALLREDUCE(bbb, aaa, 1, MPI_INTEGER, MPI_SUM, mpi_comm_master, ierr)

        !AB: distribute within the group
        Call MPI_BCAST(aaa, 1, MPI_INTEGER, 0, mpi_comm_group, ierr)

    endif

    twsumm_is = twsumm_is + MPI_WTIME()-tzero
    nwsumm_is = nwsumm_is + 1

End Subroutine gisum_all_scalar

!AB: >>> WORLDWIDE summing-up routines - more efficient with groups than the original g?sum_world*()

!AB: <<< WORLDWIDE summing-up routines - the original g?sum_world*()

Subroutine grsum_world_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision vector
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Real( Kind = wp ), Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1005)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,mpi_wp,MPI_SUM,MPI_COMM_WORLD,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1006)

    twsumm_rv = twsumm_rv + MPI_WTIME()-tzero
    nwsumm_rv = nwsumm_rv + 1

End Subroutine grsum_world_vector

Subroutine grsum_world_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision scalar
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Intent( InOut ) :: aaa

    Real( Kind = wp )                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,mpi_wp,MPI_SUM,MPI_COMM_WORLD,ierr)

    aaa = bbb

    twsumm_rs = twsumm_rs + MPI_WTIME()-tzero
    nwsumm_rs = nwsumm_rs + 1

End Subroutine grsum_world_scalar

Subroutine gisum_world_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - integer vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                  :: n_l,n_u,n_s,fail

    Integer, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1003)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1004)

    twsumm_iv = twsumm_iv + MPI_WTIME()-tzero
    nwsumm_iv = nwsumm_iv + 1

End Subroutine gisum_world_vector

Subroutine gisum_world_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - integer scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Intent( InOut ) :: aaa

    Integer                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,ierr)

    aaa = bbb

    twsumm_is = twsumm_is + MPI_WTIME()-tzero
    nwsumm_is = nwsumm_is + 1

End Subroutine gisum_world_scalar

!AB: >>> WORLDWIDE summing-up routines - the original g?sum_world*()


Subroutine gcheck_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - boolean vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Logical, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                  :: n_l,n_u,n_s,fail

    Logical, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1001)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_LOGICAL,MPI_LAND,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1002)

    twsumm_lv = twsumm_lv + MPI_WTIME()-tzero
    nwsumm_lv = nwsumm_lv + 1

End Subroutine gcheck_vector

Subroutine gcheck_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - boolean scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Logical, Intent( InOut ) :: aaa

    Logical                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,MPI_LOGICAL,MPI_LAND,mpi_comm_group,ierr)

    aaa = bbb

    twsumm_ls = twsumm_ls + MPI_WTIME()-tzero
    nwsumm_ls = nwsumm_ls + 1

End Subroutine gcheck_scalar

Subroutine gisum_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - integer vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                  :: n_l,n_u,n_s,fail

    Integer, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1003)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_SUM,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1004)

    tgsumm_iv = tgsumm_iv + MPI_WTIME()-tzero
    ngsumm_iv = ngsumm_iv + 1

End Subroutine gisum_vector

Subroutine gisum_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - integer scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Intent( InOut ) :: aaa

    Integer                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,MPI_INTEGER,MPI_SUM,mpi_comm_group,ierr)

    aaa = bbb

    tgsumm_is = tgsumm_is + MPI_WTIME()-tzero
    ngsumm_is = ngsumm_is + 1

End Subroutine gisum_scalar

Subroutine grsum_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision vector
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Real( Kind = wp ), Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return
    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    !TU*: Is it safe to throw errors here in MPI? Same for errors thrown below
    If (fail > 0) Call error(1005)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,mpi_wp,MPI_SUM,mpi_comm_group,ierr)
    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1006)

    tgsumm_rv = tgsumm_rv + MPI_WTIME()-tzero
    ngsumm_rv = ngsumm_rv + 1

End Subroutine grsum_vector

Subroutine gcsum_vector(aaa)!,num)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision vector
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
! amended   - Andrey Brukhno (amended for 'complex' vectors - working?)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Complex( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa
    !Integer, Intent( In ) :: num

    Integer :: n_l,n_u,n_s,fail,i

    Real( Kind = wp ), Dimension( : ), Allocatable :: bbb,ccc

    If( is_serial ) Return
    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    !Allocate (bbb(num), Stat = fail)
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1005)

    fail = 0
    !Allocate (ccc(num), Stat = fail)
    Allocate (ccc(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1005)

    n_s = Size(aaa, Dim = 1)
    !n_s = n_u - n_l !Size(aaa, Dim = 1)
    !if( n_s /= num ) then
    !    write(*,*)"Oooops! in gcsum_vector(aaa,num) n_s =?= num : ",n_s," =?= ",num
    !    return
    !    !n_s = num
    !endif

    do i = n_l, n_u
        bbb(i) = Dreal(aaa(i))
        ccc(i) = Aimag(aaa(i))
    enddo

    Call MPI_ALLREDUCE(MPI_IN_PLACE,bbb,n_s,mpi_wp,MPI_SUM,mpi_comm_group,ierr)
    Call MPI_ALLREDUCE(MPI_IN_PLACE,ccc,n_s,mpi_wp,MPI_SUM,mpi_comm_group,ierr)

    do i = n_l, n_u
        aaa(i) = CMPLX(bbb(i),ccc(i))
    enddo

    Deallocate (bbb, Stat = fail)

    !TU*: Restore these errors for allocation error?
    !If (fail > 0) Call error(1006)
    
    Deallocate (ccc, Stat = fail)
    !If (fail > 0) Call error(1006)

    tgsumm_cv = tgsumm_cv + MPI_WTIME()-tzero
    ngsumm_cv = ngsumm_cv + 1

End Subroutine gcsum_vector

Subroutine grsum_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global summation subroutine - working precision scalar
!                                         version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Intent( InOut ) :: aaa

    Real( Kind = wp )                  :: bbb

    If( is_serial ) Return
    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,mpi_wp,MPI_SUM,mpi_comm_group,ierr)

    aaa = bbb

    tgsumm_rs = tgsumm_rs + MPI_WTIME()-tzero
    ngsumm_rs = ngsumm_rs + 1

End Subroutine grsum_scalar

Subroutine gimax_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global maximum subroutine - integer vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                  :: n_l,n_u,n_s,fail

    Integer, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1007)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_MAX,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1008)

    tgimax_iv = tgimax_iv + MPI_WTIME()-tzero
    ngimax_iv = ngimax_iv + 1

End Subroutine gimax_vector

Subroutine gimax_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global maximum subroutine - integer scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Intent( InOut ) :: aaa

    Integer                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,MPI_INTEGER,MPI_MAX,mpi_comm_group,ierr)

    aaa = bbb

    tgimax_is = tgimax_is + MPI_WTIME()-tzero
    ngimax_is = ngimax_is + 1

End Subroutine gimax_scalar

Subroutine grmax_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global maximum subroutine - working precision vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Real( Kind = wp ), Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1009)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,mpi_wp,MPI_MAX,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1010)

    tgrmax_rv = tgrmax_rv + MPI_WTIME()-tzero
    ngrmax_rv = ngrmax_rv + 1

End Subroutine grmax_vector

Subroutine grmax_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global maximum subroutine - working precision scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Intent( InOut ) :: aaa

    Real( Kind = wp )                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,mpi_wp,MPI_MAX,mpi_comm_group,ierr)

    aaa = bbb

    tgrmax_rs = tgrmax_rs + MPI_WTIME()-tzero
    ngrmax_rs = ngrmax_rs + 1

End Subroutine grmax_scalar

Subroutine gimin_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global minimum subroutine - integer vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2007
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Dimension( : ), Intent( InOut ) :: aaa

    Integer                                  :: n_l,n_u,n_s,fail

    Integer, Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1007)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,MPI_INTEGER,MPI_MIN,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1008)

    tgimin_iv = tgimin_iv + MPI_WTIME()-tzero
    ngimin_iv = ngimin_iv + 1

End Subroutine gimin_vector

Subroutine gimin_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global minimum subroutine - integer scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2007
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Integer, Intent( InOut ) :: aaa

    Integer                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,MPI_INTEGER,MPI_MIN,mpi_comm_group,ierr)

    aaa = bbb

    tgimin_is = tgimin_is + MPI_WTIME()-tzero
    ngimin_is = ngimin_is + 1

End Subroutine gimin_scalar

Subroutine grmin_vector(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global minimum subroutine - working precision vector version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2007
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Dimension( : ), Intent( InOut ) :: aaa

    Integer                                            :: n_l,n_u,n_s,fail

    Real( Kind = wp ), Dimension( : ), Allocatable     :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    n_l = Lbound(aaa, Dim = 1)
    n_u = Ubound(aaa, Dim = 1)

    fail = 0
    Allocate (bbb(n_l:n_u), Stat = fail)
    If (fail > 0) Call error(1009)

    n_s = Size(aaa, Dim = 1)

    Call MPI_ALLREDUCE(aaa,bbb,n_s,mpi_wp,MPI_MIN,mpi_comm_group,ierr)

    aaa = bbb

    Deallocate (bbb, Stat = fail)
    If (fail > 0) Call error(1010)

    tgrmin_rv = tgrmin_rv + MPI_WTIME()-tzero
    ngrmin_rv = ngrmin_rv + 1

End Subroutine grmin_vector

Subroutine grmin_scalar(aaa)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 global minimum subroutine - working precision scalar version
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2007
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Real( Kind = wp ), Intent( InOut ) :: aaa

    Real( Kind = wp )                  :: bbb

    If( is_serial ) Return

    tzero = MPI_WTIME()

    Call MPI_ALLREDUCE(aaa,bbb,1,mpi_wp,MPI_MIN,mpi_comm_group,ierr)

    aaa = bbb

    tgrmin_rs = tgrmin_rs + MPI_WTIME()-tzero
    ngrmin_rs = ngrmin_rs + 1

End Subroutine grmin_scalar


Subroutine gtime(time)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 timing routine
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !use mpi_module, only : MPI_WTIME
    Implicit None

    Real( Kind = wp ), Intent(   Out ) :: time

!   real( Kind = wp ) :: MPI_WTIME

    Logical,           Save :: newjob
    Data                       newjob /.true./
    Real( Kind = wp ), Save :: tzero

    If (newjob) Then
       newjob = .false.

       tzero = MPI_WTIME()
       time = 0.0_wp
    Else
       time = MPI_WTIME()-tzero
    End If

End Subroutine gtime


subroutine msg_send_blocked_vector(msgtag, buf, length, proc_dest)

!*********************************************************************
!
!     double precision blocked message send
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!*********************************************************************

      use kinds_f90
      implicit none

      integer, intent (in) :: msgtag, length, proc_dest

      real(kind = wp), intent (inout) :: buf(:)

      integer :: len1, ierr

      tzero = MPI_WTIME()

      len1 = length
      if (proc_dest /= MPI_PROC_NULL) &
          call MPI_send(buf,len1,MPI_DOUBLE_PRECISION,proc_dest,msgtag,mpi_cart_comm,ierr)

      tcsend_rv = tcsend_rv + MPI_WTIME()-tzero
      ncsend_rv = ncsend_rv + 1

end subroutine


subroutine msg_send_blocked_scalar(msgtag, buf, proc_dest)

!*********************************************************************
!
!     double precision blocked message send
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!*********************************************************************

      use kinds_f90
      implicit none

      integer, intent (in) :: msgtag, proc_dest
      
      real(kind = wp), intent (inout) :: buf

      integer :: ierr

      tzero = MPI_WTIME()

      if (proc_dest /= MPI_PROC_NULL) &
          call MPI_send(buf, 1, MPI_DOUBLE_PRECISION, proc_dest,msgtag, mpi_cart_comm, ierr)
          !call MPI_send(buf,len1,MPI_DOUBLE_PRECISION,pe,msgtag, mpi_comm_group,ierr)

      tgsend_rs = tgsend_rs + MPI_WTIME()-tzero
      ngsend_rs = ngsend_rs + 1

end subroutine


subroutine msg_receive_blocked_vector(msgtag, buf, length)

!*********************************************************************
!
!     double precision blocked message receive
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!*********************************************************************

      use kinds_f90

      implicit none

      integer, intent (in) :: msgtag, length
      
      real(kind = wp), intent (inout) :: buf(:)

      integer :: len1, ierr

      tzero = MPI_WTIME()

      len1 = length
      call MPI_RECV(buf,len1,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,msgtag,mpi_cart_comm,status,ierr)

      tcrecv_rv = tcrecv_rv + MPI_WTIME()-tzero
      ncrecv_rv = ncrecv_rv + 1

end subroutine

subroutine msg_receive_blocked_scalar(msgtag, buf)

!*********************************************************************
!
!     double precision blocked message receive
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!*********************************************************************

      use kinds_f90

      implicit none

      integer, intent (in) :: msgtag
      
      real(kind = wp), intent (inout) :: buf

      integer :: ierr

      tzero = MPI_WTIME()

      call MPI_RECV(buf,1,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,msgtag,mpi_cart_comm,status,ierr)
      !call MPI_RECV(buf,len1,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,msgtag,mpi_comm_group,status,ierr)

      tgrecv_rs = tgrecv_rs + MPI_WTIME()-tzero
      ngrecv_rs = ngrecv_rs + 1

end subroutine


integer function msg_receive_unblocked(msgtag, buf, length)

!*********************************************************************
!
!     double precision blocked message receive
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!*********************************************************************

      use kinds_f90

      implicit none

      integer, intent (in) :: msgtag, length
      
      real(kind = wp), intent (inout) :: buf(:)
      
      integer :: len1, ierr, request

      tzero = MPI_WTIME()

      len1 = length
      call MPI_IRECV(buf,len1,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,msgtag,mpi_comm_group,request,ierr)

      msg_receive_unblocked=request

      tgrecv_ru = tgrecv_ru + MPI_WTIME()-tzero
      ngrecv_ru = ngrecv_ru + 1

end function

subroutine msg_wait(request)

!**********************************************************************
!
!     MPI routine to wait for requested unblocked message
!
!     copyright daresbury laboratory
!     author - w.smith october 2001
!
!     $Author: wl $
!     $Date: 2003/09/12 11:06:42 $
!     $Revision: 1.1.1.1 $
!     $State: Exp $
!
!**********************************************************************

      implicit none

      integer, intent (in) :: request

      integer :: ierr, status(MPI_STATUS_SIZE)

      tzero = MPI_WTIME()

      call MPI_WAIT(request,status,ierr)

      twait = twait + MPI_WTIME()-tzero

end subroutine


subroutine split_com(colour, key, mpi_comm_new)

    use constants_module, only : uout

    implicit none

    integer, intent(in) :: colour, key
    integer, optional, intent(inout) :: mpi_comm_new

    integer:: ierr

    if( present(mpi_comm_new) ) then

        call mpi_comm_split(MPI_COMM_WORLD, colour, key, mpi_comm_new, ierr)

    else

        call mpi_comm_split(MPI_COMM_WORLD, colour, key, mpi_comm_group, ierr)

    end if

    if( ierr /= 0 ) &
        call cry(uout,'',"ERROR: failed to split MPI_COMM_WORLD into (sub-) groups",999)

end subroutine


subroutine create_cartesian_topology(idgrp, numreplicas, grpsize)

    use constants_module, only : uout

    implicit none

    integer, intent(in) :: idgrp,  numreplicas, grpsize

    integer UP, DOWN
    parameter(UP=1)
    parameter(DOWN=2)

    integer source, dest, outbuf, i, tag, ierr,  &
           inbuf(4), dims(2), coords(2),  &
           stats(MPI_STATUS_SIZE, 8), reqs(8), cartcomm,  &
           periods(2), reorder
    data inbuf /MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,  &
        MPI_PROC_NULL/,  tag /1/, periods /0,0/, reorder /0/


    ! thh dimensions of the cartesian grid topology
    dims(1) = numreplicas
    dims(2) = grpsize

    !create the two dimensional grid communicator
    call MPI_CART_CREATE(MPI_COMM_WORLD, 2, dims, periods, reorder, mpi_cart_comm, ierr)

    !obtain the coordinates of the node in the new commincator
    call MPI_CART_COORDS(mpi_cart_comm, idnode, 2, cart_coords, ierr)
    call MPI_CART_SHIFT(mpi_cart_comm, 0, 1, map_temp_down,  map_temp_up, ierr)

!debugging-<<<
    !write(uout,"(/,/,1x,'idnode = ',I3,' group ',i3,' coords = ',I2,I2, ' neighbors(T_down, T_up) = ',I3,I3)") &
    !      idnode,idgrp, cart_coords(1),cart_coords(2), map_temp_down,  map_temp_up
    !flush(6)
!debugging->>>

end subroutine


End Module comms_mpi_module
