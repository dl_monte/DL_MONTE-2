!***************************************************************************
!   Copyright (C) 2018 by T L Underwood & Nishant Birdi                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Module containing procedures for calculating liquid crystal order 
!>   parameters
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using
!> - `kinds_f90`

module lc_order_module

    use kinds_f90

    implicit none


contains


    !TO DO: Refactor this so that it takes the configuration instead of the box number
    !       as an argument? This would be cleaner.


    !> Calculates the director (average orientation vector) of all spins in the specified box.
    !> For now this procedure considers only atoms in molecule 1, and assumes that they all
    !> have meaningful spins (i.e. of magnitude 1). Safety checks performed elsewhere should
    !> make sure that this is a sensible thing to do!
    function director(ib)

        use cell_module

        implicit none
        
            !> Box number
        integer, intent(in) :: ib
    
        real(kind=wp) :: director(3), spin(3)
        
        integer :: i,j

        director = 0.0_wp

        ! TEMPORARY: I only consider molecule 1 and assume all have menaingful orientations
        ! Loop over all atoms in molecule 1, and sum their orientations
        j = 1
        do i = 1, cfgs(ib)%mols(j)%natom

            spin = cfgs(ib)%mols(j)%atms(i)%spin
            
            ! Map orientations in the z<0 half-space to the z>0 half-space
            if(spin(3) < 0.0_wp) then

                spin = -spin

            end if

            director = director + spin
            
        end do

        director = director / sqrt( dot_product(director,director) )
        
    end function director



    !> Calculates the scalar nematic order parameter for all spins in the specified box.
    !> For now this procedure considers only atoms in molecule 1, and assumes that they all
    !> have meaningful spins (i.e. of magnitude 1). Safety checks performed elsewhere should
    !> make sure that this is a sensible thing to do!
    real(kind=wp) function scalar_nematic_op(ib, n)

        use cell_module
        
            !> Box number
        integer, intent(in) :: ib

            !> Number of the Legendre polynomial to consider - 2 or 4
        integer, intent(in) :: n

        ! Director
        real(kind=wp) :: d(3)
        
        real(kind=wp) :: t2, t4, sum2, sum4

        integer :: i, j
        
        sum2 = 0.0_wp
        sum4 = 0.0_wp
        
        ! Get the director
        d = director(ib)
        
        ! TEMPORARY: I only consider molecule 1 and assume all have menaingful orientations
        ! Loop over all atoms in molecule 1, and sum their orientations
        j = 1
        do i = 1, cfgs(ib)%mols(j)%natom

            t2 = dot_product(d, cfgs(ib)%mols(j)%atms(i)%spin)**2

            sum2 = sum2 + t2

            if(n == 4) then
                
                t4 = t2*t2
                sum4 = sum4 + t4
            
            end if
            
        end do

        sum2 = sum2 / cfgs(ib)%mols(j)%natom
        sum4 = sum4 / cfgs(ib)%mols(j)%natom

        select case(n)
        case(2)
        
            scalar_nematic_op = (3.0_wp * sum2 - 1.0_wp) * 0.5_wp
    
        case(4)
     
            scalar_nematic_op = (35.0_wp * sum4 - 30.0_wp * sum2 + 3.0_wp) * 0.125_wp 

        case default

            ! TO DO: THROW AN ERROR HERE
            
        end select

    end function scalar_nematic_op



end module lc_order_module
