! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! ***************************************************************************

!> @brief
!> - Atom type initialisation and manipulation
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90` (within routines)
!> - `atom_type` (within routines)

!> @modulefor atom type(s)

module atom_module

    implicit none

contains

!> @brief
!> - initialises atom '`atm`' to a particular type (`atmtype`')
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_type(atm, k, atm_charge, atm_mass, atmtype)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to be set up
    type(atom), intent(inout) :: atm

        !> atom index/label
    integer, intent(in) :: k

        !> atom type identifier
    integer, intent(in) :: atmtype

        !> charge and mass to set for the atom
    real (kind = wp), intent(in) :: atm_charge, atm_mass


    atm%charge = atm_charge
    atm%mass = atm_mass
    atm%atlabel = k
    atm%atype = atmtype

    atm%is_charged = ( abs(atm_charge) > 1.0e-8_wp )

end subroutine

!> @brief
!> - copies/imposes atom type of atom '`at2`' onto atom '`at1`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine copy_atom_type(at1, at2)

    use kinds_f90
    use atom_type

    implicit none

        !> atom type acceptor (structure)
    type(atom), intent(out) :: at1

        !> atom type donor (structure)
    type(atom), intent(in) :: at2

    at1%atlabel = at2%atlabel
    at1%charge = at2%charge
    at1%mass = at2%mass
    at1%site = at2%site
    at1%atype = at2%atype

    at1%is_charged = at2%is_charged

end subroutine

!> @brief
!> - initialises atom '`atm`' position to vector '`pos`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_pos(atm, pos)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> new position for the atom
    real (kind = wp), intent(in) :: pos(3)

    atm%rpos(:) = pos(:)

end subroutine set_atom_pos

!> @brief
!> - initialises atom '`atm`' reference (zero) position to vector '`pos`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_zero_pos(atm, pos)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> new ref. (zero) position for the atom
    real (kind = wp), intent(in) :: pos(3)


    if(allocated(atm%r_zero)) atm%r_zero(:) = pos(:)

end subroutine

!> @brief
!> - (re)sets atom '`atm`' COM identifier `idcom`
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_idcom(atm, k)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> atom site identifier
    integer, intent(in) :: k

    atm%idcom = k

end subroutine

!> @brief
!> - (re)sets atom '`atm`' site attribute to `k`
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_site(atm, k)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> atom site identifier
    integer, intent(in) :: k

    atm%site = k

end subroutine

!> @brief
!> - prints out atom '`atm`' position vector into file unit '`k`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine write_atom_pos(atm, k)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> file unit to write into
    integer, intent(in) :: k

    write(k, '(3f15.7,2x,i4)') atm%rpos(1:3), atm%site

end subroutine

!> @brief
!> - swaps atom types between atom '`at1`' and atom '`at2`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine swap_atom_types(at1, at2)

    use kinds_f90
    use atom_type

    implicit none

        !> atoms (structures) to swap types between
    type(atom), intent(inout) :: at1, at2

    integer :: tm, tsite, tatype, tlabel
    logical :: this_charged

    real(kind = wp) :: tmass, tcharge

    tlabel = at1%atlabel
    tatype = at1%atype
    tcharge = at1%charge
    tmass = at1%mass
    tsite = at1%site
    this_charged = at1%is_charged

    at1%atlabel = at2%atlabel
    at1%charge = at2%charge
    at1%mass = at2%mass
    at1%site = at2%site
    at1%atype = at2%atype
    at1%is_charged = at2%is_charged

    at2%atlabel = tlabel
    at2%charge = tcharge
    at2%mass = tmass
    at2%site = tsite
    at2%atype = tatype
    at2%is_charged = this_charged

end subroutine


!> @brief
!> - swaps atom postions between atom '`at1`' and atom '`at2`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine swap_atom_positions(at1, at2)

    use kinds_f90
    use atom_type

    implicit none

        !> atoms (structures) to swap postions between
    type(atom), intent(inout) :: at1, at2

        !> atom position
    real (kind = wp) ::   trpos(3)

        !> stored atom position
    real (kind = wp) ::   store_trpos(3)

        !> atom position in the neighbour-list (auto-updated)
    real (kind = wp), dimension(:), allocatable ::   tr_zero

        !> Position of lattice site corresponding to this atom - for PSMC
    real (kind = wp) :: trpsmcsite(3)

        !> Displacement of the atom from the lattice site - for PSMC
    real (kind = wp) :: trpsmcu(3)

        !> Atom's cell
    integer :: tcell

        !> array sizes
    integer :: size1 !, size2

    trpos = at1%rpos
    store_trpos = at1%store_rpos
    trpsmcsite = at1%psmcsite
    trpsmcu = at1%psmcu
    tcell = at1%cell

    at1%rpos = at2%rpos
    at1%store_rpos = at2%store_rpos
    at1%psmcsite = at2%psmcsite
    at1%psmcu = at2%psmcu
    at1%cell = at2%cell

    at2%rpos = trpos
    at2%store_rpos = store_trpos
    at2%psmcsite = trpsmcsite
    at2%psmcu = trpsmcu
    at2%cell = tcell

    if( allocated(at1%r_zero) .and. allocated(at2%r_zero) ) then
        !size1 = size(at1%r_zero)
        !size2 = size(at2%r_zero)

        size1 = min(size(at1%r_zero),size(at2%r_zero))

        allocate(tr_zero(size1))

        tr_zero(:size1)    = at1%r_zero(:size1)
        at1%r_zero(:size1) = at2%r_zero(:size1)
        at2%r_zero(:size1) = tr_zero(:size1)

        deallocate(tr_zero)
    endif

end subroutine

!> @brief
!> - copies all attributes of atom '`at2`' to atom '`at1`'
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine copy_atom(at1, at2)

    use kinds_f90
    use atom_type

    implicit none

        !> atom acceptor (structure)
    type(atom), intent(out) :: at1

        !> atom donor (structure)
    type(atom), intent(in) :: at2

    at1%atlabel = at2%atlabel
    at1%charge = at2%charge
    at1%mass = at2%mass
    at1%site = at2%site
    at1%atype = at2%atype

    at1%is_charged = at2%is_charged

    at1%rpos(:) = at2%rpos(:)

    if (allocated(at1%r_zero)) at1%r_zero(:) = at2%r_zero(:)

    at1%spin = at2%spin
    at1%cell = at2%cell

end subroutine

!> @brief
!> - resets atom '`at`' attributes to the 'clear'/'zero' state
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine zero_atom(at)

    use kinds_f90
    use atom_type

    implicit none

        !> atom to be cleared out
    type(atom), intent(inout) :: at

    at%atlabel = -1
    at%site = 0
    at%atype = 0
    at%idcom = 0
    at%rpos(:) = 0.0_wp
    at%mass = 0.0_wp
    at%charge = 0.0_wp

    at%is_charged = .false.

    if (allocated(at%r_zero)) at%r_zero(:) = 0.0_wp

end subroutine

!> @brief
!> - stores atom '`at1`' position for restoring it later (within the atom structure) 
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine store_atom_pos(at1)

    use kinds_f90
    use atom_type

    implicit none

        !> atom to work on
    type(atom), intent(inout) :: at1

    at1%store_rpos(:) = at1%rpos(:)

end subroutine

!put the atom position back
!> @brief
!> - restores atom '`at1`' position from that stored earlier (within the atom structure) 
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine restore_atom_pos(at1) 

    use kinds_f90
    use atom_type

    implicit none

        !> atom to work on
    type(atom), intent(inout) :: at1
    
    at1%rpos(:) = at1%store_rpos(:)

end subroutine


!> @brief
!> - Sets the spin of atom '`atm`' to vector '`spin`'
!> @using
!> - `kinds_f90`
!> - `atom_type`
subroutine set_atom_spin(atm, spin)

    use kinds_f90
    use atom_type

    implicit none

        !> atom (structure) to work on
    type(atom), intent(inout) :: atm

        !> new spin for the atom
    real (kind = wp), intent(in) :: spin(3)

    atm%spin(:) = spin(:)

end subroutine

!> @brief
!> - Stores the spin of atom '`at1`' for restoring it later (within the atom structure) 
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine store_atom_spin(at1)

    use kinds_f90
    use atom_type

    implicit none

        !> atom to work on
    type(atom), intent(inout) :: at1

    at1%store_spin(:) = at1%spin(:)

end subroutine

!> @brief
!> - Restores the spin of atom '`at1`' to that stored earlier (within the atom structure) 
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine restore_atom_spin(at1) 

    use kinds_f90
    use atom_type

    implicit none

        !> atom to work on
    type(atom), intent(inout) :: at1
    
    at1%spin(:) = at1%store_spin(:)

end subroutine


!> @brief
!> - Rotates the spin of atom '`at1`' by the specified rotation matrix
!> @using 
!> - `kinds_f90`
!> - `atom_type`
subroutine rotate_spin_by(at1, rot) 

    use kinds_f90
    use atom_type

    implicit none

        !> atom to work on
    type(atom), intent(inout) :: at1

        !> rotation matrix, as a list of 9 reals
    real(kind=wp), intent(in) :: rot(9)

    real(kind=wp) :: xx, yy, zz
    
    xx = at1%spin(1)
    yy = at1%spin(2)
    zz = at1%spin(3)
    
    at1%spin(1) = rot(1) * xx + rot(4) * yy + rot(7) * zz
    at1%spin(2) = rot(2) * xx + rot(5) * yy + rot(8) * zz
    at1%spin(3) = rot(3) * xx + rot(6) * yy + rot(9) * zz
    
end subroutine rotate_spin_by


end module
