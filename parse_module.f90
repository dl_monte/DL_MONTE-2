! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! ***************************************************************************

!*!!!!!!!!!!!!!!!!!!!!!
! Originally based on !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 module containing tools for parsing textual input
!
! copyright - daresbury laboratory
! author    - i.t.todorov may 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Module parse_module

  Implicit None

  Public :: int_2_char3, int_2_word, real_2_word, indexnum, &
            get_line, strip_blanks, lower_case, get_word, word_2_real

Contains


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_monte_2 routine to convert a number, 0 <= num <= 999, 
! to character*3 string, for using as part of file names
!
! copyright - daresbury laboratory 2015
! author    - a.v.brukhno November 2015
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Subroutine int_2_char3(num, word, safe)

    Implicit None

    Integer,              Intent( In    ) :: num
    Character( Len = 3 ), Intent(   Out ) :: word
    Logical,              Intent(   Out ) :: safe

    character :: chNum*3

    safe = .false.

    word = ''

    if( num < 0 .or. num > 999 ) return

    write(chNum,'(i3)')num

    word = adjustL(chNum)

    if( num < 100 ) word = '0'//trim(word)
    if( num < 10  ) word = '0'//trim(word)

    safe = .true.

    return

  End Subroutine int_2_char3

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_monte_2 routine to convert an integer number to a character word
! i.e. a string of max 20 characters, for using in cry(...) messages
!
! copyright - daresbury laboratory 2018
! author    - a.v.brukhno    March 2018
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Character(20) Function int_2_word(num)

    Implicit None

    Integer, Intent( In    ) :: num

    character(20) :: iword

    write(iword,*)num

    int_2_word = trim(adjustL(iword))

    return

  End Function int_2_word

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_monte_2 routine to convert a real(wp) figure to a character word
! i.e. a string of max 20 characters, for using in cry(...) messages
!
! copyright - daresbury laboratory 2018
! author    - a.v.brukhno    March 2018
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Character(32) Function real_2_word(rnum)

    use kinds_f90

    Implicit None

    Real(kind=wp), Intent( In    ) :: rnum

    character(32) :: rword

    write(rword,*)rnum

    real_2_word = trim(adjustL(rword))

    return

  End Function real_2_word


  !> returns the first digit in the string, if any (otherwise `0`); 'isflt = .true.` if found a floating point figure
  Integer Function indexnum(string,isflt)

!***********************************************************************
!     
!     DL_MONTE_2 routine to find numerical data within a string
!
!     Find the first digit in the string, if any.
!     If found, analyse the characters around it to figure out 
!     if the figure complies with any floating point format(s).
!     Include the minus sign, if any.
!
!     copyright - daresbury laboratory 2013
!     author    - Andrey Brukhno   May 2013
!     
!***********************************************************************

      implicit none

      !AB: *** revisit to amend for complying with F90 specs ***

      character*(*) string
      character*1 char1/''/
      integer i,lstr,ib,ie

      logical isflt

      isflt = .false.

      indexnum=0

      lstr=len(trim(string))
      do i=1,lstr

        char1 = string(i:i)

        if( char1.eq.'0' ) then
          indexnum = i
          exit
        elseif( char1.eq.'1' ) then
          indexnum = i
          exit
        elseif( char1.eq.'2' ) then
          indexnum = i
          exit
        elseif( char1.eq.'3' ) then
          indexnum = i
          exit
        elseif( char1.eq.'4' ) then
          indexnum = i
          exit
        elseif( char1.eq.'5' ) then
          indexnum = i
          exit
        elseif( char1.eq.'6' ) then
          indexnum = i
          exit
        elseif( char1.eq.'7' ) then
          indexnum = i
          exit
        elseif( char1.eq.'8' ) then
          indexnum = i
          exit
        elseif( char1.eq.'9' ) then
          indexnum = i
          exit
        endif

      enddo

      if( indexnum.gt.0 ) then

        ib = indexnum+1

        i = indexnum-1
        if(i.gt.0) then

          char1 = string(i:i)

! AB: check for the floating point before the figure
          if( char1.eq.'.' ) then 

            isflt = .true.
            indexnum = i
            i = i-1
            if(i.lt.1) return
            
            char1 = string(i:i)
            
          endif

! AB: check for the 'minus' sign before the figure ('plus' doesn't matter)
          if( char1.eq.'-' ) then 

            indexnum = i
            if( isflt ) return

          endif

        endif

! AB: the beginning has been found, but floating point remains unclear

        lstr = index(string(ib:),' ')-1
        if( lstr.lt.1 ) return
! AB: blank right after the first digit found, so it's done

        lstr = ib+lstr-1
        ie   = lstr-1

! AB: check for the floating point in the figure (before the next blank space)
        if( index(string(ib:lstr),'.').gt.0 ) then
          isflt = .true.
          return
        elseif(ie.gt.ib-1) then

          if( index(string(ib:ie),'e').gt.0 ) then
! AB: check for the exponent in the figure (third before the last non-blank/space)
            isflt = .true.
            return
          elseif( index(string(ib:ie),'E').gt.0 ) then
            isflt = .true.
            return
          elseif( index(string(ib:ie),'d').gt.0 ) then
            isflt = .true.
            return
          elseif( index(string(ib:ie),'D').gt.0 ) then
            isflt = .true.
            return
          endif

        endif

      endif

      return

  End Function indexnum


  Subroutine get_line(safe,ifile,record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to read a character string on node zero and
! broadcast it to all other nodes
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    use constants_module, only : uout, ucfg
    use comms_mpi_module, Only : master, is_parallel, idnode, gsync,gcheck,gsum

    Implicit None

    Logical,              Intent(   Out ) :: safe
    Integer,              Intent( In    ) :: ifile
    Character( Len = * ), Intent(   Out ) :: record

    Integer                              :: i,fail,rec_len
    Integer, Dimension( : ), Allocatable :: line
    logical :: communicator

    rec_len = Len(record)

    fail = 0
    Allocate (line(1:rec_len), Stat = fail)
    If (fail > 0) Call error(1011)

    record = ' '
    safe = .true.

    !TU**: We plan to broadcast to other nodes if we are node 0...
    communicator = (idnode == 0)
    !TU**: ... unless we are reading the config file, in which case we broadcast if we are master. This is
    !TU**: to account for replica exchange where multiple config files are read?
    if( ifile == ucfg ) communicator = master

    !if (idnode == 0) communicator = .true.
    !communicator = master

    !TU**: Profiling reveals that this line needlessly slows down code for the parallel version.
    !TU**: It is redundant anyway?
    If (is_parallel) Call gsync()

    If (communicator) Then

       Read(ifile,'(a)', End = 100) record

       If (is_parallel) Then
 
          !TU**: Convert the line (i.e. record) into an integer-array representation
          Do i=1,rec_len
             line(i) = Ichar(record(i:i))
          End Do

          !TU**: How could 'safe' be false here??
          Call gcheck(safe)

          !TU**: Here MPI_ALLREDUCE is used to sum 'line' from all nodes and broadcast the result to others in the workgroup. 
          !TU**: This is only correct if 'line' is 0 for non-communicator nodes, which is done below in 'else'. What about
          !TU**: if we are reading CONTROL vs CONFIG?
          Call gsum(line)

       End If

       Go To 200

    !TU**: The next 3 lines of code are inaccessible!
100    safe = .false.

       If (is_parallel) Call gcheck(safe)
       If (.not.safe) Go To 200

    Else

       !TU**: If we are not a communicator node...


       !TU**: How could 'safe' be false here??
       Call gcheck(safe)
       If (.not.safe) Go To 200

       line = 0

       Call gsum(line) 

       !TU*: Convert the integer-representation of the line into a record
       Do i=1,rec_len
          record(i:i) = Char(line(i))
       End Do

!print*,'gline',idnode,record ; flush(6)
    End If

200 Continue

    Deallocate (line, Stat = fail)
    If (fail > 0) Call error(1012)

  End Subroutine get_line

  Subroutine strip_blanks(record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to strip blanks from both ends of string
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Character( Len = * ), Intent( InOut ) :: record

    Integer :: i,j,k

    record = Trim(record)

    k = Len(record)
    j = 0

    !TU**: Added by me... Convert any tab characters (archar(9)) into 
    !TU**: spaces first, before stripping them below. Otherwise they 
    !TU**: will not be treated as whitespace.
    Do i=1,k
        If (record(i:i) == achar(9) ) Then
            record(i:i)=' '
        End If
    End do

    Do i=1,k
       If (record(i:i) /= ' ') Then
          j=i
          Go To 10
       End If
    End Do

10  Continue

    If (j > 1) Then
       Do i=j,k
          record(i-j+1:i-j+1) = record(i:i)
          record(i:i) = ' '
       End Do
       record = Trim(record)
    End If

  End Subroutine strip_blanks

  Subroutine lower_case(record)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to lower the character case of a string.
! Transportable to non-ASCII machines
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Character( Len = * ), Intent( InOut ) :: record

    Integer :: i

    Do i=1,Len(record)
       If (record(i:i) == 'A') Then
          record(i:i) = 'a'
       Else If (record(i:i) == 'B') Then
          record(i:i) = 'b'
       Else If (record(i:i) == 'C') Then
          record(i:i) = 'c'
       Else If (record(i:i) == 'D') Then
          record(i:i) = 'd'
       Else If (record(i:i) == 'E') Then
          record(i:i) = 'e'
       Else If (record(i:i) == 'F') Then
          record(i:i) = 'f'
       Else If (record(i:i) == 'G') Then
          record(i:i) = 'g'
       Else If (record(i:i) == 'H') Then
          record(i:i) = 'h'
       Else If (record(i:i) == 'I') Then
          record(i:i) = 'i'
       Else If (record(i:i) == 'J') Then
          record(i:i) = 'j'
       Else If (record(i:i) == 'K') Then
          record(i:i) = 'k'
       Else If (record(i:i) == 'L') Then
          record(i:i) = 'l'
       Else If (record(i:i) == 'M') Then
          record(i:i) = 'm'
       Else If (record(i:i) == 'N') Then
          record(i:i) = 'n'
       Else If (record(i:i) == 'O') Then
          record(i:i) = 'o'
       Else If (record(i:i) == 'P') Then
          record(i:i) = 'p'
       Else If (record(i:i) == 'Q') Then
          record(i:i) = 'q'
       Else If (record(i:i) == 'R') Then
          record(i:i) = 'r'
       Else If (record(i:i) == 'S') Then
          record(i:i) = 's'
       Else If (record(i:i) == 'T') Then
          record(i:i) = 't'
       Else If (record(i:i) == 'U') Then
          record(i:i) = 'u'
       Else If (record(i:i) == 'V') Then
          record(i:i) = 'v'
       Else If (record(i:i) == 'W') Then
          record(i:i) = 'w'
       Else If (record(i:i) == 'X') Then
          record(i:i) = 'x'
       Else If (record(i:i) == 'Y') Then
          record(i:i) = 'y'
       Else If (record(i:i) == 'Z') Then
          record(i:i) = 'z'
       End If
    End Do

  End Subroutine lower_case

  Subroutine get_word(record,word)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 subroutine to transfer a word from a string
!
! record loses a word and leading blanks
! word fills up with the word as much as it can contain
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Implicit None

    Character( Len = * ), Intent( InOut ) :: record
    Character( Len = * ), Intent(   Out ) :: word

    Logical :: transfer 
    Integer :: rec_len,word_len,rec_ind,word_ind

! Strip blanks in record

    Call strip_blanks(record)

! Get record and word lengths

    rec_len  = Len_Trim(record)
    word_len = Len(word)

! Initialise counters and word, and keep-transfering boolean

    rec_ind  = 0
    word_ind = 0

    word     = ' '

    transfer = .true.

! Start transfering

    Do While (transfer)

! Check for end of record

       If (rec_ind < rec_len) Then

          rec_ind = rec_ind + 1

! Check for end of word in record

          If (record(rec_ind:rec_ind) == ' ') transfer = .false.

       Else

          transfer = .false.

       End If

! Transfer in word if there is space in word and transfer is true

       If (word_ind < word_len .and. transfer) Then

          word_ind = word_ind + 1

          word(word_ind:word_ind) = record(rec_ind:rec_ind)

          record(rec_ind:rec_ind) = ' '

       Else

! Transfer to nothing if there is no space in word and transfer is true

          If (transfer) record(rec_ind:rec_ind) = ' '

       End If

    End Do

  End Subroutine get_word

  Function word_2_real(word)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 function for extracting real numbers from a character string
! with no blanks between the charaters of the number.
!
! (1) Numbers as 2.0e-3/3.d-04 are processible as only one slash is 
!     permited in the string!
! (2) Numbers cannot start or finish with a slash!
! (3) A blank string is read as zero!
! (4) Numbers must sensible!
!
! copyright - daresbury laboratory
! author    - i.t.todorov june 2004
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    use kinds_f90
    use constants_module, only : uout
    use comms_mpi_module, only : master

    Implicit None

    Character( Len = * ), Intent( In    ) :: word

    Character( Len = 20 ) :: forma
    Integer               :: word_end,slash_position
    Real( kind = wp )     :: word_2_real,denominator

    denominator = 1.0_wp

    word_end = Len_Trim(word)
    slash_position = Index(word,'/')

    If (word_end /= 0) Then
       If (slash_position == 1 .or. slash_position == word_end) Go To 30
    Else
       word_2_real = 0.0_wp
       Return
    End If

    If (slash_position > 0) Then
       forma = ' '
       Write(forma, 20) word_end - slash_position
       Read(word(slash_position + 1:word_end), forma, Err = 30) denominator
       word_end = slash_position - 1
    End If

    forma = ' '
    Write(forma,20) word_end
    Read(word(1:word_end), forma, Err = 30) word_2_real
    word_2_real = word_2_real / denominator

    Return

20  Format('(f',i4,'.0)')
30  Continue
    If (master) Write(uout,'(1x,3a)') &
       "*** warning - word_2_real exepected to read a number but found # ", word(1:word_end), " # ***"
    Call error(1)

  End Function word_2_real

  Function truncate_real(r)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! dl_poly_3 function for truncating real numbers to the approximate
! precision in decimal digits for the +/-0.___E+/-___ representation,
! which is 2*kind(real)-1 or 2*wp-1, as defined by kinds_f90 (wp)
!
! copyright - daresbury laboratory
! author    - i.t.todorov october 2005
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Use kinds_f90

    Implicit None

    Real( Kind = wp ), Intent( In    ) :: r

    Logical               , Save :: newjob
    Data                            newjob       / .true. /
    Character( Len = 20  ), Save :: forma
    Integer               , Save :: k

    Character( Len = 100 ) :: word
    Integer                :: e_position,word_end,i
    Real( Kind = wp )      :: truncate_real

    If (newjob) Then
       newjob = .false.

       forma = ' '
       k = 2*wp + 2

       Write(forma ,10) k+7,k
10     Format('(0p,e',i4,'.',i4,')')
    End If

    word = ' '
    Write(word,forma) r
    word_end = Len_Trim(word)
    e_position = 0
    e_position = Index(word,'E')
    If (e_position == 0) e_position = Index(word,'e')
    Do i=e_position-3,word_end
       If (i+3 <= word_end) Then
          word(i:i)=word(i+3:i+3)
       Else
          word(i:i)=' '
       End If
    End Do

    Call strip_blanks(word)
    truncate_real=word_2_real(word)

  End Function truncate_real

End Module parse_module
