! ***************************************************************************
! *   Copyright (C) 2007 - 2015 by J.A.Purton                               *
! *   john.purton[@]stfc.ac.uk                                              *
! *                                                                         *
! *   Contributors:                                                         *
! *   -------------                                                         *
! *   A.V.Brukhno - Free Energy Diff, planar pore & algorithm optimizations *
! *   andrey.brukhno[@]stfc.ac.uk abrukhno[@]gmail.com                      *
! *                                                                         *
! *   T.L.Underwood - Lattice/Phase-Switch MC method & optimizations        *
! *   t.l.Underwood[@]bath.ac.uk                                            *
! ***************************************************************************

!> @brief
!> - Simulation control: setting defaults, reading CONTROL file, setting switches & parameters
!> @usage 
!> - most of the control is done via `<control_type>` instance `'job'` normally
!> - @stdusage 
!> - @stdspecs
!> @using 
!> - `kinds_f90`

!> @modulefor simulation control: setting defaults & reading CONTROL file

module control_module

    use kinds_f90

    implicit none


contains


!> @brief
!> - Initialisation of default values in the control structure `job`
!> @using
!> - `kinds_f90`
!> - `constants_module`
!> - `control_type`

!> initialise the default values of control switches and parameters (job structure)
subroutine initialise_defaults(job, nconfigs, shortcut, vdwrcut, eunit)

    use kinds_f90
    use constants_module
    use control_type

        !> structure containing global simulation control switches & parameters
    type(control), intent(inout) :: job

        !> number of configurations (microstates) read in from CONFIG file
    integer, intent(in) :: nconfigs

        !> spherical cutoff radius (global and VDW)
    real (kind = wp), intent(in):: shortcut, vdwrcut

        !> energy unit to be used
    real (kind = wp), intent(in):: eunit

    job % nconfigs = nconfigs
    job % numreplicas = 1

    job % is_slit = .false.
    job % is_XY_npt = .false.
    job % moveatm = .false.
    job % movemol = .false.
    job % rotatemol = .false.
    job % swapatoms = .false.
    job % swapmols = .false.
    job % thintegration = .false.
    job % semiwidomatms = .false.
    job % semigrandatms = .false.
    job % semigrandmols = .false.
    job % substitute = .false.
    job % perturbation = .false.
    job % exchangebias = .false.
    job % usecavitybias = .false.
    job % useobias_rot = .false.
    job % useobias_gcmc = .false.
    job % useorthogonal = .false.
    job % useddmc = .false.
    job % include_mol = .false.
    job % scalevec = .false.
    job % calcstress = .false.
    job % calcvel = .false.
    job % calcforce = .false.
    job % calcforce = .true.
    job % gcmcatom = .false.
    job % gcmcmol = .false.
    job % ewald_alpha_given = .false.
    job % scalesize = .false.
    job % uselist = .false.
    job % lauto_nbrs = .false.
    job % sysrestart = .false.              ! default = no restart

    job % cavity_grid = .true.
    job % cavity_mesh = .false.
    job % cavity_x = 0.5_wp
    job % cavity_y = 0.5_wp
    job % cavity_z = 0.5_wp
    job % cavity_radius = 1.0_wp
    job % cavity_update = 1000

    !job % hist_molids = ""

    allocate(job % coultype(nconfigs))
    job % cell_type = 0                     ! 0 -> 3D (bulk), less than 0 -> 2D-slit (panar pore)
    job % coultype = 1                      ! accounting for Coulomb interactions: 1(Ewald)/2(direct)/3(shifted)/4(shifted+damped)
    job % ewald_alpha = 0.0_wp
    job % maxvolchange = 0.001_wp           ! maximum vol change in monte carlo
    job % vol_bin = 0.0_wp                  ! volume bin size for V-grid in NpT, default: 0.0 (NVT)
    job % vol_bin_power = 1.0_wp            ! power/exponent for bin growth on V-grid in NpT, [1 .. 2] default: 1.0 (linear)
    job % nbrlist  = 50                     ! stride (frequency) of nbr list updates
    job % thblist  = 50                     ! stride (frequency) of thb list updates
    job % statsout = 1000                   ! print stats data every n-th iteration, default: 1000
    job % sysprint = 1000                   ! print data every nth iteration, default: 1000
    job % sysdump  = 1000                   ! dump coords/etc every n-th it, default: 1000
    job % syscheck = 10000                  ! check & print accumulated vs recalculated energy, default: 10000

    job % revcon_format  = DLM ! = 0        ! output format for REVCON: [DLM/DLP]* / [DLP-2/4]
    job % archive_format = DLM ! = 0        ! output format for trajectory: ARCHIVE[DLM/DLP]* / HISTORY[DLP-2/4] / TRAJECTORY[DCD] 
    job % traj_format    = DLM ! = 0        ! input format for trajectory cvonversion: HISTORY[DLP-2/4] / TRAJECTORY[DCD]
    job % traj_beg  = 0                     ! first frame to consider for trajectory cvonversion
    job % traj_end  = 0                     ! last frame to consider for trajectory cvonversion
    job % traj_step = 0                     ! frames skip-step for trajectory cvonversion
    job % dcd_form  = 0                     ! by default store the DCD frames in single precision
    job % dcd_cell  = 2                     ! by default store the PBC cell details with DCD frames (orthorhomibc by default)
    job % nfltprec  = 0

    job % maxiterations = 0                 ! no of iterations
    job % sysequil = 0                      ! no equilibarion steps
    job % sysheat = -1

    job % energyunit = eunit                ! define default unit as ev
    job % systemp= 0.0_wp                   ! system temperature
    job % syspres= 0.001_wp                 ! system pressure, default: 1 atm
    job % madelungacc = 1.0e-6_wp           ! accuracy of madelung sum
    job % dielec = 1.0_wp
    job % verletshell = 2.0_wp              ! verlet skin distance (ang)
    job % iondisp = 0.1_wp                  ! maximum displace of ion
    job % moldisp = 0.1_wp
    job % molrot = 1.0_wp
    job % shortrangecut = shortcut          !AB: global cutoff radius
    job % vdw_cut = vdwrcut                 !AB: it might have been set in FIELD ('vdw') or later in CONTROL ('vdwcut')
    !job % vdw_cut = -1.0_wp
    job % acc_atmmve_ratio = 0.37_wp
    job % acc_molmve_ratio = 0.37_wp
    job % acc_molrot_ratio = 0.37_wp
    job % acc_vol_ratio = 0.37_wp
    job % acc_atmmve_update = 100 !0
    job % acc_molmve_update = 100 !0
    job % acc_molrot_update = 100 !0
    job % acc_vol_update = 100 !0
    job % max_thb_nbrs =  6
    job % max_nonb_nbrs = 0	
    job % stacksize = 100
    job % simulationmode = "mc"             ! simulation mode
    job % type_vol_move = NPT_OFF           ! default ensemble (NVT)
    job % numsemigrandatms = 0
    job % numsemiwidomatms = 0
    job % numswap = 0
    job % nummolmove = 0
    job % nummolrot = 0
    job % atmmovefreq = 0
    job % molmovefreq = 0
    job % molrotfreq = 0
    job % semiwidomatmsfreq = 0
    job % semigrandatmsfreq = 0
    job % semigrandmolfreq = 0
    job % vol_change_freq = 0
    job % swapfreq = 0
    job % gcmcatomfreq = 0
    job % gcmcmindist = 0.0

    job % gibbsatomtran = .false.
    job % gibbsmoltran = .false.
    job % gibbs_indvol = .false.
    job % gibbs_atmtran_freq = 0
    job % gibbs_moltran_freq = 0
    job % num_atmtran_gibbs = 0
    job % num_moltran_gibbs = 0

    job % gibbsatomexch = .false.
    job % gibbsmolexch = .false.
    job % gibbs_atmexch_freq = 0
    job % gibbs_molexch_freq = 0
    job % num_atmexch_gibbs = 0
    job % num_molexch_gibbs = 0

    job % is_gibbs_ensemble = .false.

    job % usegaspres   = .false.
    job % usechempotkt = .false.
    job % usechempotkj = .false.

    job % useseqmove    = .false.
    job % useseqmolmove = .false.
    job % useseqmolrot  = .false.
    job % useseqmovernd = .false.
    job % useseqmolmovernd = .false.
    job % useseqmolrotrnd  = .false.

    job % numgcmcmol = 0
    job % gcmcmolfreq = 0
    job % gcmcmindist = 0.0_wp

    job % repexch = .false.
    job % repexch_freq = 1000

    job % fedcalc = .false.
    job % fed_trial_freq = 10000000 !AB: huge number to skip FED if input fails

    job % toler = 100.0_wp

    job % lzdensity = .false.
    job % lzcomzero = .false.
    job % nzden = 0
    job % zden_freq = 1000

    job % lrdfs = .false.
    job % lrdfx = .false.
    job % nrdfs = 0
    job % rdfs_freq = 1000

    job % jobtime = 1e6_wp
    job % closetime = 200.0_wp

    !sjc: random number seed
    job % lseeds = .false.
    job % rseeds = .false.
     
    !default seed values - taken from dl_poly_2
    job % seeds_ijkl(1) = 12
    job % seeds_ijkl(2) = 34
    job % seeds_ijkl(3) = 56
    job % seeds_ijkl(4) = 78

    !TU: Defaults for atom rotation moves
    job % rotateatm = .false.
    job % atmrotfreq = 0
    job % numatmrot = 0
    job % acc_atmrot_update = 100
    job % acc_atmrot_ratio = 0.37_wp
    job % atmrot = 1.0_wp

    !scp
    !scan defaults - for molecule
    job % scanmol = .false.
    job % num_scan_start = 0
    job % num_scan_stop = 0
    job % scan_step = 0.1_wp
    job % scan_slab = .false.
    job % scan_svec(1) =1.0_wp
    job % scan_svec(2) =1.0_wp
    job % scan_svec(3) =1.0_wp
    
end subroutine initialise_defaults


!> @brief
!> - Reading the simulation parameters and flags from CONTROL file, storing the values in control structure `job`
!> @using
!> - `kinds_f90`
!> - `parse_module`
!> - `constants_module`
!> - `control_type`
!> - `species_module`, only : get_species_type
!> - `comms_mpi_module`, only : master,mxnode,idnode
!> - `fed_interface_module`
!> - `slit_module`

!> read in CONTROL file: directives, switches & parameters
subroutine read_control(job)

    use kinds_f90
    use parse_module
    use constants_module
    use control_type
    use species_module, only : get_species_type, set_species_type, check_atom_defined, valid_species_type
    use comms_mpi_module, only : master,mxnode,idnode
    use fed_interface_module
    use slit_module
    
    implicit none

        !> structure containing global simulation control switches & parameters
    type(control), intent(inout) :: job

    integer :: nread, i, j, num, nmax, nbond, nang, ib
    integer, allocatable, dimension(:) :: atomi, atomj, atomk, pot
    character :: line*256, line1*128, word*64, word1*64,contrl_sysname*72, tag*1, errormsg*72
    !TU: 'wholeline' contains the last line read from the file. This is dumped in the error
    !TU: message if there is an error. (Note that 'line' is worked on by the parser, and
    !TU: loses words each time get_word is called).
    character :: wholeline*256
    logical :: use_none, safe, finished, ensflag, more, isflt
    real(kind=wp) :: tmp

    use_none = .true.
    finished = .false.
    ensflag  = .false.
    safe     = .true.
    nread    = uctrl

    job % fedcalc = .false.
    job % repexch = .false.

    !AB: CONTROL is opened in dl_monte.f90
    !if (idnode == 0) open(nread, File = 'CONTROL', Status = 'old')

11  call get_line(safe,nread,contrl_sysname)
    if (.not.safe) then
        errormsg = "end of file reached unexpectedly!"
        Go To 1000
    end if
    call strip_blanks(contrl_sysname)

    call get_word(contrl_sysname, word)
    call lower_case(word)

    if( word == "" .or. word(1:1) == "#" ) Go To 11

    first: do while (.not.finished)

        call get_line(safe,nread,line)
        wholeline = line
!        if (.not.safe) Go To 1000
        if (.not.safe) then
            errormsg = "end of file reached unexpectedly (in 'use' loop)!"
            Go To 1000
        end if
        call lower_case(line)
        call get_word(line, word)

        ! AB: due to trimming within `get_word(..)` any spaces around `word` are removed
        if( word == "" .or. word(1:1) == "#" ) cycle first

        if(word /= "use" .and. word /= "finish") then

            call error(9)

        endif

        if (word == "finish") then

            finished = .true.

        else

            use_none = .false.

            call get_word(line, word)

            if( word == "none" .or. word(1:7) == "default" .or. word == "standard" ) then

                use_none = .true.

            else if (word == "seqmove" .or. word == "seqatmmove") then

                job % useseqmove = .true.

            else if (word == "seqmovernd" .or. word == "seqatmmovernd") then

                job % useseqmove = .true.
                job % useseqmovernd = .true.

            else if (word == "seqmolmove") then

                job % useseqmolmove = .true.

            else if (word == "seqmolmovernd") then

                job % useseqmolmove = .true.
                job % useseqmolmovernd = .true.

            else if (word == "seqmolrot") then

                job % useseqmolrot = .true.

            else if (word == "seqmolrotrnd") then

                job % useseqmolrot = .true.
                job % useseqmolrotrnd = .true.

            else if (word(1:5) == "ortho") then

                job % useorthogonal = .true.

            else if (word(1:3) == "fed") then

                job % fedcalc = .true.

                !if( master ) then
                !    write(uout,*)
                !    write(uout,*)"*===============================================*"
                !    write(uout,*)'* FED calculation(s) are still under testing!!! *'
                !    write(uout,*)"*===============================================*"
                !end if

                call initialise_fed(job,line,nread)

            else if (word(1:7) == "repexch") then

                job % repexch = .true.

                call get_word(line, word1)
                job % numreplicas = nint(word_2_real(word1))
                call get_word(line, word1)
                job % repexch_inc = word_2_real(word1)
                call get_word(line, word1)
                job % repexch_freq = nint(word_2_real(word1))

                if( job % repexch_freq < 1 ) job % repexch_freq = 1000

                if( mxnode < 2 ) &
                    call cry(uout,'', &
                         "ERROR: directive 'use repexch ...' assumes a parallel job !!!",999)

            else if (word == "rotquaternion") then

                job % usequaternion = .true.

            else if (word == "reset") then

                job % usereset = .true.

                call get_word(line, word1)
                job % reset_freq = nint(word_2_real(word1))

                if( job % reset_freq < 1 ) &
                    call cry(uout,'', &
                         "ERROR: missing or too small stride (frequency) in directive 'use reset <stride>' !!!",999)

            else if (word == "feynmanhibbs") then

                job % usefeynmanhibbs = .true.

                call cry(uout,'', &
                     "WARNING: directive 'use feynmanhibbs' has effect in compilation with tabulated potentials only !!!",0)

            else if (word == "gaspressure") then

                job % usegaspres   = .true.

                job % usechempotkt = .false.
                job % usechempotkj = .false.

            else if (word == "chempotkt") then

                job % usechempotkt = .true.

                job % usegaspres   = .false.
                job % usechempotkj = .false.

            else if (word == "chempotkj") then

                job % usechempotkj = .true.

                job % usegaspres   = .false.
                job % usechempotkt = .false.

            else if (word == "gcexcludeslab") then

                job % usegcexcludeslab = .true.

                call get_word(line, word1)
                job % gcexcludeslab_dim = nint(word_2_real(word1))
                call get_word(line, word1)
                job % gcexcludeslab_lbound = word_2_real(word1)
                call get_word(line, word1)
                job % gcexcludeslab_ubound = word_2_real(word1)

                if( job%gcexcludeslab_dim < 1 .or. job%gcexcludeslab_dim > 3 ) then
                
                    call cry(uout,'', &
                        "ERROR: Invalid dimension specified with 'use gcexcludeslab'. "// &
                        "Check syntax!",999)

                end if

                if( job%gcexcludeslab_ubound <= job%gcexcludeslab_lbound ) then

                    call cry(uout,'', &
                        "ERROR: Bounds specified with 'use gcexcludeslab' are equal or "// &
                        "the upper bound < lower bound.",999)

                end if

                if(      job%gcexcludeslab_ubound < -0.5_wp .or. job%gcexcludeslab_lbound > 0.5_wp &
                    .or. job%gcexcludeslab_ubound < -0.5_wp .or. job%gcexcludeslab_lbound > 0.5_wp ) then

                    call cry(uout,'', &
                        "ERROR: Fractional bounds specified with 'use gcexcludeslab' must be "// &
                        "between -0.5 and 0.5",999)

                end if

            else if( word == "cavitybias" ) then

                job % usecavitybias = .true.

                call get_word(line, word1)
                if( word1(1:4) == "mesh" ) then
                    job % cavity_grid = .false.
                    job % cavity_mesh = .true.
                    call get_word(line, word1)
                endif

                if( word1(1:3) == "all" ) then
                    call get_word(line, word1)
                    job % cavity_x = word_2_real(word1)
                    job % cavity_y = job % cavity_x
                    job % cavity_z = job % cavity_x
                else
                    job % cavity_x = word_2_real(word1)

                    call get_word(line, word1)
                    job % cavity_y = word_2_real(word1)

                    call get_word(line, word1)
                    job % cavity_z = word_2_real(word1)

                    !call get_word(line, word1)
                endif

                call get_word(line, word1)
                if( word1(1:3) == "rad" ) call get_word(line, word1)
                job % cavity_radius = word_2_real(word1)

                if( max(job%cavity_x,job%cavity_y,job%cavity_z) > job%cavity_radius ) &
                    call cry(uout,'', &
                         "ERROR: using cavity bias with grid dimension(s) "//&
                         "larger than cavity radius is meaningless !!!",999)

                call get_word(line, word1)
                if( word1(1:3) == "upd" ) call get_word(line, word1)
                job % cavity_update = nint(word_2_real(word1))

                if( job % cavity_update < 1 ) then

                    call cry(uout,'', &
                         "ERROR: using cavity bias with update stride "//&
                         "smaller than 1 is meaningless !!!",999)

                else if( job % cavity_update < 1000 ) then

                    call cry(uout,'', &
                         "WARNING: using cavity bias with update stride "//&
                         "smaller than 1000 is very inefficient (time consuming) !!!",0)

                endif

            else if (word(1:6) == "orient") then

                job % useorientation = .true.

            else if (word == "orientbiasrot") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been implemented yet...",999)

                job % useobias_rot = .true.

                call get_word(line, word1)

                job % obias_rot_k = nint(word_2_real(word1))

            else if( word(1:5) == "clist" ) then

                job % clist = .true.

                if( master ) call cry(uout,'', &
                    "WARNING: Cell list functionality is experimental, and not yet supported for "// &
                    "all interaction types or MC schemes",0)

            else if (word == "xymoves") then

                job % usexymoves = .true.

                if( master ) call cry(uout,'', &
                    "WARNING: xymoves functionality is experimental, and not yet supported for "// &
                    "all interaction types or MC schemes",0)


            else if (word == "orientbiasgcmc") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been implemented yet...",999)

                job % useobias_gcmc = .true.

            else if (word == "exchangebias") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been implemented yet...",999)

                job % exchangebias = .true.

            else if (word == "moldata") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been implemented yet...",999)

                job % lmoldata = .true.

            else if (word(1:6) == "domain") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been implemented yet...",999)

                job % useddmc = .true.

            else

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'use "//trim(word)//"' has not been recognised !!!",999)

                call error(10)

            endif

        endif

    enddo first

    if( use_none ) &
        call cry(uout,'', &
             "NOTE: Standard simulation course (i.e. default behaviour)",0)

    finished = .false.

    second: do while (.not.finished)

        call get_line(safe,nread,line)
        wholeline = line
!        if (.not.safe) Go To 1000
        if (.not.safe) then
            errormsg = "end of file reached unexpectedly (in main loop)! Missing 'start'?"
            Go To 1000
        end if

        !AB: 'line1' preserves the list of molecules given after keyword 'only' (if any)
        line1 =""
        num = index(line,'only ') + 5
        if( num > 0 .and. len_trim(line(num:)) > 1 ) line1 = line(num:)

        call lower_case(line)
        call get_word(line, word)

        ! AB: due to trimming within `get_word(..)` any spaces around `word` are removed
        if( word == "" .or. word(1:1) == "#" ) cycle second

        if (word == "calcstress") then

            if( master ) call cry(uout,'', &
                "ERROR: the option requested by '"//trim(word)//"' has not been implemented yet...",999)

            !job % calcstress = .true.

        else if (word == "scale") then

            if( master ) call cry(uout,'', &
                "ERROR: the option requested by '"//trim(word)//"' has not been implemented yet...",999)

            !job % scalevec = .true.
            call get_word(line, word)
            job % vecscalex = word_2_real(word)
            call get_word(line, word)
            job % vecscaley = word_2_real(word)
            call get_word(line, word)
            job % vecscalez = word_2_real(word)

        else if (word == "grow") then

            if( master ) call cry(uout,'', &
                "ERROR: the option requested by '"//trim(word)//"' has not been implemented yet...",999)

            !job % scalesize = .true.
            call get_word(line, word)
            job % scalex = word_2_real(word)
            call get_word(line, word)
            job % scaley = word_2_real(word)
            call get_word(line, word)
            job % scalez = word_2_real(word)

        else if (word(1:6) == "vdwcut") then

            !if( job % vdw_cut > 0.0_wp .and. job % vdw_cut < job % shortrangecut ) &
                call cry(uout,'', &
                     "ERROR: VDW CUTOFF setting in CONTROL"//&
                    &"' - the value found in FIELD must be only set and used !!!",999)
                    ! "WARNING: VDW CUTOFF set in both FIELD & CONTROL"//&
                    !&"' - the value found in CONTROL will be used !!!",0)

            call get_word(line, word)
            job % vdw_cut = word_2_real(word)

        else if (word(1:5) == "toler") then
            call get_word(line, word)
            job % toler = word_2_real(word)

        else if (word(1:4) == "temp") then

            call get_word(line, word)
            job % systemp = word_2_real(word)

            if( job % fedcalc ) call fed_check_temp(job)

        else if (word(1:4) == "pres") then
            call get_word(line, word)

            if( word(1:6) == "kunits" ) then
 
               job % syspres_kunits = .true.
               call get_word(line, word)

            end if

            job % syspres = word_2_real(word)

        else if (word(1:5) == "seeds") then

            job % lseeds = .true.
            job % fseeds = .false.

            call get_word(line, word)
            job % seeds_ijkl(1) = nint(word_2_real(word))

            call get_word(line, word)
            job % seeds_ijkl(2) = nint(word_2_real(word))

            call get_word(line, word)
            job % seeds_ijkl(3) = nint(word_2_real(word))

            call get_word(line, word)
            job % seeds_ijkl(4) = nint(word_2_real(word))

        else if (word == "ranseed") then

            job % rseeds = .true.
            job % fseeds = .false.
            job % lseeds = .false.

        else if( word(1:4) == "slit" ) then

            !AB: define the cell as 3D (bulk, default) or 2D-slit (planar pore)
            !AB: checks and amendments are done in initialise_slit(job, 1), see below

            call get_word(line, word)

            job % is_slit = .true.

            if( word(1:5) == "walls" ) call get_word(line, word)

            if( word(1:4) == "hard" ) then 

                !AB: this is the default
                walls_soft    = SLIT_EXT0
                job%cell_type = SLIT_HARD2

                call get_word(line, word)
                if( word(1:6) == "zfrac1" ) then

                    call get_word(line, word)
                    slit_gcmc_z = word_2_real(word)
                    if( abs(slit_gcmc_z) > 0.9999_wp ) slit_gcmc_z = 0.0_wp
                    if( abs(slit_gcmc_z) > 0.0_wp ) is_slit_gcmc_z = .true.

                    is_slit_frac_z = is_slit_gcmc_z
                    !slit_frac_z = slit_gcmc_z

                else if( word(1:6) == "zfrac2" ) then

                    call get_word(line, word)
                    slit_gcmc_z = word_2_real(word)
                    if( abs(slit_gcmc_z) > 0.4999_wp ) slit_gcmc_z = 0.0_wp
                    if( abs(slit_gcmc_z) > 0.0_wp ) is_slit_gcmc_z = .true.

                    is_slit_frac_z = is_slit_gcmc_z
                    slit_frac_z = slit_gcmc_z

                end if

            else if( word(1:4) == "soft" )  then

                call get_word(line, word)
                walls_soft = nint(word_2_real(word)) !AB: number of soft walls

                job%cell_type = -walls_soft !AB: SLIT_SOFT1/2

                call get_word(line, word)
                if( word(1:6) == "zfrac1" ) then

                    call get_word(line, word)
                    slit_gcmc_z = word_2_real(word)
                    if( abs(slit_gcmc_z) > 0.9999_wp ) slit_gcmc_z = 0.0_wp
                    if( abs(slit_gcmc_z) > 0.0_wp ) is_slit_gcmc_z = .true.

                    is_slit_frac_z = is_slit_gcmc_z
                    !slit_frac_z = slit_gcmc_z

                else if( word(1:6) == "zfrac2" ) then

                    call get_word(line, word)
                    slit_gcmc_z = word_2_real(word) 
                    if( abs(slit_gcmc_z) > 0.4999_wp ) slit_gcmc_z = 0.0_wp
                    if( abs(slit_gcmc_z) > 0.0_wp ) is_slit_gcmc_z = .true.

                    is_slit_frac_z = is_slit_gcmc_z
                    slit_frac_z = slit_gcmc_z

                end if

            else if (word(1:6) == "charge") then

                if( job%cell_type > -1  ) then 
                    walls_soft    = SLIT_EXT0
                    job%cell_type = SLIT_HARD2
                end if

                call get_word(line, word)
                walls_charged = nint(word_2_real(word)) !AB: number of charged walls

                if( walls_charged > 0 ) then

                    call get_word(line, word)
                    SCD1 = word_2_real(word)

                    if( walls_charged == 2 ) then

                      call get_word(line, word)
                      SCD2 = word_2_real(word)

                    else if( walls_charged > 2 ) then
                        call cry(uout,'(/,1x,a,/)', &
                        "ERROR: number of charged surfaces in slit > 2 !!!",999)
                    end if

                    job % coultype = -1

                else 
                    walls_charged = 0
                end if

            else if (word(1:6) == "mfa") then

                if( job%cell_type > -1  ) then 
                    walls_soft    = SLIT_EXT0
                    job%cell_type = SLIT_HARD2
                end if

                job % coultype = -1

                !AB: all configs with the same type of Coulomb treatment
                call get_word(line, word)
                mfa_type = nint(word_2_real(word))

                if( abs(mfa_type) > 2 ) &
                    call cry(uout,'(/,1x,a,/)', &
                         "ERROR: MFA type out of range - abs(mfa) must be < 3 !!!",999)

                    call get_word(line, word)
                    mfa_ncycles = nint(word_2_real(word))

                    call get_word(line, word)
                    damp_mfa = word_2_real(word)

                if( mfa_ncycles < 1 ) then

                    damp_mfa = 0.0_wp

                end if

            else 
                call cry(uout,'(/,1x,a,/)', &
                     "ERROR: unknown or unsupported keyword for slit/walls/mfa !!!",999)
            end if

        else if (word(1:4) == "diel" .or. word(1:3) == "eps") then

            call get_word(line, word)

            if ( word(1:4) == "permit" ) call get_word(line, word)

            job % dielec = word_2_real(word)

            if( job % dielec <= 0 .and. master ) then

                write(uout,'(/,/,a,f16.8)')"ERROR: unphysical value of dielectric constant ", job % dielec

                call error(999)

            end if

        else if (word == "noewald" .or. word == "nocoul" .or. word == "noelec") then

            !if( job % is_slit ) call error(21)

            call get_word(line, word)
            if (word == "all") then

                job % coultype = 0

            else

                ib = nint(word_2_real(word))
                job % coultype(ib) = 0

            endif

        else if (word == "distewald") then

            if( job % is_slit .and. .not.is_slit_frac_z .and. mfa_type/=0 ) call error(21)

            job % distribgvec = .true.

        else if (word(1:5) == "ewald") then
        !AB: coultype = 1 <=  "ewald" - normal Ewald summation using erfc AND reciprocal space terms; 
        !AB: only available in bulk (XYZ-PBC)

            job % coultype = 1

            call get_word(line, word)

            if(word(1:4) == "prec") then

                if( job % is_slit .and. .not.is_slit_frac_z .and. mfa_type/=0 ) call error(21)
            
                call get_word(line, word)
                job % madelungacc = word_2_real(word)

            else if (word(1:3) == "sum") then

                if( job % is_slit .and. .not.is_slit_frac_z .and. mfa_type/=0 ) call error(21)
                
                job % ewald_alpha_given = .true.
                call get_word(line, word)
                job % ewald_alpha = word_2_real(word)
                call get_word(line, word)
                job % kmax1 = nint(word_2_real(word))
                call get_word(line, word)
                job % kmax2 = nint(word_2_real(word))
                call get_word(line, word)
                job % kmax3 = nint(word_2_real(word))

            else if( word(1:4) == "real" ) then
            !AB: coultype < 0 <= additional specific cases
            !AB: coultype = 2 <= "ewald real" in XYZ-PBC with cutoff (the same as in Ewald, i.e. using erfc/r) 
            !AB: but no sum in reciprocal space

                job % coultype = 2

                if( job % is_slit ) then
                    job % coultype = -2
                    mfa_type = 0
                endif

            else

                call error(12)

            endif

        else if( word(1:4) == "coul" .or. word == "direct" .or. word == "explicit" ) then
        !AB: coultype =-2 <= "explicit" or "direct" bare Coulomb, no shift, no damping; 
        !AB: truncated at cutoff or box edges (in bulk / slit)

            job % coultype = -1

        else if( word(1:5) == "shift" ) then
        !AB: coultype = 3 <= "shift[ed]" - shifted-force Coulomb without damping by erfc/r); only available in bulk (XYZ-PBC)
        !AB: coultype = 4 <= "shift[ed] damp[ed]" - shifted-force Coulomb damped by erfc/r); only available in bulk (XYZ-PBC)
        !AB: coultype = 4 <= "shift[ed] prec[ision]" - shifted-force Coulomb damped by erfc/r); only available in bulk (XYZ-PBC)
        !AB: the last two options differ in how parameter 'alpha' is determined (see below)

            job % coultype = 3
            job % ewald_alpha_given = .false. !.true.
            call get_word(line, word)

            if (word(1:4) == "prec") then

                job % coultype = 4
                call get_word(line, word)

                tmp = word_2_real(word)
                tmp = max(min(abs(tmp),0.5_wp),1.0e-20_wp)
                job % madelungacc = tmp

                !job % ewald_alpha = sqrt(abs(log(tmp*job%shortrangecut* &
                !    sqrt(abs(log(tmp*job%shortrangecut))))))/job%shortrangecut

            else if (word(1:4) == "damp") then

                job % coultype = 4
                job % ewald_alpha_given = .true.

                call get_word(line, word)
                job % ewald_alpha = word_2_real(word)

            else !if (word(1:3) == "cut") then

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: shifted-force (SF) bare Coulomb (no damping)!",0)

            !else
            !    call error(13)

            endif

            if( job % is_slit ) then 
                job % coultype = -job % coultype !call error(21)
                mfa_type = 0
            endif

        else if (word(1:4) == "verl") then
            call get_word(line, word)
            job % verletshell = word_2_real(word)

        else if (word == "nbrlist") then

            job%uselist = .true.

            call get_word(line, word)

            if (word == "auto") then
                job % lauto_nbrs = .true.
            else
                job % nbrlist = nint(word_2_real(word))
            endif

        else if (word == "clist") then

            if( .not. job % clist ) then

                call cry(uout,'', &
                     "ERROR: 'clist' directive in main block in CONTROL requires 'use clist' directive in use block!",999)
            
            end if

            call get_word(line, word)

            if (word == "printdetails") then

                job % clist_printdetails = .true.

            else if (word == "check") then

                job % clist_check = .true.

                call get_word(line, word)

                job % clist_checkfreq = nint(word_2_real(word))                

                if( job % clist_checkfreq < 1 ) then

                    call cry(uout,'', &
                        "ERROR: 'clist check <freq>' used with <freq> < 1, which is not allowed!",999)                    

                end if

            else 

                call cry(uout,'', &
                     "ERROR: unrecognised directive '"//trim(word)//"' used with 'clist' directive",999)

            end if

        else if (word == "maxthbnbrs") then

            call get_word(line, word)
            job % max_thb_nbrs = nint(word_2_real(word))

        else if (word == "maxnonbondnbrs") then

            call get_word(line, word)
            job % max_nonb_nbrs = nint(word_2_real(word))

        else if (word == "thbupdate") then

            call get_word(line, word)
            job % thblist = nint(word_2_real(word))

        else if (word(1:6) == "steps") then

            call get_word(line, word)
            job % maxiterations = nint(word_2_real(word))

        else if (word(1:5) == "equil") then
            call get_word(line, word)
            job % sysequil = nint(word_2_real(word))

        else if (word(1:4) == "heat") then
            call get_word(line, word)
            job % sysheat = nint(word_2_real(word))

        else if (word(1:5) == "print") then

            call get_word(line, word)
            job % sysprint = nint(word_2_real(word))

            if( job % sysprint < 1 ) then 
                job % sysprint = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: energies output stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

        else if (word(1:5) == "check") then

            call get_word(line, word)
            job % syscheck = nint(word_2_real(word))

            if( job % syscheck < 1 ) then 
                job % syscheck = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: energy checks stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

         else if (word(1:12) == "nbrlistcheck") then

            job % nbrlistcheck = .true.

            call get_word(line, word)
            job % nbrlistcheckfreq = nint(word_2_real(word))

            if( job % nbrlistcheckfreq < 1 ) then 
                job % nbrlistcheckfreq = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: neighbour list checks stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

         else if (word(1:17) == "speciescountcheck") then

            job % speciescountcheck = .true.

            call get_word(line, word)
            job % speciescountcheckfreq = nint(word_2_real(word))

            if( job % speciescountcheckfreq < 1 ) then 
                job % speciescountcheckfreq = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: species count checks stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

        else if (word(1:15) == "energycheckprec") then

            call get_word(line, word)
            job % checkenergyprec = word_2_real(word)

        else if (word(1:5) == "stack") then

            call get_word(line, word)
            job % stacksize = nint(word_2_real(word))

        !TU*: Added by TU...
        else if (word(1:15) == "statssamplefreq") then

            call get_word(line, word)
            job % sample_stats_freq = nint(word_2_real(word))

            if( job % sample_stats_freq < 1 ) then 
                job % sample_stats_freq = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: statistics sampling stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

        else if (word(1:4) == "stat") then

            call get_word(line, word)

            if( word(1:3) == "mov" ) then
                job % lmovstats = .true.
                cycle second
            end if

            job % statsout = nint(word_2_real(word))

            if( job % statsout < 1 ) then 
                job % statsout = 1

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: statistics output stride (frequency) < 1 in CONTROL - resetting to 1 !!!",0)

            end if

        !TU: Code for YAMLDATA...
        else if(word == "yamldata") then

            job % useyamldata = .true.

            call get_word(line, word)

            if( word == "" .or. word(1:1) == "#" ) then
               
                !TU: The directive "yamldata" by itself simply enables default output to YAMLDATA
                cycle second

            else if(word == "scalarop2") then

                !TU: Output the scalar nematic order parameter, 2nd Legendre polynomial
                job % yamldata_output_scalarop2 = .true.
                
                cycle second

            else if(word == "scalarop4") then

                !TU: Output the scalar nematic order parameter, 4th Legendre polynomial
                job % yamldata_output_scalarop4 = .true.
                
                cycle second
                
            else 

                !TU: Read the frequency of output to YAMLDATA
                
                job % yamldata_freq = nint(word_2_real(word))

                if( job % yamldata_freq < 1 ) then
                        
                    call cry(uout,'(/,1x,a,/)', &
                         "WARNING: stride (frequency) of output to YAMLDATA.* < 1 in "//&
                         &"'yamldata' directive - resetting to 1 !!! ",0)
                        
                    job % yamldata_freq = 1
                        
                end if
                
            end if

        else if (word == "sample") then

            call get_word(line, word)

            if(word(1:5) == "coord") then

                call get_word(line, word)
                job % sysdump = nint(word_2_real(word))

                call get_word(line, word)
                if( word(1:4) == "only" ) then

                    nmax = index(line1,'#')
                    if(nmax == 0) nmax = index(line1,'!')
                    if(nmax == 0) nmax = index(line1,';')

                    if( nmax > 1 ) then
                        job % hist_molids = adjustL(line1(1:nmax-1))
                    else
                        job % hist_molids = adjustL(line1)
                    end if

                    if( len_trim(job % hist_molids) < 1 ) then
                        call cry(uout,'(/,1x,a,/)', &
                                 "ERROR: incomplete directive 'sample coordinates' in CONTROL file - "//&
                                &"either remove keyword 'only' or provide subset of molecules !!!",999)
                    end if

                end if

!AB: this is not used now (requested frequency of storing the latest max. displacements for atoms and molecules)
            else if (word(1:5) == "displ") then

                if( master ) call cry(uout,'', &
                    "ERROR: the option requested by 'sample "//trim(word)//"' has not been implemented yet...",999)
!
!                job % lsample_displacement = .true.

                call get_word(line, word)
                job%sample_disp_avg = nint(word_2_real(word))

            else if (word(1:3) == "rdf") then

                job % lrdfs = .true.

                call get_word(line, word)
                job % nrdfs = nint(word_2_real(word))

                call get_word(line, word)
                job % rdf_max = word_2_real(word)

                call get_word(line, word)
                job % rdfs_freq = nint(word_2_real(word))

                call get_word(line, word)
                job % lrdfx = ( word(1:2)=='ex' )

            !else if (word == "zdensity") then
            else if (word(1:4) == "zden") then

                job % lzdensity = .true.

                call get_word(line, word)
                job % nzden = nint(word_2_real(word))

                call get_word(line, word)
                job % zden_freq = nint(word_2_real(word))
                
                call get_word(line, word)
                job % lzcomzero = ( word(1:4)=="cent" .or. word(1:4)=="comz" )

                if( job % lzcomzero ) then

                    call get_word(line, word)
                    if( word(1:4) == "only" ) then

                        nmax = index(line1,'#')
                        if(nmax == 0) nmax = index(line1,'!')
                        if(nmax == 0) nmax = index(line1,';')

                        if( nmax > 1 ) then
                            job % zcom_molids = adjustL(line1(1:nmax-1))
                        else
                            job % zcom_molids = adjustL(line1)
                        end if

                        if( len_trim(job % zcom_molids) < 1 ) then
                            call cry(uout,'(/,1x,a,/)', &
                                     "ERROR: incomplete directive 'sample zdensity' in CONTROL file - "//&
                                    &"either remove keyword 'only' or provide subset of molecules !!!",999)
                        end if

                    end if

                endif

            !else if (word == "energy") then
            else if (word(1:4) == "ener") then

                job % lsample_energy = .true.

                call get_word(line, word)
                job % sample_energy_freq = nint(word_2_real(word))

            !else if (word == "volume") then
            else if (word(1:3) == "vol") then

                job % lsample_volume = .true.

                call get_word(line, word)
                job % sample_volume_freq = nint(word_2_real(word))

                if( job % sample_volume_freq < 1 ) then

                    call cry(uout,'(/,1x,a,/)', &
                             "WARNING: stride (frequency) of collecting P(V) < 1 in "//&
                            &"'sample volume' directive - resetting to 1 !!! ",0)

                    job % sample_volume_freq = 1

                end if

                call get_word(line, word)
                job % vol_bin = word_2_real(word)

                if( job % vol_bin <= 0.0_wp ) &
                    call cry(uout,'', &
                             "ERROR: volume bin size < 0 in 'sample volume' directive !!! ",999)

                call get_word(line, word)
                if( word /= "" .and. word(1:1) /= '#' ) job % vol_bin_power = word_2_real(word)

                if( job % vol_bin_power < 1.0_wp .or. job % vol_bin_power > 2.0_wp ) then

                     call cry(uout,'(/,1x,a,/)', &
                             "WARNING: volume bin power out of range [1 .. 2] in "//&
                            &"'sample volume' directive - resetting to 1 !!!",0)

                     job % vol_bin_power = 1.0_wp

                end if

            !TU: Code for sampling orientations...
            else if (word(1:6) == "orient") then
                
                if( .not. job % useorientation ) then
                    
                    call cry(uout,'(/,1x,a,/)', &
                        "ERROR: 'sample orient[ation]' requires 'use orient[ation]' directive "//&
                        &"in 'use' block",999)

                end if

                call get_word(line, word)

                if(word(1:4) == "init") then

                    job % sample_orientation_init = .true.

                else

                    job % sample_orientation = .true.
                
                    if( word == "" .or. word(1:1) == "#" ) then
                
                        cycle second

                    else

                        job % sample_orientation_freq = nint(word_2_real(word))
                    
                        if( job % sample_orientation_freq < 1 ) then
                        
                            call cry(uout,'(/,1x,a,/)', &
                                 "WARNING: stride (frequency) of sampling orientations < 1 in "//&
                                 &"'sample orient[ation]' directive - resetting to 1 !!! ",0)
                        
                            job % sample_orientation_freq = 1
                        
                        end if

                    end if

                end if

            endif

        else if (word == "archiveformat") then

            call get_word(line, word)
            if (word == "dlmonte") then

                job % archive_format = DLM ! = 0

                call get_word(line, word)
                if( word /= "" .and. word(1:1) /= '#' ) &
                    job%nfltprec = nint(word_2_real(word))

            else if (word == "dlpoly") then

                job % archive_format = DLP ! = 1

                call get_word(line, word)
                if( word /= "" .and. word(1:1) /= '#' ) &
                    job%nfltprec = nint(word_2_real(word))

            else if (word(1:7) == "dlpoly2") then

                job % archive_format = DLP2 ! = 2

                if (word == "dlpoly2+dcd") then 

                    job % archive_format = DLP2DCD ! = 3

                    call get_word(line, word)
                    
                    if( word(1:3) == "dbl" .or. word(1:6) == "double" ) then
                        job % dcd_form = 1
                        call get_word(line, word)
                    end if
                    
                else
                
                    call get_word(line, word)

                end if
                
                if( word /= "" .and. word(1:1) /= '#' ) &
                    job%nfltprec = nint(word_2_real(word))

            else if (word(1:7) == "dlpoly4") then

                job % archive_format = DLP4 ! = 4

                if (word == "dlpoly4+dcd") then 

                    job % archive_format = DLP4DCD ! = 5

                    call get_word(line, word)
                    if( word(1:3) == "dbl" .or. word(1:6) == "double" ) job % dcd_form = 1

                end if

            else if (word == "dcd") then

                job % archive_format = DCD ! = 6

                call get_word(line, word)
                if( word(1:3) == "dbl" .or. word(1:6) == "double" ) job % dcd_form = 1

            else if (word == "vtk") then

                job % archive_format = VTK

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: 'archiveformat vtk' is an experimental feature under development",0)

            else if (word == "xyz") then

                job % archive_format = XYZ

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: 'archiveformat xyz' is an experimental feature under development",0)
                
            else

                call cry(uout,'(/,1x,a,/)', &
                     "ERROR: unknown 'archiveformat' in CONTROL - select from keywords "//&
                    &" 'dlmonte' [default], 'dlpoly', 'dlpoly2[+dcd]', or 'dlpoly4[+dcd]' !!!",999)

            endif

        else if (word == "trajconvert") then

            if( mxnode > 1 ) then

                call cry(uout,'(/,1x,a,/)', &
                     "ERROR: found directive 'trajconvert' in CONTROL - this mode "//&
                    &"can be invoked only in a SERIAL run or single thread only !!!",999)

            end if

            job % simulationmode = "trajconv"

            call get_word(line, word)
            job % traj_beg  = nint(word_2_real(word))

            call get_word(line, word)
            job % traj_end  = nint(word_2_real(word))

            call get_word(line, word)
            job % traj_step = nint(word_2_real(word))

            call get_word(line, word)
            if (word == "format") call get_word(line, word)

            if (word == "dlmonte") then

                job % traj_format = DLM ! = 0

            else if (word == "dlpoly") then

                job % traj_format = DLP ! = 1

            else if (word == "dlpoly2") then

                job % traj_format = DLP2 ! = 2

            else if (word == "dlpoly4") then

                job % traj_format = DLP4 ! = 4

            else if (word(1:3) == "dcd") then

                job % traj_format = DCD ! = 6

                !call get_word(line, word)
                if( len(trim(word))>4 .and. (word(5:7) == "dbl" .or. word(5:10) == "double") ) job % dcd_form = 1

            else

                call cry(uout,'(/,1x,a,/)', &
                     "ERROR: unknown 'trajconvert format' in CONTROL - select from keywords: "//&
                    &" 'dlmonte', 'dlpoly', 'dlpoly2', 'dlpoly4', or 'dcd[ dbl]' !!!",999)

            endif

        else if (word == "revconformat") then

            call get_word(line, word)

            if (word == "dlmonte") then
                job % revcon_format = DLM  ! = 0
            else if (word == "dlpoly") then
                job % revcon_format = DLP  ! = 1
            else if (word == "dlpoly2") then
                job % revcon_format = DLP2 ! = 2
            else if (word == "dlpoly4") then
                job % revcon_format = DLP4 ! = 4
                
            else if (word == "vtk") then

                job % revcon_format = VTK

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: 'revconformat vtk' is an experimental feature under development",0)

            else if (word == "xyz") then

                job % revcon_format = XYZ

                call cry(uout,'(/,1x,a,/)', &
                     "WARNING: 'revconformat xyz' is an experimental feature under development",0)
                
            else 
                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: unknown 'revconformat' in CONTROL - select from keywords "//&
                        &" 'dlmonte' [default], 'dlpoly', 'dlpoly2', or 'dlpoly4' !!!",999)
            endif

        else if (word == "revconfreq") then
   
            call get_word(line, word)
            job % extrarevconfreq = word_2_real(word)
            job % extrarevcon = .true.

        else if (word == "paratom") then

            job%lparallel_atom = .true.

        else if (word(1:6) == "jobtim") then

            call get_word(line, word)
            job % jobtime = word_2_real(word)
            call get_word(line, word)
            job % closetime = word_2_real(word)

!scp: add "scan" move for molecule

        else if (word == "scan") then

!examples:
!1.
!scan molecule 1 0 100 2.0 
!dioxygen  
!              only molecules scanned, 1=number of molecules selected (name on next line)
!                                      0=initial step, 100=final step 2.0=step length
!                                      dioxygen=molecule name in FIELD file
!                                      note: the step length is used to build a 3D grid in this case of 2 Ang
!
!
!scan molecule 1 0 0 2.0 
!dioxygen 
!              same as above but will generate a full 3D grid in the simulation cell spearated by 2 Ang i.e. complete scan
!
!scan molecule 1 101 200 2.0 
!dioxygen 
!               same as above but will start from 101st grid point and go to 200th (feature towards running scans in parallel)
!
!scan slab 0.2 0.2 0.2 molecule 1 0 100 2.0
!dioxygen
!               slab a b c, when a, b, c =< 1 enables a grid scan to be performed on a portion of the cell
!
!Note: the initial position of the chosen molecule should be specified in the CONFIG
!      and please remember to include the molecule and its coordinates in CONFIG! 



               call get_word(line, word)

              ! if(word(1:6) == "full") then
              ! job % num_scan_start = 0
              ! job % num_scan_stop = 0
              ! call get_word(line, word)
              !   job % scan_step = (word_2_real(word))
              ! else if(word(1:6) == "part") then
              !   call get_word(line, word)
              !  job % num_scan_start = nint(word_2_real(word))
              !  call get_word(line, word)
              !  job % num_scan_stop = nint(word_2_real(word))
              !  call get_word(line, word)
              !  job % scan_step = (word_2_real(word))
              ! else if(word(1:6) == "slab") then
              ! job % num_scan_start = 0
              ! job % num_scan_stop = 0
              ! call get_word(line, word)
              ! job % scan_slab = .true.  ! use slab dimension from scan_slab
              !  endif
                if(word(1:4) == "slab") then
                job % scan_slab = .true.
                do i=1,3
                call get_word(line, word)
                job % scan_svec(i) = (word_2_real(word))
                enddo
                call get_word(line, word)
                endif

                if(word(1:6) == "molecu") then
! at the moment re-using molmove etc
                call get_word(line, word)
                job % nummolmove = nint(word_2_real(word))
!
                call get_word(line, word)
                job % num_scan_start = nint(word_2_real(word))
                call get_word(line, word)
                job % num_scan_stop = nint(word_2_real(word))
                call get_word(line, word)
                job % scan_step = (word_2_real(word))
                    if (abs(job % scan_step).lt.1.0e-10_wp) then
                       errormsg = "scan step set too small! try:scan molecule 1 0 0 0.1"
                       Go To 1000
                    end if
                job % scanmol = .true.

                allocate (job % molmover(job % nummolmove))
                allocate (job % molmove(3,job % nummolmove))
                
                do i = 1, job % nummolmove
                    call get_line(safe,nread,line)
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'scan mol' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % molmover(i))

                    job % molmove(:,i) = 1.0_wp

                    call get_word(line, word)
                    call lower_case(word)
                    !if( word == "" .or. word(1:1) == "#" ) cycle second

                    if( trim(word) == "xyz" ) then
                        call get_word(line, word)
                        job % molmove(1,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % molmove(2,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % molmove(3,i) = abs(word_2_real(word))

                        !if( master ) write(uout,*)" xyz input for molecule # ",i," : ",job % molmove(:,i)
                    endif
                enddo
                else

                    call cry(uout,'', &
                             "ERROR: directive 'scan "//trim(word)// &
                           & "' unrecognized !!!",15)

                endif

          !??      job % vol_change_freq = nint(word_2_real(word))

          !??      ensflag = .true.
          !??      endif
!scpend


        else if (word == "move") then

            call get_word(line, word)

            if(word(1:4) == "atom") then
                call get_word(line, word)
                job % numatmmove = nint(word_2_real(word))
                call get_word(line, word)
                job % atmmovefreq = nint(word_2_real(word))
                job % moveatm = .true.

                allocate (job % atmmover(job % numatmmove))
                allocate (job % atmmover_type(job % numatmmove))
                allocate (job % atmmove(3,job % numatmmove))

                do i = 1, job % numatmmove
                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move atom' list!"
                       Go To 1000
                    end if

                    call get_word(line,word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move atom'!"
                        go to 1000
                    end if

                    job % atmmover(i) = word
                    
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move atom' list!"
                        go to 1000
                    end if
                    job % atmmover_type(i) = get_species_type(word)

                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%atmmover(i), job%atmmover_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%atmmover(i)//" "//set_species_type(job%atmmover_type(i)) &
                                   //"' in 'move atom' list not defined in FIELD!"
                        go to 1000
                    end if
                        
                    job % atmmove(:,i) = 1.0_wp

                    call get_word(line, word)
                    call lower_case(word)
                    !if( word == "" .or. word(1:1) == "#" ) cycle second

                    if( trim(word) == "xyz" ) then
                        call get_word(line, word)
                        job % atmmove(1,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % atmmove(2,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % atmmove(3,i) = abs(word_2_real(word))

                        !if( master ) write(uout,*)" xyz input for atom # ",i," : ",job % atmmove(:,i)
                    endif
                enddo
                
            else if(word(1:10) == "rotateatom") then
                call get_word(line, word)
                job % numatmrot = nint(word_2_real(word))
                call get_word(line, word)
                job % atmrotfreq = nint(word_2_real(word))
                job % rotateatm = .true.

                allocate (job % atmroter(job % numatmrot))
                allocate (job % atmroter_type(job % numatmrot)) 

                do i = 1, job % numatmrot
                    call get_line(safe,nread,line)
                    wholeline = line
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move rotateatom' list!"
                       Go To 1000
                    end if
                   
                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move rotateatom'!"
                        go to 1000
                    end if

                    job % atmroter(i) = word
                    
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move rotateatom' list!"
                        go to 1000
                    end if
                    job % atmroter_type(i) = get_species_type(word)

                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%atmroter(i), job%atmroter_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%atmroter(i)//" "//set_species_type(job%atmroter_type(i)) &
                                   //"' in 'move rotateatom' list not defined in FIELD!"
                        go to 1000
                    end if
               
                    
                enddo
                
            else if(word(1:6) == "molecu") then
                call get_word(line, word)
                job % nummolmove = nint(word_2_real(word))
                call get_word(line, word)
                job % molmovefreq = nint(word_2_real(word))
                job % movemol = .true.

                allocate (job % molmover(job % nummolmove))
                allocate (job % molmove(3,job % nummolmove))
                
                do i = 1, job % nummolmove
                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move mol' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % molmover(i))

                    job % molmove(:,i) = 1.0_wp

                    call get_word(line, word)
                    call lower_case(word)
                    !if( word == "" .or. word(1:1) == "#" ) cycle second

                    if( trim(word) == "xyz" ) then
                        call get_word(line, word)
                        job % molmove(1,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % molmove(2,i) = abs(word_2_real(word))
                        call get_word(line, word)
                        job % molmove(3,i) = abs(word_2_real(word))

                        !if( master ) write(uout,*)" xyz input for molecule # ",i," : ",job % molmove(:,i)
                    endif
                enddo

            else if (word(1:9) == "rotatemol") then

                job % rotatemol = .true.
                call get_word(line, word)
                job % nummolrot = nint(word_2_real(word))
                call get_word(line, word)
                job % molrotfreq = nint(word_2_real(word))

                allocate (job % molroter(job % nummolrot))

                do i = 1, job % nummolrot

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move rot' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % molroter(i))

                enddo

            else if (word == "volume") then

                if (ensflag) then

                    call error(14)

                endif

                call get_word(line, word)

                if( word == "gibbs" ) then
 
                    job%is_gibbs_ensemble = .true.
                    job % type_vol_move = NPT_GIBBS

                    call get_word(line, word)

                    if( word(1:4) == "freq" ) call get_word(line, word)

                !TU: Remaining directives here are for conventional (i.e. non-Gibbs) volume moves...
                else if( word == "cubic" ) then

                    call cry(uout,"(/,6('+'),/,1x,a,/,6('+'))", &
                             "NOTE: directive 'move volume "//trim(word)// &
                           & "' assumes diagonal cell matrix and equal cell dimensions!",0)

                    call get_word(line, word)

                    if( word(1:3) == "lin" .or. word(1:3) == "dim" ) then

                        job % type_vol_move = NPT_LIN

                        call get_word(line, word)
                        if( word(1:3) == "dim" ) call get_word(line, word)

                    else if( word(1:3) == "inv" ) then

                        job % type_vol_move = NPT_INV

                        call get_word(line, word)
                        if( word(1:3) == "dim" ) call get_word(line, word)

                    else if( word(1:3) == "log" .or. word(1:2) == "ln" ) then

                        job % type_vol_move = NPT_LOG

                        call get_word(line, word)
                        if( word(1:1) == "v" ) call get_word(line, word)

                    else if( indexnum(word,isflt) > 0 ) then 

                        job % type_vol_move = NPT_VOL

                    else if( word(1:4) == "freq" ) then

                        job % type_vol_move = NPT_VOL
                        call get_word(line, word)

                    endif

                else if( word(1:6) == "vector" .or. word(1:6) == "matrix" ) then

                    job % type_vol_move = NPT_VEC

                    !JG: This directive is not compatible with orthogonal implementation
                    ! in that as initially orthogonal cells will become non-orthogonal.  
                    ! By default turn off useorthgonal if this is required.
                    call cry(uout,"(/,6('+'),/,1x,a,/,6('+'))", &
                             "NOTE: directive ' volume "//trim(word)// &
                           & "' turns off code optimisations for orthorhombic cells!",0)

                    job % useorthogonal = .false.

                    call get_word(line,word)
                    if( word(1:4) == "freq" ) call get_word(line, word)

                !TU: Code for orthorhombic cells with unconcstrained cell shape - x, y and z
                !TU: cell dimensions are moved separately - anisotropic moves for ortho cells
                !TU: (ln V sampling assumed; equal probability of moves changing x, y and z
                !TU: dimensions)    
                else if( word(1:8) == "orthoani") then

                    call cry(uout,"(/,6('+'),/,1x,a,/,6('+'))", &
                             "NOTE: directive 'move volume "//trim(word)// &
                           & "' assumes diagonal cell matrix!",0)

                     job % type_vol_move = NPT_ORTHOANI

                     call get_word(line, word)
                    
                    
                !TU: Code for orthorhombic cells with constrained cell shape
                else if( word(1:5) == "ortho" ) then

                    call cry(uout,"(/,6('+'),/,1x,a,/,6('+'))", &
                             "NOTE: directive 'move volume "//trim(word)// &
                           & "' assumes diagonal cell matrix!",0)

                    call get_word(line, word)

                    if( word(1:3) == "log" .or. word(1:2) == "ln" ) then

                        job % type_vol_move = NPT_LOG
                        !job % type_vol_move = NPT_LOG_ORTHO

                        call get_word(line, word)
                        if( word(1:1) == "v" ) call get_word(line, word)

                        if( word == "xy" ) then 
                            job % is_XY_npt = .true.
                            call get_word(line, word)
                        end if
                        
                        !if( master .and. job % is_XY_npt ) &
                        !    write(uout,*)" XY-NPT requested by CONTROL"

                    else if( word(1:3) == "lin" .or. word(1:3) == "dim" .or. word(1:1) == "L" ) then

                        job % type_vol_move = NPT_LIN2

                        call get_word(line, word)
                        if( word(1:3) == "dim" ) call get_word(line, word)

                        if( word == "xy" ) then 
                            job % is_XY_npt = .true.
                            call get_word(line, word)
                        end if

                    else if( word(1:3) == "inv" ) then

                        job % type_vol_move = NPT_INV2

                        call get_word(line, word)
                        if( word(1:3) == "dim" .or. word(1:1) == "L" ) call get_word(line, word)

                        if( word == "xy" ) then 
                            job % is_XY_npt = .true.
                            call get_word(line, word)
                        end if

                    else if( indexnum(word,isflt) > 0 ) then 

                        job % type_vol_move = NPT_VOL

                    else if( word(1:4) == "freq" ) then

                        job % type_vol_move = NPT_VOL
                        call get_word(line, word)

                    endif
                    
                else

                    call cry(uout,'', &
                             "ERROR: directive 'move volume "//trim(word)// &
                           & "' unrecognized !!!",15)

                endif

                job % vol_change_freq = nint(word_2_real(word))

                ensflag = .true.

            else if (word == "gcinsertatom") then

                !TU-: Is gcmcindist safe if not put in the file?
                
                job % gcmcatom = .true.
                call get_word(line, word)
                job % numgcmcatom = nint(word_2_real(word))
                call get_word(line, word)
                job % gcmcatomfreq = nint(word_2_real(word))
                call get_word(line, word)
                job % gcmcmindist = word_2_real(word)

                allocate(job % gcmcatminsert(job % numgcmcatom))
                allocate(job % gcmcatminsert_type(job % numgcmcatom))

                allocate (job % gcmc_atompot(job % numgcmcatom))

                do i = 1, job % numgcmcatom

                    call get_line(safe,nread,line)
                    wholeline = line

!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move gcatom' list!"
                       Go To 1000
                    end if

                    !atom name
                    call get_word(line,word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move gcinsertatom'!"
                        go to 1000
                    end if

                    job % gcmcatminsert(i) = word
                    
                    !atom type
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move gcinsertatom' list!"
                        go to 1000
                    end if
                    job%gcmcatminsert_type(i) = get_species_type(word)

                    !fugacity / chem. potential / gas pressure
                    call get_word(line, word)
                    job % gcmc_atompot(i) = word_2_real(word)
                    
                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%gcmcatminsert(i), job%gcmcatminsert_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%gcmcatminsert(i)//" "//set_species_type(job%gcmcatminsert_type(i)) &
                                   //"' in 'move gcinsertatom' list not defined in FIELD!"
                        go to 1000
                    end if

                    
                enddo

            else if (word == "gcinsertmol") then

                job % gcmcmol = .true.
                call get_word(line, word)
                job % numgcmcmol = nint(word_2_real(word))
                call get_word(line, word)
                job % gcmcmolfreq = nint(word_2_real(word))
                call get_word(line, word)
                job % gcmcmindist = word_2_real(word)

                allocate (job%gcinsert_mols(job % numgcmcmol))
                allocate (job%gcmc_molpot(job % numgcmcmol))

                do i = 1, job%numgcmcmol

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'move gcmol' list!"
                       Go To 1000
                    end if

                    !molecule name
                    call get_word(line, job%gcinsert_mols(i))

                    !fugacity / chem. potential / gas pressure
                    call get_word(line, word)
                    job % gcmc_molpot(i) = word_2_real(word)

                enddo

            else if(word == "swapatoms") then

                if( job % swapmols ) then
                    errormsg = "Combination of 'swapatoms' and 'swapmols' moves is not supported!"
                    Go To 1000
                end if

                job % swapatoms = .true.
                call get_word(line, word)
                job % numswap = nint(word_2_real(word))
                call get_word(line, word)
                job % swapfreq = nint(word_2_real(word))

                allocate (job % swapele1(job % numswap), job % swapele2(job % numswap))
                allocate (job % swapele1_type(job % numswap), job % swapele2_type(job % numswap))

                allocate(job%atmswapfreq(job%numswap))
                job%atmswapfreq = 0
                
                do i = 1, job % numswap
                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'swapatoms' list!"
                       Go To 1000
                    end if
                   
                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move swapatoms'!"
                        go to 1000
                    end if
                    job % swapele1(i) = word
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move swapatoms' list!"
                        go to 1000
                    end if
                    job % swapele1_type(i) = get_species_type(word)

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move swapatoms'!"
                        go to 1000
                    end if
                    job % swapele2(i) = word
                    
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move swapatoms' list!"
                        go to 1000
                    end if
                    job % swapele2_type(i) = get_species_type(word)

                    !TU: Check that the atomic species have already been defined in FIELD
                    safe = check_atom_defined(job%swapele1(i), job%swapele1_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%swapele1(i)//" "//set_species_type(job%swapele1_type(i)) &
                                   //"' in 'move swapatoms' list not defined in FIELD!"
                        go to 1000
                    end if
                    safe = check_atom_defined(job%swapele2(i), job%swapele2_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%swapele2(i)//" "//set_species_type(job%swapele2_type(i)) &
                                   //"' in 'move swapatoms' list not defined in FIELD!"
                        go to 1000
                    end if

                    !TU: Read the frequency of this type of swap move. If it is absent then the frequency
                    !TU: keeps the default value of 0, which is used later to mean that all types of
                    !TU: atom swap moves will be performed at the same frequency
                    call get_word(line, word)
                    if( .not. ( word == "" .or. word(1:1) == "#" ) ) then
                        
                        job % atmswapfreq(i) = nint(word_2_real(word))

                        if( job % atmswapfreq(i) < 1) then
                            errormsg = "Relative frequency of atom swap move must be > 0"
                            go to 1000
                        end if
                        
                    end if
                    
                enddo

            else if(word == "swapmols") then

                if( job % swapatoms ) then
                    errormsg = "Combination of 'swapatoms' and 'swapmols' moves is not supported!"
                    Go To 1000
                end if

                job % swapmols = .true.
                call get_word(line, word)
                job % numswap = nint(word_2_real(word))
                call get_word(line, word)
                job % swapfreq = nint(word_2_real(word))

                allocate (job % swapele1(job % numswap), job % swapele2(job % numswap))

                do i = 1, job % numswap

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'swapmols' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % swapele1(i))

                    call get_word(line, job % swapele2(i))

                enddo

            else if (word == "semiwidomatoms") then

                !TU: NOTE: This is undocumented in the manual, and the functionality is untested
                !TU: (Note that there are no safety checks here regarding atom species appearing in FIELD)
                
                job % semiwidomatms = .true.
                call get_word(line, word)
                job % numsemiwidomatms= nint(word_2_real(word))
                call get_word(line, word)
                job % semiwidomatmsfreq = nint(word_2_real(word))

                allocate (job % semiwidomele1(job % numsemiwidomatms), job % semiwidomele2(job % numsemiwidomatms))
                allocate (job % semiwidom1_type(job % numsemiwidomatms), job % semiwidom2_type(job % numsemiwidomatms))

                do i = 1, job % numsemiwidomatms

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'semiwidomatoms' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % semiwidomele1(i))
                    call get_word(line, word)
                    job % semiwidom1_type(i) = get_species_type(word)

                    call get_word(line, job % semiwidomele2(i))
                    call get_word(line, word)
                    job % semiwidom2_type(i) = get_species_type(word)
                    
                enddo

            else if (word == "semigrandatoms") then

                job % semigrandatms = .true.
                call get_word(line, word)
                job % numsemigrandatms = nint(word_2_real(word))
                call get_word(line, word)
                job % semigrandatmsfreq = nint(word_2_real(word))
                call get_word(line, word)

                !TU: Guard against Deltamu being specified on the same line as 'semigrandatoms',
                !TU: which is the old obsolete way of specifying Deltamu - which I have changed
                !TU: because it does not allow multiple semi-grand move types to have different
                !TU: Deltamu values
                if( .not. ( word == "" .or. word(1:1) == "#" ) ) then

                    errormsg = "Unexpected value/word on 'semigrandatoms' line!"
                    Go To 1000                    

                end if

                allocate (job % deltatrans(job % numsemigrandatms))
                allocate (job % semigrandele1(job % numsemigrandatms), job % semigrandele2(job % numsemigrandatms))
                allocate (job % semigrand1_type(job % numsemigrandatms), job % semigrand2_type(job % numsemigrandatms))

                do i = 1, job % numsemigrandatms
                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'semigrandatoms' list!"
                       Go To 1000
                    end if

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move semigrandatoms'!"
                        go to 1000
                    end if
                    job % semigrandele1(i) = word

                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move semigrandatoms' list!"
                        go to 1000
                    end if                    
                    job % semigrand1_type(i) = get_species_type(word)

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move semigrandatoms'!"
                        go to 1000
                    end if
                    job % semigrandele2(i) = word

                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move semigrandatoms' list!"
                        go to 1000
                    end if                    
                    job % semigrand2_type(i) = get_species_type(word)

                    !TU: Read the Deltamu for this pair of atomic species
                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move semigrandatoms'!"
                        go to 1000
                    end if
                    job % deltatrans(i) = word_2_real(word)
                    

                    !TU: For semigrand involving spin type atoms make sure both types are of type spin
                    if( job%semigrand1_type(i) == ATM_TYPE_SPIN .or. job%semigrand2_type(i) == ATM_TYPE_SPIN ) then

                        if( .not. (job%semigrand1_type(i) == ATM_TYPE_SPIN .and. job%semigrand2_type(i) == ATM_TYPE_SPIN) ) then

                            call cry(uout,'',"ERROR: Semi-grand forbidden between a spin and a non-spin type atom!",999)

                        end if

                    end if

                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%semigrandele1(i), job%semigrand1_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%semigrandele1(i)//" "//set_species_type(job%semigrand1_type(i)) &
                                   //"' in 'move semigrandatoms' list not defined in FIELD!"
                        go to 1000
                    end if
                    safe = check_atom_defined(job%semigrandele2(i), job%semigrand2_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%semigrandele2(i)//" "//set_species_type(job%semigrand2_type(i)) &
                                   //"' in 'move semigrandatoms' list not defined in FIELD!"
                        go to 1000
                    end if
                    
                enddo

            else if (word == "semigrandmol") then

                job%semigrandmols = .true.

                call get_word(line, word)
                job % numsemigrandmol = nint(word_2_real(word))
                call get_word(line, word)
                job % semigrandmolfreq = nint(word_2_real(word))
                call get_word(line, word)

                !TU: Guard against Deltamu being specified on the same line as 'semigrandatoms',
                !TU: which is the old obsolete way of specifying Deltamu - which I have changed
                !TU: because it does not allow multiple semi-grand move types to have different
                !TU: Deltamu values
                if( .not. ( word == "" .or. word(1:1) == "#" ) ) then

                    errormsg = "Unexpected value/word on 'semigrandmol' line!"
                    Go To 1000                    

                end if

                allocate (job % deltatrans(job % numsemigrandmol))
                allocate(job%semigrand1_mols(job % numsemigrandmol))
                allocate(job%semigrand2_mols(job % numsemigrandmol))

                !molecule pairs to be swapped
                do i = 1, job%numsemigrandmol

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'semigrandmol' list!"
                       Go To 1000
                    end if

                    !molecule names
                    call get_word(line, job%semigrand1_mols(i))
                    call get_word(line, job%semigrand2_mols(i))

                    !TU: Read the Deltamu for this pair of molecule species
                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move semigrandmols'!"
                        go to 1000
                    end if
                    job % deltatrans(i) = word_2_real(word)

                    !TU**: TO DO: Add safety checks: that the molecule types are defined in FIELD

                enddo

            else if (word == "gibbstransfatom") then

                job% gibbsatomtran = .true.
                job%is_gibbs_ensemble = .true.
                call get_word(line, word)
                job % num_atmtran_gibbs = nint(word_2_real(word))
                call get_word(line, word)
                job % gibbs_atmtran_freq = nint(word_2_real(word))
                call get_word(line, word)

                job % gibbsmindist = nint(word_2_real(word))
                allocate(job % gibbsatmtrans(job % num_atmtran_gibbs))
                allocate(job % gibbsatmtrans_type(job % num_atmtran_gibbs))

                do i = 1, job % num_atmtran_gibbs

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'gibbstransfatom' list!"
                       Go To 1000
                    end if

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move gibbstransfatom'!"
                        go to 1000
                    end if
                    job % gibbsatmtrans(i) = word
                    
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move gibbstransfatom' list!"
                        go to 1000
                    end if
                    job%gibbsatmtrans_type(i) = get_species_type(word)

                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%gibbsatmtrans(i), job%gibbsatmtrans_type(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%gibbsatmtrans(i)//" "//set_species_type(job%gibbsatmtrans_type(i)) &
                                   //"' in 'move gibbstransfatom' list not defined in FIELD!"
                        go to 1000
                    end if
                    
                enddo

            else if (word == "gibbsexchatom") then

                job% gibbsatomexch = .true.
                job%is_gibbs_ensemble = .true.

                call get_word(line, word)
                job % num_atmexch_gibbs = nint(word_2_real(word))
                call get_word(line, word)
                job % gibbs_atmexch_freq = nint(word_2_real(word))
                call get_word(line, word)

                allocate(job % gibbsatmexchange1(job % num_atmexch_gibbs))
                allocate(job % gibbsatmexchange2(job % num_atmexch_gibbs))
                allocate(job % gibbsatmexchange_type1(job % num_atmexch_gibbs))
                allocate(job % gibbsatmexchange_type2(job % num_atmexch_gibbs))

                do i = 1, job % num_atmexch_gibbs

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'gibbsexchatom' list!"
                       Go To 1000
                    end if

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move gibbsexchatom'!"
                        go to 1000
                    end if
                    job % gibbsatmexchange1(i) = word
                    
                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move gibbsexchatom' list!"
                        go to 1000
                    end if
                    job%gibbsatmexchange_type1(i) = get_species_type(word)

                    call get_word(line, word)
                    if( word == "" .or. word(1:1) == "#" ) then
                        errormsg = "Unexpected blank line or line starting with '#' after 'move gibbsexchatom'!"
                        go to 1000
                    end if
                    job % gibbsatmexchange2(i) = word

                    call get_word(line, word)
                    if(.not. valid_species_type(word)) then
                        errormsg = "Invalid atom type '"//trim(word)//"' in 'move gibbsexchatom' list!"
                        go to 1000
                    end if
                    job%gibbsatmexchange_type2(i) = get_species_type(word)

                    !TU: Check that the atomic species has already been defined in FIELD
                    safe = check_atom_defined(job%gibbsatmexchange1(i), job%gibbsatmexchange_type1(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%gibbsatmexchange1(i)//" "//set_species_type(job%gibbsatmexchange_type1(i)) &
                                   //"' in 'move gibbsexchatom' list not defined in FIELD!"
                        go to 1000
                    end if
                    safe = check_atom_defined(job%gibbsatmexchange2(i), job%gibbsatmexchange_type2(i))
                    if( .not. safe ) then
                        errormsg = "Atom type '"//job%gibbsatmexchange2(i)//" "//set_species_type(job%gibbsatmexchange_type2(i)) &
                                   //"' in 'move gibbsexchatom' list not defined in FIELD!"
                        go to 1000
                    end if


                    
                enddo

            else if (word == "gibbstransfmol") then

                job% gibbsmoltran = .true.
                job%is_gibbs_ensemble = .true.
                call get_word(line, word)
                job % num_moltran_gibbs = nint(word_2_real(word))
                call get_word(line, word)
                job % gibbs_moltran_freq = nint(word_2_real(word))
                call get_word(line, word)

                job % gibbsmindist = nint(word_2_real(word))
                allocate(job % gibbsmolinsert(job % num_moltran_gibbs))

                do i = 1, job % num_moltran_gibbs

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'gibbstransfmol' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % gibbsmolinsert(i))

                enddo

            else if (word == "gibbsexchmol") then

                job% gibbsmolexch = .true.
                job%is_gibbs_ensemble = .true.
                call get_word(line, word)
                job % num_molexch_gibbs = nint(word_2_real(word))
                call get_word(line, word)
                job % gibbs_molexch_freq = nint(word_2_real(word))
                call get_word(line, word)

                allocate(job % gibbsmolexchange1(job % num_molexch_gibbs))
                allocate(job % gibbsmolexchange2(job % num_molexch_gibbs))

                do i = 1, job % num_molexch_gibbs

                    call get_line(safe,nread,line)
                    wholeline = line
!                    if (.not.safe) Go To 1000
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly in 'gibbsexchmol' list!"
                       Go To 1000
                    end if
                    call get_word(line, job % gibbsmolinsert(i))

                enddo

            else if (word == "lambda") then

                job % lambdamoves = .true.
                call get_word(line, word)

                if(word =="onemolecule") then

                    job % lambdaonemolecule = .true.

                    call get_word(line, word)

                end if

                job % lambdamovefreq = nint(word_2_real(word))

                if( job % lambdaonemolecule ) then

                    call get_line(safe,nread,line)
                    if (.not.safe) then
                       errormsg = "end of file reached unexpectedly after 'move lambda onemolecule'!"
                       Go To 1000
                    end if
                    call get_word(line, job % lambdamoltarget)

                end if


            else if (word == "lambdainsertmol") then

                job % lambdainsertmol = .true.

                call get_word(line, word)
                job % lambdainsertfreq = nint(word_2_real(word))

                call get_line(safe,nread,line)
                if (.not.safe) then
                    errormsg = "end of file reached unexpectedly after 'move lambdainsertmol'!"
                    go to 1000
                end if
 
                call get_word(line, job % lambdainsert_real)
                call get_word(line, job % lambdainsert_frac)                                

                !TU**: Depending on flags in the 'use' block, the 'activity' read here may be in fact the
                !TU**: chemical potential in various units or the fugacity (partial pressure). Conversion
                !TU**: of this into the activity is done elsewhere.
                call get_word(line, word)
                job % lambdainsert_activity = word_2_real(word)

            else

                call cry(uout,'', &
                         "ERROR: directive 'move "//trim(word)//"' unrecognised!" &
                         //achar(10)//"        Problematic line is: '"//trim(wholeline),11)
            endif

        else if (word == "maxatmdist") then

            call get_word(line, word)
            job % iondisp = word_2_real(word)

        else if (word == "maxmoldist") then

            call get_word(line, word)
            job % moldisp = word_2_real(word)

        else if (word == "maxmolrot") then

            call get_word(line, word)
            job % molrot = word_2_real(word)
            
        else if (word == "maxatmrot") then

            call get_word(line, word)
            job % atmrot = word_2_real(word)
            

        else if (word(1:8) == "maxvolch") then

            call get_word(line, word)
            job % maxvolchange = word_2_real(word)

        else if (word(1:18) == "acceptatmmoveratio") then

            call get_word(line, word)
            job % acc_atmmve_ratio = word_2_real(word)

        else if (word(1:19) == "acceptatmmoveupdate") then

            call get_word(line, word)
            job % acc_atmmve_update = word_2_real(word)

        else if (word(1:18) == "acceptmolmoveratio") then

            call get_word(line, word)
            job % acc_molmve_ratio = word_2_real(word)

        else if (word(1:19) == "acceptmolmoveupdate") then

            call get_word(line, word)
            job % acc_molmve_update = word_2_real(word)


        else if (word(1:17) == "acceptmolrotratio") then

            call get_word(line, word)
            job % acc_molrot_ratio = word_2_real(word)

        else if (word(1:18) == "acceptmolrotupdate") then

            call get_word(line, word)
            job % acc_molrot_update = word_2_real(word)

        else if (word(1:17) == "acceptatmrotratio") then

            call get_word(line, word)
            job % acc_atmrot_ratio = word_2_real(word)

        else if (word(1:18) == "acceptatmrotupdate") then

            call get_word(line, word)
            job % acc_atmrot_update = word_2_real(word)
            
        else if (word(1:14) == "acceptvolratio") then

            call get_word(line, word)
            job % acc_vol_ratio = word_2_real(word)

        else if (word(1:15) == "acceptvolupdate") then

            call get_word(line, word)
            job % acc_vol_update = word_2_real(word)

        else if (word(1:11) == "maxlambdach") then

            call get_word(line, word)
            job % maxlambdachange = word_2_real(word)

        else if (word == "initiallambda") then

            call get_word(line, word)
            job % initiallambda = word_2_real(word)

        else if (word(1:7) == "restart") then
!AB: not clear what does this do - ?
!AB: it is not described in the manual!

            finished = .true.
            job % sysrestart = .true.

        else if (word(1:5) == "start") then

            finished = .true.

        else

            call cry(uout,'', &
                "ERROR: unrecognised directive in CONTROL: '"//trim(word)//"'!!!" &
                //achar(10)//"        Problematic line is: '"//trim(wholeline)//"'",3)

        endif

    enddo second

    if (.not.finished) then

        !TU: I think this is unreachable; the error  'end of file reached unexpectedly' is
        !TU: triggered if 'start' is missing
        
        call error(7)

    endif

    job % is_gcmc = ( job%gcmcatom .or. job%gcmcmol .or. job%gibbsatomtran .or. job%gibbsmoltran & 
                             .or. job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch )

    !AB: based on the above, initialise 2D-slit paramarters if needed
    !AB: on the way check for consistency between cell_type and coultype
    call initialise_slit(job, 1)

    return

1000 continue  !call error(4)

    call cry(uout,'',"ERROR: reading CONTROL has stopped : "//trim(errormsg) &
               //achar(10)//"        Problematic line is: '"//trim(wholeline)//"'", 999)

end subroutine read_control


!> @brief
!> - Printing and ensuring consistency of the simulation parameters and flags stored in control structure `job`
!> @using
!> - `kinds_f90`
!> - `constants_module`
!> - `control_type`
!> - `species_module`, only : set_species_type
!> - `comms_mpi_module`, only : master,mxnode,msg_bcast
!> - `parallel_loop_module`, only : idgrp
!> - `external_potential_module`, only : nextpot
!> - `slit_module`
!>
!> print and check simulation parameters & switches
subroutine printandcheck(job)

    use constants_module
    use control_type
    use species_module, only : set_species_type
    use comms_mpi_module, only : master,mxnode,msg_bcast
    use parallel_loop_module, only : idgrp
    use external_potential_module, only : nextpot
    use slit_module

    implicit none

        !> structure containing global simulation control switches & parameters
    type(control), intent(inout) :: job

    integer   :: i, ncfg
    character :: tag*1
    integer   :: time
    integer, dimension(4) :: seed

    logical :: is_GCMC

    is_GCMC = .false.

    time = 0
    seed = 0

        !if( master ) flush(uout)

        if( job%is_slit ) then

            job%is_XY_npt = .true.

            if( master ) &
                write(uout,"(/,1x,'cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] = ',i10,' (slit: XY-PBC)')") &
                      job % cell_type

            if( job%cell_type > -1 ) call error(28)

            if( job%cell_type == SLIT_HARD2 .and. nextpot > 0 ) call error(29)

            if( job%cell_type > SLIT_HARD2 .and. nextpot == 0 ) call error(29)

            !if ( all(job%coultype == 1) .and. mfa_type==0 .and. is_slit_frac_z ) then
            if ( job%coultype(1) == 1 .and. mfa_type==0 .and. is_slit_frac_z ) then
            
               if( master ) &
               write(uout,"(/,1x,'cell (box) type [ < 0 -> 2D-slit; inner slab!!!] = ',i10,' coultype = ',10i10)") &
                   & slit_type, job%coultype, mfa_type
            
            else if( job%coultype(1) > 0 ) then
                !if( job%coultype(1) > 0 .or. job%coultype(1) == -1 ) call error(21)
                call error(21)
            
            end if

            if( job%nzden < 1 .or. .not.job%lzdensity ) call error(35)

            !AB: this should have been done above (see initialise_slit())
            !if( job % coultype(1) == -2 ) job % cell_type = job % cell_type*10-abs(mfa_type)

            if( is_slit_frac_z ) then 
            !if( abs(slit_frac_z) > 0.0_wp ) then

                if( slit_frac_z > 0.0_wp ) then
                !AB: botoom & top are set symmetrically relative to the center

                    slit_zfrac1 =-slit_frac_z
                    slit_zfrac2 = slit_frac_z

                else if( slit_frac_z < 0.0_wp ) then
                !AB: botoom & top are symmetrically shifted inwards

                    slit_zfrac1 =-0.5_wp - slit_frac_z
                    slit_zfrac2 = 0.5_wp + slit_frac_z

                else if( slit_gcmc_z > 0.0_wp ) then
                !AB: botoom is shifted inwards

                    slit_zfrac1 = slit_gcmc_z - 0.5_wp

                else if( slit_gcmc_z < 0.0_wp ) then
                !AB: top is shifted inwards

                    slit_zfrac2 = slit_gcmc_z + 0.5_wp

                end if

            end if

            if( master ) then

                write(uout, "(/,1x,a,2i10)") &
                     'SLIT (planar pore): number of hard / soft walls  = ', &
                     (2-walls_soft), walls_soft

                if( abs(slit_frac_z) > 0.0_wp ) then

                    write(uout, "(/,1x,a,3f12.6)") &
                     'SLIT (planar pore): fract. Z-shift for two walls = ', slit_frac_z, &
                                                                            slit_zfrac1, &
                                                                            slit_zfrac2
                else if( abs(slit_gcmc_z) > 0.0_wp ) then

                    write(uout, "(/,1x,a,3f12.6)") &
                     'SLIT (planar pore): fract. Z-shift for one wall  = ', slit_gcmc_z, &
                                                                            slit_zfrac1, &
                                                                            slit_zfrac2
                end if

                write(uout, "(/,1x,a,i10,2f12.6)") &
                     'SLIT (planar pore): number of charged walls, SCD = ', &
                     walls_charged, SCD1, SCD2

                if( job % coultype(1) == 0 ) then

                    write(uout,"(/,1x,'- no charges, no electrostatics required; pure short-range/VdW/metal(?) force-field')")

                !else if( job % coultype(1) == -2 ) then
                else !if( job % coultype(1) == -1 ) then

                    write(uout, "(/,1x,a,2i10,f15.7,2f4.0)") &
                         'SLIT (planar pore): MFA correction cutoff type   = ', &
                         mfa_type,mfa_ncycles,damp_mfa,onoff_rc,onoff_xy

                    if( mfa_type /= 0 .and. job % statsout > job % sysprint ) then 

                        write(uout,"(/,1x,'- MFA is invoked: statsout must be <= sysprint (reset)! ')")

                        job % statsout = job % sysprint
                    end if

                end if

            end if

        else

            if( master ) &
                write(uout,"(/,1x,'cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] = ',i10,' (bulk: XYZ-PBC)')") &
                      job % cell_type

            if( job % cell_type < 0 ) call error(28)

!TU**: DEBUGGING: TRIPS IF SLIT IS NOT IN USE WITH DIRECT COULOMB
!!$            if( job % coultype(1) < 0 ) call error(21)

            if( job % coultype(1) == 0 .and. master ) &
            & write(uout,"(/,1x,'- no charges, no electrostatics required; pure short-range/VdW/metal(?) force-field')")

        end if

    !if( master ) flush(uout)

    if (master) &
       & write(uout,"(/,1x,'global cutoff for interactions (Angstroms)       = ',e15.7)") job % shortrangecut 

    if (job%vdw_cut < 1.0e-10_wp) job%vdw_cut = job%shortrangecut
    if (job%vdw_cut > job%shortrangecut ) job%vdw_cut = job%shortrangecut

    if (master) &
       & write(uout,"(/,1x,'cutoff for vdw interactions (Angstroms)          = ',e15.7)") job % vdw_cut

    if(job%uselist) then

        !AB: temporarily disallow using Verlet neighbour lists for GCMC and Gibbs ensemble
        if( job % gcmcatom .or. job % gcmcmol .or. job%gibbsatomtran .or. job%gibbsmoltran & 
                           .or. job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch ) & 
            call cry(uout,'',"Verlet neighbour lists cannot be used with GC and Gibbs ensembles!!!",999)

        if (master) &
        & write(uout,"(/,1x,'verlet shell distance (Angstroms)                = ',e15.7)") job % verletshell

    else

        job % verletshell = 1.0e-6

        if (master) &
        & write(uout,"(/,1x,'Verlet neighbour lists not used                  = ',2l3)") job % uselist, job%lauto_nbrs

    endif

    if (master) &
        & write(uout,"(/,1x,'system temperature (Kelvin)                      = ',e15.7)") job % systemp    

    if (job % systemp == 0) then
        
        call error(8)

    endif

    if( job % type_vol_move == NPT_VEC ) then

        if( job%is_slit ) &
            call cry(uout,'',"ERROR: anisotropic cell distortion is not allowed for slit !!!",999)

        if (master) then

            !write(uout,"(/,1x,'flexible cell expansion in general case')")
            write(uout,"(/,1x,'anisotropic NPT ensemble for FLEXIBLE cell,type  = ',i10)")   job % type_vol_move
            write(uout,"(/,1x,'frequency of maximum V-step updates  (MC cycles) = ',i10)")   job % acc_vol_update    
            write(uout,"(/,1x,'optimum acceptance for volume moves     (target) = ',e15.7)") job % acc_vol_ratio    
            write(uout,"(/,1x,'initial maximum V-step    (type-dependent units) = ',e15.7)") job % maxvolchange
            if( job % lsample_volume ) then
                write(uout,"(/,1x,'V-grid bin size for P(V) distribution            = ',e15.7)") job % vol_bin
                write(uout,"(/,1x,'V-grid power for P(V) distribution  (dV*i^power) = ',e15.7)") job % vol_bin_power
            end if

        else if( job%is_slit ) then

            call error(999)

        endif

    !TU: Code for anisotropic volume moves for orthorhombic cells
    else if( job%type_vol_move == NPT_ORTHOANI ) then

        if( job%is_slit ) &
            call cry(uout,'',"ERROR: anisotropic cell distortion is not allowed for slit !!!",999)

        if (master) then

            write(uout,"(/,1x,'anisotropic NPT ensemble, ORTHORHOMBIC cell,  type = ',i10)") job % type_vol_move            
            write(uout,"(/,1x,'frequency of maximum V-step updates  (MC cycles) = ',i10)")   job % acc_vol_update    
            write(uout,"(/,1x,'optimum acceptance for volume moves     (target) = ',e15.7)") job % acc_vol_ratio    
            write(uout,"(/,1x,'initial maximum V-step    (type-dependent units) = ',e15.7)") job % maxvolchange
            if( job % lsample_volume ) then
                write(uout,"(/,1x,'V-grid bin size for P(V) distribution            = ',e15.7)") job % vol_bin
                write(uout,"(/,1x,'V-grid power for P(V) distribution  (dV*i^power) = ',e15.7)") job % vol_bin_power
            end if

        end if
        
        
    !TU: Code for shape-preserving volume moves for orthorhombic cells
    !else if( job%type_vol_move == NPT_LOG_ORTHO ) then
    else if( job%type_vol_move == NPT_LOG .or. job%type_vol_move == NPT_VOL ) then

        if (master) then

            if( job % is_XY_npt ) then
                write(uout,"(/,1x,'semiisotropic XY-NPT ensemble, ORTHORHOMBIC type = ',i10)") job % type_vol_move
            else
                write(uout,"(/,1x,'isotropic NPT ensemble, ORTHORHOMBIC cell,  type = ',i10)") job % type_vol_move
            end if
            
            write(uout,"(/,1x,'frequency of maximum V-step updates  (MC cycles) = ',i10)")   job % acc_vol_update    
            write(uout,"(/,1x,'optimum acceptance for volume moves     (target) = ',e15.7)") job % acc_vol_ratio    
            write(uout,"(/,1x,'initial maximum V-step    (type-dependent units) = ',e15.7)") job % maxvolchange
            if( job % lsample_volume ) then
                write(uout,"(/,1x,'V-grid bin size for P(V) distribution            = ',e15.7)") job % vol_bin
                write(uout,"(/,1x,'V-grid power for P(V) distribution  (dV*i^power) = ',e15.7)") job % vol_bin_power
            end if

        end if

    else if( job%type_vol_move == NPT_INV .or. job%type_vol_move == NPT_LIN &
        .or. job%type_vol_move == NPT_INV2 .or. job%type_vol_move == NPT_LIN2 ) then

        !if( job%is_slit .or. job % is_XY_npt ) then
        if( job % is_XY_npt ) then
            if( job%type_vol_move == NPT_LIN ) job%type_vol_move = NPT_LIN2
            if( job%type_vol_move == NPT_INV ) job%type_vol_move = NPT_INV2
        end if

        if (master) then

            if( job % is_XY_npt ) then
                write(uout,"(/,1x,'semiisotropic XY-NPT ensemble, ORTHORHOMBIC type = ',i10)") job % type_vol_move
            else
                write(uout,"(/,1x,'isotropic NPT ensemble for CUBIC cell, move type = ',i10)") job % type_vol_move
            end if
            
            write(uout,"(/,1x,'frequency of maximum V-step updates  (MC cycles) = ',i10)")   job % acc_vol_update    
            write(uout,"(/,1x,'optimum acceptance for volume moves     (target) = ',e15.7)") job % acc_vol_ratio    
            write(uout,"(/,1x,'initial maximum V-step     (type dependent unit) = ',e15.7)") job % maxvolchange
            if( job % lsample_volume ) then
                write(uout,"(/,1x,'V-grid bin size for P(V) distribution       (dV) = ',e15.7)") job % vol_bin
                write(uout,"(/,1x,'V-grid power for P(V) distribution  (dV*i^power) = ',e15.7)") job % vol_bin_power
            end if

        endif

    else if( job%type_vol_move == NPT_GIBBS ) then

        if (master) then

            write(uout,"(/,1x,'Gibbs ensemble with coupled volume moves,  type  = ',i10)") job % type_vol_move

            write(uout,"(/,1x,'frequency of maximum V-step updates  (MC cycles) = ',i10)")   job % acc_vol_update    
            write(uout,"(/,1x,'optimum acceptance for volume moves     (target) = ',e15.7)") job % acc_vol_ratio    
            write(uout,"(/,1x,'initial maximum V-step     (type dependent unit) = ',e15.7)") job % maxvolchange
            if( job % lsample_volume ) then
                write(uout,"(/,1x,'V-grid bin size for P(V) distribution       (dV) = ',e15.7)") job % vol_bin
                write(uout,"(/,1x,'V-grid power for P(V) distribution  (dV*i^power) = ',e15.7)") job % vol_bin_power
            end if

        end if

    else

        job % type_vol_move = 0
        job % maxvolchange  = 0.0_wp
        job % vol_bin       = 0.0_wp
        job % vol_bin_power = 1.0_wp
        
        job % is_XY_npt = .false.
        job % lsample_volume = .false.
        
        if (master) write(uout,"(/,1x,'constant volume simulation (NVT), i.e. volume not sampled')")

    endif

    if(job%syspres_kunits) then

        if(master) write(uout,"(/,1x,'system pressure (K units)                      = ',e15.7)") job % syspres    
        job % syspres = job % syspres * BOLTZMAN

    else

        if(master) write(uout,"(/,1x,'system pressure (katms)                          = ',e15.7)") job % syspres    
        job % syspres = job % syspres / PRSFACT

    end if


!   if(job % calcstress) write(uout,"(/,1x,'stresses will be calculated')")

    if (master) &
        & write(uout,"(/,1x,'total number of MC steps (cycles)                = ',i10)") job % maxiterations

    if (master) &
        & write(uout,"(/,1x,'number of equilibration steps                    = ',i10)") job % sysequil

    if (master) &
        & write(uout,"(/,1x,'number of pre-heating steps                      = ',i10)") job % sysheat

    if (job % sysheat > job % sysequil) then

        call error(16)

    endif

    if (master) &
        &  write(uout,"(/,1x,'maximum no of non-bonded neighbours              = ',i10)") job % max_nonb_nbrs

    if (job%uselist .and. master) then

        write(uout,"(/,1x,'neighbour list will be used')")

        if (job%lauto_nbrs) then

            write(uout,"(/,1x,'nbrlist will be recalculated as necessary')")

        else

            write(uout,"(/,1x,'neighbour list updated every (cycles)            = ',i10)") job % nbrlist

        endif

    endif

    !TU: Neighbour list checks can only be used if neighbour lists are used
    if( job % nbrlistcheck .and. (.not. job%uselist) ) then

        call cry(uout,'',"ERROR: Neighbour list checks cannot be used without the use of neighbour lists!",999)

    end if

    if (master) then

        if( job%nbrlistcheck ) &
            write(uout,"(/,1x,'neighbour list checks used every (cycles)        = ',i10)") job % nbrlistcheckfreq

        if( job%speciescountcheck ) &
            write(uout,"(/,1x,'species count checks used every (cycles)         = ',i10)") job % speciescountcheckfreq

        write(uout,"(/,1x,'maximum no of three-body triplets per atom       = ',i10)") job % max_thb_nbrs 

        write(uout,"(/,1x,'three body list updated every (cycles)           = ',i10)") job % thblist

        !TU*: Added by me...
        write(uout,"(/,1x,'statistics sampled for averages every (cycles)   = ',i10)") job % sample_stats_freq

        !TU*: Is it really 'block averaging' that is done?
        write(uout,"(/,1x,'stack-size for block averaging                   = ',i10)") job % stacksize
    
        write(uout,"(/,1x,'statistics data saved every (cycles) -> PTFILE*  = ',i10)") job % statsout
    
        write(uout,"(/,1x,'energy frames printed every (cycles) -> OUTPUT*  = ',i10)") job % sysprint
    
        write(uout,"(/,1x,'energy checks printed every (cycles) -> OUTPUT*  = ',i10)") job % syscheck
    
        write(uout,"(/,1x,'level of precision used in energy checks         = ',e15.7)") job % checkenergyprec
    
        write(uout,"(/,1x,'configurations stored every (cycles) -> REVCON*  = ',i10)") job % sysdump

        if( job % archive_format == DLP2 ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> HISTORY* = ',2i10,' (DL_POLY-2 format)')") &
                       job % sysdump, job%nfltprec
            !write(uout,"(/,1x,'TRAJECTORY* is stored every (cycles) as DCD file = ',2i10)") job % sysdump, &
            !           job%maxiterations/job%sysdump

            !AB: make REVCON consistent with HISTORY & readable by VMD
            !job % revcon_format = DLP2 !=2

        else if( job % archive_format == DLP2DCD ) then

            write(uout,"(/,1x,'trajectories archived every (cycles) -> HISTORY* = ',2i10,' (DL_POLY-2 format)')") &
                       job % sysdump, job%nfltprec
            
            if( trim(adjustL(job % simulationmode)) /= "trajconv" ) then
                write(uout,"(/,1x,'TRAJECTORY* is stored every (cycles) as DCD file = ',2i10)") job % sysdump, &
                      job%maxiterations/job%sysdump
            else
                job % archive_format = DLP2 !=2
            end if

            !AB: make REVCON consistent with HISTORY & readable by VMD
            !job % revcon_format = 2

        else if( job % archive_format == DLP4 ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> HISTORY* = ',i10,' (DL_POLY-4 format)')") job % sysdump
            !write(uout,"(/,1x,'TRAJECTORY* is stored every (cycles) as DCD file = ',2i10)") job % sysdump, &
            !           job%maxiterations/job%sysdump

            !AB: make REVCON consistent with HISTORY & readable by VMD
            !job % revcon_format = DLP4 !=4

        else if( job % archive_format == DLP4DCD ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> HISTORY* = ',i10,' (DL_POLY-4 format)')") job % sysdump

            if( trim(adjustL(job % simulationmode)) /= "trajconv" ) then
                write(uout,"(/,1x,'TRAJECTORY* is stored every (cycles) as DCD file = ',2i10)") job % sysdump, &
                      job%maxiterations/job%sysdump
            else
                job % archive_format = DLP4 !=4
            end if

            !AB: make REVCON consistent with HISTORY & readable by VMD
            !job % revcon_format = DLP4

        else if( job % archive_format == DCD ) then
            write(uout,"(/,1x,'TRAJECTORY* is stored every (cycles) as DCD file = ',2i10)") job % sysdump, &
                       job%maxiterations/job%sysdump

            !AB: make REVCON readable by VMD
            !job % revcon_format = DLP2 ! DLP must be the format compatible with DLM eventually

        !TU: VTK format is still experimental and under development...
        else if( job % archive_format == VTK ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> VTK* = ',i10)") job % sysdump

        else if( job % archive_format == XYZ ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> XYZ* = ',i10)") job % sysdump
            
        else if( job % archive_format > DLM ) then
            write(uout,"(/,1x,'trajectories archived every (cycles) -> ARCHIVE* = ',i10,' (DL_POLY style)')") &
                       job % sysdump
        else 
            write(uout,"(/,1x,'trajectories archived every (cycles) -> ARCHIVE* = ',i10,' (DL_MONTE style)')") &
                       job % sysdump
        end if

        if( job % archive_format < DLP2 .and. len_trim(job % hist_molids) > 0 ) then
            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: inconsistent directive 'sample coordinates' in CONTROL file - "//&
                    &"keyword 'only' followed by subset of molecules cannot be used with the given format !!!",999)
        end if

        if( job % revcon_format == DLP4 ) then
            write(uout,"(/,1x,'REVCON configuration data in DL_POLY-4 HISTORY format')")
        else if( job % revcon_format == DLP2 ) then
            write(uout,"(/,1x,'REVCON configuration data in DL_POLY-2 HISTORY format')")
        else if( job % revcon_format == DLP ) then
            write(uout,"(/,1x,'REVCON configuration data in DL_POLY style (DLP/DLM hybrid)')")
        !TU: VTK format REVCON is still experimental and under development...
        else if( job % revcon_format == VTK ) then
            write(uout,"(/,1x,'REVCON configuration data in Legacy VTK format')")
        else if( job % revcon_format == XYZ ) then
            write(uout,"(/,1x,'REVCON configuration data in Extended XYZ format')")
        else 
            write(uout,"(/,1x,'REVCON configuration data in DL_MONTE style (extended native)')")
        end if

        !TU: Forbid the Gibbs ensemble with DCD because it is not supported
        if( ( job%archive_format == DLP2DCD .or. job%archive_format > DLP4 ) & 
            .and. job%is_gibbs_ensemble  ) then

            call cry(uout,'(/,1x,a,/)', &
                     "ERROR: DCD trajectory format is not supported with Gibbs ensemble."// &
                      " Amend 'archiveformat' in CONTROL.",999)

        end if

        if( trim(adjustL(job % simulationmode)) == "trajconv" ) then

            call cry(uout,'(/,1x,a,/)', &
                     "MODE: found directive 'trajconvert' in CONTROL"//&
                    &" - preparing for trajectory conversion...",0)

            if( job % archive_format == job % traj_format ) &
                call cry(uout,'(/,1x,a,/)', &
                         "ERROR: trajectory input and output formats "//&
                        &"are the same in CONTROL - nothing to do !!!",999)

            if( job % traj_format == DCD ) then

              if( job % archive_format == DLP2 ) then

                call cry(uout,'(/,1x,a,/)', &
                         "TRAJECTORY [DCD] -> HISTRAJ [DL_POLY-2]"//&
                        &" - converting existing trajectory",0)

              else if( job % archive_format == DLP4 ) then

                call cry(uout,'(/,1x,a,/)', &
                         "TRAJECTORY [DCD] -> HISTRAJ [DL_POLY-4]"//&
                        &" - converting existing trajectory",0)

              else if( job % archive_format == DLP ) then

                call cry(uout,'(/,1x,a,/)', &
                         "TRAJECTORY [DCD] -> ARCTRAJ [DL_POLY style]"//&
                        &" - converting existing trajectory",0)

              else if( job % archive_format == DLM ) then

                call cry(uout,'(/,1x,a,/)', &
                         "TRAJECTORY [DCD] -> ARCTRAJ [DL_MONTE style]"//&
                        &" - converting existing trajectory",0)

              else 

                  call cry(uout,'(/,1x,a,/)', &
                           "ERROR: unsupported trajectory conversion - "//&
                          &"revise input -> output formats in CONTROL !!!",999)

              end if

              if( len_trim(adjustL(job % hist_molids)) > 0 .and. job % archive_format > DLP ) then

                  write(uout,"(/,1x,'the subset of molecules (coordinates) to be stored - check :')")
                  write(uout,"(/,1x,a)")trim(adjustL(job % hist_molids))

              end if

            else if( job % archive_format == DCD ) then

              if( job % traj_format < DLP2 ) then

                call cry(uout,'(/,1x,a,/)', &
                         "ARCHIVE [DL_MONTE or DL_POLY style] -> TRAJARC [DCD]"//&
                        &" - converting existing trajectory",0)

              else if( job % traj_format < DCD ) then

                call cry(uout,'(/,1x,a,/)', &
                         "HISTORY [DL_POLY 2 or 4] -> TRAJHIS [DCD]"//&
                        &" - converting existing trajectory",0)

              else 

                  call cry(uout,'(/,1x,a,/)', &
                           "ERROR: unsupported trajectory conversion - "//&
                          &"revise input -> output formats in CONTROL !!!",999)

              end if

            else 

                call cry(uout,'(/,1x,a,/)', &
                     "ERROR: unsupported trajectory conversion - "//&
                    &"revise input -> output formats in CONTROL !!!",999)

            end if

            return

        else

            if( job % archive_format < DLP ) then
                write(uout,"(/,1x,'ARCHIVE* trajectory data in DL_MONTE style (all)')")

            else if( job % archive_format == DLP ) then
                write(uout,"(/,1x,'ARCHIVE* trajectory data in DL_POLY style (all)')")

            else if( job % archive_format == DLP2 ) then
                write(uout,"(/,1x,'HISTORY* in DL_POLY-2 (all/subset)')")

            else if( job % archive_format == DLP2DCD ) then
                write(uout,"(/,1x,'HISTORY* & TRAJECTORY* in DL_POLY-2 (all/subset) & DCD (all) formats')")

            else if( job % archive_format == DLP4 ) then
                write(uout,"(/,1x,'HISTORY* in DL_POLY-4 (all/subset)')")

            else if( job % archive_format == DLP4DCD ) then
                write(uout,"(/,1x,'HISTORY* & TRAJECTORY* in DL_POLY-4 (all/subset) & DCD (all) formats')")

            else if( job % archive_format == DCD ) then
                write(uout,"(/,1x,'Only TRAJECTORY* data in DCD format (all)')")

            end if

            if( len_trim(adjustL(job % hist_molids)) > 0 .and. job % archive_format > DLP &
                .and. job % archive_format < DCD ) then

                if( job % archive_format /= DLP2DCD .and. job % archive_format < DLP4DCD ) then

                    call cry(uout,'(/,1x,a,/)', &
                         "ERROR: Archiving coordinates for a subset of molecules requires DCD for the whole system - "//&
                         &"revise archive_format in CONTROL !!!",999)

                else

                    write(uout,"(/,1x,'the subset of molecules (coordinates) to be stored - check :')")
                    write(uout,"(/,1x,a)")trim(adjustL(job % hist_molids))

                end if

            end if

        end if

        ncfg = size(job % coultype)

        do i = 1, ncfg

            if (job % coultype(i) == 0) then 

                write(uout,"(/,1x,'electrostatics is not used (no Coulomb interactions)')")

                !if (job % distribgvec) error(???)

            else 

                !if( job%is_slit .and. (job%coultype(i) > 0 .or. job%coultype(i) == -1) ) call error(21)
                if( job%is_slit ) then
                
                    if( job%coultype(i) == 1 .and. mfa_type==0 .and. is_slit_frac_z ) then
                    
                    else if( job%coultype(i) > 0 ) then
                    
                        call error(21)

                    end if

                end if

                write(uout,"(/,1x,'Coulomb treatment for configuration',i3,'           = ',i10)") i,job % coultype(i)

                write(uout,"(/,1x,'dielectric constant (medium permittivity)        = ',e15.7)") job % dielec

                if (job % coultype(i) == 2) write(uout,"(/,1x,'calculation of ewald sum in real space only')")

                if (job % ewald_alpha_given.and.job % coultype(i) == 1) then

                    write(uout,"(/,1x,'alpha parameter for ewald                        = ',e15.7)") job % ewald_alpha    
                    write(uout,"(/,1x,'kmax1                                            = ',i10)") job % kmax1
                    write(uout,"(/,1x,'kmax2                                            = ',i10)") job % kmax2
                    write(uout,"(/,1x,'kmax3                                            = ',i10)") job % kmax3

                else

                    write(uout,"(/,1x,'ewald precision                                  = ',e15.7)") job % madelungacc    

                endif

                if (job % coultype(i) == 3) &
                    write(uout,"(/,1x,'damped+shifted coulomb sum : alpha               = ',e15.7)") job % ewald_alpha

            end if

            if (job % distribgvec) then

                write(uout,"(/,1x,'g-vectors to be distributed over nodes')")

            else

                write(uout,"(/,1x,'all g-vectors stored on each node')")

            endif

        enddo

    endif

    if (job % moveatm .and. master) then

        write(uout,"(/,1x,'atom types to be moved')")

        do i = 1, job % numatmmove
            tag = set_species_type(job % atmmover_type(i))
            if(master) write(uout,'(5x,a8,1x,a4,a,3i3)')job % atmmover(i), tag, " XYZ = ", int(job % atmmove(:,i))
        enddo

        write(uout,"(/,1x,'initial maximum for atom displacement            = ',e15.7)") job % iondisp    
        write(uout,"(/,1x,'displacement distance update  (cycles)           = ',i10)") job % acc_atmmve_update    
        write(uout,"(/,1x,'acceptance ratio for atom displacement           = ',e15.7)") job % acc_atmmve_ratio    

        if (job % atmmovefreq == 0) then

            call error(17)

        else

            write(uout,"(/,1x,'atoms moved with stride (frequency)              = ',i10)") job % atmmovefreq

        endif

        if (job % useseqmovernd) then

            write(uout,"(/,1x,'atoms will be moved randomly in a sweep ')")

        elseif (job % useseqmove) then
        
            write(uout,"(/,1x,'atoms will be moved sequentially ')")

        endif

    endif

    if (job % movemol .and. master) then

        write(uout,"(/,1x,'molecule types to be moved ')")

        do i = 1, job % nummolmove
            write(uout,'(5x,a8,a,3i3)') job % molmover(i), " XYZ = ", int(job % molmove(:,i))
        enddo

        write(uout,"(/,1x,'initial maximum for mol displacement             = ',e15.7)") job % moldisp    
        write(uout,"(/,1x,'displacement distance update  (cycles)           = ',i10)") job % acc_molmve_update    
        write(uout,"(/,1x,'acceptance ratio for mol  displacement           = ',e15.7)") job % acc_molmve_ratio    

        if (job % molmovefreq == 0) then

            call error(18)

        else

            write(uout,"(/,1x,'molecules moved with stride (frequency)          = ',i10)") job % molmovefreq

        endif

        if (job % useseqmolmovernd) then

            write(uout,"(/,1x,'molecules will be moved randomly in a sweep ')")

        elseif (job % useseqmolmove) then
        
            write(uout,"(/,1x,'molecules will be moved sequentially ')")

        endif

    endif

    if (job % rotatemol .and. master) then

        write(uout,"(/,1x,'molecule types to be rotated ')")

        do i = 1, job % nummolrot
            write(uout,'(5x,a8)') job % molroter(i)
        enddo

        write(uout,"(/,1x,'initial maximum for mol rotation (degrees)       = ',e15.7)") job % molrot    
        !TU: Note: this change of units is applied to non-master nodes below
        job % molrot = job % molrot * TORADIANS
        write(uout,"(/,1x,'initial maximum for mol rotation (radians)       = ',e15.7)") job % molrot    
        write(uout,"(/,1x,'mol rotation angle update  (cycles)              = ',i10)") job % acc_molrot_update    
        write(uout,"(/,1x,'acceptance ratio for mol rotation                = ',e15.7)") job % acc_molrot_ratio    

        if (job % molrotfreq == 0) then
            
            call error(19)

        else

            write(uout,"(/,1x,'molecules rotated with stride (frequency)        = ',i10)") job % molrotfreq

        endif

        if (job % useseqmolrotrnd) then

            write(uout,"(/,1x,'molecules will be rotated randomly in a sweep ')")

        elseif (job % useseqmolrot) then
        
            write(uout,"(/,1x,'molecules will be rotated sequentially ')")

        endif

    endif

    if (job % rotateatm .and. master) then

        write(uout,"(/,1x,'atom types to be rotated ')")

        do i = 1, job % numatmrot
            tag = set_species_type(job % atmroter_type(i))
            write(uout,'(5x,a8,1x,a4)') job % atmroter(i), tag
        enddo

        write(uout,"(/,1x,'initial maximum for atom rotation (degrees)      = ',e15.7)") job % atmrot
        !TU: Note: this change of units is applied to non-master nodes below
        job % atmrot = job % atmrot * TORADIANS
        write(uout,"(/,1x,'initial maximum for atom rotation (radians)      = ',e15.7)") job % atmrot
        write(uout,"(/,1x,'atom rotation angle update (cycles)              = ',i10)") job % acc_atmrot_update    
        write(uout,"(/,1x,'acceptance ratio for atom rotation               = ',e15.7)") job % acc_atmrot_ratio    

        if (job % atmrotfreq == 0) then
            
            call error(19)

        else

            write(uout,"(/,1x,'atoms rotated with stride (frequency)            = ',i10)") job % atmrotfreq

        endif


        write(uout,"(/,1x,'atoms will be rotated randomly in a sweep ')")

    endif
    
    ! this makes sure max rotation is the same on all nodes
    if (.not.master) job % molrot = job % molrot * TORADIANS
    if (.not.master) job % atmrot = job % atmrot * TORADIANS

    is_GCMC = ( job%gcmcatom .or. job%gcmcmol .or. job%gibbsatomtran .or. job%gibbsmoltran & 
                             .or. job%gibbs_indvol .or. job%gibbsatomexch .or. job%gibbsmolexch )

    job % is_gcmc = is_GCMC

    if (master) write(uout,"(/,1x,'tolerance for rejection (energy units)           = ',e15.7)") job % toler

    job % toler = job % toler * job % energyunit 

    if(job % lzdensity) then

        if( job % is_slit ) job % lzcomzero = .false.

        if (master) then

            if( job % lzcomzero ) then
              if( len_trim(adjustL(job%zcom_molids)) > 0 ) then
                write(uout,"(/,1x,'z-density will be calculated upon removal of COM(z) for ',a)")&
                      trim(adjustL(job%zcom_molids))
              else
                write(uout,"(/,1x,'z-density will be calculated upon removal of COM(z)')")
              endif
            else
                write(uout,"(/,1x,'z-density will be calculated')")
            endif

            write(uout,"(/,1x,'the no of z-density bins                         = ',i10)") job % nzden     

            write(uout,"(/,1x,'the stride (frequency) of z-density calculations = ',i10)") job % zden_freq

        endif

    endif

    if(job % lrdfs) then

        if (master) write(uout,"(/,1x,'rdfs will be calculated')")
        if (master) &
            & write(uout,"(/,1x,'the no of rdf bins                               = ',i10)") job % nrdfs
        if (master) &
            & write(uout,"(/,1x,'the maximum distance for rdf calculation         = ',e15.7)") job % rdf_max
        if (master) &
            & write(uout,"(/,1x,'the stride (frequency) of rdf calculations       = ',i10)") job % rdfs_freq

    endif

    if (master .and. job % repexch) then

        !TU**: Is the below statement correct for non-master nodes? I suspect not. See below! Need everything to be the same within a workgroup
        !TU**: Surely non-master nodes need the same repexch_freq as the master nodes? Or do master nodes only 'need to know' the frequency somehow?
        !TU**: It couldn't hurt for all nodes to have the correct repexch frequency??

        !if( abs(job % repexch_inc) < 1.0e-08 .or. job % fedcalc ) job % repexch_freq = job % maxiterations+1
        if( abs(job % repexch_inc) < 1.0e-08 ) job % repexch_freq = job % maxiterations+1

        if( job % repexch_freq < 1 ) job % repexch_freq = 1000

        write(uout,"(/,1x,'replica exchange monte carlo will be employed')")

        write(uout,"(/,1x,'the number of replicas                           = ',i6)") job % numreplicas

        write(uout,"(/,1x,'the temperature increment (K)                    = ',e15.7)") job % repexch_inc

        write(uout,"(/,1x,'stride (frequency) of attempted exchange         = ',i6)") job % repexch_freq

    endif

    if (master .and. job % lsample_energy) then

        write(uout,"(/,1x,'stride (frequency) of energy sampling            = ',i6)") job % sample_energy_freq

    endif

    if (job%rseeds .and. job%lseeds) call error(169)

    if (job%rseeds) then

        !sjc: start 
        if (master) then

            call system_clock(time)

            if(time > 10000) then

                time = mod(time, 10000) + 178   !TU: 'time' ranges from 178 to 10177 (inclusive)

            endif

            !TU: From the paper 'Toward A Universal Random Number Generator', G. Marsaglia & A. Zaman,
            !TU: which describes the random number generator: "i, j and k must be in the range 1 to 178, and 
            !TU: not all 1, while l may be any integer from 0 168". The 4 elements in 'seed' correspond to 
            !TU: i, j, k, l (in that order)

            !TU: Old way of doing it (could lead to invalid seeds)
            !seed(1) = mod(time, 178) + 1                        !TU: i ranges from 1-178 (inclusive)
            !seed(2) = mod(time, 169)                            !TU: j ranges from 0-168 (inclusive)
            !seed(3) = mod(   (seed(1)+seed(2))*time, 178) + 1   !TU: k ranges from 1-178 (inclusive)
            !seed(4) = mod(abs(seed(1)-seed(2))*time, 178) + 1   !TU: l ranges from 1-178 (inclusive)

            !TU: A way which guarantees valid seeds...
            seed(1) = mod(time, 178) + 1                        !TU: i ranges from 1-178 (inclusive)
            seed(2) = mod(abs(seed(1)-seed(2))*time, 178) + 1   !TU: j ranges from 1-178 (inclusive)
            seed(3) = mod(   (seed(1)+seed(2))*time, 178) + 1   !TU: k ranges from 1-178 (inclusive)
            seed(4) = mod(time, 169)                            !TU: l ranges from 0-168 (inclusive)
            if( seed(1)==1 .and. seed(2)==1 .and. seed(3)==1 ) seed(1) = 178

        endif

        !TU**: Shares seeds with all nodes?
        call msg_bcast(seed,4)

        job % seeds_ijkl(:) = seed(:)

        if (master) &
            & write(uout,"(/,1x,'random number generator seeded from clock')")

    endif

    if (job%lseeds) then

        ! initial values of i,j,k must be in range 1 to 178 (not all 1)
        ! initial value of l must be in range 0 to 168

        if (any(job % seeds_ijkl(1:3) > 179) .or. any(job % seeds_ijkl(1:4) < 1)) call error(168)
        if (job % seeds_ijkl(4) > 168) call error(168)

        !TU: Catch if i, j and k are all 1
        if( job % seeds_ijkl(1) == 1 .and. &
            job % seeds_ijkl(2) == 1 .and. &
            job % seeds_ijkl(3) == 1  ) call error(168)

        call msg_bcast(job%seeds_ijkl,4)

        if (master) &
            & write(uout,"(/,1x,'random number generator seeded from user input')")

    endif

    !TU: Set different seeds for different workgroups in replica exchange - all threads
    !TU: within the same workgroup will have the same seed
    if(job%repexch) then

       job%seeds_ijkl(4) = mod( job%seeds_ijkl(4) + idgrp, 169)

    end if

    if (master) then

         write(uout,"(/,1x,'random number seeds (i,j,k,l) = ',4i6,a,i4,a)") &
               job % seeds_ijkl,' (for workgroup ',idgrp,')'

    endif

    if (master) then

         write(uout,"(/,1x,'total job time and close time = ',2f15.2)") job%jobtime, job%closetime

         if (job%jobtime < 1e-3_wp) call error(170) !job % jobtime = 1e6_wp !call error(170)

         if (job%closetime < 1e-3_wp) call error(171) !job % closetime = 200.0_wp !call error(171)

    endif


    if (job%usereset) then

        if( job%useorthogonal ) then

            job%usereset = .false.

            if(master) write(uout,"(/,1x,a)")&
                 "atoms will be kept in the primary cell due to 'use orthogonal' directive"

        else

            if(master) write(uout,"(/,1x,'the periodic box will be reset every ',i10,1x,'steps')") job%reset_freq

        endif

    endif

    if (master .and. job%usequaternion) then

        write(uout,"(/,1x,'rotation of molecules will use quaternions')") 

    elseif (master) then

        write(uout,"(/,1x,'rotation of molecules will use Euler method')") 

    endif

    if (master .and. job%lmoldata) then

        write(uout,"(/,1x,'molecular contributions by type will be calculated ')") 

    endif

    if (master .and. job%lsample_displacement) then

        write(uout,"(/,1x,'average displacements of atoms will be sampled ')") 
        write(uout,"(/,1x,'the no of steps after equil to record average position = ',i5)") job%sample_disp_avg

    endif

    !TU: Output for orientations
    
    if (master .and. job%useorientation) then
        
        if (job%sample_orientation) then
    
            write(uout,"(/,1x,'orientations and centres will be periodically output to ORIENT* ')")
            write(uout,"(/,1x,'output to ORIENT* stride (frequency)               = ',i5)") &
                job%sample_orientation_freq

        end if

    end if

    if( is_GCMC .and. master) then
        
        !write(uout,"(/,1x,'Grand-Canonical (GCMC, muVT) scheme is invoked ')")

        if( job%usegaspres ) then

            write(uout,"(/,1x,'Grand-Canonical (GCMC, muVT) scheme using partial pressure (katm)')")

        else if( job%usechempotkt ) then

            write(uout,"(/,1x,'Grand-Canonical (GCMC, muVT) scheme using chemical potential (kT)')")

        else if( job%usechempotkj ) then

            write(uout,"(/,1x,'Grand-Canonical (GCMC, muVT) scheme using chemical potential (kJ/mol)')")

        else 

            write(uout,"(/,1x,'Grand-Canonical (GCMC, muVT) scheme using chemical activity (internal units)')")

        end if

        if( job % usegcexcludeslab ) then

            write(uout,"(/,1x,'excluding a slab region in the system from GCMC insertions')")

            write(uout,"(/,1x,'lattice vector GCMC exclusion slab is perpendicular to = ',i5)") job%gcexcludeslab_dim

            write(uout,"(/,1x,'lower bound for GCMC exclusion slab (fractional coord) = ',e15.7)") job%gcexcludeslab_lbound

            write(uout,"(/,1x,'upper bound for GCMC exclusion slab (fractional coord) = ',e15.7)") job%gcexcludeslab_ubound

            !TU: Forbid the GCMC slab exclusion with cavity bias - they use different branches
            !TU: in the code. Also forbid use with slit

            if( job % usecavitybias ) then

                call cry(uout,'', &
                    "ERROR: GCMC exclusion slab cannot be used with cavity bias",999)

            end if
            
            if( job % is_slit ) then

                call cry(uout,'', &
                    "ERROR: GCMC exclusion slab cannot be used with slit",999)

            end if

        end if

        if( job % usecavitybias ) then

            if( job % cavity_x < 0.0_wp .or. &
                job % cavity_y < 0.0_wp .or. &
                job % cavity_z < 0.0_wp ) call error(134)

            if( job % cavity_radius < 0.0001 ) call error(135)

            if( job % cavity_mesh ) then
                write(uout,"(/,1x,'cavity bias mesh spacing                         = ',3e15.7)") &
                      job % cavity_x, job % cavity_y, job % cavity_z
            else 
                write(uout,"(/,1x,'cavity bias grid spacing                         = ',3e15.7)") &
                      job % cavity_x, job % cavity_y, job % cavity_z
            endif

            write(uout,"(/,1x,'cavity bias core radius                          = ',e15.7)") &
                  job % cavity_radius

            write(uout,"(/,1x,'cavity bias update stride (frequency)            = ',i8)") &
                  job % cavity_update

        end if

    end if

    !TU: Output for cell lists

    if (master .and. job%clist) then

        write(uout,"(/,1x,'cell lists will be used')") 

        if( job%clist_printdetails )  write(uout,"(/,1x,'detailed information about cell lists will be output to OUTPUT files')") 

    end if


    if (master .and. job%usexymoves) then

        write(uout,"(/,1x,'translation and rotation moves will be limited to x/y plane')") 

    end if


    if( master ) flush(uout)

end subroutine printandcheck

!> release memory allocated for control 'job'
subroutine dealloc_control(job)

    use kinds_f90
    use control_type

        !> structure containing global simulation control switches & parameters
    type(control), intent(inout) :: job

    if(allocated(job%coultype)) deallocate(job%coultype)
    if(allocated(job%gcmcmolinsert)) deallocate(job%gcmcmolinsert)
    if(allocated(job%gcmc_atompot)) deallocate(job%gcmc_atompot)
    if(allocated(job%gcmc_molpot)) deallocate(job%gcmc_molpot)
    if(allocated(job%molmove)) deallocate(job%molmove)
    if(allocated(job%atmmove)) deallocate(job%atmmove)
    if(allocated(job%molmover)) deallocate(job%molmover)
    if(allocated(job%molroter)) deallocate(job%molroter)
    if(allocated(job%atmmover)) deallocate(job%atmmover)
    if(allocated(job%atmmover_type)) deallocate(job%atmmover_type)
    if(allocated(job%trans1)) deallocate(job%trans1)
    if(allocated(job%trans1_type)) deallocate(job%trans1_type)
    if(allocated(job%trans2)) deallocate(job%trans2)
    if(allocated(job%trans2_type)) deallocate(job%trans2_type)
    if(allocated(job%gcmcatminsert)) deallocate(job%gcmcatminsert)
    if(allocated(job%gcmcatminsert_type)) deallocate(job%gcmcatminsert_type)
    if(allocated(job%gibbsatmtrans)) deallocate(job%gibbsatmtrans)
    if(allocated(job%gibbsatmtrans_type)) deallocate(job%gibbsatmtrans_type)
    if(allocated(job%gibbsatmexchange1)) deallocate(job%gibbsatmexchange1)
    if(allocated(job%gibbsatmexchange2)) deallocate(job%gibbsatmexchange2)
    if(allocated(job%gibbsatmexchange_type1)) deallocate(job%gibbsatmexchange_type1)
    if(allocated(job%gibbsatmexchange_type2)) deallocate(job%gibbsatmexchange_type2)
    if(allocated(job%semiwidomele1)) deallocate(job%semiwidomele1)
    if(allocated(job%semiwidom1_type)) deallocate(job%semiwidom1_type)
    if(allocated(job%semiwidomele2)) deallocate(job%semiwidomele2)
    if(allocated(job%semiwidom2_type)) deallocate(job%semiwidom2_type)
    if(allocated(job%semigrandele1)) deallocate(job%semigrandele1)
    if(allocated(job%semigrand1_type)) deallocate(job%semigrand1_type)
    if(allocated(job%semigrandele2)) deallocate(job%semigrandele2)
    if(allocated(job%semigrand2_type)) deallocate(job%semigrand2_type)
    if(allocated(job%swapele1)) deallocate(job%swapele1)
    if(allocated(job%swapele1_type)) deallocate(job%swapele1_type)
    if(allocated(job%swapele2)) deallocate(job%swapele2)
    if(allocated(job%swapele2_type)) deallocate(job%swapele2_type)
    if(allocated(job%gcinsert_mols)) deallocate(job%gcinsert_mols)
    if(allocated(job%semigrand1_mols)) deallocate(job%semigrand1_mols)
    if(allocated(job%semigrand2_mols)) deallocate(job%semigrand2_mols)
    if(allocated(job%gibbsmolinsert)) deallocate(job%gibbsmolinsert)
    if(allocated(job%gibbsmolexchange1)) deallocate(job%gibbsmolexchange1)
    if(allocated(job%gibbsmolexchange2)) deallocate(job%gibbsmolexchange2)
    !if(allocated(job%)) deallocate(job%)

end subroutine dealloc_control

end module
