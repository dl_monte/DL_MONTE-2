!***************************************************************************
!   Copyright (C) 2021 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> @brief
!> - Module containing high-level procedures and variables pertaining to cell lists.
!> - Wrapper for low-level procedures in `cell_list_module`; procedures here act on
!>   DL_MONTE-specific data structures and variables, e.g. atom, config and molecule 
!>   objects, and the control_type object containing simulation control variables.
!> - Errors and warnings are output to unit `uout` in `constants_module`
!> @usage 
!> - @stdusage 
!> - @stdspecs
!> @module for cell lists
module cell_list_wrapper_module

    use kinds_f90
    use cell_list_module
    use constants_module, only : uout
    use cell_module, only : nconfigs, cfgs

    implicit none


contains


    !> @brief
    !> - Initialises cell lists for all boxes, given the variables already in the control_type object
    !> - Sets up the cells according to the current box dimensions, for all boxes
    !> - Assigns atoms to cells according to their current positions
    !> - Prints information to `uout`
    subroutine cell_list_initialisation(job)

        use control_type
        use comms_mpi_module, only : master

            !> Simulation control variables
        type (control), intent(inout) :: job

        integer :: ib

        if( master ) then

            write(uout,*) 
            write(uout,*) "=================================================="
            write(uout,*) "        Initial cell list specifications"
            write(uout,*) "=================================================="
            write(uout,*) 

        end if

        ! Safety checks...

        if( job%useseqmove ) call cry(uout, '', &
                    "ERROR: Sequential moves are not supported with cell lists", 999)

        if( job%semigrandmols .or. job%swapmols ) call cry(uout, '', &
                    "ERROR: Semi-grand and swap moves with molecules are not supported with cell lists", 999)
  
        ! The configuration must be orthogonal (i.e. a diagonal matrix containing the cell vectors).
        ! An easy way to guarantee this is to insist that the 'use ortho' keyword is used, since when this
        ! is in effect safety checks are performed at config input and upon parsing of CONTROL to make sure
        ! the cell matrix and volume move choices are consisted with 'use ortho'
        if( .not. job%useorthogonal ) call cry(uout, '', &
                    "ERROR: Cell lists must be used with orthorhombic box and 'use ortho' functionality", 999)


        ! Set-up in earnest...

        if( job%shortrangecut /= job%vdw_cut .and. master ) then
         
            write(uout,*) "NOTE: Detected different global and VDW cut-offs: using largest of two to inform cell lists" 
            write(uout,*)

        end if

        job%clist_rmin = max(job%shortrangecut, job%vdw_cut)

        ! If neighbour lists ARE NOT in use then we use the potential cut-off as the minimum cell size.
        ! If neighbour lists ARE in use then the only purpose of the cell lists is to set up the neighbour
        ! lists, and so we use the potential cut-off plus the Verlet skin size as the minimum cell size.
        if( job%uselist ) then

            job%clist_rmin = job%clist_rmin + job%verletshell

            if( master ) then

                write(uout,*) "Cell lists will be used only to set neighbour lists. Min. cell size is "// &
                              "potential cut-off + Verlet shell size." 
                write(uout,*)

            end if

        else

            if( master ) then

                write(uout,*) "Cell lists will be used in searches for interating atom pairs. Min. cell "// &
                              "size is potential cut-off." 
                write(uout,*)

            end if


        end if

        do ib = 1, nconfigs

            if( master ) then

                write(uout,*) "--------------------------------------------------"
                write(uout,*) "Cell list specifications for box ", ib
                write(uout,*) 

            end if

            call set_up_cell_list(job, ib)
            
            if( master ) call print_cell_list_info(cfgs(ib)%celllist, uout, job%clist_printdetails)

        end do

        if( master ) write(uout,*) "=================================================="

    end subroutine cell_list_initialisation




    !> For the specified box, initialises the cell list, and sets the cells for each atom, according to
    !> the current atomic positions and box dimensions for the box, from scratch.
    !> It is assumed here that the arrays in the cell_list object are not allocated.
    subroutine set_up_cell_list(job, ib)

         use control_type

             !> Simulation control variables
         type (control), intent(in) :: job

             !> Configuration/box index
         integer, intent(in) :: ib

         real(wp) :: Lx, Ly, Lz

         Lx = cfgs(ib)%vec%latvector(1,1)
         Ly = cfgs(ib)%vec%latvector(2,2)
         Lz = cfgs(ib)%vec%latvector(3,3)

         ! Initialise the cell_list object - does not assign atoms to cells
         call initialise_cell_list(cfgs(ib)%celllist, Lx, Ly, Lz, job%clist_rmin, job%clist_maxpercell)

         ! Determines and sets the 'cell' variable for all atoms and adds the atoms to the cell list
         call config_set_cells(ib, .true.)

    end subroutine set_up_cell_list




    !> Clears the cell list for the specified box, i.e. deallocate the cell
    !> list and set the cells on all atom type objects in the configuration to 0.
    subroutine tear_down_cell_list(ib)

            !> Configuration/box index
        integer, intent(in) :: ib

        integer :: i, j

        call deallocate_cell_list(cfgs(ib)%celllist)

        do i = 1, cfgs(ib)%num_mols

            do j = 1, cfgs(ib)%mols(i)%natom

                cfgs(ib)%mols(i)%atms(j)%cell = 0

            end do

        end do

    end subroutine tear_down_cell_list




    !> Sets the cell for an atom using its current position, and updates the cell list
    !> object for the box so that the atom is in that cell
    subroutine atom_set_cell(ib, i, j, initial)

        use cell_module, only : pbc_cart_pos_calc

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> Atom index within molecule
        integer, intent(in) :: j

            !> Flag which is .true. if we are setting a cell for the first time, 
            !> in which case the atom is not removed from its 'old' cell
        logical, intent(in) :: initial

        integer :: cellold, cellnew
        real(wp) :: rpos(3)

        cellold = cfgs(ib)%mols(i)%atms(j)%cell
        rpos = cfgs(ib)%mols(i)%atms(j)%rpos

        ! The cell list functionality requires atom positions which are in the range
        ! 0 <= x < Lx for the purposes of determining the cell (and similarly
        ! for y and z). We need to wrap the x, y and z coordinates so that they are
        ! within this range.

        rpos(1) = rpos(1) / cfgs(ib)%celllist%Lx
        rpos(2) = rpos(2) / cfgs(ib)%celllist%Ly
        rpos(3) = rpos(3) / cfgs(ib)%celllist%Lz

        rpos(:) = rpos(:) - floor(rpos(:))

        rpos(1) = rpos(1) * cfgs(ib)%celllist%Lx
        rpos(2) = rpos(2) * cfgs(ib)%celllist%Ly
        rpos(3) = rpos(3) * cfgs(ib)%celllist%Lz

        cellnew = cell_from_position(cfgs(ib)%celllist, rpos(1), rpos(2), rpos(3))

        ! Remove atom from its old cell (except at initialisation)
        if( .not.initial ) call remove_atom_from_cell(cfgs(ib)%celllist, cellold, i, j)

        call add_atom_to_cell(cfgs(ib)%celllist, cellnew, i, j)
        cfgs(ib)%mols(i)%atms(j)%cell = cellnew

    end subroutine atom_set_cell




    !> Remove a specified atom from the cell list, and set the cell on the atom type
    !> object to 0.
    subroutine atom_remove_from_cell_list(ib, i, j, relabel)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> Atom index within molecule
        integer, intent(in) :: j

            !> If .true. it is assumed that when the atom is deleted, the last atom in the atom's
            !> molecule is relabeled as `j`, and hence the atom indices in the cell list object
            !> are relabelled accordingly. For this to work *this procedure must be called before 
            !> atom object for `j` is actually deleted from the configuration*, otherwise the
            !> relabelling will be wrong
        logical :: relabel


        integer :: cellold

        cellold = cfgs(ib)%mols(i)%atms(j)%cell
        call remove_atom_from_cell(cfgs(ib)%celllist, cellold, i, j)
        cfgs(ib)%mols(i)%atms(j)%cell = 0

        if(relabel) then

            if(j /= cfgs(ib)%mols(i)%natom) call relabel_atom(cfgs(ib)%celllist, i, cfgs(ib)%mols(i)%natom, i, j)

        end if

    end subroutine atom_remove_from_cell_list




    !> Stores the cell for the specified atom in the `store_cell` variable for the atom, with
    !> no change to the cell list for the box
    subroutine atom_store_cell(ib, i, j)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> Atom index within molecule
        integer, intent(in) :: j

        cfgs(ib)%mols(i)%atms(j)%store_cell = cfgs(ib)%mols(i)%atms(j)%cell

    end subroutine atom_store_cell




    !> Sets the cell for the specified atom to its `store_cell` variable, and updates the
    !> cell list object for the box so that the atom is in that cell (i.e. the atom is
    !> moved from its 'current' cell into the new cell `store_cell`)
    subroutine atom_restore_cell(ib, i, j)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> Atom index within molecule
        integer, intent(in) :: j

        call remove_atom_from_cell(cfgs(ib)%celllist, cfgs(ib)%mols(i)%atms(j)%cell, i, j)
        cfgs(ib)%mols(i)%atms(j)%cell = cfgs(ib)%mols(i)%atms(j)%store_cell
        call add_atom_to_cell(cfgs(ib)%celllist, cfgs(ib)%mols(i)%atms(j)%cell, i, j)

    end subroutine atom_restore_cell




    !> Sets the cell for all atoms in a molecule given the atoms' current positions, and
    !> updates the cell list object for the box so that the atoms are in those cells
    subroutine molecule_set_cells(ib, i, initial)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> Flag which is .true. if we are setting cells for the first time, 
            !> in which case the atoms are not removed from their 'old' cell. 
        logical, intent(in) :: initial

        integer :: j

        do j = 1, cfgs(ib)%mols(i)%natom

            call atom_set_cell(ib, i, j, initial)

        end do

    end subroutine molecule_set_cells




    !> Remove atoms in the specified molecule from the cell list, and set the cells on the
    !> atom type objects to 0. 
    subroutine molecule_remove_from_cell_list(ib, i, relabel)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

            !> If .true. it is assumed that when the molecule `i` is deleted, the last molecule in the
            !> configuration is relabeled as `i`, and hence the molecule indices in the cell list object
            !> are relabelled accordingly. For this to work *this procedure must be called before 
            !> molecule object for `i` is actually deleted from the configuration*, otherwise the
            !> relabelling will be wrong
        logical :: relabel

        integer :: j

        do j = 1, cfgs(ib)%mols(i)%natom

            call atom_remove_from_cell_list(ib, i, j, .false.)

        end do

        if(relabel) then

            if(i /= cfgs(ib)%num_mols) then

                call relabel_molecule(cfgs(ib)%celllist, cfgs(ib)%num_mols, i)

            end if

        end if

    end subroutine molecule_remove_from_cell_list




    !> Stores the cell for all atoms in the specified molecule using 
    !> the `store_cell` variable for the atoms, with no change to the cell list for the
    !> box
    subroutine molecule_store_cells(ib, i)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

        integer :: j

        do j = 1, cfgs(ib)%mols(i)%natom

            call atom_store_cell(ib, i, j)

        end do       

    end subroutine molecule_store_cells




    !> Sets the cells for all atoms in the specified molecule to those
    !> in the `store_cell` variables for the atoms, and updates the cell list object for
    !> the box so that the atoms are in those cells (i.e. the atoms are moved from their
    !> 'current' cells into the new cells in the `store_cell` variables)
    subroutine molecule_restore_cells(ib, i)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Molecule index within configuration
        integer, intent(in) :: i

        integer :: j

        do j = 1, cfgs(ib)%mols(i)%natom

            call atom_restore_cell(ib, i, j)

        end do       

    end subroutine molecule_restore_cells




    !> Sets the cell for atoms in the specified configuration given the atoms' current positions,
    !> and updates the cell list object for the box so that the atom is in that cell
    subroutine config_set_cells(ib, initial)

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Flag which is .true. if we are setting cells for the first time, 
            !> in which case the atoms are not removed from their 'old' cell
        logical, intent(in) :: initial

        integer :: i

        do i = 1, cfgs(ib)%num_mols

            call molecule_set_cells(ib, i, initial)

        end do

    end subroutine config_set_cells




    !> Stores the cell for all atoms in the specified configuration using 
    !> the `store_cell` variable for the atoms, with no change to the cell list for the box
    subroutine config_store_cells(ib)

            !> Configuration/box index
        integer, intent(in) :: ib

        integer :: i

        do i = 1, cfgs(ib)%num_mols

            call molecule_store_cells(ib, i)

        end do

    end subroutine config_store_cells




    !> Sets the cells for all atoms in the specified configuration to the
    !> the `store_cell` variables for the atoms, and updates the cell list object for the
    !> box so that the atoms are in those cells (i.e. the atoms are moved from their
    !> 'current' cells into the new cells in the `store_cell` variables)
    subroutine config_restore_cells(ib)

            !> Configuration/box index
        integer, intent(in) :: ib

        integer :: i

        do i = 1, cfgs(ib)%num_mols

            call molecule_restore_cells(ib, i)

        end do

    end subroutine config_restore_cells




    !> Checks that the cell lists for the given configuration is correct.
    !> This is achieved by storing the current cell for all atoms in the configuration,
    !> recalculating the cells for the atoms, and comparing the recalculated cells to
    !> the stored ones.
    subroutine check_cell_list(ib, iter)

        use comms_mpi_module, only : master

            !> Configuration/box index
        integer, intent(in) :: ib

            !> Iteration number (used in output messages)
        integer, intent(in) :: iter

        integer :: i, j, cell, count, natoms
        logical :: found

        character :: chnum1*10, chnum2*10, chnum3*10, chnum4*10

        if( master ) then

            write(uout,*)
            write(uout,'(1x,a,i4,a,i10,a)') "Checking cell lists for box ",ib, " at iteration ",iter,"..."
            write(uout,*)

        end if

        ! Check that the current cells of all atoms reflect their current positions...

        call config_store_cells(ib)
        call config_set_cells(ib, .false.)

        do i = 1, cfgs(ib)%num_mols

            do j = 1, cfgs(ib)%mols(i)%natom

                if( cfgs(ib)%mols(i)%atms(j)%cell /= cfgs(ib)%mols(i)%atms(j)%store_cell ) then

                    write(chnum1,'(i10)') i
                    write(chnum2,'(i10)') j
                    write(chnum3,'(i10)') cfgs(ib)%mols(i)%atms(j)%store_cell
                    write(chnum4,'(i10)') cfgs(ib)%mols(i)%atms(j)%cell

                    call cry(uout,'',"ERROR: Cell for (mol, atom) = ("//trim(adjustl(chnum1))//", "//trim(adjustl(chnum2))// &
                                     ") does not reflect its position! Current & correct cell for atom = "// &
                                     trim(adjustl(chnum3))//" & "//trim(adjustl(chnum4)),999)

                end if

            end do

        end do

        call config_restore_cells(ib)


        ! Check that the cell list object is correct...


        if(     .not. cfgs(ib)%celllist%Lx == cfgs(ib)%vec%latvector(1,1) &
           .or. .not. cfgs(ib)%celllist%Ly == cfgs(ib)%vec%latvector(2,2) &
           .or. .not. cfgs(ib)%celllist%Lz == cfgs(ib)%vec%latvector(3,3) ) then

            call cry(uout,'',"ERROR: Box dimensions in cell lists do not reflect configuration!",999)

        end if

        ! Check in the cell list object that the number of atoms is correct, and that the elements in the cell list make sense

        count = 0

        do cell = 1, cfgs(ib)%celllist%Ncells

            natoms = cfgs(ib)%celllist%natoms(cell)

            count = count + natoms

            ! list(:,n,cell) for n from 1 to natoms should be non-zero, and the other elements
            ! should be 0
            if( any( cfgs(ib)%celllist%list(:,1:natoms,cell) == 0 ) ) then

                call cry(uout,'',"ERROR: Cell list has become corrupted: an element which should be non-zero is not!",999)

            end if
            if( cfgs(ib)%celllist%maxpercell > natoms ) then

                if( any( cfgs(ib)%celllist%list(:,(natoms+1):,cell) /= 0 ) ) then

                    call cry(uout,'',"ERROR: Cell list has become corrupted: an element which should be zero is not!",999)

                end if

            end if



        end do

        if( count /= cfgs(ib)%number_of_atoms ) then

            call cry(uout,'',"ERROR: Number of atoms in cell list does not reflect configuration!",999)

        end if

        ! Check that each atom is located in the appropriate cell within the cell list object...

        do i = 1, cfgs(ib)%num_mols

            do j = 1, cfgs(ib)%mols(i)%natom

                found = is_atom_in_cell(cfgs(ib)%celllist, cfgs(ib)%mols(i)%atms(j)%cell, i, j)

                if( .not. found ) then

                    call cry(uout,'',"ERROR: Cell list is missing an atom!",999)

                end if

            end do

        end do

    end subroutine check_cell_list




end module cell_list_wrapper_module
