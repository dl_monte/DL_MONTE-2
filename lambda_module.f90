!***************************************************************************
!   Copyright (C) 2022 by T L Underwood                                    *
!   t.l.underwood@bath.ac.uk                                               *
!                                                                          *
!***************************************************************************

!> Low-level module containing variables and procedures pertaining to the Hamiltonian 
!> coupling parameter 'lambda'. Note that there are different lambda parameters for
!> the VdW and Coulomb interactions ('lambda_vdw' and 'lambda_coul'), and that their 
!> values are determined by the value of the master lambda parameter (the variable
!> 'lambda'). Hence changes should only be made to the master lambda parameter via
!> the function 'set_lambda', which updates 'lambda_vdw' and 'lambda_coul' accordingly.
!> (Simply updating 'lambda' alone will lead to 'lambda_vdw' and 'lambda_coul' being
!> out of sync with the master 'lambda').
!> - 
!> @usage 
!> - @stdusage 
!> - @stdspecs
!>
!> @modulecontaining lambda Hamiltonian coupling parameter
module lambda_module

    use kinds_f90

    implicit none

        !> Flag signifying that the lambda functionality is active with regards to the force field, i.e. there is a
        !> lambda-dependent VdW potential in use or lambda-dependent Coulomb interactions
    logical :: using_lambda_ff = .false.

        !> Master Hamiltonian coupling parameter 'lambda' determining the strength of selected interactions within the
        !> system for continuous fractional component MC or thermodynamic integration. This should only be changed
        !> via the `set_lambda` below, since the coupling parameters for the VdW and Coulomb interactions are tied
        !> to this.
    real(kind = wp) :: lambda

        !> Hamiltonian coupling parameter 'lambda' for the VdW interactions. This variable is tethered to the master
        !> `lambda` and should not be altered manually.
    real(kind = wp) :: lambda_vdw

        !> Hamiltonian coupling parameter 'lambda' for the Coulomb interactions. This variable is tethered to the master
        !> `lambda` and should not be altered manually.
    real(kind = wp) :: lambda_coul


    ! Variables determining the functional forms of lambda_vdw and lambda_coul vs. lambda

        !> Determines the coupling between the VdW lambda and the master lambda: lambda_vdw increases linearly from
        !> 0 to 1 in the range of lambda from lambda_vdw_lbound to lambda_vdw_ubound, and is 0 for lambda < lambda_vdw_lbound,
        !> and is 1 for lambda > lambda_vdw_ubound
    real(kind = wp) :: lambda_vdw_lbound = 0.0_wp

        !> Determines the coupling between the VdW lambda and the master lambda: lambda_vdw increases linearly from
        !> 0 to 1 in the range of lambda from lambda_vdw_lbound to lambda_vdw_ubound, and is 0 for lambda < lambda_vdw_lbound,
        !> and is 1 for lambda > lambda_vdw_ubound
    real(kind = wp) :: lambda_vdw_ubound = 1.0_wp

        !> Determines the coupling between the Coulomb lambda and the master lambda: lambda_ccoul increases linearly from
        !> 0 to 1 in the range of lambda from lambda_coul_lbound to lambda_coul_ubound, and is 0 for lambda < lambda_coul_lbound,
        !> and is 1 for lambda > lambda_coul_ubound
    real(kind = wp) :: lambda_coul_lbound = 0.0_wp

        !> Determines the coupling between the Coulomb lambda and the master lambda: lambda_ccoul increases linearly from
        !> 0 to 1 in the range of lambda from lambda_coul_lbound to lambda_coul_ubound, and is 0 for lambda < lambda_coul_lbound,
        !> and is 1 for lambda > lambda_coul_ubound
    real(kind = wp) :: lambda_coul_ubound = 1.0_wp


    ! Other variables

        !> Flag which signifies that the 'lambdapath' block in FIELD has been read.
    logical :: lambdapath_was_read = .false.




contains


    !> @brief
    !> - Initialises all lambda-related variables based on the simulation control parameters
    subroutine initialise_lambda(job)

        use control_type

        implicit none

            !> Simulation control parameters
        type(control), intent(in) :: job

        ! Note that we must call 'set_lambda' to set lambda so that the VdW and Coulomb 
        ! coupling parameters are updated correctly!
        call set_lambda(job%initiallambda)

    end subroutine initialise_lambda



    !> Reads parameters corresponding to parametrisation of lambda_vdw and lambda_coul vs. lambda
    !> from the specified unit. In practice this reads a block of lines following 'lambdapath' in FIELD, with 
    !> the block terminating at a line beginning with 'finish' 
    subroutine read_lambdapath(unit)

        use parse_module
        use constants_module, only : uout

            !> Unit to read the 'lambdapath' block from
        integer :: unit

        character :: line*80, word*40
        logical :: more, safe, is_done

        logical :: opt_read(4) = .false.

        if( lambdapath_was_read ) call cry(uout, '', &
            "ERROR: 'lambdapath' block in FIELD has been specified twice.", 999)

        lambdapath_was_read = .true.

        using_lambda_ff = .true.

        more = .true.
        safe = .true.

        do while( more )

            call get_line(safe,unit,line)
            if (.not.safe) call error(999) !TU**: Safe to call error vs. cry?
            call lower_case(line)

            call get_word(line, word)

            if( word == "" .or. word(1:1) == "#" ) cycle

            if( word == "finish" ) then

                more = .false.

            else if( word == "vdwstart" ) then

                if( opt_read(1) ) call cry(uout, '', &
                    "ERROR: '"//trim(word)//"' has already been defined in 'lambdapath' block in FIELD", 999)

                opt_read(1) = .true.

                call get_word(line, word)
                lambda_vdw_lbound = word_2_real(word)

            else if( word == "vdwend" ) then

                if( opt_read(2) ) call cry(uout, '', &
                    "ERROR: '"//trim(word)//"' has already been defined in 'lambdapath' block in FIELD", 999)

                opt_read(2) = .true.

                call get_word(line, word)
                lambda_vdw_ubound = word_2_real(word)

            else if( word == "coulstart" ) then

                if( opt_read(3) ) call cry(uout, '', &
                    "ERROR: '"//trim(word)//"' has already been defined in 'lambdapath' block in FIELD", 999)

                opt_read(3) = .true.

                call get_word(line, word)
                lambda_coul_lbound = word_2_real(word)

            else if( word == "coulend" ) then

                if( opt_read(4) ) call cry(uout, '', &
                    "ERROR: '"//trim(word)//"' has already been defined in 'lambdapath' block in FIELD", 999)

                opt_read(4) = .true.

                call get_word(line, word)
                lambda_coul_ubound = word_2_real(word)

            else

                ! Unrecognised keyword

                call cry(uout, '', &
                    "ERROR: Unrecognised keyword '"//trim(word)//"' in 'lambdapath' block in FIELD.", 999)

            end if

        end do

    end subroutine read_lambdapath




    !> Prints the lambda path parameters and checks their validity. This is safe to call if lambda is not
    !> in use - it only does something if the `using_lambda_ff` flag is .true.
    subroutine print_check_lambdapath()

        use constants_module, only : uout
        use comms_mpi_module, only : master, idnode

        real(kind = wp), parameter :: tol = 1.0E-9_wp

        if( .not. using_lambda_ff ) return

        if( master ) then

            write(uout,"(/,/,1x,'Detected lambda-dependent force field is in use...')")

            write(uout,"(/,1x,100('-'))")
            write(uout,"(' Hamiltonian coupling parameter (lambda) path parametrisation:')")
            write(uout,"(1x,100('-'))")

            write(uout,"(/,1x,a,f8.6)") &
                "lambda used in VdW potentials is 0 below master lambda = ",lambda_vdw_lbound
            write(uout,"(/,1x,a,f8.6)") &
                "lambda used in VdW potentials is 1 above master lambda = ",lambda_vdw_ubound
            write(uout,"(/,1x,a,f8.6,a,f8.6)") &
                "lambda used in VdW potentials increases from 0 to 1 between master lambda = ",lambda_vdw_lbound, &
                " and ",lambda_vdw_ubound

            write(uout,"(/,1x,a,f8.6)") &
                "lambda used in Coulomb is 0 below master lambda = ",lambda_coul_lbound
            write(uout,"(/,1x,a,f8.6)") &
                "lambda used in Coulomb is 1 above master lambda = ",lambda_coul_ubound
            write(uout,"(/,1x,a,f8.6,a,f8.6)") &
                "lambda used in Coulomb increases from 0 to 1 between master lambda = ",lambda_coul_lbound, &
                " and ",lambda_coul_ubound

            write(uout,"(/,/,/)")
       
        end if

        if( lambda_vdw_lbound < 0.0_wp-tol .or. lambda_vdw_lbound > 1.0_wp+tol )  call cry(uout, '', &
                    "ERROR: 'vdwstart' in 'lambdapath' block in FIELD must be between 0 and 1", 999)

        if( lambda_vdw_ubound < 0.0_wp-tol .or. lambda_vdw_ubound > 1.0_wp+tol )  call cry(uout, '', &
                    "ERROR: 'vdwend' in 'lambdapath' block in FIELD must be between 0 and 1", 999)

        if( lambda_coul_lbound < 0.0_wp-tol .or. lambda_coul_ubound > 1.0_wp+tol )  call cry(uout, '', &
                    "ERROR: 'coulstart' in 'lambdapath' block in FIELD must be between 0 and 1", 999)

        if( lambda_coul_lbound < 0.0_wp-tol .or. lambda_coul_ubound > 1.0_wp+tol )  call cry(uout, '', &
                    "ERROR: 'coulend' in 'lambdapath' block in FIELD must be between 0 and 1", 999)

    end subroutine print_check_lambdapath




    !> Sets the master Hamiltonian coupling parameter lambda to the specified value, and updates the variables corresponding
    !> to the VdW and Coulomb coupling parameters accordingly
    subroutine set_lambda(lambda_new)

            !> The new value for the master lambda
        real(kind = wp), intent(in) :: lambda_new

        lambda = lambda_new


        ! Set lambda_vdw

        if(lambda < lambda_vdw_lbound) then

            lambda_vdw = 0.0_wp

        else if(lambda > lambda_vdw_ubound) then

            lambda_vdw = 1.0_wp

        else

            lambda_vdw = (lambda - lambda_vdw_lbound) / (lambda_vdw_ubound - lambda_vdw_lbound)

        end if


        ! Set lamdba_coul

        if(lambda < lambda_coul_lbound) then

            lambda_coul = 0.0_wp

        else if(lambda > lambda_coul_ubound) then

            lambda_coul = 1.0_wp

        else

            lambda_coul = (lambda - lambda_coul_lbound) / (lambda_coul_ubound - lambda_coul_lbound)

        end if


    end subroutine



    !> Sets the charges on atoms in the specified configuration so that they reflect the current
    !> value of the Coulomb-specific Hamiltonian coupling parameter 'lambda_coul'
    subroutine set_lambda_charges_cfg(cfg)

        use config_type

            !> Configuration to work on
        type(config), intent(inout) :: cfg

        integer :: j

        do j = 1, cfg%num_mols

            call set_lambda_charges_mol(cfg%mols(j))

        end do

    end subroutine set_lambda_charges_cfg



    !> Sets the charges on atoms in the specified molecule so that they reflect the current
    !> value of the Coulomb-specific Hamiltonian coupling parameter 'lambda_coul'
    subroutine set_lambda_charges_mol(mol)

        use molecule_type
        use config_type
        use species_module, only : lambda_dep_charges, uniq_mol
        use molecule_module, only : scale_mol_charges_from_template

            !> Molecule to work on
        type(molecule), intent(inout) :: mol

        integer :: mol_spec

        mol_spec = mol%mol_label

        if( lambda_dep_charges(mol_spec) ) then

            call scale_mol_charges_from_template(mol, uniq_mol(mol_spec), lambda_coul)

        end if

    end subroutine set_lambda_charges_mol




end module lambda_module
